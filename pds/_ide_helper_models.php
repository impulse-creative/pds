<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace {
/**
 * User Table Model
 *
 * @author Derek J. Foster
 * 
 * First Release: v1.0
 * @property integer $id
 * @property string $hs_acc_number
 * @property string $fname
 * @property string $lname
 * @property string $pharmacy
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property string $remember_token
 * @property integer $confirmed_user
 * @property string $groups
 * @property integer $whitelisted_ip_list
 * @property \Carbon\Carbon $deleted_at
 * @property string $sf_id
 * @property string $bio
 * @property string $bio_image
 * @property string $associated_data
 * @property string $badges
 * @property-read \Illuminate\Database\Eloquent\Collection|\FileStorage[] $file
 * @property-read \Vendor $marketListing
 * @property-read \GuideEntries $guideEntry
 * @property-read \GuideAds $guideAd
 * @property-read \Illuminate\Database\Eloquent\Collection|\VipTicketRecipient[] $ticket_recipients
 * @property-read \RaffleItems $raffleItem
 * @property-read \Sponsorships $sponsorships
 * @property-read \Illuminate\Database\Eloquent\Collection|\Resources[] $resources
 * @property-read \UserAchievement $UserAchievement
 * @method static \Illuminate\Database\Query\Builder|\User whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereHsAccNumber($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereFname($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereLname($value) 
 * @method static \Illuminate\Database\Query\Builder|\User wherePharmacy($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\User wherePassword($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereRememberToken($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereConfirmedUser($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereGroups($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereWhitelistedIpList($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereDeletedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereSfId($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereBio($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereBioImage($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereAssociatedData($value) 
 * @method static \Illuminate\Database\Query\Builder|\User whereBadges($value) 
 * @method static \User admin() 
 * @method static \User vendor() 
 */
	class User {}
}

