<?php

namespace Formativ;

interface UserMailerInterface
{
	public function send($to, $view, $data);
}