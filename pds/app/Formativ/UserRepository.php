<?php

namespace Formativ;

class UserRepository implements UserRepositoryInterface
{
	public function all(array $modifiers)
	{
		// return all the users filtered by $modifiers...
	}

	public function first(array $modifiers)
	{
		// return the first users filtered by $modifiers...
	}

	public function insert(array $data)
	{
		// insert users with $data...
		return "data found in service provider".var_dump($data);
	}

	public function update(array $data, array $modifiers)
	{
		// update users filtered by $modifiers, with $data...
	}

	public function delete(array $modifiers)
	{
		// delete users filtered by $modifiers...
	}
}