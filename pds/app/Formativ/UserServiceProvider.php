<?php
 
namespace Formativ;
 
use Illuminate\Support\ServiceProvider;
 
class UserServiceProvider
extends ServiceProvider
{
  protected $defer = true;
  
  public function register()
  {
    $this->app->bind(
      "Formativ\\UserRepositoryInterface",
      "Formativ\\UserRepository"
    );
  
    $this->app->bind(
      "Formativ\\UserValidatorInterface",
      "Formativ\\UserValidator"
    );
  
    $this->app->bind(
      "Formativ\\UserMailerInterface",
      "Formativ\\UserMailer"
    );
  }
  
  public function provides()
  {
    return [
      "Formativ\\UserRepositoryInterface",
      "Formativ\\ValidatorInterface",
      "Formativ\\MailerInterface"
    ];
  }
}