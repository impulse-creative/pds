<?php

namespace Formativ;

use \Illuminate\Validation\Factory;
use Illuminate\Http\Request;
use Str;

class UserValidator implements UserValidatorInterface
{

	/**
	* Validator
	*
	* @var Illuminate\Validation\Factory
	*/
	protected $validator;

	public function __construct(Request $request, Factory $validator)
	{
		$this->request = $request;
		$this->validator = $validator;
	}

	public function passes($event)
	{
		//validate the event instance
		$data = [$this->request->get('password'), $this->request->get('email')];
        $validator = $this->validator->make($data, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
		
		if($validator->fails()) {
			return false;
		}
        return true;
	}

	public function messages($event)
	{
		//fetch the error messages for the event instance
	}

	public function on($event)
	{
		//set up the event instance and return it for chaining
	}
}