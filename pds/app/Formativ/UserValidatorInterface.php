<?php

namespace Formativ;

interface UserValidatorInterface
{
	public function passes($event);
	public function messages($event);
	public function on($event);
}