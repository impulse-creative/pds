<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class DailyDigest extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'email:daily';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Email Daily Digest.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
	}

	public function fire()
	{
            $this->line('Welcome to the daily digest emailer.');
            set_time_limit(0);
            $daily = \Notifications::where('digest', '1')->get();
            
            foreach($daily as $single) {
                //echo 'ID: ' . $single->user_id. '<br>';
                $mail = \User::sendDailyDigestEmail($single->user_id);
            }
            $this->line('Completed.');
	}



}

