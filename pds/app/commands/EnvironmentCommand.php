<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EnvironmentCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'environment';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Lists environment commands.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->line(trim("
			<comment>environment:get</comment>
			<info>gets host and environment.</info>
		"));

		$this->line(trim("
			<comment>environment:set</comment>
			<info>adds host to environment</info>
		"));

		$this->line(trim("
			<comment>environment:remove</comment>
			<info>removes host from environment</info>
		"));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

	protected function getHost()
	{
		return gethostname();
	}

	protected function getEnvironment()
	{
		return App::environment();
	}	

}
