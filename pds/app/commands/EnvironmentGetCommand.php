<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EnvironmentGetCommand extends EnvironmentCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'environment:get';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get host and environment.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->line(trim("
			<comment>Host:</comment>
			<info>" . $this->getHost() . "</info>
		"));

		$this->line(trim("
			<comment>Environment:</comment>
			<info>" . $this->getEnvironment() . "</info>
		"));
	}
}
