<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EnvironmentSetCommand extends EnvironmentCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'environment:set';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Adds host to environment.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$host = $this->getHost();
		$config = $this->getConfig();
		$overwrite = $this->option("host");
		$environment = $this->argument("environment");

		if (!isset($config[$environment]))
		{
			$config[$environment] = [];
		}

		$use = $host;
		
		if ($overwrite)
		{
			$use = $overwrite;
		}

		if (!in_array($use, $config[$environment]))
		{
			$config[$environment][] = $use;
		}

		$this->setConfig($config);

		$this->line(trim("
			<info>Added</info>
			<comment>". $use . "</comment>
			<info>to</info>
			<comment>" . $environment . "</comment>
			<info>environment</info>
		"));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array[
			[
				"environment",
				InputArgument::REQUIRED,
				"Environment to add the host to."
			]
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array[
			[
				"host",
				null,
				InputOption::VALUE_OPTIONAL,
				"Host to add.",
				null
			]
		];
	}

}
