<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class WeeklyDigest extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'email:weekly';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Email Weekly Digest.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
	}

	public function fire()
	{
            $this->line('Welcome to the weekly digest emailer.');
            set_time_limit(0);
	        $weekly = \Notifications::where('digest', '2')->get();
	        foreach($weekly as $single) {
	            $mail = \User::sendWeeklyDigestEmail($single->user_id);
	        }
            $this->line('Completed.');
	}



}

