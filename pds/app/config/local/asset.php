<?php

return [
		"dash-css" => [
			"//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css",
			"js/jquery-ui/jquery-ui.min.css",
			"css/dash-styles.css"
		],
		"dash-js" => [
			"js/custom-scripts.js",
			"js/jquery.session.js",
			"js/script.js",
			"//code.jquery.com/ui/1.11.3/jquery-ui.min.js"
		]
	];