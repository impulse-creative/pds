<?php

return [
		"dash-css" => [
			"css/Bootstrap/css/bootstrap.min.css",
			"js/jquery-ui/jquery-ui.min.css",
			"css/dash-styles.css"
		],
		"dash-js" => [
			"js/custom-scripts.js",
			"js/jquery.session.js",
			"js/script.js",
			"//code.jquery.com/ui/1.11.3/jquery-ui.min.js"
		],
		"dash-production-css" => [
			"css/Bootstrap/css/bootstrap.min.css",
			"css/shared-dash-css.min.css" => [ //don't forget to update the shared-css.min.php file array with any new files that you add
				"js/jquery-ui/jquery-ui.min.css",
				"css/dash-styles.css"
			]
		]
	];