<?php

class AccessGroupController extends \BaseController {

        public function __construct() 
        {
            parent::__construct();
            $css = [ 'main', 'dash', 'forum'];
            View::share('css', $css);
            $permissionsArray = $this->getPermissionsArray(\Group::all());
            View::share('permissionsArray', $permissionsArray);
        }
        
        public function getPermissionsArray($allPermissions) 
        {
            $permissionsArray = [];
            foreach($allPermissions as $permission) {
                $permissionsArray[$permission->id] = $permission->description;
            }
            return $permissionsArray;
        }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $accessGroups = AccessGroup::all();
            return View::make('dashboard.Admin.access-groups.index')->with('accessGroups', $accessGroups);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('dashboard.Admin.access-groups.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $rules = [
                'name'          => 'required',
                'description'   => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                return Redirect::to('access-groups/create')->withErrors($validator)->withInput(Input::all());
            }
            $name = Input::get('name');
            $description = Input::get('description');
            $permissions = Input::get('permissions');
            $message = AccessGroup::createAccessGroup($name, $description, $permissions);
            Session::flash('message', $message);
            return Redirect::to('access-groups');
            
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            return View::make('dashboard.Admin.access-groups.edit');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
