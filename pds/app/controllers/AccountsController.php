<?php

use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;
use UserRepo\Storage\User\UserRepository as UserRepoInterface;
use MarketRepo\temp\Vendors\VendorRepository as TmpVendors;
use MarketRepo\production\Vendors\PdsVendorRepository as PdsVendors;
use NotificationRepo\Notifications\NotificationRepository as NotificationInterface;
use Carbon\Carbon as Carbon;
use TeamworkRepo\TeamworkApi as TwApi;

class AccountsController extends BaseController {

    /**
     * Instantiate filters
     */
    public function __construct(GroupPermissions $groups, UserRepoInterface $user, TmpVendors $vendors, PdsVendors $pds_vendors, NotificationInterface $notifications) {

        $this->beforeFilter('csrf', array('on' => 'login'));
        $this->groups = $groups;
        $this->user = $user;
        $this->vendors = $vendors;
        $this->new_vendors = $pds_vendors;
        $this->notifications = $notifications;
        $this->api_key = 'cut656tree';
        $this->tw_company = 'pharmacyowners';
        $this->impulse_tw_id = '45584';
        $this->impulse_test_tasklist_id = Auth::user()->tasklist_ids;
        $this->requesting_user = NULL;
        $css = [ 'main', 'dash', 'forum'];
        View::share('css', $css);
    }

    /**
     * Display a listing of the accounts.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new account.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created account in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    public function refreshUserPermissions($userId) {
        $admin = strpos(Auth::user()->groups, '5') > -1 ? true : false;
        if($admin) {
            $permissions = $this->groups->updateUserPermissions($userId);
        }
        return $this->showAccounts();
    }
    
    public function getAppStats() {
        $stats['totalActive'] = User::where('confirmed_user', '1')->count();
        $stats['thirtyDaysActive'] = User::where('confirmed_user', '1')->where('updated_at', '>=', Carbon::now()->subDays(30))->count();
        $stats['sixMonthsActive'] = User::where('confirmed_user', '1')->where('updated_at', '>=', Carbon::now()->subMonths(6))->count();
        $stats['twentyFourHoursActive'] = User::where('confirmed_user', '1')->where('updated_at', '>=', Carbon::now()->subHours(24))->count();
        return $stats;
    }
    
    //show all user accounts in a paginated list
    public function showAccounts() {
        $stats = $this->getAppStats();
        
        $user = User::find(Auth::id());
        if ($this->checkForDownload() == true) {
            $this->getVendorProgressReport();
        }
        if (!$this->groups->checkIfAdmin(Auth::id())) {
            return Redirect::back()->with('Failure', 'Sorry. You do not have access to this feature');
        }
        $searchParam = '';
        $accounts = User::orderBy('pharmacy', 'ASC')->paginate('20');
        return View::make('dashboard.Admin.profile-summaries.index', compact('searchParam', 'accounts', 'user', 'stats'));
    }

    protected function vendorSignupCompletionPercentage() {
        $vendor = new Vendor();
        return $vendor->signupCompletionPercentage();
    }

    public function showSearchedAcc($type = false) {
        if (!$this->groups->checkIfAdmin(Auth::id()) || !$this->groups->checkIfPdsAdmin(Auth::id())) {
            return Redirect::back()->with('Failure', 'Sorry. You do not have access to this feature');
        }
        $stats = $this->getAppStats();
        $searchParam = '';
        
        
        switch($type) {
            case false:
            case '{id}':
                $accounts = User::search(Input::get('search-param'))->paginate(20);
                break;
            case 'most-recent':
                $accounts = User::orderBy('updated_at', 'DESC')->paginate('20');
                break;
            case 'confirmations-check':
                $accounts = User::where('confirmed_user', '0')->where('remember_token', '!=', '')->orderBy('updated_at', 'DESC')->paginate('20');
                break;
            case 'last-ten-minutes':
                $accounts = User::where('updated_at', '>=', Carbon::now()->subMinutes(15))->orderBy('updated_at', 'DESC')->paginate('20');
            case 'non-confirmed-active':
                $accounts = User::where('confirmed_user', '0')->where('updated_at', '>=', Carbon::now()->subDays(1))->orderBy('updated_at', 'DESC')->paginate('20');
            default:
                Redirect::route('accounts.index')->with('No records', 'Sorry, we could not find any matching or similar records. Please try another keyword.')->withInput();
        }
        return View::make('dashboard.Admin.profile-summaries.index', compact('accounts', 'searchParam', 'stats'));
    }

    //show summary of user account
    public function showAccountSummary($id) {
        if (!$this->groups->checkIfAdmin(Auth::id())) {
            return Redirect::back()->with('Failure', 'Sorry. You do not have access to this feature');
        }
        $summary_data = array();
        //get correct dash menu for user
        $menu_data = $this->user->getMenu();

        //get user vendor account data
        $vendor_data = Vendor::where('user_id', '=', $id)->first();

        //get user guide entry data
        $guide_entry_data = GuideEntries::where('user_id', '=', $id)->first();

        //guet user guide ad data
        $guide_ad_data = GuideAds::where('user_id', '=', $id)->first();
        //retrieve all ad types
        $guide_ads_array = isset($guide_ad_data->ad_types) ? explode(',', $guide_ad_data->ad_types) : '0';
        $guide_ad_type = is_array($guide_ads_array) ? AdTypes::whereIn('id', $guide_ads_array)->get() : array('None' => 'No Ads Chosen');
        $guide_ad_billing = isset($guide_ad_data->billing_info) ? $guide_ad_data->billing_info : 'No billing choice';
        //retrieve billing choice
        $guide_ad_billing_choice = BillTypes::where('id', '=', $guide_ad_billing)->first();
        $giveaway_item = RaffleItems::where('id', '=', $id)->first();
        //get all vip ticket recipients	
        $vip_recipients = VipTicketRecipient::where('user_id', '=', $id)->get();

        //get all sponsorship choices
        $sponsorships = Sponsorships::where('user_id', '=', $id)->first();
        $sponsorship_list = is_object($sponsorships) ? explode(':', $sponsorships->sponsorship_types) : ['0'];

        //dissect list of sponsorship choices (if exists)
        $sponsorship_choices = SponsorshipTypes::whereIn('id', $sponsorship_list)->get();
        if (!is_object($sponsorship_choices))
            $sponsorship_choices = $sponsorship_list;
        //get general user data
        $summary_data['user_data'] = $this->user->find($id);
        $groups = $this->groups->getGroupsByArray(explode(':', $user_data->groups));
        $category = is_object($vendor_data) ? $this->getCategory($vendor_data->category) : '';
        $subCategory = is_object($vendor_data) ? $this->getSubCategory($vendor_data->subCategory) : '';
        //send all user account data to view
        return View::make('dashboard.Admin.profile-summaries.summary', compact('summary_data', 'menu_data', 'vendor_data', 'guide_entry_data', 'user_data', 'guide_ad_data', 'guide_ad_type', 'guide_ad_billing_choice', 'vip_recipients', 'sponsorship_choices', 'groups', 'category', 'subCategory', 'giveaway_item'));
    }

    public function getCategory($id) {
        $category = MarketCategories::find($id);
        return $category->category_name;
    }

    public function getSubCategory($id) {
        $subCategory = MarketSubCategories::find($id);
        return $subCategory->name;
    }
    
    public function ghostUser($user_id){
        $ghoster_id = Auth::id();
        $expiresAt = \Carbon\Carbon::now()->addMinutes(10);
        $user_to_ghost = User::find($user_id);
        
        if($user_to_ghost->confirmed_user == 1)
        {
            Cache::add('ghoster', $ghoster_id, $expiresAt);
            Auth::loginUsingId($user_to_ghost->id);
            return Redirect::route('forum.index');
        }
        
        return Redirect::back();
       
    }
    
    public function unghostUser($ghoster){
        Auth::loginUsingId($ghoster);
        Cache::forget('ghoster');
        return Redirect::route('accounts.index');
    }
    

    /**
     * Show the form for editing the specified account.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the user profile information
     *
     * @return Redirect
     */
    public function update() {
        //Update profile information
        $input = Input::all();
//        dd($input);
        //intialize validation rules
        if(\Input::get('changepassword') == 1) {
            $rules = User::$password_change_rules;
        } else {
            $rules = User::$profile_update_rules;
        }
        $validation = self::formValidator($input, $rules);

        if ($validation == true) {
            return $this->updateBasicInformation($input);
        } else {
            return Redirect::back()->with("Failure", "Please fill out all of the necessary fields to change your information.")->withInput();
        }
        //should never come to this point
        return Redirect::back()->with('Internal Error', "Something is wrong with validation methods in the accounts controller. Please contact your site administrator through your support services.");
    }

    /**
     * Method that updates basic user information 
     * 
     * @param array newInfo
     */
    protected function updateBasicInformation($newInfo) {
        $assData['facebook'] = $newInfo['facebook'];
        $assData['twitter'] = $newInfo['twitter'];
        $assData['linkedin'] = $newInfo['linkedin'];

        $subscription_preferences = $this->notifications->setUserSubscriptionPreferences($newInfo['user_id'], $newInfo['digest']);
        $achievements = $this->checkForAchievements($newInfo);
        return $this->user->updateUserProfile($assData, $newInfo);
    }

    public function storeFile($path, $inputName) {
        //attempt to retrieve served file from client
        if ($_FILES[$inputName]["size"][0] !== 0) {
            //intialize variables
            $data = Input::file(); //array of files
            $numOfFiles = count($data[$inputName]); //number of files in array with index 'file'
            //remove previous logo associated with the company (this is in case of a logo update)
            foreach (glob(public_path() . $path . Auth::id() . "_*.*") as $filename) {
                //deletes the previous files
                unlink($filename);
            }

            //set the file name prefixed with the user id (good for deletes, moves, etc.)
            $name = Auth::id() . "_" . rand(0, 10000) . "_" . $data[$inputName]->getClientOriginalName();
            //find temp location when initially downloaded.
            $tmp_path = $data[$inputName]->getRealPath();
            //construct the store path of our server (this is where the file will be stored)
            $local_store_path = public_path() . $path . $name;
            //store the file on the server 
            $local_path = self::storeFileLocally($name, $data[$inputName], $local_store_path, $path);

            return $name;
        } else {
            //send user back if file is non-existant
            return Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    public static function storeFileLocally($filename, $file, $local_path, $path) {
        //move file from the tmp directory to '/storage/files', using public path as the root
        $file->move(public_path() . $path, $filename);

        //return path the file was stored in
        return $local_path;
    }

    public function checkForAchievements($data) {
        $profileImage = is_object($data['bio_image']) ? \UserAchievement::addAchievement($data['user_id'], 6) : '';
        $facebook = $data['facebook'] != '' ? \UserAchievement::addAchievement($data['user_id'], 7) : '';
        $twitter = $data['twitter'] != '' ? \UserAchievement::addAchievement($data['user_id'], 7) : '';
        $linkedin = $data['linkedin'] != '' ? \UserAchievement::addAchievement($data['user_id'], 7) : '';
        $bio = $data['bio'] != '' ? \UserAchievement::addAchievement($data['user_id'], 8) : '';
        return null;
    }

    public function updateVendorAccount() {
        self::global_xss_clean();

        //set validation rules
        $rules = $this->vendors->getUpdateRules();

        //do the validation
        $validator = Validator::make(Input::all(), $rules);

        //check for validation failure
        if ($validator->fails()) {
            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput()->with('Failure', 'Could not update your vendor account! Please check your input and try again.');
        } else {
            return $this->vendors->updateVendorAccount();
        }
    }

    /**
     * Method that approves a marketplace listing for viewing 
     *
     * @param int $id //listing id
     */
    public function updateToApproved($id) {
        $user = $this->new_vendors->getByCompanyId($id);
        $condition = Input::get('undo');

        $user->approved = $condition == 1 ? 0 : 1;
        if ($user->save()) {
            if ($condition == 1)
                return Redirect::back()->with('Success', 'Account successfully removed from approved listings. Approve the account to see it in the <a href="' . URL::to("marketplace/company/list") . '" class="force-orange-text" target="_blank">listings</a>');

            $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $user->company_name);
            $approved_acc_profile = '<a href="' . URL::to("/marketplace/company") . '/' . $user->id . '/' . preg_replace('/ /', '-', $scrubbed_title) . '" class="force-orange-text">here</a>';
            return Redirect::back()->with('Success', 'Account successfully approved for the marketlace. View the listing by clicking "View Marketplace Listing" above!');
        }

        return Redirect::back()->with('Failure', 'Account could not be approved. Either the user does not exist or the id used was invalid. Contact the web administrator if this problem persists.');
    }

    /**
     * Method that makes a marketplace listing a featured listing
     *
     * @param int $listing_id
     */
    public function updateToFeatured() {
        $un_feature = Input::get('un_feature');
        $listing_id = Input::get('listing_id');
        $vendor = Vendor::find($listing_id);

        if ($un_feature) {
            $vendor->featured = 0;
            if ($vendor->save())
                return Redirect::back()->with('Success', 'Listing has been removed as a featured marketplace vendor.');
        }else {
            $vendor->featured = 1;
        }

        $marketplace_url = URL::route("marketplace.index");
        if ($vendor->save())
            return Redirect::back()->with('Success', 'Listing has successfully been added as a featured marketplace vendor. <a class="orange-text" href="' . $marketplace_url . '">Check the listing page now</a>');

        return Redirect::back()->with('Failure', 'Was not able to update listing to featured');
    }

    /**
     * Method that confirms a user account, only used if a user has trouble with confirming registration
     *
     * @param int $id //account id
     */
    public function updateToConfirmed($id) {
        $user = $this->user->find($id);
        $user->confirmed_user = 1;
        $updated = User::updateConfirmedInHubspot($user->email);
        if ($user->save()) {
            return Redirect::back()->with('Success', 'User account was successfully confirmed');
        }

        return Redirect::back()->with('Failure', 'User could not be confirmed. User does not exist.');
    }
    
    public function updateDirectlyFromDanQuote()
    {
        $input = [
            'announcement' => trim(Input::get('announcement-content')),
            'edited_by' => Auth::id()
        ];
        $rules = [
            'announcement' => 'required'
        ];
                
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) {
            return Redirect::back()->with('Failure', 'Could not save the new quote. Please check your input and try again.');
        }
        
        $old_entry = DB::table('dan_quote')->select('id')->first();
        
        if (count($old_entry) > 0) {
            $new_quote = DB::table('dan_quote')->where('id', $old_entry->id)->update($input);
            
            return Redirect::back()->with('Success', 'Successfully saved the new quote.');
        }
        
        $new_quote = DB::table('dan_quote')->insertGetId($input);
        
        if (intval($new_quote)) {
            return Redirect::back()->with('Success', 'Successfully saved the new quote.');
        }
        
        throw new Exception('[Accounts method: Updating Dan Quote] Could not save the new quote, but validating your input was successful, something unexpected happened when saving the data.');
    }

    /**
     * Helper method to help check validators response easier
     * 
     * @param array userInput
     * @param array rules  --> for help on this, see: http://laravel.com/docs/validation#basic-usage
     * @return bool
     */
    protected function formValidator($userInput, $rules) {

        //intialize validator
        $validator = Validator::make($userInput, $rules);

        //check for validation failure
        if ($validator->fails()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Remove the specified account from the database.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $user = new User();
        $response = $user->removeUserAcc($id);

        if ($response) {
            return Redirect::back()->with('Success', 'User was successfully removed.');
        }

        return Redirect::back()->with('Failure', 'Could not remove user account. Please try again.');
    }

    /**
     * Clean all input received from the user's submitted form 
     *
     * @param Post data
     * @return none
     * @author Derek J. Foster
     */
    public static function global_xss_clean() {
        // Recursive cleaning for array [] inputs, not just strings.
        $sanitized = static::cleanInput(Input::all());
        Input::merge($sanitized);
    }

    public static function cleanInput($dirty_values) {
        $result = array();

        foreach ($dirty_values as $key => $value) {
            echo '<script>//console.log(' . $key . ');</script>';
            // Don't allow tags on key either, maybe useful for dynamic forms.
            $key = strip_tags($key);

            // If the value is an array, we will just recurse back into the
            // function to keep stripping the tags out of the array,
            // otherwise we will set the stripped value.
            if (is_array($value)) {
                $result[$key] = static::cleanInput($value);
            } else {
                // I am using strip_tags(), you may use htmlentities(),
                // also I am doing trim() here, you may remove it, if you wish.
                $result[$key] = trim(strip_tags($value));
            }
        }

        return $result;
    }

    public function checkForDownload() {
        $queryVar = Input::get('downloadVendorProgressReport');
        if (isset($queryVar) && $queryVar == 1) {
            return true;
        }
        return false;
    }

    public function getVendorProgressReport() {
        $vendors = User::where('groups', 'like', '%3%')->get();
        $csv = "id, company, name, email, Marketplace, 100 Word Profile, Guide Ad Completed, Raffle Item Completed, Vip Tickets Complete, Sponsorships, Confirmed Email, Synced Company\r\n";
        foreach ($vendors as $vendor) {
            $id = $vendor->id;
            $company = str_replace($vendor->pharmacy, ',', '');
            $name = $vendor->fname . ' ' . $vendor->lname;
            $email = $vendor->email;
            $confirmed = $vendor->confirmed_user == 0 ? 'not confirmed' : 'confirmed';
            $synced = $vendor->hss_acc_number != 'none' ? 'true' : 'false';
            $listing_completed = !is_object(Vendor::where('user_id', '=', $id)->first()) ? '' : 'completed';
            $market_profile_completed = !is_object(GuideEntries::where('user_id', '=', $id)->first()) ? '' : 'completed';
            $guide_ad_completed = !is_object(GuideAds::where('user_id', '=', $id)->first()) ? '' : 'completed';
            $raffle_item_completed = !is_object(RaffleItems::where('user_id', '=', $id)->first()) ? '' : 'completed';
            $vip_tickets_complete = !is_object(VipTicketRecipient::where('user_id', '=', $id)->first()) ? '' : 'completed';
            $sponsorships_complete = !is_object(Sponsorships::where('user_id', '=', $id)->first()) ? '' : 'completed';
            $csv .= "$id, $company, $name, $email, $listing_completed, $market_profile_completed, $guide_ad_completed, $raffle_item_completed, $vip_tickets_complete, $sponsorships_complete, $confirmed, $synced\r\n";
        }
        $date = date('F-d-Y');
        $directory = public_path() . '/storage/reports/';
        $filename = $date . '_Vendor-Progress-Report.csv';
        $fileSave = file_put_contents($directory . $filename, $csv);
        if ($fileSave != false) {
            $url = URL::to('/storage/reports/');
            echo '<iframe id="download" class="hidden" src="' . $url . '/' . $filename . '">Your browser doesn\'t support this download</iframe>';
            return Redirect::route('dash.main');
        } else {
            Session::flash('There was an error saving your report.');
        }
    }
    
}
