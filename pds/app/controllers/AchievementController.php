<?php

class AchievementController extends \BaseController {

        public function __construct() {
            parent::__construct();
            $css = [ 'dash', 'forum', 'main' ];
            View::share('css', $css);
        }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $achievements = Achievement::getActiveAchievements();
            return View::make('dashboard.Admin.achievements.index')->with('achievements', $achievements);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('dashboard.Admin.achievements.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $rules = [
                'name'          => 'required',
                'description'   => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                
                return Redirect::to('achievements/create')->withErrors($validator)->withInput(Input::all());
            }
            $name = Input::get('name');
            $description = Input::get('description');
            $message = Achievement::createAchievement($name, $description);
            Session::flash('message', $message);
            return Redirect::to('achievements');
            
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
        {
            $achievement = Achievement::find($id);
            return View::make('dashboard.Admin.achievements.show')->with('achievement', $achievement);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $achievement = Achievement::find($id);
            return View::make('dashboard.Admin.achievements.edit')->with('achievement', $achievement);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $rules = [
                'name'          => 'required',
                'description'   => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                return Redirect::to('achievements/'.$id.'/edit')->withErrors($validator)->withInput(Input::all());
            }
            $name = Input::get('name');
            $description = Input::get('description');
            $message = Achievement::editAchievement($id, $name, $description);
            Session::flash('message', $message);
            return Redirect::to('achievements');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
            return dd('Not Allowed');
	}


}
