<?php

class BadgeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
         public function __construct() {
             parent::__construct();
             $css = [ 'dash', 'forum', 'main' ];
            View::share('css', $css);
            View::share('pageTitle', 'Badges | PDSadvantage');
         }
    
	public function index()
	{
            $badges = Badge::getActiveBadgesForIndex();
            $achievements = \Achievement::all();
            return View::make('dashboard.Admin.badges.index', compact('badges', 'achievements'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            $achievements = \Achievement::getActiveAchievements();
            return View::make('dashboard.Admin.badges.create')->with('achievements', $achievements);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $rules = [
                'name'          => 'required',
                'description'   => 'required',
                'badge_image'   => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                
                return Redirect::to('badges/create')->withErrors($validator)->withInput(Input::all());
            }
            $name = Input::get('name');
            $description = Input::get('description');
            if(Input::file('badge_image')) {
                $filename = Badge::storeBadge(preg_replace('/[^A-Za-z0-9 _ .-]/', '', $name), 'badge_image');
            }
            $achievements = Input::get('achievements');
            $message = Badge::createBadge($name, $description, $filename, $achievements);
            Session::flash('message', $message);
            return Redirect::to('badges');
            
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
        {
            $badge = Badge::find($id);
            return View::make('dashboard.Admin.badges.show')->with('badge', $badge);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $badge = Badge::find($id);
            $achievements = \Achievement::all();
            return View::make('dashboard.Admin.badges.edit', compact('badge', 'achievements'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $rules = [
                'name'          => 'required',
                'description'   => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                
                return Redirect::to('badges/'.$id.'/edit')->withErrors($validator)->withInput(Input::all());
            }
            $name = Input::get('name');
            $description = Input::get('description');
            $url = Input::get('url');
            $achievements = Input::get('achievements');
            $message = Badge::editBadge($id, $name, $description, $url, $achievements);
            Session::flash('message', $message);
            return Redirect::to('badges');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
            
	}



}
