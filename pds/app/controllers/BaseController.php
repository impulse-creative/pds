<?php

use Former\Facades\Former;
use Carbon\Carbon as Carbon;

class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public function __construct() {
        $sections = Auth::check() ? $this->getUserData() : '';
        $this->global_permissions_access = Auth::check() ? $sections['permissions'] : '';
        $this->global_user_info = Auth::check() ? $sections['user'] : '';
        View::share('sections', $sections);
        View::share('pageTitle', 'PDSadvantage');
    }

    protected function setupLayout() {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function getUserData() {
        $id = Auth::id();
        $permissions = \User::getCurrentPermissions();
        $sections['user'] = $this->find($id);
        $sections['menu-data'] = $this->getMenu();
        $sections['permissions'] = $permissions;
        $sections['permissionsArray'] = explode(':', \User::getCurrentPermissions());
        $sections['name'] = $sections['user']['fname'] . ' ' . $sections['user']['lname'];
        $sections['profile_url'] = $this->scrubUserName($sections['name']);
        $sections['getting-started'] = $this->returnGettingStartedNotifications($id);
        $sections['notifications'] = $this->getNotifications($id);
        $sections['notifications-record'] = Notifications::userHasNotificationsRow($id);
        return $sections;
    }
    
    
    public function getNotifications($id) {
        $notifications = new Notifications();
        return $notifications->getAllNotifications($id);
    }
    
    public function returnGettingStartedNotifications($id) {
        $achievements = UserAchievement::getUserAchievements($id);
        return 9 - count((array) $achievements->completed);
    }

    public function find($id) {
        return \User::find($id);
    }

    public function getMenu() {
        $permittedMenu = 'dashboard.group-dash-views.menu';
        return $permittedMenu;
    }

    public function scrubUserName($name) {
        $scrubbed = preg_replace('/[^a-z\d\s]+/i', '', $name);
        $urlSafe = preg_replace('/ /', '-', $scrubbed);
        return $urlSafe;
    }

}
