<?php
use BoxView\lib\BoxViewApi;
use BoxView\lib\BoxViewDocument;
use BoxView\BoxViewHandler\BoxViewCreator as FileCreator;
use BoxView\BoxViewHandler\BoxViewDisplay as FileDisplay;
use BoxView\BoxViewHandler\BoxViewDestroyer as FileDestroyer;

class BoxView extends \BaseController {

        public function __construct() {
            parent::__construct();
            $this->box_api_key = '5qri8f4u0731tlcvgdfqlvmh9abgxtcq';
            if (Auth::check()) {
                View::share('usr_email', Auth::user()->email);
                View::share('usr_name', Auth::user()->fname);
                View::share('user_permissions', explode(':', Auth::user()->groups));
            }

            $css = [ 'main', 'forum'];
            View::share('css', $css);
            View::share('pageTitle', 'PDS Resource Library | PDSadvantage');
        }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $creator = new FileCreator($this);
            return dd($creator->storeInBoxView(Input::all()));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($file_id)
	{
            set_time_limit(0);
            $file_types_array = $this->getResourceGroups();
            $display = new FileDisplay($this);
            $resource_html = $display->displayStoredFile(Input::all(), $file_id);
            $id = $file_id;
            $resource = Resources::find($id);
            $pageTitle = $resource->filename. ' | PDSadvantage';
            return View::make('Forums.Library.partials.resource-preview', compact('file_types_array', 'resource_html', 'resource', 'pageTitle'));
	}
        
        public function getResourceGroups($return_array = false, $with_permission_details = false) {
            $groups = \ResourceGroups::with('group')->get();
            if ($return_array)
                return $groups;

            $group_list = [];

            $group_list[0] = "--Select Access Level--";
            foreach ($groups as $group) {
                //check permissions for user
                $usr_groups = self::getUserGroups(\Auth::id());

                //dd($usr_groups);
                if (is_null($usr_groups))
                    return \Redirect::route('dash.main');
                if (in_array($group->permitted_groups, $usr_groups)) {
                    $group_list[$group->id] = $group->description;
                }
            }

            return $group_list;
        }
        
        public function logRefreshEvent() {
            Log::info('User refreshed resource preview.', ['resource id' => Input::get('boxview_id')]);
            return 'logging resource refresh successful';
        }

        /**
         * Get current user groups
         *
         * @param int $id //user's id
         */
        public function getUserGroups() {
            if (\Auth::check()) {
                $group_string = \Auth::user()->groups;
                $groups = explode(':', $group_string);

                $acl_list = [];
                foreach ($groups as $group) {
                    $acl_list[] = $group;
                }

                return $acl_list;
            }

            return 0;
        }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from BoxView.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
            $id = Input::get('id');
            $destroyer = new FileDestroyer($this);
            return $destroyer->destroyFile($id);
	}


}
