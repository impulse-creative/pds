<?php

class BruteForceBlocker extends BaseController {

	//array of throttle settings. # of failed responses => response
	protected static $default_throttle_settings = [
		10 => 1,			//delay in seconds
		20 => 2,			//delay in seconds
		50 => 3,			//delay in seconds
		150 => 4,			//delay in seconds
		300 => 'captcha'	//use robot catcher question
	];

	//time frame to use when retrieving number of failed login attempts from database
	protected static $time_frame_minutes = 10;
	private static $table = 'failed_logins';
	protected static $custom_throttle_settings = array();
	protected static $response_array =	array(
			'status' => 'safe',
			'message' => null
	);
	/**
	 * Set custom throttle settings 
	 *
	 * @param array $throttle_options
	 */
	protected static function setThrottleOptions($throttle_options)
	{
		self::$custom_throttle_settings = $throttle_options;
	}

	/**
	 * Set the table name to your custom failed attempts table. Defaults to 'failed_logins'
	 * 
	 * @param string $table_name
	 */
	protected static function setTable($table_name)
	{
		self::$table = $table_name;
	}

	/**
	 * Retrieve the table name
	 * 
	 * @return string $table = <table name>
	 */	
	protected static function getTable()
	{
		return self::$table;
	}

	/**
	 * Create the failed attempts table
	 *
	 * @param string $table_name
	 */
	private static function createTable()
	{
		$table_name = self::getTable();

		Schema::dropIfExists($table_name);

		Schema::create($table_name, function($table)
		{
    		$table->increments('id');
    		$table->integer('user_id');
    		$table->string('user_email');
    		$table->string('user_ip');
    		$table->dateTime('attempted');
		});

		return Response::make($table_name.' was successfully created in the database.');
	}

	//testing function
	public function testing()
	{
		$ip = self::getIpAddress();
		$response = self::checkWhitelistFrontDoor(Auth::user()->email, $ip);

		return var_dump($response);
	}

	/**
	 * Retrieve client ip address of their machine
	 *
	 * Designed to retrieve ip if the original address is masked or public in terms of standard location
	 * @return string $ip
	 */
	protected static function getIpAddress()
	{
		//retrieve client ip address
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip = $_SERVER['HTTP_CLIENT_IP']; 					//oldschool
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip = $_SERVER['HTTP_X_FORWARDED_FOR']; 			//if they have an ip mask or are in a hidden ip
		} elseif(!empty($_SERVER['REMOTE_ADDR'])) {
    		$ip = $_SERVER['REMOTE_ADDR']; 						//usual method
		}else{
			$ip = Request::getClientIp(true);
		}
		return $ip;
	}

	/**
	 * Add failed login attempt to the database
	 *
	 * @param integer $user_id
	 * @param string  $account_email
	 * @param string  $ip_address
	 * @return 		  response/redirect
	 */
	public static function addFailedLoginAttempt($user_id, $account_email, $ip_address)
	{
		//catch if user id does not exist or is invalid
		if (!is_int($user_id) || $user_id == 0) throw new Exception('User id is not valid');
		if (!$account_email) throw new Exception('[BruteForceProtection Exception]: Account email is non-existant.');
		//get current timestamp
		$current_time = date('Y-m-d H:i:s');
		$ip = self::getIpAddress();
		$table = self::getTable();
		$current_time = new DateTime();
		//attempt to insert failed login attempt
		$insert_failed_attempt = DB::table($table)->insert(array(
			array('user_id' => $user_id, 'user_email' => $account_email, 'user_ip' => $ip, 'attempted' => $current_time)
		));
		if($insert_failed_attempt) {
			return 1;
		}else {
			return Redirect::back()->with('Insert Failure', '[BruteForce Insertion Failure]: '.$e.'. Contact the web administrator.');
		}
	}

	public static function getLoginStatus($options = null)
	{
		$whitelist_check = Whitelist::checkWhitelistFrontDoor($email, $ip_in_question);

		//retrieve latest failed login attempts
		$latest_failure = self::retrieveLatestAttempts();

		//get default throttle settings, update if options parameter is not null
		if(is_null($options))
		{
			$throttle_settings = self::$default_throttle_settings;
		}else {
			$throttle_settings = $options;
		}

		//retrieve first throttle limit by resetting the array's internal pointer to the first element
		reset($throttle_settings);
		//retrieve index element of the current array position
		$first_throttle_limit = key($throttle_settings);

		//retrieve all recently failed login attempts
		try {
			//get all failed attempts

			//return Response::make('before query');
			$all_failures = DB::select('select * from failed_logins where attempted > DATE_SUB(NOW(), INTERVAL ? MINUTE)', array(self::$time_frame_minutes));

			//reverse settings order for iteration
			krsort($throttle_settings);

			$recent_failure_count = count($all_failures);

			if($recent_failure_count >= $first_throttle_limit){
				//it's apparent that the amount of login attempts are an issue, time to react
				echo "<br/>Failure count: ".$recent_failure_count."<br/>";
			}

			//$ip = self::getIpAddress();
			//self::addFailedLoginAttempt(Auth::id(), Auth::user()->email, $ip);

			return var_dump($all_failures);
		} catch (Exception $e) {
			//return errors
			$response_array['status'] = 'error';
			$response_array['message'] = $e;
		}

		return $response_array;

	}

	/** 
	 * Retrieve the latest failed attempts from the failed logins table
	 *
	 * @return datetime $latest_failed_attempt || response $response_array
	 */
	protected static function retrieveLatestAttempts()
	{
		//attempt to retrieve latest failed attempt
		try {
			$results = DB::table('failed_logins')->select('attempted')->first();

			//convert to date timestamp
			$latest_failed_attempt = (int) date('U', strtotime($results->attempted));
			return $latest_failed_attempt;
		} catch (Exception $e) {
			self::$response_array['status'] = 'error';
			self::$response_array['message'] = $e;
			return $response_array;
		}
	}




}
