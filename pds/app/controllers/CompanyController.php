<?php

use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;
use CreatorServices\Companies\CreateCompany as CompanyCreator;
use RemovalServices\Companies\CompanyRemoval as CompanyRemoval;
use HandlerServices\Companies\CompanyReader as CompanyReader;
use HandlerServices\Companies\CompanyUpdator as CompanyUpdator;

class CompanyController extends \BaseController {

        public function __construct(GroupPermissions $groups){
            parent::__construct();
            $this->groups = $groups;
            $css = [ 'main', 'dash', 'forum'];
            $this->pharmacy_name = $this->toAsciiUrl($this->global_user_info->pharmacy);
            View::share('css', $css);
        }
    
        public function toAsciiUrl($str) {
            $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
            $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
            $clean = strtolower(trim($clean, '-'));
            $clean = preg_replace("/[\/_| -]+/", '-', $clean);
            return $clean;
        }
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($userId)
	{
            $company_list = $this->getUserCompanyIdList();
            $pharmacy_data = $this->getPharmacyData($userId);
            if($pharmacy_data['companies'] == null) return Redirect::route('dash.main')->with('Failure' , 'It appears you don\'t belong to any companies.  Please contact support if this persists.');
            return View::make('Projects.pharmacy', compact('pharmacy_data'))
                    ->nest('pharmacy_profile', 'Projects.pharmacy-details', compact('pharmacy_data'));
	}

        public function getUserCompanyIdList(){
            $user = $this->global_user_info;
            $companies = json_decode($user->companies);
            if($companies != '{}' || $companies != 0) {
                $company_list = array();
                foreach($companies as $company_id => $employee_type) {
                    $company_list[] = $company_id;
                }
                return $company_list;
            }
            return null;
        }
        
        public function getPharmacyData($userId) {
            $reader = new CompanyReader($this);
            $pharmacy_data['companies'] = Company::getAllCompanies($userId);
            $pharmacy_data['approvals'] = $reader->getCompanyApprovals($pharmacy_data['companies']);
            $pharmacy_data['employees'] = $reader->getCompanyMembers($pharmacy_data['companies']);
            $employees = $reader->getEmployeeData($pharmacy_data['companies']);
            $pharmacy_data['employee_data'] = $employees === 'no employees' ? NULL : $employees;
            $pharmacy_data['topic_count'] = $reader->getCompanyTotalTopicsReleased($pharmacy_data['companies']);
            $pharmacy_data['reply_count'] = $reader->getCompanyTotalTopicReplies($pharmacy_data['companies']);
            return $pharmacy_data;
        }
        
        
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            //
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $creator = new CompanyCreator($this);
            return $creator->createCompany(Input::except('_token'));
	}

        public function createCompanyFailedValidation($errors) {
            return Redirect::route('dash.main')
                    ->withInput()
                    ->withErrors($errors)
                    ->with('Open Setup', 'Re-open setup modal.');
        }
        
        public function createCompanySuccess($email) {
            return Redirect::route('dash.main')
                    ->with('Success', 'Successfully submitted your Pharmacy. Make sure your team members use the email: '.$email. ' for the admin email when signing up.');
        }
        
        public function adminDoesNotExist($failed_email) {
            return Redirect::route('dash.main')
                    ->with('False Admin', 'Admin with email: '.$failed_email.' does not exist. Please consult your owner for his correct email on this account.')
                    ->with('Open Setup', 'Re-open setup modal.')
                    ->withInput();
        }
        
        public function companyIsVendor($current_user) {
            $current_user->companies = json_encode('{}', true);
            $current_user->save();
            return Redirect::route('dash.main')->with('Success', 'Successfully submitted your vendor company registration request! You should hear from a PDS team member within 24 hours by phone or email.');
        }
        
        public function setVendor($id) {
            $user = User::find($id);
            $user->account_type = 2;
            $user->companies = json_encode('{}', true);
            $user->save();
            return Redirect::route('dash.main')->with('Success', 'You will be asked to setup your company at a later date.');
        }
        
        public function addUserForApproval($companyId) {
            /* What to do here now */
            $creator = new CompanyCreator($this);
            return $creator->addCompanyToApprovalList($companyId);
        }
        
        public function successfullyAddedToApprovalList(){
            return Redirect::route('dash.main')->with('Success', 'Your request has been submitted for approval! Please notify your management team to approve your account in myPharmacy.');
        }
        
        public function failedToAssociateCompanyWithRequestingUser() {
            return Redirect::route('dash.main')->with('Failure' , 'Could not save your information or submit your approval. Refresh and try again. If the problem persists, please contact the web administrator.');
        }
        
        public function failedToAddToApprovalList(){
            return Redirect::route('dash.main')
                    ->with('Failure', 'Could not store your approval form for submission. Please check your input and try again!')
                    ->with('Open Setup', 'Re-open setup modal.')
                    ->withInput();
        }
        
        
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}
        
        /**
         *  Display list of related resources
         * @param $search_input
         * @return Response
         */
        public function getRelatedCompanies(){
            $data = [
                'name' => Input::get('name'),
                'address' => Input::get('address'),
                'phone' => Input::get('phone'),
                'owner_email' => Input::get('owner_email')
            ];
            $company = new Company();
            return $results = json_encode($company->searchCompanies($data));
            /*$json_response = '{"1":{"company":"Pharmacy Ajax", "owner_name":"Derek J. Foster", "address":"1611 Red Cedar Dr. apt #11"},"2":{"company":"Pharmacy Ajax 2", "owner_name":"James Foster", "address":"1611 Red Cedar Dr. apt #11"}}';
            return $json_response; */
        }

        
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}
        
        public function updateEmployeeToAdmin($employeeId, $companyId){
            $updator = new CompanyUpdator($this);
            return $updator->updateEmployeeToAdmin($employeeId, $companyId);
        }
        
        public function failedToUpdateEmployeeToAdmin() {
            return Redirect::route('pharmacy.profile.index', ['pharmacy_name' => $this->pharmacy_name])->with('Failure', 'Could not swap admin roles with the selected user.');
        }
        
        public function successfullySwappedAdminRoles() {
            return Redirect::route('pharmacy.profile.index')->with('Success', 'Successfully swapped admin roles.');
        }
        
        /**
         * update approval status of company employee. Basically, change 'pending' to their employee type
         * 
         * @param int $id
         * @param int $company_id
         * @return Reponse
         */
        public function updateEmployeeApprovalStatus($id, $company_id, $employee_type) {
            $updator = new CompanyUpdator($this);
            return $updator->updateEmployeeApprovalStatus($id, $company_id, $employee_type);
        }
        
        public function successfullyApprovedEmployee($pharmacy_name, $userId, $companyId, $employeeType) {
            $name = strtolower(preg_replace('/ /', '-', $pharmacy_name));
            if($this->addMemberToCompany($userId, $companyId, $employeeType)) {
                $approvalRecord = CompanyApprovals::where('company_id', $companyId)->where('requesting_user', $userId)->first();
                $permissions = $this->refreshUserPermissions($userId);
                if($approvalRecord != null) {
                    $approvalRecord->delete();
                }
                return Redirect::route('pharmacy.profile.index', ['userId' => Auth::id()])->with('Success', 'You successfully updated the users permissions.');
            }
            return Redirect::back()->withErrors(['error', 'Something happened while trying to add the member to your company. Please click on the support button at the bottom right to contact support.']);
            
        }
        
        public function refreshUserPermissions($id, $downgrade = false) {
            if($downgrade) {
                return $this->groups->grantAccessGroupPermissions(2, $id);
            } else {
                return $this->groups->updateUserPermissions($id);
            }
            
            
        }

        public function addMemberToCompany($userId, $companyId, $employeeType) {
            
            if(Company::updateMember($userId, $companyId, $employeeType)) {
                return true;
            }
            return false;
        }
        
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
            //
	}
        
        /**
         * Reset user companies field to default, this means they will have to revisit pharmacy registration on their dashboard
         * 
         * @param int $id
         * @return target
         */
        public function resetUserCompanies($id){
            $remover = new CompanyRemoval($this);
            return $remover->resetUserCompany($id);
        }
        
        public function successfulUserCompanyReset() {
            return Redirect::route('pharmacy.profile.index')->with('Success', 'Successfully reset user\'s company association. They will now have to go through company setup again.');
        }


}
