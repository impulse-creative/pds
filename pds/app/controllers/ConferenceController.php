<?php

use UserRepo\Storage\User\UserRepository as UserRepoInterface;
use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;

class ConferenceController extends \BaseController {

    public function __construct(UserRepoInterface $users, GroupPermissions $user_group) {
        $this->user_group = $user_group;
        $this->users = $users;
        $css = [ 'conference'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Conference | PDSadvantage');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //check if registered vendor, if so, then use their current profile to bind the model data to the form
        $vendor = new Vendor();
        $id = Auth::id();
        $pdsVendor = $vendor->getVendorById($id);
        //check to see if user already has created a listing, if so, give default data as user data
        $user = count($pdsVendor) > 0 ? $pdsVendor : $this->users->find($id);
        $vendorDetails = $this->getVendorDetails($pdsVendor);
        if ($user)
            return View::make('Signup.2015-Conference.index', compact('user', 'vendorDetails'));
        //if this point is reached, we have a problem
        return Redirect::back()->with('Registration Access Failed', 'Could not get enough data to access the Conference Registration. Please contact the administrator.');
    }

    public function getVendorDetails($pdsVendor) {
        $vendor['chosenCategories'] = isset($pdsVendor->category) ? $pdsVendor->category : 0;
        $vendor['chosenSubCategories'] = isset($pdsVendor->subCategory) ? $pdsVendor->subCategory : null;
        $vendor['allCategories'] = MarketCategories::getCategoryArray();
        $vendor['allSubCategories'] = $vendor['chosenCategories'] === 0 ?
                MarketSubCategories::getSubCategoryArray() :
                MarketSubCategories::getSubCategoryArray($vendor['chosenCategories']);
        $vendor['completed'] = count($pdsVendor) > 0 ? 1 : 0;
        return $vendor;
    }

    public function indexFaq() {
        $menu_data = $this->users->getMenu();
        $user = User::find(Auth::id());
        return View::make('Signup.2015-Conference.progress-partials.faq', compact('user', 'menu_data'));
    }

    public function indexFaqSheet() {
        $menu_data = $this->users->getMenu();
        $user = $this->users->find(Auth::id());
        $off_menu = 1;
        return View::make('Support.index', compact('menu_data', 'user', 'off_menu'));
    }

    public function indexConferenceHours() {
        $menu_data = $this->users->getMenu();
        $user = User::find(Auth::id());
        return View::make('Signup.2015-Conference.progress-partials.conference-hours', compact('user', 'menu_data'));
    }

    public function indexConferenceHotel() {
        $menu_data = $this->users->getMenu();
        $user = User::find(Auth::id());
        return View::make('Signup.2015-Conference.progress-partials.conference-hotel', compact('user', 'menu_data'));
    }

    //attempt to retrieve all subcategories for 
    public function getVendorSubCategories($id) {
        $options = VendorSubCategories::where('id', 'LIKE', '%' . $id . '.%')->get();
        $something = '';
        foreach ($options as $key => $value) {
            $something .= '<option value="' . $value->id . '">' . $value->name . '</option>';
        }
        return $something;
    }

    public function indexGuideEntry() {
        //check if registered vendor, if so, then use their current profile to bind the model data to the form
        $pds_vendor_user = DB::table('pds_vendors')->where('user_id', '=', Auth::id())->first();
        $user = $this->users->find(Auth::id());

        //check to see if user already has created a listing, if so, swap out the data
        if (!is_null($pds_vendor_user))
            $user = $pds_vendor_user;

        $guide_entries = DB::table('guide_entry')->where('user_id', '=', Auth::id())->first();

        if (is_object($guide_entries)) {
            $completed = 1;
            return View::make('Signup.2015-Conference.progress-partials.hundred-word-profile', compact('user', 'guide_entries', 'completed'));
        } else {
            $completed = 0;
            return View::make('Signup.2015-Conference.progress-partials.hundred-word-profile', compact('user', 'completed'));
        }
    }

    public function indexGuideAd() {
        $user = DB::table('pds_vendors')->where('user_id', '=', Auth::id())->first();
        $guide_choices = DB::table('guide_ads')->where('user_id', '=', Auth::id())->first();
        $completed = is_object($guide_choices) ? 1 : 0;
        return View::make('Signup.2015-Conference.progress-partials.conference-guide', compact('user', 'guide_choices', 'completed'));
    }

    public function indexRaffle() {
        $pds_user = DB::table('pds_vendors')->where('user_id', '=', Auth::id())->first();
        $user = $pds_user;

        $raffle_user = RaffleItems::where('user_id', '=', Auth::id())->first();
        $completed = is_object($raffle_user) ? 1 : 0;
        if (isset($raffle_user))
            $user = $raffle_user;

        return View::make('Signup.2015-Conference.progress-partials.vendor-raffle', compact('user', 'completed'));
    }

    public function indexVipTickets() {
        $user_get = VipTicketRecipient::where('user_id', '=', Auth::id())->get();
        if (is_object($user_get)) {
            $incr = 1;
            $user = array();
            foreach ($user_get as $key => $value) {
                $user['vip_' . $incr . '_fname'] = $value->vip_fname;
                $user['vip_' . $incr . '_lname'] = $value->vip_lname;
                $user['vip_' . $incr . '_email'] = $value->vip_email;
                $user['vip_' . $incr . '_company'] = $value->vip_company_name;
                $user['vip_' . $incr . '_phone'] = $value->vip_phone;
                $incr++;
            }
        }

        $agreement_check = DB::table('vip_recipients')->where('user_id', '=', Auth::id())->first();

        $term_agreement = isset($agreement_check) ? $agreement_check->term_agreement : 0;
        $completed = is_object($agreement_check) ? 1 : 0;
        return View::make('Signup.2015-Conference.progress-partials.vip-tickets', compact('user', 'term_agreement', 'completed'));
    }

    public function indexSponsorshipOpportunities() {
        $chosen_sponserships = Sponsorships::where('user_id', '=', Auth::id())->first();

        //retrieve all sponosorship types
        $sponsorships = SponsorshipTypes::all();

        //check if user has not chosen any yet
        if (is_null($chosen_sponserships)) {
            $sponsor_choices = array('0');
            $bill_type = '0';
            $completed = 0;
        } else {
            $completed = 1;
            $sponsor_choices = explode(":", $chosen_sponserships->sponsorship_types);
            $bill_type = $chosen_sponserships->billing_choice;
        }

        return View::make('Signup.2015-Conference.progress-partials.sponsors', compact('sponsor_choices', 'bill_type', 'sponsorships', 'completed'));
    }

    public function editListingApproval() {
        
    }

    /**
     * Create new marketplace listing from the conference vendor sign up 
     * 
     * @return Redirect or View
     */
    public function createNewListing() {
        self::global_xss_clean();

        //set validation rules
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:pds_vendors',
            'phone' => 'required',
            'company_name' => 'required',
            'category' => 'required|NotIn:0',
            'subCategory' => 'required|NotIn:0',
            'agree' => 'required'
        );

        $approved = Input::get('approve_previous');
        if (isset($approved)) {
            $vendor = $this->users->find(Auth::id());
            $vendor->prev_marketplace_approved = 1;
            if ($vendor->save()) {
                return Redirect::route('conference.show.guide-entry');
            } else {
                return Redirect::back()->with('Failure', 'Failed to save approval. Please reload the page and try again.');
            }
        }


        //determine if updating or adding new for this account

        $check_existance = DB::table('pds_vendors')->where('user_id', '=', Auth::id())->first();
        if (isset($check_existance)) {
            $rules = array(
                'agree' => 'required'
            );
            $vendor = Vendor::find($check_existance->id);
        } else {
            $vendor = new Vendor;
        }

        //do the validation
        $validator = Validator::make(Input::all(), $rules);

        //check for validation failure
        if ($validator->fails()) {
            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $agreed_to_terms = Input::get('agree');
            $agreed_to_video_terms = Input::get('agreeVid');
            $description = Input::get('description');


            //get hubspot company/acc id
            $user_hs_num = $this->users->find_hs_acc_num(Auth::id());

            $vendor->user_id = Auth::id();
            //$vendor->hs_acc_number = User::getUserHSAccountId(Input::get('email'));
            $vendor->hs_acc_number = $user_hs_num;
            $vendor->first_name = Input::get('first_name');
            $vendor->last_name = Input::get('last_name');
            $vendor->email = Input::get('email');
            $vendor->phone = Input::get('phone');
            $vendor->company_name = Input::get('company_name');
            if (!preg_match("~^(?:f|ht)tps?://~i", Input::get('company_website'))) {
                $url = "http://" . Input::get('company_website');
            } else {
                $url = Input::get('company_website');
            }
            $vendor->company_website = $url;
            $vendor->address = Input::get('address');
            if (Input::get('city'))
                $vendor->city = Input::get('city');
            if (Input::get('state_or_region'))
                $vendor->state_or_region = Input::get('state_or_region');
            if (Input::get('postal_code'))
                $vendor->postal_code = Input::get('postal_code');
            if (Input::file('company_logo')) {
                $local_path = self::storeCompanyLogo();
                $vendor->company_logo = $local_path;
            } else {
                //do nothing
            }
            $vendor->category = Input::get('category');
            $vendor->subCategory = Input::get('subCategory');
            if (Input::get('description'))
                $vendor->description = isset($description) ? $_POST['html_edits'] : 'No description';
            if (Input::get('company_blog_or_vlog'))
                $vendor->company_blog_or_vlog = Input::get('company_blog_or_vlog');
            if (isset($agreed_to_terms))
                $vendor->term_agreement = is_array(Input::get('agree')) ? 1 : 0;
            //============
            //delete this from here once you've removed it from migrations
            if (isset($agreed_to_video_terms))
                $vendor->video_permission = is_array($agreed_to_video_terms) ? 1 : 0;
            //============
            if ($vendor->save()) {
                $apiArray = array(
                    'properties' => [
                        array(
                            'name' => 'x2015_vendor',
                            'value' => true
                        ),
                        array(
                            'name' => 'contact_first_name',
                            'value' => ucwords($vendor->first_name)
                        ),
                        array(
                            'name' => 'contact_last_name',
                            'value' => ucwords($vendor->last_name)
                        ),
                        array(
                            'name' => 'contact_email',
                            'value' => ucwords($vendor->email)
                        ),
                        array(
                            'name' => 'contact_phone',
                            'value' => ucwords($vendor->phone)
                        ),
                        array(
                            'name' => 'company_website',
                            'value' => ucwords($vendor->company_website)
                        ),
                        array(
                            'name' => 'company_address',
                            'value' => ucwords($vendor->address)
                        ),
                        array(
                            'name' => 'company_city',
                            'value' => ucwords($vendor->city)
                        ),
                        array(
                            'name' => 'company_state',
                            'value' => ucwords($vendor->state_or_region)
                        ),
                        array(
                            'name' => 'company_postal_code',
                            'value' => ucwords($vendor->postal_code)
                        ),
                        array(
                            'name' => 'company_logo',
                            'value' => URL::to('/storage/files/company_logos/') . $vendor->company_logo
                        ),
                        array(
                            'name' => 'category',
                            'value' => ucwords($vendor->category)
                        ),
                        array(
                            'name' => 'sub_category',
                            'value' => ucwords($vendor->subCategory)
                        ),
                        array(
                            'name' => 'description',
                            'value' => ucwords($vendor->description)
                        ),
                        array(
                            'name' => 'company_blog',
                            'value' => ucwords($vendor->company_blog_or_vlog)
                        ),
                        array(
                            'name' => 'compay_name',
                            'value' => ucwords($vendor->company_name)
                        )
                    ]
                );
                $company = HubSpot::Companies();
                $updatedCompany = $company->update_company($vendor->hs_acc_number, $apiArray);
                return Redirect::route('conference.show.guide-entry');
            } else {
                throw new Exception('Could not save the new listing');
            }
        }
    }

    public function storeCompanyLogo() {
        //attempt to retrieve served file from client
        if ($_FILES['company_logo']["size"][0] !== 0) {
            //intialize variables
            $data = Input::file(); //array of files
            $numOfFiles = count($data['company_logo']); //number of files in array with index 'file'
            //remove previous logo associated with the company (this is in case of a logo update)
            foreach (glob(public_path() . "/storage/files/company_logos/" . Auth::id() . "_*.*") as $filename) {
                //deletes the previous files
                unlink($filename);
                //var_dump($filename);
            }

            //set the file name prefixed with the user id (good for deletes, moves, etc.)
            $name = Auth::id() . "_" . rand(0, 10000) . "_" . $data['company_logo']->getClientOriginalName();
            //find temp location when initially downloaded.
            $tmp_path = $data['company_logo']->getRealPath();
            //construct the store path of our server (this is where the file will be stored)
            $local_store_path = public_path() . '/storage/files/company_logos/' . $name;
            //store the file on the server 
            $local_path = self::storeCompanyLogoLocally($name, $data['company_logo'], $local_store_path);

            return $name;
        } else {
            //send user back if file is non-existant
            return Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    /**
     * FILE STORAGE METHODS
     * @author Derek Foster
     */
    //store the uploaded file locally
    public function storeCompanyLogoLocally($filename, $file, $local_path) {
        //move file from the tmp directory to '/storage/files', using public path as the root
        $file->move(public_path() . '/storage/files/company_logos', $filename);

        //return path the file was stored in
        return $local_path;
    }

    /**
     * HUBSPOT ACCOUNTS HANDLERS
     * Check to see if the user has a hubspot account
     */
    //checks to see if the user has a hubspot account
    protected function checkForHubspotAccount($email) {
        $contactConnect = HubSpot::contacts();
        $test = $contactConnect->get_contact_by_email($email);
        if (isset($test->properties) && is_object($test->properties))
            return $test->properties;
        return var_dump($test);
    }

    /**
     * Create a new Conference Guide entry
     * 
     * @return Response
     */
    public function createNewGuideEntry() {
        static::global_xss_clean();
        $agreed_to_terms = Input::get('agree');
        $description = Input::get('description');
        //set validation rules
        $rules = array(
            'sales_first_name' => 'required',
            'sales_last_name' => 'required',
            'sales_email' => 'required|email|unique:guide_entry',
            'company_name' => 'required',
            'agree' => 'required',
            'description' => 'required'
        );

        //determine if updating or adding new for this account
        $check_existance = DB::table('guide_entry')->where('user_id', '=', Auth::id())->first();
        if (isset($check_existance)) {
            $rules = array(
                'agree' => 'required'
            );
            $entry = GuideEntries::find($check_existance->id);
        } else {
            $entry = new GuideEntries;
        }

        //do the validation
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $user_id = DB::table('pds_vendors')->select('id')->where('user_id', '=', Auth::id())->first();

            if (!is_object($user_id)) {
                $user = $this->users->find(Auth::id());
            } else {
                $user = Vendor::find($user_id->id);
            }
            return Redirect::route('conference.create.guide-entry')->withErrors($validator)->with('user', $user)->withInput();
        } else {

            //get hubspot company/acc id
            $user_hs_num = User::select('hs_acc_number')->where('id', '=', Auth::id())->first();

            $entry->user_id = Auth::id();
            $entry->hs_acc_number = $user_hs_num->hs_acc_number;
            $entry->sales_first_name = Input::get('sales_first_name');
            $entry->sales_last_name = Input::get('sales_last_name');
            $entry->sales_email = Input::get('sales_email');
            $entry->company_name = Input::get('company_name');
            $entry->company_website = Input::get('company_website');
            $entry->address = Input::get('address');
            if (Input::get('city'))
                $entry->city = Input::get('city');
            if (Input::get('state_or_region'))
                $entry->state_or_region = Input::get('state_or_region');
            if (Input::get('postal_code'))
                $entry->postal_code = Input::get('postal_code');
            if (Input::file('company_guide_entry_img')) {
                $local_path = self::storeGuideLogo();
                $entry->company_guide_entry_img = $local_path;
            } else {
                //do nothing
            }
            //return var_dump($_POST['html_edits']);
            if (Input::get('description'))
                $entry->description = isset($description) ? $_POST['html_edits'] : 'No description';
            if (isset($agreed_to_terms))
                $entry->term_agreement = is_array(Input::get('agree')) ? 1 : 0;
            if ($entry->save()) {
                $apiArray = array(
                    'properties' => [
                        array(
                            'name' => 'ad_logo',
                            'value' => URL::to('/storage/files/guide_logos/') . $entry->company_logo
                        ),
                        array(
                            'name' => 'sales_contact',
                            'value' => ucwords($entry->sales_first_name) . ' ' . ucwords($entry->sales_last_name)
                        ),
                        array(
                            'name' => 'sales_email',
                            'value' => ucwords($entry->sales_email)
                        ),
                        array(
                            'name' => 'ad_description',
                            'value' => ucwords($entry->description)
                        )
                    ]
                );
                $company = HubSpot::Companies();
                $updatedCompany = $company->update_company($entry->hs_acc_number, $apiArray);

                return Redirect::route('conference.show.guide-ad');
            } else {
                throw new Exception('Could not save the company as a new guide entry.');
            }
        }
    }

    public function storeGuideLogo() {
        if ($_FILES['company_guide_entry_img']["size"][0] !== 0) {
            //intialize variables
            $data = Input::file(); //array of files
            $numOfFiles = count($data['company_guide_entry_img']); //number of files in array with index 'file'
            //remove previous logo associated with the company (this is in case of a logo update)
            foreach (glob(public_path() . "/storage/files/guide_logos/" . Auth::id() . "_*.*") as $filename) {
                //deletes the previous files
                unlink($filename);
                //var_dump($filename);
            }

            $name = Auth::id() . "_" . rand(0, 10000) . "_" . $data['company_guide_entry_img']->getClientOriginalName();
            $local_store_path = public_path() . '/storage/files/guide_logos' . $name;
            $local_path = self::storeGuideLogoLocally($name, $data['company_guide_entry_img'], $local_store_path);

            return $name;
        } else {
            return Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    /**
     * FILE STORAGE METHODS
     * @author Derek Foster
     */
    //store the uploaded file locally
    public function storeGuideLogoLocally($filename, $file, $local_path) {
        //move file from the tmp directory to '/storage/files', using public path as the root
        $file->move(public_path() . '/storage/files/guide_logos', $filename);

        return $local_path;
    }

    public function createNewGuideAd() {
        //clean the input
        //static::global_xss_clean();
        //rules for validation
        $rules = array(
            'ad_type' => 'required',
            'bill_info' => 'required',
            'company_ad' => 'required',
            'agree' => 'required'
        );
        //determine if updating or adding new for this account
        $check_existance = DB::table('guide_ads')->where('user_id', '=', Auth::id())->first();
        if (count($check_existance) == 1) {
            $rules = array(
                'agree' => 'required'
            );
            $guide_ad = GuideAds::find($check_existance->id);
        } else {
            $guide_ad = new GuideAds;
        }

        //create validator for new guide ad entry
        $validator = Validator::make(Input::all(), $rules);

        //Check for validation failure, if failure, return to view with errors
        if ($validator->fails()) {
            $messages = $validator->messages();
            $user_id = DB::table('pds_vendors')->select('id')->where('user_id', '=', Auth::id())->first();
            //return var_dump($user_id);
            $user = Vendor::findOrFail($user_id->id);
            $guide_choices = DB::table('guide_ads')->where('user_id', '=', Auth::id())->first();
            return Redirect::route('conference.show.guide-ad')->withErrors($validator)->with('user', $user)->with('guide_choices', $guide_choices);
        } else {
            //if successful, insert new entry into guide ad table
            $guide_ad->user_id = Auth::id();
            if (Input::file('company_ad')) {
                $local_path = self::storeGuideAd();
                $guide_ad->company_ad = $local_path;
            }
            //get hubspot company/acc id
            $user_hs_num = User::select('hs_acc_number')->where('id', '=', Auth::id())->first();
            $guide_ad->hs_acc_number = $user_hs_num->hs_acc_number;
            $ad_type_entry = Input::get('ad_type');
            if (is_null($ad_type_entry)) {
                $guide_choices = DB::table('guide_ads')->where('user_id', '=', Auth::id())->first();
                return Redirect::back()->with('guide_choices', $guide_choices)->with('no_ad_types', 'Please choose an ad type.');
            } else {
                $ad_type_list = implode(",", Input::get('ad_type'));
                $guide_ad->ad_types = $ad_type_list;
            }
            $guide_ad->billing_info = Input::get('bill_info');
            $agreed_to_terms = Input::get('agree');
            if (isset($agreed_to_terms))
                $guide_ad->term_agreement = is_array(Input::get('agree')) ? 1 : 0;
            if ($guide_ad->save()) {
                $apiArray = array(
                    'properties' => [
                        array(
                            'name' => 'ad_type',
                            'value' => $guide_ad->ad_types
                        ),
                        array(
                            'name' => 'billing_info',
                            'value' => $guide_ad->billing_info
                        ),
                        array(
                            'name' => 'company_ad',
                            'value' => URL::to('/storage/files/guide_ads/') . $guide_ad->company_ad
                        )
                    ]
                );
                $company = HubSpot::Companies();
                $updatedCompany = $company->update_company($guide_ad->hs_acc_number, $apiArray);

                return Redirect::route('conference.show.raffle');
            } else {
                throw new Exception('Could not save the ad spot. Please contact the Web Administrator');
            }
        }
    }

    public function storeGuideAd() {
        if ($_FILES['company_ad']["size"][0] !== 0) {
            //intialize variables
            $data = Input::file(); //array of files
            $numOfFiles = count($data['company_ad']); //number of files in array with index 'file'
            //remove previous logo associated with the company (this is in case of a logo update)
            foreach (glob(public_path() . "/storage/files/guide_ads/" . Auth::id() . "_*.*") as $filename) {
                //deletes the previous files
                unlink($filename);
                //var_dump($filename);
            }

            $name = Auth::id() . "_" . rand(0, 10000) . "_" . $data['company_ad']->getClientOriginalName();
            self::storeGuideAdLocally($name, $data['company_ad']);

            return $name;
        } else {
            return Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    /**
     * FILE STORAGE METHODS
     * @author Derek Foster
     */
    //store the uploaded file locally
    public function storeGuideAdLocally($filename, $file) {
        //move file from the tmp directory to '/storage/files', using public path as the root
        $file->move(public_path() . '/storage/files/guide_ads', $filename);
    }

    public function createNewGiveawayItem() {
        //clean the input
        static::global_xss_clean();

        $rules = array(
            'item_name' => 'required'
        );

        //determine if updating or adding new for this account
        $check_existance = RaffleItems::where('user_id', '=', Auth::id())->first();
        if (!is_null($check_existance)) {
            $rules = array(
                'item_name' => 'required'
            );
            $raffle_item = RaffleItems::find($check_existance->id);
        } else {
            $raffle_item = new RaffleItems;
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $user_id = RaffleItems::where('user_id', '=', Auth::id())->first();

            $user = RaffleItems::findOrFail($user_id->id);
            return Redirect::route('conference.show.raffle')->withErrors($validator)->with('user', $user)->withInput();
        } else {
            $raffle_item->user_id = Auth::id();
            //get hubspot company/acc id
            $user_hs_num = User::select('hs_acc_number')->where('id', '=', Auth::id())->first();
            $raffle_item->hs_acc_number = $user_hs_num->hs_acc_number;
            $raffle_item->item_name = Input::get('item_name');
            if ($raffle_item->save()) {
                $apiArray = array(
                    'properties' => [
                        array(
                            'name' => 'giveaway_item',
                            'value' => $raffle_item->item_name
                        )
                    ]
                );
                $company = HubSpot::Companies();
                $updatedCompany = $company->update_company($raffle_item->hs_acc_number, $apiArray);
                return Redirect::route('conference.vip-tickets');
            } else {
                throw new Exception('Could not save the raffle item. Please contact the Web Administrator');
            }
        }
    }

    public function addVipTicketApi($companyId, $apiArray) {
        $company = HubSpot::Companies();
        $updatedCompany = $company->update_company($companyId, $apiArray);
    }

    public function createNewVipTicketEntries() {
        static::global_xss_clean();

        $rules = array(
            'vip_1_fname' => 'required',
            'vip_1_lname' => 'required',
            'vip_1_company' => 'required',
            'vip_1_email' => 'required',
            'vip_1_phone' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $agreement_check = DB::table('vip_recipients')->where('user_id', '=', Auth::id())->first();

            $term_value = isset($agreement_check) ? $agreement_check->term_agreement : 0;
            return Redirect::route('conference.vip-tickets')->withInput()->withErrors($messages)->with('term_agreement', $term_value);
        } else {
            $successful_query = false;
            //determine if updating or adding new for this account
            $check_existance = VipTicketRecipient::where('user_id', '=', Auth::id())->first();

            $now = new DateTime();
            if (is_null($check_existance)) {
                //get hubspot company/acc id
                $user_hs_num = User::select('hs_acc_number')->where('id', '=', Auth::id())->first();
                $one_visited = 0;
                $two_visited = 0;
                $three_visited = 0;
                $recipient_list = Input::all();
                $companyId = $user_hs_num->hs_acc_number;
                foreach ($recipient_list as $key => $value) {
                    if (preg_match("/^vip_1/", $key)) {
                        if ($one_visited == 1) {
                            continue;
                        } else {
                            $one_visited = 1;
                        }
                        $insert_recipient_sql = array('vip_phone' => Input::get('vip_1_phone'), 'created_at' => $now, 'updated_at' => $now, 'hs_acc_number' => $user_hs_num->hs_acc_number, 'user_id' => Auth::id(), 'vip_fname' => Input::get('vip_1_fname'), 'vip_lname' => Input::get('vip_1_lname'), 'vip_company_name' => Input::get('vip_1_company'), 'vip_email' => Input::get('vip_1_email'));
                        $apiArray1 = array(
                            'properties' => [
                                array(
                                    'name' => 'vip_1_email',
                                    'value' => Input::get('vip_1_email')
                                ),
                                array(
                                    'name' => 'vip_1_name',
                                    'value' => ucwords(Input::get('vip_1_fname')) . ' ' . ucwords(Input::get('vip_1_lname'))
                                ),
                                array(
                                    'name' => 'vip_1_company',
                                    'value' => Input::get('vip_1_company')
                                )
                            ]
                        );
                        self::addVipTicketApi($companyId, $apiArray1);
                        $successful_query = true;
                    } elseif (preg_match("/^vip_2/", $key)) {
                        if ($two_visited == 1) {
                            continue;
                        } else {
                            //if (Input::get($key) == '') continue;
                            $two_visited = 1;
                        }
                        $insert_recipient_sql = array('vip_phone' => Input::get('vip_2_phone'), 'created_at' => $now, 'updated_at' => $now, 'hs_acc_number' => $user_hs_num->hs_acc_number, 'user_id' => Auth::id(), 'vip_fname' => Input::get('vip_2_fname'), 'vip_lname' => Input::get('vip_2_lname'), 'vip_company_name' => Input::get('vip_2_company'), 'vip_email' => Input::get('vip_2_email'));
                        $apiArray2 = array(
                            'properties' => [
                                array(
                                    'name' => 'vip_2_email',
                                    'value' => Input::get('vip_2_email')
                                ),
                                array(
                                    'name' => 'vip_2_name',
                                    'value' => ucwords(Input::get('vip_2_fname')) . ' ' . ucwords(Input::get('vip_2_lname'))
                                ),
                                array(
                                    'name' => 'vip_2_company',
                                    'value' => Input::get('vip_2_company')
                                )
                            ]
                        );
                        self::addVipTicketApi($companyId, $apiArray2);
                        $successful_query = true;
                    } elseif (preg_match("/^vip_3/", $key)) {
                        if ($three_visited == 1) {
                            continue;
                        } else {
                            //if (Input::get($key) == '') continue;
                            $three_visited = 1;
                        }
                        $insert_recipient_sql = array('vip_phone' => Input::get('vip_3_phone'), 'created_at' => $now, 'updated_at' => $now, 'hs_acc_number' => $user_hs_num->hs_acc_number, 'user_id' => Auth::id(), 'vip_fname' => Input::get('vip_3_fname'), 'vip_lname' => Input::get('vip_3_lname'), 'vip_company_name' => Input::get('vip_3_company'), 'vip_email' => Input::get('vip_3_email'));
                        $apiArray3 = array(
                            'properties' => [
                                array(
                                    'name' => 'vip_3_email',
                                    'value' => Input::get('vip_3_email')
                                ),
                                array(
                                    'name' => 'vip_3_name',
                                    'value' => ucwords(Input::get('vip_3_fname')) . ' ' . ucwords(Input::get('vip_3_lname'))
                                ),
                                array(
                                    'name' => 'vip_3_company',
                                    'value' => Input::get('vip_3_company')
                                )
                            ]
                        );
                        self::addVipTicketApi($companyId, $apiArray3);
                        $successful_query = true;
                    } else {
                        continue;
                    }
                    $insert_recipients = DB::table('vip_recipients')->insert(array(
                        $insert_recipient_sql
                    ));
                }
                //check if query failed or not
                if ($successful_query == true) {
                    return Redirect::route('conference.choose.sponserships');
                } else {
                    return Redirect::route('conference.vip-tickets')->with('Insert Failure', 'Failed to insert the recipients, please check your entries and try again. If the problem persists, contact your Web Administrator.')->withInput();
                }
            } else {
                //get hubspot company/acc id
                $user_hs_num = User::select('hs_acc_number')->where('id', '=', Auth::id())->first();
                $update_recipients = DB::table('vip_recipients')->where('user_id', '=', Auth::id())->get();
                $incr = 1;
                foreach ($update_recipients as $key => $value) {
                    if (Input::get('vip_' . $incr . '_email') == '' || Input::get('vip_' . $incr . '_lname') == '' || Input::get('vip_' . $incr . '_fname') == '' || Input::get('vip_' . $incr . '_company') == '') {
                        $incr++;
                        continue;
                    }

                    $update_recipient = VipTicketRecipient::find($value->id);
                    $update_recipient->hs_acc_number = $user_hs_num->hs_acc_number;
                    $update_recipient->vip_fname = Input::get('vip_' . $incr . '_fname');
                    $update_recipient->vip_lname = Input::get('vip_' . $incr . '_lname');
                    $update_recipient->vip_company_name = Input::get('vip_' . $incr . '_company');
                    $update_recipient->vip_email = Input::get('vip_' . $incr . '_email');
                    $update_recipient->vip_phone = Input::get('vip_' . $incr . '_phone');
                    if ($update_recipient->save()) {
                        ${'apiArray' . $incr} = array(
                            'properties' => [
                                array(
                                    'name' => 'vip_' . $incr . '_email',
                                    'value' => Input::get('vip_' . $incr . '_email')
                                ),
                                array(
                                    'name' => 'vip_' . $incr . '_name',
                                    'value' => ucwords(Input::get('vip_' . $incr . '_fname')) . ' ' . ucwords(Input::get('vip_' . $incr . '_lname'))
                                ),
                                array(
                                    'name' => 'vip_' . $incr . '_company',
                                    'value' => Input::get('vip_' . $incr . '_company')
                                )
                            ]
                        );
                        self::addVipTicketApi($update_recipient->hs_acc_number, ${'apiArray' . $incr});
                        $success = 1;
                    } else {
                        $success = 0;
                    }
                    $incr++;
                }
                //check if query failed or not
                if ($update_recipients) {
                    return Redirect::route('conference.choose.sponserships');
                } else {
                    if ($success == 0)
                        return Redirect::back()->withInput()->with('Update Failure', 'Failed to update record with new value set: <strong>' . Input::get('vip_' . $incr . '_fname') . ',' . Input::get('vip_' . $incr . '_lname') . ', and ' . Input::get('vip_' . $incr . '_email'));
                    return Redirect::route('conferece.vip-tickets')->with('Insert Failure', 'Failed to insert the recipients, please check your entries and try again. If the problem persists, contact your Web Administrator.');
                }
            }
        }
    }

    public function createNewSponsorshipChoices() {
        static::global_xss_clean();
        $rules = array(
            'bill_info' => 'required',
            'opportunities' => 'required'
        );

        //get hubspot company/acc id
        $user_hs_num = User::select('hs_acc_number')->where('id', '=', Auth::id())->first();
        //determine if updating or adding new for this account
        $check_existance = DB::table('sponsorships')->where('user_id', '=', Auth::id())->first();
        if (count($check_existance) == 1) {
            $sponsorships = Sponsorships::find($check_existance->id);
        } else {
            $sponsorships = new Sponsorships;
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::route('conference.choose.sponserships')->withInput()->withErrors($validator);
        } else {

            $sponsorship_list = implode(":", Input::get('opportunities'));

            $sponsorships->user_id = Auth::id();
            $sponsorships->hs_acc_number = $user_hs_num->hs_acc_number;
            $sponsorships->sponsorship_types = $sponsorship_list;
            $sponsorships->billing_choice = Input::get('bill_info');

            if ($sponsorships->save()) {
                //update sale limit of sponsorship item
                $sponsorships_to_update = Input::get('opportunities');
                foreach ($sponsorships_to_update as $key => $value) {
                    $type = SponsorshipTypes::find($value);
                    $type->sale_limit = $type->sale_limit != 0 ? $type->sale_limit - 1 : 0;
                    $type->save();
                }
                $apiArray = array(
                    'properties' => [
                        array(
                            'name' => 'sponsorships',
                            'value' => $sponsorships->sponsorship_type
                        ),
                        array(
                            'name' => 'sponsor_billing',
                            'value' => $sponsorships->billing_choice
                        )
                    ]
                );
                $company = HubSpot::Companies();
                $updatedCompany = $company->update_company($sponsorships->hs_acc_number, $apiArray);
                return Redirect::route('dash.main')->with('Successful Conference Signup', '<strong>Congrats!</strong> You are now registered for the conference. Please check your email for details regarding your next steps.');
            } else {
                return Redirect::back()->with('Registration Failure', 'Could not register your account. Check your information and try again!');
            }
        }
    }

    public function downloadGuideAd($filename) {
        $file = public_path() . '/storage/files/guide_ads/' . $filename;
        $headers = array(
            'Content-Type: application/octet-stream',
        );
        return Response::download($file, $filename, $headers);
        //return $filename;
    }

    public function downloadMarketplaceLogo($filename) {
        $file = public_path() . '/storage/files/company_logos/' . $filename;
        $headers = array(
            'Content-Type: application/octet-stream',
        );
        return Response::download($file, $filename, $headers);
        //return $filename;
    }

    public function downloadGuideLogo($filename) {
        $file = public_path() . '/storage/files/guide_logos/' . $filename;
        $headers = array(
            'Content-Type: application/octet-stream',
        );
        return Response::download($file, $filename, $headers);
        //return $filename;
    }

    /**
     * Clean all input received from the user's submitted form 
     *
     * @param Post data
     * @return none
     * @author Derek J. Foster
     */
    public static function global_xss_clean() {
        // Recursive cleaning for array [] inputs, not just strings.
        $sanitized = static::cleanInput(Input::all());
        Input::merge($sanitized);
    }

    public static function cleanInput($dirty_values) {
        $result = array();

        foreach ($dirty_values as $key => $value) {
            echo '<script>//console.log(' . $key . ');</script>';
            // Don't allow tags on key either, maybe useful for dynamic forms.
            $key = strip_tags($key);

            // If the value is an array, we will just recurse back into the
            // function to keep stripping the tags out of the array,
            // otherwise we will set the stripped value.
            if (is_array($value)) {
                $result[$key] = static::cleanInput($value);
            } else {
                // I am using strip_tags(), you may use htmlentities(),
                // also I am doing trim() here, you may remove it, if you wish.
                $result[$key] = trim(strip_tags($value));
            }
        }

        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
