<?php

use UserRepo\Storage\User\UserRepository as UserRepoInterface;
use GroupRepo\Permissions\Groups\GroupRepository as GroupInterface;
use EventsRepo\Storage\Events\EventsRepository as EventsInterface;

class EventsController extends BaseController {

    /**
     * Construct controller global dependencies
     *
     * @param service providers
     */
    public function __construct(UserRepoInterface $users, GroupInterface $groups, EventsInterface $events) {
        parent::__construct();
        $this->users = $users;
        $this->permissions = $groups;
        $this->events = $events;
        //logged in users data available anywhere in events, if they're logged in.
        if (Auth::check()) {
            $user = self::getCurrentUserData();
            $group_list = explode(':', $user->groups);
            View::share('user', $user);
            View::share('user_access_list', $group_list);
        }
        $css = [ 'events', 'forum', 'main'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Events | PDSadvantage');
    }

    /**
     * Get index for events 
     */
    public function getIndex() {
        $this->removePastDueEvents();
        $months = self::getMonths();
        $event_types = $this->events->getEventTypesArray();
        //add default option to event types
        $event_types[0] = 'All Events';
        $events = $this->events->allWithDetailsPaginated();
        return View::make('Events.index', compact('events', 'event_types', 'months'));
    }

    /**
     * Get alternative calendar index
     */
    public function getCalendarIndex() {
        $this->removePastDueEvents();
        $months = self::getMonths();
        $event_types = $this->events->getEventTypesArray();
        //add default option to event types
        $event_types[0] = 'All Events';
        $events = $this->events->allWithDetails();
        return View::make('Events.partials.calendar', compact('events', 'event_types', 'months'));
    }

    /**
     * Get filtered index -- filtered by user search key 
     */
    public function getSearchedEventsIndex() {
        $this->removePastDueEvents();
        $months = self::getMonths();
        $event_types = $this->events->getEventTypesArray();
        //add default option to event types
        $event_types[0] = 'All Events';
        $events = $this->events->getSearchResults();
        return View::make('Events.index', compact('events', 'event_types', 'months'));
    }

    public function getMonths() {
        return ['January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0];
    }
    
    public function removePastDueEvents() {
        $events = $this->events->all();
        $current_date = \Carbon\Carbon::now();
        foreach($events as $event){
        $event_date = new \Carbon\Carbon($event->start_event);
            if(!is_null($event->start_event) && $event_date->addDay() < $current_date) {
                $deleteEvent = $this->events->removeEvent($event->id);
            }
        }
    }

    /**
     * Get index for single event view
     */
    public function getEventIndex($id) {
        $event = $this->events->find($id);
        $member_levels = self::getAccessLevelsForEventPrices();
        $event_form = $this->events->getHubspotEventForm($event->hs_form_id);
        $lowest = self::getLowestPrice($event);
        $similar_events = $this->events->getSimilarEventsBasedOffTitle($event->title, $event->id);
        //return var_dump($similar_events);
        $achievement = \UserAchievement::addAchievement(Auth::id(), 3);
        $units = $this->events->getUnitsForPricingArray();
        $pageTitle = $event->title.' | PDSadvantage';
        return View::make('Events.event', compact('event', 'member_levels', 'event_form', 'similar_events', 'units', 'lowest', 'pageTitle'));
    }

    public static function getLowestPrice($event) {
        $prices = [
            'core' => $event->core_price,
            'advanced' => $event->advanced_price,
            'elite' => $event->elite_price
        ];
        $price = $prices['core'];
        $level = 'core';
        foreach ($prices as $tempLevel => $tempPrice) {
            if ($tempPrice <= $price) {
                $price = $tempPrice;
                $level = $tempLevel;
            }
        }
        $lowest['price'] = $price;
        $lowest['level'] = $level;
        $lowest['string'] = 'As low as $' . $price . ' for ' . $level . ' members'; //lowest price response string
        return $lowest;
    }

    /**
     * Get index for filtered event view
     *
     * @param int $event_type
     */
    public function getFilteredEventIndex($id, $event) {
        $months = self::getMonths();
        if ($id == 0)
            return Redirect::route('events.index');
        $events = $this->events->getAllWithDetailsFiltered($id);
        $event_types = $this->events->getEventTypesArray();
        //add default option to event types
        $event_types[0] = 'All Events';

        $admin_access_levels = self::getAccessLevelsNeededForEventAdminTools();
        $is_mod = $this->permissions->checkMultiAccess($admin_access_levels);

        return View::make('Events.index', compact('events', 'event_types', 'is_mod', 'months'));
    }

    public function getAccessLevelsNeededForEventAdminTools() {
        $admin_access_levels = [
            $this->permissions->getEventModId(),
            $this->permissions->getEmployeeId(),
            $this->permissions->getImpulseId(),
            $this->permissions->getAdminId()
        ];

        return $admin_access_levels;
    }

    public function getAccessLevelsForEventPrices() {
        $access_levels = [
            $this->permissions->getCoreId(),
            $this->permissions->getEliteId(),
            $this->permissions->getAdvancedId()
        ];

        return $access_levels;
    }

    public function getPriceMessageToShow($user_restrictions) {
        
    }

    public function getCurrentUserData() {
        $user = $this->users->find(Auth::id());
        return $user;
    }

    /**
     * Get index for adding new events
     */
    public function getAddNewEventsView() {
        $event_types = $this->events->getEventTypesArray();
        //add default option to event types
        //get event states available
        $states = $this->events->getEventStatesArray();
        //add default option to states select
        $states[0] = '--Select Event State--';
        $units = $this->events->getUnitsForPricingArray();
        $update_events_editor_data = $this->getDescriptionEditorInfo();
        $how_it_works_editor_data = $this->getEditorInfoArray('editHowItWorks', 'editor2', 'add-new-event', 'how_it_works', NULL);
        $who_should_attend_editor_data = $this->getEditorInfoArray('editWhoAttends', 'editor3', 'add-new-event', 'attending', NULL);
        $pricing_details_editor_data = $this->getEditorInfoArray('editPricingDetails', 'editor4', 'add-new-event', 'price_description', NULL);
        return View::make('Events.add', compact('event_types', 'states', 'units'))
                ->nest('description_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $update_events_editor_data))
                ->nest('how_it_works_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $how_it_works_editor_data))
                ->nest('who_should_attend_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $who_should_attend_editor_data))
                ->nest('pricing_details_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $pricing_details_editor_data));
    }
    
    public function getDescriptionEditorInfo() {
        $data['editor_class'] = "newEventEditor";
        $data['form_class'] = "add-new-event";
        $data['editor_id'] = "editor1";
        $data['input_name'] = "event_description";
        $data['old_input'] = '';
        return $data;
    }
    public function getEditorInfoArray($editor_class, $editor_id, $form_class, $input_name, $old_input = NULL) {
        $data['editor_class'] = $editor_class;
        $data['form_class'] = $form_class;
        $data['input_name'] = $input_name;
        $data['editor_id'] = $editor_id;
        $data['old_input'] = is_null($old_input) ? '' : $old_input;
        return $data;
    }

    /**
     * Handle adding a new event
     */
    public function postAddNewEvent() {
        $validator = self::validateNewEventInput();
        //return var_dump($validator);
        if ($validator->fails()) {
            return Redirect::route('get.add.event')->withInput()->withErrors($validator)->with('Failure', 'Could not save the event. Errors present in input.');
        }
        $eventImage = '';
        if (Input::file('event_image')) {
            $filename = $this->events->storeEventPhoto(preg_replace('/[^A-Za-z0-9 _ .-]/', '', Input::get('title')), 'event_image');
            $eventImage = $filename;
        }

        return $this->events->createNewEvent($eventImage);
    }

    public function validateNewEventInput() {
        $params = Events::getRequiredFields();
        //return var_dump($params);
        $validator = Validator::make($params["input"], $params["rules"]);
        return $validator;
    }

    /**
     * Get view to update event information
     *
     * @param int $event_id
     */
    public function getUpdateIndex($event_id) {
        $event = $this->events->find($event_id);

        $event_types = $this->events->getEventTypesArray();
        //add default option to event types
        //get event states available
        $states = $this->events->getEventStatesArray();
        //add default option to states select
        $states[0] = '--Select Event State--';
        $units = $this->events->getUnitsForPricingArray();
        $update_events_editor_data = $this->getUpdateEditorInfoArray('editDescription', 'editor1', 'add-new-event', 'event_description', $event->description);
        $how_it_works_editor_data = $this->getUpdateEditorInfoArray('editHowItWorks', 'editor2', 'add-new-event', 'how_it_works', $event->how_does_it_work);
        $who_should_attend_editor_data = $this->getUpdateEditorInfoArray('editWhoAttends', 'editor3', 'add-new-event', 'attending', $event->who_should_attend);
        $pricing_details_editor_data = $this->getUpdateEditorInfoArray('editPricingDetails', 'editor4', 'add-new-event', 'price_description', $event->pricing_description);
        return View::make('Events.update', compact('event_types', 'states', 'units', 'event'))
                    ->nest('description_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $update_events_editor_data))
                    ->nest('how_it_works_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $how_it_works_editor_data))
                    ->nest('who_should_attend_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $who_should_attend_editor_data))
                    ->nest('pricing_details_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $pricing_details_editor_data));
    }
    
    public function getUpdateEditorInfoArray($editor_class, $editor_id, $form_class, $input_name, $old_input = NULL) {
        $data['editor_class'] = $editor_class;
        $data['form_class'] = $form_class;
        $data['input_name'] = $input_name;
        $data['editor_id'] = $editor_id;
        $data['old_input'] = is_null($old_input) ? '' : $old_input;
        return $data;
    }

    public function getEventsForCalendar() {
        $event_list = $this->events->allWithDetails();

        $events = array();
        foreach ($event_list as $key => $event) {
            $temp = array();
            $start_event = new \Carbon\Carbon($event->start_event);
            $date = $start_event->format("Y-m-d");
            $temp['date'] = $date;
            $temp['event_type'] = $event->event_type;
            $temp['title'] = $event->title;
            $clean_title = self::cleanTitleForUrl($event->title);
            $temp['url'] = URL::route('app.view.event', ['id' => $event->id, 'event' => $clean_title]);
            $location = $event->event_type == 4 ? $event->city . ', ' . $event->location->state : $event->type;
            $temp['event_location'] = $location;
            $temp['event_description'] = $event->description;
            $events[] = $temp;
        }

        return $events;
    }

    public function cleanTitleForUrl($dirty_string) {
        $lather = str_replace(' ', '_', $dirty_string); // Replaces all spaces with underscores.
        return $rinse = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $lather)); // Removes special chars and makes lower case
    }

    /**
     * Update singular event
     */
    public function updateEventInformation() {
        $eventImage = '';
        if (Input::hasFile('event_image')) {
            $filename = $this->events->storeEventPhoto(preg_replace('/[^A-Za-z0-9 _ .-]/', '', Input::get('title')), 'event_image');
            $eventImage = $filename;
        }
        return $this->events->updateEvent($eventImage);
    }

    /**
     * Remove single event
     */
    public function deleteEvent() {
        return $removed = $this->events->removeEvent();
    }

}
