<?php

/* Autogenerated Forum Controller */
/* @author Derek J. Foster
  /* Hook point of the Forum package inside your laravel application */
/* Feel free to override methods here to fit your requirements */

use UserRepo\Storage\Forums\TopicRepository as Topics;
use UserRepo\Storage\Forums\CategoryRepository as Categories;
use UserRepo\Storage\Forums\MessageRepository as Messages;
use UserRepo\Storage\User\UserRepository as UserRepo;
use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;
use CreatorServices\Forum\Creator as ForumCreator;
use FileRepo\Storage\Resources\FileRepository as FileInterface;
use Files\Store\StoreFiles as FileStorage;

class ForumController extends \BaseController {

    private $categories;
    private $topics;
    private $messages;

    public function __construct(Topics $topics, Categories $categories, Messages $messages, UserRepo $users, GroupPermissions $groups, FileInterface $files) {
        parent::__construct();
        $this->users = $users;
        $this->files = $files;
        $this->topics = $topics;
        $this->categories = $categories;
        $this->messages = $messages;
        $this->permissions = $groups;
        $this->page = 'Message Boards';
        View::share('page', $this->page);
        if (Auth::check()) {
            $this->user_permissions = explode(':', Auth::user()->groups);
            View::share('usr_email', Auth::user()->email);
            View::share('usr_name', Auth::user()->fname);
            View::share('usr_company', Auth::user()->pharmacy);
            View::share('usr_permissions', explode(':', Auth::user()->groups));
        }
        $css = [ 'forum', 'main'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Message Boards | PDSadvantage');
    }

    public static function getFilter($type) {
        if (isset($type)) {
            $category = ForumCategories::where('id', $type)->first();
            return count($category) > 0 ? $category->id : 0;
        }
        return 0;
    }

    //website main index, public facing data
    public function getIndex() {
        $forum_data = $this->getForumData();
        return View::make('Forums.forum-index', compact('forum_data'));
    }
    
    public function getPrivateForum() {
        if(Auth::user()->account_type == 2) return Redirect::route('dash.main')->with('Failure' , 'Vendors should not have access to that feature.');
        $forum_data = $this->getForumData(null, true, Input::get('companyId'));
        return View::make('my-pharmacy.index', compact('forum_data'));
    }
    
    public function searchPrivateForum() {
        if(Auth::user()->account_type == 2) return Redirect::route('dash.main')->with('Failure' , 'Vendors should not have access to that feature.');
        $forum_data = $this->getForumData(Input::get('search_key'), true, Input::get('companyId'));
        return View::make('my-pharmacy.index', compact('forum_data'));
    }
    
    public function getFreeCategoryArray() {
        $categories = $this->categories->determineCategoryType('free');
        foreach($categories as $category) {
            $array[] = $category->id;
        }
        return $array;
    }
    
    public function getProperTopics($categoryId, $searchKeys, $companyId, $private) {
        
        if($searchKeys != null) {
            $topics = $this->topics->getSearchedTopics($searchKeys, $private);
        } else {
            if($companyId > 1) {
                $topics = $this->topics->getAllPrivateForumTopics($categoryId, $companyId);
            } else {
                if($private != true) {
                    $topics =  $topics = intval($categoryId) ? $this->topics->getAllForumTopics($categoryId) : $this->topics->getAllForumTopics();
                } else {
                    /* This needs to gget all users) */
                    $topics = $this->topics->getAllPrivateForumTopics();
                }
                
            }    
        }
        return $topics;
    }
    
    public function getUserCompanies() {
        $companies = json_decode(User::find(Auth::id())->companies);
        return $companies;
    }
    
    public function getForumData($searchKeys = null, $private = false, $companyId = null) {
        $user = User::find(Auth::id());
        if($private != 0) {
            $data['privateCategories'] = $this->categories->getAllCategories('private');
        } 
        $data['allCategories'] = $user->employee_type != 2 ? $this->categories->getAllCategories() : null;
        $data['paidCategories'] = $user->employee_type != 2 ? $this->categories->getAllCategories('paid') : null;
        $data['freeCategories'] = $user->employee_type != 2 ? $this->categories->getAllCategories('free') : null;
        $data['freeCategoriesArray'] = $user->employee_type != 2 ? $this->getFreeCategoryArray() : null;
        $data['categoryId'] = self::getFilter(Input::get('category'));
        $data['pharmacy'] = $data['categoryId'] == 8 ? 'TRUE' : 'FALSE';
        $data['authors'] = $this->categories->getAuthors();
        $data['allTopics'] = $this->getProperTopics($data['categoryId'], $searchKeys, $companyId, $private);
        $data['companies'] = Company::getCompanyDropdown();
        $data['featuredPosts'] = $user->employee_type != 2 ? $this->topics->getAllFeatured($data['categoryId']) : null;
        $data['activeCompany'] = $companyId;
        /* 
         * Do we really need any of these below?  Because of base controller permissions.  maybe we also just send it
         * to the view share as an array already...   then we don't have to worry about all of these.
         * will take some refactoring.  That is what I like the weekends for.
         */
        $data['permissions'] = \User::getCurrentPermissions();
        $data['acl'] = $this->permissions->allInArray();
        $data['usr_permissions'] = $this->permissions->getUserGroups(); //should really get rid of one of these and probably all
        return $data;
    }
     
    public function makeTopicUnFeatured($id) {
        $post = ForumTopics::find($id);
        $post->featured = 0;
        if($post->save()) {
            $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $post->title);
            $postTitle = preg_replace('/ /', '-', $scrubbedTitle);
            return Redirect::to('message-boards/'.$postTitle.'/'.$id);
        }
        return Redirect::to('message-boards/'.$postTitle.'/'.$id);
    }
    
    public function makeTopicFeatured($id) {
        $post = ForumTopics::find($id);
        $post->featured = 1;
        if($post->save()) {
            $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $post->title);
            $postTitle = preg_replace('/ /', '-', $scrubbedTitle);
            return Redirect::to('message-boards/'.$postTitle.'/'.$id);
        }
        return Redirect::to('message-boards/'.$postTitle.'/'.$id);
    }
    /**
     * Using category id given by the user, this method finds the available topics based off of the user's chosen category id
     *
     * @param string $condition
     */
    public static function getForumDashboard() {
        $allTopics = $this->topics->getAllTopics($categoryId);
        return $allTopics;
    } 

    public function getSearchedIndex() {
        $search_key = Input::get('search_key');
        $forum_data = $this->getForumData($search_key);
        return View::make('Forums.forum-index', compact('forum_data'));
    }
    
//    public function testing()
//    {
//        return $this->topics->checkAccessFromTopicParent();
//    }

    /**
     * Get update view for chosen topic
     *
     * @param int $topic_id
     */
    public function getTopicUpdate($topic_id) {
        //attempt to retrieve chosen topic
        $topic = $this->topics->findTopicById($topic_id); 

        //retrieve available authors
        $authors = $this->categories->getAuthors();

        //if user is submitting edits to the topic, then update the record
        $editing = Input::get('edit');
        if ($editing) {
            $rules = [
                'title' => 'required',
                'description' => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->with('Failure', 'Topic could not be updated. Errors are present.')->withInput()->withErrors($validator);
            }

            $topic->title = Input::get('title');
            $topic->description = Input::get('description');

            if ($topic->save()) {
                return Redirect::back()->with('Success', 'Topic successfully updated.');
            }

            return Redirect::back()->with('Failure', 'Topic changes could not be saved. Please reload the page and try again. If the problem persists, please call your web administrator.')->withInput();
        }

        return View::make('Forums.Manager.edit-topic', compact('topic', 'authors'));
    }

    /**
     * Get recent topics
     */
    public function getRecentTopics() {
        $categories = $this->categories->allPaginated(10);
        $category_data = $this->categories->all();
        $categoriesArray = array();
        foreach ($category_data as $category) {
            $categoriesArray[$category->id] = $category->title;
        }

        //attempt to retrieve all available author choices
        $authors = $this->categories->getAuthors();

        //get recent posts
        $recent_posts = $this->topics->getRecentTopics(12);
        $recent_method = true;
        return View::make('Forums.index', compact('categories', 'categoriesArray', 'recent_posts', 'sub_topics', 'category_id', 'recent_method', 'authors'));
    }

    //dashboard index for forum manager, accessible only by pds admins
    public function getManagerIndex() {
        //retrieve the correct dashboard menu for the user
        $menu_data = $this->users->getMenu();

        //attempt to retrieve all available categories
        $categories = $this->categories->all();

        $catsForTopics = array();
        foreach ($categories as $mainkey => $value) {
            $catsForTopics[$value->id] = $value->title;
        }

        $authors = $this->categories->getAuthors();
        $acl = $this->permissions->allInArray();
        return View::make('Forums.Manager.index', compact('menu_data', 'categories', 'authors', 'catsForTopics', 'acl'));
    }

    //get info on category the user wishes to update (ajax method)
    public function getCategoryInfo($cat_id) {
        return $this->categories->findCategoryById($cat_id);
    }

    public function createNewCategory() {
        $creator = new ForumCreator($this);
        return $creator->createCategory(Input::all());
    }
    
    //success call from the ForumCreator
    public function categoryCreationSucceeds() {
        return Redirect::back()->with('Success', 'The new category has been created successfully!');
    }
    
    //failure call from the ForumCreator
    public function categoryCreationFails($errors) {
        return Redirect::back()->with('Failure', 'The new category was not able to be created. Click \'Create New Category\' to see your errors.')->withInput()->withErrors($errors);
    }

    public function createNewTopic() {
        //get data that is ungaurded for creation in the forum topics model
        $data = Input::only('title', 'parent_category', 'description', 'is_pharmacy_private_post', 'company' );
        $creator = new ForumCreator($this);
        
        $response = $creator->createPost($data);
        if(isset($response->id)) {
            if(Input::hasFile('attachment')) {
                $file_storage = new FileStorage($this);
                $store_attachment = $file_storage->storeTopicAttachment($response, $response['id']);
                return $this->returnCreationResponse($store_attachment);
            }
        }

        //returns normal success or failure response for topic creation
        return $this->postCreationSucceeds();
    }
    
    public function replyWithTopicAttachmentSuccess($topic_id) {
        return Redirect::route('forum.index')->with('Success', 'Successfully created new topic with its attachment.');
    }
    
    public function replyWithTopicAttachmentFailure($topic_id) {
        return Redirect::back()->withInput()->with('Failure', 'We created your topic but could not store the attachment. Please remove the topic and try again, or change the format of the file you are attaching.');
    }
    
    public function returnCreationResponse($store_attachment) {
        if($store_attachment === 1) {
            return $this->postCreationSucceeds();
        }else{
            return $store_attachment;
        }
    }
    
    //success call from the ForumCreator
    public function postCreationSucceeds() {
        return Redirect::back()->with('Success', 'The new topic has been created successfully!');
    }
    
    //failure call from the ForumCreator
    public function postCreationFails($errors){
        return Redirect::back()->with('Failure', 'The new topic was not able to be created. Click \'Create New Topic\' to see your errors.')->withInput()->withErrors($errors);
    }

    public function updateCategoryInfo() {
        $rules = [
            'title' => 'required',
            'subtitle' => 'required'
        ];

        $validator = Validator::make(Input::only('title', 'subtitle', 'admin_only'), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $privacy_choice = Input::get('admin_only');
        $cat_id = Input::get('cat_id');
        $cat_to_change = $this->categories->findCategoryByIdObject($cat_id);

        $cat_to_change->title = Input::get('title');
        $cat_to_change->subtitle = Input::get('subtitle');
        $cat_to_change->admin_only = isset($privacy_choice) ? $privacy_choice : $cat_to_change->admin_only;

        if ($cat_to_change->save()) {
            return Redirect::back()->with('Success', 'The category has been updated successfully');
        }

        return Redirect::back()->with('Failure', 'The category could not be update with the given information. Please try again.');
    }

    public function updateCategoryRestriction() {
        $privacy_choice = Input::get('choice');
        $id = Input::get('cat_id');

        $cat_update = $this->categories->findCategoryByIdObject($id);

        $cat_update->admin_only = $privacy_choice;

        if ($cat_update->save()) {
            if ($privacy_choice == 1) {
                $response = 'Successfully made private';
            } else {
                $response = 'Successfully made public';
            }
            return Redirect::back()->with('Success', $response);
        }
        return Redirect::back()->with('Failure', 'Failed to make public. Please try again. If the problem persists, contact your web administrator.');
    }

    /**
     * Update the topic field 'featured' to the desired boolean value (1 or 0)
     * 1 means featured item, and naturally 0 will mean not feature item
     * 
     * @param query string: update_type
     */
    public function updateTopicFeature() {
        //init vars
        $id = Input::get('id');
        $type = Input::get('update_type');

        //retrieve topic
        $topic = $this->topics->findTopicById($id);

        //update topic featured value based on the update_type
        $topic->featured = $type == 1 ? 1 : 0;

        if ($topic->save()) {
            if ($type == 0)
                return Redirect::back()->with('Success Remove', 'Topic was successfully removed from the featured listings in the message boards.');
            return Redirect::back()->with('Success', 'Topic is now listed as a featured item in the message boards.');
        }

        return Redirect::back()->with('Failure', 'Topic could not be updated. Please try again. If this problem persists, contact the web administrator in support.');
    }

    public function destroyCategory($cat_id) {
        //retrieve category by id
        $category = $this->categories->findCategoryByIdObject($cat_id);

        if ($category->forceDelete()) {
            //retrieve topics of category being deleted
            $category_topics = $this->categories->getTopicsByParent($cat_id);

            foreach ($category_topics as $key => $value) {
                if ($value->forceDelete())
                    continue;
                return Redirect::back()->with('Failure', 'Whoops! Something went wrong with removing the category\'s topics. Please refresh and try again.');
            }

            return Redirect::back()->with('Success', 'Category was successfully removed.');
        }else {
            return Redirect::back()->with('Failure', 'Whoops! Something went wrong with removing the Category chosen. Please refresh and try again.');
        }
    }

    public function destroyTopic($topic_id) {
        //delete the messages associated with the topic
        $delete_messages = $this->messages->deleteFromParentTopic($topic_id);

        if ($delete_messages == "success") {
            //now we can remove the parent topic
            $topic = $this->topics->findTopicById($topic_id);

            //soft delete topic
            if ($topic->delete()) {
                return Redirect::route('forum.index')->with('Success', 'Topic was successfully removed.');
            } else {
                return Redirect::route('forum.index')->with('Failure', 'Topic could not be removed. Please refresh the page and try again.');
            }
        }

        return Redirect::route('forum.index')->with('Failure', 'Could not remove messages from topic. PLease contact the web administrator.');
    }

}
