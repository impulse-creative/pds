<?php

/* FORUM MESSAGE CONTROLLER
 * 
 * Description: The purpose of this controller is to deliver the forum posts to the front end user, as well as help the user
 * 				create, update, and destroy their own entries; provided they have the permissions to do so.
 * @author Derek
 */

use UserRepo\Storage\Forums\TopicRepository as Topics;
use UserRepo\Storage\Forums\CategoryRepository as Categories;
use UserRepo\Storage\Forums\MessageRepository as Messages;
use UserRepo\Storage\User\UserRepository as UserRepo;
use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;
use NotificationRepo\Notifications\NotificationRepository as NotificationRepo;
use CreatorServices\Forum\Creator as ForumCreator;
use Files\Store\StoreFiles as FileStorage;
use Carbon\Carbon as Carbon;

class ForumMessageController extends \BaseController {

    private $categories;
    private $topics;
    private $messages;

    public function __construct(GroupPermissions $groups, Topics $topics, Categories $categories, Messages $messages, UserRepo $users, NotificationRepo $notifications) {
        $this->users = $users;
        $this->topics = $topics;
        $this->categories = $categories;
        $this->messages = $messages;
        $this->permissions = $groups;
        $this->notifications = $notifications;
        $this->page = 'Message Boards';
        View::share('page', $this->page);
        if (Auth::check()) {
            View::share('usr_email', Auth::user()->email);
            View::share('usr_name', Auth::user()->fname);
        }
        //$this->user_email = Auth::user()->email;
        $css = [ 'forum', 'main'];
        
        View::share('css', $css);
        View::share('pageTitle', 'PDS Message Boards | PDSadvantage');
    }

    /**
     * Create a new comment for a message board topic
     *
     * @param text $data
     * @param int  $author_id
     * @param int  $parent_topic
     */
    public function createNewComment() {
        $creator = new ForumCreator($this);
        return $creator->createReply(Input::except('page_id'), Input::get('page_id'));
    }
    
    public function replySuccess($notification, $message_id, $page_id, $return_url,$attachment = NULL) {
        if(isset($attachment)){
            $filestorage = new FileStorage($this);
            return $response = $filestorage->storeMessageAttachment(Input::all(), $message_id);
        }
        return Redirect::to($return_url)->with('Success', 'Successfuly replied to post, ' . $notification);
    }
    
    public function replyFailed($errors) {
        return Redirect::back()->with('Failure', 'Failed to replied to post. Could not save your reply, please check your input and try again.')->withInput()->withErrors($errors);
    }
    
    public function topicUpdateFailed() {
        throw new Exception('Failed to update topic\'s update time field.');
    }
    
    public function replyWithAttachmentSuccess($message_id) {
        return Redirect::to(URL::previous().'#'.$message_id)->with('Success', 'Successfuly replied to post and stored your attachment.');
    }
    
    public function replyWithAttachmentFailure($message_id) {
        return Redirect::to(URL::previous().'#'.$message_id)->with('Failure', 'Successfuly replied to post and but could not store your attachment.');
    }

    public function getAllCategories() {
        $categoryData = $this->categories->all();
        $categoriesArray = array();
        $categoriesArray[0] = '-- Select a Category --';
        foreach ($categoryData as $category) {
            $categoriesArray[$category->id] = $category->title;
        }
        return $categoriesArray;
    }

    public function getEditComment($msg_id) {
        $message = $this->messages->findMessageById($msg_id);
        $forum_data['acl'] = $this->permissions->allInArray();
        $forum_data['usr_permissions'] = $this->permissions->getUserGroups();
        $edit_reply_editor_data = $this->getPostMessageEditorInfo($message->data);
        return View::make('Forums.posts.edit-reply', compact('message', 'forum_data'))
                ->nest('reply_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $edit_reply_editor_data));
    }

    public function getPostMessageEditorInfo($old_input) {
        $data['editor_class'] = "edit-reply";
        $data['form_class'] = "add-reply";
        $data['input_name'] = "data";
        $data['editor_id'] = "editUserReply";
        $data['old_input'] = $old_input;
        return $data;
    }
    
    public function updateTopicComment($msg_id) {
        //get message to edit
        $message = $this->messages->findMessageById($msg_id);

        //return var_dump(Input::all());
        $message->data = Input::get('data');

        if ($message->save()) {
            $rules = [
                'data' => 'required'
            ];

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails() || Input::get('data') == ' ') {
                return Redirect::back()->with('Failure', 'Failed to update your reply. Errors present.')->withInput()->withErrors($validator);
            }

            $topic = $this->topics->findTopicById($message->parent_topic);

            $title = $topic->title;
            $topic_id = $topic->id;

            $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $title);
            $final_title = preg_replace('/ /', '-', $scrubbed_title);
            $post_url = Input::get('private') != null && Input::get('private') == 'true' ? URL::to('message-boards/private/company/'.$final_title.'/'.$topic_id.'?private=true&category='.Input::get('category').'&companyId='.Input::get('companyId').'#'.$message->id) : URL::route('forum.post', [$final_title, $topic_id]) . '#' . $message->id;
            return Redirect::to($post_url)->with('Success', 'Successfuly updated your reply.');
        }


        return Redirect::back()->with('Failure', 'Failed to updat your reply. Please try again. If the problem persists please contact the web administrator.');
    }

}
