<?php

/* FORUM POST CONTROLLER
 * 
 * Description: The purpose of this controller is to deliver the forum posts to the front end user, as well as help the user
 * 				create, update, and destroy their own entries; provided they have the permissions to do so.
 * @author Derek
 */

use UserRepo\Storage\Forums\TopicRepository as Topics;
use UserRepo\Storage\Forums\CategoryRepository as Categories;
use UserRepo\Storage\Forums\MessageRepository as Messages;
use UserRepo\Storage\User\UserRepository as UserRepo;
use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;
use Carbon\Carbon;

class ForumPostController extends \BaseController {

    private $categories;
    private $topics;
    private $messages;

    public function __construct(Topics $topics, Categories $categories, Messages $messages, UserRepo $users, GroupPermissions $groups) {
        $this->users = $users;
        $this->topics = $topics;
        $this->categories = $categories;
        $this->messages = $messages;
        $this->permissions = $groups;
        $this->page = 'Message Boards';
        View::share('page', $this->page);
        if (Auth::check()) {
            View::share('usr_email', Auth::user()->email);
            View::share('usr_name', Auth::user()->fname);
            View::share('usr_company', Auth::user()->pharmacy);
        }
        $css = [ 'forum', 'main'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Message Boards | PDSadvantage');
    }

    public function getForumPost($title, $post_id) {
        if(ForumTopics::find($post_id) == null) {
            return Redirect::route('forum.index');
        }
        $topic_data = $this->getTopicData($post_id);
        $forum_data = $this->getCategoriesAndAcl();
        //this means a post was selected, so we need to update the number of views on the post
        if (!$topic_data['update_view'])
            throw new Exception('Post id was invalid. Could not update the number of views for the post.');
        
        $update_editor_data = $this->getUpdateEditorInfo($topic_data);
        $message_editor_data = $this->getPostMessageEditorInfo();
        $pageTitle = $topic_data['topic']->title. ' | PDSadvantage';
        if(Input::get('private') != null) {
            $forum_data['companies'] = Company::getCompanyDropdown();
            $forum_data['privateCategories'] = $this->categories->getAllCategories('private');
            return View::make('my-pharmacy.post', compact('forum_data', 'topic_data', 'pageTitle'))
                ->nest('update_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $update_editor_data))
                ->nest('reply_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $message_editor_data));
        }
        return View::make('Forums.posts.index', compact('forum_data', 'topic_data', 'pageTitle'))
                ->nest('update_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $update_editor_data))
                ->nest('reply_editor', 'dashboard.partials.bootstrap-wisywig', array('editor_data' => $message_editor_data));
    }
    
    
    
    public function getUpdateEditorInfo($topic_data) {
        $data['editor_class'] = "updateTopicEditor";
        $data['form_class'] = "updateForumTopic";
        $data['input_name'] = "description";
        $data['editor_id'] = 'editor1';
        $data['old_input'] = $topic_data['topic']->description;
        return $data;
    }
    
    public function getPostMessageEditorInfo() {
        $data['editor_class'] = "topic-reply";
        $data['form_class'] = "add-reply";
        $data['editor_id'] = 'editor2';
        $data['input_name'] = "data";
        $data['old_input'] = '';
        return $data;
    }
    
    public function getCategoriesAndAcl() {
        $forum_data = array();
        $forum_data['acl'] = $this->permissions->allInArray();
        $forum_data['allCategories'] = $this->categories->getAllCategories();
        $forum_data['paidCategories'] = $this->categories->getAllCategories('paid');
        $forum_data['freeCategories'] = $this->categories->getAllCategories('free');
        $forum_data['usr_permissions'] = $this->permissions->getUserGroups();
        return $forum_data;
    }
    
    public function getTopicData($post_id) {
        $topic_data = array();
        $topic_data['categoryId'] = ForumController::getFilter(Input::get('category'));
        $topic_data['categories'] = $this->categories->allPaginated(6);
        $topic_data['topic'] = $this->topics->findTopicWithDetails($post_id);
        $topic_data['realTopic'] = $this->topics->findTopicById($post_id);
        $topic_data['authors'] = $this->categories->getAuthors();
        $topic_data['update_view'] = $this->topics->updateNumViews($post_id);
        $topic_data['authorId'] = $topic_data['topic']->id;
        $topic_data['author'] = $this->users->find($topic_data['authorId']);
        $topic_data['parentCategory'] = $this->categories->findCategoryByIdObject($topic_data['topic']->parent_category);
        $topic_data['messages'] = $this->messages->findMessagesByParent($topic_data['topic']->id);
        $topic_data['count'] = $this->messages->getCommentCount($topic_data['topic']->id);
        return $topic_data;
    }
}
