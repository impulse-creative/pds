<?php

/**
 * Controller for Group Permissions
 * @author Derek J. Foster
 */
use GroupRepo\Permissions\Groups\GroupRepository as GroupPermissions;
use UserRepo\Storage\User\UserRepository as UserRepoInterface;

class GroupController extends BaseController {

    public $restful = true;

    public function __construct(GroupPermissions $groups, UserRepoInterface $user) {
        $this->groups = $groups;
        $this->user = $user;
        $this->pds_property_group = 'pds_advantage___permission_levels';
        $css = [ 'dash', 'forum', 'main'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Permissions | PDSadvantage');
    }

    //show main view of the group administration tool
    public function showIndex() {
        $id = Auth::id();
        $menu_data = $this->user->getMenu();
        $user = $this->user->find($id);
        $groups = $this->groups->getNonResourceGroups(true);
        $resource_groups = $this->groups->getResourceGroups(true, true);
        //return var_dump($resource_groups);
        $allowed = $this->groups->checkIfAdmin($id);
        $employees = User::isEmployee();
        if ($allowed) {
            return View::make('dashboard.Groups.index', compact('menu_data', 'groups', 'user', 'resource_groups', 'employees'));
        } else {
            return Redirect::route('dash.main');
        }
    }

    //show view of group 'add' administration tool
    public function showAdd() {
        //retrieve the correct dashboard menu for the user
        $menu_data = $this->user->getMenu();
        return View::make('dashboard.Groups.add', compact('menu_data'));
    }

    //show view of group 'edit' administration tool
    public function showEdit() {
        //retrieve the correct dashboard menu for the user
        $menu_data = $this->user->getMenu();
        return View::make('dashboard.Groups.alter-group', compact('menu_data'));
    }

    //handle post request for group 'add' administration tool
    public function addGroup() {
        $input = [
            'name' => trim(Input::get('name')),
            'hs_field' => trim(str_replace(' ', '_', Input::get('name'))),
            'description' => trim(Input::get('description'))
        ];
        $rules = [
            'name' => 'required',
            'description' => 'required'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::route('acl.addgroups')->with('errors', $form->getErrors())->withErrors($validator);
        }

        $new_group = str_replace(' ', '_', Input::get('name'));
        $property_group_name = $this->pds_property_group;
        $description = trim(Input::get('description'));

        $sync_groups = $this->groups->addPropertyToHS($property_group_name, $new_group);

        $synced = 0;
        switch ($sync_groups['errors']) {
            case 200:
                $response = 'Successfully Created Group and Synced with Hubspot.';
                $synced = 1;
                break;
            case 409:
                $response = 'The field you are trying to create already exists!';
                $synced = 1;
                break;
            case 500:
                $response = "Internal Error: Contact Web Administrator ";
                $synced = 0;
                break;
            case 404:
                $response = 'Could not find the property in Hubspot. Please check your input.';
                $synced = 0;
                break;
            default:
                $response = 'Could not add the property to Hubspot. Please contact your web administrator. Error Hubspot returned: ' . $sync_groups['errors'] . ' | ' . $sync_groups['hs_response'];
                return Redirect::route("acl.control")->with('errors', $response);
                break;
        }

        if ($this->groups->checkIsValidForAdd()) {
            Group::create([
                "name" => $new_group,
                "description" => $description,
                "hs_field" => "permit_".$new_group,
                "hs_synced" => $synced
            ]);

            return Redirect::route("acl.control")->with('Success', $response);
        }

        return Redirect::route("acl.addgroups")->withInput([
                    "name" => Input::get("name")
                ])->with('errors', $form->getErrors());
    }

    //handle post request for group 'edit' administration tool
    public function edit() {
        $menu_data = $this->user->getMenu();

        $group = $this->groups->getGroupById(Input::get("id"));
        $url = URL::full();
        if (isset($_POST["name"])) {
            $original_name = strtolower('permit_' . str_replace(' ', '_', $group->name));
            $new_name = str_replace(' ', '_', Input::get('name'));
            $new_group = 'permit_' . strtolower($new_name);
            $permission_group = $this->pds_property_group;
            if ($this->groups->checkIsValidForEdit()) {
                $sync_update = $this->groups->updateGroupName($original_name, $new_group, $permission_group);
                switch ($sync_update['errors']) {
                    case 200:
                        $response = 'Successfully Created Group and Synced with Hubspot.';
                        $synced = 1;
                        break;
                    case 409:
                        $response = 'The field you are trying to create already exists!';
                        $synced = 1;
                        break;
                    case 500:
                        $response = " Internal Error: Contact Web Administrator ";
                        $synced = 0;
                        break;
                    case 404:
                        $response = 'Could not find the property in Hubspot. Please check your input.';
                        $synced = 0;
                        break;
                    default:
                        $response = 'Could not add the property to Hubspot. Please contact your web administrator. Error Hubspot returned: ' . $sync_update['errors'] . ' | ' . $sync_update['hs_response'];
                        return Redirect::route("acl.control")->with('errors', $response);
                        break;
                }
                $group->name = $new_name;
                $group->save();
                return Redirect::route("acl.control");
            }

            return Redirect::to($url)->withInput([
                        "name" => $new_name,
                        "errors" => $form->getErrors(),
                        "url" => $url,
                        "menu_data" => $menu_data
            ]);
        }

        return View::make("dashboard.Groups.alter-group", [
                    "form" => $form = null,
                    "group" => $group,
                    "menu_data" => $menu_data
        ]);
    }

    public function editResourceGroup() {
        $menu_data = $this->user->getMenu();
        $group = $this->groups->getResourceGroupById(Input::get('id'));
        $url = URL::full();
        $access_list = $this->groups->allInArray();
        if (isset($_POST["name"])) {

            if ($this->groups->checkIfPdsAdmin(Auth::id()) || $this->groups->checkIfAdmin(Auth::id()) || $this->groups->checkAccess('9')) {
                $group->group_name = trim(Input::get('name'));
                $group->description = trim(Input::get('description'));
                $group->permitted_groups = Input::get('access');
                if ($group->save()) {
                    return Redirect::route("acl.control")->with('Success', 'Successfully edited the resource group!');
                }
                return Redirect::route("acl.control")->with('Failure', 'Could not update the record. Please try again.');
            }

            return Redirect::to($url)->withInput([
                        "name" => $new_name,
                        "errors" => $form->getErrors(),
                        "url" => $url,
                        "menu_data" => $menu_data,
                        "access" => $access_list
            ]);
        }

        return View::make("dashboard.Groups.Resource_Groups.edit", [
                    "form" => $form = null,
                    "group" => $group,
                    "menu_data" => $menu_data,
                    "access" => $access_list
        ]);
    }

    //handles post request for group 'delete' functionality
    //Group view prompts user to choose if they actually want to delete the group
    //before firing this function.
    public function delete() {
        //initialize variables
        $group_id = Input::get('id');
        $is_resource_group = Input::get('resource_group');
        //if removing resource group instead of standard permission group, use different method
        if ($is_resource_group == true) {
            return self::removeResourceGroup($group_id);
        }

        return self::removePermissionGroup($group_id);
    }

    public function removeResourceGroup($id) {
        $group = $this->groups->getResourceGroupById($id);

        if ($this->groups->checkIfPdsAdmin(Auth::id()) || $this->groups->checkIfAdmin(Auth::id()) || $this->groups->checkAccess('9')) {
            $group->delete();
            return Redirect::route("acl.control")->with('Success', 'Successfully Removed resource group!');
        }

        return Redirect::route("acl.control")->with('errors', 'Could not remove the resource group. Please try again.');
    }

    public function removePermissionGroup($id) {
        //if it wasn't a resource group, then it is a permission group, continue with other processes
        $group = $this->groups->getGroupById($id);

        $property_name = self::modifyForHS($group->name);

        $HS_removal_response = $this->groups->removeFromHSGroup($property_name);

        $response = self::getHubspotCustomisedRemovalResponse($HS_removal_response);

        if ($this->groups->checkIsValidForDelete()) {
            $group->delete();
            return Redirect::route("acl.control")->with('Success', $response);
        }

        return Redirect::route("acl.control")->with('errors', $response);
    }

    public function modifyForHS($name) {
        $hs_group_prefix = 'permit_';
        $temp = strtolower($name);
        $modified_name = $hs_group_prefix . $temp;

        return $modified_name;
    }

    public function getHubspotCustomisedRemovalResponse($removal_response) {
        switch ($removal_response['errors']) {
            case 204:
                $response = 'Successfully Deleted Group and Synced with Hubspot.';
                break;
            case 500:
                $response = " Internal Error: Contact Web Administrator ";
                return Redirect::route("acl.control")->with('errors', $response);
                break;
            case 404:
                $response = 'Could not find the property in Hubspot. Please check your input.';
                return Redirect::route("acl.control")->with('errors', $response);
                break;
            default:
                $response = 'Hubspot could not process your request. Please contact your web administrator. Error Hubspot returned: ' . $removeFromHS['errors'];
                return Redirect::route("acl.control")->with('errors', $response);
                break;
        }
    }
    
    

}
