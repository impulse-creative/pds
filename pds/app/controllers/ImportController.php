<?php
use \Carbon\Carbon;

class ImportController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }
       
    public function importEverythingNow() {
        
        set_time_limit(0);
        $this->importUsers();
        $this->importTopics();
        $this->importMessages();
        $this->importCompanies();
        /*$this->makeUsers();
        $this->makeTopics();
        $this->makeMessages(); */
        echo 'donemang';
    }
    public function importUsers() {
        
        //$file = fopen(public_path() . '/storage/files/import/pds_export.sql', 'r');
        set_time_limit(0);
        $file = fopen(public_path() . '/storage/files/import/pds_export_after_jeff_bitches.sql', 'r');
        $user = '';
        $count = 0;
        while (!feof($file)) {
            $line = fgets($file);
            if(($count > 0 && strpos($line, 'NSERT [dbo].[AccountUsers]') == 1 && strlen($user) > 0) || strpos($line, 'IDENTITY_INSERT [dbo].[AccountPharmacies] OFF') > 1) {
                $newUser = explode('VALUES', $user);
                $data = substr_replace($newUser[1], '', 1, 1);
                $data = substr_replace($data, '', -3);
                $data = str_replace("N'''", "'\'", $data);
                $data = str_replace(" N'", " '", $data);
                $data = str_replace("''", "\'", $data);
                $userData = str_replace("''", "\'", $data);
                echo '<hr>';
                
                $array = str_getcsv($userData, ',', "'");
                
                $oldId = trim($array[0]);
                $oldCompany = trim($array[1]);
                
                $name = trim($array[2]);
                
                if(strpos($name, ' ')) {
                    $fullName = explode(' ', $name);
                    $fname = $fullName[0];
                    $lname = $fullName[1];
                } else {
                    $fname = $name;
                    $lname = $name;
                }
                
                
                $email = $array[3];
                $imported = new Importusers();
                $imported->old_id = $oldId;
                $imported->old_company = str_replace("\'", "'", $oldCompany);
                $imported->fname = str_replace("\'", "'", $fname);
                $imported->lname = str_replace("\'", "'", $lname);
                $imported->email = $email;
                $imported->timestamps = false;
                
                if($imported->save()) {
                    echo 'saved '.$imported->id;
                    $user = '';
                } else {
                    var_dump('------>>>>>>>BROKENWHALEMOOSEATACK.PLEASEPREPAREALMONDSNACKS.REPORTDIEORELSE');
                }
                
                // make the user
            }
            


            if (strpos($line, 'NSERT [dbo].[AccountUsers]') == 1) {
                $count++;
                echo 'This is the beginning of the user. Should be 0<hr>';
                echo $user;
                echo '<hr>';
                echo strlen($user);
                $user = $line;
            } elseif (strpos($line, '/****** Object:  Table [dbo].[AccountPharmacies]')) {
                $user = '';
            } else { 
                if (strlen($user) > 0) { 
                    if (strlen($line) < 3) {
                        $user .='<br><br>';
                    }
                    echo $line.'<br>';
                    echo '[' . strlen($line) . ']<br>';
                    $user .= $line;
                } else {
                    //echo 'ignore this line maybe';
                }
            }
            
            
        }
    }
    
    

    public function importTopics() {
        echo 'starting topic import man';
        set_time_limit(0);
        $file = fopen(public_path() . '/storage/files/import/pds_export_after_jeff_bitches.sql', 'r');
        $topic = '';
        $count = 0;
        while (!feof($file)) {
            $line = fgets($file);
            if (strpos($topic, 'NULL)') > 0 || strpos($topic, ' 2)') > 0 || strpos($topic, ' 1)') > 0 || strpos($topic, ' 3)') > 0 || strpos($topic, ' 4)') > 0) {
                $newTopic = explode('VALUES', $topic);
                $data = substr_replace($newTopic[1], '', 1, 1);
                $data = substr_replace($data, '', -3);
                $data = str_replace("N'''", "'\'", $data);
                $data = str_replace(" N'", " '", $data);
                $data = str_replace("''", "\'", $data);
                $topicData = str_replace("''", "\'", $data);
                
                $array = str_getcsv($topicData, ',', "'");
                
                $oldId = trim($array[0]);
                $oldMessageBoardId = trim($array[1]);
                $oldUserId = trim($array[2]);
                $oldDate = trim($array[3]);
                $time = $oldDate;
                
                $newTime = str_replace('CAST', '', $time);
                $newTime = str_replace('(', '', $newTime);
                $newTime = str_replace(' AS DateTime)', ' ', $newTime);
                $additionalDays = hexdec(substr($newTime, 0, 10));
                $stupidDate = "1900-01-01";
                $newDate = date('Y-m-d', strtotime($stupidDate. ' + '.$additionalDays.' days'));
                //$startTime = Carbon::createFromFormat('m/d/Y h:i a', \Input::get('start_date'));
                if($newDate >  "2013-01-01") {
                    echo 'newdate = '.$newDate;
                    
                    $lastRepliedUser = trim($array[5]);
                    $oldSubject = trim($array[6]);
                    $oldMessage = trim($array[7]);
                    $oldCategory = isset($array[8]) ?  trim($array[8]) : 'NULL';
                    //$oldCategory = trim($array[8]);
                    $imported = new Importtopics();
                    $imported->old_id = $oldId;
                    $imported->old_message_board_id = $oldMessageBoardId;
                    $imported->old_user_id = $oldUserId;
                    $imported->last_replied_user = $lastRepliedUser;
                    $imported->old_subject = str_replace("\'", "'", $oldSubject);
                    $imported->old_message = str_replace("\'", "'", $oldMessage);
                    $imported->old_category = $oldCategory;
                    $imported->old_date = $newDate;

                    if($imported->save()) {
                        echo $imported->id.' created man<br>';
                        $topic = '';
                    } else { 
                        echo '------>>>>>>>BROKENWHALEMOOSEATACK.PLEASEPREPAREALMONDSNACKS.REPORTDIEORELSE';
                    }
                } else {
                    $topic = '';
                }
                
            }


            if (strpos($line, 'INSERT [dbo].[MessageBoardTopics]') === 0) {
                $topic = $line;
            } elseif (strpos($line, '/****** Object:  Table [dbo].[MessageBoards]')) {
                $topic = '';
            } else {
                if (strlen($topic) > 0) {
                    if (strlen($line) < 3) {
                        $topic .='<br><br>';
                    }
                    $topic .= $line;
                } else {
                    if(strpos($line, 'GO') === 0) {
                        
                    } elseif(strpos($line, "print 'Processed")) {
                        
                    } else {
                        echo 'WHY THE penguins ARE you HERe';
                    }
                    
                }
            }
        }
    }

    public function importMessages() {
        
        set_time_limit(0);
        $file = fopen(public_path() . '/storage/files/import/pds_export_after_jeff_bitches.sql', 'r');
        $message = '';
        $storedTopics = Importtopics::lists('old_id');
        
        
        while (!feof($file)) {
            $line = fgets($file);
            if (strpos($message, "')") > 0) {
                $newMessage = explode('VALUES', $message);
                $data = substr_replace($newMessage[1], '', 1, 1);
                $data = substr_replace($data, '', -3);
                $data = str_replace("N'''", "'\'", $data);
                $data = str_replace(" N'", " '", $data);
                $data = str_replace("''", "\'", $data);
                $messageData = str_replace("''", "\'", $data);
                $array = str_getcsv($messageData, ',', "'");
                $oldId = $array[0];
                $oldTopicId = $array[1];
                if(in_array($oldTopicId, $storedTopics)) {
                    echo 'in array<br>';
                    $oldUserId = $array[2];
                    $oldDate = $array[3];
                    $oldMessage = $array[4];
                    $imported = new Importmessages();
                    $imported->old_author_id= $oldUserId;
                    $imported->old_id = $oldId;
                    $imported->old_topic_id = $oldTopicId;
                    $imported->old_date = $oldDate;
                    $imported->message = str_replace("\'", "'", $oldMessage);
                    $imported->timestamps = false;
                    if($imported->save()) {
                        echo $imported->old_id.'<br>';
                        
                        $message = '';
                    } else { 
                        echo '------>>>>>>>BROKENWHALEMOOSEATACK.PLEASEPREPAREALMONDSNACKS.REPORTDIEORELSE';
                    }
                }
                
                
            }


            if (strpos($line, 'INSERT [dbo].[MessageBoardMessages]') === 0) {
                $message = $line;
            } elseif (strpos($line, '/****** Object:  Table [dbo].[AccountUsers]')) {
                $message = '';
            } else {
                if (strlen($message) > 0) {
                    if (strlen($line) < 3) {
                        $message .='<br><br>';
                    }
                    if(strpos($line, 'GO') === 0) {
                        
                    } elseif(strpos($line, "print 'Processed")) {
                        
                    } else {
                        
                    }
                    $message .= $line;
                } else {
                    //echo 'ignore this line maybe';
                }
            }
        }
    }
    public function importCompanies() {
        
        $file = fopen(public_path() . '/storage/files/import/pds_export_after_jeff_bitches.sql', 'r');
        $company = '';
        $count = 0;
        $die = 0;
        $newCount = 0;
        while (!feof($file)) {
            $line = fgets($file);
            
            if (strpos($company, "NULL)") > 0 || (strpos($company, 'RT [dbo].[AccountPharmacies]') > 1 && $count > 0)) {
                $newCompany= explode('VALUES', $company);
                
                //d(explode(',' $newCompany[0]);
                $data = substr_replace($newCompany[1], '', 1, 1);
                $data = substr_replace($data, '', -3);
                $data = str_replace("N'''", "'\'", $data);
                $data = str_replace(" N'", " '", $data);
                $data = str_replace("''", "\'", $data);
                $companyData = str_replace("''", "\'", $data);
                $array = str_getcsv($companyData, ',', "'");
                
                $Company = new \Company();
                $Company->old_company_id = trim($array[0]);
                $Company->name = str_replace("\'", "'", trim($array[1]));
                $Company->phone = trim($array[2]);
                $Company->domain_name = trim($array[3]);
                $Company->email_from = trim($array[4]);
                $Company->address = trim($array[5]);
                $Company->city = trim($array[6]);
                $Company->state = trim($array[7]);
                $Company->postal_code = trim($array[8]);
                $Company->owner_name = trim($array[9]);
                $Company->owner_email = trim($array[10]);
                //password
                //lastlogin
                $Company->ca_account = trim($array[20]);
                $Company->company_account = trim($array[21]);
                $Company->time_zone = trim($array[22]);
                $Company->level = trim($array[13]);
                $Company->is_vendor = trim($array[27]);
                
                if($Company->save()) {
                    $count++;
                    echo $count.'<br>';
                    $company = '';
                } else { 
                    var_dump('SAVEFAIL');
                }
                
                $company = '';
                
            }


            if (strpos($line, 'INSERT [dbo].[AccountPharmacies]') === 0) {
                $count++;
                $company = $line;
            } elseif (strpos($line, 'LASTLINE')) {
                $company = '';
            } else {
                if (strlen($company) > 0) {
                    if (strlen($line) < 3) {
                        $company .='<br><br>';
                    }
                    echo '[' . strlen($line) . ']<br>';
                    $company .= $line;
                } else {
                    //echo 'ignore this line maybe';
                }
            }
        }
    }
    
    public function makeTopics() {
        dd('not allowed');
        set_time_limit(0);
        $topics = Importtopics::all();
        foreach($topics as $topic) {
            $new = new ForumTopics();
            if(trim($topic->old_message_board_id) == 1) {
                $new->parent_category = 2;
            } elseif(trim($topic->old_message_board_id) == 2) {
                $new->parent_category = 7;
            } else {
                $new->parent_category = 8;
            }
            $new->author_id = $this->getImportedUserId(trim($topic->old_user_id));
            $new->title = str_replace("\'", "'", trim($topic->old_subject));
            $new->description = str_replace("\'", "'", trim($topic->old_message));
            $new->last_replied = $this->getImportedUserId(trim($topic->last_replied_user));
            $new->old_topic_id = $topic->old_id;
            
            $time = $topic->old_date;
            $newDate = date('Y-m-d', strtotime($time));
            //$startTime = Carbon::createFromFormat('m/d/Y h:i a', \Input::get('start_date'));
            $new->created_at = Carbon::createFromFormat('Y-m-d', $newDate);
            $new->updated_at = Carbon::createFromFormat('Y-m-d', $newDate);
            
            $new->post_views = 10;
            $new->featured = 0;
            echo '<hr>';
            
            if ($new->save()) {
                echo 'saved';
            } else {
                echo 'failed';
            }
            
            
        }
    }
    
    public function getParentId($oldTopicId) {
        dd('not allowed');
        $topic = ForumTopics::where('old_topic_id', $oldTopicId)->first();
        if($topic == null) {
            return 'skip';
        } else {
            return $topic->id;
        }
    }
    
    public function makeMessages() {
        dd('not allowed');
        set_time_limit(0);
        $messages = Importmessages::all();
        foreach($messages as $message) {
            $new = new ForumMessages();
            $new->parent_topic = $this->getParentId(trim($message->old_topic_id));
            $new->author_id = $this->getImportedUserId(trim($message->old_author_id));
            $new->data = str_replace("\'", "'", trim($message->message));
            
            $time = $message->old_date;
            
            $newTime = str_replace('CAST', '', $time);
            $newTime = str_replace('(', '', $newTime);
            $newTime = str_replace(' AS DateTime)', ' ', $newTime);
            
            $additionalDays = hexdec(substr(trim($newTime), 0, 10));
            $stupidDate = "1900-01-01";
            $newDate = date('Y-m-d', strtotime($stupidDate. ' + '.$additionalDays.' days'));
            //$startTime = Carbon::createFromFormat('m/d/Y h:i a', \Input::get('start_date'));
            $carbonDate = Carbon::createFromTimestamp(strtotime($newDate));
            $new->created_at = $carbonDate;
            $new->updated_at = $carbonDate;
            echo '<hr>';
            if($new->parent_topic != 'skip') {
                if ($new->save()) {
                    echo 'saved: '.$new->id.'<br>';
                } else {
                    echo 'failed';
                }
            } else {
                echo 'SKIPPED YO';
            }
            
            
        }
    }
    
    public function getImportedUserId($oldId) {
        
        $user = User::where('old_id', $oldId)->first();
        if($user == null) {
            return 1;
        } else {
            return $user->id;
        }
    }
    
    
    
    public function addEmployeeTypeToUsers() {
        $companies = Company::all();
        foreach($companies as $company) {
            $user = User::where('email', $company->owner_email)->first();
            if($user != null) {
                $user->employee_type = 1;
            
                if($user->save()) {
                    echo 'saved a user<br>';
                } else {
                    echo '<hr>';
                    echo 'SAVEFUAILED';
                    echo '<hr>';
                }
            }
            
        }
        
    }

    
    public function assUsersWithCompanies() {
        $companies = Company::all();
        $count = 0;
        foreach($companies as $company) {
            $count++;
            //dd($company);
            $userEmail = $company->owner_email;
            $user = User::where('email', $userEmail)->first();

            if($user != null) {
                
            
                if(!isset($user->companies) || $user->companies == '' || $user->companies == '0') {
                    echo $count.'<br>';
                    //d($user->companies);
                    $userCompany = new stdClass();
                    $userCompany->{$company->id} = "1";
                    $user->companies = json_encode($userCompany); //makes the user management
                } else {

                    $userCompanies = json_decode($user->companies);
                    $userCompanies->{$company->id} = "1"; 
                    $user->companies = json_encode($userCompanies);
                }

                if($user->save()) {
                    echo 'USer: '.$user->id.' '.$user->fname.' '.$user->lname.' was saved for<br>';
                } else {
                    echo 'bottom';
                }
            }
            
        }
    }
    
    public function makeUsers() {
        dd('not allowed');
        set_time_limit(0);
        $users = Importusers::all();
        foreach($users as $user) {
            $email = $user->email;
            $first_name = $user->fname;
            $last_name = $user->lname;
            $password = $user->password;
            $oldId = $user->old_id;
            $oldCompany = $user->old_company;
            //echo $oldCompany;
            $company = Importcompanies::getCompanyName($oldCompany);
            //dd($company);
            if($this->checkEmailForAccount($email) == null) {
                $userId = $this->createImportedUser($company, $first_name, $last_name, $email, $password, $oldId);
            } else {
                $account = $this->checkEmailForAccount($email);
                $userId = $this->updateImportedUser($account, $oldId, $company, $email);
            }
            echo 'id: '.$userId.'<br>';
        }
        echo 'done';
    }
    
    public function updateImportedUser($user, $oldId, $company, $email) {
        
        $user->old_id = $oldId;
        $user->pharmacy = $company;
        $user->email = $email;
        if ($user->save()) {
            //return user id to controller
            return $user->id;
        } else {
            return 'failed';
        }
    }
    
    public function checkEmailForAccount($email) {
        
        $user = User::where('email', $email)->first();
        if($user == null) {
            return;
        }
        return $user;
        
    }
    public function createImportedUser($company, $first_name, $last_name, $email, $password, $oldId) {
        
        $user = new User;
        $user->fname = ucwords($first_name);
        $user->lname = ucwords($last_name);
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->pharmacy = ucwords($company);
        $user->remember_token = '';
        $user->bio = '';
        $user->associated_data = '';
        $user->groups = '0:12';
        $user->old_id = $oldId;
        $user->imported = 1;
        $user->whitelisted_ip_list = 1;
        if ($user->save()) {
            //return user id to controller
            return $user->id;
        } else {
            return 'failed';
        }
    }

    public function updateOwnerUserRecords() {
        set_time_limit(0);
        Company::chunk(100, function($companies) {
            foreach($companies as $company) {
                $user = User::where('email', $company->owner_email)->first();
                if($user != null) {
                    $user->account_type = 1;
                    $user->employee_type = 1;
                    if($user->save()) {
                        echo 'SAVED '.$user->email.'<br>';
                    } else {
                        echo 'not saved dude<br>';
                    }
                    
                }
            }
        });
        $users = User::where('confirmed_user', '1')->where('companies', '!=', '0')->where('account_type', '0')->get();
        foreach($users as $weirdUser) {
            if(!preg_match('/pending/', $weirdUser->companies)) {
                $weirdUser->account_type = 1;
                $weirdUser->employee_type = 1;
                if($weirdUser->save()) {
                    echo 'saved for '.$weirdUser->email.'<br>';
                } else {
                    echo 'NOTE SAVED '.$weirdUser->email.'<br>';
                }
            }
        }
        
    }
    
    public function adminIdToCompanies() {
        set_time_limit(0);
        Company::chunk(100, function($companies) {
            foreach($companies as $company) {
                $user = User::where('email', $company->owner_email)->first();
                if($user != null) {
                    $company->admin_id = $user->id;
                    $employees = json_decode($company->employees, true);
                    $employees[$user->id] = '1';
                    $company->employees = json_encode($employees);
                    $company->save();
                    
                }
                echo $company->name.'<br>';
            }
        });
        echo 'done';
    }
    
    public function AddOldCompanyToUsers() {
        set_time_limit(0);
        Importusers::chunk(100, function($users) {
            foreach($users as $user) {
                $realUser = User::where('email', $user->email)->first();
                if($realUser != null) {
                    $realUser->old_company = $user->old_company;
                    if($realUser->save()) {
                        $company = Company::where('old_company_id', $realUser->old_company)->first();
                        if($company != null) {
                            Company::updateMember($realUser->id, $company->id, 1, true);
                            echo 'added'. $realUser->fname.'=>'.$company->name.'<br>';
                        }


                    }
                }
                
                
            }
        });
        echo 'DONE<hr>DONE<hr>';
    }
    
    public function addNotificationsForAllUsers() {
        $users = User::all();
        foreach($users as $user) {
            
            $hasNotifications = Notifications::userHasNotificationsRow($user->id);
            if($user->confirmed_user == 1) {
                echo $user->id.'<br>';
                echo 'is confirmed<br>';
                $notifications = Notifications::where('user_id', $user->id)->first();
                if($notifications == null) {
                    echo 'creating new<br>';
                    $new = new Notifications();
                    $new->user_id = $user->id;
                    $new->digest = 1;
                    $new->topics = '';
                    $new->resources = '';
                    $new->events = '';
                    if($new->save()) {
                        echo 'saved<br>';
                    }
                echo 'notsaved<br>';
                    
                }
            }
            
        }
        
    }
    
    public function getPayingMembers() {
        User::chunk(100, function($users) {
            foreach($users as $user) {
                $groups = explode(':', $user->groups);
                $admin = in_array('1', $groups) || in_array('5', $groups) ? ',admin' : '';
              
                if(in_array('2', $groups) && $user->confirmed_user == 1) {
                    //echo $user->id.','.$user->fname.','.$user->lname.','.$user->email.',member'.$admin.'<br>';
                } else {
                    if($user->confirmed_user == 1) {
                        echo $user->id.','.$user->fname.','.$user->lname.','.$user->email.',non-member'.$admin.'<br>';
                    }
                    
                }
                
            }
        });
    }
    
}
