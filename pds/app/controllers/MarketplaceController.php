<?php

use MarketRepo\temp\Vendors\VendorRepository as MarketRepository;
use MarketRepo\production\Vendors\PdsVendorRepository as RealMarketRepository;

class MarketplaceController extends \BaseController {

    public function __construct(MarketRepository $vendors, RealMarketRepository $pds_vendors) {
        $this->vendors = $vendors;
        $this->new_vendors = $pds_vendors;
        $this->page = 'Marketplace';
        View::share('page', $this->page);
        $css = [ 'main', 'market'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Vendor Marketplace | PDSadvantage');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $all_vendors = $this->vendors->all();
        $filtered_results = array();
        $categories = $this->vendors->getOldCategories();
        $subCategoriesArray = $this->vendors->getSubCategories();

        return View::make('Marketplace.index-main', compact('results', 'filtered_results', 'all_vendors', 'categories'));
    }

    public function indexCompany($company_id, $company_name) {
        $company = preg_replace('/-/', ' ', $company_name);
        $results = $this->vendors->getByCompanyId($company_id);
        $categories = $this->vendors->getOldCategories();
        return View::make('Marketplace.company', compact('company', 'results', 'categories'));
    }

    public function indexNewCompany($company_id, $company_name) {
        $company = preg_replace('/-/', ' ', $company_name);
        $results = $this->new_vendors->getByCompanyId($company_id);
        $categories = $this->vendors->getOldCategories();
        return View::make('Marketplace.pds_company', compact('company', 'results', 'categories'));
    }

    public function getFilteredListing() {
        $all_vendors = $this->vendors->all();
        $real_cat = Input::get('temp_filter');
        $categories = $this->vendors->getOldCategories();
        $filtered_results = $this->vendors->getAllByCategory($real_cat);

        return View::make('Marketplace.index', compact('filtered_results', 'all_vendors', 'real_cat', 'categories'));
    }

    

    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $vendors = '{ "vendors" : [ { "vendor" : { "contact_info" : { "href" : "wayne.boese@rxaap.com",
                "text" : "American Associated Pharmacies<br>Vice President of Sales: Wayne Boese<br>Tel: 877.797.9227 ext. 240<br>Email: wayne.boese@rxaap.com<br>Website: rxaap.com <br>Address: 211 Lonnie E. Crawford Blvd Scottsboro, AL 35767"
              },
            "description" : "American Associated Pharmacies (AAP) is a national member-owned and directed cooperative of 2,000+ independent pharmacies working together like a “chain.” Members benefit from two wholly-owned AAP subsidiaries. United Drugs is a managed care program that negotiates competitive managed-care contracts and related services for independent pharmacies. Associated Pharmacies, Inc. (API) is a member-owned warehouse that provides customers with savings on brand Rx, generic Rx, and select OTC products. The rapid growth of AAP since its founding in 2009 has led to development of numerous beneficial programs, favorable contracts and competitive pricing that have produced substantial increases in member/owner rebates and dividends.",
            "logo" : { "alt" : "Battle Creek Equipment",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/aap%20logo_rgb.jpg?1347379075.82378",
                "text" : ""
              },
            "vendor" : "American Associated Pharmacies",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.pharmacyowners.com/marketplace/anx/tel:248447-4050",
                "text" : "ANX<br>Contact: Mark Wayne<br>Tel: 248-447-4050<br>Email: waynem@anx.com<br>Website: www.anx.com<br>Address: 2000 Town Center Ste. 2050 Southfield, MI 48075"
              },
            "description" : "ANX helps pharmacies reduce the risk of credit card theft for less than $1 per day. ANX customers receive $100k of data breach protection plus easy to use tools, support and security technology to make PCI compliance easier and faster.  The Payment Card Industry Data Security Council has certified ANX as a Qualified Security Assessor (QSA) and Approved Scanning Vendor (ASV).  Thousands of worldwide locations trust ANX to secure their information.",
            "logo" : { "alt" : "ANX",
                "href" : "http://www.anx.com/",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/ANXlogo-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "ANX",
            "years" : "2013, 2014 Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "fahertyt@arkrayusa.com",
                "text" : "Arkray<br>Terry Fahertyfahertyt@arkrayusa.com(952)646-2909<br>Address:5198 W 76th StreetEdina, MN 55439<br>Website:www.ArkrayUSA.com"
              },
            "description" : "Trust comes with experience and when you work with ARKRAY, you are working with a company that has devoted more than half a century to creating solutions that streamline diabetes care. As the 5th leading blood glucose manufacturer in the diabetes industry, ARKRAY offers customers a complete diabetes solution for testing and management. We strive to improve patient outcomes, lower overall cost of care and provide cutting-edge reliable products. ARKRAY’s complete product line is cost effective and comes backed by 24/7 customer support and free value added products and programs for customer retention.",
            "logo" : { "alt" : "Arkray",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/arkray.jpg",
                "text" : ""
              },
            "vendor" : "ARKRAY",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://mhall@assuredpharma.com",
                "text" : "Assured Pharmaceuticals <br>Sales: Matthew Hall<br>Tel: 704.419.3005<br>Email: mhall@assuredpharma.com<br>Website: www.assuredpharmaceuticals.com<br>Address: PO Box 536 Gastonia, NC 28053"
              },
            "description" : "Assured Pharmaceuticals, being an Assured Waste Solutions Division, is a DEA registered pharmaceutical returns company for credit and destruction, and we are able to process all Schedule 1-5 drugs. We employ our in house pharmacy staff and Field Technicians whom are bonded and insured, and they’re required to pass a comprehensive background check and regular drug screenings. Assured Pharmaceuticals takes pride in the way we operate and most importantly we operate in the most environmentally friendly way possible. We have implemented green business practices such as our paperless 222 processing and incinerate to energy facility for waste disposal which is compliant with all federal and local regulations. Moreover, we are a diversity supplier and GSA Certified. Assured Pharmaceuticals takes much pride in our Client Relations team and the way they discover solutions according to the client’s needs, while maintaining a strong and professional relationship through communication. In addition, we have implemented the web-based controlled substance ordering system, which offers a cost-effective solution ensuring DEA compliance while eliminating the paper Form 222. Assured Pharmaceuticals is in full compliance with the DEA, EPA, FDA, DOT, OSHA, and all other federal and state agencies. We have very strict security practices in place, and it is one of the roots of our business practice. We have also taken an added precaution, which our client’s information is stored at one of the most secure data storage facilities located in Las Vegas, NV. Our SafeTrak Storage Program offers secure storage for all classes, including level 1.",
            "logo" : { "alt" : "Assured Pharmacueticals",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/assuredpharm%20logo.png?1357677403.91421",
                "text" : ""
              },
            "vendor" : "Assured Pharmaceuticals",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "steve.roberts.ateb.com",
                "text" : "Ateb<br>Sales Manager: Steve Roberts<br>Tel: 919.882.4927<br>Email: steve.roberts.ateb.com<br>Website:www.Ateb.com<br>Address: 2600 Summer Blvd Ste 158 Raleigh, NC 27616"
              },
            "description" : "If you are looking for ways to increase medication adherence, improve your pharmacy’s PDC Scores, and build patient loyalty... Ateb has the answer.",
            "logo" : { "alt" : "Ateb",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/ateb_logo.jpg",
                "text" : ""
              },
            "vendor" : "Ateb",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "AT Design Team is a German-based company, specialized in pharmacy redesign. More than 65 pharmacies in the USA have used the experience of architect Reiner Theis in the last years to improve existing pharmacies or to develop new buildings. AT Design Team delivers quality “Made in Germany” for affordable prices. All projects are presented in 3D and it is even possible to see videos, as if you are walking thru your new pharmacy. AT Design Team is creating wellness feelings and attractive shopping experiences, to increase front end and Rx sales.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=284044&moduleid=548095&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "AT Design Team"
          } },
      { "vendor" : { "contact_info" : { "href" : "Mary.Brown@BattleCreekEquipment.com",
                "text" : "Battle Creek Equipment<br>Sales Manager: Mary Brown<br>Tel: 800.253.0854<br>Email Mary Brown<br>Website: BattleCreekEquipment.com <br>Address: 307 W. Jackson St Battle Creek, MI 49037"
              },
            "description" : { "href" : "http://www.battlecreekequipment.com/",
                "text" : "Battle Creek is known for serving the Physical Therapy and Rehab market since 1930 with our flagship Thermophore® moist heat therapy line for home or clinic use. Companion Ice It!® cold therapy and Pedlar® Light Exerciser styles, including one designed specifically for professional use, are also popular with therapists and consumers. New Good2Go™ microwave moist heat therapy products provide portable heat therapy for home or clinical use. Ask about our BedWarmer™ to improve the sleep of clients with cold feet or poor circulation with gentle all-night warmth. Please come by and see us! www.BattleCreekEquipment.com"
              },
            "logo" : { "alt" : "Battle Creek Equipment",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/battlecreek.jpg",
                "text" : ""
              },
            "vendor" : "Battle Creek",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.Bell-Horn.com",
                "text" : "Bell-Horn<br>Tel: 800.226.4799<br>Website:www.Bell-Horn.com"
              },
            "description" : "Bell-Horn is a manufacturer and distributor of a full line of retail orthopedic braces, supports and compression hosiery.",
            "logo" : { "alt" : "Bell-Horn",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/bellhorn.png",
                "text" : ""
              },
            "vendor" : "Bell-Horn",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "patrick.mcgrath@berlinger.ch",
                "text" : "Berlinger, USA<br>Tel: 508-366-0084<br>Email:patrick.mcgrath@berlinger.ch<br>Website:www.Berlinger.com"
              },
            "description" : { "href" : "info@berlinger.com",
                "text" : "Our Fridge-tag®2 has been adopted by the state boards of health in MA, VA, TN, & NH as the preferred method for monitoring vaccine storage. The Fridge-tag®2 has a USB interface and removable external probe. The 60 DAY temperature report takes the pain out of pharmacy refrigerator monitoring – and it is inexpensive. If you are carrying vaccines in your pharmacy you should consider using the Fridge-tag®2 in order to meet the changing regulatory expectations. Data from the Fridge-tag®2 has saved your peers thousands of dollars in insurance claims when the power goes out - ask us how! See our booth at the conference or ask for an order form via info@berlinger.com Find us on the web: www.berlinger.com phone: 508-366-0084"
              },
            "logo" : { "alt" : "Berlinger",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/berlinger.jpg",
                "text" : ""
              },
            "vendor" : "Berlinger, USA",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "vjafari@pwcosmetics.com",
                "text" : "Bio Oil<br>Sales Contact:Vince Jafarivjafari@pwcosmetics.com949-297-9062Website:www.bio-oilusa.comAddress:75 Enterprise, Ste 300Aliso Viejo, CA 92656"
              },
            "description" : "Bio-Oil is a specialist skincare product that helps improve the appearance of scars, stretch marks, and uneven skin tone. Its advanced formulation, which contains the breakthrough ingredient PurCellin oil, also makes it highly effective for numerous other skin concerns including aging and dehydrated skin.",
            "logo" : { "alt" : "biooil resized 206",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/biooil-resized-206.jpg",
                "text" : ""
              },
            "vendor" : "Bio-Oil",
            "years" : "2014 PDS Conference  Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.BOCusa.org",
                "text" : "BOC<br>Tel: 877.776.2200<br>Website: www.BOCusa.org"
              },
            "description" : "You are committed to providing your patients with what they need to be successful. We at BOC understand your need for a credentialing partner that feels the same way about you.",
            "logo" : { "alt" : "BOC International",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/boc-international.png",
                "text" : ""
              },
            "vendor" : "BOC International",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Bionix Health at Home is the consumer division of Bionix Development Corporation, a manufacturer of innovative medical devices for nearly 30 years. Our products are designed to professional medical standards and are presently used by physician in offices and hospitals throughout the United States along with 50 additional countries worldwide.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276329&moduleid=542549&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Bionix Health At Home"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Do your customers ever ask you which teeth whitening product works best? Normally you would have to tell them to get the strongest whitening solution from their dentist. Great News! You now have the opportunity to sell the same BRUSHONSMILE® Teeth Whitening Pens that dental professionals carry.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283744&moduleid=546895&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "BrushOnSmile"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.cardinalhealth.com/communityindependentpharmacies",
                "text" : "Cardinal Health<br>Tel: 614.757.5000<br>Website: www.cardinalhealth.com/communityindependentpharmacies<br>Address 7000 Cardinal Place Dublin, OH 43017"
              },
            "description" : "Headquartered in Dublin, Ohio, Cardinal Health, Inc. is a health care services company that improves the cost-effectiveness of health care. Cardinal Health is an essential link in the health care supply chain, providing pharmaceuticals and medical products to more than 60,000 locations each day. Cardinal Health provides programs, products and services to pharmacies across the United States. From superior delivery and inventory controls, to marketing, franchising and Internet-based solutions, Cardinal Health offers a comprehensive array of business solutions to meet the needs of the community pharmacy.",
            "logo" : { "alt" : "Cardinal Health",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/cardinal%20logo.png?1358286509.22617.jpg",
                "text" : ""
              },
            "vendor" : "Cardinal Health",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Buy-Sellapharmacy.com is the only national company focused exclusively on the world of independent pharmacy owners. The company evaluates, appraises and, when requested, markets independent pharmacies it lists for sale to a national buyer database of more than 3,000 people. In the course of its thirteen year history, the company has successfully brokered the sale of more than 400 independent pharmacies and evaluated/appraised an additional 400 businesses. The company has a nine man team of industry professionals who work with our clients to achieve the best possible outcome in all areas of our expertise.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=284043&moduleid=548088&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Buy-SellaPharmacy.com"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.CardsnSuchOnline.com",
                "text" : "Cards N’ Such<br>Tel: 800.345.4887<br>Website:www.CardsnSuchOnline.com<br>Bryan RittmillerDirector of Sales6189758877 Mobile6189100410 Mobile 23032894887 Officebryanr@cardsnsuch.net"
              },
            "description" : { "href" : "http://www.cards-n-such.com/",
                "text" : "We are one of the largest greeting card distributors in the nation. Our specialty is our unique ability to supply retailers with the highest quality and best variety of products. Several different card programs are available to our retailers to suit their individual needs and clientele. These programs include a full retail program, or a variety of discount programs. We accommodate any size department, no matter how big or small, with all we can offer. We also offer a variety of ancillary product, including sunglasses, reading glasses, plush items, and gift wrap. Our pride comes from our commitment to service! www.cards-n-such.com"
              },
            "logo" : { "alt" : "Cards N Such",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/cardsnsuch.png",
                "text" : ""
              },
            "vendor" : "Cards N Such",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "sales@computer-rx.net  ",
                "text" : "Computer-Rx<br>Tel: 800.647.5288<br>Email: sales@computer-rx.net<br>Website: www.winRX.net<br>Address: 11825 S. Portland Oklahoma City, OK 73170"
              },
            "description" : "WinRx is integrated with the leading Pharmacy Automation, Robotics, and IVR systems including ScriptPro, AutoMed, Parata, Voice Tech, Ateb and Telemanager.",
            "logo" : { "alt" : "Computer Rx",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/computerrx.png",
                "text" : ""
              },
            "vendor" : "Computer-Rx",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Cortland Data created and administrates the RxCherryPick purchasing strategy. What does the RxCherryPick strategy do for pharmacy? - Increase cash flow. - Reestablish reimbursement profitability. - Reduce incoming sales calls. - Deliver the highest savings/rebate ratio in the industry. - Make life better! Check with a Cortland Data representative to see if your pharmacy qualifies to use the RxCherryPick strategy.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283743&moduleid=546888&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Cortland Data Corp"
          } },
      { "vendor" : { "contact_info" : { "href" : "jim@cpgbrokers.com",
                "text" : "CPG Brokers<br>Master Broker/ Consultant: Jim Embry<br>Tel: 407.350.1967 <br>Email: jim@cpgbrokers.com<br>Website: www.cpgbrokers.com<br>Address: PO Box 623487 Oveido, FL 32762"
              },
            "description" : "CPGBrokers builds strong relationships by providing strategic consulting and sales execution to consumer goods companies competing in health and beauty category and food segments in the United States, and Canada. CPGBrokers has a strong record of delivering successful plans and proven execution strategies for its client base of growing companies. Utilizing a combination of direct brokers and network of regional brokers, calling on key decision makers for all major chains across many trade channels such as Food, Drug, Mass, Clubs, Hardware/DIY, Military, Nutritional, Dollar Stores, with special focus on the Independent Wholesale Distributors.Key capabilities are:",
            "logo" : { "alt" : "CPG Broker 2 resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/CPG-Broker-2-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "CPG Brokers",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Cortland Data created and administrates the RxCherryPick purchasing strategy. What does the RxCherryPick strategy do for pharmacy? - Increase cash flow. - Reestablish reimbursement profitability. - Reduce incoming sales calls. - Deliver the highest savings/rebate ratio in the industry. - Make life better! Check with a Cortland Data representative to see if your pharmacy qualifies to use the RxCherryPick strategy.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283743&moduleid=546888&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Cortland Data Corp"
          } },
      { "vendor" : { "contact_info" : { "href" : "Lou.Cerritelli@DesignerGreetings.com",
                "text" : "Designer Greetings<br>Company Contact:Lou CerritelliLou.Cerritelli@DesignerGreetings.com800-654-6960Website:www.DesignerGreetings.comAddress:11 Executive AvenueEdison, NJ 08817"
              },
            "description" : "As an award-winning, family-owned company, Designer Greetings takes great pride in producing one of the most extensive greeting card lines in the United States – over 21,000 cards of outstanding quality, value and selection. Our everyday counter card offering spans over 160 feet and we have a full selection of greeting cards for all seasons. Styles ranging from traditional to alternative are sure to move and amuse.",
            "logo" : { "alt" : "designer greeting resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/designer greeting-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Designer Greetings",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283896&moduleid=547596&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Datarithm"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.DrComfort.com",
                "text" : "Dr. Comfort<br>Tel: 877.713.5175<br>Website:www.DrComfort.com"
              },
            "description" : "Dr. Comfort is the premier provider of diabetic footwear. Our shoes are the finest quality, designed for comfort, and come in a wide variety of beautiful colors and styles. Dr. Comfort also features an onsite state-of-the-art pedorthic laboratory—offering custom inserts, toe fillers and custom shoe modifications—as well as expert advice whenever you need it.",
            "logo" : { "alt" : "Dr. Comfort",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/dr-comfort-header.png",
                "text" : ""
              },
            "vendor" : "Dr. Comfort",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.emdeon.com/pharmacyservices ",
                "text" : "Emdeon <br>Vice President of Pharmacy Sales: Richard Brook<br>Tel: 877.300.0297<br>Email: rbrook@emdeon.com<br>Website: www.emdeon.com/pharmacyservices"
              },
            "description" : "Emdeon is a leading provider of electronic solutions to the pharmacy industry offering comprehensive and innovative solutions for claims management and analysis, ePrescribing clinical services and simplification of complex billing and processing issues such as Medicare/Medicaid DME billing and patient loyalty programs.",
            "logo" : { "alt" : "Emdeon",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/emdeon_logo_hires_large%20(1).jpg?1359755908.22801",
                "text" : ""
              },
            "vendor" : "Emdeon",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.FlavoRx.com",
                "text" : "FLAVORx<br>Tel: 800.884.5771<br>Website:www.FlavoRx.com"
              },
            "description" : "Are you looking for ways to bring more customers through your doors? What about keeping your customers happy so they would never consider switching to another pharmacy? FLAVORx can help you accomplish both. Pharmacies that promote the FLAVORx custom-flavoring service bring in, on average, 16 to 24 new customers each year and add $5,000 to $10,000 to their bottom-line just from flavoring medications. What’s more, new research indicates moms are far more satisfied with their pharmacy experience when they’ve had a child’s medication custom flavored. Don’t let another unflavored day go by. Add the FLAVORx service to your pharmacy today.",
            "logo" : { "alt" : "Flavorx",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/flavorx.png",
                "text" : ""
              },
            "vendor" : "FLAVORx",
            "years" : "2014 PDS Conference Bronze Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "rob@freedomrxinc.com",
                "text" : "Freedom Pharmaceuticals<br>Sales <br>Tel: 877.839.8547877.839.8547 <br>Email: info@freedomrxinc.com<br>Website: www.freedomrxinc.com<br>Address: 801 W. New Orleans St. Broken Arrow, OK 74011<br>Call<br>Send SMS<br>Add to Skype<br>You\"ll need Skype CreditFree via Skype"
              },
            "description" : "Freedom Pharmaceuticals, Inc. (“Freedom”) specializes in the supply and custom repackaging of fine compounding chemicals - active pharmaceutical ingredients (APIs) and excipients - to independent compounding pharmacies throughout the United States.At Freedom, our goals are simple: supply your compounding pharmacy with the highest quality fine chemicals available; consult with your pharmacy on proper third-party billing techniques; and, give your staff the personalized customer service necessary to operate successfully in this rapidly changing industry.",
            "logo" : { "alt" : "Mevesi",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/conference/2013%20vendors/freedom%20logo%20.jpg?1354294806.35276",
                "text" : ""
              },
            "vendor" : "Freedom Pharmaceuticals",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "rays@goldpromotions.com",
                "text" : "Gold Promotions<br>Contact:Raymond SobotkaEmail:rays@goldpromotions.comTel:614-876-7354<br>Website:www.goldpromotions.com"
              },
            "description" : "Gold Promotions is your family owned jewelry source. We specialize in fine jewelry. We are the first wholesale jeweler to sell specifically to professional groups. We sell diamonds, colored stones, fine watches, coins, gold, platinum and silver. We also offer unique fashion jewelry that your customers can enjoy for years. Stop by our booth to talk about the great opportunities that exist.",
            "logo" : { "alt" : "describe the image",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/gold promotions-resized-600.jpg",
                "text" : ""
              },
            "vendor" : "Gold Promotions",
            "years" : "2014 Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "HBSinfo@sxc.com",
                "text" : "Health Business Systems<br>Tel: 800.444.1427<br>Email HBSinfo@sxc.com<br>Website: HBSRx.com<br>Address: 738 Louis Dr. Warminster, PA 18974"
              },
            "description" : "The Smart Choice in Pharmacy Software! For over 30 years, HBS has been delivering next-generation technology to support all channels of pharmacy—retail (independent and chain), institutional/nursing home and mail order/central fill. For those looking for the latest in pharmacy software, RxGENESYS is the premier dispensing system setting new standards of excellence in the field of pharmacy practice.",
            "logo" : { "alt" : "Health Business Systems",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/conference/bgc13/exhibitors/hbslogo.jpg?1354034059.11988",
                "text" : ""
              },
            "vendor" : "Health Business Systems",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "tshannon@ideationgifts.com",
                "text" : "Ideation, Inc<br>Sales: Tim Shannon<br>Tel: 800-521-3088 ext 225<br>Email: tshannon@ideationgifts.com<br>Website: www.ideationgifts.com<br>Facebook:www.facebook.com/ideationgifts<br>Address: 2910 Huron ParkwayAnn Arbor, MI 48105"
              },
            "description" : "Ideation has been helping independent retailers throughout the country grow their business with our customized catalogs and mailing programs. We are the oldest and largest business of its kind in the industry.",
            "logo" : { "alt" : "ideation resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/ideation-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Ideation",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "We offer business insurance and professional liability insurance to independent pharmacy owners in all 50 states.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=284042&moduleid=548081&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Heffernan Insurance Brokers"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.flavma.com",
                "text" : "iMedicare<br>Chief Executive Officer: Flaviu Simihaian<br>Website: www.imedicare.com"
              },
            "description" : { "href" : "http://www.flavma.com/imedicare",
                "text" : "iMedicare is the pharmacy software firm that developed iMedicare, the most accurate and convenient Medicare Part D Plan finder. The firm provides software solutions tailored to independent pharmacies designed to improve their workflow and competitiveness in a rapidly evolving market by leveraging the latest in mobile technology. Their signature software has been used by pharmacists around the country to better service and inform their Medicare patients with less time in front of a computer, time that now goes to serving more customers. To learn more about iMedicare, visit www.imedicare.com"
              },
            "logo" : { "alt" : "Flavma",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/flavma.png?1359756454.48263",
                "text" : ""
              },
            "vendor" : "iMedicare",
            "years" : "2013, 2014 PDS Conference Exhibitor:"
          } },
      { "vendor" : { "contact_info" : { "href" : "pgrams@incrediwear.com",
                "text" : "Incrediwear<br>Sales: Patrick Grams<br>Tel: 530-345-5808<br>Email: pgrams@incrediwear.com<br>Website: www.BuyIncrediwear.com<br>Address: 3120 Thorntree Dr. Chico, CA 95973"
              },
            "description" : "Incrediwear is the only brace and sock line tested and proven to increase circulation by 12.1 to 22% at rest. Used by vascular and orthopedic surgeons, diabetic specialists, anesthesiologists and podiatrists to help treat patients suffering from Raynaud’s Syndrome, diabetes, neuropathy, carpel tunnel, plantar fasciitis, arthritis and tendonitis, cramps, joint swelling, paresthesia (pins and needles), etc. Incrediwear technology has been featured in Health Science Institute, Podiatry Today and Lower Extremity Review (LER). All braces have medical billing codes as well as laser Doppler studies which demonstrate the increase in blood flow that occurs while wearing them.",
            "logo" : { "alt" : "Incrediwear",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/incrediwear.jpg?1361908109.20406",
                "text" : ""
              },
            "vendor" : "Incrediwear",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "Member.Services@ipcrx.com",
                "text" : "Independent PharmacyCooperative<br>Tel: 800.755.1531<br>Email:Member.Services@ipcrx.com<br>Website:www.IPCrx.com"
              },
            "description" : { "href" : "http://www.ipcrx.com/",
                "text" : "IPC is the largest group purchasing organization for independent pharmacy with 4500 members. Proud to deliver profitable programs and substantial rebate opportunities for nearly 30 years, IPC takes you to the next level with incredible services and solutions for assuring your pharmacy’s success. Our vast array of purchasing opportunities include an Rx Advantage Program; OTC savings of cost minus 15%; and a Retail Merchandise offering. IPC contracts with 50+ vendors bringing a full range of programs to the membership. IPC’s Government Affairs division represents member interests with regard to state and national legislation. Members receive timely updates on pertinent issues.  www.ipcrx.com"
              },
            "logo" : { "alt" : "ipc resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/ipc-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Independent Pharmacy Cooperative",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "InfoWerks provides an unmatched suite of customized pharmacy data services that goes well beyond data conversions. Delivered with security that goes well beyond compliance. For example:",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276342&moduleid=542620&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Infowerks"
          } },
      { "vendor" : { "contact_info" : "2012, 2013, 2014 PDS Conference Exhibitor",
            "description" : "Isagenix Systems include:  Cellular Replenishing, Weightloss Healthy Aging and Energy and Performance. Our Organic systems offer Significant Weight, Fat loss and Inch loss, Energy, Fitness Performance,Less Stress, Youthful Aging.  Increased Health and Restful Sleep.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276079&moduleid=541611&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Isagenix"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.pharmacyowners.com/marketplace/keycentrix/tel:316.262.2231",
                "text" : "KeyCentrix<br>Sales TeamTel: 316.262.2231<br>Email: sales@keycentrix.com<br>Website: www.KeyCentrix.com<br>Address: 2420 N. Woodlawn Bldg 500 Wichita, KS 67220"
              },
            "logo" : { "alt" : "KeyCentrix",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/keycentrix.jpg",
                "text" : ""
              },
            "vendor" : "KeyCentrix",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.rxlps.com",
                "text" : "Lagniappe Pharmacy Services<br>Tel:<br>866.641.9032<br>Website:<br>www.rxlps.com"
              },
            "description" : "Lagniappe Pharmacy Services works to maximize your pharmacy’s efficiency and profitability while connecting you to other healthcare providers and services to provide positive patient outcomes. Our pharmacy systems offer Rx imaging, true FIFO inventory management, configurable workflow, fully integrated POS, nursing home/LTC with eMARs, DME billing, claims reconciliation services and much, much more. Using our industry leading patient portal, myPharmacyConnect, our users are able to extend the reach of their pharmacy and connect to their pharmacy customers using online profiles for easy refills, texting, e-mail, and voice messaging with all formats available in English, Spanish and other languages.",
            "logo" : { "alt" : "lagniappe resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/lagniappe-resized-165.png",
                "text" : ""
              },
            "vendor" : "Lagniappe Pharmacy Services",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "description" : "Pharmacies, like fingerprints, are all unique. With more than four decades in your industry, Kirby Lester certainly understands. We provide your pharmacy with more than just a one-size-fits-all box.",
            "logo" : { "alt" : "kirby lester resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/kirby lester-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Kirby Lester"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.LeventhalProductions.com",
                "text" : "Leventhal Productions<br>Tel: 314.997.7213<br>Website:www.LeventhalProductions.com"
              },
            "description" : { "href" : "http://www.leventhalproductions.com/",
                "text" : "Leventhal Productions is an advertising agency that “exclusively” works with independently owned pharmacies and cluster groups of pharmacies for the past 30-years. This full-service agency produces and writes television and radio commercials, outdoor billboard and printed pieces, website design and implementation, and negotiates extremely competitive media packages that includes “BONUS” commercials and “VALUE ADDED” promotions. Judy Leventhal has a long history in the pharmacy/broadcast advertising field. Ms. Leventhal designed and implemented the Leader Media program for Cardinal Health from 1999 – 2010. Leventhal Productions will get the job done and create the positive RESULTS you are looking for. Give us a chance, you won’t be disappointed.  www.leventhalproductions.com"
              },
            "logo" : { "alt" : "Leventhal Productions",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/leventhal.png",
                "text" : ""
              },
            "vendor" : "Leventhal Productions",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.KodakPictureKiosk.com",
                "text" : "Logix, Inc.<br>Tel: 800.465.6449<br>Website:www.KodakPictureKiosk.com"
              },
            "description" : "If you have ever considered a self serve photo kiosk for your store this is a great chance to evaluate the latest generation Kodak Picture Maker and even print your own pictures at the conference. You’ll be surprised at just how easy they are to use!",
            "logo" : { "alt" : "Logix Inc",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/logix.jpg",
                "text" : ""
              },
            "vendor" : "Logix Inc",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.BetterPharmacyHealth.com",
                "text" : "McKesson / HealthMart<br>Tel: (855) 625-7385<br>Website: www.BetterPharmacyHealth.com"
              },
            "description" : "McKesson brings everything together with Health Mart to help you compete and thrive with a comprehensive portfolio and common brand.",
            "logo" : { "alt" : "describe the image",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/Healthmart - McKesson-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "McKesson / Healthmart",
            "years" : "2012, 2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.BetterPharmacyHealth.com",
                "text" : "McKesson / HealthMart<br>Tel: (855) 625-7385<br>Website: www.BetterPharmacyHealth.com"
              },
            "description" : "McKesson brings everything together with Health Mart to help you compete and thrive with a comprehensive portfolio and common brand.",
            "logo" : { "alt" : "describe the image",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/Healthmart - McKesson-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "McKesson / Healthmart",
            "years" : "2012, 2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276206&moduleid=542154&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "MedHere Today"
          } },
      { "vendor" : { "contact_info" : { "href" : "jkmotga@msn.com",
                "text" : "Medicine-On-Time<br>Company Contact:John KalvelageEmail:jkmotga@msn.com Phone:800-722-8824 Website:www.medicine-on-time.comStreet Address:6 North Park Drive, Ste 100Hunt Valley, Maryland21030"
              },
            "logo" : { "alt" : "medicine on time resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/medicine on time-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Medicine-On-Time",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.MICROmerchantSys.com",
                "text" : "MICRO Merchant Systems <br>Tel: 516.408.3999 <br>Website: www.MICROmerchantSys.com"
              },
            "description" : "Increase Revenue and Profitability, Increase Efficiency, Reduce Dispensing Errors... These are the goals that we keep in mind while developing solutions for you.",
            "logo" : { "alt" : "Micro Merchant Systems",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/micro-merchant-systems.jpg",
                "text" : ""
              },
            "vendor" : "Micro Merchant Systems",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : { "href" : "http://www.medisca.com",
                "text" : "In only 20 years, MEDISCA has developed a solid reputation as an innovative and entrepreneurial company in the pharmaceutical compounding industry. MEDISCA is a leading FDA-registered supplier of quality pharmacy compounding products and offers excellent service to the North American, Australian and other international markets. We are dedicated to providing our clients with the highest quality compounding products, such as Active Pharmaceutical Ingredients (APIs), Bases, Equipment, Devices, Oils, Colors and Flavors, coupled with unparalleled service and competitive pricing. Certificates of Analysis (CofA), Safety Data Sheets (SDS), and Technical Specifications are available for all fine chemicals and APIs at www.medisca.com or upon request. Furthermore, our three distribution centers (Plattsburgh, NY, Las Vegas, NV, and Dallas, TX) are fully stocked to support your compounding needs and serve you better.   Our trusted educational partner MEDISCA NETWORK offers pharmacy compounding training programs."
              },
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283730&moduleid=546826&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "MEDISCA"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Minnesota Independent Cooperative, Inc. was founded nearly a decade ago as an “Independent for Independents” – we provide independent pharmacies a cost-saving option to their primary wholesaler. In true cooperative fashion, there are no contracts, fees or minimums – you have the freedom to order what you need, when you need it. We combine cost efficiencies and apply cutting-edge intelligence to lower your costs and increase your profits. Operating in a state-of-the-art facility, our security and environmental control measures are based on best practices and published VAWD standards that ensure drug integrity and supply chain security.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276325&moduleid=542529&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Minnesota Independent Cooperative"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.Natural-Creations.net",
                "text" : "Natural Creations<br>Tel: 877.647.1601<br>Website:www.Natural-Creations.net"
              },
            "description" : "Integrated approach to wellness utilizing nutraceuticals, homeopathics, and botanicals.",
            "logo" : { "alt" : "Natural Creations",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/naturalcreations.png",
                "text" : ""
              },
            "vendor" : "Natural Creations",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "MTM Videos is an international leader in the pharmacy services field with an expertise in increasing patient adherence. MTM Videos provides over 200 Patient Education Videos that support pharmacists and other healthcare professionals in educating their patients, developing marketing services and increasing profits.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276046&moduleid=541457&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "MTM Videos"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://mluu@naturalhealthbrands.net",
                "text" : "Natural Health Remedies LLC<br>Sales: Martino D. Luu R.Ph<br>Tel: 609.914.5345<br>Email: mluu@naturalhealthbrands.net<br>Website: www.buyreplenishvitamins.com<br>Address: 2 Midbridge Drive Medford, NJ 08055"
              },
            "description" : { "href" : "http://www.buyreplenishvitamins.com",
                "text" : "Introducing Replenish brand multi Vitamins, clinically formulated to address Drug Induced Nutrient Depletion (DIND) in the following drug categories: Statins, PPI/H2 , Anti-hypertensives, Oral Hypoglycemics, Oral Contraceptives , Anti-Convulsants, and Anti-Inflammatories (corticosteroids and NSAIDs). As a trusted healthcare provider, your patients rely on you to educate them on the potential serious health implications associated with DIND. Our clinical team, made up of a Doctor of Pharmacy, Medical Doctor and Naturopathic physician, have created specific formulations targeting DIND in complete (1 pill per day) multivitamins . As a small business owner, we understand the need to find effective products that can support a healthy bottom line. With a 50% gross margin, Replenish brand will provide a good return on your investment and help grow your business. Our entire product line is made in the USA under certified GMP conditions and has the backing of our strong clinical team. With the Replensh brand you can feel confidant you are in providing a high quality, cost effective solution (24.95 for 60 day supply) to your patients. Replenish is not available in large retailers or chains, we are committed to the small business owner. Find out what other Independent healthcare providers already know! “REPLENISH provides me the product that I can not only recommend with confidence, but it has an excellent profit margin and I am assured of the repeat sales due to the exclusivity to independent pharmacies. It’s a great product for both the patient and Pharmacy.” Visit us at www.buyreplenishvitamins.com"
              },
            "logo" : { "alt" : "Natural Health Remedies",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/conference/2013%20vendors/natural%20remedies%20logo.jpg?1354293413.73251",
                "text" : ""
              },
            "vendor" : "Natural Health Remedies LLC",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=cstredney@naturalpartners.com",
                "text" : "Natural Partners<br>Director of Sales: Paul Phillips<br>Tel: 480.368.9355 x110<br>Email: pphillips@naturalpartners.com<br>Website: www.naturalpartners.com<br>Address: 8445 E Hartfort Drive Scottsdale, AZ 85255"
              },
            "description" : "Natural Partners provides a convenient way for health-care practitioners to better serve their patients. Since 1995, we have operated as a one-stop shop, supplying the alternative health care community with over 13,000 professional-grade natural supplements and products from over 115 trusted brands This ensures patients get the care they deserve from a professional they trust. With 2-day delivery anywhere in the US, we carry exclusive vendors and trusted brands including: Thorne Research, Bezwecken, Professional Complementary Health Formulas, Integrative Therapeutics, Pure Encapsulations, Vital Nutrients and more",
            "logo" : { "alt" : "Natural Partners",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/np_logo%20(1).jpg?1356621149.70574",
                "text" : ""
              },
            "vendor" : "Natural Partners",
            "years" : "2013, 2014 PDSConference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "kapwah@mhainc.com",
                "text" : "Net-Rx<br>Contact:Paul Butler(866) 336-3879 kapwah@mhainc.com<br>Website: www.Net-Rx.com"
              },
            "description" : "Net-Rx provides operational services exclusively for pharmacies in the Long Term Care, Home Infusion, and Retail classes of trade, helping these organizations lower operational costs, increase profitability and provide industry specific solutions.",
            "logo" : { "alt" : "Net-Rx",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/netrx.jpg",
                "text" : ""
              },
            "vendor" : "Net-Rx",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "prosales@nordicnaturals.com",
                "text" : "Nordic Naturals<br>Tel: 800-662-2544<br>Email: prosales@nordicnaturals.com<br>Website: www.nordicnaturals.com<br>Address: 111 Jennings DriveWatsonville, CA 95076"
              },
            "description" : "Since 1995, Nordic Naturals has been an industry leader in omega-3 fish oil supplementation, setting standards for purity, freshness, taste, and sustainability. Our firm policy has always been to harvest 100% of our fish in compliance with the Norwegian fisheries management system, which has been a model for the sustainable harvest of marine life for over 30 years.",
            "logo" : { "alt" : "nordic naturals resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/nordic naturals-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Nordic Naturals",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://meredith_sweeney@pack4u.com",
                "text" : "Pack4U<br>Director, Strategic Accounts: Meredith Sweeney<br>Tel: 855.472.2548 <br>Email: meredith_sweeney@pack4u.com<br>Website: www.Pack4U.com"
              },
            "description" : "Pack4U offers pharmacy and provider solutions, including a fully automated, state-of the art central refill operation in Orlando, FL, making outsourced packaging, Catalyst oneMARTM and a team of experts available to community pharmacies. These Pack4U pharmacy customers form a Pharmacy Network, each offering safe, standardized medication practices including 7-day delivery of multi-dose packaging and an industry leading electronic MAR, oneMAR. For the first time, facilities have a new option for organization-wide pharmacy delivery: choose the Pack4U Pharmacy Network, unified through technology and dedicated to extraordinary service in local communities.",
            "logo" : { "alt" : "Pack4U",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/pack4u_logo_rgb.jpg?1355757619.9071",
                "text" : ""
              },
            "vendor" : "Pack4U",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "NOVA is a leading innovator and manufacturer of Mobility, Bathroom, and Independent Living products including canes, rolling walkers, transport chairs, and mobility accessories. NOVA is also the creator of HME180, the most comprehensive and effective retail program in the industry.  Since 1993, NOVA has grown organically with a strong foundation and values of service, quality and integrity. NOVA’s mission is to provide superior products with great function and fashion for people to live a healthy, independent, and beautiful lifestyle. Our commitment is represented by our extraordinary staff, excellent warranty, education, innovation, and charitable contributions globally.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=284045&moduleid=548102&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Nova Medical Products"
          } },
      { "vendor" : { "contact_info" : { "href" : "success@parata.com",
                "text" : "Parata Systems<br>Contact:<br>Jay Blandford888-727-2821Email:success@parata.com<br>Website: www.Parata.com"
              },
            "description" : "Parata is for pharmacists who want to grow their business, Parata provides technology solutions to support coordinated care, better health outcomes and lower costs.",
            "logo" : { "alt" : "Parata Systems",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/parata_logo.jpg",
                "text" : ""
              },
            "vendor" : "Parata Systems",
            "years" : "2013, 2014 PDS Conference Presenting & Event Sponsor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.freedomdata.com ",
                "text" : "PDX Freedom Data Systems<br>Director of Programming: Paul Dyas<br>Tel: 800.624.1745 <br>Email: pdyas@pc1systems.com<br>Website: www.freedomdata.com www.pdxinc.com www.pc1systems.com"
              },
            "description" : "Over 10,000 independent and chain pharmacies across the U.S. have benefitted from technologies provided by PDX and affiliates NHIN, Rx.com, pc I professional systems, and Freedom Data Systems.† Built on over 30 years of commitment to excellence in retail pharmacy and healthcare, these companies offer an unparalleled suite of solutions for pharmacies of any size, in any market. Offerings include the advanced Enterprise Pharmacy System, a national Electronic Pharmacy Record, and SIGIS-certified POS solutions along with leading-edge integrations and services such as store-based mail order and Central Fill; AR reconciliation, contract management, and chasing claims services; and a pharmacy-friendly PBM.",
            "logo" : { "alt" : "PDX Freedom Data Systems",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/pdx%20company%20%20logos.jpg?1358450434.10885",
                "text" : ""
              },
            "vendor" : "PDX Freedom Data Systems",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "charlene@penetran.com  ",
                "text" : "Penetran Plus & Ocean Blue<br>Sales: Charlene Sancilio<br>Email: charlene@penetran.com<br>Tel: 800.282.5511 EXT. 104<br>Website: www.Penetran.com<br>Address: 521 Northlake Blvd North Palm Beach, FL 33408"
              },
            "description" : "Penetran Products is commited to offering products which are extraordinary, unique and not \"Me Too\" products that are common in most stores. We specialize in servicing Independent Pharmacies and Health Care Professionals. All are products are Made in USA.",
            "logo" : { "alt" : "Penetran",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/penetran.png",
                "text" : ""
              },
            "vendor" : "Penetran Plus & Ocean Blue",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "PharmaSmart<br>Tel: 800.781.0323<br>Website:www.Pharma-Smart.com",
            "description" : { "href" : "http://www.pharma-smart.com/",
                "text" : "PharmaSmart designs, manufactures, and distributes innovative blood pressure screening systems to assist in the detection and management of hypertension. Our pharmacy focused blood pressure program provides remote access solutions that measure key data and trigger instant service diagnostics. Theblood pressure readings can be uploaded to a secure online blood pressure tracker and are compatible with emerging patient and electronic health records. Our Hypertension Management Smart Card increases patient traffic and builds profit for the pharmacy. Our Blood Pressure systems safely and accuratelyadminister over 65 million BP tests each year. www.pharma-smart.com"
              },
            "logo" : { "alt" : "PSLogo resized 165",
                "href" : "http://www.pharma-smart.com/",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/PSLogo-resized-165.gif",
                "text" : ""
              },
            "vendor" : "PharmaSmart",
            "years" : "2013 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "theresa.rodriguez@pharmacyautomationsupplies.com",
                "text" : "Pharmacy Automation Supplies<br>Contact:Theresa Rodriguez<br>Tel: 800-798-1401<br>Email:theresa.rodriguez@pharmacyautomationsupplies.com<br>Website: http://www.pharmacyautomationsupplies.com<br>Address:146 S. Pinnacle DrRomeoville, IL 60446"
              },
            "description" : "Rexam is a leading global consumer packaging company. Rexam Prescription Products Inc. sets the standard for providing pharmacy with traditional and innovative prescription packaging solutions. Our regulatory correct products have superior quality, and we’re the preferred prescription packaging manufacturer by robotic automation companies. Rexam is proud to be partnered with Pharmacy Automation Supplies (PAS).",
            "logo" : { "alt" : "phcy automation supplies resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/phcy automation supplies-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Pharmacy Automation Supplies",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "dswanson@pharmacyfirst.com",
                "text" : "Pharmacy First<br>Contact:Dalia Swanson<br>Tel: 913-661-0298 ext. 118<br>Email:dswanson@pharmacyfirst.com<br>Website: www.PharmacyFirst.com<br>Address:11880 College Blvd Suite 420Overland Park, KS 66210"
              },
            "description" : "Pharmacy First, a network of over 6,000 independent pharmacies, is dedicated to aiding the independent pharmacy in their battle to remain competitive and profitable in today’s environment. Through our comprehensive menu of services including, but not limited to, contracting assistance, central payment and direct payment reconciliation, and network services, we are working to put money back in the independents pocket. By joining our contracting assistance program, pharmacies automatically gain access to 70+ payor contracts. Our expansive pharmacy network combined with our experience has given us the ability to enhance 100% of our PBM agreements and 82% of our addendums. Pharmacies also have the choice to join our reconciliation and recovery program knowing that we are watching their every penny.",
            "logo" : { "alt" : "Pharmacy First",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/pharmacyfirst_highres_pflogo.jpg?1358450330.76251",
                "text" : ""
              },
            "vendor" : "Pharmacy First",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "sales@pioneerrx.com",
                "text" : "PioneerRx<br>Tel: 800.850.5111<br>Email: sales@pioneerrx.com<br>Website: www.PioneerRx.com<br>Address: 408 Kay Lane Shreveport, LA 71115"
              },
            "description" : "With the experience of over 35 years of pharmacy software development and support, PioneerRx was built from the ground up to provide the pharmacy community with a solution that can grow as the industry changes for another 35 years. Our features are designed to provide speed, simplicity, consistency and flexibility. However, our ultimate goal of development is to help pharmacies make more money and have more fun.",
            "logo" : { "alt" : "Pioneer Rx",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/pioneer.jpg",
                "text" : ""
              },
            "vendor" : "Pioneer Rx",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.PPOK.com",
                "text" : "RxSelect Pharmacy Services, PPOK<br>Tel: 877.557.5707<br>Website:www.PPOK.com"
              },
            "description" : { "href" : "https://www.ppok.com/",
                "text" : "RxSelect Pharmacy Services, powered by PPOk, offers the most comprehensive suite of solutions that are focused for and managed by retail pharmacy, including, Third Party Contracting Services, Central Payment, Claims Reconciliation, RxLinc Claims Transmission, Contract Compliance Monitoring, MAC Evaluations, RxProtect Audit Services and Buying Group Programs for over 1900 pharmacies. PPOk is a membership owned Not-for-Profit corporation whose sole purpose is to ensure the viability, success, and competitiveness of its participating pharmacies. www.ppok.com"
              },
            "logo" : { "alt" : "RxSelect Pharmacy Network",
                "href" : "https://www.ppok.com/",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/RxSelect Pharmacy Network-resized-165.png",
                "text" : ""
              },
            "vendor" : "RxSelect Pharmacy Services, powered by PPOk",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "ripper@prescriptiongenerator.com",
                "text" : "Prescription Generator<br>Contact: Kelsey Ripper<br>Tel: 888.476.1337X3<br>Email: ripper@prescriptiongenerator.com<br>Website: www.PrescriptionGenerator.com"
              },
            "description" : "Our mission is simple, affordable and easy to execute! We are dedicated to the success and profitability of privately owned pharmacies throughout the country. Prescription Generator consistently exposes independent pharmacies to their local community, while uncovering the hidden opportunities within it. By confirming new interest, we are simultaneously re-engaging local businesses, acting as a proactive entity within the community, and eliminating the added expenses of a sales employee! The RxGen Team caters to the pharmacies every need throughout the entire process. Our services highlight the pharmacy and enhance community ties. We capture the essence of being a tight knit community on the verge of flourishing.",
            "logo" : { "alt" : "",
                "src" : "",
                "text" : ""
              },
            "vendor" : "Prescription Generator",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "How long have you been thinking about addressing adherence and loyalty? What have you done to address it?",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283742&moduleid=546881&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Prescribe Wellness"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.Statinzyme.com",
                "text" : "Statinzyme<br>Tel: 800.56.VITAMIN<br>Website:www.Statinzyme.com"
              },
            "description" : { "href" : "http://www.statinzyme.com/",
                "text" : "Prescription Vitamins, LLC presents Statinzyme! Statinzyme is an advanced supplemental formula that was developed by a pharmacist to help prevent the harmful side effects of cholesterol-lowering medications. Over 33 million Americans take prescription statins. For most independent pharmacies, that means 50% of your customer base takes a prescription statin medication, and that represents an enormous potential revenue stream for complementary products. Statinzyme contains 9 different scientifically proven ingredients all in one serving. If purchased separately it would cost the patient over $100. Statinzyme gives patients an amazing value while protecting their liver, heart, muscles, and brain. It quickly and efficiently gives the pharmacy an opportunity to solidify and enhance their relationship with their patients and simultaneously increase your profits by 400% on every statin prescription you fill. www.statinzyme.com"
              },
            "logo" : { "alt" : "Prescription Vitamins LLC",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/statinzyme.png",
                "text" : ""
              },
            "vendor" : "Prescription Vitamins, LLC",
            "years" : "2012, 2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "mike@popus.com",
                "text" : "Prince of Peace<br>Sales: Michael JarrettEmail:mike@popus.comTel:510.887.1899Website:popus.comStreet Address:3536 Arden RoadHayward, CA 94545"
              },
            "description" : "With over 100 years of proven success, Tiger Balm\"s world renowned Ultra Strength is the #1 Ointment in the category. It is a versatile external medication that provides fast effective relief for bodily aches and pains.",
            "logo" : { "alt" : "prince of peace resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/prince of peace-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Prince of Peace",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.QCPmeds.com",
                "text" : "QCP - Quality CareProductsTel: 800.337.8603 www.QCPmeds.com<br>."
              },
            "description" : "Quality Care Products, LLC (QCP) offers innovative pharmaceutical repacking for unit of use and unit dose for pharmacists, hospitals, and doctors. We are federally licensed by the FDA, DEA. In addition we utilize 6 Sigma principles in our operations to provide you the most efficient and effective packaging solutions available on the market today.",
            "logo" : { "alt" : "QCP",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/qcp.jpg",
                "text" : ""
              },
            "vendor" : "Quality Care Products",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "Jenny@rjhedges.com",
                "text" : "R.J. Hedges & Associates Sales Contact: Jenny Schell<br>Tel: 724.357.8380<br>Email: Jenny@rjhedges.com<br>Website:www.RJHedges.com<br>Address:978 Pumphouse Rd. PO Box H New Florence, PA 15944"
              },
            "description" : "R.J. Hedges & Associates is one of the nation’s leading compliance companies who offer complete and comprehensive healthcare compliance and accreditation services for independent pharmacies and medical supply companies throughout the United States. The programs that are developed are complete turnkey programs, which meet the ever-changing regulatory environment. All compliance programs are written for the specific facility and backed up with Computer Based Training and on-going support.",
            "logo" : { "alt" : "describe the image",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/rjhedges new-resized-600.png",
                "text" : ""
              },
            "vendor" : "R.J. Hedges & Associates",
            "years" : "PDS Conference Exhibitor: 2014, 2013, 2012"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.relayhealth.com ",
                "text" : "Relay Health<br>Tel: 888.743.8735<br>Website: www.relayhealth.com<br>Address:  1564 Northeast Expressway Atlanta, GA 30329-2010"
              },
            "description" : "RelayHealth manages the nation’s largest pharmacy network that helps pharmacies improve their financial and operational performance in four very unique ways. With RelayHealth solutions, pharmacies can:",
            "logo" : { "alt" : "RelayHealth",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/relayhealth-full-color-logo.jpg?1348610298.68893",
                "text" : ""
              },
            "vendor" : "RelayHealth",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "theresa.neal@repeatrewards.com",
                "text" : "Repeat Rewards<br>Company Contact:Theresa Nealtheresa.neal@repeatrewards.com952-974-1100 x111Street Address:6321 Bury Dr. STE19Eden Prairie, MN55346<br>Website:www.RepeatRewards.com"
              },
            "description" : "We know that getting customers in the door is only half the battle – you need to keep them coming back again! We offer a professional, well-managed loyalty rewards program that is proven to contribute greatly to retaining customers and increasing sales by encouraging repeat visits. We will work with you to create a program specifically designed for your business, we have no contracts, and you own the database!",
            "logo" : { "alt" : "Repeat Rewards resized 165",
                "href" : "http://www.repeatrewards.com/",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/Repeat Rewards-resized-165.jpeg",
                "text" : ""
              },
            "vendor" : "RepeatRewards",
            "years" : "2013 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "sales@rxmedic.com",
                "text" : "RxMedic<br>William Humphriessales@rxmedic.com8008823819Website:www.rxmedic.com"
              },
            "description" : "Based in Wake Forest, North Carolina, RxMedic provides leading edge dispensing technology to pharmacies. Each RxMedic product is strategically engineered to increase productivity and enhance patient safety while providing pharmacy staff more time for customer service. Offering automation from a table top unit up to a fully automated 256 cell dispensing robot, RxMedic has an affordable solution for every pharmacy. The RM64 is the newest in pharmacy automation from RxMedic that automates your top 64 drugs, filling up to five prescriptions a minute.",
            "logo" : { "alt" : "http://contact-form-uploads.s3.amazonaws.com/37772/cacf8ee7-8331-4d0a-a6cd-428af702a671/RxMedic%20Logo%20w%20Tag.JPG",
                "src" : "http://contact-form-uploads.s3.amazonaws.com/37772/cacf8ee7-8331-4d0a-a6cd-428af702a671/RxMedic%20Logo%20w%20Tag.JPG",
                "text" : ""
              },
            "vendor" : "RxMedic",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "info@rxnetusa.com",
                "text" : "Rx-Net<br>Sales: Chuck Cannata<br>Email: info@rxnetusa.com <br>Tel: 866-506-3044866-506-3044<br>Fax:913-766-9285913-766-9285<br>Website: www.rxnetusa.com <br>Call<br>Send SMS<br>Add to Skype<br>You\"ll need Skype CreditFree via Skype"
              },
            "logo" : { "alt" : "Rx net",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/Rx-net.jpg",
                "text" : ""
              },
            "vendor" : "Rx-Net",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "dmjensen@rxsystems.com  ",
                "text" : "Rx Systems, Inc.<br>Sales: Derek Jensen<br>Tel: 800.922.9142<br>Email: dmjensen@rxsystems.com<br>Website: www.RxSystems.com<br>Address: 121 Point W. Blvd St. Charles, MO 63301"
              },
            "description" : "Rx Systems, Inc. is a full-service provider of pharmacy packaging and supplies including labels, bags, vials, counter supplies, and LTC packaging supplies, plus the Eyecon pill counter. We offer the latest thermal and laser formats supported by all major software companies.  Custom bags only cost the average pharmacy an additional $10 per month over stock bags.  Review our assortment of vial closures. We save the average pharmacy 15-30% on vials. Custom logo caps are available.  If you want to try a pill counter that surpasses the competition, then try the Eyecon pill counter using the free two week trial.  You have to see it to believe it!",
            "logo" : { "alt" : "Rx Systems",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/rxsystems.jpg",
                "text" : ""
              },
            "vendor" : "Rx Systems, Inc.",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Our Pharmacy Planning Team contributes over 70 years of combined experience based on more than 2,000 pharmacy installations since 1973. Our goal is to incorporate the owner\"s business model and our expertise to develop a plan that maximizes profits, increases efficiency and improves overall customer service.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276326&moduleid=542536&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "RxPlanning Solutions - Div. of Display Options, Inc."
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.Rx30.com",
                "text" : "Rx30<br>Tel: 407.614.0050<br>Website:www.Rx30.com"
              },
            "description" : "The Rx30 Pharmacy System provides your pharmacy incomparable Prescription Filling, Nursing Home, Consulting, Accounts Receivable, Workflow Management, Signature Capture, IV processing, Compounding, Integrated ...The Rx30 Pharmacy System provides your pharmacy incomparable Prescription Filling, Nursing Home, Consulting, Accounts Receivable, Workflow Management, Signature Capture, IV processing, Compounding, Integrated POS Solutions and an abundance of value-added vendor interfaces to provide you a total turnkey dispensing solution. Whether you are a small independent start-up filling 30 prescriptions a day or a 100+ independent chain operation filling 1500 prescriptions a day.",
            "logo" : { "alt" : "Rx30",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/rx30.png",
                "text" : ""
              },
            "vendor" : "Rx30 Pharmacy System",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : " info@scimera.com",
                "text" : "Scimera BioScience<br>8250 NW 27th StreetSuite 306Doral, Florida 33122<br>Office: (305) 662-4065Toll Free: (855) 724-6372Fax: (305) 662-5519Email: info@scimera.com<br>Website: www.scimera.com"
              },
            "description" : "Scimera Bioscience® develops cutting-edge bioceuticals that address cardiovascular, immune system, bone and joint, gastrointestinal, respiratory, prostate, and a wide array of health issues. Our team is comprised of physicians, pharmacists, chemists, consumer advocates, and a management team with extensive clinical exposure and expertise – all with the goal of producing science-based health support. Scimera utilizes in-depth clinical studies, advanced ingredient research, and empirical data to promote a clearer understanding of the disease process and the importance of proactive intervention. All products are made in the USA in an FDA-inspected facility that meets the stringent standards of current Good Manufacturing Practices (GMP).",
            "logo" : { "alt" : "Scimera BioScience",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/logo&nutag.jpg?1361894906.63293",
                "text" : ""
              },
            "vendor" : "Scimera BioScience",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "description" : "Sancilio and Company, Inc. is a research based biopharmaceutical company focused on: Cardiovascular, Dental and Women\"s Health.",
            "logo" : { "alt" : "Sancilio",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/sancilio.png",
                "text" : ""
              },
            "vendor" : "Sancilio & Company, Inc.",
            "years" : "2013 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "clucia@sevonutraceuticals.com",
                "text" : "Sevo Nutraceuticals  <br>Christi Luciaclucia@sevonutraceuticals.com<br>Office: 508-755-7257Fax: 508-519-7770<br>Website: www.thinkPERCEPTIV.com<br>www.sevonutraceuticals.com"
              },
            "description" : "Sevo Nutraceuticals is in the business of licensing and commercializing innovative, science-backed nutraceutical compounds developed at research institutions and elsewhere. Sevo’s first product PERCEPTIV™, is a patented nutraceutical formulation for improving cognitive health.  Sevo licensed the formula from the University of Massachusetts, Lowell in 2012 and brought PERCEPTIV to market  later that year.  PERCEPTIV’S formula has shown proven efficacy in seven independent clinical studies which have been published in scientific journals and presented at the Alzheimer\"s Association International Conferences in both Paris and Boston.",
            "logo" : { "alt" : "Sevo Nutraceuticals",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/sevo%20pharmaceuticals%20logo%20.jpg?1358434498.91542",
                "text" : ""
              },
            "vendor" : "Sevo Nutraceuticals",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "cbsexton@sextonsinventory.com  ",
                "text" : "Sexton & Associates, Inc.<br>Sales: Charles Sexton<br>Tel: 412.719.5606<br>Email: cbsexton@sextonsinventory.com<br>Website: www.sextonsinventory.com<br>Address: PO Box 491 Dover, OH 44622"
              },
            "description" : "Specializing in Return On Investment for Independent Pharmacies Year Endings—Insurance—Buy & Sell",
            "logo" : { "alt" : "Sexton and Associates",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/Sexton&AssocLogo-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Sextons & Associates, Inc",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "hmartin@softwriters.com",
                "text" : "SoftWriters<br>Company Contact: Heather Martinhmartin@softwriters.com412-492-9841Website:www.softwriters.comAddress:3960 William Flynn HwyAllison Park, PA 15101"
              },
            "logo" : { "alt" : "softwriters resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/softwriters-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "SoftWriters",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "sstahmer@soundworldsolutions.com",
                "text" : "Sound World Solutions<br>Company Contact:Shawn Stahmersstahmer@soundworldsolutions.com847-939-6101Website:www.soundworldsolutions.comAddress:960 N Northwest HwyPark Ridge, IL60068"
              },
            "description" : "If you\"ve been looking for an innovative, high-margin new product for the front of your store that allows you to help your customers while also providing a dramatic impact on your bottom line, take a look at Sound World Solutions. Our focus is on helping people rediscover the power of connection, recognizing that sound and clarity are an integral part of human interaction.",
            "logo" : { "alt" : "SWS logo FINAL (2) resized 165",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/SWS logo FINAL (2)-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Sound World Solutions",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "wseiler@srspharmacy.com  ",
                "text" : "SRS Pharmacy Systems<br>Sales: Wayne Seiler<br>Tel: 800.673.6226 ext 207<br>Email: wseiler@srspharmacy.com<br>Website: www.srspharmacy.com"
              },
            "description" : "For over 22 years, SRS Pharmacy Systems has been providing an integrated pharmacy management solution to independent community pharmacies and small chains with 98% customer loyalty. At SRS, our Linux-based system is designed to help make your business operate more efficiently and accurately, to keep your processes consistent, your inventories low and your profitability high.",
            "logo" : { "alt" : "SRS Pharmacy Systems",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/logo_srs_web.jpg?1347288744.54084",
                "text" : ""
              },
            "vendor" : "SRS Pharmacy Systems",
            "years" : "2012, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "ollin@sykes-cpa.com",
                "text" : "Sykes & Company, P.A. <br>Contact:Olin Sykesollin@sykes-cpa.com252-482-7644<br>Website: www.Sykes-cpa.com"
              },
            "description" : "For over 35 years, the accountants at Sykes & Company, P.A. have been helping independent pharmacies grow and thrive.",
            "logo" : { "alt" : "sykes2014 resized 165",
                "href" : "http://www.sykes-cpa.com/",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/sykes2014-resized-165.png",
                "text" : ""
              },
            "vendor" : "Sykes & Company, P.A.",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "sales@telemanager.com  ",
                "text" : "TeleManager Technologies<br>Contact:Paul Kobylevsky800-600-0435sales@telemanager.com<br>Website: www.TeleManager.com<br>Address: 211 Warren St. Ste 409 Newark, NJ 07103"
              },
            "description" : "TeleManager Technologies is a leading provider of Cloud-Based Communications Solutions to pharmacies. Our mission is to help any type of pharmacy enhance their level of service to patients and prescribers, as well as improve their operational efficiency, to help reduce operating costs.",
            "logo" : { "alt" : "TeleManager Technologies",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/telemanager.png",
                "text" : ""
              },
            "vendor" : "TeleManager Technologies, Inc",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.theperfectmat.com  ",
                "text" : "The Perfect Mat<br>Sales & Marketing Contact: Chris Stumpf<br>Tel: 800.888.2019<br>Website: www.theperfectmat.com"
              },
            "description" : "The Perfect Mat offers a full line of matting for your pharmacy. We specialize in anti-fatigue matting and logo matting. It is hard to argue with our motto, \"When you feel better, you do better.\" Stop by our booth to relieve your aching feet, legs and back. Applying ergonomic principles in the workplace increases productivity and efficiency, reduces errors and waste, increases worker satisfaction and morale, and ultimately assists in the ensuring of quality work and customer service. All of this results in a BETTER experience for your customers and increases the opportunity for repeat business. We offer a FREE 2 Week Evaluation Program for a 1” x 3 foot x 4 foot mat & a Show Special! STOP BY FOR DETAILS.",
            "logo" : { "alt" : "The Perfect Mat",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/conference/2013%20vendors/tpm_logo_600dpi.gif?1351289487.21613",
                "text" : ""
              },
            "vendor" : "The Perfect Mat",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "drewerts@thriftywhite.com",
                "text" : "Thrifty White Pharmacy<br>Doug RewertsDirector Affiliated Pharmacy Program<br>Email: drewerts@thriftywhite.com<br>Tel:direct 763.513.4372cell    612.803.3788fax    763.248.7583<br>Address: 6055 Nathan Lane N. Suite 200 Plymouth, MN 55442"
              },
            "description" : "Thrifty White Pharmacy is a 100% employee owned pharmacy chain headquartered in the Upper Midwest.  Recently recognized as Pharmacy Innovator of the Year in 2012 by Chain Drug Review, Thrifty White currently owns and operates more than 90 retail pharmacies and provides service to more than 20,000 patients in multiple skilled nursing, independent living and alternate care facilities.",
            "logo" : { "alt" : "thrifty white",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/thrifty white.jpg",
                "text" : ""
              },
            "vendor" : "Thrifty White Pharmacy",
            "years" : "2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.pharmacyowners.com/marketplace/tri-state-distribution/tel:931-738-2174",
                "text" : "Tri State Distribution<br>Sales Manager: Bill McGuire<br>Tel: 931-738-2174<br>Email: bmcguire@tsdi.net<br>Website: www.provial.com<br>Address: 600 Vista Drive Sparta, TN 38583"
              },
            "description" : "Tri State Distribution specializes in helping our customers compete in today’s tough business environment by offering unique products, programs and services designed not only to save you money, but more importantly, to help you make more money, too. No other prescription packaging company can help you do this; indeed, most vials and closures haven’t changed substantially in form or function for almost 40 years. Imagine trying to compete today with typewriters and business methods and equipment of the 70’s – yet that is exactly what you do when you use traditional vials and bottles that were first introduced at that time, too!",
            "logo" : { "alt" : "Tri State Distribution",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/tristate.png",
                "text" : ""
              },
            "vendor" : "Tri State Distribution",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "Total Package Sales (TPS) was established in 2008 and offers a full service team of sales representatives. TPS was established to provide easy “One Stop Shopping” with our primary products focusing on the nursing mom, eco-friendly family, and new baby.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=283761&moduleid=546971&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "Total Package Sales"
          } },
      { "vendor" : { "contact_info" : { "href" : "amy@truedosepets.com",
                "text" : "True-Dose<br>Sales: Amy Malpocker<br>Tel: 877-447-7710<br>Email: amy@truedosepets.com<br>Website: www.TrueDosePets.com<br>Address: 6209 Mid Rivers Dr. Ste. 107 St. Charles, MO 63304"
              },
            "description" : { "href" : "http://www.truedosepets.com/",
                "text" : "Headquartered in St. Charles, MO, True-Dose follows FDA pharmaceutical standards to provide superior liquid supplements for dogs and cats. The patented metered dose delivery system accurately delivers highly concentrated doses of liquid. This is a clean, easy way to administer supplements on a daily basis. True-Dose offers formulations for Joint Care, Wellness, Skin & Coat, Calming and Agility as well as pet dental cleaning products. True-Dose products are available in pharmacies across the country. www.truedosepets.com"
              },
            "logo" : { "alt" : "Tru-Dose",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/truedose.gif",
                "text" : ""
              },
            "vendor" : "True-Dose Liquid Pet Supplements",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.pharmacyowners.com/marketplace/vinco/tel:1-800-245-1939",
                "text" : "Vinco, Inc.<br>Jack Byrd<br>Tel: Office 1-800-245-1939Cell 561-558-4247.<br>Email: jbyrd@vincoinc.com<br>Website: www.vincoinc.com<br>Address: 1519 Mars-Evans City Road Evans City, PA 16033"
              },
            "description" : "Vinco, Inc. is committed to excel as an ‘outcomes-based’ designer, manufacturer and distributor of superior nutritional supplements.  As such we use only the best ingredients and take every precaution to ensure the highest standard in manufacturing and distribution procedures. Let us help you help your patients.",
            "logo" : { "alt" : "",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/vinco.gif",
                "text" : ""
              },
            "vendor" : "Vinco, Inc",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.vitaphone.com",
                "text" : "Vitaphone<br>Contact: Jeffrey Landis<br>Tel: 1.888.869.3965 <br>Direct:702-241-5140<br>Email: jlandis@vitaphoneus.com<br>Website: www.vitaphone.com<br>Address:<br>7140 Dean Martin Drive, Suite 700Las Vegas, Nevada 89118"
              },
            "description" : "Do you have a multi-dose robotic pouching machine? Would you like to maximize your investment in your robot?",
            "logo" : { "alt" : "Vitaphone",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/vitaphone%20hs%20logo.jpg?1358522766.38351",
                "text" : ""
              },
            "vendor" : "Vitaphone Health Solutions",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : "2014 PDS Conference Exhibitor",
            "description" : "For over two decades, voiceTech’s pharmacy technology services have used a sophisticated integration with most popular pharmacy systems to  provide new levels of efficiency and customer satisfaction. Thousands of pharmacies have enjoyed the benefits of voiceTech’s IVR technology and the other technology services offered.  Additionally, it’s voiceTech’s goal to help pharmacies increase revenue and improve their bottom-line by providing convenient ways for people to do business with the pharmacy, by automatically handling routine tasks for the pharmacy staff and most importantly by proactively communicating with customers in order to drive business into the pharmacy.  Our technology services are available individually or in any combination to meet specific pharmacy needs and budget.",
            "logo" : { "alt" : "RSS Feed",
                "href" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/rss.aspx?tabid=276067&moduleid=541550&maxcount=25",
                "src" : "http://www.pharmacyowners.com/CMS/UI/Modules/BizBlogger/res/feed-icon-16x16.gif",
                "text" : ""
              },
            "vendor" : "voiceTech"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.xymogen.com/",
                "text" : "XYMOGEN<br>Tel: 1.800.647.6100 <br>Website:www.xymogen.com"
              },
            "description" : { "href" : "http://www.xymogen.com",
                "text" : "XYMOGEN, a family owned health sciences company headquartered in Orlando, Fla., has been providing high-quality dietary supplements to licensed healthcare practitioners for more than a quarter century. The nutraceutical company has introduced numerous innovations to the functional medicine community, and its Medical Board of Advisors consists of clinical practitioners who represent a broad range of specialties. XYMOGEN’s unprecedented growth continues with a new, state-of-the-art, 136,000 squarefoot manufacturing facility and laboratory, and a strategic expansion into Canada and Australia, two of the largest international markets. XYMOGEN’s strength as a company was reinforced in 2007, 2008, 2010, and 2011, when it was recognized by Inc. magazine as one of the 5000 fastest-growing private companies in America. For more information, visit www.xymogen.com"
              },
            "logo" : { "alt" : "XYMOGEN",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/xymogen%20logo.jpg?1345218729.91666",
                "text" : ""
              },
            "vendor" : "XYMOGEN",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.bankonyourself.com/PharmacyOwners",
                "text" : "Bank on Yourself<br>Tel: <br>800.687.8345<br>Email:<br>Grant@ThompsonThurman.com<br>Website:<br>www.ThompsonThurman.com"
              },
            "description" : { "href" : "http://www.pharmacyowners.com/blog/bid/89759/Retirement-Planning-for-Pharmacy-Owners",
                "text" : "Discover how to fire your banker, bypass Wall Street and take control of your financial future! <br>Bank on Yourself author Pamela Yellen joined us for a conference call where she discussed the benefits pharmacy owners find when using the program. <br>Access the recorded call here. <br>Grant Thompson and Chris Thurman are the Bank On Yourself Authorized Advisors specializing in helping independent pharmacy owners achieve lifetime financial security and reach their personal and professional goals and dreams – without taking unnecessary risks.<br>Find out how big your nest-egg could grow (safely, predictably and guaranteed) if you added the Bank On Yourself method to your financial plan.<br>Contact Thompson & Thurman at 800-687-8345 or Grant@ThompsonThurman.com for your FREE, no-obligation Analysis!  Or visit  www.BankOnYourself.com/PharmacyOwners"
              },
            "logo" : { "alt" : "Bank On Yourself",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/bank%20on%20yourself.png",
                "text" : ""
              },
            "vendor" : "Bank On Yourself",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.CreativePharmacist.com",
                "text" : "CreativePharmacist<br>Sales Manager: <br>David Pope<br>Tel: <br>706.210.9087 <br>Email: <br>david@creativepharmacist.com<br>Website: <br>www.CreativePharmacist.com<br>Address: <br>858 Jasmine Tr. Evans, GA 30809 "
              },
            "description" : { "href" : "http://www.CreativePharmacist.com",
                "text" : "CreativePharmacist.com is the 1 hour a month solution for independent pharmacies to set themselves apart from the competition by re-establishing themselves as a local health support knowledge base. We provide the plan, resources and marketing that improve your patients\" outcomes and your pharmacy\"s bottom line. Check us out to discover more!"
              },
            "logo" : { "alt" : "Creative Pharmacist",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/creativepharmacist.png",
                "text" : ""
              },
            "vendor" : "Creative Pharmacist",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.FranklinEyewear.com",
                "text" : "Franklin Eyewear<br>Sales: <br>Al Underwood<br>Tel: <br>877.766.0321<br>Email: <br>alunderwood@gmail.com<br>Website: <br>www.FranklinEyewear.com<br>Address: <br>PO Box 13887 Jackson, MS 39236"
              },
            "description" : { "href" : "ttp://www.FranklinEyewear.com",
                "text" : "Priced to compete with the discount chains<br>FranklinEyewear® is in the business of providing to the independant pharmacy an attractive and effective eyewear program competitive with the major chain stores. We do that by offering a stylish quality product at a low price that is easy to stock.<br>Our sunglasses and reading glasses retail for $6.99 and are color coded for easy stocking and reordering. Sunglasses are color coded by category, and reading glasses by strength .  <br>At this price our quality and style will encourage multiple purchases from your customers. <br>We know of instances where those who have bought our product have told their friends and brought new customers to a store just because of the reading glasses, which are a necessity for many.<br>Our color coding system makes restocking simple, and our styles keep your display up to date.<br>Keeping the colors organized and the display properly stocked is the formula for success.<br>We also offer Sun Readers for $9.99 retail.  They are great for your beach reading, or being able to read your smartphone in the sunlight!<br>Franklin Eyewear has it\"s roots when Al Underwood left college and started selling sunglasses and  other specialty items in 1975.  Reading glasses became of a part of that mix in 1985, just before the first baby boomers reached age 40.   Having the right styles at the right price has proven Franklin Eyewear® to many as the eyewear program for their store!"
              },
            "logo" : { "alt" : "Franklin Eyewear",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/franklin.png",
                "text" : ""
              },
            "vendor" : "Franklin Eyewear",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.liveoakbank.com/pharmacy",
                "text" : "Live Oak Bank<br>Senior Loan Officer: <br>Ed Webman, RPh<br>Tel: <br>407.539.0396<br>Email: <br>Ed.Webman@liveoakbank.com<br>Website: <br>www.liveoakbank.com/pharmacy<br>Address: <br>2605 Iron Gate Dr, Suite 100<br>Wilmington, NC 28412"
              },
            "description" : { "href" : "http://www.liveoakbank.com/video-series/pharmacy-testimonal-video-series/",
                "text" : "You take care of the people in the community - Live Oak Bank takes care of financing your dreams.<br>Live Oak was founded in 2008 with one goal: provide business loans to independent businesspeople in niche industries. We support the unique business opportunities available to pharmacists. We have a truly personalized and efficient lending approach. Contact us to learn how easy it is to acquire or refinance a practice or jump start your expansion project with pharmacy lending options, sound advice and fast service from Live Oak Bank. <br>Visit our Pharmacy Testimonial Video Series"
              },
            "logo" : { "alt" : "Live Oak Bank",
                "src" : "http://contact-form-uploads.s3.amazonaws.com/37772/59d479be-a1db-4516-bceb-742fdcd8130b/LOB-Red-Primary.jpg",
                "text" : ""
              },
            "vendor" : "Live Oak Bank",
            "years" : "2013, 2014 PDS Conference Event Sponsor<br>2012 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.matchrx.com/",
                "text" : "MatchRx<br>Customer Service:<br>Karen Dabish<br>Tel: <br>877.590.0808 <br>Email: kdabish@matchrx.com<br>Website: <br>www.MatchRX.com<br>Address: <br>210 E. 3rd St, Ste. 100 Royal Oak, MI 48067 "
              },
            "description" : { "href" : "http://www.matchrx.com/demos/",
                "text" : "Join a community of over 2,700 pharmacy members buying and selling overstock from one another. With a marketplace of over 42,000 items, chances are you’ll find what you need at a great price while helping a fellow independent. Members have collectively saved millions of dollars by using MatchRX. MatchRX is your online marketplace, allowing pharmacies across the country to buy and sell overstock, soon-to-expire, and partial pack prescription drugs from one another for 10%-90% off WAC. Register for free at www.MatchRX.com. Website: www.MatchRX.com Video link: http://www.matchrx.com/demos/"
              },
            "logo" : { "alt" : "MatchRx",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/matchrx.png?1358289004.57011",
                "text" : ""
              },
            "vendor" : "MatchRx",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.promot.com/",
                "text" : "Promotions Unlimited<br>Vice President of Sales:<br>Diane Taleck<br>Tel:<br>262-681-7000 <br>ext. 465<br>Email:<br>diannat@promot.com<br>Website: <br>www.Promot.com<br>Address: <br>7601 Durand AVE, Mt. Pleasant, WI 53177"
              },
            "description" : { "href" : "",
                "text" : "For over 35 years, Promotions Unlimited has provided pharmacies with front end product, advertising and merchandising solutions nationwide.<br>We are the most competitive provider of products in terms of price, selection and quality across our 3 wholesale distribution programs: a seasonal and everyday monthly pre-book program, an everyday dollar program and a seasonal fabric pre-book program.<br>Our advertising helps stores to become more competitive and profitable by reaching out to new shoppers and motivating existing customers to shop their store more often. On a monthly basis, we create and distribute promotional advertising vehicles on our customers behalf. The offered advertising vehicles include printed circular/flyers and coupon books, email distribution of the sales, and in-store signage to support the sale (shelf talkers and window signage).<br>Did you know of 70% of shopper\"s purchasing decisions are made in-store? Our merchandising programs help increase those impulse buys. We offer over 46 planograms of our everyday dollar merchandise, signage for dollar merchandise, seasonal decoration kits and shelf talkers of our monthly pre-book merchandise, complete with suggested retail prices better than the mass merchandisers.<br>Year-round, we invite retailers to come and shop our 60,000 sq. ft. showroom. We display over 9,000 products from our monthly pre-book program and over 3,000 items from our everyday dollar program. Products can be ordered simply with the click of a scanner. Not only is it easy, but for 1st time customers, it\"s free- we\"ll cover all your travel expenses!<br>Check out www.promot.com for more information!"
              },
            "logo" : { "alt" : "Promotions Unlimited",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/promotions%20unlimited%20new%20logo.jpg?1358278179.67866",
                "text" : ""
              },
            "vendor" : "Promotions Unlimited",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.rm-solutions.com/",
                "text" : "Retail Management Solutions<br>VP Sales & Marketing: <br>Mike Gross<br>Tel: <br>360.339.4985<br>Email: <br>mikeg@rm-solutions.com<br>Website:  <br>www.RM-Solutions.com<br>Address: <br>101 Capitol Way N, Ste. 200 Olympia, WA 98501"
              },
            "description" : { "href" : "http://www.pharmacyowners.com/Portals/37772/images/RMS%20MarketplaceAd.jpeg",
                "text" : "Point-of-sale system designed specifically for pharmacies<br>As an industry leader in pharmacy point-of-sale systems, RMS can provide any business with a solution that is sized just to fit, yet adaptable for for multi-location growth."
              },
            "logo" : { "alt" : "Retail Management Solutions",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/retailmanagementsolutions.gif",
                "text" : ""
              },
            "vendor" : "Retail Management Solutions",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.takechargerx.com/",
                "text" : "Take Charge<br>Sales: <br>Terry Forshee<br>Tel: <br>800.782.3444<br>Email: <br>dt4c@takechargerx.com<br>Website: <br>www.TakeChargeRx.com<br>Address: <br>1690 25th St. NW, Ste. D Cleveland, TN 37311"
              },
            "description" : { "href" : "",
                "text" : "Professional Weight Loss & Healthy Lifestyle Strategies<br>Take Charge Professional Obesity Education - Healthy Lifestyle Strategies is a company providing a turnkey system of lifestyle modification for Community Pharmacists. Take Charge® is a very profitable 13 week, one on one program for weight loss and permanent lifestyle modification focused on the patient w/type 2 diabetes, hyperlipidemia, HTN and other cardiovascular diseases. The patient is led step-by-step through their personalized program by a knowledgeable trained health care professional that they have learned to trust with their health…their Pharmacist. Take Charge® is designed to enhance the Pharmacist’s status as a Health Care Provider.<br>The Pharmacist serves as educator, motivator, monitor and guide as the patient, through trial and error, learns and implements lifestyle changes that improve their health and well-being. The sole purpose for Take Charge is to position the Community Pharmacist as THE go to professional for the treatment and prevention of obesity and the co-morbid conditions such as type 2 diabetes, hyperlipidemia, hypertension and other cardiovascular diseases.<br>At a cost of only $349/month, Take Charge is more than a niche for your pharmacy...it is an entry point into the future of community pharmacy as a true Healthcare Provider. At current pricing a minimum of 6 patients PER YEAR totally pays for the program."
              },
            "logo" : { "alt" : "Take Charge",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/pds-tv/marketplace/takecharge.jpg",
                "text" : ""
              },
            "vendor" : "Take Charge",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } },
      { "vendor" : { "contact_info" : { "href" : "http://www.waypointpharmacyadvisors.com/",
                "text" : "Waypoint Pharmacist Advisors<br>Sales: <br>Benjamin Coakley<br>Tel: <br>843.873.4420<br>Email: <br>Ben@waypointus.com<br>Website: <br>www.WaypointPharmacy<br>Advisors.com<br>Address: <br>10570 Dorchester Rd. Summerville, SC 29485"
              },
            "description" : { "href" : "",
                "text" : "We take care of pharmacists!<br>Ensuring independent community pharmacists optimize every opportunity and enjoy the greatest possible independence during and after their life in pharmacy is our mission.<br>After 30 plus years of experience, we have identified and validated the “lifecycle” all community pharmacists must navigate. Fundamentally, the Community Pharmacist Lifecycle® is about awareness; it helps you determine where you are, where you are going and how thoroughly you have completed each stage. This clarity empowers you to optimize your valuable and irreplaceable time and life energy going forward.<br>We take care of you by applying five core disciplines: Personal financial decision-making, planning and management Investment planning and execution Risk management Exit and succession plan development and execution Estate and retirement planning.<br>Within each discipline, we continuously develop and refine strategies, tools, and capabilities to ensure you enjoy the greatest possible independence during and after your life in pharmacy.<br>To earn your trust, we make a significant investment of knowledge, time and effort in initiating a relationship that is all about you.<br>In addition to offering many free educational modules and do-it-yourself planning tools, we offer a personal Discovery Workshop and Inspired Independence Blueprint. These valuable components of The Inspired Independence Process™ enable you to thoroughly assess what working with Waypoint will do for you.<br>Whether you are nearing the end of your career and thinking about exiting your pharmacy, brand new to the business, or somewhere in between, call us… we can help.<br>Waypoint Pharmacy Advisors<br>10570 Dorchester Road<br>Summerville, SC 29485<br>Phone: 843-873-4420<br>www.waypointus.com<br>info@waypointus.com"
              },
            "logo" : { "alt" : "Waypoint Pharmacist Advisors",
                "src" : "http://www.pharmacyowners.com/Portals/37772/images/waypoint-resized-165.jpg",
                "text" : ""
              },
            "vendor" : "Waypoint Pharmacist Advisors",
            "years" : "2013, 2014 PDS Conference Exhibitor"
          } }
    ] }';

        $vendor_object = json_decode($vendors);

        //var_dump($vendor_object);
        foreach ($vendor_object->vendors as $key => $val) {
            foreach ($val as $key => $value) {
                var_dump($value);
                $vendors = new TmpVendor;
                //return var_dump(count($value->description));
                $vendors->vendor = $value->vendor;
                $vendors->hs_acc_number = 0;
                if (isset($value->description)) {
                    if (is_object($value->description)) {
                        echo "-------------<br/>";
                        var_dump($value->vendor);
                        var_dump($value->description);
                        echo "-------------<br/>";
                        if ($value->description->text !== '') {
                            $vendors->site_url = $value->description->href;
                            $vendors->description = $value->description->text;
                        } else {
                            $vendors->description = 'No Description Available';
                        }
                    } else {
                        echo "==============<br/>";
                        var_dump($value->vendor);
                        var_dump($value->description);
                        echo "==============<br/>";
                        $vendors->description = $value->description;
                    }
                } else {
                    $vendors->description = 'No Description Available';
                }
                $vendors->years = isset($value->years) ? $value->years : 'No participation to this date.';
                if (isset($value->contact_info)) {
                    if (is_object($value->contact_info)) {
                        echo 'Yay';
                        var_dump($value->vendor);
                        var_dump($value->contact_info->href);
                        var_dump($value->contact_info->text);
                        $vendors->contact_href = isset($value->contact_info->href) ? $value->contact_info->href : '#';
                        $vendors->contact_info = $value->contact_info->text;
                    } else {
                        echo 'Woo!';
                        var_dump($value->vendor);
                        var_dump($value->contact_info);
                        $vendors->contact_info = $value->contact_info;
                    }
                } else {
                    $vendors->contact_info = 'No Contact Info Available';
                }
                $vendors->logo_href = $value->logo->src;
                $vendors->logo_src = $value->logo->src;
                $vendors->logo_alt = $value->logo->alt;
                $vendors->save();
            }
        }
        return var_dump($vendor_object->vendors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
