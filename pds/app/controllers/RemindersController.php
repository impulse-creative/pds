<?php

use UserRepo\Storage\User\UserRepository as UserRepo;

class RemindersController extends \BaseController {

    public static $rules = [
        'email' => 'required|email',
        'password' => 'required|alpha_num|confirmed',
        'password_confirmation' => 'required|alpha_num'
    ];
    
    public function __construct(UserRepo $users) {
        parent::__construct();
        $this->users = $users;
        $css = [ 'main', 'dash'];
        View::share('css', $css);
    }

    /**
     * Display the password reset request view.
     *
     * @return Response
     */
    public function getRemind() {
        $user_data = new User();
        $menu_data = $this->users->getMenu();
        $group_list = DB::table('group')->select('*')->get();
        $user = User::where('id', Auth::id())->first();
        View::share('pageTitle', 'PDSadvantage');
        return View::make('dashboard.Settings.settings', compact('menu_data', 'user'));
    }

    public function getForgotPassIndex() {
        return View::make('dashboard.Settings.forgot-pass');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind() {
        View::composer('emails.auth.reminder', function($view) {
            $view->with(['email' => Input::only('email')]);
        });
        $remind = Password::remind(Input::only('email'), function($message) {
                    $message->subject('[Pharmacy Development Services] Password Reset Request');
                });
        switch ($remind) {
            case Password::INVALID_USER:
                return Redirect::route('login.form')->with('error', 'Your email doesn\'t match our records, we were unable to send a password reset request');
            case Password::REMINDER_SENT:
                Session::put('Sent', 'Email was sent successfully. Please check your email at: ' . Input::get('email') . '.');
                return Redirect::route('login.form')->with('Success', 'Email was sent successfully. Please check your email at: ' . Input::get('email') . '.');
            default:
                # code...
                break;
        }
        Session::put('Sent', 'Email was sent successfully. Please check your email at: ' . Input::get('email') . '.');
        if (Auth::check()) {
            $menu_data = $this->users->getMenu();
        }
    }

    public function postForgotRemind() {
        Config::set('auth.reminder.email', 'emails.auth.remind_user');
        $pass_remind = Password::remind(Input::only('email'), function($message) {
                    $message
                            ->subject('[Pharmacy Development Services] Password Reset Request')
                            ->from('welcome@pharmacy-owners.com', 'Pharmacy Development Services');
                });
        switch ($pass_remind) {
            case Password::INVALID_USER:
                return Redirect::route('login.form')->with('error', 'Your email doesn\'t match our records, we were unable to send a password reset request');
                break;
            case Password::REMINDER_SENT:
                Session::put('Sent', 'Email was sent successfully. Please check your email at: ' . Input::get('email') . '.');
                return Redirect::back()->with('Success', 'Email was sent successfully. Please check your email at: ' . Input::get('email') . '.');
                break;
            default:
                # code...
                break;
        }
    }

    public function postConfirmationRemind($token = null) {
        if (is_null($token)) {
            App::abort(404);
        } else {
            return Response::make('hey');
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset($token = null) {
        if (is_null($token)) {
            App::abort(404);
        }
        return View::make('dashboard.Settings.password-manager.reset-form', compact('token'));
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset() {
        $credentials = Input::only(
                        'email', 'password', 'password_confirmation', 'token'
        );
        
        $validation = $this->validatePwdReset($credentials);
        
        if($validation->fails()) {
            return Redirect::back()->withErrors($validation->messages())->withInput();
        }
        
        $response = Password::reset($credentials, function($user, $password) {
                    $user->password = Hash::make($password);
                    $user->confirmed_user = 1;
                    
                    $user->save();
                });
                
        return $this->getResponseResults($response);
    }
    
    public function validatePwdReset($input) {
        $validator = Validator::make($input, self::$rules);
        
        return $validator;
    }
    
    public function getResponseResults($response) {
        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::route('login.form')->with('error', 'Your email doesn\'t match our records, we were unable to send a password reset request');

            case Password::PASSWORD_RESET:
                Session::put('passwordReset', 'Your password has been successfully reset! Please login again.');
                Auth::logout();
                return Redirect::route('login.form')->with('Success', 'Your password was reset.');
        }
    }

}
