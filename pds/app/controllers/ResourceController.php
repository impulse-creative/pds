<?php

use UserRepo\Storage\User\UserRepository as UserRepoInterface;
use GroupRepo\Permissions\Groups\GroupRepository as GroupInterface;
use UserRepo\Storage\Forums\CategoryRepository as Categories;
use FileRepo\Storage\Resources\FileRepository as FileInterface;
use BoxView\BoxViewHandler\BoxViewCreator as FileCreator;
use BoxView\lib\BoxViewApi;
use BoxView\lib\BoxViewDocument;

class ResourceController extends BaseController {

    public function __construct(UserRepoInterface $users, GroupInterface $groups, Categories $categories, FileInterface $files) {
        parent::__construct();
        $this->beforeFilter('csrf', array('on' => 'login'));
        $this->users = $users;
        $this->groups = $groups;
        $this->categories = $categories;
        $this->files = $files;
        if (Auth::check()) {
            View::share('usr_email', Auth::user()->email);
            View::share('usr_name', Auth::user()->fname);
            View::share('user_permissions', explode(':', Auth::user()->groups));
        }
        $this->box_api_key = '5qri8f4u0731tlcvgdfqlvmh9abgxtcq';
        $css = [ 'main', 'forum'];
        View::share('css', $css);
        View::share('pageTitle', 'PDS Resource Library | PDSadvantage');
    }

    /**
     * Display a listing of the resources.
     *
     * @return Response
     */
    public function index() {
        $userdata = new User();
        //make sure the user gets the correct menu based off of their dedicated group permissions
        $menu_data = $this->users->getMenu();
        //retreive the resource files in their entirety
        $showFiles = new FileStorage();
        $table_data = json_decode($showFiles->showFilesByType(1));
        $user = User::find(Auth::id());
        $user_groups = explode(':', $user->groups);

        return View::make('dashboard.Resources.resource_accordion', compact('menu_data', 'table_data', 'user', 'user_groups'));
    }

    public function indexTrashcan() {
        $user = new User;

        //make sure the user gets the correct menu based off of their dedicated group permissions
        $menu_data = $this->users->getMenu();

        $soft_deletes = FileStorage::withTrashed()->where('deleted_at', '!=', 'null')->get();

        return View::make('dashboard.Resources.Trashcan.index')->with('menu_data', $menu_data)->with('soft_deletes', $soft_deletes);
    }

    /**
     * Retrieve the view for adding resource groups
     */
    public function getAddResourceGroup() {
        $access = $this->groups->allInArray();
        return View::make('dashboard.Groups.Resource_Groups.add', compact('access'));
    }

    /**
     * Get the main view that displays the resources in the PDS App
     */
    public function getLibraryIndex() {
        //default resource to 1 -- first default resource type -- for now >.>
        $viewing_resource = '1';
        $file_type = FileTypes::find($viewing_resource);
        $resource_type = $file_type->type_name;
        $search_key = Input::get('search_key');
        $resources = $this->files->allWithForeignDetails();

        //get hs resource groups
        $file_types_array = self::getResourceContainerList();
        //get categories
        $resource_cats = self::getResourceTypes();

        //determine access to available resource tools
        $admin_tools = $this->groups->checkIfAdmin(Auth::id()) == 1 ? 1 : 0;

        //get list of resource containers -- with access levels
        $acl = self::getResourceContainerList();
        $tempGroups = ResourceGroups::all();
        $allResourceGroups[0] = '--Select what category this resource belongs to--';
        foreach ($tempGroups as $group) {
            $allResourceGroups[$group->id] = $group->group_name;
        }

        //get list of available permissions
        $access_list = self::getGroupAccessList();

        return View::make('Forums.Library.index-all', compact('allResourceGroups', 'file_types_array', 'resources', 'resource_type', 'admin_tools', 'viewing_resource', 'acl', 'resource_cats', 'access_list', 'search_key'));
    }

    public function getGlobalResourceDependencies() {
        $resource_category = Input::get('type');
        $viewing_resource = isset($resource_category) ? $resource_category : '1';
        $file_type = Group::find($viewing_resource);
        $resource_type = $file_type->type_name;

        $resources = $viewing_resource != '1' ? $this->files->allWithForeignDetails($viewing_resource) : $this->files->allWithForeignDetails();

        //get hs resource groups
        $file_types_array = $this->groups->getResourceGroups();

        //get categories
        $resource_cats = self::getResourceTypes();

        //determin available tools
        $admin_tools = $this->groups->checkIfAdmin(Auth::id()) == 1 ? 1 : 0;

        //get list of access levels

        $acl = self::getResourceContainerList();

        //get list of available permissions
        $access_list = self::getGroupAccessList();

        //get user image
        $user_hs_img = \User::getBioImage(Auth::id());

        return ['user_hs_img' => $user_hs_img,
            'file_types_array' => $file_types_array,
            'resources' => $resources,
            'resource_type' => $resource_type,
            'viewing_resource' => $viewing_resource,
            'admin_tools' => $admin_tools,
            'acl' => $acl,
            'resource_cats' => $resource_cats,
            'access_list' => $access_list
        ];
    }

    public function getFilteredForumIndex() {
        $viewing_resource = Input::get('type');
        $file_type = Group::find($viewing_resource);
        $resource_type = $file_type == null ? '' : $file_type;
        $resources = $this->files->allWithForeignDetails($viewing_resource);
        //get hs resource groups
        $groups = $this->groups->getResourceGroups();

        $file_types_array = $groups;

        //get categories
        $resource_cats = self::getResourceTypes();

        //determine available tools
        $admin_tools = $this->groups->checkIfAdmin(Auth::id()) == 1 ? 1 : 0;

        //get list of access levels
        $acl = self::getResourceContainerList();

        //get list of available permissions
        $access_list = self::getGroupAccessList();

        //get user image
        $user_hs_img = \User::getBioImage(Auth::id());
        $tempGroups = ResourceGroups::all();
        $allResourceGroups[0] = '--Select what category this resource belongs to--';
        foreach ($tempGroups as $group) {
            $allResourceGroups[$group->id] = $group->group_name;
        }

        return View::make('Forums.Library.index-all', compact('allResourceGroups', 'user_hs_img', 'file_types_array', 'resources', 'resource_type', 'viewing_resource', 'admin_tools', 'acl', 'resource_cats', 'access_list'));
    }

    public function getEditResource($id) {
        //get resource container groups
        $file_types_array = $this->groups->getResourceGroups();

        //get resource to edit
        $resource = $this->files->find($id);

        //get categories
        $resource_cats = self::getResourceTypes();

        //get list of access levels
        $acl = self::getResourceContainerList();

        //get list of available permissions
        $access_list = self::getGroupAccessList();

        return View::make('Forums.Library.partials.edit-resource', compact('file_types_array', 'resource', 'user_hs_img', 'acl', 'resource_cats', 'access_list'));
    }

    /**
     * Retrieve list of resource containers 
     */
    public function getResourceContainerList() {
        return $this->groups->getResourceGroups();
    }

    /**
     * Retrieve access list for listing
     */
    public function getGroupAccessList() {
        return $this->groups->allInArray();
    }

    /**
     * Get resource types
     *
     * @return array $file_types
     */
    public function getResourceTypes() {
        $file_types = FileTypes::all();
        $file_types_array = array();
        $file_types_array[0] = '-- Select Types --';
        foreach ($file_types as $type) {
            $name = $type->type_name;
            if (preg_match('/_/', $name)) {
                $name = str_replace('_', ' ', $type->type_name);
            }
            $file_types_array[$type->id] = $name;
        }
        return $file_types_array;
    }

    /**
     * Get social sharing preview 
     *
     * @param int $resource_id
     */
    public function getSocialPreview($id) {
        //get available categories -- all
        $file_types_array = $this->groups->getResourceGroups();

        //get resource to edit
        $resource = $this->files->findResourceWithRelatedDetails($id);
        $pageTitle = $resource->filename. ' | PDSadvantage';
	if ($resource->external_link != 1) {
            return View::make('Forums.Library.partials.social-share-preview', compact('file_types_array', 'resource', 'user_hs_img', 'pageTitle'));
        } else {
            return Redirect::away($resource->resource_name);
        }
    }

    /**
     * Get trash bin
     *
     * @return view 
     */
    public function getTrashBin() {
        //default resource to 1 -- first default resource type -- for now >.>
        $viewing_resource = '1';
        $file_type = FileTypes::find($viewing_resource);
        $resource_type = $file_type->type_name;

        //get trashed resources
        $resources = new Resources();
        $resources = $this->files->allWithTrashed();

        //get available categories -- all
        $file_types_array = $this->groups->getResourceGroups();

        //get hubspot acc information -- if exists 
        $user_hs_img = \User::getBioImage(Auth::id());

        //determine available tools
        $admin_tools = $this->groups->checkIfAdmin(Auth::id()) == 1 ? 1 : 0;

        return View::make('Forums.Library.trashcan.trash', compact('user_hs_img', 'file_types_array', 'resources', 'resource_type', 'admin_tools', 'viewing_resource'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Redirect to dashboard
     */
    public function store() {
        $stored_file = $this->files->storeNewResource();
        if ($stored_file === 1) {
            return Redirect::back()->with('Success', 'Resource was successfully added!');
        } else {
            return $stored_file;
        }
    }

    /**
     * Store new resource groups
     *
     * @param string $name
     * @param string $description
     * @param string $access_level
     */
    public function wistia($div) {
        $groups = $this->groups->getResourceGroups();
        $file_types_array = $groups;
        return View::make('Forums.Library.wistia', compact('div', 'file_types_array'));
    }

    public function storeNewResourceGroup() {
        $params = self::getValidationParameters();

        $validator = Validator::make($params['input'], $params['rules']);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator)->with('errors', 'Validation failed on the input values. Please make sure to fill all of the form fields.');
        }

        $new_group = trim(Input::get('name'));
        $description = trim(Input::get('description'));

        $resource_created = self::createResource($new_group, $description, Input::get('access'));

        if ($resource_created) {
            return Redirect::route("acl.control")->with('Success', 'Successfully created the new resource group!');
        }

        return Redirect::route("acl.addgroups")->withInput([
                    "name" => Input::get("name"),
                    "description" => Input::get('description')
                ])->with('errors', $form->getErrors());
    }

    public function getValidationParameters() {
        return ['input' =>
            [
                'name' => trim(Input::get('name')),
                'description' => trim(Input::get('description')),
                'access' => Input::get('access')
            ],
            'rules' =>
            [
                'name' => 'required',
                'description' => 'required',
                'access' => 'required|NotIn:0'
            ]
        ];
    }

    public function createResource($group_name, $description, $access) {
        if ($this->groups->checkAccess('5') || $this->groups->checkAccess('1') || $this->groups->checkAccess('9')) {
            ResourceGroups::create([
                "group_name" => $group_name,
                "description" => $description,
                "permitted_groups" => Input::get('access')
            ]);
            return 1;
        }
        return 0;
    }

    /**
     * Download the specified resource.
     *
     * @param  int $filename
     * @return Response
     */
    public function show($filename) {
        return $this->files->downloadResource($filename);
    }

    //-->Display files by type
    public function showTableData($type_id) {
        return $this->files->getFileByType($type_id);
    }

    //-->multi-file download
    public function showDownloadMulti($file_list) {
        return $this->files->downloadMulti($file_list);
    }
    
    public function downloadMultiFiles() {
        $file_name = Input::get('file_name');
        return $this->files->downloadMulti($file_name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Update file existing file information 
     *
     * @return Back to resource page with success or failure session flash
     */
    public function updateFileInfo() {
        self::global_xss_clean();
        $stored_file = $this->files->updateResourceInfo();
        //return var_dump($stored_file);
        if (!$stored_file) {
            return Redirect::back()->with('Edit Failure', 'Please fill out each form field')->withInput();
        } else {
            return Redirect::back()->with('Edit Success', 'File successfully updated.');
        }
    }

    /**
     * Update the resource type
     *
     * @param int $resource_id
     */
    public function updateResourceType($resource_id, $new_type) {
        if (!$this->files->updateResourceType($resource_id, $new_type)) {
            return Redirect::back()->with('Failure', 'Failed to update resource type. Please reload the page and try again.');
        }

        return Redirect::back()->with('Success', 'Successfully updated resource type!');
    }

    /**
     * Update resource with edits
     *
     * @param int $id
     */
    public function updateResource() {

        $id = Input::get('id');
        $updated = $this->files->updateResourceFile($id);
        if ($updated == 1) {
            return Redirect::back()->withInput()->with('Success', 'Successfully updated resource!');
        }
        $file = Input::file('file');
        $rules = self::resourceValidationRules();
        
        $input = Input::only('filename', 'description', 'resource_type', 'access_level', 'permission');
        //return var_dump($input);
        //validate input
        $validate = Validator::make($input, $rules);

        //check for validation failure
        if ($validate->fails()) {
            return Redirect::back()->with('Failure', 'Could not update the resource. Please check your information and try again.')->withInput()->withErrors($validate);
        }
        //attempt to retrieve resource for editing
        $resource = $this->files->find($id);

        //update resource values
        $resource->filename = trim(Input::get('filename'));
        $resource->description = trim(Input::get('description'));
        $resource->resource_type = trim(Input::get('resource_type'));
        $resource->access_level = trim(Input::get('permission'));
        $resource->resource_container = trim(Input::get('access_level'));

        if (!is_null($file[0])) {
            $update_file = $this->files->updateResourceFile($id);
        }

        //check for successful update
        if ($resource->save()) {
            return Redirect::back()->withInput()->with('Success', 'Successfully updated resource!');
        }

        return Redirect::back()->withInput()->with('Failure', 'Could not update the resource. Reload the page. Please contact the web administrator if this persists.');
    }

    /**
     * update resource file only
     *
     * @param int $id
     */
    public function updateResourceFile() {
        $id = Input::get('id');
        if (\Input::hasFile('file')) {
            //intialize variables
            $data = \Input::file('file'); //array of files
            $resource = Resources::find($id);
            if(isset($resource->boxview_id)){
                $box = new Box_View_API($this->box_api_key);
                $doc = new Box_View_Document();
                $doc->id = $resource->boxview_id;

                // Change name and update document.
                $doc->name = $resource->filename;
                $realname = $resource->author . '_' . preg_replace('/ /', '_', $data[0]->getClientOriginalName());
                $doc->file_url = public_path().'/storage/files/Library/fullsized/'.$realname;
                $box->update($doc);
            }
        }
        $replace = $this->files->replaceResourceFile($id);

        if ($replace) {
            return Redirect::back()->with('Success', 'File was successfully replaced!');
        }

        return Redirect::back()->with('Failure', 'File could not be replaced. Reload the page and try again, if the problem persists please contact the web administrator.');
    }

    public function resourceValidationRules() {
        return [
            'filename' => 'required',
            'description' => 'required',
            'resource_type' => 'required',
            'access_level' => 'required',
            'permission' => 'required'
        ];
    }

    /**
     * Restore file from trashbin
     *
     * @param int $resource_id
     */
    public function restoreResource($resource_id) {
        $restore = $this->files->reverseDeleteResource($resource_id);

        if ($restore) {
            return Redirect::back()->with('Success', 'Resource was successfully restored!');
        }

        return Redirect::back()->with('Failure', 'Resource could not be restored. Please try again. If the issue persists, contact the web administrator.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id //resource id
     * @return Response
     */
    public function destroy($id) {
        $user_id = Auth::id();
        $user_permissions = explode(':', Auth::user()->groups);
        //return var_dump($id);
        if (in_array('9', $user_permissions) || in_array('1', $user_permissions) || in_array('5', $user_permissions) ) {
            if ($this->files->deleteResource($id)) {
                return Redirect::back()->with('Success', 'Successfully deleted resource. The resource is still available for restoration in the <a class="orange-text" href="'. URL::route('app.trashed.resource') .'">trashbin.</a>');
            }

            return \Redirect::back()->with('Failure', 'Item does not exist or the deletion failed, so we cannot remove it');
        } else {
            return \Redirect::back()->with('Failure', 'Insufficient permissions to remove resource');
        }
    }

    /**
     * Reverse the destroy process and bring back the chosen file
     * 
     */
    public function destroyReversal() {
        $file_id = Input::get('file_id');
        //restore a file by user id
        if ($this->files->reverseDeleteResource($file_id)) {
            return Redirect::route('resource.trashbin')->with('Restore Success', 'File was successfully retreived from the void.');
        }

        return Redirect::route('resource.trashbin')->with('Restore Failure', 'File could not be retreived from the void.');
    }

    /**
     * Permanently destroy the resource'
     *
     * @param int $file_id
     * @return Response
     */
    public function destroyForGood($resource_id) {
        if ($this->files->destroyResource($resource_id)) {
            return Redirect::route('app.trashed.resource')->with('Success', 'File was tossed into the void.');
        }

        return Redirect::route('app.trashed.resource')->with('Failure', 'File could not be deleted permanently. Please try again. If the problem persists contact the web administrator.');
    }

    /**
     * Clean all input received from the user's submitted form 
     *
     * @param Post data
     * @return none
     * @author Derek J. Foster
     */
    public static function global_xss_clean() {
        // Recursive cleaning for array [] inputs, not just strings.
        $sanitized = static::cleanInput(Input::all());
        Input::merge($sanitized);
    }

    public static function cleanInput($dirty_values) {
        $result = array();

        foreach ($dirty_values as $key => $value) {
            // Don't allow tags on key either, maybe useful for dynamic forms.
            $key = strip_tags($key);

            // If the value is an array, we will just recurse back into the
            // function to keep stripping the tags out of the array,
            // otherwise we will set the stripped value.
            if (is_array($value)) {
                $result[$key] = static::cleanInput($value);
            } else {
                // I am using strip_tags(), you may use htmlentities(),
                // also I am doing trim() here, you may remove it, if you wish.
                $result[$key] = trim(strip_tags($value));
            }
        }

        return $result;
    }

}
