<?php

use UserRepo\Storage\User\UserRepository as UserRepoInterface;

class SupportController extends \BaseController {


	public function __construct(UserRepoInterface $users) 
	{
            parent::__construct();
            $this->users = $users;
            $css = [ 'main', 'dash', 'forum' ];
            View::share('css', $css);
            View::share('pageTitle', 'Support | PDSadvantage');
                
	}

	public function index()
	{
            
    }
        public function indexSupport()
        {
            $user = new User();
            $menu_data = $this->users->getMenu();
            $user = User::find(Auth::id());
            $off_menu = 0;
            return View::make('Support.index', compact('user', 'menu_data', 'off_menu'));
        }
        
        
	/**
	 * Clean all input received from the user's submitted form 
	 *
	 * @param Post data
	 * @return none
	 * @author Derek J. Foster
	 */
	public static function global_xss_clean()
	{
    	// Recursive cleaning for array [] inputs, not just strings.
    	$sanitized = static::cleanInput(Input::all());
    	Input::merge($sanitized);
	}

	public static function cleanInput($dirty_values)
	{
    	$result = array();

    	foreach ($dirty_values as $key => $value) {
    		echo '<script>//console.log('.$key.');</script>';
        	// Don't allow tags on key either, maybe useful for dynamic forms.
        	$key = strip_tags($key);

        	// If the value is an array, we will just recurse back into the
        	// function to keep stripping the tags out of the array,
        	// otherwise we will set the stripped value.
        	if (is_array($value)) {
            	$result[$key] = static::cleanInput($value);
        	} else {
            	// I am using strip_tags(), you may use htmlentities(),
            	// also I am doing trim() here, you may remove it, if you wish.
            	$result[$key] = trim(strip_tags($value));
        	}
        }

    	return $result;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        
        public function showGettingStarted()
        {
            $achievements = Achievement::getActiveAchievements();
            $userAchievements = UserAchievement::getUserAchievements(Auth::id());
            if($userAchievements->completed != null) {
                foreach($userAchievements->completed as $key => $value) {
                    if($key > 0) {
                        $completedAchievements[$key] = true;
                    }
                }
            } else {
                $completedAchievements = [0=>0];
            }
            
            return View::make('Getting-Started.index', compact('achievements', 'userAchievements', 'completedAchievements'));
        }


}



