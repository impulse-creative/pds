<?php

use Carbon\Carbon as Carbon;
use TeamworkRepo\TeamworkApi as TwApi;
use UserRepo\Storage\User\UserRepository as UserRepoInterface;
use HandlerServices\Companies\CompanyReader as CompanyReader;
//use TeamworkRepo\Storage\API\TeamworkRepository as TeamworkInterface;

class TeamworkController extends BaseController {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public function __construct(UserRepoInterface $user) {
        parent::__construct();
        $this->api_key = 'cut656tree';
        $this->tw_company = 'pharmacyowners';
        $this->impulse_tw_id = '45584';
        $this->user = $user;
        $css = [ 'main', 'dash', 'forum'];
        View::share('css', $css);
        View::share('pageTitle', 'Projects | PDSadvantage');
    }
    
    
    public function getSingleCompany($company_id) {
        $single_company_hook_url = "http://". $this->tw_company .".teamwork.com/companies/". $company_id .".json";
        
        $channel = curl_init();
        curl_setopt( $channel, CURLOPT_URL, $single_company_hook_url );
        curl_setopt( $channel, CURLOPT_RETURNTRANSFER, 1 ); 
        curl_setopt( $channel, CURLOPT_HTTPHEADER, 
            array( "Authorization: BASIC ". base64_encode( $this->api_key .":xxx" ))
        );

        $results =  curl_exec ( $channel );

        curl_close ( $channel );
        
        return $results;
    }
    
    public function index(){
        $user = User::find(Auth::id());
        $reader = new CompanyReader($this);
        $companiesAvailable = $reader->getAllCompaniesById();
        $pharmaciesRegistered = $reader->getTwRegisteredCompanies();
        
        $pharmaciesInTW = $pharmaciesRegistered === 'None' ? [] : $pharmaciesRegistered;
        if(isset($user->teamwork_company_id) && $user->employee_type === 2) {
            $company_id_json = json_decode($user->companies, true);
            $company_id = array_keys($company_id_json);
            $employee_company = Company::where('id', $company_id[0])->where('tw_company_id', $user->teamwork_company_id)->first();
            return Redirect::route('get.pharmacy.project', ['companyId' => $company_id[0], 'custom_project_id' => $employee_company->tw_company_id]);
        } else {
            return View::make('Projects.pharmacy-register-list', compact('companiesAvailable', 'pharmaciesInTW'));
        }
    }
    
    public function noCompaniesAvailable() {
        return Redirect::route('dash.main')->with('Failure', 'Whoops!');
    }
    
    public function getProjectManagement($companyId) {
        if(!Auth::check()) {
            return Redirect::route('dash.main')->with('Failure', 'Whoops! Looks like you\'re not logged in! You must be logged in to view projects.');
        }
        if(Auth::user()->companies === '0' || Auth::user()->employee_type === 0) {
            return Redirect::route('dash.main')->with('Failure', 'Whoops! Looks like you don\'t have your account associated with a company! Please contact PDS for details via the support button in the navigation.');
        }
        $custom_project_id = Input::get('custom_project_id');
        $company = Company::find($companyId);
        $reader = new CompanyReader($this);
//        $requesting_user = $this->getProjectViewerId($companyId);
        $user = User::find($company->admin_id);
        $tw_data['user'] = isset($user[0]) ? $user[0] : $user;
        if($tw_data['user']->coach_id != '' || $tw_data['user']->is_id != '') {
            $tw_data['tw_managed_users'] = $this->getCoachOrISUsersTheyAreManaging();
        }
        //get project id from query string, if exists, if not, then use first in list of company's projects
        $project = isset($custom_project_id) ? Input::get('projectId') : $custom_project_id;
        $tw_data['company_project_list'] = $this->getAvailableProjects($companyId);
        $project_id_list = array_keys($tw_data['company_project_list']);
        $project_id = isset($project) ? $project : end($project_id_list);
        $tw_data['project_id'] = $project_id;
        $tw_data['pharmacy_name'] = $this->getProjectViewCompany($companyId);
        $tw_data['company'] = $company;
        if(isset($tw_data['user']->companies) || $tw_data['user']->companies !== '0' || $tw_data['user']->companies !== '"{}"') {
            $tw_data['tw_projects'] = json_decode($this->getAllProjects());
            $tw_data['tw_company'] = json_decode($this->getAllTeamworkCompanies());
            $tw_data['tw_milestones'] = json_decode($this->getAllUpcomingMilestonesInProject($project_id));
            $tw_data['tw_messages'] = json_decode($this->getAllMessagesInProject($project_id));
            $tw_data['tw_tasklist'] = json_decode($this->getAllTasksInProject($project_id));
            $tw_data['tw_activity_feed'] = json_decode($this->getAllActivityInProject($project_id));
            $tw_data['tw_pharmacy_employees'] = $reader->getCompanyEmployeesByCompanyId($companyId);
        }
        return View::make('Projects.account-dashboard', compact('tw_data'));   
    }
    
    //check to see what id the user is referencing for the viewing project
    //if current user is viewing own, then return current user id,
    //if not, return other user id
    public function getProjectViewerId($id){
        $this->requesting_user = $this->checkAccess($id);
        if(array_key_exists('Unauthorized', $this->requesting_user)) {
            return Redirect::route('dash.main')->with('Failure', 'You do not have access to this user\'s project account information. Please use an appropriate account.');
        }
        return isset($this->requesting_user['coach/IS']) ? $this->requesting_user['coach/IS'] : $this->requesting_user;
    }
    
    public function getProjectViewCompany($id) {
        $tw_pharmacy = TeamworkCompanies::where('company_id', $id)->first();
        
        if(count($tw_pharmacy) > 0){
            return $tw_pharmacy->pharmacy_name;
        }
        
        return 'none';
    }
    
    public function getAllActivityInProject($project_id) {
        $action = 'projects/'.$project_id.'/latestActivity';
        $project_activity_hook = "http://".$this->tw_company.".teamwork.com/".$action.".json";
        
        return $response = $this->teamworkGetRequest($project_activity_hook);
    }
    
    public function getPharmacyEmployees($hs_acc_number)
    {   
        $company_data = new User();
        $employees = $company_data->getCompanyEmployees($hs_acc_number);
        return $employees;
    }
    
    //return json string of all companies
    public function getAllTeamworkCompanies() {
        $action = 'companies';
        $multi_company_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
        
        return $response = $this->teamworkGetRequest($multi_company_hook_url);
    }
    
    public function getAllTasksInProject($project_id) {
        $action = 'tasks';
        $tasks_hook = "http://". $this->tw_company .".teamwork.com/projects/".$project_id."/". $action .".json?filter=within14&ignore-start-dates=false";
//        dd($tasks_hook);
        $teamwork = new TwApi($this);
        return $teamwork->teamworkGetRequest($tasks_hook);
    }
    
    public function getAddNewProject()
    {
        $tw_company = $this->getTeamworkCompany(Input::get('for_user'));
        return View::make('Projects.add-project', compact('tw_company'));
    }
    
    public function getTeamworkCompany($user_id) {
        return TeamworkCompanies::where('owner_id', $user_id)->first();
    }
    
    public function getAddNewMilestone()
    {
        $requesting_user = $this->getProjectViewerId(Input::get('for_user'));
        $potential_company_users = $this->user->getUsersWithSameSalesForceId(Input::get('for_user'));
        $company_project_list = $this->getAvailableProjects($requesting_user);
        return View::make('Projects.add-milestone', compact('potential_company_users', 'company_project_list'));
    }
    
    public function getAddNewMessage(){
        $requesting_user = $this->getProjectViewerId(Input::get('for_user'));
        $company_project_list = $this->getAvailableProjects($requesting_user);
        $potential_company_users = $this->user->getUsersWithSameSalesForceId(Input::get('for_user'));
        return View::make('Projects.add-message', compact('company_project_list', 'potential_company_users'));
    }
    
    public function getAddNewTask()
    {
        $project_id = Input::get('project_id');
        $potential_company_users = $this->user->getUsersWithSameSalesForceId(Input::get('for_user'));
        $company_task_lists = $this->getAllTaskListsInProject($project_id);
        return View::make('Projects.add-task', compact('potential_company_users', 'company_task_lists'));
    }
    
    public function getAllPeopleInProject($project_id) {
        $action = 'projects/'.$project_id.'/people';
        $people_hook = "http://". $this->tw_company .".teamwork.com/".$action.".json";
        return $this->teamworkGetRequest($people_hook);
    }
    
    public function getForumSubmitThankYou()
    {
        return View::make('Projects.request-thank-you');
    }
    
    public function postCreateNewCompany()
    {
        return $create_tw_company = $this->createNewTeamworkCompany();
    }
    
    public function processUserTeamworkAccessAndAccountData()
    {
        $teamwork = new TwApi($this);
        return $teamwork->processTeamworkRegistrationRequest(Input::get('pharmacy_id'));
    }
    
//    public function createNewTeamworkEmployeeUser()
//    {
//       //action to create new user
//        $action = 'people';
//        $scrubbed_username = preg_replace('/[^a-z\d\s]+/i', '', Auth::user()->pharmacy);
//        $polished_username = Auth::id().'_'.preg_replace('/ /', '-', $scrubbed_username);
//        $pwd = Auth::id().'_'.Auth::user()->hs_acc_number.'_tw_pass';
//        $company_account_created = $this->getMasterProjectDetails();
//        
//        if(isset($company_account_created['company_id'])) {
//            $data = $this->getNewTeamworkEmployeeInput($company_account_created['company_id'], $polished_username, $pwd);
//        }else{
//            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[Teamwork Employee Failure]: Your company owner has not set up his account yet.');
//        }
//        
//        $created_user = self::newTwPostRequest($action, $data);
//        if(is_object($created_user)) return $created_user;
//        if(isset($created_user['tw_status']->STATUS) && $created_user['tw_status']->STATUS === "Error") {
//            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[Teamwork Employee Failure]: Your employee account could not be set up. Message: '.$created_user->MESSAGE);
//        }
//        
//        return $response = $this->insertNewEmployeeUserDataIntoUserRecord($polished_username, $pwd, $created_user['tw_user_id']->id, $company_account_created['company_id'], $company_account_created['projects']);
//    }
    
    public function postCreateNewTwProject()
    {
        return $create_project = $this->createNewTeamworkProject();
    }
    
    public function postCreateTwProjectMilestone()
    {
        $project_id = Input::get('project_id');
        return $this->createNewMilestoneForProject($project_id);
    }
    
    public function postCreateNewTwUser()
    {
        return $create_tw_user = $this->createNewTeamworkUser();
    }
    
    public function postCreateNewTwTask()
    {
        return $create_task = $this->createNewTaskInTasklist();
    }
    
    public function postCreateNewTwMessage()
    {
        $project_id = Input::get('project_id');
        return $this->createNewMessageForProject($project_id);
    }
    
    public function createNewMessageForProject($project_id) {
        $action = 'projects/'.$project_id.'/posts';
        $data = self::getNewMessageInput();
        
        $validator = $this->validateNewMessage();
        if($validator->fails()) {
            return Redirect::back()->with('Failure', '[VALIDATION ERROR]: Could not validate the input provided. Please check your input and try again.');
        }
        
        $created_message = $this->teamworkPostRequest($action, $data);
        if (isset($created_message->STATUS) && $created_message->STATUS === "Error") {
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[ERROR]: Something went wrong. Teamwork says: '.$created_message->MESSAGE);
        }
        
        if(isset($created_message->messageId)) {
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Success', '[SUCCESS]: Successfully sent message!'); 
        }
        
        throw new Exception('Could not send the message for Teamwork project.');
    }
    
    public function getNewMessageInput()
    {
        $private = Input::get('is_private');
        return array(
            'post' => array(
                'title' => Input::get('message_title'),
                'notify' => [Input::get('message_recipient')],
                'body' => Input::get('message_body'),
                'private' => isset($private) ? $private : '0'
            )
        );
    }
    
    public function validateNewMessage(){
        $input = Input::all();
        $rules = [
            'message_title' => 'required',
            'message_body' => 'required',
            'message_recipient' => 'required'
        ];
        
        return $validator = Validator::make($input, $rules);
    }
    
    public function createNewMilestoneForProject($project_id) {
        $action = 'projects/'.$project_id.'/milestones';
//        return var_dump(Input::all());
        $carbon_milestone_deadline = new \Carbon\Carbon(Input::get('deadline_date'));
        $correct_milestone_format = $carbon_milestone_deadline->format('Ymd');
        $data = array(
            'milestone' => array(
                'title' => Input::get('milestone_title'),
                'description' => Input::get('milestone_description'),
                'deadline' => $correct_milestone_format,
                'notify' => Input::get('notify_party'),
                'reminder' => Input::get('remind_party'),
                'responsible-party-ids' => Input::get('responsible_party')
            )
        );
        
        $validator = $this->validateNewMilestone();
        if($validator->fails()){
            return Redirect::back()->with('Failure', '[VALIDATION ERROR]: Could not validate milestone input.')->withInput()->withErrors($validator);
        }
        
        $created_milestone =  $this->teamworkPostRequest($action, $data);
        if (isset($created_milestone->STATUS) && $created_milestone->STATUS === "Error") {
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[ERROR]: Something went wrong. Teamwork says: '.$created_milestone->MESSAGE);
        }
        
        if($created_milestone->STATUS === 'OK'){
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Success', '[SUCCESS]: Successfully created milestone!'); 
        }
        
        throw new Exception('Could not create the milestone for Teamwork project.');
    }
    
    public function validateNewMilestone(){
        
        $input = Input::all();
        $rules = [
            'milestone_title' =>  'required',
            'milestone_description' => 'required',
            'deadline_date' => 'required',
            'project_id' => 'required|NotIn:0'
        ];
        
        return $validator = Validator::make($input, $rules);
    }
    
    public function getAllTeamworkProjectCategories()
    {
        $action = 'projectCategories';
        $project_category_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
        
        return $response = self::teamworkGetRequest($project_category_hook_url);
    }
    
    public function getAllProjects()
    {
        $action = 'projects';
        $projects_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
        
        return $response = self::teamworkGetRequest($projects_hook_url);
    }
    
    public function getAllMessagesInProject($project_id)
    {
        $action = 'posts';
        $messages_hook = "http://". $this->tw_company .".teamwork.com/projects/".$project_id."/". $action .".json";
        return $response = self::teamworkGetRequest($messages_hook);
    }
    
    public function getAllUpcomingMilestonesInProject($project_id)
    {
        $action = 'milestones';
        $milestone_hook_url = "http://". $this->tw_company .".teamwork.com/projects/".$project_id."/". $action .".json?find=upcoming";
        
        return $response = self::teamworkGetRequest($milestone_hook_url);
    }
    
    public function removeTeamworkCompany()
    {
        return $single_company_hook_url = "http://". $this->tw_company .".teamwork.com/companies/.json";
    }
    
    public function getTestTaskTemplate()
    {
        $action = 'tasklists/templates';
        $template_hook = "http://". $this->tw_company .".teamwork.com/".$action.".json";
        
        return $response = self::teamworkGetRequest($template_hook);
    }
    
    public function companyIsAlreadyRegistered($userId){
        return Redirect::route('get.project.management', ['id' => $userId])->with('Success', 'Welcome back, <span class="orange-text">'.Auth::user()->fname.'</span>. You already have an account registered in teamwork with this user.');
    }
    
    public function teamworkFailureResponse($userId, $response) {
        return Redirect::route('get.project.management', ['id' => $userId])->with('Failure', '[Teamwork Failure]: Teamwork response: '.$response->MESSAGE);
    }
    
    public function createUserAndBootstrapTasks($tw_company_id, $pharmacy_id)
    {
        $teamwork = new TwApi($this);
        return $teamwork->bootstrapUserAndTasks($tw_company_id, $pharmacy_id);
    }
   
    public function failedToCreateNewMessage($created_message) {
        return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[Teamwork Failure Welcome Message]: Teamwork response: '.$created_message['tw_status']->MESSAGE);
    }
    
    public function successfulBootstrapProject() {
        return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Success', '[Teamwork Bootstrap Success]: Your company is now set up in teamwork!');
    }
    
    public function failedToCreateNewTasklist($new_tw_tasklist) {
        return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[Teamwork Failure New Tasklist]: Teamwork response: '.$new_tw_tasklist->MESSAGE);
    }
    
    public function addCompanyIdToTeamworkCompanyRecord($tw_company_id, $user)
    {
        $reader = new CompanyReader($this);
        $user_company_list = $reader->getAllUserCompanies($user->id);
        $company_id_list = array();
        foreach($user_company_list as $registered_company_id) {
            $tw_company = new TeamworkCompanies;
            $company = Company::find($registered_company_id);
            
            $tw_company->pharmacy_name = $company->name;
            $tw_company->pharmacy_owner = Auth::user()->fname.' '.Auth::user()->lname;
            $tw_company->owner_id = $user->id;
            $tw_company->teamwork_company_id = $tw_company_id;
            $tw_company->company_id = $registered_company_id;

            if($tw_company->save()) {
                $company_id_list[] = $registered_company_id;
            }
        }
        if(count($company_id_list) > 0) {
            return $tw_company_id;
        }
        
        throw new Exception($user->fname.'('.$user->email.') did not have registered companies to create Teamwork company profiles.');
    }
    
    public function getMasterProjectDetails()
    {
        $company_account_created = User::with('teamwork')->company()->get();
        $details = array();
        foreach($company_account_created as $company) {
            if(isset($company->teamwork->id)) {
                $details['company_id'] = $company->teamwork->teamwork_company_id;
                $details['projects'] = $company->teamwork->teamwork_project_ids;
            }
        }
        return $details;
    }
    
    public function updateEmployeeTwCompanyId($id) {
        if(is_null(Auth::user()->teamwork_company_id)){
            return Redirect::back()
                    ->with('Failure', 'You do not have a myPDS company id to give to this employee. Please set up your myPDS account <a class="orange-text" href="'.URL::route('get.project.management', ['id' => Auth::id()]).'">here</a>. If you already have, then please contact support using the question bubble in the bottom right hand corner of the window.');
        }
        $action = "people";
        $employee = User::find($id);
        $data = $this->getNewTeamworkEmployeeInput($employee);
        $created_user = self::newTwPostRequest($action, $data);
        if (isset($created_user->STATUS) && $created_user->STATUS === "Error") {
            $tw_api = new TwApi($this);
            $pre_existing_account_id = $tw_api->getPreExistingUser($employee->email);
            \Log::info('Previous Account Available', ['Had to grab pre-existing acc for id' => $employee->id]);
        }
        $user_tw_id = isset($pre_existing_account_id) ? $pre_existing_account_id : $created_user['tw_user_id']->id;
        $addToProjects = $this->addUserToAvailableProjects($employee->companies, $user_tw_id);
        if(count($addToProjects) == 0) {
            return Redirect::back()->with('Failure', 'Could not add the user to projects!');
        }
        $employee->teamwork_company_id = Auth::user()->teamwork_company_id;
        $employee->teamwork_user_id = $user_tw_id;
        $employee->job_title = User::getUserJobTitleFromHS($employee->email);
        $employee->project_name = $data['person']['user-name'];
        $employee->project_password = $data['person']['password'];
        $employee->save();
        return Redirect::back()->with('Success', 'Successfully added employee to myPDS. They can now view the information from your myPDS account.');
    }
    
    public function getPeopleInTeamwork($action){
        $companyuser_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
        return $tw_response = json_decode(self::teamworkGetRequest($companyuser_hook_url));
    }
    
    public function addUserToAvailableProjects($employee_companies, $user_id) {
        $companies = json_decode($employee_companies, true);
        $projects = array();
        foreach($companies as $company_id => $employee_type) {
            $projects[] = $this->getProjectIdFromTeamworkRecord($company_id);
        }
        if(count($projects) > 0) {
            foreach($projects as $project_id) {
                $action = "projects/".$project_id."/people/".$user_id;
                $tw_api = new TwApi($this);
                $created_user = $tw_api->curlPostRequestUrlOnly($action);
                if (isset($created_user->STATUS) && $created_user->STATUS === "Error") {
                    \Log::info('Add to project failure', ['Could not add user to project' => 'not sure why']);
                }
            }
            return $projects;
        }
        
        return [];
    }
    
    public function getProjectIdFromTeamworkRecord($company_id){
        $teamwork_company = TeamworkCompanies::where('company_id', $company_id)->first();
        $teamwork_project_id = str_replace(':', '', $teamwork_company->teamwork_project_ids);
        return $teamwork_project_id;
    }
    
    public function getEmployeesInCompany($company_id){
        $action = 'companies/'.$company_id.'/people';
        $companyuser_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";

        return $tw_response = json_decode(self::teamworkGetRequest($companyuser_hook_url));
    }
    
    public function getNewTeamworkEmployeeInput($employee)
    {
        return array(
            'person' => array(
                'first-name' => $employee->fname,
                'last-name' => $employee->lname,
                'email-address' => $employee->email,
                'user-name' => preg_replace('/-/', '//', $employee->email),
                'password' => $employee->id.'_tw_pass',
                'company-id' => Auth::user()->teamwork_company_id,
                'autoGiveProjectAccess' => 'yes',
                'administrator' => 'no',
                'canAddProjects' => 'no',
                'sendWelcomeEmail' => 'no',
                'welcomeEmailMessage' => 'You are now added to your company project! Welcome to the future of teamwork.',
                'title' => is_null($employee->job_title) ? 'No Title Found' : $employee->job_title
            )
        );
    }
   
    
    public function addNewTwCompanyIdToUserData($tw_id, $user) {
        $user->teamwork_company_id = $tw_id;
        if ($user->save()) {
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Success', '[SUCCESS]: Successfully registered your company for <a target="_blank" class="orange-text" href="http://teamwork.com/">Teamwork</a> project management tools. &nbsp;<a href="#" class="orange-text project-access" target="_blank">Login</a>');
        }
        
        return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[FAILURE]: Could not store the new company instance id for Teamwork Company Registration.');
    }
    
    public function teamworkPostRequest($action, $json){
        $data_string = json_encode($json);   
        $company_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HEADER, 1);
        curl_setopt($channel, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt( $channel, CURLOPT_URL, $company_hook_url );
        curl_setopt($channel, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string),
            "Authorization: BASIC ". base64_encode( $this->api_key .":xxx"))
        );

        $result = curl_exec($channel);
        $response = json_decode($result);
        curl_close( $channel );
        return $response;
    }
    
    public function newTwPostRequest($action, $json){
        $data_string = json_encode($json);                                                                               
        $company_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
        $curl_response = $this->curlPostRequest($company_hook_url, $data_string);
        $headers = explode("\r\n", $curl_response);
        $status = $this->getReadableResponse($headers);
        if(preg_match('/id/', $status)) $id = $status;
        $tw_status = json_decode($status);
//        dd($tw_status);
        if(isset($tw_status->MESSAGE) && $tw_status->STATUS === "Error") {
            return $tw_status;
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[TEAMWORK ERROR]: Teamwork post request response -> ['.$tw_status->MESSAGE.']');
        }
        $id_of_new_item = explode(':', $id);
        $item_id = new stdClass();
        $item_id->id = trim($id_of_new_item[1]);
        $response['tw_user_id'] = $item_id;
        $response['tw_status'] = $tw_status;
        return $response;
    }
    
    public function getReadableResponse($headers) {
        $id = 'id:0';
        $status = NULL;
        foreach($headers as $header) {
            if (strpos($header, 'id:') === 0) {
                return $id = $header;
            }
            if (strpos($header, '{"STATUS":') === 0) {
                return $status = $header;
            }
            if (strpos($header, '{"MESSAGE":') === 0) {
                return $status = $header;
            }
        }
    }
    
    public function curlPostRequest($url, $data) {
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HEADER, 1);
        curl_setopt($channel, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt( $channel, CURLOPT_URL, $url );
        curl_setopt($channel, CURLOPT_POSTFIELDS, $data);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data),
            "Authorization: BASIC ". base64_encode( $this->api_key .":xxx"))
        );
        $result = curl_exec($channel);
        curl_close( $channel );
        return $result;
    }
    
    public function teamworkGetRequest($url){
        $channel = curl_init();
        
        curl_setopt( $channel, CURLOPT_URL, $url );
        curl_setopt( $channel, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $channel, CURLOPT_HTTPHEADER, 
            array( "Authorization: BASIC ". base64_encode( $this->api_key .":xxx" ))
        );

        $results =  curl_exec ( $channel );

        curl_close ( $channel );
        
        return $results;
    }
    
    public function getAvailableProjects($requesting_user = null)
    {
        if(isset($requesting_user)){ $this->requesting_user['current_user'] = $requesting_user; }
        //find company owner for current user
        if(isset($this->requesting_user['current_user'])) {
//            $user = User::find($this->requesting_user['current_user']);
            $company_account_created = TeamworkCompanies::where('company_id', $requesting_user)->get();
        }else{
            $user_id = isset($this->requesting_user['coach/IS']) ? $this->requesting_user['coach/IS'] : $this->requesting_user['current_user'];
            $user = $this->user->find($user_id);
            $company_account_created = $user->with('teamwork')->get();
        }
        
        $tw_company = $this->getTwCompanyFromResult($company_account_created);
        if(isset($tw_company)){
            //get project names and id's
            return $project_list = $this->getProjectList($tw_company);
        }
        
        return ['none' => '--No Projects Found--'];
        
    }
    
    public function getTwCompanyFromResult($company_account_created) {
        foreach($company_account_created as $company) {
            if(isset($company->teamwork->id)) {
                return $company->teamwork;
            }
            if(isset($company->owner_id)){
                return $company;
            }
        }
        
        return NULL;
    }
    
    public function getProjectList($tw_company) {
        $projects_available = json_decode($this->getAllProjects(), true);
        $projects_to_match = array();
//        dd($projects_available['projects']);
        foreach($projects_available['projects'] as $project) {
            $projects_to_match[$project['id']] = $project['name'];
        }
        $projects = explode(':', $tw_company->teamwork_project_ids);

        $project_list = array();
        $project_list[0] = '-- Select a Project--';
        foreach($projects as $project) {
            if(array_key_exists($project, $projects_to_match)) {
                $project_list[$project] = $projects_to_match[$project];
            }
        }

        return $project_list;
    }

    public function checkAccess($project_company_id) {
        $company = Company::find($project_company_id);
        $user = $this->user->find($company->admin_id);
        $who_is_looking = array();
        if($user->id != Auth::id()) {
            if( (isset($user->my_is) || isset($user->my_coach)) && ($user->my_is == Auth::user()->is_id || $user->my_coach == Auth::user()->coach_id) ) {
                $who_is_looking['coach/IS'] = $user->id;
                return $who_is_looking;
            }
            $who_is_looking['unauthorized'] = 'Unauthorized';
            return $who_is_looking;
        }
        $who_is_looking['current_user'] = $user->id;
        return $who_is_looking;
    }
    
}
