<?php

/**
 * UserController
 *
 * This controller houses the methods necessary to complete all user actions
 * These include, but not limited to:
 * Logging in, reading profile data, updating profile data, adding new profile data,
 * and presenting the views available to the particular user.
 *
 * @author derek
 */
use UserRepo\Storage\User\UserRepository as UserRepoInterface;
use GroupRepo\Permissions\Groups\GroupRepository as GroupInterface;
use MarketRepo\production\Vendors\PdsVendorRepository as VendorInterface;
use UserRepo\Storage\Forums\TopicRepository as TopicInterface;
use NotificationRepo\Notifications\NotificationRepository as NotificationInterface;
use EventsRepo\Storage\Events\EventsRepository as EventsInterface;
use FileRepo\Storage\Resources\FileRepository as FileInterface;
use TeamworkRepo\TeamworkApi as TwApi;
//use TeamworkRepo\Storage\API\TeamworkRepository as TeamworkInterface;

class UserController extends \BaseController {

    //table primary key
    protected $primaryKey = 'id';

    /**
     * Instantiate filters and Service Providers
     */
    public function __construct(EventsInterface $events, FileInterface $files, UserRepoInterface $users, GroupInterface $groups, VendorInterface $vendors, TopicInterface $topics, NotificationInterface $notifications) {
        $this->beforeFilter('csrf', array('on' => 'login'));
        $this->users = $users;
        $this->groups = $groups;
        $this->vendors = $vendors;
        $this->topics = $topics;
        $this->notifications = $notifications;
        $this->api_key = 'cut656tree';
        $this->tw_company = 'pharmacyowners';
        $this->impulse_tw_id = '45584';
        $this->events = $events;
        $this->files = $files;
        if (Auth::check()) {
            View::share('usr_email', Auth::user()->email);
            View::share('usr_name', Auth::user()->fname);
            View::share('user_permissions', explode(':', Auth::user()->groups));
        }
        $css = [ 'main', 'dash', 'forum'];
        View::share('css', $css);
        View::share('pageTitle', 'Dashboard | PDSadvantage');
    }
    public function unsubscribeUser($id) {
        $notification = Notifications::where('user_id', $id)->first();
        if($notification == null) {
            return dd('There was an error');
        } else {
            $notification->digest = 0;
            if($notification->save()) {
                return dd('You have been unsubscribed');
            }
        }
        
        
    }
    public function testDailyDigest($id = 92) {
        return \User::sendDailyDigestEmail($id, 'preview');
    }
    
    public function sendDailyDigestEmailNow($id) {
        return User::sendDailyDigestEmail($id);
    }
    
    public function getAllTeamworkUsers() {
        $tw_api = new TwApi($this);
        $action = 'people';
        $tw_users = $tw_api->getPeopleInTeamwork($action);
        $data = array();
        foreach($tw_users['people'] as $person) {
            $user = User::where('email', $person['email-address'])->first();
            if(count($user) > 0) {
                $data[$user->id] = [
                    'teamwork_company_id' => $person['companyId'],
                    'project_name' => $person['user-name'],
                    'teamwork_user_id' => $person['id']
                ];
                $user->update($data[$user->id]);
                continue;
            }
        }
        dd($data);
    }
    
    /**
     * Testing
     */
    public function testing() {
        return $this->getAllTeamworkUsers();
        
        $sql = "SELECT pds.user_id, pds.hs_acc_number, pds.first_name, pds.last_name, "
                . "pds.email, pds.phone, pds.company_name, pds.company_website, pds.address, pds.city, "
                . "pds.state_or_region, pds.postal_code, pds.company_logo, pds.category, pds.subCategory, "
                . "pds.description, pds.company_blog_or_vlog, pds.term_agreement, pds.video_permission, "
                . "pds.approved, "
                . "cat.id, cat.category_name, "
                . "subcat.id, subcat.name, pds.id from `pds_vendors` AS pds  "
                . "INNER JOIN `categories` as cat ON cat.id = pds.category "
                . "JOIN `sub_categories` as subcat ON subcat.id = pds.subCategory";
        $vendors = DB::select($sql);
        
        /*$vendors = Vendor::with('MarketCategories', 'MarketSubCategories')->get();
        dd($vendors); */
        $csv = "id, user_id, hs_acc_number, first_name, last_name, email, phone, company_name, company_website, address,"
                ."city, state_or_region, postal_code, company_logo, category, category_id, subCategory, subCategory_id,"
                ."description, company_vlog_or_blog, term_agreement, video_permission, approved\r\n";
        foreach($vendors as $vendor) {
            $csv .= '"'.$vendor->id.'", "'.$vendor->user_id.'", "'.$vendor->hs_acc_number.'", "'.$vendor->first_name
                    .'", "'.$vendor->last_name.'", "'.$vendor->email.'", "'.$vendor->phone.'", "'.$vendor->company_name
                    .'", "'.$vendor->company_website.'", "'.$vendor->address.'", "'.$vendor->city.'", "'.$vendor->state_or_region
                    .'", "'.$vendor->postal_code.'", "'.$vendor->company_logo.'", "'.$vendor->category_name.'", "'.$vendor->category
                    .'", "'.$vendor->name.'", "'.$vendor->subCategory.'", "'.$vendor->description.'", "'.$vendor->company_blog_or_vlog
                    .'", "'.$vendor->term_agreement.'", "'.$vendor->video_permission.'", "'.$vendor->approved."\"\r\n";
            
        }
        $date = date('F-d-Y');
        $directory = public_path() . '/storage/reports/';
        $filename = $date . '_Marketplace_export.csv';
        $fileSave = file_put_contents($directory . $filename, $csv);
        
        if($fileSave != false) {
            $url = URL::to('/storage/reports/');
            echo '<iframe id="download" class="hidden" src="' . $url . '/' . $filename . '">Your browser doesn\t support this download</iframe>';
            return Redirect::back()->with('Your Report was created');
        } else {
            Session::flash('There was an error saving your report.');
        }
    }

    /**
     * Display user login view
     */
    public function showLogin() {
        $cookie = Cookie::get('company');
        if (isset($cookie)) {
            $user = new User();
            $userObj = $user->queryUser($cookie);
            $user->handleSessionCreation($userObj);
            $id = User::getUserId();
            Auth::loginUsingId($id);
            return Redirect::route('dash.main');
        } else {
            return View::make('Login.index');
        }
    }

    /**
     * Display captcha login
     */
    public function showCaptchaLogin() {
        return View::make('Login.captcha-index');
    }

    /**
     * Show dashboard to logged in user
     */
    public function checkUserConfirmation($email) {
        $checkIfConfirmed = Auth::user()->confirmed_user;
        if ($checkIfConfirmed == 0) {
            return 0;
        }
        return 1;
    }

    public function checkWhitelist($id) {
        $check_whitelist = Whitelists::where('user_id', '=', $id)->first();
        if (is_null($check_whitelist)) {
            //add ip to whitelist
            $whitelist = Whitelists::addCurrentToWhitelist($id);
            //update user record with associated whitelist id
            $user = $this->users->find(Auth::id());
            $user->whitelisted_ip_list = $whitelist;
            $user->save();
        }
    }

    public function showDash() {
        $id = Auth::id();
        if($this->checkIfUserAccountIsConfirmed($id)) {
            return View::make('Signup.block-from-dash')->with('Failure', 'Please confirm your email');
        }
        $this->checkWhitelist($id);
        $permissions = \User::getCurrentPermissions();
        $vendor['vendor'] = strpos($permissions, '3') > -1 ? true : false;
        $vendor['percentage'] = $vendor['vendor'] == true ? self::vendorSignupCompletionPercentage() : false;
        $vendor['vendor-data'] = $vendor['percentage'] > 0 ? Vendor::select('company_name')->where('user_id', '=', $id) : 'false';
        $forum['recent'] = self::getRecentForum();
        $forum['popular'] = self::getPopularForum();
        $events['upcoming_events'] = self::getUpcomingEvents();
        $blog_posts = FeedReader::read('http://www.pharmacyowners.com/blog/rss.xml');
        $newest_resources = self::getNewestResources();
        $dan_quote = DB::table('dan_quote')->select(DB::raw('announcement as danQuote'))->first();
        $notifications = $this->notifications->getAllNotifications($id);
        $event_states = $this->events->getEventStatesArray();
        return View::make('dashboard.group-dash-views.dashboard-main', compact('vendor', 'forum', 'events', 'blog_posts', 'newest_resources', 'dan_quote', 'notifications'))
                ->nest('pharmacy_setup_modal', 'dashboard.partials.pharmacy-setup', compact('event_states'));
    }

    public function checkIfUserAccountIsConfirmed($id) {
        $email = User::getUserEmail($id);
        if ($this->checkUserConfirmation($email) != 1) {
            $data = [
                'user_email' => $email,
                'siteKey' => '6Lc7e_8SAAAAAJiiEc-ZUin_pM0MWifqmeDr1jDX',
                'secret' => '6Lc7e_8SAAAAABiwwVvvyg7pWjntWyZ4fI291G8P',
                'lang' => 'en'
            ];
            return 1;
        }

        return 0;
    }
    
    public function getUpcomingEvents()
    {
        return $upcoming = $this->events->allUpcomingEvents();
    }
    
    public function getNewestResources()
    {
        return $resources = $this->files->latestResources();
    }

    public function getRecentForum($total = 5) {
        $recent = $this->topics->getRecentTopics($total);
        return $recent;
    }
    
    public function sendAllDigests() {
        set_time_limit(0);
        $daily = \Notifications::where('digest', '1')->get();
        foreach($daily as $single) {
            $mail = User::sendDailyDigestEmail($single->user_id);
        }
    }


    public function getDailyDigest($id) {
        $permissions = \User::getUserPermissions($id);
        $vendor['vendor'] = strpos($permissions, '3') > -1 ? true : false;
        $vendor['percentage'] = $vendor['vendor'] == true ? self::vendorSignupCompletionPercentage() : false;
        $vendor['vendor-data'] = $vendor['percentage'] > 0 ? Vendor::select('company_name')->where('user_id', '=', $id) : 'false';
        $forum['recent'] = self::getRecentForum(10);
        $forum['popular'] = self::getPopularForum(10);
        $notifications['topics'] = $this->notifications->getNotificationsForDashboard($id, 'topics');
        $notifications['resources'] = $this->notifications->getNotificationsForDashboard($id, 'resources');
        return View::make('emails.daily-digest', compact('vendor', 'forum', 'notifications'));
    }

    public function getPopularForum($total = 5) {
        $recent = $this->topics->getRecentPopularTopics($total);
        return $recent;
    }

    public function getUsersPosts($id) {
        $forum['posts'] = $this->topics->getTopicsByUser($id);
        $forum['replies'] = $this->topics->getRepliesByUser($id);
        return $forum;
    }

    protected function vendorSignupCompletionPercentage() {
        $vendor = new Vendor();
        return $vendor->signupCompletionPercentage();
    }

    private function ForcePush($email) {
        //initialize model objects to access the non static methods
        $failedLogins = new FailedLogins();
        $whitelists = new Whitelists();

        //retrieve necessary user information for protection
        $ip = FailedLogins::getIpAddress();
        $user_id = $this->users->findByEmail($email);

        //add record to login failure table
        $failedLogins->addFailedLoginAttempt($user_id->id, $email, $ip);
        //set custom login status time frame
        $failedLogins->setTimeFrame(1);
        $response = $failedLogins->getLoginStatus();

        return $response;
    }

    /**
     * HANDLES USER LOGIN
     *
     * This function creates a new user in the database
     * or, if the user already exists, it logs them in as well as creating 
     * a session cookie to allow for easy logins throughout the day; even if 
     * they exit the browser
     */
    public function login() {
        $user = new User;
        $company = trim(Input::get('company-name'));
        $first_name = trim(Input::get('fname'));
        $last_name = trim(Input::get('lname'));
        $email = self::cleanEmail(Input::get('email'));
        $password = Input::get('password');
        $accountType = Input::get('account-type');
        $action = Input::get('action');
        if ($action == 'login') {
            return $this->loginUser($user, $email);
        } else {
            return $this->registerUser($user, $company, $first_name, $last_name, $email, $password, $accountType);
        }
    }
    
    public function checkIfPdsEmployee($email) {
        $user = User::where('email', $email)->first();
        $permissions = explode(':', $user->groups);
        if(in_array('14', $permissions) && !in_array('5', $permissions)) {
            return $this->addEmployeeToPdsCompany($user);
        }
        return 'Not pds';
    }
    
    public function addEmployeeToPdsCompany($user) {
        $pds_company = Company::where('owner_email', 'msc@pharmacy-owners.com')->first();
        $company_id = $pds_company->id;
        $employee_type = $user->employee_type === 0 ? "pending" : (string)$user->employee_type;
        $user->companies = json_encode([$company_id => $employee_type]);
        return $user->save();
    }
    
    public function registerUser($user, $company, $first_name, $last_name, $email, $password, $accountType) {
        $validator = self::getLoginValidator();
        if ($validator->passes()) {
            $id = $user->createUser($company, $first_name, $last_name, $email, $password, $accountType);
            $updatePermissions = $this->groups->updateUserPermissions($id);
            self::sendConfirmation($email, $id);
            return Redirect::route('dash.main');
        } else {
            $check_trash = User::onlyTrashed()->where('email', $email)->first();
            if (is_object($check_trash)) {
                return Redirect::back()->with('Failure', 'It seems you had a previous account with these credentials that was terminated by PDS.<br/><br/> Please contact Koreen @ <a href="tel:5612752659">561-2752659</a> or <a href="mailto:koreen@pharmacy-owners.com">koreen@pharmacy-owners.com</a> to have your previous information permanently removed for a fresh registration.')->withInput();
            }
            return Redirect::back()->withErrors($validator->messages())->withInput();
        }
    }

    public function manualVerifyUser() {
        $data = Input::all();
        if ($data['email'] == '' || $data['email'] == null) {
            return Redirect::back()->with('Error', 'Please Try Again');
        }

        $resp = $this->checkCaptcha($data);

        if ($resp->errorCodes == 'error') {
            return Redirect::back()->with('Error', 'Please Try Again');
        } elseif ($resp != null && $resp->success) {
            return $this->manualConfirm($data);
        } else {
            return $resp;
        }
    }

    public function manualConfirm($data) {
        $user = $this->users->findByEmail($data['email']);
        $user->confirmed_user = 1;
        if ($user->save()) {
            User::updateConfirmedInHubspot($user->email);
            return Redirect::route('dash.main');
        }
    }

    public function checkCaptcha($data) {
        File::requireOnce('vendor/recaptcha/recaptchalib.php');
        $secret = '6Lc7e_8SAAAAABiwwVvvyg7pWjntWyZ4fI291G8P';
        $reCaptcha = new ReCaptcha($secret);
        if ($data["g-recaptcha-response"]) {
            $resp = $reCaptcha->verifyResponse(
                    $_SERVER["REMOTE_ADDR"], $data["g-recaptcha-response"]
            );
            return $resp;
        } else {
            return 'error';
            return Redirect::back()->with('Error', 'Please Try Again');
        }
    }

    public function loginUser($user, $email) {
        $credentials = $this->getLoginCredentials();
        if (Auth::attempt($credentials, Input::has('remember-my-creds'))) {
            $checkIfPdsEmployee = $this->checkIfPdsEmployee($email);
            if($checkIfPdsEmployee !== 'Not pds') {
                \Log::info('PDS Employee Connected to PDS Company', ['PDS Employee Connected' => 'Employee success: '.$email]);
            }
            if (Input::get('bot-blocker')) {
                autoBotBlocker(Input::get('bot-blocker'));
            }
            $userObj = $user->queryUser($email);
            $user->handleSessionCreation($userObj);
            $this->checkConfirmedEmail($email);
            $cookieCreate = Cookie::make('company', $email, 11520); //sets auto login cookie
            return $this->updatePermissionsThenSendToDash($userObj->id);
        } else {
            return $this->runFailedLoginBruteForceProtectionThenBackToLogin($email);
        }
    }

    public function updatePermissionsThenSendToDash($id) {
        $updatePermissions = $this->groups->updateUserPermissions($id);
        if ($updatePermissions == 1) {
            return Redirect::intended('dash.main');
        } else {
            return Redirect::back()->with('something went wrong');
        }
    }

    public function runFailedLoginBruteForceProtectionThenBackToLogin($email) {
        $user = $this->users->findByEmail($email);
        if (is_null($user))
            return Redirect::back()->with('Unsuccessful Login', 'Please choose a valid email.');
        $force_field_responder = self::ForcePush($email);

        switch ($force_field_responder['status']) {
            case 'whitelist_approved':
                //continue on without interruption until they log in
                break;
            case 'safe':
                //continue on until throttle is jostled again
                break;
            case 'error':
                //log the message into log file
                $error_log = $force_field_responder['message'];
                return View::make('Login/force-blocker')->with('time_remaining', $error_log);
                break;
            case 'delay':
                $remaining_time_before_login = $force_field_responder['message'];
                return View::make('Login/force-blocker')->with('time_remaining', $remaining_time_before_login);
                break;
            case 'captcha':
                $message = $force_field_responder['message'];
                return View::make('Login/captcha-index')->with('message', $message);
                break;
            default:
                throw new Exception('Could not get a read from the force field responder. We are flying blind out here, sir.');
                break;
        }
        return Redirect::route('login.form')->with('Unsuccessful Login', 'Invalid email or password.')->withInput();
    }

    public function updateIntegrationInformation($user, $email) {
        $user_data = $this->users->find(Auth::id());
        if ($user_data->hs_acc_number == 'none') {
            $companyId = $this->updateHubspotAccountId($user, $user_data, $email);
            if ($user_data->sf_id == 'none' && $companyId != 'none') {
                $this->updateSalesforceId($user, $user_data, $companyId);
            }
            return Redirect::route('dash.main')->with('Success', 'Login Successful. Have a nice day.');
        } elseif ($user_data->sf_id == 'none') {

            $this->updateSalesforceId($user, $user_data, Auth::user()->hs_acc_number);
            return Redirect::route('dash.main')->with('Success', 'Login Successful. Have a nice day.');
        }
        return $user_data;
    }

    public function checkConfirmedEmail($email) {
        $checkIfConfirmedEmail = Auth::user()->confirmed_user;
        if ($checkIfConfirmedEmail == 0) {
            self::sendConfirmation($email, Auth::id());
            return Redirect::route('dash.main');
        }
    }

    public function autoBotBlocker($check_bot_blocker) {
        //var_dump($check_bot_blocker);
        if ($check_bot_blocker) {
            if ($check_bot_blocker == 16) {
                //continue
            } else {
                return Redirect::back()->with('Unsuccessful Login', 'Your math was incorrect!');
            }
        }
    }

    protected function getLoginValidator() {
        return Validator::make(Input::all(), [
            'fname' => 'required',
            'lname' => 'required',
            'company-name' => 'required',
            'account-type' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);
    }

    //collect the necessary login credentials from form submit
    protected function getLoginCredentials() {
        return [
            'email' => self::cleanEmail(Input::get('email')),
            'password' => Input::get('password')
        ];
    }

    protected function cleanEmail($email) {
        return filter_var($email, FILTER_SANITIZE_EMAIL);
    }

    // does validation onto the user_id (making user it's a number and > 0)
    private function cleanUserId($user_id) {
        $user_id = intval($user_id);

        if ($user_id === 0) {
            throw new Exception('Invalid user ID');
        }

        return $user_id;
    }

    /**
     * ===================================
     * EMAIL CONFIRMATIN LOGIC METHODS
     * ===================================
     */
    //Show email Confirmation
    public function getConfPage($token = null, $id) {
        //if the token that comes through doesn't exist
        if (is_null($token))
            return Response::make('Token either is incorrect or does not exist! Please request another or try again.');

        return View::make('Signup.confirm-email')->with('token', $token)->with('id', $id);
    }

    //Email account confirmation
    public function sendConfirmation($email, $id) {
        return User::sendConfirmationRequest($email, $id);
    }

    
    public function checkConfToken() {
        $user_data = new User();
        $token = Input::only('token');
        $id = Input::get('user_id');
        $tokenInQuestion = self::getStoredConfEmailToken($token, $id);
        $user = $this->users->find($id);
        $email = $user->email;

        if ($tokenInQuestion) {
            if($user->imported == 1) return View::make('Signup.choose-password')->with('user', $user);
            //set up user session and present the dashboard
            $cookieCreate = Cookie::make('company', $email, 11520); //sets auto login cookie
            $userObj = $user_data->queryUser($email); //retrieves user data for session variables
            $user_data->handleSessionCreation($userObj); //creates user session
            $id = User::getUserId(); //retrieves user id from session
            self::deleteConfToken($id); //delete confirmation token
            $data = $user_data->queryUserBasicInfo($id); //retrieves user's basic information for dash
            //$this->checkAccountType($user);
            if (Auth::login($user, true)) {
                return Redirect::route('dash.main')->withCookie($cookieCreate);
            } else {
                return Redirect::route('login.form')->with('Failure', 'Could not log in. Please try again.');
            }
        } else {
            return Redirect::to('Signup.request-confirm')->with('user_email', $email);
        }
    }
    
    public function changeImportedPassword() {
        
        $user = User::find(Input::get('userid'));
        $user->confirmed_user = 1;
        $updated = User::updateConfirmedInHubspot($user->email);
        $user->imported = 0;
        $user->password = Hash::make(Input::get('password'));
        Session::put('imported', 'Thanks for activating your account, Please log in now!');
        if($user->save()) {
            return Redirect::route('login.form');
        }
        
    }

    //checks to see if confirmation token recieved can be used to reference the relative stored token
    //in the database
    private function getStoredConfEmailToken($token, $id) {
        $userInQuestion = $this->users->find($id);
        if ($userInQuestion->confirmed_user) {
            return Redirect::route('dash.main');
        }
        $tokenInQuestion = DB::table('confirmations_for_user')->where('confirmations_tokenscol', '=', $token)->first();
        return Response::json($tokenInQuestion->confirmations_tokenscol);
    }

    //remove confirmation token from database table so user can access the dashboard.
    private static function deleteConfToken($id) {
        $user = User::find($id);
        $user->confirmed_user = 1;
        $updated = User::updateConfirmedInHubspot($user->email);
        $user->save();
        $deleteToken = DB::table('confirmations_for_user')->where('id', '=', $id)->delete();
    }

    /**
     * HUBSPOT ACCOUNT HANDLERS
     * Includes all methods of accessing user hubspot accounts, if they have one anyways
     */
    //checks to see if the user has a hubspot account
    public static function checkHSAccount($email) {
        $user = new User();
        return $user->checkForHubspotAccount($email);
    }

    //retrieve contact associated company id
    public static function retrieveHSCompanyId($email) {
        $user = new User();
        return $user->getHubspotCompanyId($email);
    }
    
    public function getHSCompany($id) {
        $companyId = $this->users->find_hs_acc_num($id);
    }

    public function updateSalesforceId($user, $user_data, $companyId) {
        $salesforceId = $user->getSalesforceCompanyId($companyId);
        if ($salesforceId != 'none') {
            $user_data->sf_id = $salesforceId;
        }
        if ($user_data->save()) {
            return $salesforceId;
        }
    }

    public function addNewUserPermissions($permissions = '0:12', $email) {
        $groupIds = explode(':', $permissions);
        $contacts = HubSpot::contacts();
        $contact = $contacts->get_contact_by_email($email);
        $vid = $contact->properties->vid;
        $permissions = $this->getHubspotLabels($groupIds);
        $updatedContact = $contact->update_contact($vid, $permissions);
    }

    public function getHubspotLabels($groupIds) {
        foreach ($groupIds as $id) {
            $group = $this->groups->getGroupById($id);
            $labels[$group->hs_field] = 'yes';
        }
        return $labels;
    }

    public function updateHubspotAccountId($user, $user_data, $email) {
        $user_id = Auth::id();
        $guideAds = GuideAds::where('user_id', $user_id)->first();
        $guideEntries = GuideEntries::where('user_id', $user_id)->first();
        $apiData = $user->getUserHSAccountId($email, $user_data->fname, $user_data->lname);
        if ($apiData != 'none') {
            $user_data->hs_acc_number = $apiData;
            if (isset($guideAds->hs_acc_number)) {
                $guideAds->hs_acc_number = $apiData;
            }
            if (isset($guideEntries->hs_acc_number)) {
                $guideEntries->hs_acc_number = $apiData;
            }
            if ($user_data->save()) {
                return $apiData;
            }
        }
    }

    public function showUserProfileById($targetId) {
        $target['id'] = $targetId;
        $target['user'] = User::find($targetId);
        $user = User::find($targetId);
        $forum = self::getUsersPosts($targetId);
        $states_list = $this->events->getEventStatesArray();
        $states_list[-1] = '--Select a State--';
        $notifications = Notifications::where('user_id', $targetId)->first();
        $digest = $notifications != null ? $notifications->digest : 0;
        return View::make('dashboard.group-dash-views.dash-profile', compact('target', 'forum', 'user', 'states_list', 'digest'));
    }
    
    public function showUpdatePermissions($targetId) {
        $target['id'] = $targetId;
        $target['user'] = $this->users->find($targetId);
        $permissions = $this->groups->allInArrayForPermissionUpdates();
        return View::make('dashboard.group-dash-views.add-permissions', compact('target', 'permissions'));
    }
    
    public function updatePermissions() {
        $input = Input::get();
        $new_permissions = array();
        foreach($input as $key => $permission_id) {
            if(intval($permission_id) && $key != 'target_id') $new_permissions[$key] = $permission_id;
        }
        $user = $this->users->find($input['target_id']);
        $update_hs_account = $this->updateHubspotPermissions($user, $new_permissions);
        $user->groups = implode(':', $new_permissions);
        if($user->save() && $update_hs_account === 'success'){
            return Redirect::route('accounts.index')->with('Success', 'Successfully updated user permissions for '.ucwords($user->pharmacy));
        }
        
        return Redirect::route('accounts.index')->with('Failure', 'Failed to update user permissions for '.ucwords($user->pharmacy));
    }
    
    public function updateHubspotPermissions($user, $new_permissions) {
        $contact = HubSpot::contacts();
        $hs_user = $contact->get_contact_by_email($user->email);
        if(isset($hs_user->status)) {
            if(intval($user->vid)) {
                $hs_user = $contact->get_contact_by_id($user->vid);
            }else{
                $hs_user = $user->addUserToHubspot($user);
            }
        }
//        dd($hs_user);
        if(isset($hs_user->status) && $hs_user->status == 'error') throw new Exception('Could not save the new permission updates in Hubspot. The user doesn\'t exist inside the hubspot system. Please Contact Support');
        $user->vid = $hs_user->vid;
        $leftover_hs_permissions = $this->groups->allInArrayForPermissionUpdates();
        $params = [];
        foreach($new_permissions as $hs_field_name => $permission_id) {
            $key_to_find = array_search($hs_field_name, $leftover_hs_permissions);
            if($key_to_find) {
                unset($leftover_hs_permissions[$key_to_find]);
            }
            $params[$hs_field_name] = 'true';
        }
        foreach(array_slice($leftover_hs_permissions, 1) as $id => $hs_field_name) {
            $params[$hs_field_name] = 'false';
        }
        $update_contact = $contact->update_contact($user->vid, $params);
        if (is_null($update_contact)) {
            if($user->save()) {
                return 'success'; 
            }
            
        }
        
        throw new Exception('Could not save the new permission updates in Hubspot. Error: '.var_dump($update_contact));
    }

    public function hideNotification() {
        if (Request::ajax()) {
            $data = Input::all();
            $response = $this->notifications->hideNotification($data);
            return $response;
        }
        return 'You are doing something bad';
    }

    public function addNotification() {
        if (Request::ajax()) {
            $data = Input::all();
            $notification = new Notifications();
            $response = $notification->addNewNotification($data);
            return $response;
        }
        return 'You are doing something bad';
    }
    
    public function saveEmployees() {
        $data = Input::all();
        foreach($data['coach_id'] as $key => $value) {
            $user = User::find($key);
            $user->coach_id = $data['coach_id'][$key];
            $user->is_id = $data['is_id'][$key]; 
            $user->project_name = $data['teamwork_id'][$key];
            $user->project_password = $data['teamwork_password'][$key];
            $user->save();
        }
        return Redirect::route('acl.control')->with('Success', 'Employees Successfully Updated');
    }

    public function showNewUsers($email = false, $message = 'Welcome to the Approval System.') {
        $users = $email == false ? User::getAllNewUsers() : User::getUserByEmail($email);
        $accessGroups = AccessGroup::lists('name', 'id');
        $accessGroups[-1] = 'Select An Access Level';
        return View::make('dashboard.Admin.approvals', compact('users', 'accessGroups'))
                ->with('message', $message);
    }
    
    public function approveNewUser($userId, $accessLevel) {
        if(in_array(1, explode(':', Auth::user()->groups))) {
            $groups = AccessGroup::find($accessLevel);
            $permissions = $groups->permissions;
            $user = User::find($userId);
            if($user != null) {
                $user->groups = $permissions;
                if($user->save()) {
                    $groupsList = Group::lists('id', 'hs_field');
                    $permissionsArray = explode(':', $permissions);
                    $newPermissions = [];
                    foreach($groupsList as $key => $value){
                        if(in_array($value, $permissionsArray)) {
                            $newPermissions[$key] = $value;
                        }
                    }
                    $updated = $this->updateHubspotPermissions($user, $newPermissions);
                    if($updated = 'success') {
                        return $this->showNewUsers($email = false, $user->fname.' '.$user->lname.' was approved.');
                    }

                }
            }
            
            
        }
        
        return $this->showNewUsers($email = false);
        
            
        
    }
    
    public function addNewUser($input = false) {
        $input = $input != false ? $input : Input::all();
        $validator = self::getUserValidator();
        if ($validator->passes()) {
            $user = $this->createImportedUser($input);
            $added = Company::updateMember($user->id, $input['company'], $input['access']);
            if($added) {
                return Redirect::back()->with('Success', 'You created '.ucwords($input['fname']).'\'s account.  Please have them log in to the system.  They will need to verify their email and also choose a password.');
            }
        } 
        //dd($validator->messages());
        return Redirect::back()->withErrors($validator->messages())->withInput();   
    }
    
    public function createImportedUser($input) {
        $user = new User;
        $user->fname = ucwords($input['fname']);
        $user->lname = ucwords($input['lname']);
        $user->email = $input['email'];
        $user->password = Hash::make('ThisisApasswordTheyWillNeverTypeInProbably');
        $user->pharmacy = Company::find($input['company'])->name;
        $user->remember_token = '';
        $user->bio = '';
        $user->associated_data = '';
        $user->groups = User::getCurrentPermissions();
        $user->companies = '{"'.$input['company'].'":"'.$input['access'].'"}';
        $user->employee_type = $input['access'];
        $user->imported = 1;
        $user->whitelisted_ip_list = 1;
        if ($user->save()) {
            //return user id to controller
            return $user;
        }
    }
            
    
    public static function getUserValidator() {
        return Validator::make(Input::all(), [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:users',
            'company' => 'required',
            'access' => 'required|NotIn:0'
        ]);
    }
}
