<?php

class pdsMarketplaceController extends \BaseController {

    public function __construct() {
        parent::__construct();
        $css = [ 'dash', 'forum', 'main'];
        View::share('css', $css);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $marketplace = TmpVendor::all();
        
        return View::make('dashboard.Admin.marketplace.index')->with('marketplace', $marketplace);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('dashboard.Admin.marketplace.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $rules = [
            'vendor' => 'required',
            'category' => 'required',
            'real_email' => 'required',
            'site_url' => 'required',
            'description' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            return Redirect::to('pdsmarketplace/create')->withErrors($validator)->withInput(Input::all());
        }
        $marketplace = new TmpVendor();
        $marketplace->logo_src = Input::get('logo_src');
        $marketplace->vendor = Input::get('vendor');
        $marketplace->category = Input::get('category');
        $marketplace->subcategory = Input::get('subcategory');
        $marketplace->real_email = Input::get('real_email');
        $marketplace->site_url = Input::get('site_url');
        $marketplace->contact_href = Input::get('contact_href');
        $marketplace->description = Input::get('description');
        $marketplace->contact_info = Input::get('contact_info');
        $marketplace->years = Input::get('years');
        if($marketplace->save()){   
            Session::flash('message', 'Sucessfully saved marketplace listing');
            return Redirect::to('pdsmarketplace');
        }
        Session::flash('message', 'Could not save marketplace listing. Please check your input.');
        return Redirect::to('pdsmarketplace');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $vendor = TmpVendor::find($id);
        return View::make('dashboard.Admin.marketplace.show')->with('marketplace', $vendor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $vendor = TmpVendor::find($id);
        return View::make('dashboard.Admin.marketplace.edit')->with('marketplace', $vendor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $rules = [
            'vendor' => 'required',
            'category' => 'required',
            'real_email' => 'required',
            'site_url' => 'required',
            'description' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('pdsmarketplace/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
        }
        $marketplace = TmpVendor::find($id);
        $marketplace->logo_src = Input::get('logo_src');
        $marketplace->vendor = Input::get('vendor');
        $marketplace->category = Input::get('category');
        $marketplace->subcategory = Input::get('subcategory');
        $marketplace->real_email = Input::get('real_email');
        $marketplace->site_url = Input::get('site_url');
        $marketplace->contact_href = Input::get('contact_href');
        $marketplace->description = Input::get('description');
        $marketplace->contact_info = Input::get('contact_info');
        $marketplace->years = Input::get('years');
        if($marketplace->save()) {   
            Session::flash('message', 'Sucessfully updated marketplace listing');
            return Redirect::to('pdsmarketplace');
        }
        Session::flash('message', 'Could not update the marketplace listing. Please check your input.');
        return Redirect::to('pdsmarketplace');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        return 'Please go back. You can\'t delete that';
    }
    
    public function displayUploadedMarketplaceListings() {
        $vendors = Vendor::with('category')->orderBy('company_name', 'ASC')->get();
        
        return View::make('dashboard.Admin.marketplace.oldvendors')->with('vendors', $vendors);
    }
    
    public function importFromCompletedListings() {
        /*
        $vendors = TmpVendor::lookForMutations();
        echo '<table>';
        foreach($vendors as $vendor) {
            echo '<tr><td>'.$vendor->id.'</td><td>'.$vendor->vendor.'</td><td>'.$vendor->approved.'</td></tr>';
        }
        echo '</table>';
        */
        
        /*$vendors = Vendor::all();
        foreach($vendors as $listing) {
            
            $vendor = new TmpVendor();
            $vendor->vendor = $listing->company_name;
            $vendor->logo_alt = $listing->company_name;
            $vendor->real_email = $listing->email;
            $vendor->contact_href = $listing->email;
            $vendor->site_url = $listing->company_website;
            $vendor->contact_info = $listing->company_name.'<br>'.$listing->address.'<br>'
                    .$listing->city.', '.$listing->state_or_region.' '.$listing->postal_code;
            $vendor->logo_src = URL::to('storage/files/company_logos/'.$listing->company_logo);
            $vendor->description = $listing->description == null ? '' : $listing->description;
            $vendor->approved = 1;
            
            if($vendor->save()) {
                echo '<hr>';
                var_dump($vendor);
                echo '<img style="max-width: 250px;" src="' . $vendor->logo_src . '" />';
                //echo '<br><a href="' . $line[5] . '" target="_blank">Link</a>';
                var_dump($vendor->logo);
            } else {
                echo 'ddidnt save';
            }
        }*/
    }
    

    public function importVendorsFromCsv() {
        /*
        $file = fopen(public_path() . '/storage/files/import/lasttime.csv', 'r');
        while (($line = fgetcsv($file)) != FALSE) {
            
            $vendor = new TmpVendor();
            $vendor->vendor = $line[0];
            $vendor->logo_alt = $line[0];
            
            $vendor->contact_href = $line[1];
            $vendor->site_url = $line[1];
            $vendor->contact_info = $line[0].'<br>'.$line[2].'<br>'.$line[3].', '.$line[4].' '.$line[5];
            $vendor->logo_src = URL::to('storage/files/company_logos/'.$line[6]);
            $vendor->description = $line[11];
            $vendor->category = $line[7];
            $vendor->subCategory = $line[9];
            $vendor->approved = 1;
            if($vendor->save()) { 
                echo '<hr>';
                var_dump($vendor);
                echo '<img style="max-width: 250px;" src="' . $vendor->logo_src . '" />';
                echo '<br><a href="' . $line[5] . '" target="_blank">Link</a>';
                var_dump($vendor->logo);
            } else {
                echo 'ddidnt save';
            }
        }*/
    }

    public function get_logo_url($url, $name) {
        if (strpos($url, 'app.box.com') > -1) {
            return $this->saveMarketplaceLogo($url, $name);
        }
        return $url;
    }

    public function saveMarketplaceLogo($boxUrl, $name) {
        $filename = preg_replace('/[^a-zA-Z0-9_-]/', '', $name);
        switch ($boxUrl) {
            case 'https://app.box.com/s/p6jm3mpi5484i5s9yq0p':
            case 'https://app.box.com/s/is5bvgavl60r94tw1xtm':
            case 'https://app.box.com/s/1kbp6mie7y7ttjopdyje':
            case 'https://app.box.com/s/mkllwsp7ccqdlnj4zgrr':
            case 'https://app.box.com/s/61duz8repr7yba6fl80y':
            case 'https://app.box.com/s/8vf21z3r67aifxty2tply8ky1yuen1no':
            case 'https://app.box.com/s/ukvxka6q6pisfk6qfvyx':
            case 'https://app.box.com/s/mgxs5rnh92i69a6xxukd':
            case 'https://app.box.com/s/0hpxyoqoz2iyk5kgopkw':
            case 'https://app.box.com/s/kggjiy5ve218ex0x6pxvstkl4ef896g4':
            case 'https://app.box.com/s/d9t4mkzgddiyfg99b8nrxnvz0it8njx4':
            case 'https://app.box.com/s/1vy1eud612y5dluag8ov':
            case 'https://app.box.com/s/3bcu9rjwpx4mpx96vexu':
            case 'https://app.box.com/s/wnxa5xozdzsd0m5uemms':
            case 'https://app.box.com/s/89bgatvt4pq9rphcf271':
            case 'https://app.box.com/s/eps9rel4m83hjwywmzql':
            case 'https://app.box.com/s/3c859dxuqsxr08l2fnk6':
            case 'https://app.box.com/s/8fk0wtgtkwy1yyz5abv1':
            case 'https://app.box.com/s/x8yzjpmfyillx2g6y70n':
            case 'https://app.box.com/s/0m9hznfw25r2lxg9zuj8':
            case 'https://app.box.com/s/oa94by7nz2hbttxzirzm':
            case 'https://app.box.com/s/6gg0n5w0fbkngwzmflbr':
            case 'https://app.box.com/s/hhvuftj2ajn0p1kwfuhb':
            case 'https://app.box.com/s/30gx3xnj9umyt4y815ys':
            case 'https://app.box.com/s/d93wj285nzs6oktdjzlo331rj9fpj7yr':
            case 'https://app.box.com/files/0/f/1477478323/1/f_13930600455':
            case 'https://app.box.com/s/rmpvqg14l5rpsxlbrc9t':
            case 'https://app.box.com/s/ar05n7gg7l8hgkveepfl':
            case 'https://app.box.com/s/3wwjuyi7b1ukfo4l7b2zed9u8yilptyp':


                $ext = '.png';
                break;
            case 'https://app.box.com/s/oscn73uia4z03dp4y9ej9osvl011mjft':
                $ext = '.jpeg';
                break;
            case 'https://app.box.com/s/h2ht3j4ji2mjb0urt5si':
            case 'https://app.box.com/s/g22stjwgp7j5f3six2p6':
                $ext = '.gif';
                break;
            case 'https://app.box.com/s/z4h8ivli20kheimrlt2ga3obaz56qnb5':
                $ext = '.tif';
                break;
            default:
                $ext = '.jpg';
        }
        var_dump($filename);
        //$saved = file_put_contents(public_path().'/storage/files/import/'.$filename.$ext, $boxUrl);


        return URL::to('/storage/files/import/' . $filename . $ext);
    }

}
