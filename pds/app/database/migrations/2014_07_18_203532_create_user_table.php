<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//whitelist table
		Schema::create('whitelists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('registered_ip_list', 250);
		});

		//user table
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('hs_acc_number')->index();
			$table->string('fname', 128);
			$table->string('lname', 128);
			$table->string('pharmacy', 180);
			$table->string('email');
			$table->string('password');
			$table->dateTime('updated_at');
			$table->dateTime('created_at');
			$table->string('remember_token')->default('');
			$table->integer('confirmed_user')->default(0);
			$table->string('groups')->default('general');
			$table->integer('whitelisted_ip_list')->unsigned();
		});

		Schema::table('users', function(Blueprint $table)
		{
			$table->foreign('whitelisted_ip_list')->references('id')->on('whitelists')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("group");
		Schema::dropIfExists('ad_types');
		Schema::dropIfExists('guide_ads');
		Schema::dropIfExists('bill_types');
        Schema::dropIfExists('users');
		Schema::dropIfExists('whitelists');
		Schema::dropIfExists('resource_types');
	}
}
