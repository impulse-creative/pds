<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('author');
			$table->string('filename');
			$table->string('file_version')->default("1.0");
			$table->string('tmp_file_path');
			$table->integer('download_count')->default(0);
			$table->string('drop_box_link');
			$table->timestamps();
			$table->softDeletes();
                        $table->integer('resource_type_id')->unsigned();
		});

		Schema::table('resource_files', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('resource_type_id')->references('id')->on('resource_types')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('resource_files');
	}

}
