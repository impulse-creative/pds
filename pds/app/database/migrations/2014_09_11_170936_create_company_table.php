<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vendors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('logo_alt')->default('PDS Vendor');
			$table->string('logo_src')->default('null');
			$table->string('logo_href')->default('#');
			$table->string('vendor')->default('Company Name');
			$table->string('years');
			$table->text('description');
			$table->text('contact_info');
			$table->string('contact_href')->default('#');
			$table->string('site_url')->default("#");
            $table->string('hs_acc_number');
			$table->timestamps();
			$table->softDeletes();
		});
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('vendors');
	}

}
