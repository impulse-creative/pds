<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
             	Schema::create('categories', function (Blueprint $table)
                {
                        $table->increments('id');
                        $table->string('category_name');
                });

                Schema::create('pds_vendors', function(Blueprint $table)
                {
                        $table->engine = 'InnoDB';
                        $table->increments('id'); 
                        $table->integer('user_id')->unsigned();
                        $table->string('hs_acc_number');
                        $table->string('first_name');
                        $table->string('last_name');
                        $table->string('email');
                        $table->string('phone')->default('Unavailable');
                        $table->string('company_name');
                        $table->string('company_website')->default('No Website listed');
                        $table->string('address')->default('No Address listed');
                        $table->string('city')->nullable(); 
                        $table->string('state_or_region')->nullable(); 
                        $table->string('postal_code')->nullable();
                        $table->string('company_logo')->nullable();
                        $table->integer('category')->unsigned()->default(0);
                        $table->string('subCategory', 32)->references('id')->on('sub_categories');
                        $table->text('description')->nullable();
                        $table->string('company_blog_or_vlog')->nullable();
                        $table->boolean('term_agreement')->default(0);
                        $table->integer('featured')->default(0);
                        $table->integer('video_permission')->default(0);
                        $table->timestamps();
                        $table->softDeletes();
                });

                Schema::table('pds_vendors', function(Blueprint $table)
                {
                    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                    $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::dropIfExists('pds_vendors');
                Schema::dropIfExists('categories');
                Schema::dropIfExists('sub_categories');
                
        }

}
