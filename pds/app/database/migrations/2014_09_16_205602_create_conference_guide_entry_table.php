<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConferenceGuideEntryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guide_entry', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('sales_first_name');
			$table->string('sales_last_name');
			$table->string('sales_email');
			$table->string('company_name');
			$table->string('company_website')->default('No Site Given');
			$table->string('address')->default('No Address Given');;
			$table->string('city')->nullable();
			$table->string('state_or_region')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('company_guide_entry_img')->nullable();
			$table->text('description');
			$table->integer('term_agreement')->default(0);
            $table->string('hs_acc_number');
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::table('guide_entry', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                        $table->foreign('hs_acc_number')->references('hs_acc_number')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('guide_entry');
	}

}
