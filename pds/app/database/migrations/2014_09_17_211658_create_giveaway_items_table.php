<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiveawayItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('giveaway_items', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('item_name');
                        $table->string('hs_acc_number');
		});

		Schema::table('giveaway_items', function (Blueprint $table)
		{
                    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                    $table->foreign('hs_acc_number')->references('hs_acc_number')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('giveaway_items');
	}

}
