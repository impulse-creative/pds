<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVipTicketRecipientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vip_recipients', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('vip_fname');
			$table->string('vip_lname');
			$table->string('vip_company_name');
			$table->string('vip_email');
			$table->timestamps();
			$table->softDeletes();
                        $table->string('hs_acc_number');
		});

		Schema::table('vip_recipients', function (Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                        $table->foreign('hs_acc_number')->references('hs_acc_number')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('vip_recipients');
	}

}
