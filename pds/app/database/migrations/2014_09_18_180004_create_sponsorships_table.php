<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sponsorship_types', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('type')->nullable();
			$table->string('price')->nullable();
			$table->integer('sale_limit')->default(0);
		});

		Schema::create('sponsorships', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('sponsorship_types');
			$table->integer('billing_choice')->default(0);
            $table->string('hs_acc_number');
		});
                
        Schema::table('sponsorships', function (Blueprint $table)
		{
		        $table->foreign('hs_acc_number')->references('hs_acc_number')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sponsorship_types');
		Schema::dropIfExists('sponsorships');
	}

}
