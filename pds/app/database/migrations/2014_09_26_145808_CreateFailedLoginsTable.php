<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailedLoginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('failed_logins', function(Blueprint $table)
		{
    			$table->increments('id');
    			$table->integer('user_id')->unsigned();
    			$table->string('user_email');
    			$table->string('user_ip');
    			$table->dateTime('attempted');
		});

		Schema::table('failed_logins', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('failed_logins');
	}

}
