<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumTableTopics extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_topics', function(Blueprint $table) 
		{
			$table->increments('id');

			/* Attributes */
			$table->integer('parent_category')->unsigned();
			$table->integer('author_id')->unsigned();
			$table->string('title');
			$table->integer('post_views')->default(0);

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::table('forum_topics', function(Blueprint $table)
		{
			$table->foreign('parent_category')->references('id')->on('forum_categories')->onDelete('cascade');
			$table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_topics');
	}

}
