<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewResourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resources', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('filename')->nullable();
			$table->string('resource_name');
			$table->text('description')->nullable();
			$table->integer('resource_type')->unsigned();
			$table->string('file_size');
			$table->string('file_type');
			$table->integer('author')->unsigned();
			$table->string('resource_share_link');
			$table->integer('subscribe')->default(0);
			$table->dateTime('expire_date');
			$table->timestamps();
		});

		Schema::table('resources', function(Blueprint $table)
		{
			$table->foreign('resource_type')->references('id')->on('resource_types')->onDelete('cascade');
			$table->foreign('author')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('resource', function(Blueprint $table)
		{
			$table->dropForeign('resource_type');
			$table->dropForeign('author');
		});
		Schema::dropIfExists('resources');
	}

}
