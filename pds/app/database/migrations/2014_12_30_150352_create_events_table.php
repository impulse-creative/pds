<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('featured_img')->nullable();
			$table->text('description');
			$table->text('who_should_attend')->nullable();
			$table->text('how_does_it_work')->nullable();
			$table->text('pricing_description')->nullable();
			$table->integer('free_event')->default(0);
			$table->double('core_price')->default(0.00);
			$table->double('advanced_price')->default(0.00);
			$table->double('elite_price')->default(0.00);
			$table->dateTime('expiration_date')->nullable();
			$table->string('hs_form_id')->nullable();
			$table->integer('is_featured')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('events');
	}

}
