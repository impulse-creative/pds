<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToEvents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events', function(Blueprint $table)
		{
			$table->text('link_to_next_free');
			$table->text('link_to_next_core');
			$table->text('link_to_next_advanced');
			$table->text('link_to_next_elite');
			$table->text('per_core');
			$table->text('per_advanced');
			$table->text('per_elite');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events', function(Blueprint $table)
		{
			
		});
	}

}
