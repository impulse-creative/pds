<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyTableForTeamworkCompanyAccounts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teamwork_companies', function(Blueprint $table) {
			$table->increments('id');
			$table->string('pharmacy_name');
			$table->string('pharmacy_owner');
			$table->string('teamwork_company_id')->nullable();
			$table->string('teamwork_project_ids')->nullable();
			$table->string('teamwork_user_id')->nullable();
			$table->string('coach_or_IS_name')->nullable();
			$table->string('owner_api_key')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('teamwork_companies');
	}

}
