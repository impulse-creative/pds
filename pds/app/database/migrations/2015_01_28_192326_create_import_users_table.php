<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('importusers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('old_id');
			$table->string('old_company');
			$table->string('fname');
			$table->string('lname');
			$table->string('password')->default('resetThisNow');
			$table->string('email');
			$table->integer('imported')->default(1);

		});

		Schema::table('users', function(Blueprint $table)
		{
			$table->string('old_id');
			$table->string('old_company');
			$table->string('imported');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('importusers');
	}

}
