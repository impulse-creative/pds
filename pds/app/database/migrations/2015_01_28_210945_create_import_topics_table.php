<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportTopicsTable extends Migration {

	public function up()
	{
		Schema::create('importtopics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('old_id');
			$table->string('old_message_board_id');
			$table->string('old_user_id');
			$table->string('last_replied_user');
			$table->string('old_subject');
			$table->string('old_message');
			$table->string('old_category');
			$table->timestamps();
			$table->integer('imported')->default(1);

		});

		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('importtopics');
	}

}
