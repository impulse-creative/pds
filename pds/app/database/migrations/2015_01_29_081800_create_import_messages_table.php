<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('importmessages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('old_id');
			$table->string('old_topic_id');
			$table->string('old_date');
			$table->string('message');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('importmessages');
		
	}

}
