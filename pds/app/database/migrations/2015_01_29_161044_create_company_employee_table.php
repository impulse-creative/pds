<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyEmployeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pharmacy_employees', function(Blueprint $table) {
			$table->increments('id');
			$table->string('hs_company_id');
			$table->string('employee_name');
			$table->string('employee_email');
			$table->string('employee_phone')->nullable();
			$table->string('employee_title')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pharmacy_employees');
	}

}
