<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStringToTextForForums extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE importtopics MODIFY COLUMN old_subject TEXT');
		DB::statement('ALTER TABLE importtopics MODIFY COLUMN old_message TEXT');
		DB::statement('ALTER TABLE importmessages MODIFY COLUMN message TEXT');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
