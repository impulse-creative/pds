<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('phone');
			$table->string('domain_name');
			$table->string('email_from');
			$table->string('address');
			$table->string('city');
			$table->string('state');
			$table->string('postal_code');
			$table->string('owner_name');
			$table->string('owner_email');
			$table->string('level');
			$table->string('ca_account');
			$table->string('company_account');
			$table->string('time_zone');
			$table->string('is_vendor');
			$table->string('verify_url');
			$table->string('sf_account_id');
			$table->integer('admin_id');
			$table->string('murder_date');
			$table->text('employees');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
