<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Eloquent::unguard();
        // $this->call('WhitelistTableSeeder');
        // $this->call('UserTableSeeder');
        // $this->call('ResourceTypesTableSeeder');
        // $this->call('AdTypesTableSeeder');
        // $this->call('BillTypesTableSeeder');
        // $this->call('MarketCategoriesTableSeeder');
        // $this->call('MarketSubCategoriesTableSeeder');
        // $this->call('SponsorshipsTableSeeder');
        // $this->call('GroupTableSeeder');
        // $this->call('EventTypeSeeder');
        // $this->call('EventStateSeeder');
        $this->call('ProvinceSeeder');
	}
}

class ProvinceSeeder extends Seeder {
    public function run() 
    {
        $states = [
            "Alberta" => "AB",
            "British Columbia" => "BC",
            "Manitoba" => "MB",
            "New Brunswick" => "NB",
            "Newfoundland and Labrador" => "NL",
            "Nova Scotia" => "NS",
            "Northwest Territories" => "NT",
            "Nunavut" => "NU",
            "Ontario" => "ON",
            "Prince Edward Island" => "PE",
            "Quebec" => "QC",
            "Saskatchewan" => "SK",
            "Yukon" => "YT"
        ];
        foreach ($states as $full => $short) {
            EventStates::create([
                'state' => $full,
                'state_abreviation' => $short
            ]);
        }
    }
}

class EventStateSeeder extends Seeder {
    public function run()
    {
        $states = [
            "Alabama" => "AL",
            "Alaska" => "AK",
            "Arizona" => "AZ",
            "Arkansas" => "AR",
            "California" => "CA",
            "Colorado" => "CO",
            "Connecticut" => "CT",
            "Delaware" => "DE",
            "Florida" => "FL",
            "Georgia" => "GA",
            "Hawaii" => "HI",
            "Idaho" => "ID",
            "Illinois" => "IL",
            "Indiana" => "IN",
            "Iowa" => "IA",
            "Kansas" => "KS",
            "Kentucky" => "KY",
            "Louisiana" => "LA",
            "Maine" => "ME",
            "Maryland" => "MD",
            "Massachusetts" => "MA",
            "Michigan" => "MI",
            "Minnesota" => "MN",
            "Mississippi" => "MS",
            "Missouri" => "MO",
            "Montana" => "MT",
            "Nebraska" => "NE",
            "Nevada" => "NV",
            "New Hampshire" => "NH",
            "New Jersey" => "NJ",
            "New Mexico" => "NM",
            "New York" => "NY",
            "North Carolina" => "NC",
            "North Dakota" => "ND",
            "Ohio" => "OH",
            "Oklahoma" => "OK",
            "Oregon" => "OR",
            "Pennsylvania" => "PA",
            "Rhode Island" => "RI",
            "South Carolina" => "SC",
            "South Dakota" => "SD",
            "Tennessee" => "TN",
            "Texas" => "TX",
            "Utah" => "UT",
            "Vermont" => "VT",
            "Virginia" => "VA",
            "Washington" => "WA",
            "West Virginia" => "WV",
            "Wisconsin" => "WI",
            "Wyoming" => "WY"
        ];

        foreach ($states as $full => $short) {
            EventStates::create([
                'state' => $full,
                'state_abreviation' => $short
            ]);
        }
    }
}

class EventTypeSeeder extends Seeder {
    public function run()
    {
        $event_types = [
            'Online Webinar',
            'Teleconference',
            'Onsite',
            'Regional',
            'Recorded Webinar'
        ];

        foreach ($event_types as $key => $type) {
            EventTypes::create(['type' => $type]);
        }
    }
}

class WhitelistTableSeeder extends Seeder {
	public function run()
	{
		DB::table('whitelists')->insert(array(
			'user_id' => 0,
			'registered_ip_list' => 'empty'
		));
	}
}

class UserTableSeeder extends Seeder {
	public function run()
	{
		User::create(array(
			'fname' => 'Derek',
			'lname' => 'Foster',
                        'hs_acc_number' => '171726',
			'pharmacy' => 'Impulse Creative',
			'email' => 'derek@chooseimpulse.com',
			'password' => Hash::make('impulserocks'),
			'confirmed_user' => 1,
			'groups' => '5',
			'whitelisted_ip_list' => 1
		));
	}
}

class GroupTableSeeder extends Seeder {
	public function run()
	{
        $groups = [
            'admin',
            'customer',
            'vendor',
            'general',
            'impulse_admin',
            'forum_mod',
            'event_mod',
            'exhibitor_mod',
            'resource_mod',
            'industry_forums',
            'member_forums',
            'events',
            'resources',
            'employee',
            'member_resources',
            'access_control'
        ];

        foreach ($groups as $key => $value) {
            Group::create([
                'name' => $value
            ]);
        }
    }

}

class ResourceTypesTableSeeder extends Seeder {
	public function run()
	{
		FileTypes::create(array(
			'type_name' => 'human_resources'
		));

                FileTypes::create(array(
                        'type_name' => 'marketing'
                ));

                FileTypes::create(array(
                        'type_name' => 'tax'
                ));

                FileTypes::create(array(
                        'type_name' => 'spreadsheet'
                ));

                FileTypes::create(array(
                        'type_name' => 'webinar'
                ));

                FileTypes::create(array(
                        'type_name' => 'video'
                ));
	}	
}

class AdTypesTableSeeder extends Seeder {
	public function run()
	{
		$types = array('half-page', 'full-page', 'two-page', 'inside-front', 'inside-back');
		foreach($types as $key => $val) {
			AdTypes::create(array(
				'type' => $val
			));
		}

	}
}

class MarketCategoriesTableSeeder extends Seeder {
    public function run()
    {
        $types = array(
            'Business Services', 
            'Compounding', 
            'DME / LTC', 
            'Home Health Care', 
            'Medication / Suppliers', 
            'Pharmacy Automation', 
            'Rx Packaging / Preparation', 
            'Store Supplies', 
            'Technology'
        );
        foreach($types as $val) {
            MarketCategories::create(array(
                'category_name' => $val
            ));
        }
    }
}

class MarketSubCategoriesTableSeeder extends Seeder {
    public function run()
    {
        $types = array(
            '1.1'=>'Account Reconciliation/Management',
            '1.2'=>'Accreditation',
            '1.3'=>'Acquisitions',
            '1.4'=>'Advertising Agency',
            '1.5'=>'Certified Public Accounting',
            '1.6'=>'Construction / Remodeling',
            '1.7'=>'Distributor',
            '1.8'=>'Financial / Billing & Consulting',
            '1.9'=>'Inventory Control & Management',
            '1.10'=>'Pharmacy Insurance',
            '1.11'=>'Pharmacy Management',
            '1.12'=>'Reconciliation Services',
            '1.13'=>'Retirement Planning',
            '1.14'=>'Third Party Administrator',
            
            '2.1'=>'Cosmeceuticals',
            '2.2'=>'Flavoring Systems',
            '2.3'=>'Compounding Supplies / Hardware',
            
            '3.1'=>'Diabetic Footwear',
            '3.2'=>'Orthopedic Braces',
            
            '4.1'=>'Blood Glucose Monitoring',
            '4.2'=>'Blood Pressure Program',
            
            '5.1'=>'Discount Pharmaceutical Supplier',
            '5.2'=>'Purchasing Organization',
            '5.3'=>'Wholesalers',
            
            '6.1'=>'Pharmacy Automation',
            '6.2'=>'Pharmacy Computers',
            '6.3'=>'Pill Counter',
            
            '7.1'=>'Adherence / Compliance Programs',
            '7.2'=>'Medication Management System',
            '7.3'=>'Packaging / Repackaging Supplies',
            '7.4'=>'Prescription Labels',
            
            '8.1'=>'Eyewear',
            '8.2'=>'Greeting Cards',
            '8.3'=>'Health/Wellness Products',
            '8.4'=>'Kodak Pictures',
            '8.5'=>'Refrigerator Tags',
            '8.6'=>'Vials / Bottles',
            '8.7'=>'Vision',
            
            '9.1'=>'Business Management Technology',
            '9.2'=>'Interactive Voice Response',
            '9.3'=>'Patient messaging',
            '9.4'=>'Pharmacy Management Software',
            '9.5'=>'Phone Systems',
            '9.6'=>'Point of Sale Systems',
            '9.7'=>'Robot',
            '9.8'=>'Telecommunications',
            
            
        );
        foreach($types as $key => $val) {
            MarketSubCategories::create(array(
                'id' => $key,
                'name' => $val
            ));
        }
    }
}

class BillTypesTableSeeder extends Seeder {
	public function run()
	{
		$types = array('on-file', 'send-invoice', 'paid-in-package');
		foreach($types as $key => $val) {
			BillTypes::create(array(
				'type' => $val
			));
		}
	}
}

class SponsorshipsTableSeeder extends Seeder {
	public function run()
	{
		$types = [
            0  => 'Reusable Water Bottle:1:$4,000',
            1  => 'Conference Tote Bags:1:$3,500', 
            2  => 'Introduction of Keynote Speaker:1:$3,000',
            3  => 'Entrepreneur of the Year:1:$3,000',
            4  => 'Pharmacist of the Year:1:$3,000',
            5  => 'Hotel Room Custom Keys:1:$3,000',
            6  => 'Registration Area Sponsors:1:$3,000',
            7  => 'Lunch Sponsor:3:$2,500',
            8  => 'Floor Stickers:6:$2,000',
            9  => 'Refreshment Break Sponsor:6:$2,000',
            10 => 'Tote Bag Stuffer:2:$1,500',
            11 => 'Chair Drop:1:$1,500',
            12 => 'Name Badge Lanyard:1:$1,000',
            13 => 'Conference Pens:1:$800'
        ];
		foreach($types as $key => $val) {
            $values = explode(':', $val);
			SponsorshipTypes::create([
                'type' => $values[0],
                'price' => $values[2],
                'sale_limit' => $values[1]
            ]);
		}
	}
}







