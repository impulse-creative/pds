<?php

namespace BoxView\BoxViewHandler;
use \BoxView\lib\BoxViewApi;
use \BoxView\lib\BoxViewDocument;
use \Auth;
use \Validator;
use \Resources;
use Box_View_API;
use Box_View_Document;

class BoxViewCreator {
    
    public $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
        $this->box_api_key = '5qri8f4u0731tlcvgdfqlvmh9abgxtcq';
    }
    
    public function storeInBoxView($input, $real_file = null, $extra_input = null) {
        $file = isset($extra_input) ? $extra_input['file_id'] : $input['file_id'];;
        $filename = isset($extra_input) ? $extra_input['filename'] : $input['filename'];
        $real_filename = isset($input['real_filename']) ? $input['real_filename'] : $real_file;
        $box = new Box_View_API($this->box_api_key);

        // Create new document we want to upload.
        $doc = new Box_View_Document(array(
          'name' => '[User-'.Auth::id().'] '.$filename,
          'file_path' => public_path().'/storage/files/Library/fullsized/'.$real_filename,
        ));

        // Upload the new document.
        $response = $box->upload($doc);
        $store_boxview_id = $this->storeBoxviewIdOnResource($file, $response->id); 
        if($store_boxview_id == 'success') {
            return $response->id;
        }

        return 'failed';
    }
    
    public function storeBoxviewIdOnResource($id, $boxview_id){
        $resource = Resources::find($id);
        $resource->boxview_id = $boxview_id;
        if( $resource->save()) {
            return 'success';
        }

        return 'failure';
    }
    
}
