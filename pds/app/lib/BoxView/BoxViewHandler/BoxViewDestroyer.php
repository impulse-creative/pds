<?php

namespace BoxView\BoxViewHandler;
use \BoxView\lib\BoxViewApi;
use \BoxView\lib\BoxViewDocument;
use \Resources;
use \Redirect;
use Box_View_API;
use Box_View_Document;

class BoxViewDestroyer {
    
    public $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
        $this->box_api_key = '5qri8f4u0731tlcvgdfqlvmh9abgxtcq';
    }
    
    public function destroyFile() {
        $boxview_resources = Resources::boxviewRecords()->get();
        $doc = new Box_View_Document();
        $box = new Box_View_API($this->box_api_key);
        foreach($boxview_resources as $resource){
            $doc->id = $resource->boxview_id;
            $resource->boxview_id = NULL;
            $resource->save();
        }
        return Redirect::route('library.index')->with('Success', 'Successfully flushed boxview\'s storage.');
    }
    
}
