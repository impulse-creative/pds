<?php

namespace BoxView\BoxViewHandler;
use BoxView\BoxViewHandler\BoxViewCreator;
use \BoxView\lib\BoxViewApi;
use \BoxView\lib\BoxViewDocument;
use \Auth;
use \Resources;
use Box_View_API;
use Box_View_Document;
use URL;

class BoxViewDisplay {
    
    public $listener;
    private $box_api_key = '5qri8f4u0731tlcvgdfqlvmh9abgxtcq';
    
    public function __creator($listener) {
        $this->listener = $listener;
    }
    
    public function displayStoredFile($input, $file_id) {
        $id = $file_id;
        $input['file_id'] = $id;
        $resource = Resources::find($id); //replace with $id, comes through query string?
        $boxview_creator = new BoxViewCreator($this->box_api_key);
        if(is_null($resource->boxview_id)){
            $input['filename'] = '[User-'.Auth::id().'] '.$resource->filename;
            foreach(Resources::$unsupported_boxview_file_types['image_types'] as $type){
                if($type === $resource->file_type) {
                    return self::getImgViewPartial($resource);
                }
            }
            foreach(Resources::$unsupported_boxview_file_types['video_types'] as $type) {
                if($type === $resource->file_type) {
                    return self::getVideoSupportResponseViewPartial();
                }
            }
            $new_boxview_id = $boxview_creator->storeInBoxView($id, $resource->resource_name, $input);
            $resource->boxview_id = intval($new_boxview_id) ? $new_boxview_id : NULL;
            $resource->save();
        }
        
        return $this->getDisplayRequestResponse($resource);
    }
    
    private static function getImgViewPartial($resource) {
        $url = URL::to('/storage/files/Library/fullsized').'/'.$resource->resource_name;
        $html  = '<div class="center col-lg-12 col-md-12 col-sm-12 col-xs-12">';
        $html .= '<img src="'. $url .'" style="max-width: 50%; text-align:center; height: auto;"/>';
        $html .= '</div>';
        return $html;
    }
    
    private static function getVideoSupportResponseViewPartial() {
        $html  = '<div class="large-margin">';
        $html .= '<h4>We currently do not support previews of uploaded videos, zip files, or audio files. Sorry for the inconvenience.</h2>';
        $html .= '</div>';
        return $html;
    }
    
    public function getDisplayRequestResponse($resource) {
        $box = new Box_View_API($this->box_api_key);

        $doc = new Box_View_Document();
        $doc->id = $resource->boxview_id;
        $response = $box->view($doc);
        $timeout_max = 1000;
        $timout_counter = 0;
        do {
            $timout_counter += 1;
            $doc->id = $resource->boxview_id;
            $response = $box->view($doc);
            if($timout_counter >= $timeout_max) {
                \Log::error('Boxview Request timed out.', ['boxview id' => $resource->boxview_id]);
                return '<div>We\'re sorry. The request to view the resource timed out. Please <a id="'.$resource->id.'" href="'.URL::current().'" class="orange-text">click here</a> to try again.</div>';
            }
        }while($response === 'try again');
        $html = '<iframe src="' . $doc->session->url . '" style="width: 100%; min-height: 800px;"></iframe>';
        return $html;
    }
        
    // List all documents for this boxview app.
//        $docs = $box->load();
//        var_dump($docs);
}
