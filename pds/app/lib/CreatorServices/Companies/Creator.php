<?php
namespace CreatorServices\Companies;

use Validator;
use Auth;
use Company;
use User;
use Input;
use \CompanyApprovals;
use Exception;

class CreateCompany {
    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
    }
    
    public function validateCompany($input) {
        $validator = Validator::make($input, Company::$rules);
        
        if($validator->fails()) {
            return $this->listener->createCompanyFailedValidation($validator->messages());
        }
    }
            
    public function createCompany($data){
        $input = $this->getCompanyInput($data);
        if($this->validateCompany($input) != true) $this->validateCompany($input);
        //get owner information by email
        $admin = $this->getUserByEmail($input['owner_email']);
        if(count($admin) == 0){
            return $this->listener->adminDoesNotExist($input['owner_email']);
        }
        //get owner id and name
        $input['admin_id'] = $admin->id;
        $input['owner_name'] = $admin->fname.' '.$admin->lname;
        $employee_record[$admin->id] = $admin->employee_type;
        $input['employees'] = json_encode($employee_record);
        //update current user's record with company json string { company_id : employee type }
        $user = User::find(Auth::id());
        $companyList = $user->companies == 0 ? [] : json_decode($user->companies, true);
        //create company, store the company id and employee type on the users table, then return response
        return $this->createCompanySaveUserCompanyListThenReturnResponse($companyList, $user, $input);
    }
    
    public function createCompanySaveUserCompanyListThenReturnResponse($companyList, $user, $input) {
        try {
            $employeeType = \Input::get('employee_type');
            $company = Company::create($input);
            $companyList[$company->id] = $employeeType;
            $user->companies = json_encode($companyList);
            $user->employee_type = $employeeType;
            $user->save();
            return $this->listener->createCompanySuccess($input['owner_email']);   
        } catch (Exception $ex) {
            throw new Exception('Could not create the company or save the company data to the user. Response: '.$ex, $ex->getCode());
        }
    }
    
    protected function getCompanyInput($input){
        $user_info = [
            'name' => $input['pharmacy_name'],
            'phone' => $input['pharmacy_phone'],
            'domain_name' => $input['pharmacy_site'],
            'email_from' => Auth::user()->email,
            'address' => $input['pharmacy_address'],
            'city' => $input['pharmacy_city'],
            'state' => $input['pharmacy_state'],
            'postal_code' => $input['pharmacy_zip'],
            'owner_name' => $input['owner_name'],
            'owner_email' => $input['owner_email'],
            'employees' => '{ "'.Auth::user()->id.'" : "'.\Input::get('employee_type').'" }'
        ];
        return $user_info;
    }
    
    public function getUserByEmail($email){
        return User::where('email', $email)->first();
    }
    
    public function addCompanyToApprovalList($companyId){
        $employee_type = Input::get('type');
        $input = [
            'company_id' => $companyId,
            'requesting_user' => Auth::id()
        ];
        $validator = Validator::make($input, CompanyApprovals::$rules);
        
        if($validator->fails()){
            return $this->listener->failedToAddToApprovalList();
        }
        
        //add json string of company relation to requesting user
        $add_company_association = $this->addCompanyToUserTable($companyId, $employee_type);
        if($add_company_association) {
            $new_approval = CompanyApprovals::create($input);
            return $this->listener->successfullyAddedToApprovalList();
        }
        
        return $this->listener->failedToAssociateCompanyWithRequestingUser();
    }
    
    public function addCompanyToUserTable($companyId, $employee_type){
        $user = User::find(Auth::id());
        $companyList = $user->companies == 0 ? json_decode('{}', true) : json_decode($user->companies, true);
        $companyList[$companyId] = 'pending';
        $user->companies = json_encode($companyList);
        return $user->save();
    }
}