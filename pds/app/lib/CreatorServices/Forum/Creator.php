<?php 
namespace CreatorServices\Forum;

use Validator;
use ForumTopics;
use ForumCategories;
use ForumMessages;
use UserAchievement;
use Input;
use URL;
use Auth;

class Creator {
    
    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
    }
    
    public function createPost($data) {
        //create validator and return response
        $rules = ForumTopics::$rules;
        $data['author_id'] = Auth::id();
        $validator = Validator::make($data, ForumTopics::$rules);

        //if validator response indicates fail, return with errors and old input
        if ($validator->fails()) {
            return $this->listener->postCreationFails($validator->messages());
        }
        if($data['parent_category'] == 17 || $data['parent_category'] == 18) {
            $data['company'] = intval(Input::get('company'));
            $data['is_pharmacy_private_post'] = 1;
        } else {
            $data['is_pharmacy_private_post'] = 0;
            $data['company'] = 0;
        }
        
        //attempt to create new topic
        $createTopic = ForumTopics::create($data);
        
        if ($createTopic) {
            $grantAchievement = UserAchievement::addAchievement(\Auth::id(), 1);
            return $createTopic;
        } 
        
        throw new TopicException('Error: Could not store the topic data.');
    }
    
    public function createCategory($data) {
        $member_only_choice = \Input::get('admin_only');
        $rules = ForumCategories::$rules;
        $private = Input::get('mypharmacy') != null ? Input::get('mypharmacy') : 0;
        $input = [
            'title' => $data['title'],
            'subtitle' => $data['subtitle'],
            'private' => $private,
            'icon' => $data['category_icon'],
            'admin_only' => isset($member_only_choice) ? $member_only_choice : 0, //is actually used to determine if only members can view it (Core, Advanced, Elite)
            'min_access_level' => $data['min_access_level']
        ];
        
        $this->addExtraCategoryRuleAndInputIfNecessary($member_only_choice, $rules, $input);

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->listener->categoryCreationFails($validator->messages());
        }
        
        $createCategory = ForumCategories::create($input);

        if ($createCategory) {
            return $this->listener->categoryCreationSucceeds();
        } else {
            return $this->listener->categoryCreationFails($validator->messages());
        }
    }
    
    public static function addExtraCategoryRuleAndInputIfNecessary($member_only, $rules, $input){
        //check to see if they clicked 'members only', if so, then require the minimum access level select 
        if ($member_only == 1) { 
            $rules['min_access_level'] = 'required'; 
            $input['min_access_level'] = Input::get('min_access_level'); 
        }
    }
    
    public function createReply($data, $page_id) {
//        $data['data'] = Input::get('html_edits');
        $validator = Validator::make($data, ForumMessages::$rules);
        if($validator->fails()) {
            return $this->listener->replyFailed($validator->messages());
        }
        
        $create_message = \ForumMessages::create($data);
        $create_message->touch();
        if ($create_message) {
            $new_message_id = $create_message->id;
            return $this->updateTopicAndReturnListenerResponse($new_message_id, $data, $page_id);
        }
        
        return $this->listener->replyFailed($validator->messages());
    }
    
    public function updateTopicAndReturnListenerResponse($message_id, $data, $page_id) {
        $topic = \ForumTopics::find($data['parent_topic']);
        $topic->last_replied = $data['author_id'];
        $topic->updated_at = \Carbon\Carbon::now();
//        $topic->timestamps = false;
        $attachment = NULL;
        if(Input::hasFile('attachment')) $attachment = Input::file('attachment');
        if($topic->save()) {
            $topic->touch();
            $notification = 'thank you.';
            if (intval($message_id)) $grantAchievement = \UserAchievement::addAchievement(Auth::id(), 2);
            $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $topic->title);
            $page = in_array($page_id, [0, 1], false) ? '' : '?page='.$page_id;
            $return_url = isset($data['company']) ? URL::to('message-boards').'/private/company/'.preg_replace('/ /', '-', $scrubbedTitle).'/'.$topic->id.$page.'?private=true&category='.$topic->parent_category.'&companyId='.$topic->company.'#'.$message_id : URL::to('message-boards').'/'.preg_replace('/ /', '-', $scrubbedTitle).'/'.$topic->id.$page.'#'.$message_id;
            $updateNotification = $this->createTopicNotification($return_url, $topic->id, $topic->updated_at, $message_id);
            if($updateNotification != true) $notification .= $updateNotification;
            return $this->listener->replySuccess($notification, $message_id, $page_id, $return_url, $attachment);
        }
        
        return $this->listener->topicUpdateFailed();
    }
    
    public function createTopicNotification($url, $topicId, $date, $messageId) {
        $notifications = \Notifications::where('user_id', \Auth::id())->first();
        if($notifications == null) {
            $notifications = new \Notifications();
            $notifications->user_id = \Auth::id();
            $notifications->topics = new \stdClass();
            $topics = new \stdClass();
        } else {
            if(isset($notifications->topics) && is_object($notifications->topics)) {
                $topics = json_decode($notifications->topics);
            } else {
                $topics = new \stdClass();
            }
            
        }
        if(!isset($topics->{$topicId})) $topics->{$topicId} = new \stdClass ();
        $topics->{$topicId}->date = $date; 
        $topics->{$topicId}->show = 1;
        $topics->{$topicId}->messageId = $messageId;
        $topics->{$topicId}->url = $url; 
        
        $notifications->topics = json_encode($topics);
        if($notifications->save()) {
            return true;
        }
        return '.. There was a problem creating the notification. Please Contact Support';
    }
}