<?php

namespace EventsRepo;

use Illuminate\Support\ServiceProvider;

class EventsServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'EventsRepo\Storage\Events\EventsRepository',
			'EventsRepo\Storage\Events\EloquentEventsRepository'
		);
	}
}