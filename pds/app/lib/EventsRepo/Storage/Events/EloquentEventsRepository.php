<?php

/**
 * Title: Service provider for pds events
 * Date : 12/30/2014
 * Description: This Service Provider allows pds to access all of their events and
 * 				any data pertaining to events
 * 
 * @author: Derek J. Foster
 */

namespace EventsRepo\Storage\Events;

use \Carbon\Carbon;
use Events;

class EloquentEventsRepository extends \BaseController implements EventsRepository {


    /**
     * Retrieve all events in an eloquent object array
     */
    public function all() {
        return \Events::all();
    }
    
    /**
     * Retrieve all trashed events
     */
    public function allTrashed() {
        return \Events::onlyTrashed()->get();
    }

    /**
     * Retrieve all events with their related details attached
     */
    public function allWithDetails() {
        return \Events::with('type', 'location')->orderBy('start_event', 'ASC')->get();
    }

    /**
     * Retrieve all events with their related details attached, and then initialize pagination
     */
    public function allWithDetailsPaginated() {
        return \Events::with('type', 'location')->orderBy('start_event', 'ASC')->simplePaginate(10);
    }
    
    /**
     * Retrieve all upcoming events
     */
    public function allUpcomingEvents()
    {
        $current_time = \Carbon\Carbon::now();
        $dt_add_month = \Carbon\Carbon::now()->addMonth();
        $events = \Events::whereBetween('start_event', [$current_time, $dt_add_month])->orderBy('start_event', 'ASC')->get();
        return $events;
    }

    /** Retrieve all events with their related details and filter by the input param
     *
     * @param int $event_type
     */
    public function getAllWithDetailsFiltered($event_type) {
        if ($event_type == '0') {
            return \Redirect::route('events.index');
        }
        $events = \Events::with('type', 'location')->where('event_type', $event_type)->orderBy('start_event', 'ASC')->simplePaginate(10);
        return $events;
    }

    /**
     * Retrieve particular event
     *
     * @param int $id //event id
     */
    public function find($id) {
        return \Events::find($id);
    }

    /**
     * Create a new event and insert it into the database 
     */
    public function createNewEvent($eventImage) {
        $input = self::getEventInput($eventImage);
        $duplicate_choice = \Input::get('duplicate');
        $duplicate_event = isset($duplicate_choice) ? $duplicate_choice : 'single';
        $event = \Events::create($input['input']);
        if ($event) {
            if($duplicate_event !== 'single') {
                return \Redirect::route('get.add.event')->with('Success', 'Successfully created the new event! Here is a form with all your previous data so you can easily duplicate your event!')->withInput();
            }
            return \Redirect::route('events.index')->with('Success', 'Successfully created the new event!');
        }
        return \Redirect::route('events.index')->with('Failure', 'Failed to created the new event! Please try again. If the problem persists, please contact the web administrator.');
    }

    public function makeEventLink($url) {
        if (strpos($url, 'http') > -1) {
            return $url;
        }
        return 'http://' . $url;
    }

    public function getEventInput($image) {
        $startTime = Carbon::createFromFormat('m/d/Y h:i a', \Input::get('start_date'));

        $isFeatured = \Input::get('is_featured');
        $isMemberOnly = \Input::get('is_member_only');
        $city = trim(\Input::get('event_city'));
        $state = trim(\Input::get('event_state'));

        return [
            "input" => [
                'title' => trim(\Input::get('title')),
                'featured_img' => $image,
                'is_featured' => isset($isFeatured) ? $isFeatured : 0,
                'free_event' => \Input::get('is_free') == 1 ? 1 : 0,
                'non_member_price' => \Input::get('non_member_price') != '' ? trim(\Input::get('non_member_price')) : '0.00',
                'core_price' => \Input::get('core_price') != '' ? trim(\Input::get('core_price')) : '0.00',
                'elite_price' => \Input::get('elite_price') != '' ? trim(\Input::get('elite_price')) : '0.00',
                'advanced_price' => \Input::get('advanced_price') != '' ? trim(\Input::get('advanced_price')) : '0.00',
                'link_to_next_core' => $this->makeEventLink(trim(\Input::get('link_to_next_core'))),
                'link_to_next_advanced' => $this->makeEventLink(trim(\Input::get('link_to_next_advanced'))),
                'link_to_next_elite' => $this->makeEventLink(trim(\Input::get('link_to_next_elite'))),
                'link_to_next_free' => $this->makeEventLink(trim(\Input::get('link_to_next_free'))),
                'link_to_next_non_member' => $this->makeEventLink(trim(\Input::get('link_to_next_non_member'))),
                'per_non_member' => trim(\Input::get('per_non_member')),
                'per_core' => trim(\Input::get('per_core')),
                'per_advanced' => trim(\Input::get('per_advanced')),
                'per_elite' => trim(\Input::get('per_elite')),
                'hs_form_id' => trim(\Input::get('hs_form_id')) != '' ? trim(\Input::get('hs_form_id')) : '403afad0-9dc9-431d-bcc9-118afc64332a',
                'description' => trim(\Input::get('event_description')),
                'who_should_attend' => trim(\Input::get('attending')),
                'how_does_it_work' => trim(\Input::get('how_it_works')),
                'pricing_description' => \Input::get('price_description') != '' ? trim(\Input::get('price_description')) : null,
                'event_type' => \Input::get('event_types'),
                'city' => isset($city) ? $city : null,
                'state' => isset($state) && $state != 0 ? $state : null,
                'start_event' => trim($startTime),
                'is_member_only' => isset($isMemberOnly) ? $isMemberOnly : 0,
                'created_by' => \Auth::id(),
                'updated_by' => \Auth::id()
            ]
        ];
    }

    /**
     * Retrieve all records relevant to search key 
     *
     * @param string $search_key
     */
    public function getSearchResults() {
        $query = \Events::search(trim(\Input::get('search_key')))->orderBy('created_at', 'ASC')->paginate(10);
        return $query;
    }

    /**
     * Retrieve all event types
     */
    public function getEventTypes() {
        return \EventTypes::all();
    }

    public function getUnitsForPricingArray() {
        $units = [ 0 => '--Select One--', 1 => 'Person', 2 => 'Session', 3 => 'Event', 4 => 'Company'];
        return $units;
    }

    /**
     * Retrieve all event types and return in array form
     */
    public function getEventTypesArray() {
        $types = \EventTypes::all();
        $type_list = [ 0 => '--Select One--'];

        foreach ($types as $type) {
            $type_list[$type->id] = $type->type;
        }

        return $type_list;
    }

    /**
     * Retrieve all event states and return in array form
     */
    public function getEventStatesArray() {
        $states = \EventStates::all();
        $state_list = [];

        foreach ($states as $state) {
            $state_list[$state->id] = $state->state_abreviation.' - '.$state->state;
        }

        return $state_list;
    }

    /**
     * Update an event from the database
     *
     * @param int $id //event id
     */
    public function updateEvent($image) {
        $input = self::getEventInput($image);
        $id = \Input::get('event_id');

        $event = self::find($id);
        if (is_null($image) || $image == '')
            unset($input['input']['featured_img']);
        $input['input']['created_by'] = $event->created_by;
        $input['input']['updated_by'] = \Auth::id();
        $updated = $event->update($input['input']);
        if ($updated) {
            return \Redirect::route('events.index')->with('Success', 'Successfully updated the event!');
        }

        return \Redirect::route('events.index')->with('Failure', 'Failed to update the event! Please try again. If the problem persists, please contact the web administrator.');
    }

    /**
     * Remove an event from the database
     *
     * @param int $id //event id
     */
    public function removeEvent($id = null) {
        $id = is_null($id) ? \Input::get('event_id') : $id;

        $event = self::find($id);

        if ($event->delete()) {
            return is_null($id) ? \Redirect::route('events.index')->with('Success', 'Successfully removed the event!') : \Redirect::route('events.index');
        }

        return \Redirect::route('events.index')->with('Failure', 'Failed to removed the event!');
    }

    /**
     * Clean string of spaces and special chars
     *
     * @param string $dirty_string
     */
    public function cleanForId($dirty_string) {
        $lather = str_replace(' ', '_', $dirty_string); // Replaces all spaces with underscores.

        return $rinse = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $lather)); // Removes special chars and makes lower case
    }

    /**
     * Retrieve event form embed code from HubSpot form builder
     *
     * @param string $form_id
     */
    public function getHubspotEventForm($form_id) {
        $forms = \HubSpot::forms();
        $form = $forms->get_form_by_id($form_id);
        //return var_dump($form_id);
        return end($form);
    }

    /**
     * Retrieve similar events based off of current event title
     *
     * @param string $event_title
     */
    public function getSimilarEventsBasedOffTitle($event_title, $viewing_event) {
        $modified_title = self::cleanEventTitle($event_title);
        return $similar_events = \Events::where('title', 'LIKE', '%' . $modified_title . '%')->where('id', '!=', $viewing_event)->get();
    }

    /**
     * Clean title for similar events query
     * @param string $title 
     */
    public function cleanEventTitle($title) {
        $exploded_title = explode(' ', $title);
        foreach ($exploded_title as $key => $title_chunk) {
            if (intval($title_chunk))
                $exploded_title[$key] = '';
        }
        return $modified_title = implode(' ', $exploded_title);
    }
    
    
    public function storeEventPhoto($filename, $inputName) {
        if ($_FILES[$inputName]["size"][0] !== 0) {
            $lazyPath = '/storage/files/events/';
            $data = \Input::file();
            $tempFileName = $data[$inputName]->getRealPath();
            $newFileName = 'event-' . \Auth::id() . '-' . rand(0, 10000) . "_" . $data[$inputName]->getClientOriginalName();
            $newPath = public_path() . $lazyPath;
            $saved = $this->storePhotoLocally($tempFileName, $newPath . $newFileName);
            if ($saved) {
                return $newFileName;
            }
            return \Redirect::back()->with('No Files', 'Something Happened Man, And I have no clue why');
        } else {
            return \Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    public function storePhotoLocally($tempFile, $newFile) {
        if (!\File::move($tempFile, $newFile)) {
            die('Upload Failed Miserably');
        }
        return true;
    }

}
