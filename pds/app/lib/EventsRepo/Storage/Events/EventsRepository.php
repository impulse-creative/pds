<?php

namespace EventsRepo\Storage\Events;

interface EventsRepository {
	public function all();

	public function allWithDetails();

	public function getAllWithDetailsFiltered($event_type);

	public function createNewEvent($image);

	public function getEventTypes();

	public function getEventTypesArray();

	public function getEventStatesArray();

	public function updateEvent($image);

	public function removeEvent();
}