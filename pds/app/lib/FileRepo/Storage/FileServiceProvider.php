<?php

namespace FileRepo\Storage;

use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'FileRepo\Storage\Resources\FileRepository',
			'FileRepo\Storage\Resources\EloquentFileRepository'
		);
	}
}