<?php

/**
 * Title: Service provider for pds resources
 * Date : 11/12/14
 * Description: This Service Provider allows pds to access all of their files and resources
 * 				and perform simple CRUD operations on each instance.
 * 
 * @author: Derek J. Foster
 */

namespace FileRepo\Storage\Resources;
use \Illuminate\Database\Eloquent\SoftDeletingTrait;
use Resources;

class EloquentFileRepository extends \BaseController implements FileRepository {
    use SoftDeletingTrait;
    
    protected $dates = ['deleted_at'];
    /**
     * Retrieve all available resources
     * 
     * @return object array
     */
    public function all() {
        return \Resources::library()->get();
    }

    /**
     * Retrieve all available resources including trashed
     *
     * @return object array
     */
    public function allWithTrashed() {
        $resources = \Resources::with('user')->onlyTrashed()->get();
        $resource_list = [];
        foreach($resources as $key => $resource)
        {
            if(is_null($resource->deleted_at)) continue;
            $resource_list[] = $resource;
        }
        return $resource_list;
    }
    
    public function latestResources()
    {
        return $resources = \Resources::with('type', 'user', 'container')->library()->orderBy('updated_at', 'DESC')->limit(7)->get();
    }

    /**
     * Retreive all resources for application library --pdsadvantage
     *
     * @return object array
     */
    public function allWithForeignDetails($type = null) {
        //if the user is using the search filter input
        $search_keys = \Input::get('search_key') != null ? explode(' ', \Input::get('search_key')) : false;    
        $final_permissions = explode(':', \Auth::user()->groups);
        if ($search_keys != false) {
            return self::resourcesMatchingSearchResults($search_keys, $final_permissions);
        }

        //if there are no filters for the results
        if (is_null($type)) {
            return self::allResourcesNoFilters($final_permissions);
        }
        //if user chooses a category/container
        return self::resourcesSortedByResourceContainer($type, $final_permissions);
    }
    
    private function resourcesMatchingSearchResults($search_keys, $permissions)
    {
        $search_results = array();
        foreach($search_keys as $key) {
            $searched_files = \Resources::with('type', 'user', 'container')->library()->where('filename', 'LIKE', '%' . $key . '%')->orWhere('description', 'LIKE', '%' . $key . '%')->whereIn('access_level', $permissions)->where('deleted_at', null)->orderBy('created_at', 'DESC')->get();
            foreach($searched_files as $file) {
                $filter = $this->getSearchFilter($search_results);
                if($filter !== 'Empty' && array_key_exists($file->id, $filter)) continue;
                $search_results[] = $file;
            }
        }
        return $search_results;
//        return \Resources::with('type', 'user', 'container')->where('filename', 'LIKE', '%' . $search_key . '%')->orWhere('description', 'LIKE', '%' . $search_key . '%')->whereIN('access_level', $permissions)->orderBy('created_at', 'DESC')->paginate(25);
    }
    
    protected function getSearchFilter($search_results) {
        if(count($search_results) > 0) {
            $search_filter = array();
            foreach($search_results as $file) {
               $search_filter[$file->id] = $file;
            }
            return $search_filter;
        }
        
        return 'Empty';
    }
    
    private function allResourcesNoFilters($permissions)
    {
        return \Resources::with('type', 'user', 'container')->library()->whereIN('access_level', $permissions)->orderBy('created_at', 'DESC')->paginate(25);
    }
    
    public function resourcesSortedByResourceContainer($type, $permissions)
    {
        return \Resources::with('type', 'user', 'container')->library()->where('resource_container', '=', $type)->whereIN('access_level', $permissions)->orderBy('created_at', 'DESC')->paginate(25);
    }

    private function getKeywordsFromPhrase($phrase)
    {
        $keywords = strpos(trim($phrase), ' ') == false ? $phrase : explode(' ', $phrase);
        return $keywords;
    }
        
    /**
     * Retrieve file with related data
     *
     * @param int $id //resource id
     */
    public function findResourceWithRelatedDetails($id) {
        return \Resources::with('type', 'user')->where('id', $id)->orderBy('created_at', 'DESC')->first();
    }

    /**
     * Retrieve specific resource by id
     *
     * @param int $id
     */
    public function find($id) {
        return \Resources::find($id);
    }

    /**
     * Retrieve resources by their resource type
     *
     * @param int $type_id
     */
    public function getFileByType($type_id) {
        $id = \Auth::id();
        $permissions = new \GroupForm();

        if (!($permissions->isGeneralUser($id))) {
            $filesToShow = \Resources::where('resource_type', $type_id)->library()->get();
            return $filesToShow;
        } else {
            return \Response::make("Insufficient permissions.");
        }
    }

    /**
     * Store a new resource on the server, then record its relevant data in the database
     *
     * @param null
     * @return Redirect
     */
    public function storeNewResource($new_type = null) {
        //validate new resource input
        $validator = self::validateNewResourceInput();
        //if validation fails, tell the user to rethink his input
        if ($validator->fails()) {
            return \Redirect::back()->with('Failure', 'Could not create resource with the input given.')->withInput()->withErrors($validator);
        }

        //init variables
        $type = \Input::get('resource_type');
        $modified_type = isset($type) ? $type : null;
        $file = \Input::file();
        
        //if user decided to use a link instead of upload a file, store form data in database
        if (\Input::get('is_link') || \Input::get('is_wistia')) {
            $saved = $this->saveFileWithURL();
            return 1;
        }
        
        //if user decides to upload file, store file and file data in database
        if (\Input::hasFile('file')) {
            return $saved_file = self::storeNewResourceFile($file, $modified_type);
        }
        
        return \Redirect::back()->with('Failure', 'Critical error. Could not save any portion of the submission. Please contact the web administrator.')->withInput();
    }
    
    public function saveFileWithURL($id = null) {
        $linkedFile = self::createOrUpdateResource($id);
        if (\Input::get('is_wistia')) {
            if (preg_match('/"([^"]+)"/', \Input::get('url'), $embed)) {
                $linkedFile->resource_name = $embed[1];
            }
            $linkedFile->wistia = 1;
        } else {
            $linkedFile->resource_name = trim(\Input::get('url'));
            $linkedFile->external_link = 1;
        }
        $linkedFile->filename = trim(\Input::get('filename'));
        $linkedFile->description = trim(\Input::get('data'));
        $linkedFile->resource_type = \Input::get('resource_type');
        $linkedFile->file_size = '0';
        $linkedFile->file_type = 'link';
        $linkedFile->author = \Input::get('user');
        $linkedFile->expire_date = trim(\Input::get('expiration'));
        $linkedFile->featured = 0;
        $linkedFile->resource_share_link = '';
        $linkedFile->access_level = trim(\Input::get('permission'));
        $linkedFile->resource_container = \Input::get('access_level');
        if ($linkedFile->save()) {
            $resource = \Resources::find($linkedFile->id);
            $resource->resource_share_link = \URL::route('app.preview.resource', $linkedFile->id);
            $resource->save();
            return 1;
        }
    }
    
    private function createOrUpdateResource($id)
    {
        if (is_null($id)) {
            return $linkedFile = new \Resources;
        }
        return $linkedFile = \Resources::find($id);
    }
   
    
    private function validateNewResourceInput()
    {
        $params = self::getNewResourceValidationParams();
        //return $params['input'];
        return $validator = \Validator::make($params['input'], $params['rules']);
    }

    private function storeNewResourceFile($file, $new_type) {
        $storage = new \Resources;
        $numOfFiles = count($file['file']); //number of files in array with index 'file'
        $author_id = \Auth::id();
        if ($numOfFiles > 1) {
            return \Redirect::back()->with('Failure', 'Please only add one file per new resource. Thank you.');
        } //fail if there is more than one file
        
        $filename = trim(\Input::get('filename'));
        $originalFileName = $file['file']->getClientOriginalName();
        $local_store_path = public_path() . '/storage/files/Library/fullsized/' . $originalFileName;
        $f_size = self::humanFileSize($file['file']->getSize());
        $f_type = $file['file']->getClientOriginalExtension();
        $featured = 0;
        $description = trim(\Input::get('data'));
        $expiration = trim(\Input::get('expiration'));
        return $store_file = self::storeFileLocally($author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $file['file'], $expiration, $new_type);
    }

    public function getNewResourceValidationParams() {
        return ['input' =>
            [
                'author' => \Input::get('user'),
                'filename' => trim(\Input::get('filename')),
                'description' => trim(\Input::get('data')),
                'resource_type' => trim(\Input::get('resource_type')),
                'expire_date' => trim(\Input::get('expiration')),
                'access_level' => trim(\Input::get('access_level')),
                'permission' => trim(\Input::get('permission')),
                'file' => \Input::file('file'),
                'url' => trim(\Input::get('url'))
            ],
            'rules' =>
            [
                'author' => 'required',
                'filename' => 'required',
                'description' => 'required',
                'resource_type' => 'required|NotIn:0',
                'access_level' => 'required|NotIn:0',
                'permission' => 'required|NotIn:0',
                'file' => 'required_without:url',
                'url' => 'required_without:file'
            ]
        ];
        /**
         * If we ever need to restrict file types add this rule below with parameters delineated by commas
         * 'file' => 'required|mimes:jpeg,png,jpg,gif,zip,avi,css,doc,docx,eps,excel,mov,mp3,pdf,ppt,psd,txt,wav,zip'
         */
    }
    
    public function storeNewForumFileAttachment($topic) {
        $file = \Input::file('attachment');
        $numOfFiles = count($file); //number of files in array with index 'file'
        $author_id = \Auth::id();
        if ($numOfFiles > 1) {
            return \Redirect::back()->with('Failure', 'Please only add one file per post. Thank you.');
        } //fail if there is more than one file
        
        $filename = trim(\Input::get('filename'));
        $originalFileName = $file->getClientOriginalName();
        $local_store_path = public_path() . '/storage/files/Attachments/' . $originalFileName;
        $f_size = self::humanFileSize($file->getSize());
        $f_type = $file->getClientOriginalExtension();
        $featured = 0;
        $description = 'Forum Post Attachment';
        $store_file = self::storeFileLocally($author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $file, NULL, NULL, $topic);
        //dd($store_file);
        return $store_file;
    }

    /**
     * Update file being used for a resource
     *
     * @return response
     */
    public function updateResourceFile($id) {
        $file = \Input::file();
        $author_id = \Auth::id();
        if (\Input::get('is_link') || \Input::get('is_wistia')) {
            $saved = $id != null ? $this->saveFileWithURL($id) : $this->saveFileWithURL();
            return 1;
        } else {
            if (\Input::hasFile('file')) {
                //intialize variables
                $data = \Input::file(); //array of files
                $numOfFiles = count($data['file']); //number of files in array with index 'file'
                
                if ($numOfFiles > 1)
                    return Redirect::back()->with('Failure', 'Too many files selected for this update.');
                for ($i = 0; $i < $numOfFiles; $i++) {
                    foreach ($data as $key => $value) {
                        $filename = trim(\Input::get('filename'));
                        $originalFileName = $value[$i]->getClientOriginalName();
                        $local_store_path = public_path() . '/storage/files/Library/fullsized/' . $originalFileName;
                        $f_size = self::humanFileSize($value[$i]->getSize());
                        $f_type = $value[$i]->getClientOriginalExtension();
                        $featured = 0;
                        $description = trim(\Input::get('description'));
                        $expiration = trim(\Input::get('expiration'));
                        $store_file = self::storeFileUpdate($id, $author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $value[$i], $expiration);
                        
                    }
                }
                return 1;
            } else {
                return 0;
            }
        }
    }

    /**
     * Replace resource file only -- doesn't update other resource information
     *
     * @param int $resource_id
     */
    public function replaceResourceFile($id) {
        $file = \Input::file();
        $author_id = \Auth::id();

        if (\Input::get('is_link') || \Input::get('is_wistia')) {
            $saved = $this->saveFileWithURL();
            return 1;
        } else {
            if (\Input::hasFile('file')) {
                //intialize variables
                $data = \Input::file(); //array of files
                $numOfFiles = count($data['file']); //number of files in array with index 'file'
                if ($numOfFiles > 1)
                    return Redirect::back()->with('Failure', 'Too many files selected for this update.');
                for ($i = 0; $i < $numOfFiles; $i++) {
                    foreach ($data as $key => $value) {
                        $filename = trim(\Input::get('filename'));
                        $originalFileName = $value[$i]->getClientOriginalName();
                        $local_store_path = public_path() . '/storage/files/Library/fullsized/' . $originalFileName;
                        $f_size = self::humanFileSize($value[$i]->getSize());
                        $f_type = $value[$i]->getClientOriginalExtension();
                        $featured = 0;
                        $description = trim(\Input::get('description'));
                        $expiration = trim(\Input::get('expiration'));
                        $store_file = self::storeResourceFileOnly($id, $author_id, $filename, $originalFileName, $f_size, $f_type, $value[$i], $expiration);
                    }
                }
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function humanFileSize($size, $unit = "") {
        if ((!$unit && $size >= 1 << 30) || $unit == "GB")
            return number_format($size / (1 << 30), 2) . "GB";

        if ((!$unit && $size >= 1 << 20) || $unit == "MB")
            return number_format($size / (1 << 20), 2) . "MB";

        if ((!$unit && $size >= 1 << 10) || $unit == "KB")
            return number_format($size / (1 << 10), 2) . "KB";

        return number_format($size) . " bytes";
    }

    /**
     * Store new resource locally and record its data into the database
     * 
     * @author Derek Foster
     */
    public function storeFileLocally($author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $file, $expiration = null, $new_type = null, $topic = null) {
        //store file meta data to the database for reference in the application
        $new_resource = new \Resources;

        //hash for link sharing
//        $share_link_hash = '#' . strtolower(preg_replace('/ /', '_', $filename));
        $stored_file_name = time(). '_' . preg_replace('/ /', '_', $originalFileName);
//        $file_path = public_path().'/storage/files/Library/fullsized/'.$stored_file_name;
////        if(file_exists($file_path)){
////            $stored_file_name = $author_id.'_'.time().'_'.preg_replace('/ /', '_', $originalFileName);
////        }
        
        //check current section
        $typeGiven = trim(\Input::get('resource_type'));
        if ($typeGiven != null || \Input::hasFile('attachment')) {
            $new_resource->author = $author_id;
            $new_resource->filename = isset($topic) ? $topic->title.' - Attachment' : $filename;
            $new_resource->file_size = $f_size;
            $new_resource->file_type = $f_type;
            $new_resource->description = $description;
            if(\Input::hasFile('attachment')) {
                $new_resource->resource_type = 1;
                $new_resource->is_forum_attachement = 1;
                $new_resource->topic_id = $topic->id;
            } else {
                $new_resource->resource_type = is_null($new_type) ? $typeGiven : $new_type;
            }
            $new_resource->featured = $featured;
            $new_resource->resource_name = $stored_file_name;
            $new_resource->resource_share_link = '';
            $permission = \Input::get('permission');
            $new_resource->access_level = isset($permission) ? trim($permission) : 4;
            $access_level = \Input::get('access_level');
            $new_resource->resource_container = isset($access_level) ? $access_level : 0;
            if ($expiration != '' || isset($expiration)) {
                $new_resource->expire_date = \Carbon\Carbon::createFromTimeStamp(strtotime($expiration));
            }
            if ($new_resource->save()) {
                $resource = \Resources::find($new_resource->id);
                $resource->resource_share_link = \URL::route('app.preview.resource', $new_resource->id);
                $resource->save();
                //move file from the tmp directory to '/storage/files/Library/fullsized/', using public server path as the base string
                if(\Input::hasFile('attachment')){
                    $file->move(public_path() . '/storage/files/Attachments', $stored_file_name);
                }else{
                    $file->move(public_path() . '/storage/files/Library/fullsized', $stored_file_name);   
                }
                return 1;
            }

            return 0;
        } else {
            return \Redirect::back()->with('Failure', 'Could not find a valid resource type for your file. File was not saved.');
        }
    }

    /**
     * Store new updated file for a resource
     *
     */
    public function storeFileUpdate($id, $author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $file, $expiration = null) {
        //store file meta data to the database for reference in the application
        $old_resource = \Resources::find($id);

        /*$remove_old = self::destroyResourceFile($author_id, $originalFileName);

        if (!$remove_old) {
            return Redirect::back()->withInput()->with('Failure', 'Could not remove old resource. Please contact the web administrator.');
        }*/

        //new version number
        $old_v = intval($old_resource->file_version);
        $new_v = $old_v + 1;
        
        
        //hash for link sharing
        $share_link_hash = '#' . strtolower(preg_replace('/ /', '_', $filename));

        //check current section
        $typeGiven = trim(\Input::get('file_type')) == '' ? trim(\Input::get('resource_type')) : trim(\Input::get('file_type'));
        
        if ($typeGiven != null) {
            $attributes = [
                'filename' => $filename,
                'file_size' => $f_size,
                'file_type' => $f_type,
                'description' => $description,
                'resource_type' => $typeGiven,
                'featured' => $featured,
                'resource_name' => $author_id . '_' . preg_replace('/ /', '_', $originalFileName),
                'resource_share_link' => \URL::route('app.preview.resource', $id),
                'file_version' => $new_v
            ];
            if (!is_null($expiration) && $expiration != '') {
                $attributes['expire_date'] = \Carbon\Carbon::createFromTimeStamp(strtotime($expiration));
            }
            $old_resource->update($attributes);
            //move file from the tmp directory to '/storage/files/Library/fullsized/', using public server path as the base string
            $file->move(public_path() . '/storage/files/Library/fullsized', $author_id . '_' . preg_replace('/ /', '_', $originalFileName));
            return 1;
        } else {
            return \Redirect::back()->with('Failure', 'Could not find a valid resource type for your file. File was not saved.');
        }
    }

    /**
     * Update resource file info only
     */
    public function storeResourceFileOnly($id, $author_id, $filename, $originalFileName, $f_size, $f_type, $file, $expiration = null) {
        //store file meta data to the database for reference in the application
        $old_resource = \Resources::find($id);

        $remove_old = self::destroyResourceFile($author_id, $old_resource->resource_name);

        if (!$remove_old) {
            return Redirect::back()->withInput()->with('Failure', 'Could not remove old resource. Please contact the web administrator.');
        }

        //new version number
        $old_v = intval($old_resource->file_version);
        $new_v = $old_v + 1;

        //hash for link sharing
        $share_link_hash = '#' . strtolower(preg_replace('/ /', '_', $filename));

        //check current section
        $typeGiven = trim(\Input::get('file_type'));
        if ($typeGiven != null) {
            $attributes = [
                'file_size' => $f_size,
                'file_type' => $f_type,
                'resource_type' => $typeGiven,
                'resource_name' => $author_id . '_' . preg_replace('/ /', '_', $originalFileName),
                'resource_share_link' => \URL::route('app.preview.resource', $id),
                'file_version' => $new_v . '.0'
            ];
            if (!is_null($expiration) && $expiration != '') {
                $attributes['expire_date'] = \Carbon\Carbon::createFromTimeStamp(strtotime($expiration));
            }
            $old_resource->update($attributes);
            //move file from the tmp directory to '/storage/files/Library/fullsized/', using public server path as the base string
            $file->move(public_path() . '/storage/files/Library/fullsized', $author_id . '_' . preg_replace('/ /', '_', $originalFileName));
            return 1;
        } else {
            return \Redirect::back()->with('Failure', 'Could not find a valid resource type for your file. File was not saved.');
        }
    }

    /**
     * Store new resource type
     *
     * @param string $type_name
     */
    public function storeNewType($type_name) {
        $new_type = new \FileTypes;

        $new_type->type_name = $type_name;

        if ($new_type->save()) {
            return $new_type->id;
        }

        return 0;
    }

    /**
     * Retrieve resources associated to a particular user 
     *
     * @param int $id //user id
     */
    public function getUserResources($id) {
        $filesToShow = \Resources::where('author', $id)->get();

        return $filesToShow;
    }

    /**
     * Download resource
     *
     * @param string $filename
     */
    public function downloadResource($filename) {
        $is_attachment = \Input::get('is_attachment');
        $resource = \Resources::where('resource_name', 'LIKE', "%$filename%")->first();
        if($is_attachment === 'true'){
            $file = public_path() . "/storage/files/Attachments/" . $resource->resource_name;
        }else{
            $file = public_path() . "/storage/files/Library/fullsized/" . $resource->resource_name;   
        }
        $mimeType = substr($filename, strpos($filename, ".") + 1);
        $headers = [
            'Content-Type:' => 'application/' . $mimeType,
        ];
        $num = $resource->download_count;
        $newNum = $num + 1;
        $resource->download_count = $newNum;
        $resource->save();
        return \Response::download($file, $filename, $headers);
    }

    /**
     * Download multiple resources
     *
     * @param array $file_list
     */
    public function downloadMulti($file_list) {
        $list = $file_list;
        $file_path = public_path() . "/storage/files/Library/fullsized/";
        $tmp_path = public_path() . "/storage/tmp_zips/";
        $is_wistia = \Input::only('is_wistia');
        $is_link = \Input::only('is_link');
        
        $zip = new \ZipArchive();
        $zip_name = time() . ".zip"; // Zip name
        $zip->open($tmp_path . $zip_name, \ZipArchive::CREATE);
        foreach ((array) $list as $key => $file) {
            $files = explode(",", $file);
            foreach ($files as $key => $file_id) {
                //increment file's download count
                $resource = \Resources::where('id', $file_id)->first();
                $num = $resource->download_count;
                $newNum = $num + 1;
                $increment = \Resources::find($file_id);
                $increment->download_count = $newNum;
                if (!$increment->save()) {
                    throw new \Exception('Could not update the download count for the resource you are trying to download.');
                }

                if (preg_match('/wistia/', $resource->resource_name) || preg_match('/http/', $resource->resource_name)) {
                    return 'link';
                }

                //get real file name
                $path = $file_path . $resource->resource_name;
                
                self::checkIfFileExists($path, $zip);
            }
        }
        $zip->close();

        $url = \URL::to('/') . '/storage/tmp_zips/' . $zip_name;

        return $url;
    }
   
    
    private function checkIfFileExists($path, $zip){
        if (file_exists($path)) {
            $zip->addFromString(basename($path), file_get_contents($path));
        } else {
            echo "file does not exist";
        }
    }

    /**
     * Update resource type
     *
     * @param int $resource_id
     */
    public function updateResourceType($resource_id, $new_type) {
        $resource = self::find($resource_id);

        $resource->resource_type = $new_type;

        if ($resource->save()) {
            return 1;
        }

        return 0;
    }

    /**
     * Update file's existing file information 
     *
     * @return Back to resource page with success or failure session flash
     */
    public function updateResourceInfo() {
        //get validation reqs
        $input = self::getNewResourceValidationParams();
        
        //set and check validation
        $validator = \Validator::make($input['input'], $input['rules']);

        if ($validator->fails()) {
            return 0;
        } else {
            $file = self::find(Input::get('file_id'));
            $file->update($input['input']);
            return 1;
        }
    }

    public function getNewFileInput() {
        return $input = array(
            'filename' => trim(\Input::get('filename')),
            'description' => trim(\Input::get('description')),
            'file_version' => trim(\Input::get('file_version')),
            'sub_version_number' => trim(\Input::get('sub_version_number'))
        );
    }

    public function getFileInfoChangeValidator() {
        return $rules = array(
            'file_name' => 'required',
            'owner_name' => 'required'
        );
    }

    /**
     * Delete resource from viewable lists -- not a permanent deletion
     *
     * @param int $resource_id
     */
    public function deleteResource($resource_id) {
        $checkExist = new \GroupForm;
        if ($checkExist->isValidForDelete($resource_id)) {
            $file = self::find($resource_id);
            if ($file->delete())
                return 1;

            return 0;
        }else {
            return 0;
        }
    }

    /**
     * Restore a soft deleted resource
     *
     * @param int $resource_id
     */
    public function reverseDeleteResource($resource_id) {
        if (\Resources::withTrashed()->where('id', $resource_id)->get()) {
            $restore = \Resources::withTrashed()->where('id', $resource_id)->restore();
            return 1;
        }

        return 0;
    }

    /**
     * Destroy resource permanently 
     *
     * @param int $resource_id
     */
    public function destroyResource($resource_id) {
        //remove file from server 
        $file = \Resources::onlyTrashed()->where('id', $resource_id)->first();

        //grab real file name
        $real_file_name = $file->resource_name;
        foreach (glob(public_path() . "/storage/files/Library/fullsized/" . $real_file_name) as $filename) {
            //deletes the previous files
            unlink($filename);
        }

        if (is_object($file)) {
            $files = \Resources::withTrashed()->where('id', $resource_id)->forceDelete();
            return 1;
        }

        return 0;
    }

    /**
     * Destroy resource file
     *
     * @param int $user_id
     * @param string $file
     */
    public function destroyResourceFile($user_id, $file) {
        try {
            foreach (glob(public_path() . "/storage/files/Library/fullsized/" . $file) as $filename) {
                //deletes the previous files
                unlink($filename);
            }
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }

}
