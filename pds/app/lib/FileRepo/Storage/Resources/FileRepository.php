<?php

namespace FileRepo\Storage\Resources;

interface FileRepository {
	public function all();

	public function allWithTrashed();

	public function allWithForeignDetails($type);

	public function findResourceWithRelatedDetails($id);

	public function find($id);

	public function getFileByType($type_id);

	public function storeFileLocally($author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $file, $expiration = null, $new_type = null);

	public function storeFileUpdate($id, $author_id, $filename, $originalFileName, $description, $f_size, $f_type, $local_store_path, $featured, $file, $expiration = null);

	public function storeResourceFileOnly($id, $author_id, $filename, $originalFileName, $f_size, $f_type, $file, $expiration = null);

	public function replaceResourceFile($id);

	public function humanFileSize($size, $unit);

	public function storeNewResource($new_type = null);

	public function updateResourceFile($id);

	public function getUserResources($id);

	public function downloadResource($filename);

	public function downloadMulti($file_list);

	public function updateResourceInfo();

	public function getNewFileInput();

	public function getFileInfoChangeValidator();

	public function updateResourceType($resource_id, $new_type);

	public function deleteResource($resource_id);

	public function reverseDeleteResource($resource_id);

	public function destroyResource($resource_id);

	public function destroyResourceFile($user_id, $file);
}