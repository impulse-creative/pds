<?php
namespace Files\Store;

use ForumTopics;
use ForumMessages;
use Resources;
use Input;

class StoreFiles {
    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
    }
    
    public function storeMessageAttachment($input, $message_id){
        $file_data = $this->getMessageAttachmentRequiredInput($input, $message_id);
        //hash for link sharing
        $share_link_hash = '#' . strtolower(preg_replace('/ /', '_', $file_data['filename']));
        $stored_file_name = $file_data['resource_name'];
        $file_path = public_path().'/storage/files/Attachments/'.$stored_file_name;
        $file = Input::file('attachment');
        if(file_exists($file_path)){
            $stored_file_name = $file_data['author'].'_'.time().'_'.preg_replace('/ /', '_', $file_data['resource_name']);
        }

        $new_attachment = Resources::create($file_data);
        if ($new_attachment) {
            $saveToMsg = $this->addAttachmentIdToMessageRecord($message_id, $new_attachment->id);
            //add share link to resource
            $resource = \Resources::find($new_attachment->id);
            $resource->resource_share_link = \URL::route('app.preview.resource', $new_attachment->id);
            $resource->save();
            //move file from the tmp directory to '/storage/files/Library/fullsized/', using public server path as the base string
            $file->move(public_path() . '/storage/files/Attachments', $stored_file_name);
            return $this->listener->replyWithAttachmentSuccess($message_id);
        }
        
        return $this->listener->replyWithAttachmentFailure($message_id);
    }
    
    
    public function addAttachmentIdToMessageRecord($message_id, $attachment_id) {
        $message = ForumMessages::find($message_id);
        $message->attachment_id = $attachment_id;
        return $message->save();
    }
    
    public function getMessageAttachmentRequiredInput($input, $message_id){
        $file_storage_data = array();
        $file = \Input::file('attachment');
        $numOfFiles = count($file); //# of files in array
        $file_storage_data['author'] = \Auth::id();
        if ($numOfFiles > 1) {
            $this->listener->tooManyAttachments($numOfFiles);
        } //fail if there is more than one file
        $file_storage_data['filename'] = 'Message Attachment';
        $stored_file_name = time() . '_' . preg_replace('/ /', '_', $file->getClientOriginalName());
        $file_storage_data['resource_name'] = $stored_file_name;
        $file_storage_data['file_size'] = $this->humanFileSize($file->getSize());
        $file_storage_data['file_type'] = $file->getClientOriginalExtension();
        $file_storage_data['featured'] = 0;
        $file_storage_data['description'] = 'Forum Message Attachment';
        $file_storage_data['forum_message_id'] = $message_id;
        $file_storage_data['resource_type'] = 7;
        $file_storage_data['created_at'] = \Carbon\Carbon::now();
        $file_storage_data['updated_at'] = \Carbon\Carbon::now();
        $file_storage_data['access_level'] = $this->getAccessLevel(Input::get('parent_topic'));
        $file_storage_data['is_forum_attachement'] = 1;
        return $file_storage_data;
    }
    
    public function storeTopicAttachment($input, $topic_id){
        $file_data = $this->getTopicAttachmentRequiredInput($input, $topic_id);
        
        //hash for link sharing
        $share_link_hash = '#' . strtolower(preg_replace('/ /', '_', $file_data['filename']));
        $stored_file_name = $file_data['resource_name'];
        $file_path = public_path().'/storage/files/Attachments/'.$stored_file_name;
        $file = Input::file('attachment');
        if(file_exists($file_path)){
            $stored_file_name = $file_data['author'].'_'.time().'_'.preg_replace('/ /', '_', $file_data['resource_name']);
        }

        $new_attachment = Resources::create($file_data);
        if ($new_attachment) {
            $saveToTopic = $this->addAttachmentIdToTopic($topic_id, $new_attachment->id);
            //add share link to resource
            $resource = \Resources::find($new_attachment->id);
            $resource->resource_share_link = \URL::route('app.preview.resource', $new_attachment->id);
            $resource->save();
            //move file from the tmp directory to '/storage/files/Library/fullsized/', using public server path as the base string
            $file->move(public_path() . '/storage/files/Attachments', $stored_file_name);
            return $this->listener->replyWithTopicAttachmentSuccess($topic_id);
        }
        
        return $this->listener->replyWithTopicAttachmentFailure($topic_id);
    }
    
    public function addAttachmentIdToTopic($topic_id, $attachment_id) {
        $topic = ForumMessages::find($topic_id);
        $topic->attachment_id = $attachment_id;
        return $topic->save();
    }
    
    public function getTopicAttachmentRequiredInput($input, $topic_id){
        $file_storage_data = array();
        $file = \Input::file('attachment');
        $numOfFiles = count($file); //# of files in array
        $file_storage_data['author'] = \Auth::id();
        if ($numOfFiles > 1) {
            $this->listener->tooManyAttachments($numOfFiles);
        } //fail if there is more than one file
        $file_storage_data['filename'] = 'Topic Attachment';
        $stored_file_name = time() . '_' . preg_replace('/ /', '_', $file->getClientOriginalName());
        $file_storage_data['resource_name'] = $stored_file_name;
        $file_storage_data['file_size'] = $this->humanFileSize($file->getSize());
        $file_storage_data['file_type'] = $file->getClientOriginalExtension();
        $file_storage_data['featured'] = 0;
        $file_storage_data['description'] = 'Forum Message Attachment';
        $file_storage_data['topic_id'] = $topic_id;
        $file_storage_data['resource_type'] = 7;
        $file_storage_data['created_at'] = \Carbon\Carbon::now();
        $file_storage_data['updated_at'] = \Carbon\Carbon::now();
        $file_storage_data['access_level'] = $this->getAccessLevel($topic_id);
        $file_storage_data['is_forum_attachement'] = 1;
        return $file_storage_data;
    }
    
    public function getAccessLevel($parent_topic) {
        $topic = ForumTopics::with('categories')->where('id', $parent_topic)->first();
        return $topic->categories->min_access_level;
    }
    
    public function humanFileSize($size, $unit = "") {
        if ((!$unit && $size >= 1 << 30) || $unit == "GB")
            return number_format($size / (1 << 30), 2) . "GB";

        if ((!$unit && $size >= 1 << 20) || $unit == "MB")
            return number_format($size / (1 << 20), 2) . "MB";

        if ((!$unit && $size >= 1 << 10) || $unit == "KB")
            return number_format($size / (1 << 10), 2) . "KB"; 

        return number_format($size) . " bytes";
    }
}