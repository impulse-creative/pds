<?php

namespace GroupRepo\Permissions;

use Illuminate\Support\ServiceProvider;

class AclServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'GroupRepo\Permissions\Groups\GroupRepository',
			'GroupRepo\Permissions\Groups\EloquentGroupRepository'
		);
	}
}