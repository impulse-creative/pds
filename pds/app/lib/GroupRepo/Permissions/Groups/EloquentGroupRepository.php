<?php

/**
 * Title: Service provider for group permissions
 * Date : 11/12/14
 * Description: This service provider enables the ability to provide restriction handling on any route or view in
 * 				the application. There are areas where users are not allowed to visit, so the service provider
 * 				manages where to send them in the event they gain access or are denied access
 * 
 * @author: Derek J. Foster
 */

namespace GroupRepo\Permissions\Groups;

use Groups;

class EloquentGroupRepository extends \BaseController implements GroupRepository {

    /**
     * Retrieve all available permission groups 
     * 
     * @return object array
     */
    public function all() {
        return \Group::all();
    }

    /**
     * Get current user groups
     *
     * @param int $id //user's id
     */
    public function getUserGroups() {
        if (\Auth::check()) {
            $group_string = \Auth::user()->groups;
            $groups = explode(':', $group_string);

            $acl_list = [];
            foreach ($groups as $group) {
                $acl_list[] = $group;
            }

            return $acl_list;
        }

        return 0;
    }

    /**
     * Retrieve all available permission groups in simple array
     *
     * @return numerical array
     */
    public function allInArray() {
        $groups = \Group::all();
        $group_list = [];

        $group_list[0] = "--Select Minimum Permission Level--";
        foreach ($groups as $group) {
            $group_list[$group->id] = $group->description;
        }

        return $group_list;
    }
    
    /**
     * Retrieve all available permission groups in simple array
     *
     * @return numerical array
     */
    public function allInArrayForPermissionUpdates() {
        $groups = \Group::all();
        $group_list = [];

        $group_list[0] = "--Select Minimum Permission Level--";
        foreach ($groups as $group) {
            $group_list[$group->id] = $group->hs_field;
        }

        return $group_list;
    }

    /**
     * Retrieve all hubspot synced groups
     *
     * @return numerical array
     */
    public function allInHubspot() {
        $groups = \Group::synced()->get();
        $group_list = [];

        $group_list[0] = "All Resources";
        foreach ($groups as $group) {
            $group_list[$group->id] = $group->description;
        }

        return $group_list;
    }
    
    public function grantAccessGroupPermissions($groupId, $userId) {
        $user = \User::find($userId);
        $groups = \AccessGroup::find($groupId);
        $user->groups = $groups->permissions;
        if($user->save()) {
            return true;
        }
        return false;
        //this is bad and they have permissions they shouldnt -- we can check
        // for these fall thorughs by searching for NON company people 
        // with paid member permissions
    }
    
    /**
     * Get restriction group data by id
     *
     * @param int $id //id of group
     */
    public function getGroupById($id) {
        return \Group::findOrFail($id);
    }

    /**
     * Get resource restriction group data by id
     *
     * @param int $id //id of resource group
     */
    public function getResourceGroupById($id) {
        return \ResourceGroups::find($id);
    }

    /**
     * Get records that are NOT for resources
     *
     */
    public function getNonResourceGroups($return_array = false) {
        $groups = \Group::all();
        if ($return_array)
            return $groups;
        $group_list = [];

        $group_list[0] = "All Resources";
        foreach ($groups as $group) {
            $group_list[$group->id] = $group->description;
        }

        return $group_list;
    }

    /**
     * Get records that are for resources only 
     *
     */
    public function getResourceGroups($return_array = false, $with_permission_details = false) {
        $groups = \ResourceGroups::with('group')->get();
        if ($return_array)
            return $groups;

        $group_list = [];

        $group_list[0] = "--Select Access Level--";
        foreach ($groups as $group) {
            //check permissions for user
            $usr_groups = self::getUserGroups(\Auth::id());
            
            //dd($usr_groups);
            if (is_null($usr_groups))
                return \Redirect::route('dash.main');
            if (in_array($group->permitted_groups, $usr_groups)) {
                $group_list[$group->id] = $group->description;
            }
        }
        
        return $group_list;
    }

    /**
     * Cross check user groups against resource group accessible permissions
     *
     * @param array $user_groups
     * @param array $resource_group_id
     */
    public function checkAccessToResourceGroup($user_groups, $resource_group_id) {
        if (is_object($user_groups)) {
            throw new \Exception('Invalid data format for the user groups list passed through: checkAccessToResourceGroup(), ResourceGroups Model');
        }
        if (in_array($resource_group_id, $user_groups)) {
            return 1;
        }

        return 0;
    }

    /**
     * Get restriction group data by list of ids
     *
     * @param arr $groups //list of group ids
     */
    public function getGroupsByArray($groups) {
        /* query real group names from list of ids */
        $usr_group_list = \Group::whereIn('id', $groups)->get();
        $group_list = array();
        /* store group names in array to send back in response */
        foreach ($usr_group_list as $key => $group) {
            $group_list[] = $group->name;
        }

        return $group_list;
    }

    /**
     * Add user as a pds admin
     *
     * @param int $id
     */
    public function addToGroup($id, $group_id) {
        $response = self::getDefault($id, $group_id);
        if (!$response) {
            throw new \Exception('Could not add permissions to specific user. Group ID: ' . $group_id);
        }
        return $response;
    }

    /**
     * Add new company and contact field
     *
     * @param 
     */
    public function addPropertyToHS($group_name, $new_group) {

        //create a hubspot field for the new group
        $newprops = [
            'permit_' . $new_group => 'checkbox'
        ];

        $arrayobject = new \ArrayObject($newprops);
        foreach ($arrayobject as $key => $value) {
            $property['name'] = $key;
            $property['label'] = ucwords(str_replace('_', ' ', $key));
            $property['description'] = 'User Group Level: ' . $key;
            $property['groupName'] = $group_name;
            $property['type'] = 'enumeration';
            $property['formField'] = false;
            $property['fieldType'] = $value;
            $property['displayOrder'] = 6;
            $property['options'] = [
                0 => ['label' => 'Yes', 'value' => 'true'],
                1 => ['label' => 'No', 'value' => 'false']
            ];
            $contacts = \HubSpot::Properties();
            $newproperty = $contacts->create_property($key, $property);

            /* get responses */
            $hs_response = json_encode($newproperty);
            $error = $contacts->getLastStatus();
            $errors = $error;
        }

        /* -- uncomment this if you want to check if the property exists in the group
          echo var_dump($contacts->get_property_group($group_name)); -- */
        return ['hs_response' => $hs_response, 'errors' => $errors];
    }

    public function updateGroupName($name, $new_name, $permission_group) {
        //create a hubspot field for the new group
        $newprops = [
            'permit_' . $name => $new_name
        ];
        //echo var_dump($name);
        //return var_dump($new_name);
        $arrayobject = new \ArrayObject($newprops);
        foreach ($arrayobject as $key => $value) {
            $property['name'] = $value;
            $property['label'] = ucwords(str_replace('_', ' ', $key));
            $property['description'] = 'User Group Level: ' . $key;
            $property['groupName'] = $permission_group;
            $property['type'] = 'enumeration';
            $property['formField'] = false;
            $property['fieldType'] = 'checkbox';
            $property['displayOrder'] = 6;
            $property['options'] = [
                0 => ['label' => 'Yes', 'value' => 'true'],
                1 => ['label' => 'No', 'value' => 'false']
            ];
            $contacts = \HubSpot::Properties();
            $newproperty = $contacts->update_property($key, $property);

            /* get responses */
            $hs_response = json_encode($newproperty);
            $error = $contacts->getLastStatus();
            $errors = $error;
        }

        return ['hs_response' => $hs_response, 'errors' => $errors];
    }

    public function removeFromHSGroup($property) {
        //remove from HS first
        $hs_contact_properties = \HubSpot::Properties();

        $remove_property = $hs_contact_properties->delete_property($property);

        $hs_response = json_encode($remove_property);
        $errors = $hs_contact_properties->getLastStatus();

        return ['hs_response' => $hs_response, 'errors' => $errors];
    }

    /**
     * Get default permissions 
     *
     * @param int $group_id
     */
    public function getDefault($user_id, $group_id) {
        switch ($group_id) {
            case self::getVendorId():
                $user = \User::find($user_id);
                $user_groups = explode(':', $user->groups);
                $default = [
                    self::getVendorId(),
                    self::getIndustryForumModId(),
                    self::getEventsId(),
                    self::getExhibitorModId()
                ];
                foreach ($default as $key => $value) {
                    $user_groups[] = $value;
                }
                $user->groups = implode(':', $user_groups);
                if ($user->save()) {
                    return 1;
                }
                return 0;
                break;

            case self::getGeneralId():
                $user = \User::find($user_id);
                $user_groups = explode(':', $user->groups);
                $default = [
                    self::getIndustryForumModId(),
                    self::getGeneralId()
                ];
                foreach ($default as $key => $value) {
                    $user_groups[] = $value;
                }
                $user->groups = implode(':', $user_groups);
                if ($user->save()) {
                    return 1;
                }
                return 0;
                break;

            case self::getCustomerId():
                $user = \User::find($user_id);
                $user_groups = explode(':', $user->groups);
                $default = [
                    self::getResourceId(),
                    self::getMemberResourceId(),
                    self::getMemberForumId(),
                    self::getIndustryForumModId(),
                    self::getEventsId()
                ];
                foreach ($default as $key => $value) {
                    $user_groups[] = $value;
                }
                $user->groups = implode(':', $user_groups);
                if ($user->save()) {
                    return 1;
                }
                return 0;
                break;

            case self::getAdminId():
                $user = \User::find($user_id);
                $user_groups = explode(':', $user->groups);
                $default = [
                    self::getEventModId(),
                    self::getForumModId(),
                    self::getResourceModId()
                ];
                foreach ($default as $key => $value) {
                    $user_groups[] = $value;
                }
                $user->groups = implode(':', $user_groups);
                if ($user->save()) {
                    return 1;
                }
                return 0;
                break;

            case self::getEmployeeId():
                $user = \User::find($user_id);
                $user_groups = explode(':', $user->groups);
                $default = [
                    self::getEventModId(),
                    self::getForumModId(),
                    self::getResourceModId()
                ];
                foreach ($default as $key => $value) {
                    $user_groups[] = $value;
                }
                $user->groups = implode(':', $user_groups);
                if ($user->save()) {
                    return 1;
                }
                return 0;
                break;

            default:
                $user = \User::find($user_id);
                $user_groups = explode(':', $user->groups);
                $default = [
                    self::getIndustryForumModId(),
                    self::getGeneralId()
                ];
                foreach ($default as $key => $value) {
                    $user_groups[] = $value;
                }
                $user->groups = implode(':', $user_groups);
                if ($user->save()) {
                    return 1;
                }
                return 0;
                break;
        }
    }

    /**
     * Get admin id
     */
    public function getAdminId() {
        return '1';
    }

    /**
     * Get customer id
     */
    public function getCustomerId() {
        return '2';
    }

    /**
     * Get vendor id
     */
    public function getVendorId() {
        return '3';
    }

    /**
     * Get General Id
     */
    public function getGeneralId() {
        return '4';
    }

    /**
     * Get impulse admin id
     */
    public function getImpulseId() {
        return '5';
    }

    /**
     * Get forum moderator id
     */
    public function getForumModId() {
        return '6';
    }

    /**
     * Get event moderator id
     */
    public function getEventModId() {
        return '7';
    }

    /**
     * Get exhibitor moderator id
     */
    public function getExhibitorModId() {
        return '8';
    }

    /**
     * Get Resource moderator id
     */
    public function getResourceModId() {
        return '9';
    }

    /**
     * Get industry forum id
     */
    public function getIndustryForumModId() {
        return '10';
    }

    /**
     * Get member forum id
     */
    public function getMemberForumId() {
        return '11';
    }

    /**
     * Get events id
     */
    public function getEventsId() {
        return '12';
    }

    /**
     * Get resource id
     */
    public function getResourceId() {
        return '13';
    }

    /**
     * Get employee id
     */
    public function getEmployeeId() {
        return '14';
    }

    /**
     * Get member resources id
     */
    public function getMemberResourceId() {
        return '15';
    }

    public function getCoreId() {
        return '19';
    }

    public function getEliteId() {
        return '20';
    }

    public function getAdvancedId() {
        return '21';
    }

    /**
     * Initial permissions for new user
     *
     * @param int $id
     */
    public function setInitial($id) {
        $user = \User::find($id);
        $user_groups = explode(':', $user->groups);
        $user_groups[] = '4';
        $user_groups[] = '10';
        $user->groups = implode(':', $user_groups);
        if ($user->save()) {
            return 1;
        } else {
            return 0;
        }
        throw new \Exception('Could not add permissions to specific user. Group IDs: 4 and 10');
    }

    /**
     * Check if the user is a top level impulse administrator
     *
     * @param int $id //id of user
     */
    public function checkIfAdmin($id) {
        $user = \User::find($id);
        $user_groups = explode(':', $user->groups);

        if (in_array('5', $user_groups) || in_array('1', $user_groups) || in_array('impulse_admin', $user_groups)) {
            return 1;
        }

        return 0;
    }

    /**
     * Check if the user is a top level pds administrator
     *
     * @param int $id //id of user
     */
    public function checkIfPdsAdmin($id) {
        $user = \User::find($id);
        $user_groups = explode(':', $user->groups);

        if (in_array('1', $user_groups) || in_array('admin', $user_groups)) {
            return 1;
        }

        return 0;
    }

    /**
     * Check if user is a pds employee
     *
     * @param int $id
     */
    public function checkIfEmployee($id) {
        //
    }

    /**
     * Check if the user is a paying customer, or client
     * This means the user has purchased a membership with PDS
     */
    public function checkIfClient($id) {
        //
    }

    /**
     * Check if the user is a basic vendor
     *
     * @param int $id //id of user
     */
    public function checkIfVendor($id) {
        $perm = new \GroupForm();

        return $perm->isVendor($id);
    }

    /**
     * Check to see if the user has a general account
     * This would mean that we only have their email on file, they haven't paid, and they don't have a marketplace listing
     *
     * @param int $id //user id
     */
    public function checkIfGeneral($id) {
        $user = \User::find($id);
        $user_groups = explode(':', $user->groups);

        if (in_array('4', $user_groups)) {
            return 1;
        }

        return 0;
    }

    /**
     * Method to check access of any user against any restriction
     *
     * @param int $restriction //id of restriction
     */
    public function checkAccess($restriction) {
        if (!intval($restriction))
            throw new \Exception('Not a valid restriction id.');

        if (\Auth::check()) {
            $groups = explode(':', \Auth::user()->groups);
            if (in_array($restriction, $groups))
                return 1;

            return 0;
        }else {
            return \Redirect::route('login.form');
        }
    }

    /**
     * Method to check access of any user against an array of restrictions
     *
     * @param array $restrictions
     */
    public function checkMultiAccess($restrictions) {
        if (\Auth::check()) {
            $groups = explode(':', \Auth::user()->groups);
            foreach ($groups as $level) {
                if (in_array($level, $restrictions))
                    return 1;
            }

            return 0;
        }else {
            return \Redirect::route('dash.main');
        }
    }

    public function checkIsValidForAdd() {
        $valid = new \GroupForm();

        return $valid->isValidForAdd();
    }

    public function checkIsValidForEdit() {
        $valid = new \GroupForm();

        return $valid->isValidForEdit();
    }

    public function checkIsValidForDelete() {
        $valid = new \GroupForm();

        return $valid->isValidForDelete();
    }

    public function updateUserPermissions($id) {
        $user = \User::find($id);
        if($user->companies == '0' || $user->companies == "{}" || $user->account_type == 2 || $user->companies == '"{}"') {
            $user->groups = $this->checkHSPermissions($user);
        } else {
            if(strpos($user->companies, 'pending') > -1) {
                $user->groups = $this->checkHSPermissions($user);
            }
            if(count(json_decode($user->companies, true)) == 0) {
              //  var_dump('self');
                $user->groups = $this->checkHSPermissions($user);
            } else {
                //var_dump('owner');
                $user->groups = $this->getOwnerPermissions(json_decode($user->companies, true));
            }
        }
        if ($user->save()) {
            return 1;
        } else {
            \Auth::logout();
            return Redirect::back()->with('wtf');
        }
    }
    
    public function getOwnerPermissions($companies) {
        $count = 0;
        foreach($companies as $companyId => $employee_type) {
            $count++;
            if($count == 1) { 
                $user = $this->getCompanyOwner($companyId);
            } else {
                
            }
        } 
        return $this->checkHSPermissions($user);
    }
    
    public function getCompanyOwner($companyId) {
        $company = \Company::find($companyId);
        $user = \User::find($company->admin_id);
        return $user;
    }

    public function checkHSPermissions($user) {
        $groups = $this->getPdsPermissions($user);
        $access = $this->checkHubSpotAccess($groups, $user);
        return $access;
    }

    public function checkHubSpotAccess($groups, $user) {
        $access = \User::checkForPermission($groups, $user);
        $permissions = rtrim($access, ':');
        return $permissions;
    }

    public function getPdsPermissions($user) {
        $fields = \User::getAppPermissions();
        return $fields;
    }
    
    

}
