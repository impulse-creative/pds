<?php

namespace GroupRepo\Permissions\Groups;

interface GroupRepository {
	public function all();

	public function getGroupById($id);

	public function getUserGroups();

	public function getResourceGroupById($id);

	public function getResourceGroups();

	public function getNonResourceGroups();

	public function checkAccessToResourceGroup($user_groups, $resource_group_id);

	public function addToGroup($id, $group_id);

	public function addPropertyToHS($group_name, $new_group);

	public function updateGroupName($name, $new_name, $permission_group);

	public function removeFromHSGroup($property);

	public function getAdminId();

	public function getCustomerId();

	public function getVendorId();

	public function getGeneralId();

	public function getImpulseId();

	public function getForumModId();

	public function getEventModId();

	public function getExhibitorModId();

	public function getResourceModId();

	public function getIndustryForumModId();

	public function getMemberForumId();

	public function getEventsId();

	public function checkMultiAccess($restrictions);

	public function getResourceId();

	public function getEmployeeId();

	public function getMemberResourceId();

	public function checkIfAdmin($id);

	public function checkIfEmployee($id);

	public function checkIfClient($id);

	public function checkIfVendor($id);

	public function checkIfGeneral($id);

	public function checkIsValidForAdd();

	public function checkIsValidForEdit();

	public function checkIsValidForDelete();
        
        public function updateUserPermissions($user_id);

}