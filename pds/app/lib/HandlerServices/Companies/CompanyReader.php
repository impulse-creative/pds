<?php
namespace HandlerServices\Companies;

use Validator;
use Auth;
use Company;
use User;
use Input;
use Exception;
use TeamworkCompanies;

class CompanyReader {
    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
    }
    
    public function getAllCompaniesById() {
        $user = User::find(Auth::id());
        
        if(is_null($user->companies) || $user->companies == '0') {
            return $this->listener->noCompaniesAvailable();
        }
        $already_registered = $this->getListOfExistingTwPharmacies($user->id);
        $companyList = json_decode($user->companies);
        $companyIdList = array();
        foreach($companyList as $company => $employee_type) {
            if(in_array($company, $already_registered)) continue;
            $companyIdList[] = $company;
        }
        $companies = Company::whereIn('id', $companyIdList)->get();
        
        return $companies;
    }
    
    public function getListOfExistingTwPharmacies($id) {
        $company_list = $this->getAllUserCompanies($id);
        $registered_pharmacies = TeamworkCompanies::whereIn('company_id', $company_list)->get();
        $companies_registered = array();
        foreach($registered_pharmacies as $pharmacy) {
            $companies_registered[] = $pharmacy->company_id;
        }
        return count($companies_registered) > 0 ? $companies_registered : [];
    }
    
    public function getTwRegisteredCompanies($id = null) {
        $id = is_null($id) ? Auth::id() : $id;
        $company_list = $this->getAllUserCompanies($id);
        $registered_pharmacies = TeamworkCompanies::with('user')->whereIn('company_id', $company_list)->get();
        if(count($registered_pharmacies) > 0) {
            return $registered_pharmacies;
        }
        return 'None';
    }
    
    public function getAllUserCompanies($id) {
        $user = User::find($id);
        
        if(is_null($user->companies) || $user->companies === '0' || $user->companies === '"{}"') {
            return [];
        }
        $companyList = json_decode($user->companies);
        $companyIdList = array();
        foreach($companyList as $company => $employee_type) {
            $companyIdList[] = $company;
        }
        return $companyIdList;
    }
    
    public function getCompanyById($companyId) {
        if($companyId == null) return null;
        $company = Company::find($companyId);
        if(count($company) > 0) {
            return $company;
        }
        
        throw new Exception('Could not find the company requested.');
    }
    
    public function getCompanyApprovals($companies) {
        $allApprovals = [];
        if(is_null($companies) || count($companies) == 0){
            return [];
        }
        foreach($companies as $company) {
            $approvals = \CompanyApprovals::where('company_id', $company->id)->get();
            if($approvals == null) return null;
            foreach($approvals as $approval) {
                $allApprovals[$approval->company_id] = $approval->requesting_user;
            }
        }
        return $allApprovals;
    }
    
    public function getCompanyMembers($companies) {
        if(is_null($companies) || count($companies) == 0){
            return [];
        }
        foreach($companies as $company) {
            $members[$company->id] = $this->getCompanyEmployeesByCompanyId($company->id);
        }
        return $members;
    }
    public function getEmployeeData($companies) {
        $data = [];
        if(is_null($companies) || count($companies) == 0){
            return $data;
        }
        foreach($companies as $company) {
            if($company->employees === '' || is_null($company->employees)) return 'no employees';
            $employees = json_decode($company->employees, true);
            if(count($employees) > 0) {
                foreach($employees as $id => $type) {
                    if(Auth::user()->email !== $company->owner_email) {
                        $employee = User::where('id', $id)->first();
                        $employee->employee_type = $employee->employee_type === 0 ? 2 : $employee->employee_type; //defaulting to staff member cause fuck everything
                        $employee->save();
                    }
                    $data[$company->id][$id] = $type;
                }
            }
        }
        return $data;
    }
    
    public function getCompanyEmployeesByCompanyId($companyId) {
        set_time_limit(0);
        $company = Company::find($companyId);
        $employees = json_decode($company->employees, true);
        $related_users = array();
        if(is_array($employees)){
            foreach($employees as $id => $type) {
                $user = User::find($id);
                if(is_null($user->companies) || $user->companies === '0' || $user->companies === '"{}"' || $user->companies === '{}') continue;
                $related_users[] = $user;
            }
        }
        return $related_users;
    }
    
    public function getCompanyTotalTopicsReleased($companies) {
        if(is_null($companies) || count($companies) == 0){
            return [];
        }
        foreach($companies as $company) {
            $topics[$company->id] = $company->getTotalEmployeeTopicCount($company->id);
        }
        return $topics;
    }
    
    public function getCompanyTotalTopicReplies($companies) {
        if(is_null($companies) || count($companies) == 0){
            return [];
        }
        foreach($companies as $company) {
            $replies[$company->id] = $company->getTotalTopicReplies($company->id);
        }
        return $replies;
    }
}