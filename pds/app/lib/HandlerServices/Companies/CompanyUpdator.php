<?php
namespace HandlerServices\Companies;

use Validator;
use Auth;
use Company;
use User;
use Input;

class CompanyUpdator {
    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
    }
    
    public function updateEmployeeApprovalStatus($id, $company_id, $employeeType) {
        $user = User::find($id);
        $companies = json_decode($user->companies);
        foreach($companies as $pharmacy_id => $employee_type){
            if($pharmacy_id == $company_id && $employeeType != 0) {
                $companies->$pharmacy_id = (string)$employeeType;
            }
            if($employeeType == 0) {
                unset($companies->$pharmacy_id);
            } 
        }
        $user->companies = json_encode($companies);
        $user->employee_type = $employeeType;
        $user->account_type = 1;
        $user->pharmacy = \Company::find($company_id)->name;
        if($user->save()) {
            return $this->listener->successfullyApprovedEmployee($user->pharmacy, $id, $company_id, $employeeType);
        }
        return Redirect::back()->with('Failure', 'The User couldn\'t be saved, please contact support');
        
    }
    
    public function updateEmployeeToAdmin($employeeId, $companyId) {
        $newAdminUpdate = $this->updateNewAdminAndCompanyRecord($companyId, $employeeId);
        if($newAdminUpdate == 'failure') {
            return $this->listener->failedToUpdateEmployeeToAdmin();
        }
        
        return $this->listener->successfullySwappedAdminRoles();
    }
    
    public function updateNewAdminAndCompanyRecord($companyId, $employeeId) {
        $newAdmin = User::find($employeeId);
        $companies = json_decode($newAdmin->companies);
        foreach($companies as $pharmacyId => $employeeType) {
            if($pharmacyId == $companyId && $employeeType != 'pending') {
                $companies->$pharmacyId = '1';
                $newAdmin->companies = json_encode($companies);
                $newAdmin->save();
                $company = Company::find($companyId);
                $company->owner_name = $newAdmin->fname.' '.$newAdmin->lname;
                $company->owner_email = $newAdmin->email;
                $company->admin_id = $newAdmin->id;
                return $company->save();
            }
        }
        return 'failure';
    }
   
}