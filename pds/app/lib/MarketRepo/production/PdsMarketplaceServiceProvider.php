<?php

namespace MarketRepo\production;

use Illuminate\Support\ServiceProvider;

class PdsMarketplaceServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'MarketRepo\production\Vendors\PdsVendorRepository',
			'MarketRepo\production\Vendors\PdsEloquentVendorRepository'
		);
	}
}