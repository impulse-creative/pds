<?php

namespace MarketRepo\production\Vendors;

use Vendor;

class PdsEloquentVendorRepository extends \BaseController implements PdsVendorRepository {

    /**
     * Retrieve all vendors from the temporary table
     *
     * @author Derek J. Foster
     */
    public function all() {
        return \Vendor::all();

        //For when we push the production version of the marketplace live
        //return \Vendor::all();
    }

    public function getUpdateRules() {
        return array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'company_name' => 'required'
        );
    }

    /**
     * Retrieve all categories in an associative array
     *
     * @author Derek J. Foster
     * @key integer id
     * @value string category_name
     */
    public function getCategories() {
        //get all vendor categories
        $categories = \MarketCategories::all();
        $categoriesArray = array();
        foreach ($categories as $category) {
            $categoriesArray[$category->id] = $category->category_name;
        }

        return $categoriesArray;
    }

    public function getAllByCategory($category, $subcategory = null) {
        /* start changes for production here */

        $secret_cat = \Input::get('secret_category');
        if (isset($secret_cat)) {
            $results = \Vendor::where('category', $secret_cat)->where('subCategory', $subcategory)->get();
        } else {
            $results = \Vendor::where('category', $category)->orWhere('subCategory', 'LIKE', '%' . $category . '%')->get();
        }
        return $results;
    }

    /**
     * Retrieve all subcategories in an associative array of key/value pairs
     *
     * @author Derek J. Foster
     * @key integer id
     * @value string category_name
     */
    public function getSubCategories() {
        $subcat_regex = '%1.%';
        $subCategories = \MarketSubCategories::where('id', 'LIKE', $subcat_regex)->orderBy('id', 'ASC')->get();
        $subCategoriesArray = array();
        foreach ($subCategories as $subCategory) {
            $subCategoriesArray[$subCategory->id] = $subCategory->name;
        }

        return $subCategoriesArray;
    }

    public function allPaginated() {
        return \Vendor::all()->paginated(15);

        //For when we push the production version of the marketplace live
        //return \Vendor::all()->paginated(15);
    }

    public function getByCompanyId($vendor_id) {
        return \Vendor::find($vendor_id);
    }

    public function getByOwnerId($owner_id) {
        return \Vendor::where('user_id', $owner_id)->first();
    }

    public function getCompaniesByLetter($letter) {
        $vendor = \Vendor::where('vendor', 'REGEXP', '^[' . $letter . ']')->get();
        return $vendor;

        //For when we push the production version of the marketplace live
        //$vendor = \Vendor::where('vendor', 'REGEXP', '^['.$letter.']')->get();
    }

    public function createVendor($vendor_data) {
        return \Vendor::create($vendor_data);
    }
    
    public function updateVendorAccount() {
        $user_id = Input::get('user_id');
        $vendor_id = Input::get('vendor_id');
        $vendor = Vendor::find($vendor_id);
        
        //get hubspot company/acc id
        $user_hs_num = $this->user->find_hs_acc_num($user_id);

        $vendor->user_id = $user_id;
        //$vendor->hs_acc_number = User::getUserHSAccountId(Input::get('email'));
        $vendor->hs_acc_number = $user_hs_num;
        $vendor->first_name = \Input::get('first_name');
        $vendor->last_name = \Input::get('last_name');
        $vendor->email = \Input::get('email');
        $vendor->phone = \Input::get('phone');
        $vendor->company_name = \Input::get('company_name');
        $vendor->company_website = !preg_match("~^(?:f|ht)tps?://~i", \Input::get('company_website')) ? "http://" . \Input::get('company_website') : \Input::get('company_website');
        $vendor->address = \Input::get('address');
        $storeConditional = $this->storeConditionalData($vendor);
        if ($vendor->save() && $storeConditional == 'success') {
            return \Redirect::to('dashboard/accounts/summary/' . $user_id)->with('Success', 'Successfully updated the marketplace listing for: ' . $vendor->company_name . '.');
        } else {
            return \Redirect::to('dashboard/accounts/summary/' . $user_id)->with('Failure', 'Failed updated the marketplace listing for: ' . $vendor->company_name . '.');
        }
    }
    
    public function storeConditionalData($vendor) {
        $description = Input::get('description');
        if (\Input::get('city')) $vendor->city = Input::get('city');
        if (\Input::get('state_or_region')) $vendor->state_or_region = Input::get('state_or_region');
        if (\Input::get('postal_code')) $vendor->postal_code = Input::get('postal_code');
        if (\Input::file('company_logo')) {
            $local_path = self::storeCompanyLogo();
            $vendor->company_logo = $local_path;
        }
        if (\Input::get('description')) $vendor->description = isset($description) ? $_POST['html_edits'] : 'No description';
        if (\Input::get('company_blog_or_vlog')) $vendor->company_blog_or_vlog = Input::get('company_blog_or_vlog');
        
        return 'success';
    }
    
    public function storeCompanyLogo() {
        //attempt to retrieve served file from client
        if ($_FILES['company_logo']["size"][0] !== 0) {
            //intialize variables
            $data = \Input::file(); //array of files
            $numOfFiles = count($data['company_logo']); //number of files in array with index 'file'
            //remove previous logo associated with the company (this is in case of a logo update)
            foreach (glob(public_path() . "/storage/files/company_logos/" . \Auth::id() . "_*.*") as $filename) {
                //deletes the previous files
                unlink($filename);
            }

            //set the file name prefixed with the user id (good for deletes, moves, etc.)
            $name = \Auth::id() . "_" . rand(0, 10000) . "_" . $data['company_logo']->getClientOriginalName();
            //find temp location when initially downloaded.
            $tmp_path = $data['company_logo']->getRealPath();
            //construct the store path of our server (this is where the file will be stored)
            $local_store_path = public_path() . '/storage/files/company_logos/' . $name;
            //store the file on the server 
            $local_path = $this->storeCompanyLogoLocally($name, $data['company_logo'], $local_store_path);

            return $name;
        } else {
            //send user back if file is non-existant
            return \Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    /**
     * FILE STORAGE METHODS
     * @author Derek Foster
     */
    //store the uploaded file locally
    public function storeCompanyLogoLocally($filename, $file, $local_path) {
        //move file from the tmp directory to '/storage/files', using public path as the root
        $file->move(public_path() . '/storage/files/company_logos', $filename);

        //return path the file was stored in
        return $local_path;
    }

    public function removeVendor($vendor_id, $force = false) {
        //For when we push the production version of the marketplace live
        //$vendor = \Vendor::findOrFail($vendor_id);
        $vendor = \Vendor::findOrFail($vendor_id);

        if ($force) {
            return $vendor->forceDelete();
        } else {
            return $vendor->delete();
        }
    }

}
