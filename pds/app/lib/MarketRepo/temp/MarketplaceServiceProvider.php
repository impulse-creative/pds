<?php

namespace MarketRepo\temp;

use Illuminate\Support\ServiceProvider;

class MarketplaceServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'MarketRepo\temp\Vendors\VendorRepository',
			'MarketRepo\temp\Vendors\EloquentVendorRepository'
		);
	}
}