<?php

namespace MarketRepo\temp\Vendors;

use Vendor;

class EloquentVendorRepository extends \BaseController implements VendorRepository {

	/**
	 * Retrieve all vendors from the temporary table
	 *
	 * @author Derek J. Foster
	 */
	public function all()
	{
		return \TmpVendor::orderBy('vendor', 'ASC')->get();

		//For when we push the production version of the marketplace live
		//return \Vendor::all();
	}

	/**
	 * Retrieve all alphebetical letters available for browsing so we aren't left with empty nav choices
	 *
	 * @author Derek J. Foster
	 */
	public function getAllAvailablePaginated()
	{
		$vendors = $this->all();
	    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
	    $final_letters = [];
	    foreach ($vendors as $key => $value) {
	      foreach ($letters as $letter) {
	        if($value->vendor[0] === $letter && !in_array($letter, $final_letters)) {
	          $final_letters[] = $letter;
	        }
	      }
	    }

	    return $final_letters;
	}

	/**
	 * Retrieve all categories in an associative array
	 *
	 * @author Derek J. Foster
	 * @key integer id
	 * @value string category_name
	 */
	public function getCategories()
	{
		//get all vendor categories
        $categories = \MarketCategories::all();
        $categoriesArray = array();
        foreach($categories as $category) {
            $categoriesArray[$category->id] = $category->category_name;
        }

        return $categoriesArray;
	}

	public function getAllByCategory($category, $subcategory = null)
	{
		/* start changes for production here */
		
		$secret_cat = \Input::get('secret_category');
		if(isset($secret_cat))
		{
			$results = \TmpVendor::where('category', $secret_cat)->where('subCategory', $subcategory)->get();
		}else{
			$results = \TmpVendor::where('category', $category)->orWhere('subcategory', $category)->get();

		}
		return $results;
	}

	/**
	 * Retrieve all subcategories in an associative array of key/value pairs
	 *
	 * @author Derek J. Foster
	 * @key integer id
	 * @value string category_name
	 */
	public function getSubCategories()
	{
		$subcat_regex = '%1.%';
		$subCategories = \MarketSubCategories::where('id', 'LIKE', $subcat_regex)->orderBy('id', 'ASC')->get();
        $subCategoriesArray = array();
        foreach($subCategories as $subCategory) {
            $subCategoriesArray[$subCategory->id] = $subCategory->name;
        }

        return $subCategoriesArray;
	}

	public function allPaginated()
	{
		return \TmpVendor::all()->paginated(15);

		//For when we push the production version of the marketplace live
		//return \Vendor::all()->paginated(15);
	}

	public function getByCompanyId($vendor_id)
	{
		return \TmpVendor::find($vendor_id);

		//For when we push the production version of the marketplace live
		//return \Vendor::findOrFail($vendor_id);
	}

	public function getByOwnerId($owner_id)
	{
		return \TmpVendor::where('user_id', $owner_id)->first();
	}

	public function getCompaniesByLetter($letter)
	{
		$vendor = \TmpVendor::where('vendor', 'REGEXP', '^['.$letter.']')->get();
		return $vendor;

		//For when we push the production version of the marketplace live
		//$vendor = \Vendor::where('vendor', 'REGEXP', '^['.$letter.']')->get();
	}

	public function createVendor($vendor_data)
	{
		return \TmpVendor::create($vendor_data);
		//For when we push the production version of the marketplace live
		//return \Vendor::create($vendor_data);
	}

	public function removeVendor($vendor_id, $force = false)
	{
		//For when we push the production version of the marketplace live
		//$vendor = \Vendor::findOrFail($vendor_id);
		$vendor = \TmpVendor::findOrFail($vendor_id);

		if ($force) {
		 	return $vendor->forceDelete();
		}else{
			return $vendor->delete();
		}
	}
        
        public function getOldCategories() {
            $vendors = \TmpVendor::orderBy('category', 'ASC')->get();
            $categories = [];
            foreach ($vendors as $vendor) {
                if(!in_array(trim($vendor->category), $categories)) {
                    $categories[] = trim($vendor->category);
                }
            }
            return $categories;
        }
}