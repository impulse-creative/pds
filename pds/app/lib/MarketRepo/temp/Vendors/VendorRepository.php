<?php

namespace MarketRepo\temp\Vendors;

interface VendorRepository {
	public function all();

	public function getByCompanyId($vendor_id);

	public function getByOwnerId($owner_id);

	public function getAllAvailablePaginated();

	public function getAllByCategory($category, $subcategory);

	public function getCategories();

	public function getSubCategories();

	public function createVendor($vendor_id);

	public function getCompaniesByLetter($letter);

	public function removeVendor($vendor_id, $force = false);
}