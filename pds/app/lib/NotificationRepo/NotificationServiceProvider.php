<?php

namespace NotificationRepo;

use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'NotificationRepo\Notifications\NotificationRepository',
			'NotificationRepo\Notifications\EloquentNotificationRepository'
		);
	}
}