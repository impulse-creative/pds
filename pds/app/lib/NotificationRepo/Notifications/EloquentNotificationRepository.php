<?php

namespace NotificationRepo\Notifications;

use Notifications;
use Carbon\Carbon as Carbon;

class EloquentNotificationRepository extends \BaseController implements NotificationRepository {
    
    public function getDigest($id) {
        $notifications = \Notifications::where('user_id', $id)->first();
        return $notifications->digest;
    }

    public function getResources($id) {
        $notifications = \Notifications::where('user_id', $id)->first();
        return json_decode($notifications->resources);
    }

    public function getEvents($id) {
        $notifications = \Notifications::where('user_id', $id)->first();
        return json_decode($notifications->events);
    }
    
    public function getAllNotifications($id) {
        $notifications = \Notifications::where('user_id', $id)->first();
        if(is_object($notifications)) {
            return $this->verifyNotificationsExist($notifications);
        }
        return null;
    }
    
    public function verifyNotificationsExist($notifications) {
        $allNotifications = [];
        $allNotifications['topics'] = $this->checkActiveTopics(json_decode($notifications->topics));
        $allNotifications['resources'] = $this->checkActiveResources(json_decode($notifications->resources));
        return $allNotifications;
    }
    
    public function checkActiveTopics($notifications) {
        $activeNotifications = new \stdClass();
        $activeTopics = new \stdClass();
        if(is_object($notifications)) {
            foreach($notifications as $id => $notification) {
                if($notification->show != 0) {
                    $active = \ForumTopics::with('lastReplied')->where('id', $id)->first();
                    if($active != null) {
                        $activeNotifications->{$id} = $notification; //to save the notification because it exists
                        
                        $myLastTimeUpdating = $notification->date;
                        $topicLastUpdated = $active->updated_at;

                        if($myLastTimeUpdating < $topicLastUpdated ) {
                            $activeTopics->{$id} = $active;
                        }
                    }
                }
            }
        }
        $update = $this->updateMessageNotifications($activeNotifications);
        if($update == false) {
            //dd($activeNotifications); //this is an error
        }
        return $activeTopics;
    }
    
    public function checkActiveResources($notifications) {
        $activeNotifications = new \stdClass();
        $activeResources = new \stdClass();
        if(is_object($notifications)) {
            foreach($notifications as $id => $notification) {
                if($notification->show != 0) {
                    $active = \Resources::find($id);
                    if($active != null) {
                        $activeNotifications->{$id} = $notification;
                        $myLastTimeUpdating = $notification->date;
                        $resourceLastUpdated = $active->updated_at;
                        if($myLastTimeUpdating < $resourceLastUpdated) {
                            $activeResources->{$id} = $active;
                        }
                    }
                }
            }
        }
        $update = $this->updateResourceNotifications($activeNotifications);
        if($update == false) {
            //dd($activeNotifications); // errors
        }
        return $activeResources;
    }
    
    public function updateResourceNotifications($notifications) {
        $id = \Auth::id();
        $data = \Notifications::where('user_id', $id)->first();
        $data->resources = json_encode($notifications);
        if($data->save()) {
            return true;
        }
        return false;
    }
    
    
    public function updateMessageNotifications($notifications) {
        $id = \Auth::id();
        $data = \Notifications::where('user_id', $id)->first();
        $data->topics = json_encode($notifications);
        if($data->save()) {
            return true;
        }
        return false;
    }
    
    public function hideNotification($data) {
        $userId = \Auth::id();
        $type = $data['type'];
        $target = $data['id'];
        $notifications = $this->getUserNotifications($userId, $type);
        $notifications->{$target}->show = 0;
        $save = $this->saveAwesomeNotifications($userId, $notifications, $type);
        if($save == 'saved') {
            $message = '<div class="alert alert-success alert-dismissible" role="alert">';
            $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            $message .= '<strong>Notification Hidden!</strong></div>';
        } else {
            $message = '<div class="alert alert-success alert-dismissible" role="alert">';
            $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            $message .= '<strong>There was an error removing your notification, please contact support if this continues!</strong></div>';
        }
        return $message; 
    
    }

    public static function getUserNotifications($id, $type = 'topics') {
        $notifications = \Notifications::where('user_id', $id)->first();
        if(is_object($notifications)) {
            $returnObject = $notifications->{$type} != null ? json_decode($notifications->{$type}) : new \stdClass();
            return $returnObject;
        }
        return false;
    }
    
    public function getNotificationsForDashboard($id, $type='topics') {
        $results = self::getUserNotifications($id, $type);
        $filtered = self::getNotificationMessages($results, $type);
        return $filtered;
    }
    
    public function checkIfUserHasNotifications($id) {
        $record = \Notifications::where('user_id', $id)->first();
        return $record;
    }
    
    public function getUserSubscriptionPreferences($id) {
        $record = \Notifications::where('user_id', $id)->first();
        if(is_object($record)) {
            return $record->digest;
        } 
        return $this->createNotificationRow($id);
        
    }   
    
    public function createNotificationRow($id) {
        $new = new Notifications();
        $new->user_id = $id;
        $new->digest = 1;
        $new->topics = '';
        $new->resources = '';
        $new->events = '';
        if($new->save()) {
            return $new->digest;
        }
        return 0;
    }
    
    public function setUserSubscriptionPreferences($id, $digest) {
        $record = \Notifications::where('user_id', $id)->first();
        if($record == null) {
            $record = new Notifications();
            $record->user_id = $id;
        }
        $record->digest = $digest;
        $save = $record->save();
        return $save;
    }
    
    public static function getNotificationMessages($notifications, $type) {
        $notifyMessage = '';
        if($notifications != null) {
            switch($type) {
                case 'topics':
                    $notifyMessage .= self::getFilteredForumNotifications($notifications);
                    break;
                case 'resources':
                    $notifyMessage .= self::getFilteredResourceNotifications($notifications);
                    break;
            }            
        }
        return $notifyMessage;
    }
    
    public static function getFilteredForumNotifications($notifications) {
        $notifyMessage = '';
        foreach($notifications as $topicId => $meta) {
            if($meta->show != 0) {
                $topic = \ForumTopics::find($topicId);
                if($topic != null || $topic != '') {
                    $newMessage = isset($meta->id) ? $meta->id : '1';
                    $topicUpdated = Carbon::createFromTimestamp(strtotime($topic->updated_at));
                    $userUpdated = Carbon::createFromTimestamp(strtotime($meta->date));
                    $notifyMessage .= $topicUpdated->between($userUpdated, Carbon::now(), false) ? self::getNotificationMessage($newMessage, $topicUpdated, $topic, 'replied to', 'topics') : '';
                }
                
            }
        }
        return $notifyMessage;
    }
    
    public static function getFilteredResourceNotifications($notifications) {
        $notifyMessage = '';
        foreach($notifications as $resourceId => $meta) {
            if($meta->show != 0) {
                $file = \Resources::find($resourceId);
                if($file != null || $file != '') {
                    $fileUpdated = Carbon::createFromTimestamp(strtotime($file->updated_at));
                    $userUpdated = Carbon::createFromTimestamp(strtotime($meta->date));
                    $notifyMessage .= $fileUpdated->between($userUpdated, Carbon::now(), false) ? self::getNotificationMessage($fileUpdated, $file, 'updated', 'resources') : '';
                }
            }
        }
        return $notifyMessage;
    }
    
    public function getLastAuthor($topicId) {
        $latestReply = \ForumMessages::where('parent_topic', $topicId)->first();
        if (is_object($latestReply)) {
            $lastRepliedAuthor = \User::find($latestReply->author_id);
            return $lastRepliedAuthor->fname.' '.$lastRepliedAuthor->lname;
        }
        return 'Mr. Nobody';
        
    }
    
    public static function getLastReplyUser($topicId) {
        $topic = \ForumTopics::find($topicId);
        
        //dd($topic->last_replied);
        $user = \User::find($topic->last_replied);
        
        if($user == null) {
            $user = \User::find(1);
        }
        return $user;
    }
    
    public static function getBioImage($user) {
        if(!is_object($user)) {
            $url = 'https://i0.wp.com/assets1.uvcdn.com/pkg/admin/icons/user_70-c68d06098b40646a91b7656094632c19.png?ssl=1';
            $html = '<div class="user-image"><img src="'.$url.'" /></div>';
        } else {
            $html = \User::getBioImage($user->id, '');
        }
        return $html;
    }


    public static function getNotificationMessage($newMessage, $time, $notification, $didWhat, $type='topics', $ownerId = false) {
        if(property_exists($notification, 'id')) {
            $var = self::$resource($type, $notification, $ownerId);
            $difference = $time->diffForHumans(Carbon::now());
            $message = '<div style="background: #D9EDF7; padding:10px" class="alert alert-info text-left notification-alert">';
            $message .= '<button style="display:none" type="button" class="close hide-notification" ';
            $message .= 'data-target-id="'.$var['id'].'" data-type="'.$type.'" ';
            $message .= 'data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            $message .= self::getBioImage($var['author']);
            $message .= '<span style="vertical-align:top; margin: 6px 0 0 10px;display:inline-block;" class="notification-fix">'.$var['author']['fname'].' '.$var['author']['lname'].' '.$didWhat.' <a class="orange-text" href="'.\URL::to($var['url']).'?category=0#'.$newMessage.'" target="_blank">'.ucwords($var['title']).'</a></span> <span class="pull-right text-right time" style="display:none">'.$difference.'</span>';
            if(strpos($message, 'before') > -1) {
                $message = str_replace('before', 'ago', $message);
            }
            $message .= '</div>';
        } else {
            $message = '<div class="hidden" style="font-size: 18px; font-weight: 800;">Please report to the support team by using the ? icon in the bottom right corner and include a screenshot!';
            ob_start();
            var_dump($notification);
            $message .= '<div>'.ob_get_clean().'</div></div>';
        }
        
        return $message;
    }
    
    public static function getVariablesForNotifications($type, $notification, $ownerId) {
        switch($type) {
            case 'topics':
                //dd($notification);
                $var['author'] = self::getLastReplyUser($id);
                $var['title'] = $notification->title;
                $tempTitle = preg_replace('/[^a-z\d\s]+/i', '', $notification->title);
                $var['url'] = "/message-boards".preg_replace('/ /', '-', $tempTitle)."/$id";
                break;
            case 'resources':
                $var['author'] = \User::find($notification->author);
                $var['title'] = $notification->filename;
                $var['url'] = $notification->resource_share_link;
                break;
        }
        $var['id'] = $notification->id;
        return $var;
    }
    
    public function addForumNotification($userId, $topicId, $msg_id) {
        if($this->checkIfDuplicateNotification($userId, $topicId['parent_topic'], $msg_id, 'topics')) {
            return 'Your notifications have been updated.';
        }
        return 'thank you.';
    } 
    
    public function makeSubscribeAchievements($type)
    {
        switch($type) {
            case 'resources':
                $id = 4;
                break;
            case 'topics':
                $id = 5;
                break;
            default:
                return;
        }
        $grantAchievement = \UserAchievement::addAchievement(\Auth::id(), $id);
    }
    
    public function createNewNotificationRecord($id, $topicId, $column, $msg_id) {
        $date = date("Y-m-d H:i:s");
        $userNotification = new \Notifications();
        $userNotification->user_id = $id;
        $data = '{ "'.$topicId.'" : { "date" : "'.$date.'", "show" : "1", "id" : "'.$msg_id.'" } }';
        $userNotification->{$column} = $data;
        if($userNotification->save()) {
            return 'saved';    //saved
        }
        return 'failed';
        
    }

    public function updateForumNotification($id, $topicId, $currentNotifications) {
        //dd($currentNotifications);
    }
    
    public function addNewAwesomeNotification($id, $topicId, $msg_id, $type ='topics') {
        $date = date("Y-m-d H:i:s");
        $current = self::getUserNotifications($id, $type);
        $current->{$topicId} = json_decode('{ "date": "'.$date.'", "show" : "1", "id" : "'.$msg_id.'" }');
        $saved = $this->saveAwesomeNotifications($id, $current, $type);
        
        //return $saved;
    }
    
    public static function saveAwesomeNotifications($id, $notifications, $type='topics') {
        $usersNotifications = \Notifications::where('user_id', $id)->first();
        $usersNotifications->{$type} = json_encode($notifications);
        if($usersNotifications->save()) {
            return 'saved';    //saved
        }
        return 'failed';
    }
    
    
    public function addNewForumNotification($id, $topicId, $type) {
        $date = date("Y-m-d H:i:s");
        $current = $this->getForumNotifications($id);
        $current->{$topicId} = json_decode('{ "date": "'.$date.'", "show" : "1" }');
        $saved = $this->saveTopics($id, $current);
        return $saved;
    }
    
    public static function getTopics($id) {
        $notifications = \Notifications::where('user_id', $id)->first();
        if(is_object($notifications)) {
            return json_decode($notifications->topics);
        }
        return false;
    }
    
    public static function saveTopics($id, $topics) {
        $usersNotifications = \Notifications::where('user_id', $id)->first();
        $usersNotifications->topics = json_encode($topics);
        if($usersNotifications->save()) {
            return 'saved';    //saved
        }
        return 'failed';
    }
    
    
    public function addNotification($data) {
        $userId = \Auth::id();
        $type = $data['type'];
        $grantAchievement = $this->makeSubscribeAchievements($type); //resources , topics
        $notifyId = $data['id'];
        $save = $this->checkIfDuplicateNotification($userId, $notifyId, $type);
        $message = '<div class="alert alert-success alert-dismissible" role="alert">';
        $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $message .= '<strong>Subscribed!!</strong></div>';
        return $message; 
    }
    
    public function checkIfDuplicateNotification($id, $targetId, $msg_id = '', $type = 'topics') {
        $currentNotifications = $this->getUserNotifications($id, $type);
        if($currentNotifications == false) {
            if(is_object($this->checkIfUserHasNotifications($id))) {
                $this->addNewAwesomeNotification($id, $targetId, $type, $msg_id);
                return true;
            }
            
            $new = $this->createNewNotificationRecord($id, $targetId, $msg_id, $type); 
            $message = '<div class="alert alert-success alert-dismissible" role="alert">';
            $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            $message .= '<strong>Subscribed!!</strong></div>';
            return $message;
        }
        
        foreach($currentNotifications as $notification => $value) {
            if($notification == $targetId) {
                $this->addNewAwesomeNotification($id, $targetId, $msg_id, $type);
                return true;
            }
        }
        if($this->addNewAwesomeNotification($id, $targetId, $msg_id, $type) == 'saved') {
            return true;
        }
        return false;
    }    
    
    public function oldhideNotification($data) {
        $userId = \Auth::id();
        $type = $data['type'];
        $target = $data['id'];
        $notifications = $this->getUserNotifications($userId, $type);
        $tempNotification = json_encode($notifications->{$target});
        $fixedNotification = str_replace('"show":"1"', '"show":"0"', $tempNotification);
        $notifications->{$target} = json_decode($fixedNotification);
        $save = $this->saveAwesomeNotifications($userId, $notifications, $type);
        $message = '<div class="alert alert-success alert-dismissible" role="alert">';
        $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $message .= '<strong>Notification Hidden!</strong></div>';
        return $message; 
    }

            
}

