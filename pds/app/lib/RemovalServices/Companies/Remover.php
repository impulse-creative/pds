<?php
namespace RemovalServices\Companies;

use Validator;
use Auth;
use Company;
use User;
use Input;
use CompanyApprovals;

class CompanyRemoval {
    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
    }
    
    public function resetUserCompany($user_id) {
        $user = User::find($user_id);
        $user->companies = '0';
        $user->save();
        
        return $this->listener->successfulUserCompanyReset();
    }
}