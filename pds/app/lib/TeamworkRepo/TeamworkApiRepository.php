<?php

/**
 * Title: Service provider for pds teamwork
 * Date : 12/30/2014
 * Description: This Service Provider allows pds to access all of the data on the 3rd party software 'Teamwork'
 * 
 * @author: Derek J. Foster
 */

namespace TeamworkRepo;

use \Carbon\Carbon;
use API;
use Redirect;
use Auth;
use User;
use HandlerServices\Companies\CompanyReader;
use Company;
use DB;
use Validation;
use TeamworkEmployees;
use TeamworkCompanies;

class TeamworkApi {

    private $api_key = 'cut656tree';
    public $tw_company = 'pharmacyowners';
    private $impulse_tw_id = '45584';
    public $response_data = array();

    protected $listener;
    
    public function __construct($listener) {
        $this->listener = $listener;
        $this->newReader = new CompanyReader($this);
    }
    
    public function processTeamworkRegistrationRequest($pharmacy_id){
        //check if user already has an account created on Teamwork, the record on teamwork_companies is only created when the user creates a teamwork company
        $reader = new CompanyReader($this);
        $user_companies = $reader->getAllUserCompanies(Auth::id());
        $company_record = TeamworkCompanies::whereIn('company_id', $user_companies)->get();
        
        //if user has company account, they are already finished so send them to the projects index with custom welcome back message
        if(count($company_record) > 0) {
            return $this->listener->companyIsAlreadyRegistered($pharmacy_id);
        } else {
            //if user did not have an account listed in our db, then we create them one
            //and store the response data in their new company record
            $new_tw_company = $this->createTeamworkCompany();
            //if there was an error creating the account, display error message with Teamwork error response.
            if(isset($new_tw_company->STATUS) && $new_tw_company->STATUS === "Error") {
                return $this->listener->teamworkFailureResponse($pharmacy_id, $new_tw_company);
            }
            return $this->listener->createUserAndBootstrapTasks($new_tw_company, $pharmacy_id);
        }
    }
    
    public function bootstrapUserAndTasks($tw_company_id, $pharmacy_id) {
        //create new teamwork user with the company id
        $new_tw_company_admin = $this->createNewTeamworkOwnerUser($tw_company_id);
        
        if(is_object($new_tw_company_admin) && isset($new_tw_company_admin->STATUS)) {
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[Teamwork Failure New Owner]: Teamwork response: '.$new_tw_company_admin->MESSAGE);
        }
        
        $starting_projects = $this->createNewTwBootstrapProject($tw_company_id);
        
        if(is_object($starting_projects) && isset($starting_projects->MESSAGE)) {
            return Redirect::route('get.project.management', ['id' => Auth::id()])->with('Failure', '[Teamwork Failure New Project]: Teamwork response: '.$starting_projects->MESSAGE);
        }
        //create bootstrap task lists on each project in response array
        if(is_array($starting_projects)) {
            return $created_tasklist = $this->createBootstrapTwTasklist($starting_projects);
        }
        
        throw new \Exception('Invalid array: Recieved invalid response array for bootstrapping user and tasks. [TeamworkApiRepository]');
    }
    
    public function createNewTeamworkOwnerUser($tw_company_id)
    {
        //action to create new user
        $action = 'people';
        $data = $this->getNewTeamworkOwnerInput($tw_company_id);
        
        $created_user = $this->newTwPostRequest($action, $data);
        
        if (isset($created_user->STATUS) && $created_user->STATUS === "Error") {
            $pre_existing_user = $this->getPreExistingUser();
        } else {
            $pre_existing_user = $created_user['tw_item_id']->id;
        }
        
        $user = User::find(Auth::id());
        $user->project_name = preg_replace('/-/', '/ /', Auth::user()->email);
        $user->project_password = Auth::id().'_'.Auth::user()->hs_acc_number.'_tw_pass';
        $user->job_title = User::getUserJobTitleFromHS($user->email);
        $user->teamwork_company_id = $tw_company_id;
        if ($user->save()) {
            //if saved success, we return the tw company id again to create the bootstrap project 'Getting Started'
            return $this->setUserTeamworkUserIdThenReturnTwCompanyId($pre_existing_user, $tw_company_id);
        }
        
        throw new \Exception('[NEW TW USER ERROR]: Could not save the new user credentials for '.Auth::user()->fname.'\'s ('.Auth::user()->email.') auto login functionality.');
    }
    
    public function setUserTeamworkUserIdThenReturnTwCompanyId($pre_existing_user, $tw_company_id){
            $reader = new CompanyReader($this);
            $registered_company_list = $reader->getAllUserCompanies(Auth::id());
            foreach($registered_company_list as $registered_company){
                $company = Company::where('id', $registered_company)->first();
                $company->tw_company_id = $tw_company_id;
                $company->save();
                $tw_company = TeamworkCompanies::where('company_id', $registered_company)->first();
                $tw_company->teamwork_user_id = $pre_existing_user;
                $tw_company->save();
            }
            return $tw_company_id;
    }
    
    public static function cleanTwOwnerUsername($name) {
        $scrubbed_username = preg_replace('/[^a-z\d\s]+/i', '', Auth::user()->pharmacy);
        return $polished_username = preg_replace('/ /', '-', $scrubbed_username);
    }
    
    public function getNewTeamworkOwnerInput($tw_company_id)
    {
        return array(
            'person' => array(
                'first-name' => Auth::user()->fname,
                'last-name' => Auth::user()->lname,
                'email-address' => Auth::user()->email,
                'user-name' => preg_replace('/-/', '/ /', Auth::user()->email),
                'password' => Auth::id().'_'.Auth::user()->hs_acc_number.'_tw_pass',
                'company-id' => $tw_company_id,
                'autoGiveProjectAccess' => 'yes',
                'canAddProjects' => 'no',
                'administrator' => 'no',
                'sendWelcomeEmail' => 'no',
                'welcomeEmailMessage' => 'You are now added to the project! Welcome to myPDS.',
                'title' => is_null(Auth::user()->job_title) ? 'No Title Found' : Auth::user()->job_title
            )  
        );
    }
    
    public function getPreExistingUser($email = null) {
        $user_email = is_null($email) ? Auth::user()->email : $email;
        $action = 'people';
        $employees = $this->getPeopleInTeamwork($action);
        foreach($employees['people'] as $person) {
            if($person['email-address'] === $user_email){
                $id = $person['id'];
            }
        }
        return $id;
    }
    
    public function getPeopleInTeamwork($action){
        $tw_response = $this->teamworkGetRequest($action);
        $headers = explode("\r\n", $tw_response);
        return $this->getReadableResponseForGetRequests($headers);
    }
    
    public function getReadableResponseForGetRequests($headers){
        return json_decode(end($headers), true);
    }
    
    public function createBootstrapTwTasklist($project_id_list)
    {
        foreach($project_id_list as $project_id) {
            $new_tw_tasklist = $this->createNewTwTasklist($project_id);

            if(isset($new_tw_tasklist->MESSAGE) && $new_tw_tasklist->STATUS === "Error") {
                return $this->listener->failedToCreateNewTasklist($new_tw_tasklist);
            }
        }

        return $this->sendWelcomeMessage($project_id_list);
    }
    
    public function createNewTwTasklist($project_id)
    {
        $action = 'projects/'.$project_id.'/todo_lists';
        $data = $this->getBootstrapTwTasklistParams($project_id);

        $created_tasklist = $this->newTwPostRequest($action, $data);
        if (isset($created_tasklist->STATUS) && $created_tasklist->STATUS === "Error") {
            return $created_tasklist;
        }

        if (isset($created_tasklist['tw_item_id']) && isset($created_tasklist['tw_item_id']->id)) {
            return $created_tasklist['tw_item_id']->id;
        }

        throw new Exception('Invalid object [$created_tasklist->id]: line 1120');
    }
    
    public function getBootstrapTwTasklistParams($project_id) {
        $teamwork_company = TeamworkCompanies::where('teamwork_project_ids', ':'.$project_id)->first();
        return array(
            'todo-list' => array(
                'name' => $teamwork_company->pharmacy_name,
                'private' => 'false',
                'pinned' => 'false',
                'tracked' => 'true', 
                'description' => 'Company project for: '.$teamwork_company->pharmacy_name,
                'todo-list-template-id' => '395403' //might take this out at some point
            )
        );
    }
    
    public function sendWelcomeMessage($project_id_list) {
        $new_message_ids = array();
        foreach($project_id_list as $project_id){
            $action = 'projects/'.$project_id.'/posts';
            $data = array(
                'post' => array(
                    'title' => 'Welcome to Teamwork! [IC]',
                    'body' => 'Get started with teamwork today. We welcome you to the front door of success'
                )
            );
            $created_message = $this->newTwPostRequest($action, $data);
            if (isset($created_message->STATUS) && $created_message->STATUS === "Error") {
                return $this->listener->failedToCreateNewMessage($created_message);
            }   
            $new_message_ids[] = $created_message['tw_item_id'];
        }
        
        if (count($new_message_ids) > 0) {
            return $this->listener->successfulBootstrapProject();
        }
   
        throw new \Exception('Invalid object => [created_message]. Method: sendWelcomeMessage');
    }
    
    public function createNewTwBootstrapProject($tw_company_id)
    {
        $action = 'projects';
        $reader = new CompanyReader($this);
        $registered_company_id_list = $reader->getAllUserCompanies(Auth::id());
        $project_id_list = array();
        foreach ($registered_company_id_list as $registered_company_id){
            $data = $this->getBootstrapTwProjectParams($tw_company_id, $registered_company_id);

            $created_project = $this->newTwPostRequest($action, $data);
            if (isset($created_project->MESSAGE) && $created_project->STATUS === "Error") {
                return $created_project;
            }
            if (isset($created_project['tw_item_id']->id)) {
                //add project ID to project_ids
                $addCoachAndIS = $this->addCoachAndIsToProject($created_project['tw_item_id']->id);
                $tw_company = TeamworkCompanies::where('company_id', $registered_company_id)->first();
                $tw_company->teamwork_project_ids = ':'.$created_project['tw_item_id']->id;
                if($tw_company->save()) {
                    $project_id_list[] = $created_project['tw_item_id']->id;
                }
            }   
        }
        if(count($project_id_list) > 0) {
            return $project_id_list;
        }
        
        throw new \Exception('Could not store the new company project id.');
    }
    
    public function addCoachAndIsToProject($project_id) {
        $coach_id = Auth::user()->my_coach;
        $is_id = Auth::user()->my_is;
        if($coach_id === '' && $is_id === '') {
            \Log::error('There was an error with one of the coach and IS accounts being retrieved', ['TeamworkApiRepostory Response' => 'No coach or IS on record']);
        }
        $input = ['coach' => $coach_id, 'IS' => $is_id, 'owner' => Auth::id()];
        $coachAndISTwAccounts = $this->getCoachAndIsTwAccounts($input);
        if(!isset($coachAndISTwAccounts['IS']) || !isset($coachAndISTwAccounts['coach'])) {
            \Log::error('There was an error with one of the coach and IS accounts being retrieved', ['Teamwork Response' => $coachAndISTwAccounts]);
        }
        foreach ($coachAndISTwAccounts as $key => $account) {
            $action = "projects/".$project_id."/people/".$account['id'];
            $add_to_project = $this->curlPostRequestUrlOnly($action);
            if(isset($add_to_project->STATUS)) {
                \Log::error('Unuccessfully added to project', ['Teamwork Response' => $add_to_project->MESSAGE.' '.$account]);
            }
        }
        
        \Log::info('Successfully reached the end of adding coach and IS to project', ['Response' => 'Crazy stuff, hope it worked']);
        return 'success';
    }
    
    public function addEmployeeToProjectIfAccountExists($tw_company_id, $employee) {
        $teamwork_company = TeamworkCompanies::where('teamwork_company_id', $tw_company_id)->where('company_id', $employee->first())->get();
        $action = "projects/".$project_id."/people/".$account['id'];
        $add_to_project = $this->curlPostRequestUrlOnly($action);
        if(isset($add_to_project->STATUS)) {
            \Log::error('Unuccessfully added to project', ['Teamwork Response' => $add_to_project->MESSAGE.' '.$account]);
        }
    }
    
    public function getCoachAndIsTwAccounts($coachAndISArray) {
        $accounts = array();
        foreach($coachAndISArray as $key => $id) {
            if($key === 'coach' && $id != '') {
                $user = User::where('coach_id', $id)->first();
                $action = "people";
                $tw_users = $this->getPeopleInTeamwork($action);
                foreach($tw_users['people'] as $person){
                    if($person['email-address'] === $user->email){
                        $accounts['coach'] = $person;
                    }
                }
            }
            if($key === 'IS' && $id != '') {
                $user = User::where('is_id', $id)->first();
                $action = "people";
                $tw_users = $this->getPeopleInTeamwork($action);
                foreach($tw_users['people'] as $person){
                    if($person['email-address'] === $user->email){
                        $accounts['IS'] = $person;
                    }
                }
            }
            if($key === 'owner') {
                $user = User::find($id);
                $action = "people";
                $tw_users = $this->getPeopleInTeamwork($action);
                foreach($tw_users['people'] as $person){
                    if($person['email-address'] === $user->email){
                        $accounts['owner'] = $person;
                        $user->teamwork_user_id = $person['id'];
                        $user->save();
                    }
                }   
            }
        }
        return $accounts;
    }
    
    public function getBootstrapTwProjectParams($company_id, $registered_company_id){
        $company = Company::find($registered_company_id);
        return array(
           'project' => array(
               'name' => $company->name.' [IC]',
               'description' => $company->name.'\'s project.',
               'companyId' => $company_id
           )
        );
    }
    
    public function createTeamworkCompany() {
        $action = 'companies';
        $user = User::find(Auth::id());
        $data = array(
            "company" => array(
                'name' => $user->fname.' '.$user->lname.' [IC]'
            )
        );
        $created_company = $this->newTwPostRequest($action, $data);
        if (isset($created_company->MESSAGE) && $created_company->STATUS === "Error") {
            return $created_company;
        }
        if (isset($created_company['tw_item_id'])) {
            return $this->listener->addCompanyIdToTeamworkCompanyRecord($created_company['tw_item_id']->id, $user);
        }
        
        throw new \Exception('Could not create the company or store the new company instance id for Teamwork Company Registration.');
    }
    
    public function newTwPostRequest($action, $json){
        $data_string = json_encode($json);
        $tw_status = $this->getCurlResponseAfterCurlRequest($data_string, $action);
        if(isset($tw_status->MESSAGE) && $tw_status->STATUS === "Error") {
            return $tw_status;
        }
        $id_of_new_item = explode(':', $this->response_data['id']);
        $item_id = new \stdClass();
        $item_id->id = trim($id_of_new_item[1]);
        $response['tw_item_id'] = $item_id;
        $response['tw_status'] = $tw_status;
        return $response;
    }
    
    public function getCurlResponseAfterCurlRequest($data_string, $action) {
        $company_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json?pageSize=1000";
        $curl_response = $this->curlPostRequest($company_hook_url, $data_string);
        $headers = explode("\r\n", $curl_response);
        $this->response_data['status'] = $this->getReadableResponse($headers);
        if(preg_match('/id/', $this->response_data['status'])) {
            $this->response_data['id'] = $this->response_data['status'];
        }
        return $this->response_data['tw_status'] = json_decode($this->response_data['status']);
    }
    
    public function getReadableResponse($headers) {
        $id = 'id:0';
        $status = NULL;
        foreach($headers as $header) {
            if (strpos($header, 'id:') === 0) {
                return $id = $header;
            }
            if (strpos($header, '{"STATUS":') === 0) {
                return $status = $header;
            }
            if (strpos($header, '{"MESSAGE":') === 0) {
                return $status = $header;
            }
        }
    }
    
    public function curlPostRequest($url, $data) {
        try {
            $channel = curl_init();
            if (FALSE === $channel)
                   throw new Exception('failed to initialize');
            curl_setopt($channel, CURLOPT_HEADER, 1);
            curl_setopt($channel, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($channel, CURLOPT_URL, $url );
            curl_setopt($channel, CURLOPT_POSTFIELDS, $data);
            curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($channel, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data),
                "Authorization: BASIC ". base64_encode( $this->api_key .":xxx"))
            );
            $result = curl_exec($channel);
            if(FALSE === $result) throw new \Exception(curl_error($channel), curl_errno($channel));
            curl_close( $channel );
            return $result;
        } catch (\Exception $ex) {
            trigger_error(
                'Curl failed with error #'.
                $ex->getCode().': '.$ex->getMessage(),
                E_USER_ERROR);
        }
    }
    
    public function curlPostRequestUrlOnly($action) {
        try {
            $company_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
            $channel = curl_init();
            if (FALSE === $channel)
                   throw new Exception('failed to initialize');
            curl_setopt($channel, CURLOPT_HEADER, 1);
            curl_setopt($channel, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($channel, CURLOPT_URL, $company_hook_url );
            curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($channel, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($channel, CURLOPT_MAXREDIRS, 10);
            curl_setopt($channel, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                "Authorization: BASIC ". base64_encode( $this->api_key .":xxx"))
            );
            $result = curl_exec($channel);
            if(FALSE === $result) throw new \Exception(curl_error($channel), curl_errno($channel));
            curl_close( $channel );
            return $result;
        } catch (\Exception $ex) {
            trigger_error(
                'Curl failed with error #'.
                $ex->getCode().': '.$ex->getMessage(),
                E_USER_ERROR);
        }   
    }
    
    public function teamworkGetRequest($action){
        try {
            $company_hook_url = "http://". $this->tw_company .".teamwork.com/". $action .".json";
            $channel = curl_init();
            curl_setopt($channel, CURLOPT_HEADER, 1);
            curl_setopt($channel, CURLOPT_URL, $company_hook_url );
            curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($channel, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($channel, CURLOPT_MAXREDIRS, 10);
            curl_setopt($channel, CURLOPT_HTTPHEADER,
                array( "Authorization: BASIC ". base64_encode( $this->api_key .":xxx" ))
            );

            $results =  curl_exec ( $channel );
            curl_close ( $channel );

            return $results;   
        } catch (\Exception $ex) {
            trigger_error(
                'Curl failed with error #'.
                $ex->getCode().': '.$ex->getMessage(),
                E_USER_ERROR);
        }
    }

}
