<?php

namespace UserRepo\Storage;

use Illuminate\Support\ServiceProvider;

class ForumStorageServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'UserRepo\Storage\Forums\TopicRepository',
			'UserRepo\Storage\Forums\EloquentTopicRepository'
		);

		$this->app->bind(
			'UserRepo\Storage\Forums\CategoryRepository',
			'UserRepo\Storage\Forums\EloquentCategoryRepository'
		);

		$this->app->bind(
			'UserRepo\Storage\Forums\MessageRepository',
			'UserRepo\Storage\Forums\EloquentMessageRepository'
		);
	}
}