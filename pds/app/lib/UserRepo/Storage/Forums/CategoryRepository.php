<?php

namespace UserRepo\Storage\Forums;

interface CategoryRepository {
	public function all();

	public function allPaginated($per_page);

	public function findCategoryById($id);

	public function findCategoryByIdObject($id);

	public function getTopicsByParent($parent_id);

	public function getTopicsByParentForForum($parent_id);

	public function getByParentCategory($parent_id);

	public function createCategory($topic_data);

	public function removeCategory($topic_id, $force = false);
}