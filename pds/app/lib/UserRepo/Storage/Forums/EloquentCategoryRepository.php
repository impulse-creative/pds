<?php

namespace UserRepo\Storage\Forums;

use Forums;

class EloquentCategoryRepository extends \BaseController implements CategoryRepository {

    public function all() {
        return \ForumCategories::where('private', '!=', 1)->orderBy('title', 'ASC')->get();
    }
    
    public function free() {
        return \ForumCategories::where('min_access_level', 4)->where('private', '!=', 1)->orderBy('title', 'ASC')->get();
    }
    
    public function paid() {
        return \ForumCategories::whereNotIn('min_access_level', [3,4])->where('private', '!=', 1)->orderBy('title', 'ASC')->get();
    }
    
    public function secret() {
        if(\User::find(\Auth::id())->employee_type != 1) {
            return \ForumCategories::where('private', 1)->where('id', '!=', '17')->orderBy('title', 'ASC')->get();
        }
        return \ForumCategories::where('private', 1)->orderBy('title', 'ASC')->get();
    }
   
    
    public function allPaginated($per_page) {
        if (!is_numeric($per_page)) {
            throw new \InvalidArgumentException();
        }
        $permissions = new \GroupForm();
        if (\Auth::check()) {
            $user = \User::find(\Auth::id());
            $user_groups = explode(':', $user->groups);
            if ($permissions->isRemarkableAdmin(\Auth::id()) || in_array('1', $user_groups) || in_array('5', $user_groups) || in_array('6', $user_groups)) {
                $forum_data = \ForumCategories::paginate($per_page);
            } else {
                $forum_data = \ForumCategories::where('admin_only', '=', 0)->paginate($per_page);
            }
        } else {
            $forum_data = \ForumCategories::where('admin_only', '=', 0)->paginate($per_page);
        }
        return $forum_data;
    }
 
   public function getTopicsByParent($parent_id) {
        return \ForumTopics::where('parent_category', $parent_id)->get();
    }

    public function getTopicsByParentForForum($parent_id) {

        $recent_posts = \DB::table('forum_topics')
                ->select('forum_topics.id', 'users.fname', 'users.lname', 'forum_topics.title', 'forum_topics.id', 'forum_topics.featured')
                ->join('users', 'users.id', '=', 'forum_topics.author_id')
                ->where('forum_topics.parent_category', $parent_id)
                ->orderBy('forum_topics.created_at', 'DESC')
                ->get();

        return $recent_posts;
    }
    

    public function getAuthors() {
        /* WE USE THIS LOGIC IF WE NEED TO BRING IN THE FULL LIST OF AUTHORS AVAILABLE, LIKE IF WE NEED TO CHANGE THE AUTHOR OF AN EXISTING POST.*/
//        set_time_limit(0);
//        $authors_list = array();
//        \User::chunk(200, function($users) use (&$authors_list) {
//            foreach($users as $user){
//                $authors_list[] = $user;
//            }
//        });
//        $authors = array();
//        foreach ($authors_list as $author) {
//            $group_data = explode(':', $author->groups);
//            if (in_array('6', $group_data) || in_array('1', $group_data) || in_array('11', $group_data) || in_array('5', $group_data)) {
//                $authors[$author->id] = $author->fname . " " . $author->lname;
//            }
//        }
        $authors = [\Auth::id() => \Auth::user()->fname.' '.\Auth::user()->lname];
        return $authors;
    }

    public function findCategoryById($id) {
        return json_encode(\ForumCategories::find($id));
    }

    public function findCategoryByIdObject($id) {
        return \ForumCategories::find($id);
    }

    public function getByParentCategory($parent_id) {
        return \ForumCategories::where('parent_category', $parent_id)->get();
    }

    public function createCategory($category_input) {
        //return dd($category_input);
        $createNewCat = \ForumCategories::create($category_input);
        if ($createNewCat) {
            return true;
        } else {
            return false;
        }
    }

    public function removeCategory($category_id, $force = false) {
        $category = \ForumCategories::find($category_id);

        if ($category->delete()) {
            return \Redirect::route('forum.index')->with('Success', 'Successfully removed the category!');
        }

        return \Redirect::route('forum.index')->with('Failure', 'Failed to remove category from message boards. Please refresh and try again, if the problem persists, contact a web administrator.');
    }
    
    public function getAllCategories($type = 'all') {
        $categoryData = $this->determineCategoryType($type);
        $categoriesArray = array();
        $permissions = \User::getCurrentPermissions();
        $categoriesArray[0] = '-- Select a Category --';
        foreach ($categoryData as $category) {
            if (!in_array($category->min_access_level, explode(':', $permissions))) continue; 
            $categoriesArray[$category->id] = $category->title;
        }
        return $categoriesArray;
    }
    
    public function determineCategoryType($type) {
        switch($type) {
            case 'private':
                return $this->secret();
            case 'free':
                return $this->free();
            case 'paid':
                return $this->paid();
            case 'all':
            default:
                return $this->all();
        }
    }

}
