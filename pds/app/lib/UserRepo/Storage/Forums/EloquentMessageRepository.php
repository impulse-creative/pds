<?php

namespace UserRepo\Storage\Forums;

use \Carbon\Carbon;
use Forums;

class EloquentMessageRepository extends \BaseController implements MessageRepository {

    public function all() {
        return \ForumMessages::all();
    }

    public function findMessageById($id) {
        return \ForumMessages::find($id);
    }

    public function findMessagesByParent($parent_topic) {
        $messages = \ForumMessages::with('users', 'attachment')
                ->where('forum_messages.parent_topic', $parent_topic)
                ->orderBy('forum_messages.created_at', 'ASC')
                ->paginate(10);
        return $messages;
    }

    public function deleteFromParentTopic($parent_topic) {
        $messages = \ForumMessages::where('parent_topic', $parent_topic)->get();
        if (count($messages) == 0) {
            return 'success';
        }

        if (\ForumMessages::where('parent_topic', $parent_topic) != null) {
            $delete_messages = \ForumMessages::where('parent_topic', $parent_topic)->delete();
        } else {
            $delete_messages = 1;
        }


        if ($delete_messages) {
            return 'success';
        }

        return 'failure';
    }

    public function getCommentCount($topic_id) {
        $count = \ForumMessages::where('parent_topic', $topic_id)->count();

        return $count;
    }

    public function getByMessageAuthor($author_id) {
        $message_data = \ForumMessages::where('author_id', $author_id)->toArray();
        return $message_data;
    }

    public function createMessage($message_data) {
        $create_message = \ForumMessages::create($message_data);
        if ($create_message) {
            $topic = \ForumTopics::find($message_data['parent_topic']);
            $topic->last_replied = $message_data['author_id'];
            $topic->timestamps = false;
            $topic->save();
            return $create_message->id;
        }
        return false;
    }

    public function removeMessage($message_id, $force = false) {
        echo $force . "<br/>";
        return var_dump($message_id);
    }

}
