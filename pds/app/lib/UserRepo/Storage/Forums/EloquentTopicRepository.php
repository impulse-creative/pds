<?php

namespace UserRepo\Storage\Forums;

use Forums;

class EloquentTopicRepository implements TopicRepository {

    public function all() {
        return \ForumTopics::all();
    }

    public function findTopicById($id) {
        return \ForumTopics::find($id);
    }

    public static function updateLastAuthor($author, $topicId) {
        $topic = \ForumTopics::find($topicId);
        $topic->last_replied = $author;
        if ($topic->save()) {
            return true;
        }
        return false;
    }

    public function findTopicWithDetails($id) {

        return $results = \ForumTopics::with('users', 'attachment')->where('forum_topics.id', $id)
                ->orderBy('forum_topics.updated_at', 'DESC')
                ->first();
    }

    public function getByAuthor($author_id) {
        return \ForumTopics::where('author_id', $author_id)->toArray();
    }

    public function getTopicsByUser($author_id) {
        return \DB::table('forum_topics')
                        ->select('forum_topics.last_replied', 'forum_topics.created_at', 'forum_topics.id', 'forum_topics.description', 'forum_topics.updated_at', 'forum_topics.post_views', 'users.fname', 'users.lname', 'users.bio_image', 'forum_topics.title', 'forum_topics.id', 'forum_topics.featured', 'forum_topics.parent_category', 'forum_topics.author_id')
                        ->join('users', 'users.id', '=', 'forum_topics.author_id')
                        ->where('author_id', $author_id)
                        ->where('forum_topics.parent_category', '!=', '8')
                        ->where('is_pharmacy_private_post', 0)
                        ->orderBy('forum_topics.updated_at', 'DESC')
                        ->limit(10)
                        ->get();
    }

    public function getRepliesByUser($author_id) {
        $replies = \DB::table('forum_messages')
                ->select('forum_topics.last_replied', 'forum_messages.id', 'forum_messages.data', 'forum_topics.created_at', 'forum_messages.updated_at', 'forum_topics.created_at', 'forum_topics.title', 'forum_topics.id', 'users.fname', 'users.lname', 'users.bio_image', 'forum_messages.author_id', 'forum_messages.parent_topic')
                ->join('users', 'users.id', '=', 'forum_messages.author_id')
                ->join('forum_topics', 'forum_topics.id', '=', 'forum_messages.parent_topic')
                ->where('forum_messages.author_id', $author_id)
                ->where('forum_topics.parent_category', '!=', '8')
                ->where('forum_messages.parent_topic', '!=', '8')
                ->where('is_pharmacy_private_post', 0)
                ->orderBy('forum_messages.updated_at', 'DESC')
                ->limit(10)
                ->get();
        return $replies;
    }

    public static function getStaticPopularTopics($limit) {
        $recent_posts = \DB::table('forum_topics')
                ->select('forum_topics.last_replied', 'forum_topics.id', 'forum_topics.post_views', 'users.fname', 'users.lname', 'users.bio_image', 'forum_topics.title', 'forum_topics.id', 'forum_topics.featured', 'forum_topics.parent_category', 'forum_topics.author_id')
                ->join('users', 'users.id', '=', 'forum_topics.author_id')
                ->orderBy('forum_topics.post_views', 'DESC')
                ->where('is_pharmacy_private_post', 0)
                ->limit($limit)
                ->get();
        return $recent_posts;
    }

    public static function getRecentPopularTopics($limit) {
        $recent_posts = \DB::table('forum_topics')
                ->select('forum_topics.last_replied', 'forum_topics.id', 'forum_topics.post_views', 'users.fname', 'users.lname', 'users.bio_image', 'forum_topics.title', 'forum_topics.id', 'forum_topics.featured', 'forum_topics.parent_category', 'forum_topics.author_id')
                ->join('users', 'users.id', '=', 'forum_topics.author_id')
                ->orderBy('forum_topics.post_views', 'DESC')
                ->where('is_pharmacy_private_post', 0)
                ->limit($limit)
                ->get();
        return $recent_posts;
    }

    public function getRecentTopics($limit) {
        $recent_posts = \ForumTopics::whereHas('categories', function($q){
            $q->whereIn('min_access_level', explode(':', str_replace('::', ':', \User::getCurrentPermissions())));
        })->where('is_pharmacy_private_post', 0)->where('parent_category', '!=', '8')->where('parent_category', '!=', '17')->where('parent_category', '!=', '18')->with('users')->orderBy('updated_at', 'DESC')->limit($limit)->get();
        return $recent_posts;
    }

    public static function getStaticRecentTopics($limit) {
        $recent_posts = \DB::table('forum_topics')
                ->select('forum_topics.last_replied', 'forum_topics.created_at', 'forum_topics.updated_at', 'forum_topics.id', 'users.id', 'users.fname', 'users.lname', 'forum_topics.title', 'forum_topics.id', 'forum_topics.featured', 'forum_topics.parent_category', 'forum_topics.author_id')
                ->where('is_pharmacy_private_post', 0)
                ->join('users', 'users.id', '=', 'forum_topics.author_id')
                ->orderBy('forum_topics.updated_at', 'DESC')
                ->limit($limit)
                ->get();
        return $recent_posts;
    }

    public function getAllFeatured($id) {
        if ($id == 0) {
            $featuredPosts = \ForumTopics::with('users')->where('featured', 1)->where('is_pharmacy_private_post', '0')->orderBy('updated_at', 'DESC')->get();
        } else {
            $featuredPosts = \ForumTopics::with('users')->where('parent_category', $id)->where('featured', 1)->where('is_pharmacy_private_post', '0')->orderBy('updated_at', 'DESC')->get();
        }
        return $featuredPosts;
    }
    
    public function getAllForumTopics($categoryId = 0) {
        $categories = $this->getPermittedForumCategories();
        if (count($categories) > 0) {
            return $this->getForumTopicsInPermittedCategories($categories, $categoryId);
        }else {
            return null; //since user doesn't have access to any of the categories available
        }
    }
    
    
    
    public function getAllPrivateForumTopics($categoryId = null, $companies = null) {
        if($categoryId != null) {
            return \ForumTopics::with('users')->where('parent_category', $categoryId)->where('company', str_replace('"', '', $companies))->orderBy('updated_at', 'DESC')->paginate(15);
        }
        $user = \User::find(\Auth::id());
        if(json_decode($user->companies, true) == null) return null;
        if($user->employee_type == 1) {
            return \ForumTopics::with('users', 'categories')
                ->where('is_pharmacy_private_post', 1)
                ->whereIn('company', array_keys(json_decode($user->companies, true)))
                ->orderBy('updated_at', 'DESC')->paginate(15);
        } else {
            return \ForumTopics::with('users', 'categories')
            ->where('is_pharmacy_private_post', 1)
            ->where('parent_category', '!=', '17')
            ->where('company', array_keys(json_decode($user->companies, true)))
            ->orderBy('updated_at', 'DESC')->paginate(15);
        }
        
    }
    
    protected function getPermittedForumCategories(){
        $user = \User::find(\Auth::id());
        $user_groups = explode(':', $user->groups);
        return \ForumCategories::whereIN('min_access_level', $user_groups)->get();
    }
    
    protected function getForumTopicsInPermittedCategories($categories, $categoryId, $companies = false) {
        $category_id_list = $this->constructArrayOfAvailableCategories($categories);
        if ($categoryId == 0) {
            return \ForumTopics::with('users')
                    ->whereIN('parent_category', $category_id_list)
                    ->where('is_pharmacy_private_post', 0)
                    ->orderBy('updated_at', 'DESC')->paginate(15);
        } elseif( $companies != false) {
            $user = \User::find(\Auth::id());
            if($user->employee_type == 1) {
                return \ForumTopics::with('users', 'categories')
                    ->where('is_pharmacy_private_post', 1)
                    ->whereIn('company', array_keys(json_decode($user->companies, true)))
                    ->orderBy('updated_at', 'DESC')->paginate(15);
            } else {
                return \ForumTopics::with('users', 'categories')
                    ->where('is_pharmacy_private_post', 1)
                    ->where('parent_category', '!=', '17')
                    ->where('company', array_keys(json_decode($user->companies, true)))
                    ->orderBy('updated_at', 'DESC')->paginate(15);
            }
            /* THis was the previous verision */
//            return \ForumTopics::with('users')
//                    ->whereIN('parent_category', $category_id_list)
//                    ->where('company', $companies)
//                    ->orderBy('updated_at', 'DESC')->paginate(15);
        }
        return \ForumTopics::with('users')->
                where('parent_category', $categoryId)
                ->where('is_pharmacy_private_post', 0)
                ->orderBy('updated_at', 'DESC')->paginate(15);
    }

    
    protected function constructArrayOfAvailableCategories($categories) {
        foreach ($categories as $category){
            $category_id_list[] = $category->id;
        }
        
        return $category_id_list;
    }
    
    public function getPharmacyForumTopics() {
        $user = \User::find(\Auth::id());
        $sf_id = isset($user->sf_id) ? $user->sf_id : NULL;
        if (is_null($sf_id)) {
            return null;
        }else{
            return \ForumTopics::with('users')->where('is_pharmacy_private_post', 1)->paginate(15);
        }
    }

    public function getAllTopics($categoryId = 0) {
        $category_query = $this->setTopicQuery($categoryId);
        $permitted_categories = \DB::select($category_query);
        $category_list = $this->constructTopicCategoryArray($permitted_categories);
        return empty($permitted_categories) ? null : $this->getTopics($category_list, $categoryId);
    }
    
    protected function setTopicQuery($categoryId) {
        $user = \User::find(\Auth::id());
        $user_groups = explode(':', $user->groups);
        if (in_array('5', $user_groups) || in_array('1', $user_groups) || in_array('6', $user_groups)) {
            $where_clause = $categoryId == 0 ? '' : 'where id = '.$categoryId;
            return $cat_query = 'select * from forum_categories '.$where_clause;
        } else {
            $where_clause = $categoryId == 0 ? '' : 'and where id = '.$categoryId;
            return $cat_query = 'select * from forum_categories where admin_only = "0" '.$where_clause;
        }
    }
    
    protected function constructTopicCategoryArray($categories) {
        foreach ($categories as $key => $value) {
            $cat_list[] = $value->id;
        }
        
        return $cat_list;
    }
    
    protected function getTopics($cat_list, $categoryId) {
        if ($categoryId == 0) {
            $recent_posts = \ForumTopics::with('users')->whereIN('parent_category', $cat_list)->orderBy('updated_at', 'DESC')->paginate(15);
        } else {
            $recent_posts = \ForumTopics::with('users')->where('parent_category', $categoryId)->orderBy('updated_at', 'DESC')->paginate(15);
        }
        
        return $recent_posts;
    }

    public function getAllFeaturedTopics($param) {
        return \ForumTopics::with('users')->featured()->where('title', 'LIKE', '%' . $param . '%')->orderBy('updated_at', 'DESC')->get();
    }

    public function getSearchedTopics($search_key, $company = null) {
        $search_keys = explode(' ', $search_key);
        $search_results = $this->getSearchResults($search_keys, $company);
        return $search_results;
    }
    
    protected function getSearchResults($search_keys, $company) {
        $permitted_categories = self::getAccessibleCategories();
        $search_results = array();
        
        if($company == false) {
            $query = \ForumTopics::with('users', 'categories')
                ->whereIn('parent_category', $permitted_categories)
                ->where('is_pharmacy_private_post', 0)
                ->whereNested(function($query) use ($search_keys) {
                    foreach($search_keys as $key) {
                        $query->where('title', 'LIKE', "%$key%");
                        $query->orWhere('description', 'LIKE', "%$key%");
                    }
                })->orderBy('updated_at', 'DESC')->paginate(15);
        } else {
            if(\User::find(\Auth::id())->employee_type == 1) {
                $query = \ForumTopics::with('users', 'categories')
                    ->whereIn('parent_category', $permitted_categories)
                    ->where('is_pharmacy_private_post', 1)
                    ->whereNested(function($query) use ($search_keys) {
                        foreach($search_keys as $key) {
                            $query->where('title', 'LIKE', "%$key%");
                            $query->orWhere('description', 'LIKE', "%$key%");
                        }
                    })->orderBy('updated_at', 'DESC')->paginate(15);
            } else {
                $query = \ForumTopics::with('users', 'categories')
                ->whereIn('parent_category', $permitted_categories)
                ->where('is_pharmacy_private_post', 1)
                ->where('parent_category', '!=', '17')
                ->whereNested(function($query) use ($search_keys) {
                    foreach($search_keys as $key) {
                        $query->where('title', 'LIKE', "%$key%");
                        $query->orWhere('description', 'LIKE', "%$key%");
                    }
                })->orderBy('updated_at', 'DESC')->paginate(15);
            }
        }
        foreach($query as $topic) {
            $filter = $this->getSearchFilter($search_results);
            if($filter !== 'Empty' && array_key_exists($topic->id, $filter)) continue;
            $search_results[] = $topic;
        }
        
        return $search_results;
    }
    
    protected function getSearchFilter($search_results) {
        if(count($search_results) > 0) {
            $search_filter = array();
            foreach($search_results as $topic) {
               $search_filter[$topic->id] = $topic;
            }
            return $search_filter;
        }
        return 'Empty';
    }
    
    public function getAccessibleCategories() {
        $user_permissions = explode(':', \Auth::user()->groups);
        $permitted_categories = \ForumCategories::permitted($user_permissions)->get();
        $permitted_categories_id_list = array();
        foreach($permitted_categories as $category){
            $permitted_categories_id_list[] = $category->id;
        }
        return $permitted_categories_id_list;
    }

    public function searchThroughMessages($messages) {
        $parents = array();

        //store topic ids for topic query
        foreach ($messages as $key => $message) {
            $parents[] = $message->parent_topic;
        }

        //get topics from topic id array
        $topics = \ForumTopics::with('users')->whereIN('forum_topics.id', $parents)->paginate(15);

        if ($topics->count() > 0) {
            return $topics;
        }

        return 0;
    }

    public function createTopic($topic_data) {
        //Create new topic
        $createNewTopic = \ForumTopics::create($topic_data);

        //check response of create method
        if ($createNewTopic) {
            return 1;
        } else {
            return 0;
        }

        //all else fails we throw an exception
        throw new \Exception('Whoops! Something went wrong trying to create your topic. Please reset your cookies, refresh, and try again.');
    }

    public function updateNumViews($topic_id) {
        $post = \ForumTopics::find($topic_id);
        $post->post_views = $post->post_views + 1;
        $post->timestamps = false;
        if ($post->save()) {
            return 1;
        }

        return 0;
    }

    public function removeTopic($topic_id, $force = false) {
        echo $force . "<br/>";
        return var_dump($topic_id);
    }

}
