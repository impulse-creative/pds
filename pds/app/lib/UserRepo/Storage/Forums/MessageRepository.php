<?php

namespace UserRepo\Storage\Forums;

interface MessageRepository {
	public function all();

	public function findMessageById($id);

	public function findMessagesByParent($parent_topic);

	public function getCommentCount($topic_id);

	public function getByMessageAuthor($author_id);

	public function createMessage($message_data);

	public function removeMessage($message, $force = false);
}