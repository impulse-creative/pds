<?php

namespace UserRepo\Storage\Forums;

interface TopicRepository {
	public function all();

	public function findTopicById($id);

	public function findTopicWithDetails($id);

	public function getRecentTopics($limit);

	public function getAllTopics();

	public function getSearchedTopics($param);

	public function getByAuthor($author_id);

	public function createTopic($topic_data);

	public function updateNumViews($topic_id);

	public function removeTopic($topic_id, $force = false);
}