<?php

namespace UserRepo\Storage\User;
 
use User;
 
class EloquentUserRepository implements UserRepository {

    public function all()
    {
      return \User::all();
    }

    public function find($id)
    {
      return \User::find($id);
    }

    //get hubspot company/acc id
    public function find_hs_acc_num($id)
    {
      $user_hs_num = \User::select('hs_acc_number')->where('id', '=', $id)->first();
    	return $user_hs_num->hs_acc_number;
    }

    public function findByEmail($email)
    {
      return \User::where('email', '=', $email)->first();
    }

    public function create($input)
    {
        return \User::create($input);
    }

    public function getMenu()
    {
        $permittedMenu = 'dashboard.group-dash-views.menu';
        return $permittedMenu;
    }
    public function scrubUserName($name) {
        $scrubbed = preg_replace('/[^a-z\d\s]+/i', '', $name);
        $urlSafe = preg_replace('/ /', '-', $scrubbed);
        return $urlSafe;
    }
    
    public function getUsersWithSameSalesForceId($requester_id)
    {
        $requested_user = \User::find($requester_id);
        if(\Auth::check()){
            $users = \User::where('sf_id', $requested_user->sf_id)->get();
            
            $related_users = array();
            $related_users[-1] = '--Select a Recipient--';
            foreach ($users as $user) {
                if(is_null($user->teamwork_company_id) || $user->teamwork_company_id != $requested_user->teamwork_company_id) continue;
                $related_users[$user->teamwork_user_id] = $user->fname.' '.$user->lname;
            }
            
            return $related_users;
        }
        
        return Redirect::route('login.form')->with('Failure', 'Cannot access this method without logging in and acquiring the right permissions.');
    }
    
    public function getEmployeesInPharmacy($requester_sf_id) {
        return \User::with('teamwork')->where('sf_id', $requester_sf_id)->get();
    }

    public function updateUserProfile($assData, $newInfo) {
        //initialize User model object with targeted user
        $user = \User::findOrFail(\Auth::user()->id);
        //update profile information
        $user->fname = ucwords($newInfo['fname']);
        $user->lname = ucwords($newInfo['lname']);
        $user->pharmacy = ucwords($newInfo['pharmacy']);
        $user->email = $newInfo['email'];
        $user->bio = $newInfo['bio'];
        $user->city = isset($newInfo['city']) ? $newInfo['city'] : NULL;
        $user->state = isset($newInfo['state']) ? $newInfo['state'] : NULL;
        $user->website = $newInfo['website'];
        $user->project_password = isset($newInfo['project_password']) ? $newInfo['project_password'] : NULL;
        if (\Input::hasFile('bio_image')) {
            $filename = $this->storeProfilePhoto(preg_replace('/[^A-Za-z0-9 _ .-]/', '', $newInfo['fname'] . $newInfo['lname']), 'bio_image');
            $user->bio_image = $filename;
        }
        if($newInfo['changepassword'] == '1') {
            $user->password = \Hash::make($newInfo['password']);
        }
        $user->associated_data = $this->getAssociatedData($assData);
        if($user->save()) {
            return \Redirect::back()->with("Success", "Profile successfully updated.");
        }

        return \Redirect::back()->with("Failure", "Could not save your account information. Please check your input and try again.");
    }

    public static function storeProfilePhoto($filename, $inputName) {
        if ($_FILES[$inputName]["size"][0] !== 0) {
            $lazyPath = '/storage/files/profile_images/';
            $data = \Input::file();
            $tempFileName = $data[$inputName]->getRealPath();
            $newFileName = 'profile-' . \Auth::id() . '-' . rand(0, 10000) . "_" . $data[$inputName]->getClientOriginalName();
            $newPath = public_path() . $lazyPath;
            $saved = self::storePhotoLocally($tempFileName, $newPath . $newFileName);
            if ($saved) {
                return $newFileName;
            }
            return \Redirect::back()->with('No Files', 'Something Happened Man, And I have no clue why');
        } else {
            return \Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }

    public static function storePhotoLocally($tempFile, $newFile) {
        if (!\File::move($tempFile, $newFile)) {
            die('Upload Failed Miserably');
        }
        return true;
    }

    public function getAssociatedData($associated) {
        return json_encode($associated);
    }
}