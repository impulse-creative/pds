<?php

namespace UserRepo\Storage\User;

interface UserRepository {
	public function all();

	public function find($id);

	public function findByEmail($email);

	public function find_hs_acc_num($id);

	public function create($input);

	public function getMenu();
}