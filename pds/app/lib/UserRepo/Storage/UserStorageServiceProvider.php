<?php

namespace UserRepo\Storage;

use Illuminate\Support\ServiceProvider;

class UserStorageServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(
			'UserRepo\Storage\User\UserRepository',
			'UserRepo\Storage\User\EloquentUserRepository'
		);
	}
}