<?php

Form::macro("field", function($options)
{
	$markup = "";

	$type = "text";

	if(!empty($options["type"]))
	{
		$type = $options["type"];
	}

	if(empty($options["name"]))
	{
		return;
	}

	$name = $options["name"];

	$label = "";

	if(!empty($options["label"]))
	{
		$label = $options["label"];
	}

	$value = Input::old($name);

	if (!empty($options["value"]))
	{
		$value = Input::old($name, $options["value"]);
	}

	$placeholder = "";

	if(!empty($options["placeholder"]))
	{
		$placeholder = $options["placeholder"];
	}

	$class = "";

	if(!empty($options["class"]))
	{
		$class = " " . $options["class"];
	}

	$parameters = [
		"class" => "form-control" . $class,
		"placeholder" => $placeholder
	];

	$error = "";

	if(!empty($options["form"]))
	{
		$error = $options["form"]->getError($name);
	}

	if($type !== "hidden")
	{
		$markup .= "<div class='form-group'";
		$markup .= ($error ? " has-error" : "");
		$markup .= "'>";
	}

	switch($type)
	{
		case "text":
		{
			$markup .= Form::label($name, $label, [
				"class" => "control-label"
			]);

			$markup .= Form::text($name, $value, $parameters);
			break;
		}
		case "password":
		{
			$markup .= Form::label($name, $label, [
				"class" => "control-label"
			]);			

			$markup .= Form::password($name, $parameters);

			break;
		}
		case "checkbox":
		{
			$markup .= "<div class='checkbox'>";
			$markup .= "<label>";
			$markup .= Form::checkbox($name, 1, !!$value);
			$markup .= " " . $label;
			$markup .= "</label>";
			$markup .= "</div>";

			break;
		}
		case "hidden":
		{
			$markup .= Form::hidden($name, $value);
			break;
		}
	}

	if($error)
	{
		$markup .= "<span class='help-block'>";
		$markup .= $error;
		$markup .= "</span>";
	}	

	if($type !== "hidden")
	{
		$markup .= "</div>";
	}

	return $markup;
});

Form::macro("assets", function($section)
{
	$markup = "";
	$assets = Config::get("asset");
	if(isset($assets[$section]))
	{
		foreach ($assets[$section] as $key => $value) {
			$use = $value;

			if (is_string($key)) {
				$use = $key;
			}

			if (is_array($value)) {
				$result = "";
				foreach ($value as $key => $val) {
					$minify = file_get_contents($val);
					/* remove comments */
					$minify = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $minify );

					/* remove tabs, spaces, newlines, etc. */
					$minify = str_replace( array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $minify );

					$result .= $minify;
				}

				try{
				    file_put_contents($use, str_replace($result, $result, file_get_contents($use)));
				}catch(Exception $ex){
				    throw new Exception('File not putting itself where it needs to go:'.$ex);
				}

			}

			if (ends_with($use, ".css")) {
				$markup .= "<link 
					rel='stylesheet'
					type='text/css'
					href='". asset($use) ."'
				/>";
			}

			if (ends_with($use, ".js")) {
				$markup .= "<script
					src='" . asset($use) . "'
				></script>";
			}
		}
	}

	return $markup;
});

