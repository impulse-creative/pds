<?php

class AccessGroup extends Eloquent
{


    public function group()
    {
        return $this->hasMany('Group');
    }
    
    public static function getAccessGroup($id)
    {
        return AccessGroup::find($id);
    }
    public static function createAccessGroup($name, $description, $permissions) 
    {
        $accessGroup = new AccessGroup();
        $accessGroup->name = $name;
        $accessGroup->description = $description;
        $accessGroup->permissions = self::cleanUpPermissionsString($permissions);
        self::saveAccessGroup($accessGroup);
    }
    
    public static function cleanUpPermissionsString($string) {
        $tempArray = explode('-', $string);
        $permissions = '';
        foreach($tempArray as $key => $permission) {
            if($permission != '') {
                $permissions .= preg_replace('/ |\[|\]/', '', $permission).':';
            }
        }
        return trim($permissions, ':');
    }
    
    public static function saveAccessGroup($accessGroup) {
        $saved = $accessGroup->save() ? 'Access Group Added.' : 'Try Again, Something went wrong with your Access Group.';
        return $saved;
    }
}