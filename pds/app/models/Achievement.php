<?php

class Achievement extends Eloquent {
    
    protected $table = 'achievements';
    
    public function badge()
    {
        return $this->belongsToMany('Badge');
    }
    
    public static function getAchievement($id) 
    {
        return Achievement::find($id);
    }
    
    public static function getAchievementName($id) 
    {
        $achievement = Achievement::find($id);
        return $achievement->name;
    }
    
    public function getAchievements() 
    {
        return Achievement::find();
    }
    
    public static function getActiveAchievements() {
        $achievements = Achievement::where('active', '=', '1')->get();
        if($achievements != null) {
            return $achievements;
        }
        return 0;
    }
    
    public function giveUserAchievement($userId, $achievementId) 
    {
        return UserAchievement::addAchievement($userId, $achievementId);    
    }

    public static function saveAchievement($achievement) 
    {
        $saved = $achievement->save() ? 'Achievement Added.' : 'Try Again, Something went wrong';
        return $saved;
    }
    
    public static function createAchievement($name, $description) 
    {
        $achievement = new Achievement();
        $achievement->name = $name;
        $achievement->description = $description;
        return self::saveAchievement($achievement);
    }

    public static function editAchievement($id, $name, $description) 
    {
        $achievement = self::getAchievement($id);
        $achievement->name = $name;
        $achievement->description = $description;
        return self::saveAchievement($achievement);
    }
    
    
}
