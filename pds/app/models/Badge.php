<?php

class Badge extends Eloquent {
    
    protected $table = 'badges';
    protected $guarded = ['id'];
    
    public function achievement()
    {
        return $this->hasMany('Achievement');
    }
    
    public static function getBadge($id) 
    {
        return Badge::find($id);
    }
    
    public function getBadges() 
    {
        return Badge::all();
    }
    public static function getActiveBadges()
    {
        return Badge::where('active', '=', '1')->get();
    }
    
    public static function getActiveBadgesForIndex()
    {
        $badges = self::getActiveBadges();
        foreach($badges as $badge) {
            $badge->achievements = json_decode($badge->achievements);
        }
        return $badges;
    }
    
    public static function saveBadge($badge) {
        $saved = $badge->save() ? 'Badge Added.' : 'Try Again, Something went wrong with your Badge.';
        return $saved;
    }

    public static function createBadge($name, $description, $url, $achievements) 
    {
        $badge = new Badge();
        $badge->name = $name;
        $badge->description = $description;
        $badge->url = $url;
        $badge->achievements = self::verifyAchievements($achievements);
        $badge->locked = 0;
        $badge->active = 1;
        return self::saveBadge($badge);
    }
    
    public static function editBadge() 
    {
        $badge = self::getBadge($id);
        $badge->name = $name;
        $badge->description = $description;
        $badge->url = $url;
        $badge->achievements = self::verifyAchievements($achievements);
        $badge->locked = 0;
        $badge->active = 1;
        return self::saveBadge($badge);
    }
    
    public static function verifyAchievements($achievements) {
        if(is_object($achievements)) {
            return json_encode($achievements);
        }
        $fixedAchievements = strpos($achievements, '[') > -1 ? self::fixWebAchievements($achievements) : $achievements;
        return $fixedAchievements;
    }
    
    public static function fixWebAchievements($achievements) 
    {
        $tempArray = explode('-', $achievements);
        $neededAchievements = new stdClass();
        foreach($tempArray as $key => $achievement) {
            if($achievement != '') {
                $cleaned = self::getAchievementArray($achievement);
                $neededAchievements->{$cleaned[0]} = new stdClass();
                $neededAchievements->{$cleaned[0]}->times = $cleaned[1];
                $neededAchievements->{$cleaned[0]}->name = \Achievement::getAchievementName($cleaned[0]);
            }
        }
        return json_encode($neededAchievements);
    }
    
    public static function getAchievementArray($string)
    {
        $fixedString = preg_replace('/ |\[|\]/', '', $string);
        return explode(':', $fixedString);
    }
    
    public function getBadgeImageUrl($id) 
    {
        $badge = $this->getBadge($id);
        return $badge->url;
    }

    public function lockBadge($id) 
    {
        $badge = $this->getBadge($id);
        $badge->locked = 1;
    }

    public function deleteBadge($id) 
    {
        $badge = $this->getBadge($id);
        $badge->active = 0;
    }

    public static function addBadgeToUser($userId, $badgeId) 
    {
        $badge = self::getBadge($badgeId);
        $saved = User::addBadgeToUser($userId, $badge);
        //$notify = $this->notifyUser($badge);
        return $saved;
    }
    
    public function addBadgesToUser($userId, $badgeIds) 
    {
        $saved = '';
        foreach($badgeIds as $badgeId) {
            $saved .= $badgeId.': '.$this->addBadgeToUser($userId, $badgeId);
        }
        return $saved;
    }
    
    public static function storeBadge($filename, $inputName)
    {
        if($_FILES[$inputName]["size"][0] !== 0) {
            $lazyPath = '/storage/files/badge_images/';
            $data = Input::file(); 
            $tempFileName = $data[$inputName]->getRealPath();
            $newFileName = 'badge'.rand(0, 10000)."_".$data[$inputName]->getClientOriginalName();
            $newPath = public_path().$lazyPath;
            $saved = self::storeFileLocally($tempFileName, $newPath.$newFileName);
            if($saved) {
                return $newFileName;
            }
            return Redirect::back()->with('No Files', 'Something Happened Man, And I have no clue why');
        } else{
            return Redirect::back()->with('No Files', 'You have not provided any files for upload!');
        }
    }
    
    public static function storeFileLocally($tempFile, $newFile)
    {
        if(! File::move($tempFile, $newFile)) {
            die('Upload Failed Miserably');
        }   
        return true;
    }
    

    public function notifyUser($badge) 
    {
        
    }

    public function removeBadgeFromUser($id) 
    {

    }
    
    public static function checkIfUserReceivedBadge($achievements, $userId)
    {
        $currentAchievements = json_decode($achievements->completed);
        $badges = Badge::where('active', '1')->get();
        foreach($badges as $badge) {
            $count = 0;
            $neededAchievements = json_decode($badge->achievements);
            $totalAchievementsNeeded = count((array)$neededAchievements);
            foreach($neededAchievements as $key => $value) {
                
                if(!property_exists($currentAchievements, $key)) {
                    break;
                } 
                //$currentTimes = $currentAchievements->{$key};
                if($currentAchievements->{$key} >= $value->times) {
                    $count++;
                }
                
            }
            if($totalAchievementsNeeded == $count) {
                self::addBadgeToUser($userId, $badge->id); 
            }
        }
        
    }


    
}
