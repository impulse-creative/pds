<?php

class Company extends Eloquent {

    protected $table = "companies";
    protected $guarded = array();
    
    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'domain_name' => 'required',
        'email_from' => 'required',
        'address' => 'required',
        'city' => 'required',
        'state' => 'required',
        'postal_code' => 'required',
        'owner_name' => 'required',
        'owner_email' => 'required|unique:companies'
    ];
    
    public function owner() {
        return $this->hasOne('\User', 'admin_id', 'id');
    }
    
    public static function getCompanyDropdown() {
        $companies = User::find(Auth::id())->companies != '{}' ? json_decode(User::find(Auth::id())->companies) : null;
        if(!is_object($companies) && intval($companies) == 0) return null;
        foreach($companies as $id => $employeeType) {
            $company[$id] = Company::find($id)->name;
        }
        return $company;
    }
    
    public function searchCompanies($params = '') {
        /* Currently only searches for name and email */
        if($params['name'] == '' || $params['name'] == ' ') $params['name'] = 'asfdasdfasdf';
        set_time_limit(0);
        $names = explode(' ', $params['name']);
        $email = explode(' ', $params['owner_email']);
        $nameResults = $this->where(function ($nQuery) use ($names) {
            foreach($names as $name) {
                $nQuery->where('name', 'LIKE', "%$name%");
            }
        })->get();
        $emails= $this->where('owner_email', $email)->get();
        $results = $nameResults->merge($emails);
        return $results;
    }
    
    public static function belongsToCompany($id, $company) {
        $employees = json_decode(self::find($company)->employees, true);
        if(is_null($employees)) return 'no employees';
        foreach($employees as $employeeId => $employeeType) {
            if($employeeId == $id) return true;
        }
//        dd($employees);
        return false;
        
    }
    
    public function getTotalEmployeeTopicCount($companyId) {
        $employee_list = $this->getEmployeeIdList($companyId);
        $topic_count = count($employee_list) > 0 ? ForumTopics::whereIn('author_id', $employee_list)
                ->where('is_pharmacy_private_post', 0)->count() : 0;
        return $topic_count;
    }
    
    public function getTotalTopicReplies($companyId) {
        $employee_list = $this->getEmployeeIdList($companyId);
        $topic_reply_count = count($employee_list) > 0 ? ForumMessages::whereIn('author_id', $employee_list)
                ->where('company', 0)->count() : 0;
        return $topic_reply_count;
    }
    
    public function getEmployeeIdList($companyId){
        $employees = json_decode($this->find($companyId)->employees, true);
        if(is_null($employees)) return NULL;
        return array_keys($employees);
        /* $employee_list = array();
         *  code review
         * User::chunk(200, function($users) use (&$employee_list, &$companyId) {
         *   foreach($users as $user) {
         *       if(preg_match('/'.$companyId.'/', $user->companies)){
         *           $employee_list[] = $user->id;
         *       }
         *   }
         * });
         * return $employee_list; 
         */
    }
    
    public static function getAllCompanies($userId) {
        $user = User::find($userId);
//        dd($user);
        if(count(json_decode($user->companies, true)) == 0 || $user->companies === '0' || $user->companies === '{}' || $user->companies === '"{}"') return null;
        $companiesArray = json_decode($user->companies,true);
        $companies = self::whereIn('id', array_keys($companiesArray))->get();
        return $companies;
    }
    
    public static function updateMember($userId, $companyId, $employeeType, $import = false) {
        $company = self::find($companyId);
        $members = $company->employees === '0' || $company->employees === '{}'  ? $members = [] : json_decode($company->employees, true);
        $employee_type = $import == false ? $employeeType : "1";
        if($employee_type == 0) {
            unset($members[$userId]);
        } else {
            $members[$userId] = (string)$employee_type;
        }
        $company->employees = json_encode($members);
        if($company->save()) {
            return true;
        }
        return false;
    }
    
    
    
}
