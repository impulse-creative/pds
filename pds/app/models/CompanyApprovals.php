<?php

/**
 * CompanyApprovals lists each company or company employee that is awaiting approval 
 * from PDS upon registering the pharmacy or association of their account with the target's pharmacy
 *
 * @author Derek J. Foster | Impulse Creative
 */
class CompanyApprovals extends Eloquent {
    protected $table = "pending_company_approvals";
    protected $guarded = array();
    
    public static $rules = [
        'company_id' => 'required',
        'requesting_user' => 'required'
    ];
    
    public function user() {
        return $this->hasOne('User', 'requesting_user', 'id');
    }
    
    public function company() {
        return $this->hasOne('Company', 'company_id', 'id');
    }
}
