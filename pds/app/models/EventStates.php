<?php

class EventStates extends Eloquent {

        
    
	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	protected $table = 'state_list';

	/**
	 * Ungaurd variables for mass updates, insertions
	 */
	protected $guarded = [];
	public $timestamps = false;

        public function companies() {
            return $this->belongsToMany('Company', 'state', 'id');
        }
	
}