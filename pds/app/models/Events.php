<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class Events extends Eloquent {

        use SoftDeletingTrait,
            SearchableTrait;

        /**
         * Searchable rules.
         *
         * @var array
         */
        protected $searchable = [
            'columns' => [
                'description' => 10,
                'title' => 10,
                'who_should_attend' => 5,
                'how_does_it_work' => 5,
                'pricing_description' => 5
            ]
        ];
	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	protected $table = 'events';
	/**
	 * I set 'soft delete' to 'true', this means, if a file gets deleted it won't disappear immediately and we can
	 * rollback the delete if it was a mistake
	 */
	protected $softDelete = true;
        protected $dates = ['deleted_at'];

	/**
	 * Ungaurd variables for mass updates, insertions
	 */
	protected $guarded = [];


	public function type()
	{
            return $this->hasOne('EventTypes', 'id', 'event_type');
	}

	public function location()
	{
		return $this->hasOne('EventStates', 'id', 'state');
	}
        
        public static function getRequiredFields() 
        {
            $duplicate_event = Input::get('duplicate');
            $rules = [ 
                'title' => 'required',
                'event_description' => 'required',
                'event_types' => 'required|NotIn:0',
                'start_date' => 'required'
            ];
            $input = [
                'title' => trim(Input::get('title')),
                'event_description' => trim(Input::get('event_description')),
                'attending' => trim(Input::get('attending')),
                'how_it_works' => trim(Input::get('how_it_works')),
                'event_types' => Input::get('event_types'),
                'start_date' => Input::get('start_date'),
                'duplicate' => isset($duplicate_event) ? $duplicate_event : 'single'
                
            ];
            if(Input::get('is_free') == 1) {
                $rules['link_to_next_free'] = 'required';
                $input['link_to_next_free'] = Input::get('link_to_next_free');
            } else {
                $rules['price_description'] = 'required';
                $input['price_description'] = Input::get('price_description');
            }
            if(strlen(Input::get('core_price') > 0)) {
                $rules['per_core'] = 'required|NotIn:0';
                $rules['link_to_next_core'] = 'required';
                $input['per_core'] = Input::get('per_core');
                $input['link_to_next_core'] = Input::get('link_to_next_core');
            }
            if(strlen(Input::get('advanced_price') > 0)) {
                $rules['per_advanced'] = 'required|NotIn:0';
                $rules['link_to_next_advanced'] = 'required';
                $input['per_advanced'] = Input::get('per_advanced');
                $input['link_to_next_advanced'] = Input::get('link_to_next_advanced');
            }
            if(strlen(Input::get('elite_price') > 0)) {
                $rules['per_elite'] = 'required|NotIn:0';
                $rules['link_to_next_elite'] = 'required';
                $input['per_elite'] = Input::get('per_elite');
                $input['link_to_next_elite'] = Input::get('link_to_next_elite');
            }
            if(Input::get('event_types') == 4) {
                $rules['event_city'] = 'required';
                $rules['event_state'] = 'required|NotIn:0';
                $input['event_city'] = Input::get('event_city');
                $input['event_state'] = Input::get('event_state');
            }
            $params = [ 'rules' => $rules, 'input' => $input ];
            return $params;
        }

}