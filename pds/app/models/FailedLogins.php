<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class FailedLogins extends Eloquent implements UserInterface, RemindableInterface {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	protected $table = 'failed_logins';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	//array of throttle settings. # of failed responses => response
	protected static $default_throttle_settings = [
		10 => 1,			//delay in seconds
		20 => 2,			//delay in seconds
		50 => 3,			//delay in seconds
		150 => 'captcha',	//use robot catcher question
	];

	//time frame to use when retrieving number of failed login attempts from database
	protected static $table_name = 'failed_logins';
	protected static $time_frame_minutes = 15;
	protected static $custom_throttle_settings = array();
	protected static $response_array =	array(
			'status' => 'safe',
			'message' => null
	);

	/**
	 * Set custom timeframe
	 *
	 *@param integer $new_time_frame
	 */
	public function setTimeFrame($new_time_frame)
	{
		return self::$time_frame_minutes = $new_time_frame;
	}

	/**
	 * Set custom throttle settings 
	 *
	 * @param array $throttle_options
	 */
	protected function setThrottleOptions($throttle_options)
	{
		self::$custom_throttle_settings = $throttle_options;
	}

	/**
	 * Set the table name to your custom failed attempts table. Defaults to 'failed_logins'
	 * 
	 * @param string $table_name
	 */
	public function setTable($table)
	{
		self::$table_name = $table;
	}

	/**
	 * Retrieve the table name
	 * 
	 * @return string $table = <table name>
	 */	
	public function getTable()
	{
		return self::$table_name;
	}

	/**
	 * Create the failed attempts table, or you can just create the migration with artisan
	 *
	 * @param string $table_name
	 */
	private function createTable()
	{
		$table_name = self::getTable();

		Schema::dropIfExists($table_name);

		Schema::create($table_name, function($table)
		{
    		$table->increments('id');
    		$table->integer('user_id');
    		$table->string('user_email');
    		$table->string('user_ip');
    		$table->dateTime('attempted');
		});

		return Response::make($table_name.' was successfully created in the database.');
	}

	/**
	 * Retrieve client ip address of their machine
	 *
	 * Designed to retrieve ip if the original address is masked or public in terms of standard location
	 * @return string $ip
	 */
	protected function getIpAddress()
	{
		//retrieve client ip address
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip = $_SERVER['HTTP_CLIENT_IP']; 					//oldschool
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip = $_SERVER['HTTP_X_FORWARDED_FOR']; 			//if they have an ip mask or are in a hidden ip
		} elseif(!empty($_SERVER['REMOTE_ADDR'])) {
    		$ip = $_SERVER['REMOTE_ADDR']; 						//usual method
		}else{
			$ip = Request::getClientIp(true);
		}
		return $ip;
	}

	/**
	 * Add failed login attempt to the database
	 *
	 * @param integer $user_id
	 * @param string  $account_email
	 * @param string  $ip_address
	 * @return 		  response/redirect
	 */
	public function addFailedLoginAttempt($user_id, $account_email, $ip_address)
	{
		//catch if user id does not exist or is invalid
		if (!intval($user_id) || $user_id == 0) throw new Exception('User id is not valid');
		if (!$account_email) throw new Exception('[BruteForceProtection Exception]: Account email is non-existant.');
		//get current timestamp
		$current_time = date('Y-m-d H:i:s');
		$ip = self::getIpAddress();
		$table = self::getTable();
		$current_time = new DateTime();
		//attempt to insert failed login attempt
		$insert_failed_attempt = DB::table($table)->insert(array(
			array('user_id' => $user_id, 'user_email' => $account_email, 'user_ip' => $ip, 'attempted' => $current_time)
		));
		if($insert_failed_attempt) {
			return 1;
		}else {
			return Redirect::back()->with('Insert Failure', '[BruteForce Insertion Failure]: '.$e.'. Contact the web administrator.');
		}
	}


	public function getLoginStatus($options = null)
	{
		$fromDate = Carbon\Carbon::now();
	 	$tillDate = Carbon\Carbon::now()->subMinutes(self::$time_frame_minutes);
		$email = Input::get('email');
		$ip = self::getIpAddress();
		$whitelist_check = new Whitelists();
		$ip_pre_approved = $whitelist_check->checkWhitelistFrontDoor($email, $ip);

		switch ($ip_pre_approved['status']) {
			case 'whitelisted':
				$whitelist_approved = true;
				break;
			case 'error':
				$whitelist_approved = false;
				break;	
			default:
				throw new Exception('Could not recieve the whitelist status on the client IP address.');
				break;
		}

		if($whitelist_approved == true) {
			self::$response_array['status'] = 'whitelist_approved';
			self::$response_array['message'] = 'Client IP has been approved for entry';
			//var_dump(self::$response_array);
		} else {
			//continue with throttling
		}

		//retrieve latest failed login attempts
		$latest_failure = self::retrieveLatestAttempts();

		//get default throttle settings, update if options parameter is not null
		if(is_null($options))
		{
			$throttle_settings = self::$default_throttle_settings;
		}else {
			$throttle_settings = $options;
		}

		//retrieve first throttle limit by resetting the array's internal pointer to the first element
		reset($throttle_settings);
		//retrieve index element of the current array position
		$first_throttle_limit = key($throttle_settings);

		//retrieve all recently failed login attempts
		try {
			//get all failed attempts


			//select all recent failures
			$all_recent_failures = $this->selectRaw('attempted as date, COUNT(*) as count')
							     ->whereBetween( DB::raw('attempted'), [$tillDate, $fromDate] )
							     ->groupBy('date')
							     ->orderBy('date', 'DESC')
							     ->lists('count', 'date');
		    //var_dump($all_recent_failures);
			//reverse settings order for iteration
			krsort($throttle_settings);

			$recent_failure_count = count($all_recent_failures);

			if($recent_failure_count >= $first_throttle_limit){
				//it's apparent that the amount of login attempts are an issue, time to react
				foreach ($throttle_settings as $attempts => $delay) {
					if ($recent_failure_count > $attempts) {
						// we need to throttle based on delay
						if (is_numeric($delay)) {
							//find the time of the next allowed login
							$latest_failure_parse = Carbon\Carbon::parse($latest_failure);
							//echo "Carbon Parse: ".$latest_failure_parse."<br/>";
							$next_login_minimum_time = $latest_failure_parse->addSeconds($delay);
							//echo "Next login minimum time: ".$next_login_minimum_time."<br/>";
							//if the next allowed login time is in the future, calculate the remaining delay
							if(Carbon\Carbon::now() < $next_login_minimum_time){
								$remaining_delay = $delay;
								// add status to response array
								self::$response_array['status'] = 'delay';
								self::$response_array['message'] = 'Remaining minutes: '.$remaining_delay;
							}else{
								// delay has been passed, safe to login
								self::$response_array['status'] = 'safe';
							}
							$remaining_delay = $delay; //correct
							//echo 'You must wait ' . $remaining_delay . ' seconds before your next login attempt';
						} else {
							// add status to response array
							self::$response_array['status'] = 'captcha';
						}
						break;
					}
				}
			}

			//check if database connection is established
			$db_query_check = DB::connection()->getDatabaseName();
			if($db_query_check)
			{
				//empty all irrelevant (no longer recent) failure records
				try {
					//select all recent failures
					$deletePastFailures = DB::delete('delete from `failed_logins` where attempted between NOW() and ?', array($tillDate));
					//echo "<br/>here we are: <br/>";
					//var_dump($deletePastFailures);
				}catch(Exception $e) {
					//return error accordingly
					self::$response_array['status'] = 'error';
					self::$response_array['message'] = $e;
				}
			}

			return self::$response_array;
		} catch (Exception $e) {
			//return errors
			self::$response_array['status'] = 'error';
			self::$response_array['message'] = $e;
		}


		return self::$response_array;

	}

	/** 
	 * Retrieve the latest failed attempts from the failed logins table
	 *
	 * @return datetime $latest_failed_attempt || response $response_array
	 */
	protected function retrieveLatestAttempts()
	{
		//attempt to retrieve latest failed attempt
		try {
			$results = DB::table('failed_logins')->orderBy('attempted', 'desc')->first();
			//echo "Latest failed attempt: ".$results->attempted."<br/><br/>";
			//convert to date timestamp
			$latest_failed_attempt = $results->attempted;
			return $latest_failed_attempt;
		} catch (Exception $e) {
			self::$response_array['status'] = 'error';
			self::$response_array['message'] = $e;
			return self::$response_array;
		}
	}

}