<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class FileStorage extends Eloquent implements UserInterface, RemindableInterface {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */
	use UserTrait, RemindableTrait, SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	protected $table = 'resource_files';
	protected $primaryKey = 'id';
	protected $guarded = array();
	protected $dates = ['deleted_at'];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $softDelete = true;

	public function user()
	{
		return $this->hasOne('User');
	}

	/**
	 * FILE STORAGE METHODS
	 *@author Derek Foster
	 */
	//store the uploaded file locally
	public function storeFileLocally($filename, $tmp_path, $local_store_path, $file)
	{
		//initialize variables
		$author_id = Auth::id();
		$author_name = Auth::user()->fname." ".Auth::user()->lname;

		//$dropb_link = //insert link here, will implement once we get the chance to figure out how to integrate dropbox core api into laravel

		//store file meta data to the database for reference in the application
		self::storeFileData($author_id, $author_name, $filename, $tmp_path, $local_store_path);

		//move file from the tmp directory to '/storage/files', using public path as the root
		$file->move(public_path().'/storage/files', $filename);
	}

	//store file meta data to the database for reference in the application
	protected function storeFileData($author_id, $author_name, $originalFileName, $tmp_path, $local_store_path)
	{
		$newObj = new FileStorage;
		//check current section
		$typeGiven = Input::get('section');
		switch ($typeGiven) {
			case 'human_resources':
				$type = 1;
				break;
			case 'marketing':
				$type = 2;
				break;
			case 'tax_files':
				$type = 3;
				break;
			case 'spreadsheets':
				$type = 4;
				break;
			case 'webinars':
				$type = 5;
				break;
			case 'videos':
				$type = 6;
				break;
			default:
				$type = null;
				break;
		}
		if($type != null) {
			$attributes = array(
				'user_id' => $author_id,
				'author' => $author_name,
				'filename' => $originalFileName,
				'tmp_file_path' => $tmp_path,
				'drop_box_link' => $local_store_path,
				'resource_type_id' => $type,
				'original_filename' => $originalFileName
			);
			$newObj->create($attributes);
		}else {
			throw new Exception('File did not properly recieve a resource type, or the resource type does not exist within our list.');
		}
	}

	/**
	 * FILE RETREIVAL METHODS
	 * @author Derek Foster
	 */
	//show files pertaining to the current authenticated user
	public function showUsersFiles()
	{
		$id = Auth::id();
		$permissions = new GroupForm();

		if($permissions->isRemarkableAdmin($id))
		{
			return self::getUserFiles($id);
		}else{
			return Response::make("Insufficient permissions.");
		}
	}

	//list all resource files in the table
	public function showAllResourceFiles()
	{
		$id = Auth::id();
		$permissions = new GroupForm();

		if($permissions->isRemarkableAdmin($id))
		{
			return self::getAllFiles();
		}else{
			return Response::make("Insufficient permissions.");
		}
	}

	public function showFilesByType($type)
	{
		$id = Auth::id();
		$permissions = new GroupForm();

		if(!($permissions->isGeneralUser($id)))
		{
			return self::getFilesByType($type);
		}else{
			return Response::make("Insufficient permissions.");
		}
	}

	//gets all the files from the resource table
	protected function getAllFiles()
	{
		$id = Auth::id();
		$permissions = new GroupForm();

		if($permissions->isRemarkableAdmin($id))
		{
			$filesToShow = FileStorage::where('id', '>', 0)->get();
			return $filesToShow;
		}else{
			return Redirect::route('dash.main')->withErrors();
		}
	}

	//retrieve files specific to the current authenticated user
	protected function getUserFiles($id)
    {
        $filesToShow = FileStorage::where('user_id', '=', $id)->get();

        return json_encode($filesToShow);
    }

    protected function getFilesByType($type)
    {
    	$filesToShow = FileStorage::where('resource_type_id', '=', $type)->get();
    	return json_encode($filesToShow);
    }
}








