<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class FileTypes extends Eloquent implements UserInterface, RemindableInterface {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */
	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	protected $table = 'resource_types';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * These variables change the model's json form values to whatever you'd like
	 * Here, I've made sure to set the primary key to its correct value, it usually does this for us, but you never know
	 * I also set 'soft delete' to 'true', this means, if a file gets deleted it won't disappear immediately and we can
	 * rollback the delete if it was a mistake
	 */
	protected $primaryKey = 'id';
	protected $softDelete = true;

	public function resource_file()
	{
		return $this->hasMany('FileStorage');
	}

}