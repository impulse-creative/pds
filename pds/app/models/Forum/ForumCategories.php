<?php

class ForumCategories extends Eloquent {

    //specify table
    protected $table = 'forum_categories';
    protected $fillable = ['title', 'subtitle', 'admin_only', 'icon', 'min_access_level', 'private'];
    public $timestamps = false;
    public static $rules = [
            'title' => 'required',
            'subtitle' => 'required',
            'min_access_level' => 'required'
        ];

    //specifiy parent topic relationship
    public function topics() {
        return $this->hasMany('ForumTopics', 'parent_category', 'id');
    }

    public function scopeCategory($query, $type) {
        return $query->where('id', $type);
    }
    
    public function scopePermitted($query, $user_access){
        return $query->whereIN('min_access_level', $user_access);
    }

    public static function getRequiredInputAndValidationRules() {
        $rules = [
            'title' => 'required',
            'subtitle' => 'required'
        ];

        $input = [
            'title' => Input::get('title'),
            'subtitle' => Input::get('subtitle'),
            'icon' => Input::get('category_icon'),
            'admin_only' => isset($member_only_choice) ? $member_only_choice : 0
        ];

        //member only button
        $member_only_choice = Input::get('admin_only');

        //check to see if they clicked 'members only', if so, then require the minimum access level select 
        if (isset($member_only_choice)) {
            $rules['min_access_level'] = 'required';
            $input['min_access_level'] = Input::get('min_access_level');
        }

        $results = [
            'input' => $input,
            'rules' => $rules
        ];

        return $results;
    }
    
    
}
