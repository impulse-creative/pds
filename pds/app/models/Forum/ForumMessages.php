<?php

use \Carbon\Carbon;

class ForumMessages extends \Eloquent {
	
	//specify table
	protected $table = 'forum_messages';
	protected $fillable = ['data', 'author_id', 'parent_topic', 'company'];
        public static $rules = [
            'data' => 'required', 
            'author_id' => 'required', 
            'parent_topic' => 'required'
        ];

	public function getCreatedAtAttribute($date)
	{
	    return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('l, F j \\a\\t h:ia');
	}

	//specifiy parent topic relationship
	public function topics()
	{
		return $this->belongsTo('ForumTopics');
	}
        
        //specify relationship to author of message
        public function users()
        {
            return $this->hasOne('\User', 'id', 'author_id');
        }
        
        //specify relationship of attachment to message
        public function attachment() {
            return $this->hasOne('Resources', 'forum_message_id', 'attachment_id');
        }

	//specify user relationship
	public function author()
	{
            return $this->hasOne('\User');  
	}
	
}