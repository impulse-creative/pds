<?php
use Nicolaslopezj\Searchable\SearchableTrait;

class ForumTopics extends \Eloquent {

     use SearchableTrait;
     protected $searchable = [
        'columns' => [
            'description' => 10,
            'title' => 20,
        ]
    ];
     
    //specify table
    protected $table = 'forum_topics';
    protected $fillable = ['title', 'author_id', 'parent_category', 'description', 'is_pharmacy_private_post', 'company'];
    public static $rules = [
            'title' => 'required',
            'description' => 'required',
            'parent_category' => 'required|not_in:0'
        ];
    
    //specifiy user relationship
    
    public function messagesCountRelation() {
        return $this->hasOne('ForumMessages')->selectRaw('parent_topic, count(*) as count')->groupBy('parent_topic');
    }
    
    public function getMessagesCountAttribute() {
        return $this->messagesCountRelation ? $this->messagesCountRelation->count : 0;
    }
    
    public function lastReplied() {
        return $this->hasOne('\ForumMessages', 'author_id', 'last_replied');
    }
    
    public function lastRepliedMessage($id) {
        $lastMessage = ForumMessages::where('parent_topic', $id)->orderBy('created_at', 'DESC')->first();
        return $lastMessage;
        //return $this->hasOne('\ForumMessages', 'author_id', 'last_replied');
        /* Gaveup on this relation.  */
    }
    
    public function users() {
        return $this->hasOne('\User', 'id', 'author_id');
    }
    
    //specifiy sub-category relationships
    public function categories() {
        return $this->hasOne('\ForumCategories', 'id', 'parent_category');
    }
    
    //specify messages relationship
    public function messages() {
        return $this->hasMany('\ForumMessages', 'parent_topic', 'id');
    }
        
    //specify attachment relationship
    public function attachment() {
        return $this->hasOne('\Resources', 'topic_id', 'id');
    }

    //get all featured
    public function scopeFeatured($query) {
        return $query->where('featured', 1);
    }
    
    /* needs to be whereNested closure */
    public function scopeSearchTitleOrDescription($query, $search_key) {
        return $query->where('title', 'LIKE', '%'.$search_key.'%')
                ->orWhere('description', 'LIKE', '%'.$search_key.'%');
    }

}
