<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Group extends Eloquent
{
	use UserTrait, RemindableTrait;

	protected $table = "group";

	protected $softDelete = true;

	protected $guarded = [];

	/**
	 * Construct the one to many relationship between 'groups' and 'users' tables
	 */
	public function user() 
	{
		return $this->belongsToMany('User', 'groups', 'name');
	}

	/**
	 * Model Query Scopes
	 */
	public function scopeSynced($query)
	{
		return $query->where('hs_synced', '=', 1);
	}

	public function scopeResource($query)
	{
		return $query->where('is_resource_group', '=', 1);
	}

	public function scopePermissions($query)
	{
		return $query->where('is_resource_group', '=', 0);
	}
        public static function getGroupName($id) {
            $group = Group::where('id', $id)->first();
            if(is_object($group)) {
                return $group->description;
            }
            return '';
        }
        
        public static function getGroupNamesForUser($groups) {
            $names = '';
            if(!is_array($groups)) {
                $groups = explode(':', $groups);
            } 
            foreach($groups as $group) {
                $names .= '[ '.self::getGroupName($group).' ] ';
            }
            return trim($names);
        }

}