<?php

/**
 * Group Model/Controller
 * 
 */

class GroupForm extends BaseForm
{
	//add user to general group
	public static function addToGeneralGroup($id)
	{
		//finds the user by id, if not, returns control to calling method
		$user = User::findOrFail($id);

		$user->groups = "general";
		if ($user->save()) {
			//success
		}else{
			return Redirect::back()->with('Could not add to General', 'Could not add you to a group properly. Please contact the administrator if this happens again.');
		}
	}

	public static function addToVendorGroup($id)
	{
		//finds user by id, if not, returns failure
		$user = User::findOrFail($id);

		$user->groups = "vendor";
		if ($user->save()) {
			//success
		}else{
			return Redirect::back()->with('Could not add to Vendors', 'Could not add you to a group properly. Please contact the administrator if this happens again.');
		}
	}

	//checks to see if user has an account of level: general
	public function isGeneralUser($id)
	{
		if(Auth::user()->groups == "general") return 1;

		//if fail, return 0 to nullify access, since this is general, 
		// we may want to transfer them to some sort of error screen
		// because that shouldn't happen. The least permissions you can 
		// have is 'general'
		return 0;
	}

	//check to see if user has an account of level: vendor
	public function isVendor($id)
	{
		if(Auth::user()->groups == "vendor") return 1;

		//if fail, return 0 to nullify access
		return 0;
	}

	//check to see if user has an account of level: client
	public function isClient($id)
	{

		if(Auth::user()->groups == "client") return 1;

		//if fail, return 0 to nullify access
		return 0;
	}

	//checks to see if user has an account of level: admin
	public function isAdmin($id)
	{
		if(Auth::user()->groups == "admin") return 1;
			
		//if fail, return 0 to nullify access
		return 0;
	}

	//check to see if user has an account of level: impulse_admin
	public function isRemarkableAdmin($id)
	{
		$groups = explode(',', Auth::user()->groups);
		if(in_array("impulse_admin", $groups) || in_array('5', $groups)) return 1;
	
		//if fail, return 0 to nullify access
		return 0;
	}

	/**
	 * HELPER FUNCTIONS FOR RESOURCES
	 * 
	 * these are to determine if the resource(s) in question exist
	 * and if the user is able to perform a process on said resource(s).
	 */

	//checks to see if the name value is present to add it as a new group, 
	//because why would you try to add a group that is null, you crazy person
	public function isValidForAdd()
	{
		return $this->isValid([
				"name" => "required"
			]);
	}

	//checks to see if valid value for editing
	public function isValidForEdit()
	{
		return $this->isValid([
				"id" => "exists:group,id", //Be careful not to add a space after these words, the space will be processed in the query, causing simulations of anuerisms
				"name" => "required"
			]);
	}

	//checks to see if valid value for deletion
	public function isValidForDelete()
	{
		return $this->isValid([
				"id" => "exists:group,id"
			]);
	}
}