<?php

    class Importcompanies extends Eloquent
    {
        public static function getCompanyName($oldCompany) {
            $company = Importcompanies::where('old_id', 'like', '%'.trim($oldCompany).'%')->first();
            if($company == null) {
                return '';
            } else {
                return $company->old_company_name;
            }
        }
    }