<?php

class MarketCategories extends Eloquent {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $table = "categories";
	public $timestamps = false;

        public function vendor()
        {
            return $this->belongsToMany('Vendor', 'category', 'id');
        }
        
        public static function getCategoryArray()
        {
            $categories = self::all();
            $categoriesArray = array();
            $categoriesArray[0] = "--Please Select A Category--";
            foreach ($categories as $category) {
                $categoriesArray[$category->id] = $category->category_name;
            }
            return $categoriesArray;
        }
}
