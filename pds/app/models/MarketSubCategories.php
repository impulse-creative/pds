<?php

class MarketSubCategories extends Eloquent {
    /**
     * Eloquent will take the class name here and use the plural form as a 
     * reference to what the db table is that we're using in this model
     * So, in this case, we should have a table created called 'users'
     * so that this will work.
     */

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $table = "sub_categories";
    public $timestamps = false;

    public function vendor() {
        return $this->belongsToMany('Vendor', 'subcategory', 'id');
    }

    public static function getSubCategoryArray($categoryId = 1) 
    {
        $subCategories = self::where('id', 'LIKE', '%'.$categoryId.'.%')->orderBy('id', 'ASC')->get();
        $subCategoriesArray[0] = "--Please Select A SubCategory--";
        foreach ($subCategories as $subCategory) {
            $subCategoriesArray[$subCategory->id] = $subCategory->name;
        }
        return $subCategories;
    }

}
