<?php

class ResourceGroups extends Eloquent
{

	protected $table = "resource_groups";

	protected $guarded = [];


	public function group()
	{
            return $this->hasOne('Group', 'id', 'permitted_groups');
	}
}