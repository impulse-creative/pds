<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Resources extends Eloquent {
    
    
    /**
     * Eloquent will take the class name here and use the plural form as a 
     * reference to what the db table is that we're using in this model
     * So, in this case, we should have a table created called 'users'
     * so that this will work.
     */
    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     * We didn't have to set this because of Eloquent, but I'm just showing that we can
     * @var string
     */
    protected $table = 'resources';
    protected $primaryKey = 'id';
    protected $guarded = array();
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public static $unsupported_boxview_file_types = [
        'image_types' => [
            'jpg', 'jpeg', 'png'
        ],
        'video_types' => [
            'mov', 'mp4', 'mp3', 'wav', 'zip', 'link'
        ]
    ];
    
    public function user() {
        return $this->hasOne('User', 'id', 'author');
    }

    public function type() {
        return $this->hasOne('FileTypes', 'id', 'resource_type');
    }

    public function container() {
        return $this->hasOne('ResourceGroups', 'id', 'resource_container');
    }
    
    public function topic() {
        return $this->hasOne('ForumTopics', 'id', 'topic_id');
    }
    
    public function message() {
        return $this->hasOne('ForumMessages', 'attachment_id', 'forum_message_id');
    }
    
    public function scopeOfType($query, $type) {
        return $query->where('resource_type', $type);
    }
    
    public function scopeLibrary($query) {
        return $query->where('is_forum_attachement', 0);
    }
    
    public function scopeAttachments($query) {
        return $query->where('is_forum_attachement', 1);
    }
    
    public function scopeMessageAttachment($query, $id) {
        return $query->where('forum_message_id', $id);
    }
    
    public function scopeBoxviewRecords($query) {
        return $query->whereNotNull('boxview_id');
    }

}
