<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Role extends Eloquent implements UserInterface, RemindableInterface {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	//protected $table = 'roles';

	/**
	 * The attributes excluded from the model's JSON form.
	 * Not sure these will even come through this model, but you can never be too sure
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Set laravel timestamps to 'off'
	 */
	public $timestamps = false;

	/**
	 * Get users with a certain role
	 */
	public function usersWithRole($role)
	{
		return $this->belongsToMany('User', 'usr_roles');
	}

}