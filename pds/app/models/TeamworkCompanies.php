<?php
/**
 * User Profile Table Model
 * @author Nick Decker
 * 
 * First Release: v1.0
 */


class TeamworkCompanies extends Eloquent {
    public $api_key;
    public $pds_account_name;
    public $timestamps = false;
    /**
     * Set up model relationships
     */
    public function user()
    {
        return $this->hasOne('User', 'id', 'owner_id');
    }
    
    /**
     * Query scopes
     */
    public function scopeEmployees($query)
    {
        return $query->where('teamwork_company_id', Auth::user()->teamwork_company_id);
    }
}
