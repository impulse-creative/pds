<?php
/**
 * User Profile Table Model
 * @author Nick Decker
 * 
 * First Release: v1.0
 */


class TeamworkEmployees extends Eloquent {
    public $timestamps = false;
    protected $table = 'pharmacy_employees';
    protected $fillable = ['hs_company_id', 'employee_name', 'employee_email', 'employee_phone', 'employee_title'];
    
    /**
     * Set up model relationships
     */
    public function user()
    {
        return $this->hasOne('User', 'hs_acc_number', 'hs_company_id');
    }
    
    /**
     * Query scopes
     */
    public function scopeEmployees($query)
    {
        return $query->where('teamwork_company_id', Auth::user()->teamwork_company_id);
    }
}
