<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class TmpVendor extends Eloquent implements UserInterface, RemindableInterface {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */

	use UserTrait, RemindableTrait;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $table = "vendors";
	

    /** 
     * Get vendors id and company name
     *
     * @usage
     */
    public function scopeVendorget($query)
    {
        return $query->select('id', 'vendor', 'contact_href', 'contact_info');
    }
    
    public static function lookForMutations()
    {
        return TmpVendor::orderBy('vendor')->get();
    }
}