<?php

/**
 * User Table Model
 * @author Derek J. Foster
 * 
 * First Release: v1.0
 */
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Carbon\Carbon as Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {
    
    use UserTrait,
        RemindableTrait,
        SoftDeletingTrait,
        SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'fname' => 10,
            'lname' => 10,
            'email' => 5,
        ]
    ];

    public static $profile_update_rules = array(
        "fname" => "required",
        "lname" => "required",
        "pharmacy" => "required",
        "email" => "required",
        "city" => "required",
        "state" => "required",
        "digest" => "required"
    );
    
    public static $password_change_rules = array( 
        "fname" => "required",
        "lname" => "required",
        "pharmacy" => "required",
        "email" => "required",
        "city" => "required",
        "state" => "required",
        "digest" => "required",
        "password" => "required|confirmed"
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    protected $guarded = array();
    //This is the key under which the signed in user data
    //is kept in session key: user
    private static $user_session_key = 'user';
    //Soft deleting activation
    protected $dates = ['deleted_at'];

    /**
     * Array used by FactoryMuff to create Test objects
     */
    public static $factory = array(
        'title' => 'string',
        'slug' => 'string',
        'content' => 'text',
        'author_id' => 'factory|User', // Will be the id of an existent User.
    );
    
    /**
     * Construct one to many relationship between 'users' and 'resource_files' tables
     */
    public function file() {
        return $this->hasMany('FileStorage');
    }
    
    /**
     *  Companies table relation
     */
    public function companies() {
        return $this->hasMany('Company', 'id', 'admin_id');
    }
    
    /**
     *  Teamwork company relationship
     */
    public function teamwork()
    {
        return $this->hasMany('TeamworkCompanies', 'owner_id', 'id');
    }
    
    /**
     *  Company scope
     */
    public function scopeCompany($query)
    {
        return $query->where('sf_id', Auth::user()->sf_id);
    }
    
    
    /**
     *  HubSpot Company Scope
     */
    public function scopeHubspotCompany($query) {
        return $query->where('hs_acc_number', Auth::user()->hs_acc_number);
    }
    
    /**
     * User location relationship
     */
    public function state()
    {
        return $this->hasOne('EventStates', 'id', 'state');
    }

    /**
     * Market Listing Relationship
     */
    public function marketListing() {
        return $this->hasOne('Vendor', 'user_id');
    }

    /**
     * Guide Entry Relationship 
     */
    public function guideEntry() {
        return $this->hasOne('GuideEntries', 'user_id');
    }

    /**
     * Guide Ad Relationship
     */
    public function guideAd() {
        return $this->hasOne('GuideAds', 'user_id');
    }

    /**
     * VIP Ticket Items Relationship
     */
    
    public function lastReplied() {
        return $this->hasMany('ForumTopics', 'last_replied');
    }
    
    public function ticket_recipients() {
        return $this->hasMany('VipTicketRecipient', 'user_id');
    }
    
    public static function isEmployee() {
        $employees = DB::table('users')->where('groups', 'LIKE', "%14%")->get();
        return $employees;
    }
    
    public static function getAllNewUsers() {
        $users = self::where('confirmed_user', '1')->where('groups', 'not like', "%4:%")->paginate(20);
        return $users;
    }

    public static function getBioImage($id, $class = '') {
        $user = User::find($id);
        $member = in_array('2', explode(':', $user->groups)) ? ' member' : '';
        $badge = $user->created_at->diffInDays(Carbon::now()) < 30 ? '<img class="badge-profile" src="' . URL::to('storage/files/badge_images/newUser.png') . '" />' : '';
        if ($user->bio_image == '') {
            $url = 'https://i0.wp.com/assets1.uvcdn.com/pkg/admin/icons/user_70-c68d06098b40646a91b7656094632c19.png?ssl=1';
        } else {
            $url = URL::to('/storage/files/profile_images') . '/' . str_replace(' ', '%20', $user->bio_image);
        }
        if ($user->badges != '') {
            $tempBadges = json_decode($user->badges);
            $badge = '<img class="badge-profile" src="' . URL::to('storage/files/badge_images/' . $tempBadges->{1}->url) . '" />';
        }
        
        $html = '<div class="profile-image-container ' . $class . '">'.$badge.'<div class=" user-image'.$member.'" style="background: url('.trim($url).');" alt="'.$user->fname.'-'.$user->lname.'" ></div></div>';
        return $html;
    }

    /**
     * Raffle Item Relationship
     */
    public function raffleItem() {
        return $this->hasOne('RaffleItems', 'user_id');
    }

    /**
     * Sponsorship Relationships
     */
    public function sponsorships() {
        return $this->hasOne('Sponsorships', 'user_id');
    }

    /**
     * Resource relationship
     */
    public function resources() {
        return $this->hasMany('Resources', 'author', 'id');
    }

    public function notifications() {
        return $this->hasOne('Notifications', 'user_id');
    }

    public function userAchievement() {
        return $this->hasOne('UserAchievement', 'user_id', 'id');
    }
    /**
     * Set user level admin scope query logic
     *
     * @usage $admin = User::admin()->get();
     */
    public function scopeAdmin($query) {
        return $query->where('groups', '=', '5');
    }

    /**
     * Set user level vendor scope query logic
     *
     * @usage $vendors = User::vendor()->get();
     */
    public function scopeVendor($query) {
        return $query->where('groups', '=', '3');
    }
    
    /**
     * LOGIN AND SIGN UP SECTION HANDLERS
     * 
     */
    public function createUser($company, $first_name, $last_name, $email, $password, $accountType) {

        $user = new User;
        //create new user object
        $whitelist_ip = FailedLogins::getIpAddress();
        $user->hs_acc_number = self::getUserHSAccountId($email, $first_name, $last_name, $accountType);
        $user->fname = ucwords($first_name);
        $user->lname = ucwords($last_name);
        $user->email = $email;
        $user->password = self::passwordHash($password);
        $user->pharmacy = ucwords($company);
        $user->remember_token = '';
        $user->bio = '';
        $user->job_title = self::getUserJobTitleFromHS($email);
        $user->associated_data = '';
        $user->groups = '0:12';
        $user->whitelisted_ip_list = 1;
        $user->account_type = $accountType;
        $user->sf_id = self::getSalesforceCompanyId($user->hs_acc_number);
        if ($user->save()) {
            //return user id to controller
            return $user->id;
        } else {
            return Redirect::back()->with('wtf');
        }
    }

    /**
     * Hashes the user password
     */
    private function passwordHash($pass) {
        $hash = Hash::make($pass);

        return $hash;
    }

    // does cleanup onto the user_id (making sure it's a number and > 0)
    private function cleanUserId($user_id) {
        $user_id = intval($user_id);

        if ($user_id === 0) {
            throw new Exception('Invalid user ID');
        }

        return $user_id;
    }

    /**
     * Retrieve basic user account info
     */
    public function queryUserBasicInfo($id) {
        $user = DB::table('users')->where('id', '=', $id)->first();
        return $user;
    }

    /**
     * May not be needed because I'm beginning to use the Auth class for
     * logins and validations
     */
    public function queryUser($email) {
        $user = DB::table('users')->where('email', '=', $email)->first();
        return $user;
    }

    public static function getUserByEmail($email) {
        $user = User::where('email', $email)->paginate(20);
        return $user;
    }
    public function validateUserEntry() {
        $rules = array(
            'fname' => array('required'),
            'lname' => array('required'),
            'company-name' => array('required'),
            'email' => array('required', 'email', 'unique:users,email'),
            'password' => array('required', 'min:8')
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            //validation has failed
            //Session::flush();
            return Redirect::back()->with('Incomplete', 'Please complete the form to continue');
        } else {
            return true;
        }
    }

    // initiates method to create user session
    public static function handleSessionCreation($userObj) {
        if (is_object($userObj)) {
            self::createUserSession($userObj);
            //self::saveUserLogin($id->user_id);

            return true;
        } else {
            throw new Exception('Not a real object. -Source: handleSessionCreation()');
        }
    }

    // returns true if a user is signed in or false if not
    // usage: if (Accounts::isSignedIn()) { ... signed in } else { ... not signed in }
    public static function isSignedIn() {
        if (Session::has(self::$user_session_key)) {
            $user = self::getSessionUser();

            if (is_array($user) && !empty($user)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //Create user session
    private static function createUserSession($userObj) {
        if (!is_object($userObj)) {
            throw new Exception('Invalid user object provided');
        }

        $user_data = array(
            'user_id' => intval($userObj->id),
            'hs_acc_number' => $userObj->hs_acc_number,
            'fname' => $userObj->fname,
            'lname' => $userObj->lname,
            'email' => trim($userObj->email),
            'pharmacy' => trim(strtolower($userObj->pharmacy))
        );

        Session::put(self::$user_session_key, $user_data);
    }

    // returns all the data stored (in session) about the current signed in user
    // Usage: $user = User::getSessionUser();
    public static function getSessionUser() {
        $user = (array) Session::get(self::$user_session_key);

        return $user;
    }

    // returns the user ID for the signed in user
    // Usage: $user_id = User::getUserId();
    public static function getUserId() {
        $user = self::getSessionUser();

        return isset($user['user_id']) ? intval($user['user_id']) : 0;
    }

    // returns the user first name for the signed in user
    // Usage: $fname = User::getSessionUser();
    public static function getUserFirstName() {
        $user = self::getSessionUser();

        return isset($user['fname']) ? $user['fname'] : '';
    }

    // returns the user last name for the signed in user
    // Usage: $lname = User::getUserLastName();
    public static function getUserLastName() {
        $user = self::getSessionUser();

        return isset($user['lname']) ? $user['lname'] : '';
    }

    // returns the user name (formed from first and last names) for the signed in user
    // Usage: $name = User::getUserName();
    public static function getUserName() {
        $user = self::getSessionUser();

        $first_name = isset($user['fname']) ? $user['fname'] : '';
        $last_name = isset($user['lname']) ? $user['lname'] : '';

        return trim(ucwords($first_name . ' ' . $last_name));
    }

    // returns the type (investor or entrepreneur) for the signed in user
    // Usage:: $type = User::getUserType();
    public static function getUserPharmacyName() {
        $user = self::getSessionUser();

        if (!isset($user['pharmacy'])) {
            throw new Exception('Invalid user session');
        }

        return trim(strtolower($user['pharmacy']));
    }

    // returns the user email for the signed in user
    // Usage: $email = User::getSessionUserEmail();
    public static function getUserEmail($id = 0) {
        if ($id == 0) {
            $user = self::getSessionUser();
            return $user->email;
        } else {
            $user = User::find($id);
            return $user->email;
        }
        // removed because i don't think it is right
        //return isset($user['email']) ? $user['email'] : '';
    }

    /**
     * HUBSPOT ACCOUNTS HANDLERS
     * Check to see if the user has a hubspot account
     */
    //checks to see if the user has a hubspot account
    public function checkForHubspotAccount($email) {
        $contactConnect = HubSpot::contacts();
        $test = $contactConnect->get_contact_by_email($email);
        if (isset($test->properties) && is_object($test->properties))
            return $test->properties;

        return false;
    }
    
    public static function addUserToHubspot($user) {
        $contacts = Hubspot::contacts();
        $newUser = $contacts->create_contact([
            'firstname' => $user->fname,
            'lastname' => $user->lname,
            'email' => $user->email
        ]);
        if(is_null($newUser)) {
            $newContacts = Hubspot::contacts();
            $newRecord = $newContacts->get_contact_by_email($user->email);   
            return $newRecord;
        }
        return $newUser;
        
        // throw new Exception('Could not create the new user in Hubspot. Error: '.var_dump($newUser));
    }

    public static function getUserHSImage($email) {
        $contacts = HubSpot::contacts();
        $contact = $contacts->get_contact_by_email($email);
        if (isset($contact->properties->twitterprofilephoto->value)) {
            $filename = $this->saveImageFromUrl($contact->properties->twitterprofilephoto->value);
            $returnValue = $filename;
        } else {
            $returnValue = '';
        }
        return $returnValue;
    }

    public function saveImageFromUrl($url) {
        $urlArray = explode('/', $url);
        $count = count($urlArray);
        $name = $urlArray[$count - 1];
        $nameDivided = explode('.', $name);
        $countDots = count($nameDivided);
        $imageType = $nameDivided[$countDots - 1];
        $destination = public_path() . '/' . $path . '/' . $name;
        $filename = file_put_contents($destination, file_get_contents($url));
        //FOR LATER USE
        //dd($filename);
    }

    public static function getUserHSAccountId($email, $fname, $lname, $accountType = 1) {
        $contactConnect = HubSpot::contacts();
        $contact = $contactConnect->get_contact_by_email($email);
        if (isset($contact->properties->associatedcompanyid->value)) {
            $returnValue = $contact->properties->associatedcompanyid->value;
        } else {
            $returnValue = 'none';
        }
        $newaccount = self::createHubspotAccount($email, $fname, $lname, $accountType);
        return $returnValue;
    }
    
    public static function getUserJobTitleFromHS($email)
    {
        $contactConnect = HubSpot::contacts();
        $contact = $contactConnect->get_contact_by_email($email);
        if (isset($contact->properties->jobtitle->value)) {
            return $contact->properties->jobtitle->value;
        }
        
        return 'No Title Found';
    }
    
    public static function checkIfCompanyOwner($email) {
        $contactConnect = HubSpot::contacts();
        $contact = $contactConnect->get_contact_by_email($email);
        if (isset($contact->properties->title->value)) {
            return strtolower($contact->properties->title->value);
        }
        
        return 'Not Owner';
    }

    public static function getSalesforceCompanyId($hs_acc_number) {
        $companies = HubSpot::companies();
        $company = $companies->get_company($hs_acc_number);
        if (isset($company->properties->salesforceaccountid->value)) {
            $returnValue = $company->properties->salesforceaccountid->value;
        }else{
            $returnValue = 'none';
        }
        return $returnValue;
    }
    
    public function getCompanyEmployees($hs_acc_number) {
        $hs_acc_number = '13717522'; //temporary
        $employee_contact_list[0] = $this->getCompanyFromHubspot($hs_acc_number);
        $local_employee_repo_check = $this->checkIfEmployeesExistForCompanyLocally($hs_acc_number);
        if($local_employee_repo_check === 'none'){
            $companies = HubSpot::companies();
            $employee_contact_list = $this->getEmployeesFromHubspot($employee_contact_list, $hs_acc_number, $companies);
            //store the employees locally for quicker loading in future requests.
            $store_employees = $this->addEmployeesToLocalDatabase($employee_contact_list, $hs_acc_number);
            if($store_employees == 'Failures') {
                return Redirect::route('dash.main')
                        ->with('Failure', 'There were failures when storing your company employees. Please check that their information was entered correctly in Salesforce. We sent you to the dashboard so the error would not keep repeating in an endless loop.');
            }
            return $employee_contact_list;
        }else{
            return $employee_contact_list = $this->getLocallyStoredEmployees($hs_acc_number, $employee_contact_list);
        }
    }
    
    public function getCompanyFromHubspot($hs_acc_number) {
        $companies = HubSpot::companies(); //initialize companies API class instance
        $company = $companies->get_company($hs_acc_number); 
        $employee_contact_list = array();
        $employee_contact_list[0] = [
            'company_start_date' => $company->properties->pds_start_date__c->timestamp,
            'company_website' => $company->properties->company_website->value,
            '2015_conference' => $company->properties->x2015_conference_registered__c->value,
            '2014_conference' => $company->properties->x2014_conference_pharmacy_registered__c->value,
            '2013_conference' => $company->properties->x2013_conf_pharmacy_registered__c->value,
            '2012_conference' => $company->properties->x2012_conference__c->value,
            '2011_conference' => $company->properties->x2011_conference__c->value
        ];
        
        return $employee_contact_list[0];
    }
    
    public function getEmployeesFromHubspot($employee_contact_list, $hs_acc_number, $companies) {
            $contacts = HubSpot::contacts();
            $company_employees = $companies->get_all_employees($hs_acc_number);
            $contact_list = array();
            $count = 0;
            foreach($company_employees->vids as $vid) {
                $contact_list[] = $contacts->get_contact_by_id($vid);
                $count++;
            }
            for($i=1; $i <= $count - 1; $i++) {
                $employee_contact_list[$i] = [
                    'name' => $contact_list[$i]->properties->firstname->value.' '.$contact_list[$i]->properties->lastname->value,
                    'email' => $contact_list[$i]->properties->email->value,
                    'phone' => isset($contact_list[$i]->properties->mobilephone->value) ? $contact_list[$i]->properties->mobilephone->value : 'N/A',
                    'title' => isset($contact_list[$i]->properties->jobtitle->value) ? $contact_list[$i]->properties->jobtitle->value : 'N/A'
                ];
            }
            
            return $employee_contact_list;
    }
    
    public function getLocallyStoredEmployees($hs_acc_number, $employee_contact_list){
        $employees = TeamworkEmployees::where('hs_company_id', $hs_acc_number)->get();
        foreach($employees as $employee) {
            $employee_contact_list[] = [
                'name' => $employee->employee_name,
                'email' => $employee->employee_email,
                'phone' => $employee->employee_phone,
                'title' => $employee->employee_title
            ];
        }
        
        return $employee_contact_list;
    }
    
    public function addEmployeesToLocalDatabase($contact_list, $company_id) 
    {
        $employee_record = new TeamworkEmployees;
        $failures = 0;
        foreach(array_splice($contact_list, 1) as $contact){
            $input = [
                'hs_company_id' => $company_id,
                'employee_name' => $contact['name'],
                'employee_email' => $contact['email'],
                'employee_phone' => $contact['phone'],
                'employee_title' => $contact['title']
            ];
            
            $create_employee_record = $employee_record->create($input);
            
            if($create_employee_record) {
                continue;
            }
            $failures++;
        }
        
        if ($failures > 0) {
            return 'Failures';
        }
        
        return 'Success';
    }
    
    public function checkIfEmployeesExistForCompanyLocally($hs_acc_number)
    {
        $employees = TeamworkEmployees::where('hs_company_id', $hs_acc_number)->get();
        
        if(count($employees) > 0){
            return $employees;
        }
        
        return 'none';
    }
    
    public function getUserIdByEmail($email)
    {
        $user = User::where('email', $email)->first();
        
        if(is_object($user)){
            return $user->id;
        }
        
        return null;
    }

    //create a simple hubspot account for a user
    public static function createHubspotAccount($email, $fname, $lname, $accountType) {
        $hsContext = [
            'hutk' => self::getHubspotToken(),
            'ipAddress' => self::get_client_ip(),
            'pageUrl' => Request::url(),
            'pageName' => 'Signup'
        ];
        $formData = [
            'email' => $email,
            'firstname' => $fname,
            'lastname' => $lname,
            'events' => 'true',
            'accounttype' => $accountType,
            'lifecyclestage' => 'lead'
        ];
        $forms = HubSpot::forms();
        $form = $forms->submit_form('37772', '34f2e350-0777-49cc-b1eb-bac18734dddb', $formData, $hsContext);
        return 1;
    }

    public static function getHubspotToken() {
        if(isset($_COOKIE['hubspotutk'])) {
            return $_COOKIE['hubspotutk'];
        } else {
            return 1;
        }
        
    }

    public static function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')){
            $ipaddress = getenv('HTTP_CLIENT_IP');
        }else if (getenv('HTTP_X_FORWARDED_FOR')){
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        }else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        }else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        }else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        }else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        }else{
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    public static function getCurrentPermissions() {
        $id = Auth::id();
        $user = User::find($id);
        return $user->groups;
    }

    public static function getUserPermissions($id) {
        $user = User::find($id);
        return $user->groups;
    }

    public static function updateSalesforceProperties($user, $contact) {
        
        $user->sf_title =  isset($contact->properties) && property_exists($contact->properties, 'title') ? $contact->properties->title->value : '';
        if(isset($contact->properties) && property_exists($contact->properties, 'associatedcompanyid')) {
            $companyId = $contact->properties->associatedcompanyid->value;
            $company = self::getHubspotCompanyProperties($companyId);
            $companyProperties = $company->properties;
            $user->my_plan = isset($contact->properties) && property_exists($companyProperties, 'coaching_plan__c') ? $companyProperties->coaching_plan__c->value : '';
            $user->my_coach = isset($contact->properties) && property_exists($companyProperties, 'coach') ? $companyProperties->coach->value : '';
            $user->my_is = isset($contact->properties) && property_exists($companyProperties, 'implementation_specialist') ? $companyProperties->implementation_specialist->value : '';
            $user->sf_multistore = isset($contact->properties) && property_exists($companyProperties, 'pds_member_multi_store_pharmacy__c') ? $companyProperties->pds_member_multi_store_pharmacy__c->value : '';
        }
        if($user->save()) {
            return true;
        }
        return false;
    }
    
    public static function checkForPermission($groups, $user) {
        $hsUser = self::getHubspotProperties($user->email);
        //Get the stuff from hubspot about the user
        if(isset($hsUser->status) && $hsUser->status = 'error') {
            $hsUser = self::getHubspotPropertiesById($user->vid);
            if($hsUser == NULL) {
                $hsUser = self::addUserToHubspot($user);
            }
        }
        //status error, message contact does not exist
        if(isset($user->vid) && $user->vid == '') {
            $user->vid = isset($hsUser->vid) ? $hsUser->vid : '';
            $user->save();
        }
        $updated = self::updateSalesforceProperties($user, $hsUser);
        $permitted = '';
        foreach ($groups as $group) {
            if (isset($hsUser->properties->{$group})) {
                $value = $hsUser->properties->{$group}->value;
                $permitted .= $value == 'yes' || $value == 'true' ? self::getGroupIdByName($group) . ':' : '';
            }
        }
        return $permitted;
    }

    public static function getGroupIdByName($name) {
        $group = Group::where('hs_field', $name)->first();
        return $group['id'];
    }

    public static function getHubspotProperties($email) {
        $contactConnect = HubSpot::contacts();
        $contact = $contactConnect->get_contact_by_email($email);
        return $contact;
    }
    
    public static function getHubspotPropertiesById($vid) {
        $contactConnect = Hubspot::contacts();
        $contact = $contactConnect->get_contact_by_id($vid);
        return $contact;
    }
    
    public static function getHubspotCompanyProperties($companyId) {
        $companies = Hubspot::companies();
        $company = $companies->get_company($companyId);
        return $company;
    }

    public static function getAppPermissions() {
        $fields = [];
        $group = HubSpot::properties();
        $properties = $group->get_property_group('pds_advantage___permission_levels')->properties;
        //dd($properties);
        foreach ($properties as $property) {
            $fields[] = $property->name;
        }
        return $fields;
    }

    public function hubspotAccHandle($data) {
        $connection = HubSpot::contacts(); //Connect to hubspot api
        $results = $connection->get_contact_by_email($data['email']); //retrieve contact information
        //set our variables for easy reference
        $full_name = '<p id="hs-acc-name">' . $results->properties->firstname->value . ' ' . $results->properties->lastname->value . '</p>';
        $email = '<p id="hs-acc-mail">' . $results->properties->email->value . '</p>';
        $first = $results->properties->firstname->value;
        $last = $results->properties->lastname->value;

        // Put desired results into parsable array
        $return_results = array(
            'fname' => $full_name,
            'email' => $email,
            'first' => $first,
            'last' => $last,
            'show-noti' => 'show the notification'
        );
        if (isset($results->properties->photo->value)) { //not always set
            $return_results["image"] = '<img id="hs-img" class="img-circle" src="' . $results->properties->photo->value . '" width=120 height=120/>';
        }
        if (isset($results->properties->company->value)) { //not always set
            $company = $results->properties->company->value;
            $return_results["company"] = $company;
        }
        return json_encode($return_results); //return data in json format to make things easier       
    }

    public function getHubspotCompanyId($email) {
        //establist connection to hubspot contacts
        $connection = HubSpot::contacts();
        //retrieve contact data
        $contact_data = $connection->get_contact_by_email($email);
        //we will retreive the id that has a key of 'associatedcompanyid'
        //dd($contact_data);
        return $contact_data->associatedcompanyid;
    }

    /**
     * Email Confirmation on first sign up
     *
     * Email new user confirmation email
     * Pre-confirmation: User can only access basic account info on dash
     * Post-confirmation: Full access to dashboard and features (Depending on their member status anyways)
     */
    
    public static function sendConfirmationRequestForImportedUser($user) {
        $data = [
            'token'     => self::inputConfirmationToken($user->email, $user->id),
            'company'   => $user->pharmacy,
            'name'      => $user->fname.' '.$user->lname,
            'id'        => $user->id
        ];
        $sendToUser = [ 'email' => $user->email, 'name' => $data['name'], 'fname' => $user->fname ];

        Mail::send('emails.confirmation', $data, function($message) use($sendToUser) {
            $message->to($sendToUser['email'], $sendToUser['name'])->subject($sendToUser['fname'] . ', activate Your PDS Advantage Account [ Pharmacy Development Services ] ');
        });
        $message = 'verifyimporteduser<div class="alert alert alert-success hello" '
                    . 'data-dismiss="alert">'.$user->fname.', Thanks for checking out the new '
                    . 'Pds Advantage, we just sent an email to '.$user->email.' to confirm your identity! Please follow '
                    . 'the instructions inside your email to finish activating your Pds Advantage Account. If you don\'t see your'
                    . ' email right away, please check your spam or junk folder.<br><br><strong>If you are an AOL or YAHOO email user, they may take longer to go into your spam or inbox.</div>';
        return $message;
    }
    
    
    public static function sendConfirmationRequest($email, $id, $imported = false) {
        Auth::loginUsingId($id);
        $token = self::inputConfirmationToken($email, $id);
        
        $name = Auth::user()->fname . ' ' . Auth::user()->lname;
        $fname = Auth::user()->fname;
        $data = array(
            'token' => $token,
            'company' => Auth::user()->pharmacy,
            'name' => $name,
            'id' => $id
        );
        $sendToUser = [ 'email' => $email, 'name' => $name, 'fname' => $fname];

        Mail::send('emails.confirmation', $data, function($message) use($sendToUser) {
            $email = Auth::user()->email;
            $message->to($sendToUser['email'], $sendToUser['name'])->subject($sendToUser['fname'] . ', activate Your PDS Advantage Account [ Pharmacy Development Services ] ');
        });
        //if the user requests to resend confirmation email
        return Response::make('Your confirmation email was sent.  Please allow a few minutes for it to reach your inbox. If you don\'t see the email, please check your spam folder.');
    }

    //puts confirmation token into database
    private static function inputConfirmationToken($email, $id) {
        //check for existing token
        $exists = DB::table('confirmations_for_user')->where('id', '=', $id)->first();
        if ($exists)
            return $exists->confirmations_tokenscol; //if it exists, return already stored token for resend
        $token = self::createConfToken();
        $enterToken = DB::table('confirmations_for_user')->insert(
                array('id' => $id, 'confirmations_tokenscol' => $token)
        );
        return $token;
    }

    //creates random confirmation token
    private static function createConfToken() {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        $length = 25;
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    /**
     * Pre-defined functions for the 'remember me' functionality on the login page
     */
    public function getRememberToken() {
        $this->remember_token;
    }

    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    /**
     * REMOVE USER ACCOUNT
     */
    public function removeUserAcc($user_id) {
        $user = $this->find($user_id);

        /* soft delete user */
        if ($user->delete()) {
            return Redirect::route('login.form');
        }

        return Redirect::back()->with('Failure', 'Account could not be deleted. Please try again. If the problem persists, contact your Web Administrator for help.');
    }
    
    


    
    public static function sendDailyDigestEmail($id, $type = 'email') {
        set_time_limit(0);
        $user = self::find($id);
        if($user->email != '') {
            $notifications = new Notifications();
            $allNotifications = $notifications->getAllNotifications($id);
            $totalNotifications = count($allNotifications);
            $permissions = self::getUserPermissions($id);

            $activePermissions = explode(':', $permissions);
            $activeGroups = [];
            foreach($activePermissions as $group) {
                if($group != '') {
                    $activeGroups[] = $group;
                }   
            }
            $categories = ForumCategories::whereIN('min_access_level', $activeGroups)->get();
            $pemittedCategories = [];
            if($categories != null) {
                foreach($categories as $category) {
                    $pemittedCategories[] = $category->id;
                }
                $newTopics = ForumTopics::where('created_at', '>=', date('Y-m-d H:i:s', time()-48900))
                        ->whereIn('parent_category', $pemittedCategories)
                        ->where('parent_category', '!=', '8')
                        ->where('is_pharmacy_private_post', '0')
                        ->take(10)
                        ->get();
                $events = Events::where('start_event', '>=', date('Y-m-d H:i:s', time()-2592000))
                        ->take(3)
                        ->get();
                $activeTopics = ForumTopics::with('messages', 'users')
                        ->where('updated_at', '>=', date('Y-m-d H:i:s', time()-48900))
                        ->whereIn('parent_category', $pemittedCategories)
                        ->where('parent_category', '!=', '8')
                        ->where('is_pharmacy_private_post', '0')
                        ->take(10)
                        ->get()
                        ->sortBy(function($topic) {
                            return $topic->messages->count();
                        })->reverse();
                $resources = Resources::where('created_at', '>=', date('Y-m-d H:i:s', time()-604800))
                        ->orWhere('updated_at', '>=', date('Y-m-d H:i:s', time()-604800))
                        ->whereIn('access_level', $pemittedCategories)
                        ->take(10)
                        ->get();
                $danQuote = DB::table('dan_quote')->select(DB::raw('announcement as danQuote'))->first();
                $subject = count($allNotifications) > 0 ? ucfirst($user->fname).', Your PDS Daily Digest Has arrived ['.count($allNotifications).' notifications]' : ucfirst($user->fname).', Your PDS Daily Digest Has arrived';
                $sendToUser = [
                    'email' => $user->email,
                    'name' => ucwords($user->fname) . ' ' . ucwords($user->lname),
                    'fname' => $user->fname,
                    'subject' => $subject 
                ];
                
                //   For Testing purposes only 
                  
//                  return View::make('emails.daily-digest', compact('user', 'allNotifications', 'newTopics', 'events', 'activeTopics', 'resources', 'totalNotifications', 'danQuote')); 
                 
                 
                if($type == 'email') {
                    Mail::send('emails.daily-digest', compact('user', 'allNotifications', 'newTopics', 'events', 'activeTopics', 'resources', 'totalNotifications', 'danQuote'), function($message) use ($sendToUser) {
                        $message->to($sendToUser['email'], $sendToUser['name'])
                        ->subject($sendToUser['subject']);
                    });
                    if(Mail::failures()) {
                        \Log::error('Error sending digest email user.'.$user->email, ['user' => $user->email]);
                    } else {
                        $user->digest = Carbon::now();
                        $user->save();
                    }
                } else {
                    return View::make('emails.daily-digest', compact('user', 'allNotifications', 'newTopics', 'events', 'activeTopics', 'resources', 'totalNotifications', 'danQuote')); 
                }
                
            }      
        }
        
    }
    
    public static function sendWeeklyDigestEmail($id) {
        set_time_limit(0);
        $user = self::find($id);
        if($user->email != '') {
            $notifications = new Notifications();
            $allNotifications = $notifications->getAllNotifications($id);
            $totalNotifications = count($allNotifications);
            $permissions = self::getUserPermissions($id);

            $activePermissions = explode(':', $permissions);
            $activeGroups = [];
            foreach($activePermissions as $group) {
                if($group != '') {
                    $activeGroups[] = $group;
                }   
            }
            $categories = ForumCategories::whereIN('min_access_level', $activeGroups)->get();
            $pemittedCategories = [];
            if($categories != null) {
                foreach($categories as $category) {
                    $pemittedCategories[] = $category->id;

                }
                $newTopics = ForumTopics::where('created_at', '>=', date('Y-m-d H:i:s', time()-604800))
                        ->whereIn('parent_category', $pemittedCategories)
                        ->where('parent_category', '!=', '8')
                        ->where('is_pharmacy_private_post', '0')
                        ->take(10)
                        ->get();
                $events = Events::where('start_event', '>=', date('Y-m-d H:i:s', time()-2592000))
                        ->take(3)
                        ->get();
                $activeTopics = ForumTopics::with('messages', 'users')
                        ->where('updated_at', '>=', date('Y-m-d H:i:s', time()-604800))
                        ->whereIn('parent_category', $pemittedCategories)
                        ->where('parent_category', '!=', '8')
                        ->where('is_pharmacy_private_post', '0')
                        ->take(10)
                        ->get()
                        ->sortBy(function($topic) {
                            return $topic->messages->count();
                        })->reverse();
                $resources = Resources::where('created_at', '>=', date('Y-m-d H:i:s', time()-604800))
                        ->orWhere('updated_at', '>=', date('Y-m-d H:i:s', time()-604800))
                        ->whereIn('access_level', $pemittedCategories)
                        ->take(10)
                        ->get();
                $danQuote = DB::table('dan_quote')->select(DB::raw('announcement as danQuote'))->first();
                $subject = count($allNotifications) > 0 ? $user->fname.', Your PDS Daily Digest Has arrived ['.count($allNotifications).' notifications]' : $user->fname.', Your PDS Daily Digest Has arrived';
                $sendToUser = [
                    'email' => $user->email,
                    'name' => ucwords($user->fname) . ' ' . ucwords($user->lname),
                    'fname' => $user->fname,
                    'subject' => $subject 
                ];
                
                /*   For Testing purposes only 
                 * 
                 * return View::make('emails.daily-digest', compact('user', 'allNotifications', 'newTopics', 'events', 'activeTopics', 'resources', 'totalNotifications', 'danQuote')); 
                 *
                 */
                
                Mail::send('emails.daily-digest', compact('user', 'allNotifications', 'newTopics', 'events', 'activeTopics', 'resources', 'totalNotifications', 'danQuote'), function($message) use ($sendToUser) {
                    $message->to($sendToUser['email'], $sendToUser['name'])
                    ->subject($sendToUser['subject']);
                });
                if(Mail::failures()) {
                    \Log::error('Error sending digest email user id: '.$user->id . 'email: '.$user->email);
                } else {
                    $user->digest = Carbon::now();
                    $user->save();
                }
            } 
        } 
        
    }

    public static function getRecentForum($total = 5) {
        $recent = \UserRepo\Storage\Forums\EloquentTopicRepository::getStaticRecentTopics($total);
        return $recent;
    }

    public static function getPopularForum($total = 5) {
        $popular = \UserRepo\Storage\Forums\EloquentTopicRepository::getStaticPopularTopics($total);
        return $popular;
    }

    public static function getUsersPosts($id) {
        $forum['posts'] = $this->topics->getTopicsByUser($id);
        $forum['replies'] = $this->topics->getRepliesByUser($id);
        return $forum;
    }

    public static function updateConfirmedInHubspot($email) {
        $connection = HubSpot::contacts();
        $contact = $connection->get_contact_by_email($email);
        if(property_exists($contact, 'vid')) {
            $updated = $connection->update_contact($contact->vid, ['confirmed_in_pdsadvantage' => 'true']);
            return 1;
        }
        return 0;
    }
    
    public static function updateUserBadges($user) {
        $saved = $user->save() ? 'Users Badges Were Updated' : 'Something Went Wrong';
        return $saved;
    }

    public function encodeBadgesForUser($id, $object) {
        $user = User::find($id);
        $user->badges = json_encode($object);
        return $user;
    }

    public static function addBadgeToUser($id, $badge) {
        $user = User::find($id);
        $user->badges = is_object($user->badges) ? json_decode($user->badges) : new stdClass();
        $user->badges->{$badge->id} = property_exists($user->badges, $badge->id) ? $user->badges->{$badge->id} : new stdClass();
        $user->badges->{$badge->id}->name = $badge->name;
        $user->badges->{$badge->id}->url = $badge->url;
        $user->badges->{$badge->id}->date = Carbon::now();
        $user->badges = json_encode($user->badges);
        return self::updateUserBadges($user);
    }

    public function addBadgesToUser($id, $badges) {
        $userBadges = $this->getBadgesForUser($id);
        foreach ($badges as $badge) {
            $userBadges->{$id} = self::getBadgeData($badge);
        }
        $user = $this->encodeBadgesForUser($id, $userBadges);
        return $this->updateUserBadges($user);
    }

    public static function getBadgesForUser($id) {
        $user = User::find($id);
        return json_decode($user->badges);
    }

}
