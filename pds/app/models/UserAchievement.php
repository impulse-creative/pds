<?php

class UserAchievement extends Eloquent {
    
    protected $table = 'user_achievements';
    protected $guarded = ['id'];
    
    public function User() {
        return $this->hasMany('User');
    }
    
    public static function getUserAchievements($id)
    {
        $achievements = UserAchievement::where('user_id', $id)->first();
        if($achievements != null) {
            $achievements->completed = json_decode($achievements->completed);
        } else {
            $achievements = new UserAchievement();
            $achievements->user_id = $id;
            $achievements->completed = '';
        }
        return $achievements;
    }
    
    public static function getAcheivementsForDash($id) {
        $achievements = UserAchievement::where('user_id', $id)->first();
        if($achievements != null || $achievements != '') {
            return json_decode($achievements->completed);
        }
        return false;
        
    }
    
    public static function getPreviousAchievementTotal($achievements, $achievementId)
    {
        if(is_object($achievements->completed)) {
            $total = property_exists($achievements->completed, $achievementId) ? ($achievements->completed->{$achievementId} + 1) : 1; 
            return $total;
        }
        return 1;
    }
    
    public static function createAchivementObject($achievements)
    {
        $completed = is_object($achievements->completed) ? $achievements->completed : new stdClass();
        return $completed;
    }
    
    public static function addAchievement($userId, $achievementId) 
    {
        $achievements = self::getUserAchievements($userId);
        $total = self::getPreviousAchievementTotal($achievements, $achievementId);
        $completed = self::createAchivementObject($achievements);
        $completed->{$achievementId} = new stdClass();
        $completed->{$achievementId} = $total;
        $achievements->completed = json_encode($completed);
        $saved = $achievements->save() ? 'Users Achievements Were Updated' : 'Something Went Wrong';
        $badges = Badge::checkIfUserReceivedBadge($achievements, $userId);
        return $saved;
    }    
}
