<?php


class Vendor extends Eloquent {
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    protected $hidden = array('password', 'remember_token');
	protected $table = "pds_vendors";
	
    public function category()
    {
        return $this->hasOne('MarketCategories', 'id', 'category');
    }
    public function subCategory()
    {
        return $this->hasOne('MarketSubCategories', 'id', 'subcategory');
    } 
    public function getVendorById($id)
    {
        $vendor = $this->where('user_id', $id)->first();
        return $vendor;
    }


    public function signupCompletionPercentage()
    {
        if(Auth::check()) {
            //query each form table for user's data
            $listing_completed = Vendor::where('user_id', '=', Auth::id())->first();
            $market_profile_completed = GuideEntries::where('user_id', '=', Auth::id())->first();
            $guide_ad_completed = GuideAds::where('user_id', '=', Auth::id())->first();
            $raffle_item_completed = RaffleItems::where('user_id', '=', Auth::id())->first();
            $vip_tickets_complete = VipTicketRecipient::where('user_id', '=', Auth::id())->first();
            $sponsorships_complete = Sponsorships::where('user_id', '=', Auth::id())->first();

            $new_percent = 0;
            //check to see if they have a record pertaining to each form table, increment completion percent if true.
            if (is_object($listing_completed))
                $new_percent += 16.7;

            if (is_object($market_profile_completed))
                $new_percent += 16.7;

            if (is_object($guide_ad_completed))
                $new_percent += 16.7;

            if (is_object($raffle_item_completed))
                $new_percent += 16.7;

            if (is_object($vip_tickets_complete))
                $new_percent += 16.7;

            if (is_object($sponsorships_complete))
                $new_percent += 16.7;

            return $new_percent != 0 ? $new_percent : 0;
        }

        return Redirect::route('login.form')->with('Unsuccessful Login', 'Please log in to continue.');
    }
        
}