<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Whitelists extends Eloquent implements UserInterface, RemindableInterface {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 * We didn't have to set this because of Eloquent, but I'm just showing that we can
	 * @var string
	 */
	protected $table = 'whitelists';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	//array of throttle settings. # of failed responses => response
	protected static $default_throttle_settings = [
		10 => 1,			//delay in seconds
		20 => 2,			//delay in seconds
		50 => 3,			//delay in seconds
		150 => 4,			//delay in seconds
		300 => 'captcha'	//use robot catcher question
	];

	//time frame to use when retrieving number of failed login attempts from database
	protected static $time_frame_minutes = 10;
	protected static $custom_throttle_settings = array();
	protected static $response_array =	array(
			'status' => 'safe',
			'message' => null
	);
	protected $guarded = array();
	public $timestamps = false;

	//return related user
	public function user(){
		return $this->hasOne('User');
	}

	public static function addCurrentToWhitelist($id)
	{

		$whitelist_ip = FailedLogins::getIpAddress();

		$whitelist = Whitelists::create([
			'user_id' => $id,
			'registered_ip_list' => $whitelist_ip
		]);

		$whitelist = DB::table('whitelists')->where('user_id', '=', $id)->first();

		return $whitelist->id;
	}

	/**
	 * Check the whitelist availability of the user in question
	 *
	 * @param integer $user_id
	 * @param string $ip_in_question
	 */
	public function checkWhitelistFrontDoor($email, $ip_in_question)
	{
		//retrieve whitelisted IPs associated to user
		$user_ip_list = User::select('whitelisted_ip_list')->where('email', '=', $email)->first();

		//retrieve the corresponding whitelist
		$whitelist = Whitelists::select('registered_ip_list')->where('id', '=', $user_ip_list->whitelisted_ip_list)->first();

		//explode resulting list string into an array of elements
		$whitelist_array = explode(",", $whitelist->registered_ip_list);
		//echo $ip_in_question;
		//echo var_dump($whitelist_array);

		//check if ip in question is inside of the user's whitelist array
		if(in_array($ip_in_question, $whitelist_array)) {
			self::$response_array['status'] = 'whitelisted';
			self::$response_array['message'] = 'IP is recognized for safe entry.';
		}else{
			self::$response_array['status'] = 'error';
			self::$response_array['message'] = 'IP is not recognized for safe entry.';
		}

		return self::$response_array;
	}

}