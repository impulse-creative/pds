<?php

use Carbon\Carbon as Carbon;

class Notifications extends Eloquent {
	/**
	 * Eloquent will take the class name here and use the plural form as a 
	 * reference to what the db table is that we're using in this model
	 * So, in this case, we should have a table created called 'users'
	 * so that this will work.
	 */


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    
	protected $table = "notifications";
        
        public function user() {
            $this->belongsTo('User', 'id', 'user_id');
        }
        
        public function getNotifications($id,  $type) {
            $notifications = Notifications::where('user_id', $id)->first();
            ${$type} = $notifications != null ? json_decode($notifications->{$type}) : null;
            return ${$type};
        }
        
        public function getAllNotifications($id) {
            $notifications = new \Notifications();
            $topics = $notifications->getNotifications($id, 'topics');
            $resources = $notifications->getNotifications($id, 'resources');
            $activeTopics = $this->checkForActiveNotifications($topics, 'topics');
            $activeResources = $this->checkForActiveNotifications($resources, 'resources');
            return $this->combineNotifications($activeTopics, $activeResources);
        }
        
        public function combineNotifications($topics, $resources) {
            $notifications = [];
            if($topics !== null) {
                if(count($topics) == 1) {
                    $notifications[] = $topics[0];
                } else {
                    foreach($topics as $id => $topic) {
                        if($topic != null) $notifications[] = $topic;
                    }
                }
            }
            if($resources !== null) {
                if(count($resources) == 1) {
                    $notifications[] = $resources[0];
                } else {
                    foreach($resources as $id => $resource) {
                        if($resource != null) $notifications[] = $resource;
                    }
                }
                    
            }
            return $notifications;
        }
        
        public function checkForActiveNotifications($notifications, $type) {
            $active = [];
            if($notifications == null) return $active;
            
            foreach($notifications as $id => $notification) {
                if ($notification->show == 1) {
                    if($this->compareNotificationData($id, $notification, $type) != null) {
                        $active[] = $this->compareNotificationData($id, $notification, $type);
                    }
                }
            }
            return $active;
        }
        
        public function compareNotificationData($id, $notification, $type) {
            $data = $this->getNotificationData($type, $id);
            if ($data != null || $data != '') {
                $dataUpdated = $data->updated_at;
                $userUpdated = str_replace('.000000', '', $notification->date->date);
                $updatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $userUpdated);
                $isActive = $dataUpdated->between($updatedDate, Carbon::now(), false);
                if ($isActive == true) {
                    return $data;
                }
            }
            return null;
        }
        
        public function getNotificationData($type, $id) {
            if($type == 'topics') {
                return ForumTopics::find($id);
            }
            return Resources::find($id);
        }
        
        public function addNewNotification($data) {
            $id = Auth::id();
            $type = $data['type'];
            $grantAchievement = $this->makeSubscribeAchievements($type);
            $notifyId = $data['id'];
            $msgId = isset($data['msg']) ? $msgId = $data['msg'] : '';
            $save = $this->checkIfDuplicateNotification($id, $notifyId, $msgId, $type);
            $message = '<div class="alert alert-success alert-dismissible" role="alert">';
            $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            $message .= '<strong>Subscribed!!</strong></div>';
            return $message; 
        }
        
        public function makeSubscribeAchievements($type) {
            if($type == 'topics') {
                $id = 5;
            } else {
                $id = 4;
            }
            return \UserAchievement::addAchievement(\Auth::id(), $id);
        }
        
        public function checkIfDuplicateNotification($id, $targetId, $msgId, $type = 'topics') {
            $currentNotifications = $this->getUserNotifications($id, $type);
            if($currentNotifications == false) {
                if(is_object(Notifications::find($id))) {
                    $this->addNewAwesomeNotification($id, $targetId, $type, $msgId);
                    return true;
                }
                $new = $this->createNewNotificationRecord($id, $targetId, $msgId, $type); 
                $message = '<div class="alert alert-success alert-dismissible" role="alert">';
                $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                $message .= '<strong>Subscribed!!</strong></div>';
                return $message;
            }

            foreach($currentNotifications as $notification => $value) {
                if($notification == $targetId) {
                    $this->addNewAwesomeNotification($id, $targetId, $type, $msgId);
                    return true;
                }
            }
            if($this->addNewAwesomeNotification($id, $targetId, $type, $msgId) == 'saved') {
                return true;
            }
            return false;
        }    
        
        public function getUserNotifications($id, $type = 'topics') {
            $notifications = Notifications::where('user_id', $id)->first();
            if(is_object($notifications)) {
                $returnObject = $notifications->{$type} != null ? json_decode($notifications->{$type}) : new \stdClass();
                return $returnObject;
            }
            return false;
        }
        
        public function addNewAwesomeNotification($id, $targetId, $type, $msgId) {
            $notification = $this->getUserNotifications($id, $type);
            $notification->{$targetId} = new stdClass();
            $notification->{$targetId}->date = Carbon::now();
            $notification->{$targetId}->show = 1;
            $saved = $this->saveAwesomeNotifications($id, $notification, $type);
            return $saved;
        }
        
        public function saveAwesomeNotifications($id, $notification, $type) {
            $notifications = Notifications::where('user_id', $id)->first();
            $notifications->{$type} = json_encode($notification);
            if($notifications->save()) {
                return 'saved';   
            }
            return 'failed';
        }
        public function createNewNotificationRecord($id, $topicId, $column, $msgId) {
            $notification = new Notifications();
            $notification->user_id = $id;
            $notification->{$column} = new stdClass();
            $notification->{$column}->{$topicId} = new stdClass();
            $notification->{$column}->{$topicId}->date = Carbon::now();
            $notification->{$column}->{$topicId}->show = 1;
            if($notification->save()) {
                return 'saved';    //saved
            }
            return 'failed';

        }
        
        public static function createNotificationRow($id) {
            $new = new Notifications();
            $new->user_id = $id;
            $new->digest = 1;
            $new->topics = '';
            $new->resources = '';
            $new->events = '';
            if($new->save()) {
                return $new->digest;
            }
            return 0;
        }
        
        public static function userHasNotificationsRow($id) {
            $notifications = self::where('user_id', $id);
            return $notifications == null ? false : true;
        }
}
