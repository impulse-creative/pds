<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */



//DB::listen(function($sql, $bindings, $time)
//{
//    d($sql);
//    d($bindings);
//});



/* ======================
 * Set up global variables for all views
 * ======================
 */
//-->id of logged in user
Route::any('user/{id}/unsubscribe', [
    'uses' => 'UserController@unsubscribeUser'
]);
if (Auth::check()) {
    $user_info = User::findOrFail(Auth::id());
    View::share('user_info', $user_info);
    View::share('id', Auth::id());
    $hs_acc = User::findOrFail(Auth::id());
    View::share('hs_acc', $hs_acc->hs_acc_number);
}

Route::group(['prefix' => 'resource-preview'], function(){
    Route::controller('boxviewtext', 'BoxView');
    Route::any('/{file_id}', [
        'uses' => 'BoxView@show',
        'as' => 'view.doc'
    ]);
    Route::any('/store', [
        'uses' => 'BoxView@store',
        'as' => 'store.docs'
    ]);
    Route::any('/flush/boxview/files', [
        'uses' => 'BoxView@destroy',
        'as' => 'destroy.doc'
    ]);
    Route::any('log/preview/refresh', [
        'uses' => 'BoxView@logRefreshEvent',
        'as' => 'log.page.refresh'
    ]);
});

Route::group([
    "domain" => "dev.localhost"
        ], function() {
    Route::get('/', [
        'as' => 'login.form',
        'uses' => 'UserController@showLogin'
    ])->before('guest');
});


Route::model('login', 'User');
Route::controller('login', 'UserController');

//landing page
Route::get('/', function() {
    /**
     * When they reach the home page, we will check to see if their 
     * remember cookie is in place, then log them in.
     */
    $cookie = Cookie::get('company');
    if (isset($cookie)) {
        $user = new User;
        $userObj = $user->queryUserId($cookie);
        $user->handleSessionCreation($userObj);
        $id = User::getUserId();
        Auth::loginUsingId($id); //log user in through Auth ServiceProvider
        $data = $user->queryUserBasicInfo($id);
        return Redirect::intended('dash.main');
    } else {
        return Redirect::route('login.form');
    }
});

Route::any('dash.main', function() {
    return Redirect::route('dash.main');
});


//testing route
//changeable to suite testing needs, test POST and GET data.

Route::any('test/daily/digest/{id}', [
    'uses' => 'UserController@testDailyDigest'
]);
Route::any('send/all/the/miserable/digests', [
    'uses' => 'UserController@sendAllDigests'
]);


Route::any('send/single/digest/{id}', [
   'uses' => 'UserController@sendDailyDigestEmailNow' 
]);
Route::any('import/all/data/magically', [
   'uses' => 'ImportController@importEverythingNow' 
]);
Route::any('import/adminIds', ['uses' => 'ImportController@adminIdToCompanies']);
Route::any('import/companyIds', ['uses' => 'ImportController@AddOldCompanyToUsers']);
Route::any('import/notifications', ['uses' => 'ImportController@addNotificationsForAllUsers']);

Route::any('reports/activeUsers', ['uses' => 'ImportController@getPayingMembers']);
Route::any('import/update/owner/records', ['uses' => 'ImportController@updateOwnerUserRecords']);

Route::any('vendorimport', array(
    'uses' => 'pdsMarketplaceController@importVendorsFromCsv',
    'as' => 'import.vendors.csv'
));
Route::any('marketplaceimport', array(
    'uses' => 'pdsMarketplaceController@importVendorsFromJson',
    'as' => 'import.vendors.json'
));
Route::any('completedlistingsimport', array(
    'uses' => 'pdsMarketplaceController@importFromCompletedListings',
    'as' => 'import.vendors.db'
));
Route::any('asscompaniesandusers', array(
    'uses' => 'ImportController@assUsersWithCompanies',
    'as' => 'import.ass.companies'
));

Route::any('addEmployeeTypeToUsers', [
    'uses' => 'ImportController@addEmployeeTypeToUsers'
]);

Route::any('pdsUserImport', array(
    'uses' => 'ImportController@importUsers',
    'as' => 'import.old.users'
));
Route::any('pdsTopicImport', array(
    'uses' => 'ImportController@importTopics',
    'as' => 'import.old.topics'
));
Route::any('pdsMessageImport', [
    'uses' => 'ImportController@importMessages',
    'as' => 'import.old.messages'
]);
Route::any('pdsCompanyImport', [
    'uses' => 'ImportController@importCompanies',
    'as' => 'import.old.companies'
]);
Route::any('makeUsers', [
    'uses' => 'ImportController@makeUsers',
    'as' => 'make.new.users'
]);
Route::any('makeTopics', [ 
    'uses' => 'ImportController@makeTopics',
    'as' => 'make.new.topics'
]);
Route::any('makeMessages', [ 
    'uses' => 'ImportController@makeMessages',
    'as' => 'make.new.messages'
]);



  Route::get('testing', array(
  'uses' => 'UserController@testing',
  'as' => 'test.route'
  ));

// ======================
// FORUMS SECTION
// ======================
Route::group(['prefix' => 'message-boards', 'before' => 'auth'], function() {
    Route::controller('message-boards', 'ForumController');
    // Forum Index
    Route::any('main', function() {
        return Redirect::route('forum.index');
    });
    Route::any('filtered/category', function() {
        return Redirect::route('forum.filter.cat');
    });
    Route::any('search/message-boards', function() {
        return Redirect::route('forum.search.boards');
    });
    
    Route::get('make/topic/featured/{id}', [
        'uses' => 'ForumController@makeTopicFeatured'
    ]);
    Route::get('make/topic/unfeatured/{id}', [
        'uses' => 'ForumController@makeTopicUnFeatured'
    ]);
    Route::get('/', [
        'uses' => 'ForumController@getIndex',
        'as' => 'forum.index'
    ]);
    
    Route::get('/private', [
        'uses' => 'ForumController@getPrivateForum',
        'as' => 'forum.index.private'
    ]);
    Route::get('/private/company', [
        'uses' => 'ForumController@getPrivateForum',
        'as' => 'forum.index.private'
    ]);
     Route::any('/private/company/{title}/{post_id}', [
        'uses' => 'ForumPostController@getForumPost',
        'as' => 'forum.post.private'
    ]);
     
    Route::any('private/search', [
        'uses' => 'ForumController@searchPrivateForum',
        'as' => 'forum.search.private.boards'
    ]);
    
    Route::get('/pharmacy/forum/{pharmacy}', [
        'uses' => 'ForumController@getPersonalForum',
        'as' => 'pharmacy.forum.index'
    ]);
    // Forum Filtered Index
    Route::any('category', [
        'uses' => 'ForumController@getIndex',
        'as' => 'forum.filter.cat'
    ]);
    // Forum Search Filter
    Route::any('search', [
        'uses' => 'ForumController@getSearchedIndex',
        'as' => 'forum.search.boards'
    ]);
    // Forum Discussion
    Route::any('{title}/{post_id}', [
        'uses' => 'ForumPostController@getForumPost',
        'as' => 'forum.post'
    ]);
    // Forum Create New Comment For Topic
    Route::any('create/new/message', [
        'uses' => 'ForumMessageController@createNewComment',
        'as' => 'create.new.message'
    ]);
    Route::any('create/new/topic', [
        'uses' => 'ForumController@createNewTopic',
        'as' => 'app.create.forum.topic'
    ]);
    // Forum get view to update comment
    Route::any('update/reply/{msg_id}', [
    'uses' => 'ForumMessageController@getEditComment',
        'as' => 'edit.topic.message'
    ]);
    
    Route::any('update/reply/{msg_id}?private=true', [
    'uses' => 'ForumMessageController@getEditComment',
        'as' => 'edit.topic.private.message'
    ]);
    
    // Forum update comment
    Route::any('update/user/reply/{msg_id}', [
        'uses' => 'ForumMessageController@updateTopicComment',
        'as' => 'update.topic.message'
    ]);
    // Forum remove category	
    Route::any('delete/category/{cat_id}', [
        'uses' => 'ForumController@destroyCategory',
        'as' => 'app.destroy.forum.category'
    ]);
    Route::any('delete/topic/{topic_id}', [
        'uses' => 'ForumController@destroyTopic',
        'as' => 'app.destroy.forum.topic'
    ]);
    Route::controller('post', 'ForumPostController');
});


//route group for resource library --app=pdsadvantage
Route::group(['prefix' => 'library', 'before' => 'auth'], function() {
    Route::controller('library', 'ResourceController');
    Route::any('/', [
        'uses' => 'ResourceController@getLibraryIndex',
        'as' => 'library.index'
    ]);
    Route::any('filtered/resource', function() {
        return Redirect::route('library.filtered');
    });
    Route::any('category', [
        'uses' => 'ResourceController@getFilteredForumIndex',
        'as' => 'library.filtered'
    ]);
    //download single resource
    Route::any('/resources/download/{filename}', [
        'uses' => 'ResourceController@show',
        'as' => 'download.resource'
    ]);
    
    //download multiple files
    Route::post('resources/multi-download/', array(
        'uses' => 'ResourceController@downloadMultiFiles',
        'as' => 'download.multiple.resources'
    ));
    //Control new resource post data
    Route::any('resources', [
        'uses' => 'ResourceController@store',
        'as' => 'app.store.resource'
    ])->before('auth');
    //download single resource
    Route::any('download/resource/{filename}', array(
        'uses' => 'ResourceController@show',
        'as' => 'app.download.resource'
    ));
    Route::any('resources/edit/{id}', [
        'uses' => 'ResourceController@getEditResource',
        'as' => 'app.edit.resource'
    ]);
    Route::any('edit/resource', [
        'uses' => 'ResourceController@updateResource',
        'as' => 'app.update.resource'
    ]);
    Route::any('update/resource/file/{id}', [
        'uses' => 'ResourceController@updateResourceFile',
        'as' => 'app.update.resource.file'
    ]);
    Route::any('remove/resource/{id}', [
        'uses' => 'ResourceController@destroy',
        'as' => 'app.destroy.resource'
    ]);
    Route::any('resource/share/preview/{id}', [
        'uses' => 'ResourceController@getSocialPreview',
        'as' => 'app.preview.resource'
    ]);
    Route::any('resource/wistia/{div}', [
        'uses' => 'ResourceController@wistia',
        'as' => 'app.embed.wistia'
    ]);
    Route::any('resource/trash/bin', [
        'uses' => 'ResourceController@getTrashBin',
        'as' => 'app.trashed.resource'
    ]);
    Route::any('restore/resource/{resource_id}', [
        'uses' => 'ResourceController@restoreResource',
        'as' => 'app.restore.resource'
    ]);

    Route::any('destroy/resource/{resource_id}', [
        'uses' => 'ResourceController@destroyForGood',
        'as' => 'app.obliterate.resource'
    ]);
});


// ======================
// EVENTS SECTION
// ======================
Route::group(['prefix' => 'events', 'before' => 'auth'], function() {
    Route::controller('events', 'EventsController');
    // Forum Index
    Route::any('/', [
        'uses' => 'EventsController@getIndex',
        'as' => 'events.index'
    ]);
    Route::any('/add/event', [
        'uses' => 'EventsController@getAddNewEventsView',
        'as' => 'get.add.event'
    ]);
    Route::any('/update/event/{id}', [
        'uses' => 'EventsController@getUpdateIndex',
        'as' => 'get.update.event'
    ]);
    Route::any('/filtered/by/{id}/{event}', [
        'uses' => 'EventsController@getFilteredEventIndex',
        'as' => 'get.filtered.index'
    ]);
    Route::any('/calendar', [
        'uses' => 'EventsController@getCalendarIndex',
        'as' => 'get.calendar.index'
    ]);
    Route::any('/post/add/event', [
        'uses' => 'EventsController@postAddNewEvent',
        'as' => 'app.add.event'
    ]);
    Route::any('update/event', [
        'uses' => 'EventsController@updateEventInformation',
        'as' => 'app.update.event'
    ]);
    Route::any('remove/event', [
        'uses' => 'EventsController@deleteEvent',
        'as' => 'app.delete.event'
    ]);
    Route::any('view/{id}/{event}', [
        'uses' => 'EventsController@getEventIndex',
        'as' => 'app.view.event'
    ]);
    Route::any('search/events', [
        'uses' => 'EventsController@getSearchedEventsIndex',
        'as' => 'app.search.events'
    ]);
    Route::any('get/all/events/', [
        'uses' => 'EventsController@getEventsForCalendar',
        'as' => 'app.get.events'
    ]);
});



Route::get('user/login/safe', array(
    'uses' => 'UserController@showCaptchaLogin',
    'as' => 'captcha.login'
));

Route::controller('testing', 'UserController');

//post route for finding a users hubspot account when they are registering.
Route::group(array('before' => 'auth'), function() {
    Route::any('grantlast/achievement/{id}', function($id) {
        $grantAchievement = \UserAchievement::addAchievement($id, 9);
        echo $grantAchievement;
        echo 'You really did it now!';
    });

    Route::post('ajax/notification/hide', 'UserController@hideNotification')->before('csrf');
    Route::post('ajax/notification/add', 'UserController@addNotification')->before('csrf');
});

// ======================
// LOGIN/SIGN UP SECTION
// ======================
// Show the login page
Route::get('login', [
    'as' => 'login.form',
    'uses' => 'UserController@showLogin'
])->before('guest');

//Process the login data
Route::post('login', 'UserController@login');

Route::post('ajax/login/check-email', function() {
    $data = Input::all();
    if (Request::ajax()) {
        //if success, must mean the ajax data came through!
        if ($data['email']) {
            $email = $data['email'];
            $user = User::where('email', $email)->first();
            if (is_object($user)) {
                if($user->imported == 1 && $user->confirmed_user == 0) {
                    $name = $user->fname.' '.$user->lname;
                    return \User::sendConfirmationRequestForImportedUser($user);
                }
                $name = $user->fname;
                $hello = '<div class="small-padding;"><div class="alert alert alert-success hello" data-dismiss="alert">Hello again ' . $name . '!</div></div>';
                return $hello;
            } else {
                $signup = 'signuptime';
                $signup .= '<div class="small-padding;"><div class="alert alert alert-success hello" data-dismiss="alert">We don\'t recognize your email address, that means you must be signing up for a new account! Please fill out all the information below using your work email.</div></div>';
                return $signup;
            }
        }
        return;
    }
})->before('csrf');

//post route for finding a users hubspot account when they are registering.
Route::post('ajax/login/hs-acc-finder', function() {
    $data = Input::all();
    if (Request::ajax()) {
        //if success, must mean the ajax data came through!
        if ($data['email']) {
            $user = new User;
            $hsAcc = $user->checkForHubspotAccount($data['email']); //check to see if user has a hubspot account
            if ($hsAcc === false) {
                $return_results = array(
                    'acc-search-fail' => 'no-acc'
                );
                return json_encode($return_results);
            } else { // if user has hs account
                return $user->hubspotAccHandle($data);
            }
        } else {
            return 0;
        }
    }
})->before('csrf');


// ======================
// EMAIL CONFIRMATION SECTION
// ======================
//show email confirmation page
Route::get('confirm/email/{token}/{id}', array(
    'uses' => 'UserController@getConfPage',
    'as' => 'confirm.receiver'
));

//Process email confirmation token. User receives this url/{token} combo in their email.
Route::post('confirm/email/{token}/{id}', array(
    'uses' => 'UserController@checkConfToken',
    'as' => 'confirm.email'
));

Route::post('confirm/email/manual', array(
    'uses' => 'UserController@manualVerifyUser',
    'as' => 'confirm.email.manual'
));

Route::any('digest/daily/{id}', array(
    'uses' => 'UserController@getDailyDigest',
    'as' => 'get.daily.digest'
));

Route::any('digest/weekly/{id}/{date}', array(
    'uses' => 'UserController@getWeeklyDigest',
    'as' => 'get.weekly.digest'
));

// =====================
// PDS CONFERENCE SIGN UP
// =====================
Route::group(array('prefix' => 'conference', 'before' => 'auth'), function () {
    Route::any('signup', array(
        'uses' => 'ConferenceController@index',
        'as' => 'conference.signup'
    ));
    Route::post('signup', array(
        'uses' => 'ConferenceController@createNewListing',
        'as' => 'conference.create.market-listing'
    ));
    Route::get('signup/create/conference-guide-entry', array(
        'uses' => 'ConferenceController@indexGuideEntry',
        'as' => 'conference.show.guide-entry'
    ));
    Route::post('signup/create/conference-guide-entry', array(
        'uses' => 'ConferenceController@createNewGuideEntry',
        'as' => 'conference.create.guide-entry'
    ));
    Route::get('signup/create/conference-guide-ad', array(
        'uses' => 'ConferenceController@indexGuideAd',
        'as' => 'conference.show.guide-ad'
    ));
    Route::post('signup/create/conference-guide-ad', array(
        'uses' => 'ConferenceController@createNewGuideAd',
        'as' => 'conference.create.guide-ad'
    ));
    Route::get('signup/create/conference-giveaway-item', array(
        'uses' => 'ConferenceController@indexRaffle',
        'as' => 'conference.show.raffle'
    ));
    Route::post('signup/create/conference-giveaway-item', array(
        'uses' => 'ConferenceController@createNewGiveawayItem',
        'as' => 'conference.new.giveaway.item'
    ));
    Route::get('signup/distribute/free-tickets', array(
        'uses' => 'ConferenceController@indexVipTickets',
        'as' => 'conference.vip-tickets'
    ));
    Route::post('signup/add/vip-tickets', array(
        'uses' => 'ConferenceController@createNewVipTicketEntries',
        'as' => 'conference.add.vip-tickets'
    ));
    Route::get('signup/choose/sponsorship-opportunities', array(
        'uses' => 'ConferenceController@indexSponsorshipOpportunities',
        'as' => 'conference.choose.sponserships'
    ));
    Route::post('signup/record/sponsorship-choices', array(
        'uses' => 'ConferenceController@createNewSponsorshipChoices',
        'as' => 'conference.add.sponsorships'
    ));
    Route::any('signup/get/faq', array(
        'uses' => 'ConferenceController@indexFaq',
        'as' => 'conference.get.faq'
    ));
    Route::any('signup/get/faq/sheet', [
        'uses' => 'ConferenceController@indexFaqSheet',
        'as' => 'conference.get.faqsheet'
    ]);
    Route::any('signup/get/hours', array(
        'uses' => 'ConferenceController@indexConferenceHours',
        'as' => 'conference.get.hours'
    ));
    Route::any('signup/get/hotel', array(
        'uses' => 'ConferenceController@indexConferenceHotel',
        'as' => 'conference.get.hotel'
    ));
    Route::any('vendors/subcategories/{id}', array(
        'uses' => 'ConferenceController@getVendorSubCategories',
        'as' => 'conference.get.subcategories'
    ));
});
Route::controller('conference', 'ConferenceController');

// =====================
// PDS MARKETPLACE
// =====================
Route::group(['prefix' => 'marketplace'], function () {
    Route::get('/', array(
        'uses' => 'MarketplaceController@index',
        'as' => 'marketplace.index'
    ));
    Route::any('company/list', function() {
        return Redirect::to('/marketplace');
    }); 
    Route::get('company/{company_id}/{company_name}', array(
        'uses' => 'MarketplaceController@indexCompany',
        'as' => 'marketplace.company'
    ));
    Route::get('company/new_vendor/listing/{company_id}/{company_name}', [
        'uses' => 'MarketplaceController@indexNewCompany',
        'as' => 'marketplace.real.company'
    ]);
    Route::get('company/json/list', array(
        'uses' => 'MarketplaceController@create',
        'as' => 'company.json'
    ));
    Route::any('company/filter/vendor/listing', [
        'uses' => 'MarketplaceController@getFilteredListing',
        'as' => 'filter.market.listing'
    ]);
    Route::controller('company', 'MarketplaceController');
});

Route::any('/user/{id}/{name}', array(
    'uses' => 'UserController@showUserProfileById',
    'as' => 'user.profile'
))->before('auth');


Route::any('/user/{id}/update/permissions/', [
    'uses' => 'UserController@showUpdatePermissions',
    'as' => 'manually.update.user.permissions'
])->before('auth');
Route::any('/user/post/updated/permissions', [
    'uses' => 'UserController@updatePermissions',
    'as' => 'update.permissions'
])->before('auth');



//Shows dash blocker if user hasn't confirmed the email 
Route::get('/dashboard/blocked/{email}', array(
    'uses' => 'UserController@showDash',
    'as' => 'dash.blocker'
));


//sends another confirmation email if user indicates they did not receive one.
Route::post('/dashboard/block/{email}', array(
    'uses' => 'UserController@sendConfirmation',
    'as' => 'dash.blocker.post'
));


// ======================
// DASHBOARD SECTION
// ======================
// Set up group prefix for dashboard panel
Route::group(array('prefix' => 'dashboard', 'before' => 'auth'), function() {
    Route::any('hscompany/{id}', [ 'uses' => 'UserController@getHSCompany']);
    Route::get('/', array(
        'uses' => 'UserController@showDash',
        'as' => 'dash.main'
    ));
    
    Route::any('/users/add', [
        'uses' => 'UserController@addNewUser',
        'as' => 'add.new.user'
    ]);
    Route::any('main', function() {
        return Redirect::route('dash.main');
    });
    
    Route::post('main/update-info/{info}', array(
        'uses' => 'AccountsController@update',
        'as' => 'basic.info.update'
    ));
    Route::any('projects/{id}', array(
        'uses' => 'TeamworkController@index',
        'as' => 'get.project.management'
    ));
    Route::any('projects/view/company/{companyId}', [
        'uses' => 'TeamworkController@getProjectManagement',
        'as' => 'get.pharmacy.project'
    ]);
    Route::any('projects/add/project', [
        'uses' => 'TeamworkController@getAddNewProject',
        'as' => 'get.add.new.project'
    ]);
    Route::any('projects/add/milestone', [
        'uses' => 'TeamworkController@getAddNewMilestone',
        'as' => 'get.add.new.milestone'
    ]);
    Route::any('projects/add/task', [
        'uses' => 'TeamworkController@getAddNewTask',
        'as' => 'get.add.new.task'
    ]);
    Route::any('projects/thank-you', [
        'uses' => 'TeamworkController@getForumSubmitThankYou',
        'as' => 'get.thank.you.view'
    ]);
    Route::any('projects/register/company', [
        'uses' => 'TeamworkController@postCreateNewCompany',
        'as' => 'create.new.teamwork.company'
    ]);
    Route::any('projects/register/pds/company', [
        'uses' => 'TeamworkController@processUserTeamworkAccessAndAccountData',
        'as' => 'create.new.teamwork.company.account'
    ]);
    Route::any('projects/register/user', [
        'uses' => 'TeamworkController@postCreateNewTwUser',
        'as' => 'create.new.teamwork.user'
    ]);
    Route::any('projects/create/user/for/company', [
        'uses' => 'TeamworkController@createNewTeamworkEmployeeUser',
        'as' => 'create.new.company.employee'
    ]);
    Route::any('projects/approve/employee/for/myPDS/{id}', [
        'uses' => 'TeamworkController@updateEmployeeTwCompanyId',
        'as' => 'add.employee.to.mypds'
    ]);
    Route::any('projects/create/project', [
        'uses' => 'TeamworkController@postCreateNewTwProject',
        'as' => 'create.new.project'
    ]);
    Route::any('projects/create/milestone', [
        'uses' => 'TeamworkController@postCreateTwProjectMilestone',
        'as' => 'create.new.milestone'
    ]);
    Route::any('projects/ajax/milestones', [
        'uses' => 'TeamworkController@postCreateNewTwUser',
        'as' => 'ajax.teamwork.milestones'
    ]);
    Route::any('projects/create/task', [
        'uses' => 'TeamworkController@postCreateNewTwTask',
        'as' => 'create.new.task'
    ]);
    Route::any('projects/add/message', [
        'uses' => 'TeamworkController@getAddNewMessage',
        'as' => 'get.add.new.message'
    ]);
    Route::any('projects/create/message', [
        'uses' => 'TeamworkController@postCreateNewTwMessage',
        'as' => 'tw.create.new.message'
    ]);
    Route::controller('projects', 'TeamworkController');
    Route::any('dash/support', array(
        'uses' => 'SupportController@indexSupport',
        'as' => 'dash.get.support'
    ));
    Route::any('update/listing/to/featured', [
        'uses' => 'AccountsController@updateToFeatured',
        'as' => 'update.listing.featured'
    ]);
    Route::any('update/dan/quote', [
        'uses' => 'AccountsController@updateDirectlyFromDanQuote',
        'as' => 'update.dan.quote'
    ]);
    Route::any('approve/newUsers', [
        'uses' => 'UserController@showNewUsers',
        'as' => 'show.new.users'
    ]);
    Route::any('approve/newUsers/{email}', [
        'uses' => 'UserController@showNewUsers',
        'as' => 'show.new.users'
    ]);
    Route::any('approve/newUsers/{id}/{accessLevel}', [
        'uses' => 'UserController@approveNewUser',
        'as' => 'approve.new.users'
    ]);
    Route::resource('main/update-info', 'AccountsController');
    /**
     * RESOURCES
     */
    //Show resources administration view
    Route::get('resources', array(
        'uses' => 'ResourceController@index',
        'as' => 'dash.resources'
    ))->before('auth');
    //Control resource post data
    Route::post('resources', array(
        'uses' => 'ResourceController@store',
        'as' => 'store.resource'
    ))->before('auth');
    //download single resource
    Route::any('resources/show/{filename}', array(
        'uses' => 'ResourceController@show',
        'as' => 'download.resource'
    ));
    Route::any('employees/update', array(
        'uses' => 'UserController@saveEmployees',
        'as' => 'save.pds.employees'
    ));
    
    
    
    //update a single resource's information
    Route::post('resources/update/file/{file_id}', array(
        'uses' => 'ResourceController@updateFileInfo',
        'as' => 'update.file.info'
    ));
    //delete a resource
    Route::any('resources/{id}', array(
        'uses' => 'ResourceController@destroy',
        'as' => 'delete.resource'
    ))->before('auth');
    //display recent deletes, give ability to undo the deletes to admins
    Route::get('resources/softdeletes/list/index', array(
        'uses' => 'ResourceController@indexTrashcan',
        'as' => 'resource.trashbin'
    ));
    Route::any('resources/softdeletes/list/resurrect', array(
        'uses' => 'ResourceController@destroyReversal',
        'as' => 'resource.resurrect'
    ));
    Route::any('resources/softdeletes/list/destroy', array(
        'uses' => 'ResourceController@destroyForGood',
        'as' => 'resource.decimate'
    ));
    //display table data --> ajax controlled
    Route::any('resources/tabledata/{id}', array(
        'uses' => 'ResourceController@showTableData',
        'as' => 'retrieve.table'
    ));
    //download multiple files
    Route::post('resources/download/download-multi/{file_name}', array(
        'uses' => 'ResourceController@showDownloadMulti',
        'as' => 'download.multi.files'
    ));
    Route::any('resource/add/new/container', [
        'uses' => 'ResourceController@getAddResourceGroup',
        'as' => 'add.resource.group'
    ]);
    Route::any('resource/add/container', [
        'uses' => 'ResourceController@storeNewResourceGroup',
        'as' => 'insert.resource.group'
    ]);
    Route::resource('resources', 'ResourceController'); //resource for Resources :D
    /**
     * LOGOUT
     */
    //logout of the dashboard
    Route::get('logout', array('as' => 'dash.logout', function() {
            Session::flush(); //flush session variables into the void
            Auth::logout();   //log user out of authentication
            $unset_cookie = Cookie::forget('company'); //remove auto login cookie from browser
            return Redirect::route('login.form')->withCookie($unset_cookie);
        }));
    /**
     * GROUP ADMIN
     */
    //access control list admin section
    Route::get('group/index', array(
        'uses' => 'GroupController@showIndex',
        'as' => 'acl.control'
    ))->before('auth');
    //shows list of groups
    Route::get('group/add', array(
        'uses' => 'GroupController@showAdd',
        'as' => 'acl.addgroups'
    ))->before('auth');
    //add new group
    Route::post('group/add', array(
        'uses' => 'GroupController@addGroup',
        'as' => 'acl.addgrouprequest'
    ));
    //edit user groups
    Route::any('group/edit', array(
        'uses' => "GroupController@edit",
        'as' => 'acl.editor'
    ))->before('auth');
    //delete user groups
    Route::any('group/delete', array(
        'uses' => "GroupController@delete",
        'as' => 'acl.delete'
    ))->before('auth');
    Route::any('group/edit/resource_group', [
        'uses' => 'GroupController@editResourceGroup',
        'as' => 'acl.resource.group.edit'
    ]);
    Route::resource('group', 'GroupController');
    /**
     * User Accounts 
     */
    Route::get('accounts', array(
        'uses' => 'AccountsController@showAccounts',
        'as' => 'accounts.index'
    ));
    Route::any('accounts/confirmations', array(
        'uses' => 'AccountsController@showAccountsConfirmations',
        'as' => 'accounts.index.confirmed'
    ));
    Route::any('accounts/refresh/{id}', [
        'uses' => 'AccountsController@refreshUserPermissions',
        'as' => 'accounts.refresh.user'
    ]);

    Route::any('accounts/search/users', [
        'uses' => 'AccountsController@showSearchedAcc',
        'as' => 'accounts.search.users'
    ]);
    Route::any('accounts/search/users/{id}', [
        'uses' => 'AccountsController@showSearchedAcc',
        'as' => 'accounts.search.users'
    ]);
    Route::any('accounts/summary/downloadGuideAd/{filename}', array(
        'uses' => 'ConferenceController@downloadGuideAd',
        'as' => 'accounts.summary.guide.ad.download'
    ));
    Route::any('accounts/summary/downloadMarketplaceLogo/{filename}', array(
        'uses' => 'ConferenceController@downloadMarketplaceLogo',
        'as' => 'accounts.summary.marketplace.logo.download'
    ));
    Route::any('accounts/summary/downloadGuideLogo/{filename}', array(
        'uses' => 'ConferenceController@downloadGuideLogo',
        'as' => 'accounts.summary.guide.logo.download'
    ));
    Route::any('accounts/summary/{id}', array(
        'uses' => 'AccountsController@showAccountSummary',
        'as' => 'accounts.summary'
    ));
    Route::any('accounts/summary/{id}/downloadFile/{filename}', array(
        'uses' => 'ConferenceController@downloadFile',
        'as' => 'accounts.summary.download'
    ));
    Route::any('accounts/destroy/account/{id}', [
        'uses' => 'AccountsController@destroy',
        'as' => 'destroy.account'
    ]);
    Route::any('accounts/update/listing/info', [
        'uses' => 'AccountsController@updateVendorAccount',
        'as' => 'update.listing.info'
    ]);
    Route::any('accounts/approve/user/{id}', [
        'uses' => 'AccountsController@updateToApproved',
        'as' => 'approve.listing'
    ]);
    Route::any('accounts/confirm/user/{id}', [
        'uses' => 'AccountsController@updateToConfirmed',
        'as' => 'update.to.confirmed'
    ]);
    Route::any('accounts/ghost/user/{user_id}', [
        'uses' => 'AccountsController@ghostUser',
        'as' => 'ghost.user'
    ]);
    Route::any('accounts/unghost/user/{ghoster_id}', [
        'uses' => 'AccountsController@unghostUser',
        'as' => 'unghost.user'
    ]);
    
    
    Route::controller('accounts', 'AccountsController');

    /**
     * Forum Manager
     */
    Route::get('manage/pds-forum', [
        'uses' => 'ForumController@getManagerIndex',
        'as' => 'forum.manager.index'
    ]);
    Route::any('manage/pds-forum/new/category', [
        'uses' => 'ForumController@createNewCategory',
        'as' => 'create.forum.category'
    ]);
    Route::any('manage/pds-forum/update/category/{cat_id}', [
        'uses' => 'ForumController@getCategoryInfo',
        'as' => 'post.update.forum.category'
    ]);
    Route::any('manage/pds-forum/update/category/update/record', [
        'uses' => 'ForumController@updateCategoryInfo',
        'as' => 'update.forum.category'
    ]);
    Route::any('manage/pds-forum/update/topic/{id}', [
        'uses' => 'ForumController@getTopicUpdate',
        'as' => 'edit.forum.topic'
    ]);
    Route::any('manage/pds-forum/update/featured/topic', [
        'uses' => 'ForumController@updateTopicFeature',
        'as' => 'update.forum.featured'
    ]);
    Route::any('manage/pds-forum/delete/category/{cat_id}', [
        'uses' => 'ForumController@destroyCategory',
        'as' => 'destroy.forum.category'
    ]);
    
    Route::any('manage/pds-forum/new/topic', [
        'uses' => 'ForumController@createNewTopic',
        'as' => 'create.forum.topic'
    ]);
    
    Route::any('manage/pds-forum/delete/topic/{topic_id}', [
        'uses' => 'ForumController@destroyTopic',
        'as' => 'destroy.forum.topic'
    ]);
    Route::any('manage/pds-forum/remove/restriction', [
        'uses' => 'ForumController@updateCategoryRestriction',
        'as' => 'update.forum.category.restriction'
    ]);
    Route::controller('manage', 'ForumController');
    
    /**
     * PASSWORD MANAGER 
     */
    //password reset handling
    Route::get('change/settings', array(
        'uses' => 'RemindersController@getRemind',
        'as' => 'password.remind'
    ))->before('auth');
    //sends reset email
    Route::post('change/settings', array(
        'uses' => 'RemindersController@postRemind',
        'as' => 'password.request'
    ));
    Route::resource('change', 'RemindersController');
    
    
    Route::any('vendors/submitted', [
        'uses' => 'pdsMarketplaceController@displayUploadedMarketplaceListings',
        'as' => 'display.old.vendors'
    ]);
    
    /**
     * COMPANY MANAGER
     */
    
    Route::any('create/new/pharmacy', [
        'uses' => 'CompanyController@store',
        'as' => 'store.company'
    ]);
    Route::any('pharmacies/get/related/companies', [
        'uses' => 'CompanyController@getRelatedCompanies',
        'as' => 'ajax.search.companies'
    ]);
    Route::any('add/user/to/{companyId}', [ 
        'uses' => 'CompanyController@addUserForApproval'
    ]);
    Route::any('vendor/setup/{id}', [ 
        'uses' => 'CompanyController@setVendor',
        'as' => 'set.vendor'
    ]);
    Route::any('pharmacy/profile/view/{userId}', [
        'uses' => 'CompanyController@index',
        'as' => 'pharmacy.profile.index'
    ]);
    Route::any('approve/pharmacy/employee/{id}/{company_id}/{employee_type}', [
        'uses' => 'CompanyController@updateEmployeeApprovalStatus',
        'as' => 'update.employee.approval.status'
    ]);
    Route::any('deny/employee/access/{id}', [
        'uses' => 'CompanyController@resetUserCompanies',
        'as' => 'deny.employee.request'
    ]);
    Route::any('update/employee/to/admin/{employeeId}/{companyId}', [
        'uses' => 'CompanyController@updateEmployeeToAdmin',
        'as' => 'swap.admins'
    ]);
    Route::any('test/ajax/route', [ 'uses' => 'CompanyController@getRelatedCompanies']);
    
    Route::controller('pharmacies', 'CompanyController');
});
Route::any('activate/save/password', [
    'uses' => 'UserController@changeImportedPassword',
    'as' => 'signup.save.password'
])->before('csrf');
//handles showing form to reset
Route::get('change/settings/{token}', array(
    'uses' => 'RemindersController@getReset',
    'as' => 'password.reset'
));
//handles resetting
Route::post('change/settings/{token}', array(
    'uses' => 'RemindersController@postReset',
    'as' => 'password.update'
));
/**
 * PASSWORD RESET REQUEST 
 */
Route::get('retrieve/password', array(
    'uses' => 'RemindersController@getForgotPassIndex',
    'as' => 'remind.pass'
));

Route::post('retrieve/password', array(
    'uses' => 'RemindersController@postRemind',
    'as' => 'remind.handle'
));

Route::any('retrieve/password/confirmation/{token}', [
    'uses' => 'RemindersController@postConfirmationRemind',
    'as' => 'remind.confirm'
]);
Route::controller('/retrieve', 'RemindersController');

// =====================
// 404
// =====================


App::missing(function($exception) {
    //shows error page from views
    return Response::view('error', array(), 404);
});


// =====================
// Backwards Compatibility
// =====================
// Hi Derek Jeter

Route::get('confirm/email/{token}', function() {
    return Redirect::route('login.form');
});

// =====================
// Gamify
// =====================

Route::group(['before' => 'auth'], function() {
    Route::resource('badges', 'BadgeController');
    Route::resource('achievements', 'AchievementController');
    Route::resource('pdsmarketplace', 'pdsMarketplaceController');
    Route::resource('user-achievements', 'UserAchievementController');
    Route::resource('access-groups', 'AccessGroupController');
    Route::resource('companies', 'CompanyController');
    Route::any('getting-started', [
        'uses' => 'SupportController@showGettingStarted',
        'as' => 'getting.started'
    ]);
});



