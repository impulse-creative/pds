<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* ======================
 * Set up global variables for all views
 * ======================
*/
//-->id of logged in user

Route::filter('getSubdomain', function($route, $request) 
{
    $host = $request->getHost();
    $parts = explode('.', $host);
    $subdomain = $parts[0];

    // Store subdomain in session
    Session::put('subdomain', $subdomain);

});


if(Auth::check())
{
	View::share('id', Auth::id());
	$hs_acc = User::findOrFail(Auth::id());
	View::share('hs_acc', $hs_acc->hs_acc_number);
}


Route::model('login', 'User');
Route::controller('login', 'UserController');
//subdomain routing
/*
Route::group(array('domain' => '{sub}.pharmacyowners.com'), function($sub) {
	Route::get('/login', array(
	        'uses' => 'UserController@showLogin',
        	'as' => 'login.form'
	))->before('guest');
echo 'smoiles';
});
*/
 // Works -- but has /login etc after domain
Route::group(array('domain' => 'login.pharmacyowners.com'), function() {
    Route::get('/', function() {
        // ...
        return Redirect::to('http://login.pharmacyowners.com/login');
    });

});
Route::group(array('domain' => 'marketplace.pharmacyowners.com'), function() {
    Route::get('/', function() {
        // ...
        echo 'shooot';
        return Redirect::to('http://marketplace.pharmacyowners.com/company/list');
    });

});




//landing page
Route::get('/', function()
{
	/**
	 * When they reach the home page, we will check to see if their 
	 * remember cookie is in place, then log them in.
	 */

	$cookie = Cookie::get('company');
	if(isset($cookie)){
		$user = new User;
    	$userObj = $user->queryUserId($cookie);
    	$user->handleSessionCreation($userObj);
    	$id = User::getUserId();
    	Auth::loginUsingId($id); //log user in through Auth ServiceProvider
    	$data = $user->queryUserBasicInfo($id);
		return View::make('dash-main', array('data' => $data));
	}else {
		return Response::make('Home Page');
	}
});


//testing route
//changeable to suite testing needs, test POST and GET data.
Route::get('testing', array(
	'uses' => 'UserController@testing',
	'as' => 'test.table.set'
));



Route::get('testing/login/safe', array(
	'uses' => 'UserController@showCaptchaLogin',
	'as'   => 'captcha.login'
));

Route::controller('testing', 'UserController');

//post route for finding a users hubspot account when they are registering.
Route::post('ajax/login/hs-acc-finder', function()
{
    $data = Input::all();
    if(Request::ajax())
    {
        //if success, must mean the ajax data came through!
        if($data['email']){
			$user = new User;
			$hsAcc = $user->checkForHubspotAccount($data['email']); //check to see if user has a hubspot account
			if($hsAcc === false) {
                $return_results = array(                    
                    'acc-search-fail' => 'no-acc'
                );
				return json_encode($return_results);
			}else 
			{ // if user has hs account
				return $user->hubspotAccHandle($data);
			}
        }
        //if not success
        else{
            return 0;
        }
    }
})->before('csrf');

// ======================
// LOGIN/SIGN UP SECTION
// ======================
// Show the login page
Route::get('login', array(
	'uses' => 'UserController@showLogin',
	'as' => 'login.form'
))->before('guest');

//Process the login data
Route::post('login', 'UserController@login');


// ======================
// EMAIL CONFIRMATION SECTION
// ======================

//show email confirmation page
Route::get('confirm/email/{token}', array(
	'uses' => 'UserController@getConfPage',
	'as' => 'confirm.receiver'
));

//Process email confirmation token. User receives this url/{token} combo in their email.
Route::post('confirm/email/{token}', array(
	'uses' => 'UserController@checkConfToken',
	'as' => 'confirm.email'
));

// =====================
// PDS CONFERENCE SIGN UP
// =====================
Route::group(array('prefix' => 'conference', 'before' => 'auth'), function ()
{
	Route::any('signup', array(
		'uses' => 'ConferenceController@index',
		'as' => 'conference.signup'
	));
	Route::post('signup', array(
		'uses' => 'ConferenceController@createNewListing',
		'as' => 'conference.create.market-listing'
	));
	Route::get('signup/create/conference-guide-entry', array(
		'uses' => 'ConferenceController@indexGuideEntry',
		'as' => 'conference.show.guide-entry'
	));
	Route::post('signup/create/conference-guide-entry', array(
		'uses' => 'ConferenceController@createNewGuideEntry',
		'as' => 'conference.create.guide-entry'
	));
	Route::get('signup/create/conference-guide-ad', array(
		'uses' => 'ConferenceController@indexGuideAd',
		'as' => 'conference.show.guide-ad'
	));
	Route::post('signup/create/conference-guide-ad', array(
		'uses' => 'ConferenceController@createNewGuideAd',
		'as' => 'conference.create.guide-ad'
	));
	Route::get('signup/create/conference-giveaway-item', array(
		'uses' => 'ConferenceController@indexRaffle',
		'as' => 'conference.show.raffle'
	));	
	Route::post('signup/create/conference-giveaway-item', array(
		'uses' => 'ConferenceController@createNewGiveawayItem',
		'as' => 'conference.new.giveaway.item'
	));
	Route::get('signup/distribute/vip-tickets', array(
		'uses' => 'ConferenceController@indexVipTickets',
		'as' => 'conference.vip-tickets'
	));
	Route::post('signup/add/vip-tickets', array(
		'uses' => 'ConferenceController@createNewVipTicketEntries',
		'as' => 'conference.add.vip-tickets'
	));
	Route::get('signup/choose/sponsorship-opportunities', array(
		'uses' => 'ConferenceController@indexSponsorshipOpportunities',
		'as' => 'conference.choose.sponserships'
	));
	Route::post('signup/record/sponsorship-choices', array(
		'uses' => 'ConferenceController@createNewSponsorshipChoices',
		'as' => 'conference.add.sponsorships'
	));
});
Route::controller('conference', 'ConferenceController');

// =====================
// PDS MARKETPLACE
// =====================
Route::group(array('prefix' => 'marketplace'), function () {
	Route::get('company/list', array(
		'uses' => 'MarketplaceController@index',
		'as' => 'marketplace.index'
	));
	Route::get('company/{company_id}', array(
		'uses' => 'MarketplaceController@indexCompany',
		'as' => 'marketplace.company'
	));	
	Route::get('company/json/list', array(
		'uses' => 'MarketplaceController@create',
		'as' => 'company.json'
	));
	Route::controller('company', 'MarketplaceController');
});


//Shows dash blocker if user hasn't confirmed the email 
Route::get('/dashboard/blocked/{email}', array(
	'uses' => 'UserController@showDash',
	'as' => 'dash.blocker'
));

//sends another confirmation email if user indicates they did not receive one.
Route::post('/dashboard/block/{email}', array(
	'uses' => 'UserController@sendConfirmation',
	'as' => 'dash.blocker.post'
));

// ======================
// DASHBOARD SECTION
// ======================
// Set up group prefix for dashboard panel
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function () {
	//Show main dash
	Route::get('main', array(
		'uses' => 'UserController@showDash',
		'as' => 'dashboard'
	))->before('auth');
	//handle dash main post data -- this is for the ajax call to change basic profile information
	Route::post('main/update-info/{info}', array(
		'uses' => 'AccountsController@update',
		'as' => 'basic.info.update'
	));
	Route::resource('main/update-info', 'AccountsController');
	/**
	 * RESOURCES
	 */
	//Show resources administration view
	Route::get('resources', array(
		'uses' => 'ResourceController@index',
		'as' => 'dash.resources'
	))->before('auth');
	//Control resource post data
	Route::post('resources', array(
		'uses' => 'ResourceController@store',
		'as' => 'store.resource'
	))->before('auth');
	//download single resource
	Route::any('resources/show/{filename}', array(
		'uses' => 'ResourceController@show',
		'as' => 'download.resource'
	));
	//update a single resource's information
	Route::post('resources/update/file/{file_id}', array(
		'uses' => 'ResourceController@updateFileInfo',
		'as' => 'update.file.info'
	));
	//delete a resource
	Route::any('resources/{id}', array(
		'uses' => 'ResourceController@destroy',
		'as' => 'delete.resource'
	))->before('auth');
	//display recent deletes, give ability to undo the deletes to admins
	Route::get('resources/softdeletes/list/index', array(
		'uses' => 'ResourceController@indexTrashcan',
		'as' => 'resource.trashbin'
	));
	Route::any('resources/softdeletes/list/resurrect', array(
		'uses' => 'ResourceController@destroyReversal',
		'as' => 'resource.resurrect'
	));
	Route::any('resources/softdeletes/list/destroy', array(
		'uses' => 'ResourceController@destroyForGood',
		'as' => 'resource.decimate'
	));
	//display table data --> ajax controlled
	Route::any('resources/tabledata/{id}', array(
		'uses' => 'ResourceController@showTableData',
		'as' => 'retrieve.table'
	));
	//download multiple files
	Route::post('resources/download/download-multi/{file_name}', array(
		'uses' => 'ResourceController@showDownloadMulti',
		'as' => 'download.multi.files'
	));
	Route::resource('resources', 'ResourceController'); //resource for Resources :D
	/**
	 * LOGOUT
	 */
	//logout of the dashboard
	Route::get('logout', function() {
		Session::flush(); //flush session variables into the void
		Auth::logout();	  //log user out of authentication
		$unset_cookie = Cookie::forget('company'); //remove auto login cookie from browser
		return Redirect::to('/')->withCookie($unset_cookie);
	});
	/**
	 * GROUP ADMIN
	 */
	//access control list admin section
	Route::get('group/index', array(
		'uses' => 'GroupController@showIndex',
		'as' => 'acl.control'
	))->before('auth');
	//shows list of groups
	Route::get('group/add', array(
		'uses' => 'GroupController@showAdd',
		'as' => 'acl.addgroups'
	))->before('auth');
	//add new group
	Route::post('group/add', array(
		'uses' => 'GroupController@addGroup',
		'as' => 'acl.addgrouprequest'
	));
	//edit user groups
	Route::any('group/edit', array(
		'uses' => "GroupController@edit",
		'as' => 'acl.editor'
	))->before('auth');
	//delete user groups
	Route::any('group/delete', array(
		'uses' => "GroupController@delete",
		'as' => 'acl.delete'
	))->before('auth');
	Route::resource('group', 'GroupController');
	/**
	 * PASSWORD MANAGER 
	 */
	//password reset handling
	Route::get('change/settings', array(
		'uses' => 'RemindersController@getRemind',
		'as' => 'password.remind'
	))->before('auth');
	//sends reset email
	Route::post('change/settings', array(
		'uses' => 'RemindersController@postRemind',
		'as' => 'password.request'
	));
	//handles showing form to reset
	Route::get('change/settings/{token}', array(
		'uses' => 'RemindersController@getReset',
		'as' => 'password.reset'
	))->before('auth');
	//handles resetting
	Route::post('change/settings/{token}', array(
		'uses' => 'RemindersController@postReset',
		'as' => 'password.update'
	));
});


// =====================
// 404
// =====================
App::missing(function($exception) {
	//shows error page from views
	return Response::view('error', array(), 404);
});

