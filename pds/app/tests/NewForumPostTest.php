<?php

use Laracasts\TestDummy\Factory;
use League\FactoryMuffin\Facade as FactoryMuffin;

class NewForumPostTest extends TestCase {

    public function setUp()
    {
        parent::setUp();
        FactoryMuffin::define('EventStates', [
            'state' => 'string',
            'state_abreviation' => 'string',
            'state_id' => 'factory|EventStates'
        ]);
        $this->be(new User);      
    }
    
    public function testUserLogin()
    {
        $this->action('GET', 'UserController@login');
        
        $this->assertRedirectedToRoute('dash.main');
    }
    
    public function testNewEventState()
    {
        $post = FactoryMuffin::create('EventStates');
        
        $this->assertInstanceOf('EventStates', $message);
    }
    
    public function testIndex() {
        //$this->client->request('GET', '/whoever');
        //$this->get('/crazyness');
    }

}
