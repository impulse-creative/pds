<?php

use League\FactoryMuffin\Facade as FactoryMuffin;

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'local';

		return require __DIR__.'/../../bootstrap/start.php';
	}
        
        
        public function __call($method, $args)
        {
            if(in_array($method, ['get', 'post', 'put', 'patch', 'delete']))
            {
                return $this->call($method, $args[0]);
            }
            
            throw new BadMethodCallException;
        }
        
        public function setUp()
        {
            parent::setUp();
            $this->prepareForTests();
        }
        
        public function prepareForTests()
        {
            Mail::pretend(true);
        }
        
        public function tearDown()
        {
            parent::tearDown();
            
            //FactoryMuffin::deleteSaved();
        }
        
        /**
         *  Flush and reboot Eloquent model events
         * 
         * @return void
         */
        public function resetEvents()
        {
            foreach($this->getModels() as $model)
            {
                call_user_func([$model, 'flushEventListeners']);
                
                call_user_func([$model, 'boot']);
            }
        }
        
        /**
         *  Get the model names from their filename
         * 
         * @return array
         */
        public function getModels()
        {
            $files = File::files(base_path() . '/app/models');
            
            foreach ($files as $file)
            {
                $models[] = pathinfo($file, PATHINFO_FILENAME);
            }
            
            return $models;
        }

}
