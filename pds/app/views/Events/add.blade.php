@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/forum-styles.css') }}
	{{ HTML::style('css/events-styles.css') }}
	{{ HTML::style('css/bootstrap-datetimepicker.min.css') }}
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<div class="container events">
    <h1 class="small-padding">Add New Event</h1>
    <div class="add-events-container small-padding col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @if(Session::has('Success'))
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
        @endif
        @if(Session::has('Failure'))
            <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
        @endif
        {{ Form::open(['route' => 'app.add.event', 'class' => 'add-new-event', 'files' => true]) }}
        <div class="col-lg-12">
            <label for="title" class="form-label @if($errors->has('title')) red-text @endif">Event Title</label>
            @if($errors)
                @if($errors->has('title'))
                        <p class="red-text">{{ $errors->first('title', 'Please provide a title for your event.') }}</p>
                @endif
            @endif
            {{ Form::input('text', 'title', Input::old('title'), ['class' => 'form-control', 'placeholder' => 'Event Title: Success Event, Let Us Talk, etc..']) }}
        </div>
        <div class="col-lg-12 small-padding">
            <div class="is-featured-event-container col-lg-4">
                <label for="is_featured" class="form-label">Is this a featured event?&nbsp;
                    {{ Form::checkbox('is_featured', 1, false) }}
                </label>
            </div>
            <div class="is-member-only-event col-lg-4">
                <label for="is_member_only" class="form-label">Is this a members only event?&nbsp;
                    {{ Form::checkbox('is_member_only', 1, false) }}
                </label>
            </div>
            <div class="is-free-check col-lg-4">
                <label for="is_free">Is this a free event?&nbsp;
                    {{ Form::checkbox('is_free', '1')}}
                </label>
            </div>
        </div>


        <div id="price-details">
            @if($errors)
            <div class="pricing-errors">
                @if($errors->has('link_to_next_free'))
                        <p class="red-text">{{ $errors->first('link_to_next_free', 'Please provide a link for the free member to sign up.') }}</p>
                @endif
                @if($errors->has('per_advanced'))
                        <p class="red-text">{{ $errors->first('per_advanced', 'Please select a unit for Advanced Pricing.') }}</p>
                @endif
                @if($errors->has('per_elite'))
                        <p class="red-text">{{ $errors->first('per_elite', 'Please select a unit for Elite Pricing.') }}</p>
                @endif
                @if($errors->has('per_core'))
                        <p class="red-text">{{ $errors->first('per_core', 'Please select a unit for Core Pricing.') }}</p>
                @endif
                @if($errors->has('link_to_next_core'))
                        <p class="red-text">{{ $errors->first('link_to_next_core', 'Please provide a link for the core member to sign up.') }}</p>
                    @endif
                @if($errors->has('link_to_next_advanced'))
                    <p class="red-text">{{ $errors->first('link_to_next_advanced', 'Please provide a link for the advanced member to sign up.') }}</p>
                @endif
                @if($errors->has('link_to_next_elite'))
                    <p class="red-text">{{ $errors->first('link_to_next_elite', 'Please provide a link for the elite member to sign up.') }}</p>
                @endif
            </div>
            @endif
            <div class="price-info small-padding">
                <p class="purple-text">*Fill out the different price ranges below for each level of paid membership. If your event is for non-members, then you can just fill out 
                    the Non-Member price, select a 'Per' option, and then provide the link for signup (Eventbrite Link, Webinar URL, Landing Page, etc...). Please make sure all links start with http://</p>
            </div>
            <div class="free-event">
                <div class="input-group pricing col-lg-6">
                    <span class="input-group-addon">$</span>
                    <input disabled="disabled" name="free_price" value="0" type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Free">
                    <span class="input-group-addon">.00</span>

                    <span class="input-group-addon per">Per</span>

                    {{ Form::select('per_free', $units, 1, ['class' => 'form-control hidden'])}}
                </div>
                <div class="col-lg-6 small-padding">
                    {{ Form::input('text', 'link_to_next_free', Input::old('link_to_next_free'), ['class' => 'form-control', 'placeholder' => 'Please provide a link for the non-member to sign up. Eventbrite Link, Webinar URL, Landing Page, etc..']) }}
                </div>
            </div>
            <div class="paid-event">
                <div class="input-group pricing col-lg-6 non_member">
                    <span class="input-group-addon">$</span>
                    <input name="non_member_price" value="{{ Input::old('non_member_price') }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Non-Member">
                    <span class="input-group-addon">.00</span>

                    <span class="input-group-addon per">Per</span>

                    {{ Form::select('per_non_member', $units, Input::old('per_non_member'), ['class' => 'form-control'])}}
                </div>
                <div class="col-lg-5 small-padding non_member">
                    {{ Form::input('text', 'link_to_next_non_member', Input::old('link_to_next_free'), ['class' => 'form-control', 'placeholder' => 'Please provide a link for the non-member to sign up. Eventbrite Link, Webinar URL, Landing Page, etc..']) }}
                </div>

                <div class="input-group pricing col-lg-6">
                        <span class="input-group-addon">$</span>
                        <input name="core_price" value="{{ Input::old('core_price') }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Core">
                        <span class="input-group-addon">.00</span>

                        <span class="input-group-addon">Per</span>
                        {{ Form::select('per_core', $units, Input::old('per_core'), ['class' => 'form-control'])}}
                </div>
                <div class="col-lg-5 small-padding">
                    {{ Form::input('text', 'link_to_next_core', Input::old('link_to_next_core'), ['class' => 'form-control', 'placeholder' => 'Please provide a link for the core member to sign up. Eventbrite Link, Webinar URL, Landing Page, etc..']) }}
                </div>

                <div class="input-group pricing col-lg-6">
                        <span class="input-group-addon">$</span>
                        <input name="advanced_price" value="{{ Input::old('advanced_price') }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Advanced">
                        <span class="input-group-addon">.00</span>

                        <span class="input-group-addon">Per</span>

                        {{ Form::select('per_advanced', $units, Input::old('per_advanced'), ['class' => 'form-control'])}}
                </div>
                <div class="col-lg-5 small-padding">
                    {{ Form::input('text', 'link_to_next_advanced', Input::old('link_to_next_advanced'), ['class' => 'form-control', 'placeholder' => 'Please provide a link for the advanced member to sign up. Eventbrite Link, Webinar URL, Landing Page, etc..']) }}
                </div>
                <div class="input-group pricing col-lg-6">
                        <span class="input-group-addon">$</span>
                        <input name="elite_price" value="{{ Input::old('elite_price') }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Elite">
                        <span class="input-group-addon">.00</span>

                        <span class="input-group-addon per">Per</span>

                        {{ Form::select('per_elite', $units, Input::old('per_elite'), ['class' => 'form-control'])}}
                </div>
                <div class="col-lg-5 small-padding">
                    {{ Form::input('text', 'link_to_next_elite', Input::old('link_to_next_elite'), ['class' => 'form-control', 'placeholder' => 'Please provide a link for the elite member to sign up. Eventbrite Link, Webinar URL, Landing Page, etc..']) }}
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="misc-errors">
        @if($errors)
            @if($errors->has('event_types'))
                 <p class="red-text">{{ $errors->first('event_types', 'Please select an event type.') }}</p>
            @endif
            @if($errors->has('event_city'))
                 <p class="red-text">{{ $errors->first('event_city', 'Please enter a city.') }}</p>
            @endif
            @if($errors->has('event_state'))
                 <p class="red-text">{{ $errors->first('event_state', 'Please select a state.') }}</p>
            @endif
        @endif
        </div>
                
        <div class="event-attributes">
                <div class="event-type col-lg-3 small-padding">
                        <label for="event_types" class="form-label">Event Types:</label>
                        
                        {{ Form::select('event_types', $event_types, 0, ['class' => 'form-control e-types'])}}
                </div>
                <div class="regional-details col-lg-8">
                        <div class="input-group pull-left small-margin-waist">
                                <label for="event_city" class="form-label">City:</label>
                                {{ Form::input('text', 'event_city', Input::old('event_city'), ['class' => 'form-control', 'placeholder' => 'Provide the city of the event']) }}
                        </div>
                        <div class="input-group ">
                                <label for="event_state" class="form-label">State:</label>
                                {{ Form::select('event_state', $states, 0, ['class' => 'form-control']) }}
                        </div>
                </div>
                <div class="clearfix"></div>
        </div>
        <div class="date-errors">
        @if($errors)
            @if($errors->has('start_date'))
                 <p class="red-text">{{ $errors->first('start_date', 'Please select a start date / time.') }}</p>
            @endif
            @if($errors->has('end_date'))
                 <p class="red-text">{{ $errors->first('event_city', 'Please select an end date / time.') }}</p>
            @endif
        @endif
        </div>
        <div class="date_range_picker">
            <div class="start_picker col-lg-5 small-padding">
                <label for="start_date">Start Time:</label>
                <div class='input-group date' id='start_date'>
                        <input name="start_date" type='text' class="form-control" data-date-format="MM/DD/YYYY h:mm A" value="{{ Input::old('start_date') }}" placeholder="Choose a start time"/>
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
            <div class="end_picker col-lg-5 small-padding hidden">
                <label for="end_date">End Time:</label>
                <div class='input-group date' id='end_date'>
                        <input name="end_date" type='text' class="form-control" data-date-format="MM/DD/YYYY h:mm A" value="{{ Input::old('end_date') }}" placeholder="Choose an end time"/>
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
        </div>

        <div class="event-extra-details">
            <div class="date_picker col-lg-6 hidden">
                <label for="expiration">Expiration Date:</label>
                <div class='input-group date' id='datetimepicker5'>
                    <input name="expiration" type='text' class="form-control" data-date-format="MM/DD/YYYY H:mm A" value="{{ Input::old('expiration') }}" placeholder="The event expires on the end date you selected above "/>
                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-lg-6 small-padding">
                <label for="hs_form_id" class="form-label">Hubspot Form ID:</label>
                <div class="small-padding">
                        {{ Form::input('text', 'hs_form_id', Input::old('hs_form_id'), ['class' => 'form-control', 'placeholder' => '403afad0-9dc9-431d-bcc9-118afc64332a', 'id' => 'hsFormId'])}}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-12 event-textarea">
            <div class="text-errors small-padding">
            @if($errors)
                @if($errors->has('event_description'))
                    <p class="red-text">{{ $errors->first('event_description', 'Please provide a description for your event.') }}</p>
                @endif
                @if($errors->has('attending'))
                     <p class="red-text">{{ $errors->first('attending', 'Please fill out Who Should Attend Section.') }}</p>
                @endif
                @if($errors->has('how_it_works'))
                     <p class="red-text">{{ $errors->first('how_it_works', 'Please fill out How It Works Section.') }}</p>
                @endif
                @if($errors->has('price_description'))
                     <p class="red-text">{{ $errors->first('price_description', 'Please fill out the pricing description.') }}</p>
                @endif
            @endif

            </div>
            <div class="col-lg-6">
                <div class="event-description">
                    <label for="event_description" class="form-label">Description</label>
                    {{ $description_editor }}
                </div>
                <div class="who-should-attend small-padding">
                        <label for="attending" class="form-label">Who Should Attend?</label>
                        {{ $who_should_attend_editor }}
                </div>
                <div class="event-image">
                    <label>Include an image in the event description:</label>
                    {{ Form::file('event_image') }} 
                </div>
            </div>
            <div class="col-lg-6">
                <div class="how-does-it-work">
                    <label for="how_it_works" class="form-label">How Does it Work?</label>
                    {{ $how_it_works_editor }}
                </div>
                <div class="pricing-description small-padding">
                    <label for="price_description" class="form-label">Pricing Details:</label>
                    {{ $pricing_details_editor }}
                    <span class="text-muted">Please Include any Eventbrite links or next step links here</span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="input-group">
            <input type="checkbox" name="duplicate" value="1" id="duplicate" /> Please duplicate this event so that I can make an additional event on a different date
        </div>
        <div class="input-group">
                {{ Form::submit('Save Event', ['class' => 'btn btn-primary'])}}
        </div>
    {{ Form::close() }}
    </div>
</div>
@stop

@section('extra-js')
	@parent
	{{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
	{{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
	{{ HTML::script('js/moment.js') }}
	{{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
	{{ HTML::script('js/events.js') }}
@stop