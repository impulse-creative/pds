@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/forum-styles.css') }}
    {{ HTML::style('css/events-styles.css') }}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<?php
$core = isset($event->core_price) && $event->core_price != 0 ? 1 : 0;
$advanced = isset($event->advanced_price) && $event->advanced_price != 0 ? 1 : 0;
$elite = isset($event->elite_price) && $event->elite_price != 0 ? 1 : 0;
$permission = explode(':', $sections['permissions']);

function cleanForId($dirty_string) {
    $lather = str_replace(' ', '_', $dirty_string); // Replaces all spaces with underscores.
    return $rinse = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $lather)); // Removes special chars and makes lower case
}

$formid = $event->hs_form_id == '' ? '403afad0-9dc9-431d-bcc9-118afc64332a' : $event->hs_form_id;
$currentPrice = 1000000000;
?>
<div class="container medium-padding">
    <div class="single-event-container col-lg-9 col-md-9 col-sm-9 col-xs-9">
        <h1 class="purple-text pull-left">{{{ $event->title }}}</h1>
        <div class="event-details pull-right">
            <?php $created = new \Carbon\Carbon($event->start_event); ?>
            Starts on {{$created->format('D, M d')}}
        </div>
        <div class="clearfix"></div>
        <div class="description ">
            @if($event->featured_img != '')
                <img src="{{URL::to('storage/files/events/'.$event->featured_img)}}" class="pull-right"/>
            @endif
            <p>{{ $event->description }}</p>
        </div>
        <div class="attendance-suggestions">
            @if(isset($event->who_should_attend) && $event->who_should_attend != '')
                <h2 class="orange-text">Who Should Attend?</h2>
                <p>{{$event->who_should_attend}}</p>
            @endif
        </div>
        <div class="how-it-works">
            @if(isset($event->how_does_it_work) && $event->how_does_it_work != '')
                <h2 class="orange-text">How Does it Work?</h2>
                <p>{{ $event->how_does_it_work }}</p>
            @endif
        </div>
    </div>


    <div class="single-event-sidebar col-lg-3 col-md-3 col-sm-3 col-xs-3">
        @if(in_array('5', $permission) || in_array('7', $permission) || in_array('1', $permission))
            <div class="edit-event">
                <a href="{{ URL::route('get.update.event', ['event_id' => $event->id]) }}" class="btn btn-primary update-event"><i class="fa fa-lg fa-pencil-square-o"></i>&nbsp;Edit Event</a>
            </div>
        @endif
        <div class="pricing-section medium-padding">
            @if($event->free_event != 1)
            @if(Auth::check())
            @if(in_array('18', $permission))
                @if($event->core_price != 0)
                    <p class="price orange-text">${{ $event->core_price or 'No Price Set' }}</p>
                    <p>per {{$units[$event->per_core] }}</p>
                @endif
                <?php $currentPrice = $event->core_price;
                if ($event->link_to_next_core !== 'http://')
                    $link = $event->link_to_next_core;
                ?>
            @endif
            @if(in_array('19', $permission))
            @if($event->advanced_price != 0)
            <p class="price orange-text">${{ $event->advanced_price or 'No Price Set' }}</p>
            <p>per {{ $units[$event->per_advanced] }}</p>
            @endif
            <?php $currentPrice = $event->advanced_price;
            if ($event->link_to_next_advanced !== 'http://')
                $link = $event->link_to_next_advanced;
            ?>
            @endif
            @if(in_array('20', $permission))
            @if($event->elite_price != 0)
            <p class="price orange-text">${{ $event->elite_price or 'No Price Set' }}</p>
            <p>per {{ $units[$event->per_elite] }}</p>
            @endif
            <?php $currentPrice = $event->elite_price;
            if ($event->link_to_next_elite !== 'http://')
                $link = $event->link_to_next_elite;
            ?>
            @endif
            @if($currentPrice != $lowest['price'])
            <p>{{ $lowest['string'] }}</p>
            @endif
            @if(!in_array('18', $permission) && !in_array('19', $permission) && !in_array('20', $permission) && $event->is_member_only == 1)
            <p><a class="price-cta" href="http://www.pharmacyowners.com/free-consultation" target="_blank">Members Only</a></p>
            @else
            @if($event->link_to_next_free !== 'http://')
            <?php $link = $event->link_to_next_free; ?>
            @endif
            <p><a class="price-cta" href="{{$link or $event->link_to_next_non_member}}" target="_blank">Sign Up Now</a></p> 
            @endif
            <p class="description">{{ $event->pricing_description }}</p>
            @else
            <h2 class="red-text">Not Signed In!</h2>
            <p class="text-muted">Interested in creating an account?</p>
            <p><a class="price-cta" href="{{URL::route('login.form')}}" target="_blank">Sign Up Now</a></p> 
            @endif
            @else
            <h2 class="green-text">Free Event!</h2><br/>
            <p class="text-muted">Interested in membership?</p>
            <p><a class="price-cta" href="{{ $event->link_to_next_free }}" target="_blank">Sign Up Now</a></p> 
            <p class="description">{{ $event->pricing_description }}</p>
            @endif
        </div>            
        <h2>Need more info? Let's chat:</h2>
        <div>
            <script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
            <script>
                hbspt.forms.create({
                    portalId: '37772',
                    formId: '{{ $formid }}'
                });
            </script>
        </div>
        <div class="similar-events">
            <?php $created = new \Carbon\Carbon($event->created_at);
            $now = \Carbon\Carbon::now(); ?>
            @if(count($similar_events) > 0)
                <h2>Related Events</h2>
                @foreach($similar_events as $mini_event)
                    @if($mini_event->id == $event->id) <?php continue; ?> @endif
                    <?php $created = new \Carbon\Carbon($mini_event->created_at);
                    $now = \Carbon\Carbon::now(); ?>
                    <div class="mini-event-item">
                        <p><a class="orange-text" href="{{ URL::route('app.view.event', ['id' => $mini_event->id, 'event' => cleanForId($mini_event->title)]) }}">{{{$mini_event->title}}}</a></p>
                        <p class="text-muted">{{ $difference = ($created->diff($now)->days < 1) ? 'today' : $created->diffForHumans($now, true) }}</p>
                        <p>{{$created->format('g:i A')}}</p>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

</div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/events.js') }}
@stop