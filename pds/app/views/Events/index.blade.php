@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/forum-styles.css') }}
    {{ HTML::style('css/events-styles.css') }}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<?php
$permissions = explode(':', $sections['permissions']);

function cleanForId($dirty_string) {
    $lather = str_replace(' ', '_', $dirty_string); // Replaces all spaces with underscores.
    return $rinse = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $lather)); // Removes special chars and makes lower case
}

function truncate_words($text, $limit, $url, $ellipsis = '...') {
    $words = preg_split("/[\n\r\t ]+/", $text, $limit + 1, PREG_SPLIT_NO_EMPTY);
    if (count($words) > $limit) {
        array_pop($words);
        $text = implode(' ', $words);
        $text = $text . $ellipsis . '<a href="' . $url . '">Read More</a>.';
    }
    return $text;
}
?>
<div class="container">
    <div class="events-sidebar col-lg-2 col-md-2 small-padding">
        <div class="forum-search">
            {{ Form::open(['route' => 'app.search.events', 'class' => 'forum-topic-search']) }}
            <div class="control-group">
                {{ Form::input('text', 'search_key', Input::old('search_key'), ['class' => 'topic-search inp-search-field form-control', 'placeholder' => 'search events']) }}
            </div>
            {{ Form::close() }}
        </div>
        <div class="event_type_list">
            <div class="forum-category-list-mobile mobile-categories col-lg-4 inline-element">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="category-list-mobile" data-toggle="dropdown" aria-expanded="true">
                        Select Event Type
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu resource-dropdown" role="menu" aria-labelledby="category-list-mobile">
                        @foreach($event_types as $key => $type)
                            <li class="event-item"><a href="{{ URL::route('get.filtered.index', ['id' => $key, 'event' => cleanForId($type)]) }}">{{{$type}}}</a></li> 
                        @endforeach
                    </ul>
                </div>
            </div>
            <ul class="main-event-type-select">
                @foreach($event_types as $key => $type)
                    <li class="event-item">
                        <a class="event-link" href="{{ URL::route('get.filtered.index', ['id' => $key, 'event' => cleanForId($type)]) }}">{{{$type}}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="events-list-container col-lg-10 col-md-10 col-sm-10 col-xs-10">
        @if(Session::has('Info'))
            <div class="alert alert-info">{{ $topic = Session::pull('Info', 'default') }}</div>
        @endif
        @if(Session::has('Success'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
        @endif
        @if(Session::has('Failure'))
        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
        @endif
        <div class="event-title pull-left">
            <h1>Events</h1>
        </div>
        <div class="tools pull-div-right">
            <div class="calendar-view-btn inline-element small-margin-waist">
                <a href="{{ URL::route('get.calendar.index') }}" class="btn btn-primary"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Calendar View</a>
            </div>
            @if(in_array(7, $permissions))
            <div class="new-event-btn inline-element">
                <a href="{{ URL::route('get.add.event') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add New Event</a>
            </div>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class="events-cta-container">
            <!--HubSpot Call-to-Action Code -->
            <span class="hs-cta-wrapper" id="hs-cta-wrapper-5fe37a28-9cbc-4654-9e64-e53fcf538c75">
                <span class="hs-cta-node hs-cta-5fe37a28-9cbc-4654-9e64-e53fcf538c75" id="hs-cta-5fe37a28-9cbc-4654-9e64-e53fcf538c75">
                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                    <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/5fe37a28-9cbc-4654-9e64-e53fcf538c75"><img class="hs-cta-img" id="hs-cta-img-5fe37a28-9cbc-4654-9e64-e53fcf538c75" style="border-width:0px; width:100%!important;" src="https://no-cache.hubspot.com/cta/default/37772/5fe37a28-9cbc-4654-9e64-e53fcf538c75.png" /></a>
                </span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(37772, '5fe37a28-9cbc-4654-9e64-e53fcf538c75');
                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code --> 
        </div>
        <div class="event-pagination small-padding center">
            <?php echo $events->links(); ?>
        </div>
        <div class="timeline-list small-padding">
            <?php
            if (count($events) > 0) {
                $formatted_month = new \Carbon\Carbon($events[0]->start_event);
                $tmp_month = $formatted_month->format('F');
            }
            ?>
            @if(count($events) > 0)
                @foreach($events as $event)
                    <?php
                        $created_at = new \Carbon\Carbon($event->created_at);
                        $start = new \Carbon\Carbon($event->start_event);
                        $end = new \Carbon\Carbon($event->end_event);
                        $month = $start->format('F');
                        if ($month != $tmp_month) {
                            echo '</ul>';
                        }
                        $tmp_month = $month;
                        if ($months[$month] == 0) {
                            echo '<h2 class="event-month">' . $start->format("F, Y") . '</h2>';
                            echo '<ul class="timeline">';
                            $months[$month] = 1;
                        }
                    ?>
                    <li id="{{$event->id}}" class="timeline-inverted">
                        <div class="timeline-day">
                            <p>{{$start->format('D')}}</p>
                            <p>{{$start->format('d')}}</p>
                            <p>{{$start->format('M')}}</p>
                        </div>
                        <div class="timeline-badge default">
                            <div class="timeline-badge-inner primary"></div>
                        </div>
                        <div class="timeline-time-range">
                            @if(isset($event->start_event)) 
                            <p>{{$start->format('g:i A')}} <br>EST</p>
                            @endif 
                        </div>

                        <div class="timeline-panel">
                            <div class="featured-event green-background rotate @if($event->is_featured != 1) hidden @endif"><p class="horizSpace">FEATURED</p></div>
                            <div class="timeline-heading">
                                <h4 class="timeline-title"><a class="purple-text" href="{{ URL::route('app.view.event', ['id' => $event->id, 'event' => cleanForId($event->title)]) }}">{{ $event->title }}</a></h4>
                            </div>
                            <div class="timeline-body">
                                <div class="timeline-description">
                                    @if($event->featured_img != '')
                                        <img src="{{URL::to('storage/files/events/'.$event->featured_img)}}" class="pull-right"/>
                                    @endif
                                    {{ truncate_words($event->description, 50, URL::route('app.view.event', ['id' => $event->id, 'event' => cleanForId($event->title)])) }}
                                </div>
                                <p class="timeline-mobile-date text-muted">
                                    Starts on {{$start->format('D, M d')}}
                                </p>
                                <p class="timeline-location">Event
                                    @if(isset($event->city) && strlen($event->city) > 1)
                                        Location: <span> {{$event->city}},&nbsp;{{$event->location->state_abreviation}} </span>
                                    @else
                                        Type: <span> {{$event->type->type}} </span>
                                    @endif
                                </p>
                            </div>
                            <a href="{{ URL::route('app.view.event', ['id' => $event->id, 'event' => cleanForId($event->title)]) }}" class="timeline-see-more"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            @if(in_array(7, $permissions))
                                <div class="event-crud small-padding">
                                    <a href="{{ URL::route('get.update.event', ['event_id' => $event->id]) }}" class="btn btn-primary update-event"><i class="fa fa-lg fa-pencil-square-o"></i>&nbsp;Edit Event</a>
                                    <a href="{{ URL::route('app.delete.event', ['event_id' => $event->id]) }}" class="btn btn-danger delete-event confirm" data-confirm="This is a permanent action! Are you sure you want to remove this event?"><i class="fa fa-lg fa-trash-o"></i>&nbsp;Delete Event</a>
                                </div>
                            @endif
                        </div>
                    </li>
                @endforeach
            @else   
                <div class="no-events-alert">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            No events in this category!
                        </div>
                        <div class="panel-body">
                            <h3>No events are present in this category.</h3>
                            @if(in_array(7, $permissions))
                                <p>
                                    <a href="{{ URL::route('get.add.event') }}" class="btn btn-primary small-margin"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add New Event</a>
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
            </ul>
        </div>
        <div class="event-pagination small-padding enter">
            <?php echo $events->links(); ?>
        </div>
    </div>
</div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/events.js') }}
    <script style="display:none;">
        $.fn.adjustTagHeightForParent = function () {
            return $(this).each(function () {
                var parentHeight = $(this).parent().outerHeight();
                $(this).css('width', parentHeight - 2);
            });
        };
        /*--keeping your featured and member tags at a good width to match the height of the parent --*/
        $('.featured-event').adjustTagHeightForParent();
        $('.member-event').adjustTagHeightForParent();
        /*-- keeping aspect ratio of tags complimentary to the parent when resizing the window --*/
        $(window).on('resize', function () {
            $('.featured-event').adjustTagHeightForParent();
            $('.member-event').adjustTagHeightForParent();
        });
    </script>
@stop