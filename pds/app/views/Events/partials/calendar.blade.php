

@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/forum-styles.css') }}
	{{ HTML::style('css/events-styles.css') }}
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<?php 
$permissions = explode(':', $sections['permissions']);
function cleanForId($dirty_string) {
    $lather = str_replace(' ', '_', $dirty_string); // Replaces all spaces with underscores.
    return $rinse = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $lather)); // Removes special chars and makes lower case
}
?>
<div class="container">
    <div class="events-sidebar col-lg-2 col-md-2 small-padding">
        <div class="forum-search">
            {{ Form::open(['route' => 'app.search.events', 'class' => 'forum-topic-search']) }}
                {{ Form::input('text', 'search_key', Input::old('search_key'), ['class' => 'topic-search inp-search-field', 'placeholder' => 'search events']) }}
            {{ Form::close() }}
        </div>
        <div class="event_type_list">
            <ul>
            @foreach($event_types as $key => $type)
                <li class="event-item">
                        <a class="event-link" href="{{ URL::route('get.filtered.index', ['id' => $key, 'event' => cleanForId($type)]) }}">{{{$type}}}</a>
                </li>
            @endforeach
            </ul>
            <div class="calendar-view-btn input-group center">
            <a href="{{ URL::route('events.index') }}" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span>&nbsp;List View</a>
        </div>
        </div>
    </div>
    <div class="events-list-calendar-container small-padding col-lg-10 col-md-10 col-sm-10 col-xs-10">
	    @if(Session::has('Success'))
	        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
	    @endif
	    @if(Session::has('Failure'))
	        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
	    @endif
        <div class="event-title pull-left">
                <h1>Events</h1>
        </div>
        
        @if(in_array(7, $permissions))
            <div class="new-event-btn input-group pull-right">
                    <a href="{{ URL::route('get.add.event') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add New Event</a>
            </div>
        @endif
        <div class="clearfix"></div>
	    <div class="events-cta-container small-padding">
	        <!--HubSpot Call-to-Action Code -->
	        <span class="hs-cta-wrapper" id="hs-cta-wrapper-5fe37a28-9cbc-4654-9e64-e53fcf538c75">
	            <span class="hs-cta-node hs-cta-5fe37a28-9cbc-4654-9e64-e53fcf538c75" id="hs-cta-5fe37a28-9cbc-4654-9e64-e53fcf538c75">
	                <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
	                <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/5fe37a28-9cbc-4654-9e64-e53fcf538c75"><img class="hs-cta-img" id="hs-cta-img-5fe37a28-9cbc-4654-9e64-e53fcf538c75" style="border-width:0px; width:100%!important;" src="https://no-cache.hubspot.com/cta/default/37772/5fe37a28-9cbc-4654-9e64-e53fcf538c75.png" /></a>
	            </span>
	            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
	                <script type="text/javascript">
	                    hbspt.cta.load(37772, '5fe37a28-9cbc-4654-9e64-e53fcf538c75');
	                </script>
	        </span>
	        <!-- end HubSpot Call-to-Action Code -->
	    </div>
		{{-- Calendar using CLNDR.js --}}
	    <div class="cal2">

	      <script type="text/template" id="template-calendar">
	        <div class="clndr-controls">
	          <div class="clndr-previous-button">&lsaquo; Previous Month</div>
	          <div class="month"><%= month %>&nbsp;<%= year %></div>
	          <div class="clndr-next-button">Next Month &rsaquo;</div>
	        </div>
	        <div class="clndr-grid">
	          <div class="days-of-the-week">
	            <% _.each(daysOfTheWeek, function(day) { %>
	              <div class="header-day"><%= day %></div>
	            <% }); %>
	            <div class="days">
	              <% _.each(days, function(day) { %>
	                <div class="<%= day.classes %>" <% if(day.events.length) { %> data-target="#<%= day.day %>" data-toggle="modal" <% } %> ><%= day.day %>
                 
                        <% if(day.events.length) { %>
                            <% _.each(day.events, function(event) { %>
                                    <div style="margin-bottom: 5px;"><%= event.title %></div>
                            <% }, day.events); %>
                        <% } else { %>
                                    <div class="calFiller">&nbsp;</div>
                            <% } %>
                        </div>
	                <% if(day.events.length) { %>
						<div class="modal fade" id="<%= day.day %>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						        <% var event_date = new Date(day.date); %>
						        <h4 class="modal-title" id="modalLabel">Events occuring on: <%= event_date.toDateString() %></h4>
						      </div>
						      <div class="modal-body">
						      	<div class="timeline-list small-padding">
						      		<!-- JS events inserted below -->
						      		<ul class="timeline">

						      		</ul>
						      	</div>
						      </div>
						    </div>
						  </div>
						</div>
					<% } %>
	              <% }); %>
	            </div>
	          </div>
	        </div>
	        <div class="clndr-today-button">Go Back To Current Month</div>
	      </script>

	    </div>

    </div>
</div>
@stop

@section('extra-js')
	@parent
	{{ HTML::script('js/underscore.js') }}
	{{ HTML::script('js/moment.js') }}
	{{ HTML::script('js/clndr.min.js') }}
	{{ HTML::script('js/calendar-scripts.js') }}
@stop
