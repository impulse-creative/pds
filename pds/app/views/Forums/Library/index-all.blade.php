@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/forum-styles.css') }}
    {{ HTML::style('css/library-styles.css') }}
    {{ HTMl::style('css/bootstrap-datetimepicker.min.css') }}
    {{ HTML::style('css/loading.css') }}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<?php
    function shorten_string($string, $length) {
        if (strlen($string) > $length) {
            $string = trim(substr($string, 0, $length) . "...");
        }
        return $string;
    }

    function cleanForId($string) {
        $string = str_replace(' ', '_', $string); // Replaces all spaces with underscores.
        return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars and makes lower case
    }
    $permissions = explode(':', Auth::user()->groups);
?> 

<div class="container forum-container resources">
    <div class="col-lg-3 col-md-3">
        @include('Forums.Library.partials.resource-type-sidebar')
    </div>
    <div class="col-lg-9 col-md-9 resource-wrap">
        <div class="topic-list-information small-padding">
            <div class="col-lg-6 filtered-by">
                <p>
                    
                    <h1 class="purple-text">Resource Library</h1>
                    <hr/>
                </p>
            </div>
            @if(Auth::check())
                @if(in_array('1', $user_permissions) || in_array('5', $user_permissions) || in_array('9', $user_permissions))
                    <div class="col-lg-3 create-resource pull-right">
                        <a href="#" data-target="#addNewResource" data-toggle="modal" class="btn btn-primary new-resource-btn"><i class="fa fa-plus"></i>&nbsp;Add New Resource</a>
                    </div>
                    <div class="col-lg-3 check-trash pull-right">
                        <a href="{{ URL::route('app.trashed.resource') }}" class="btn btn-primary trashcan-btn"><i class="fa fa-trash-o"></i>&nbsp;Open Trashbin</a>
                    </div>
                @endif
            @endif
            <div class="clearfix"></div>
        </div>
        @if(Session::has('Success'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
        @endif
        @if(Session::has('Failure'))
        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
        @endif
        <div class="resource-wrap">
        @if(!in_array(15, $permissions))
            <div class="non-mem-alert">
                <h3><i class="fa fa-exclamation-triangle"></i>&nbsp;Oops, Looks like you’re not a PDS member. </h3>
                <p>
                    By <a href="http://hubs.ly/y0xs880" target="_blank" class="orange-text"><strong>becoming a member</strong></a>
                    you will get access to hundreds of resources and programs to benefit your pharmacy. To speak with a Pharmacy Expert about all the benefits of membership.
                </p>
                <a href="http://hubs.ly/y0xs880" target="_blank" class="membership-cat orange-text">Schedule a time to a Pharmacy Expert about Membership Now!</a>
                <p>Paid PDS Member and feel like you’re seeing this in error? Please click the ? in the bottom right of the screen and we'll get you set up quickly</p>
            </div>
        @endif
            @if(count($resources) > 0)
                @if(is_object($resources))
                    <div class="resource-pagination-links center">
                       {{ $resources->appends(Input::except('page'))->links() }}
                    </div>
                @endif
                @foreach($resources as $key => $resource)
                    @if(!in_array($resource->access_level, $permissions)) <?php continue; ?> @endif
                    @if($resource->is_forum_attachement == 1) <?php continue; ?> @endif
                    <?php
                        $filename = $resource->filename;
                        $previewable_file_types = ['xls', 'xlsx', 'docx', 'doc', 'pdf'];
                        $icon_list = ['avi', 'css', 'eps', 'excel', 'mov', 'psd', 'txt'];
                        $icon_path = in_array($resource->file_type, $icon_list) ? URL::to('/images/application/resources/icons').'/'.$resource->file_type.'.png' : URL::to('/images/application/resources/icons') . '/unknown.png';
                        $other_icons = [
                            'doc'  =>  '<i class="fa fa-2x fa-file-word-o"></i>',
                            'docx' =>  '<i class="fa fa-2x fa-file-word-o"></i>',
                            'ppt'  =>  '<i class="fa fa-2x fa-file-powerpoint-o"></i>',
                            'pptx' =>  '<i class="fa fa-2x fa-file-powerpoint-o"></i>',
                            'xls'  =>  '<i class="fa fa-2x fa-file-excel-o"></i>',
                            'xlsx' =>  '<i class="fa fa-2x fa-file-excel-o"></i>',
                            'png'  =>  '<i class="fa fa-2x fa-file-image-o"></i>',
                            'jpg'  =>  '<i class="fa fa-2x fa-file-image-o"></i>',
                            'jpeg' =>  '<i class="fa fa-2x fa-file-image-o"></i>',
                            'pdf'  =>  '<i class="fa fa-2x fa-file-pdf-o"></i>', 
                            'zip'  =>  '<i class="fa fa-2x fa-file-archive-o"></i>',
                            'mp3'  =>  '<i class="fa fa-2x fa-file-audio-o"></i>',
                            'mp4'  =>  '<i class="fa fa-2x fa-file-video-o"></i>',
                            'wav'  =>  '<i class="fa fa-2x fa-file-video-o"></i>'
                        ];
                        $use_fa = 0;
                        foreach($other_icons as $type => $icon) {
                            if($resource->file_type === $type) {
                                $use_fa = 1;
                                $fa_icon = $icon;
                            }
                        }
                        $target = $resource->external_link == 1 ? 'target="_blank"' : '';
                        $url = URL::route('app.download.resource', $resource->resource_name);
                        $linkText = ' Download';
                        if ($resource->wistia == 1) {
                            $url = URL::route('app.embed.wistia', ['div' => $resource->resource_name]);
                            $linkText = ' Open Video';
                        }
                        if ($resource->external_link == 1) {
                            $url = $resource->resource_name;
                            $linkText = ' Open';
                        }
                        $dwld_link = '<a href="' . $url . '" ' . $target . ' class="purple-text"><span class="glyphicon glyphicon-cloud-download"></span>' . $linkText . '</a>';
                    ?>
                    <div class="featured-resource">
                        <div class="resource-download vcenter">
                            @if($resource->wistia === 0)
                                {{ Form::checkbox('dwldChoice', $resource->id) }}
                            @endif
                        </div><!--
                        --><div class="resource-thumbnail vcenter">
                            <div class="thumbnail-img">
                                @if($use_fa == 1)
                                    {{ $fa_icon }}
                                @else
                                    <img src="{{ $icon_path }}" alt="PDS Resource Icon"/>
                                @endif
                            </div>
                        </div><!--
                        --><div class="resource-title vcenter">
                            <h2><a data-target="#{{ cleanForId($filename) }}" data-toggle="modal" class="purple-text">{{ shorten_string($resource->filename, 50) }}</a></h2>
                            <p>by {{ $resource->user->fname }}&nbsp;{{ $resource->user->lname }} on {{ date("m/d/Y", strtotime($resource->created_at)) }}</p>
                        </div><!--
                        --><div class="resource-downloads vcenter pull-right">
                            <p>
                                <a data-target="#{{ cleanForId($filename) }}" data-toggle="modal" class="purple-text view-resource-details"><span class="glyphicon glyphicon-search"></span>&nbsp;View</a>
                                &nbsp;{{ $dwld_link }}
                            </p>
                            <span class="download-count pull-right">{{$resource->download_count}}&nbsp;Downloads</span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>

                    {{-- hidden details modal --}}
                    <div class="modal fade details-modal" id="{{ cleanForId($filename) }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <div class="resource-actions">
                                        <div class="title vcenter">
                                            <h4 class="modal-title vcenter" id="modalLabel">
                                                Resource Details
                                            </h4>
                                        </div><!--
                                        --><div class="actions vcenter">
                                            @if(Auth::check())
                                                @if(in_array('1', $user_permissions) || in_array('5', $user_permissions) || in_array('9', $user_permissions))
                                                    <a class="purple-text" href="{{ URL::route('app.edit.resource', $resource->id) }}">
                                                        <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                                                    </a>
                                                    @if(($resource->external_link != 1 || $resource->wistia != 1) && ($resource->external_link == 0 && $resource->wistia == 0))
                                                        <a class="purple-text" href="" data-target="#{{$resource->id}}" data-toggle="modal">
                                                            <span class="glyphicon glyphicon-transfer"></span>&nbsp;Replace File
                                                        </a>
                                                    @endif
                                                @endif
                                            @endif
                                            <span class="purple-text subscribe-button" data-id="{{$resource->id }}" data-type="resources">
                                                <span class="glyphicon glyphicon-bullhorn"></span> Subscribe
                                            </span>
                                            {{$dwld_link}}
                                        </div>
                                    </div>
                                    <div class=clearfix"></div>
                                </div>
                                <div class="modal-body">
                                    <div class="resource-info">
                                        <div class="resource-icon vcenter">
                                            @if(in_array($resource->file_type, ['png', 'jpg', 'gif', 'ico', 'jpeg']))
                                                <img id="user-img" src="{{ URL::to('storage/files/Library/fullsized') }}/{{ $resource->resource_name }}" alt="{{ $resource->filename }}">
                                            @else
                                                <img src="{{ $icon_path }}" alt="PDS Resource Icon"/>
                                            @endif
                                        </div><!--
                                        --><div class="resource-details vcenter">
                                            <h2><strong>{{ $resource->filename }}</strong></h2>
                                            <p class="inline-element f-name">{{ $resource->resource_name }}<p>
                                            <p class="inline-element f-size">{{ $resource->file_size }}<p>
                                            <div class="clearfix"></div>
                                            <div class="r-description">
                                                {{ $resource->description }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="extra-details">
                                        @if($resource->expire_date != '0000-00-00 00:00:00' && $resource->expire_date != '1970-01-01 00:00:00' && $resource->expire_date != '1969-12-31 19:00:00')
                                            <div class="resource-expiration col-lg-4 col-md-4 @if(is_null($resource->expire_date)) hidden @endif">
                                                <p>Expires:&nbsp;<span class="purple-text">{{ date("M d, Y", strtotime($resource->expire_date)) }}</span></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endif
                                    </div>
                                    <div class="resource-share">
                                        <div class="col-lg-3 col-md-3">
                                            <label for="link" class="pull-left"><i class="fa fa-share-alt"></i>&nbsp;Share Link:&nbsp;</label>
                                        </div>
                                        <?php
                                        $shareLink = $resource->resource_share_link;
                                        if(strpos($resource->resource_share_link, 'http://pds.impulsecreative.com') === 0) {
                                            $shareLink = str_replace('http://pds.impulsecreative.com', Request::root(), $resource->resource_share_link);
                                        } ?>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input name="link" type="text" class="form-control r-share" value="{{$shareLink}}" data-toggle="tooltip" data-placement="top" data-original-title="Highlight and click ctrl+c (windows) or cmd+c (osx)"/>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="remove-resource">
                                        @if(in_array('1', $user_permissions) || in_array('5', $user_permissions) || in_array('9', $user_permissions))
                                            <a href="{{ URL::route('app.destroy.resource', $resource->id) }}" class="btn btn-danger pull-right small-margin"><i class="fa fa-trash-o"></i>&nbsp;Trash</a>
                                        @endif
                                        <a href="{{ URL::route('view.doc', ['file_id' => $resource->id]) }}" class="btn btn-primary pull-right small-margin small-margin-waist"><i class="fa fa-file-text-o"></i>&nbsp;Preview Resource</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Swap Resource Files --}}
                    <div class="modal fade swap-files" id="{{$resource->id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <div class="resource-actions">
                                        <h4 class="modal-title" id="modalLabel">
                                            Replace File
                                        </h4>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="resource-info">
                                        {{ Form::open(['route' => 'app.update.resource.file', 'class' => 'update-file-only', 'files' => 'true']) }}
                                        {{ Form::token() }}
                                        {{ Form::hidden('id', $resource->id) }}
                                        {{ Form::hidden('file_type', $resource->resource_type) }}
                                        <div class="input-group">
                                            <div class="thumbnail">
                                                @if(in_array($resource->file_type, ['png', 'jpg', 'gif', 'ico', 'jpeg']))
                                                    <img id="user-img" src="{{ URL::to('storage/files/Library/fullsized') }}/{{ $resource->resource_name }}" alt="{{ $resource->filename }}">
                                                @else
                                                    <?php
                                                    $use_fa = 0;
                                                    $filename = $resource->resource_name;
                                                    $icon_list = ['avi', 'css', 'doc', 'eps', 'excel', 'mov', 'mp3', 'pdf', 'ppt', 'psd', 'txt', 'wav', 'zip'];
                                                    $icon_path = in_array($resource->file_type, $icon_list) ? URL::to('/images/application/resources/icons') . '/' . $resource->file_type . '.png' : URL::to('/images/application/resources/icons').'/unknown.png';
                                                    ?>
                                                @endif
                                                <div class="caption">
                                                    <p><a href="#" class="btn btn-default new-file-btn"><i class="fa fa-file-o"></i>&nbsp;Choose a New File?</a></p>
                                                </div>
                                            </div>
                                            <div class="new-file hidden">
                                                <span class="btn btn-primary btn-file small-margin vcenter">
                                                    <form>
                                                        {{ Former::files('file', 'Browse For A New Resource', ['class' => 'btn-file']) }}
                                                    </form>
                                                </span><!--
                                                --><div class="vcenter cancel-new-file small-margin">
                                                    <a href="#" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                                </div>
                                            </div>
                                            <div class="submit-edits">
                                                {{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                @if(Auth::check())
                    <div class="downloadFiles small-padding">
                        <a class="btn btn-primary dwldFilesBtn"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download Selected Files</a>
                    </div>
                @endif
                {{-- End of Resources --}}
                @if(is_object($resources))
                    <div class="resource-pagination-links center">
                        {{ $resources->appends(Input::except('page'))->links() }}
                    </div>
                @endif
            @else
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>No resources found</div>
            @endif
        </div>
        <div class="hs_forum_cta_mobile medium-padding">
            <!--HubSpot Call-to-Action Code -->
            <span class="hs-cta-wrapper" id="hs-cta-wrapper-fc1d9070-1875-4789-b40c-8089ec496835">
                <span class="hs-cta-node hs-cta-fc1d9070-1875-4789-b40c-8089ec496835" id="hs-cta-fc1d9070-1875-4789-b40c-8089ec496835">
                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                    <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/fc1d9070-1875-4789-b40c-8089ec496835"><img class="hs-cta-img" id="hs-cta-img-fc1d9070-1875-4789-b40c-8089ec496835" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/fc1d9070-1875-4789-b40c-8089ec496835.png" /></a>
                </span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
hbspt.cta.load(37772, 'fc1d9070-1875-4789-b40c-8089ec496835');
                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
        <div class="forum-category-list-mobile mobile-categories">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="category-list-mobile" data-toggle="dropdown" aria-expanded="true">
                    Select a Resource to View
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu resource-dropdown" role="menu" aria-labelledby="category-list-mobile">
                    @foreach($file_types_array as $key => $type)
                    @if($key == 0) <li class="cat-item item-{{ $key }}"><a href="{{ URL::route('library.index') }}">All Resources</a></li> <?php continue; ?> @endif
                    <li class="cat-item item-{{ $key }}"><a href="{{ URL::to('library/category') }}?type={{$key}}">{{ $type }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>



{{-- Add New Resource -- modal --}}
<div class="modal fade" id="addNewResource" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Add New Resource</h4>
            </div>
            <div class="modal-body">
                @if(Session::has('Edit Failure'))
                <script>
                    $(document).on('ready', function () {
                        $(".fileInfoChange").trigger('click');
                    });
                </script>
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Edit Failure', 'default') }}</div>
                @endif
                @if($errors)
                    @foreach($errors->all() as $error)
                        <p class="red-text">{{$error}}</p>
                    @endforeach
                @endif
                <div class="validation-warning"></div>
                {{ Form::open(array('route' => 'app.store.resource', 'class' => 'add-new-resource-form', 'files' => true))}}
                <div class="form-group">
                    {{ Form::hidden('user', Auth::id()) }}
                    <label for="filename">File Name:</label>
                    <div class="input-group">
                        {{ Form::input('text', 'filename', Input::old('filename'), ['class' => 'form-control', 'placeholder' => 'File Name']) }}
                        @if($errors->has('filename'))
                            <p class="muted-text red-text">{{ $errors->first('filename', 'Please provide a name for your resource') }}</p>
                        @endif
                    </div>
                    <label for="description">Description:</label>
                    @if($errors->has('data'))
                        <p class="muted-text red-text">{{ $errors->first('data') }}</p>
                    @endif
                    <div class="input-group">
                        <div id="wysihtml5-toolbar" style="display:none;">
                            <div class="btn-group">
                                <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
                                <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
                            </div>
                            <div class="btn-group">
                                <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
                                    <span class="glyphicon glyphicon-list"></span>
                                </a>
                                <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
                                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                </a>
                            </div>
                        </div>
                        <textarea class="inp-field vendor-description-text" id="editor" name="data" cols="10" rows="5" placeholder="Provide a description for your resource">{{Input::old('data')}}</textarea>
                        <input class="inp-field data-html" id="hidden-html" name="html_edits" type="hidden" value="">
                    </div>

                    @if(Auth::check())
                        @if(in_array('1', explode(':', Auth::user()->groups)) || in_array('5', explode(':', Auth::user()->groups)) || in_array('9', explode(':', Auth::user()->groups)) )
                            <label for="resource_container">Choose a resource type:</label>
                            <div class="input-group">
                                {{ Form::select('resource_type', $resource_cats, 0, ['id' => 'library_resource_container', 'class' => 'form-control']) }}
                            </div>
                            @if($errors->has('resource_type')) <p class="muted-text red-text">{{$errors->first('resource_type', 'Please select a resource type') }} </p>@endif
                        @endif
                    @endif

                    @if(Auth::check())
                        @if(in_array('1', explode(':', Auth::user()->groups)) || in_array('5', explode(':', Auth::user()->groups)) || in_array('9', explode(':', Auth::user()->groups)) )
                            {{ Form::label('access_level', 'Choose a resource category:') }}
                            <div class="input-group">
                                {{ Form::select('access_level', $allResourceGroups, $viewing_resource, ['id' => 'resource_access', 'class' => 'form-control']) }}
                            </div>
                            @if($errors->has('access_level')) <p class="muted-text red-text">{{$errors->first('access_level', 'Please select a resource category') }} @endif
                        @endif
                    @endif
                    
                    @if(Auth::check())
                        @if(in_array('1', explode(':', Auth::user()->groups)) || in_array('5', explode(':', Auth::user()->groups)) || in_array('9', explode(':', Auth::user()->groups)) )
                            <label for="resource_container">Choose the minimum access level for this resource:</label>
                            <div class="input-group">
                                {{ Form::select('permission', $access_list, 0, ['id' => 'library_resource_container', 'class' => 'form-control']) }}
                                <br><br>
                                <span style="font-size:smaller">(PDS Member & Non-Member will be your most typical choices. <a href="http://pdsadvantage.uservoice.com/knowledgebase/articles/481799" target="_blank">Learn More</a>)</span>
                                @if($errors->has('permission')) <p class="muted-text red-text">{{$errors->first('permission', 'Please choose a minimum permission level for your resource') }} @endif
                            </div>
                        @endif
                    @endif
                    <label for="expiration">Expiration Date:</label>
                    <div class='input-group date' id='datetimepicker5'>
                        <input name="expiration" type='text' class="form-control" data-date-format="MM/DD/YYYY H:mm a" placeholder="Use the icon to the right to choose a date (month/day/year)"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <div id="what-kind-file">
                        <label>I want to paste in a link
                            <input type="checkbox" name="is_link" value="1" id="is_link" />
                        </label><br>
                        <label>I want to paste in a Wistia Embed
                            <input type="checkbox" name="is_wistia" value="1" id="is_wistia" />
                        </label>
                    </div>
                    <div class="input-group file-upload">
                        </label>
                        {{ Form::file('file') }}
                    </div>
                    <div class="input-group file-url">
                        {{ Form::input('text', 'url', Input::old('url'), ['class' => 'form-control', 'placeholder' => 'Paste Link or Embed Code Here']) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeFileInfoChange" data-dismiss="modal">Close</button>
                {{ Form::submit('Upload New Resource', array('class' => 'btn btn-primary basic-file-change-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<div id="loading">
    <div class="translucent"></div>
    <div class="uil-flickr-css" style="-webkit-transform:scale(0.8)">
        <div></div>
        <div></div>
    </div>
    <!--<div class="text">Loading resources...</div>-->
</div>
@stop
<style>
    .input-group.file-upload label { display:none;}
</style>
@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    {{ HTML::script('js/moment.js') }}
    {{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
    {{ HTML::script('js/jquery.session.js') }}
    {{ HTML::script('js/loading.js') }}
    {{ HTML::script('js/underscore.js') }}
    {{ HTML::script('js/library-scripts.js') }}
    <script>
        $('#category-filter-select').change(function () {
            //console.log('hit change');
            $('.filter-form').submit();
        });
        //ajax calls to each Pharmacy Document section tables and list its relevant data in the appropriate table
        $('.ajax-fill').on('click', function (e)
        {
            $('body').loading({
                message: "Retreiving Resources..."
            });

            var url_base = window.location.protocol + "//" + window.location.host;

            // run ajax request
            $.ajax({
                type: "POST",
                cache: false,
                url: url_base + "/admin/resources/tabledata/" + tableType,
                success: function (d) {
                   //console.log(d);
                },
                error: function (xhr, textStatus, thrownError) {
                    //$('.active-ajax').trigger("ajax.stop");
                    $('body').loading('stop');
                   //console.log(xhr);
                   //console.log(textStatus);
                   //console.log(thrownError);
                }
            }, "json");
        });
        $(window).load(function() {
            $('#loading').hide();
            console.log('here');
            $('.uv-icon').addClass('nothing');
        });
            $('.nothing').on('click', '.uv-icon', function() {
                console.log('what');
                $('.uv-popover-iframe').contents().find('#include_screenshot').prop('checked', true);
            });
    </script>
    @if($errors->count())
        <script>
            $('#addNewResource').modal('show');
        </script>
    @endif
@stop
