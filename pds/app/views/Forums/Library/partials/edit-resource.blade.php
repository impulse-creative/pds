@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/forum-styles.css') }}
	{{ HTMl::style('css/bootstrap-datetimepicker.min.css') }}
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop
@section('content')
<div class="container forum-container">
	<div class="col-lg-3 col-md-3">
		@include('Forums.Library.partials.resource-type-sidebar')
	</div>
	<div class="col-lg-9 col-md-9 resource-wrap">
		<div class="row title-row">
			<div class="col-lg-12 filtered-by">
				<p>
					<h2>Editing Resource: <span class="purple-text">{{$resource->filename}}</span></h1>
					<hr/>
				</p>
			</div>
		</div>
		<div class="row edit-section">
			@if(Session::has('Success'))
				<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
			@endif
			@if(Session::has('Failure'))
				<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
			@endif
			<div class="col-lg-8 col-md-8 col-xs-8">
				{{ Form::model($resource, ['route' => 'app.update.resource', 'class' => 'update-resource-form', 'files' => 'true']) }}
					{{ Form::hidden('id', $resource->id) }}
                                        {{ Form::hidden('user', Auth::id()) }}
					<label for="filename">Resource Name:</label>
					<div class="input-group">
						{{ Form::input('text', 'filename', Input::old('filename'), ['class' => 'form-control', 'placeholder' => 'File Name']) }}
					</div>
					<label for="resource_type">Resource Type:</label>
					<div class="input-group">
						{{ Form::select('resource_type', $resource_cats, $resource->resource_type, ['class' => 'form-control'])}}
					</div>
					@if(Auth::check())
                                            @if(in_array('1', explode(':', Auth::user()->groups)) || in_array('5', explode(':', Auth::user()->groups)) || in_array('9', explode(':', Auth::user()->groups)))
                                                <label for="access_level">Update Resource Container:</label>
                                                <div class="input-group">
                                                        {{ Form::select('access_level', $acl, $resource->resource_container, ['id' => 'resource_access', 'class' => 'form-control']) }}
                                                </div>
                                            @endif
					@endif
					@if(Auth::check())
                                            @if(in_array('1', explode(':', Auth::user()->groups)) || in_array('5', explode(':', Auth::user()->groups)) || in_array('9', explode(':', Auth::user()->groups)))
                                                <label for="access_level">Update Resource Access Level:</label>
                                                <div class="input-group">
                                                        {{ Form::select('permission', $access_list, $resource->access_level, ['id' => 'resource_access', 'class' => 'form-control']) }}
                                                </div>
                                            @endif
					@endif
				    <label for="description">Resource Description:</label>
					<div class="input-group">
				        <div id="wysihtml5-toolbar" style="display:none;">
				            <div class="btn-group">
				                <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
				                <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
				            </div>
				            <div class="btn-group">
				                <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
				                    <span class="glyphicon glyphicon-list"></span>
				                </a>
				                <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
				                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
				                </a>
				            </div>
				        </div>
						<textarea class="inp-field vendor-description-text" id="editor" name="description" cols="10" rows="5" placeholder="">{{ $resource->description }}</textarea>
						<input class="inp-field data-html" id="hidden-html" name="html_edits" type="hidden" value="">
                                                
                                            <label for="expiration">Expiration Date:</label>
                                            
                                            <div class='input-group date' id='datetimepicker5'>
                                                    <input name="expiration" value ="{{$resource->expiration}}" type='text' class="form-control" data-date-format="MM/DD/YYYY H:mm" placeholder="Use the icon to the right to choose a date (month/day/year)"/>
                                                    <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                            </div>
					</div>
                                    
					<div class="input-group">
						<div class="thumbnail file-upload">
							@if(in_array($resource->file_type, ['png', 'jpg', 'gif', 'ico']))
								<img id="user-img" src="{{ URL::to('storage/files/Library/fullsized/') }}/{{ $resource->resource_name }}" alt="{{ $resource->filename }}">
							@else
                                                        <?php $filename = $resource->resource_name;
                                                            $icon_list = ['avi', 'css', 'doc', 'eps', 'excel', 'mov', 'mp3', 'pdf', 'ppt', 'psd', 'txt', 'wav', 'zip'];
                                                            $icon_path = in_array($resource->file_type, $icon_list) ? URL::to('/').'/images/application/resources/icons/'.$resource->file_type.'.png' : URL::to('/').'/images/application/resources/icons/unknown.png'; 
                                                            ?>
                                                        <img id="custom-icon" src="{{ $icon_path }}" alt="{{$resource->filename}}"/>
							@endif
							<div class="caption">
								<p><a href="#" class="btn btn-default new-file-btn"><i class="fa fa-file-o"></i>&nbsp;Choose a New File?</a></p>
							</div>
						</div>
                                            <div id="what-kind-file">
                                                <label>I want to paste in a link
                                                    <input type="checkbox" name="is_link" value="1" id="is_link" {{ $checked = $resource->external_link == 1 ? 'checked="checked"' : '' }} />
                                                </label><br>
                                                <label>I want to paste in a Wistia Embed
                                                    <input type="checkbox" name="is_wistia" value="1" id="is_wistia" {{ $checked = $resource->wistia == 1 ? 'checked="checked"' : '' }} />
                                                </label>
                                            </div>
                                            
                                                <div class="input-group file-url">
                                                    {{ Form::input('text', 'url', $resource->resource_name, ['class' => 'form-control', 'placeholder' => 'Paste Link or Embed Code Here']) }}
                                                    <span>The system may have removed portions of your link.  <a target="blank" href="{{ $url = $resource->external_link == 1 ? $resource->resource_name : URL::to('library/resource/wistia/'.$resource->resource_name) }}">Verify your link works by clicking here</a></span>
                                                </div>
                                            
                                            
                                           
                                            
                                            <div class="new-file file-upload">
                                                <span class="btn btn-primary btn-file small-margin vcenter">
                                                    <form>
                                                            {{ Former::files('file', 'Browse For A New Resource', ['class' => 'btn-file'])->accept('gif', 'jpg', 'png', 'docx', 'pdf', 'csv') }}
                                                    </form>
                                                </span><!--
                                                --><div class="vcenter cancel-new-file small-margin">
                                                        <a href="#" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;Cancel</a>
                                                </div>
                                            </div>
                                            

                                            
                                            
						
						<div class="submit-edits">
							{{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
@stop

@section('extra-js')
	@parent
		{{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
		{{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
		{{ HTML::script('js/moment.js') }}
		{{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
		{{ HTML::script('js/library-scripts.js') }}
		<script>
			$('#category-filter-select').change(function(){
				//console.log('hit change');
				$('.filter-form').submit();
			});
		</script>
@stop