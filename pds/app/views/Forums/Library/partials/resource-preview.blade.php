@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/forum-styles.css') }}
    {{ HTMl::style('css/bootstrap-datetimepicker.min.css') }}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<div class="container forum-container">
    <div class="col-lg-12 col-md-12 col-sm-12 resource-wrap">
        <div class="row share-resource-view">
            @if(Session::has('Success'))
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'Success!') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'Something went wrong!') }}</div>
            @endif
            <h2 class="pull-left">Resource Preview</h2>
            <a href="{{URL::route('app.download.resource', $resource->resource_name)}}" class="pull-right btn btn-primary">
                <span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Download
            </a>
            <a href="{{ URL::route('library.index') }}" class="orange-text pull-right small-margin-waist" style="margin-top: 10px;">
                <i class="fa fa-arrow-left"></i>&nbsp;Go back to resources
            </a>
            <div class="clearfix"></div><hr>
            <div class="resource-preview col-lg-12">
                {{ $resource_html }}
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
<div id="loading">
    <div>Loading resources...</div>
</div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    {{ HTML::script('js/moment.js') }}
    {{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
    {{ HTML::script('js/library-scripts.js') }}
    <script style="display: none;">
        $('#category-filter-select').change(function () {
            $('.filter-form').submit();
        });
        $(window).load(function() {
            $('#loading').hide();
        });
        $('body').on('click', '#<?php echo $resource->id; ?>', function()
        {
            var baseUrl = window.location.protocol+"//"+window.location.host + '/resource-preview/log/preview/refresh';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="_token"]').attr('content')
                }
            });
            var boxview_id = '<?php echo $resource->id; ?>';
            $.ajax({
                type: 'post',
                cache: false,
                url: baseUrl,
                data: { boxview_id:boxview_id },
                success: function(data) {
                    console.log(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    console.log(xhr);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            }, "json");
        });
    </script>
@stop
