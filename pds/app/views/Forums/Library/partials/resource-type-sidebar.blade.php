<div class="forum-categories">
    <div class="forum-search">
        {{ Form::open(['route' => 'library.index', 'class' => 'forum-topic-search']) }}
        <div class="control-group">
            {{ Form::input('text', 'search_key', isset($search_key) ? $search_key : null, ['class' => 'topic-search inp-search-field form-control', 'placeholder' => 'search resources']) }}
        </div>
        {{ Form::close() }}
    </div>
    <div class="forum-category-list">
        <ul>  
            @foreach($file_types_array as $key => $type)
                @if($key == 0) <li class="cat-item item-{{ $key }}"><a href="{{ URL::route('library.index') }}">All Resources</a></li> <?php continue; ?> @endif
                <li class="cat-item item-{{ $key }}"><a href="{{ URL::to('library/category') }}?type={{$key}}">{{ $type }}</a></li>
            @endforeach
        </ul>
        @if(in_array('5', $user_permissions))
            {{ link_to_action('BoxView@destroy', 'Flush Boxview Storage', ['id' => '91fd000bb03d4d579ce572c592cbb6b3'], ['class' => 'btn btn-primary confirm', 'data-confirm' => 'Are you sure you want to flush this stuff?']) }}
            <p class="text-muted">Impulse Devs can only see this boxview flush link ;).</p>
        @endif
    </div>
    <div class="forum-category-list-mobile mobile-categories">
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="category-list-mobile" data-toggle="dropdown" aria-expanded="true">
                Select a Resource to View
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu resource-dropdown" role="menu" aria-labelledby="category-list-mobile">
                @foreach($file_types_array as $key => $type)
                    @if($key == 0) <li class="cat-item item-{{ $key }}"><a href="{{ URL::route('library.index') }}">All Resources</a></li> <?php continue; ?> @endif
                    <li class="cat-item item-{{ $key }}"><a href="{{ URL::to('library/category') }}?type={{$key}}">{{ $type }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="hs_forum_cta medium-padding">
        <!--HubSpot Call-to-Action Code -->
        <span class="hs-cta-wrapper" id="hs-cta-wrapper-fc1d9070-1875-4789-b40c-8089ec496835">
            <span class="hs-cta-node hs-cta-fc1d9070-1875-4789-b40c-8089ec496835" id="hs-cta-fc1d9070-1875-4789-b40c-8089ec496835">
                <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/fc1d9070-1875-4789-b40c-8089ec496835"><img class="hs-cta-img" id="hs-cta-img-fc1d9070-1875-4789-b40c-8089ec496835" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/fc1d9070-1875-4789-b40c-8089ec496835.png" /></a>
            </span>
            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
            <script type="text/javascript">
                hbspt.cta.load(37772, 'fc1d9070-1875-4789-b40c-8089ec496835');
            </script>
        </span>
        <!-- end HubSpot Call-to-Action Code -->
    </div>
</div>