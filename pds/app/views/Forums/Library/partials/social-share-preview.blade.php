@extends('layouts.pds-advantage')

@section('head')
@parent
{{ HTML::style('css/forum-styles.css') }}
{{ HTMl::style('css/bootstrap-datetimepicker.min.css') }}
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<div class="container forum-container">
    <div class="col-lg-3 col-md-3">
        @include('Forums.Library.partials.resource-type-sidebar')
    </div>
    <div class="col-lg-9 col-md-9 resource-wrap">
        <div class="row share-resource-view">
            @if(Session::has('Success'))
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
            <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
            @endif
            <?php
            $filename = $resource->resource_name;
            $icon_list = ['avi', 'css', 'doc', 'eps', 'excel', 'mov', 'mp3', 'pdf', 'ppt', 'psd', 'txt', 'wav', 'zip'];
            $icon_path = in_array($resource->file_type, $icon_list) ? URL::to('/images/application/resources/icons') . '/' . $resource->file_type . '.png' : URL::to('/images/application/resources/icons') . '/unknown.png';
            ?>
            
            <div class="social-preview col-lg-12">
                <div class="resource-info">
                    <div class="resource-icon">
                        <img src="{{ $icon_path }}"/>
                    </div>
                    <div class="resource-details">
                        <h2><strong>{{ $resource->filename }}</strong></h2>
                        <p class="inline-element f-name">{{ $resource->resource_name }}<p>
                        <p class="inline-element f-size">{{ $resource->file_size }}<p>
                            @if(in_array($resource->access_level, $user_permissions))
                                <?php $is_attachment = $resource->topic_id != 0 || (isset($resource->forum_message_id) && $resource->forum_message_id != 0) ? 'true' : 'false'; ?>
                                <a class="purple-text inline-element" href="{{ URL::route('app.download.resource', $resource->resource_name) }}?is_attachment={{$is_attachment}}">
                                    <span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Download
                                </a>
                            
                            @else
                                <p>You don't have permission to access this file</p>
                            @endif
                        <div class="clearfix"></div>
                        <a href="{{ URL::route('view.doc', ['file_id' => $resource->id]) }}" class="btn btn-primary pull-right small-margin small-margin-waist"><i class="fa fa-file-text-o"></i>&nbsp;Preview Resource</a>
                        <div class="r-description">
                            {{ $resource->description }}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="extra-details">
                    <div class="resource-cat col-lg-4 col-md-4">
                        <?php
                        $type = $resource->type->type_name;
                        if (preg_match('/_/', $type)) {
                            $type = str_replace('_', ' ', $type);
                        }
                        ?>
                        <p>Category:&nbsp;<span class="purple-text">{{ ucwords($type) }}</span></p>
                    </div>
                    <div class="resource-author col-lg-4 col-md-4">
                        <p>Author:&nbsp;<span class="purple-text">{{ $resource->user->fname }}&nbsp;{{ $resource->user->lname }}</span></p>
                    </div>
                    <div class="preview col-lg-4 col-md-4">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="resource-share">
                    <label for="link" class="pull-left"><i class="fa fa-share-alt"></i>&nbsp;Share Link:&nbsp;</label>
                    <div class="input-group">
                        <?php
                        $shareLink = $resource->resource_share_link;
                        
                        if(strpos($resource->resource_share_link, 'http://pds.impulsecreative.com') === 0) {
                            $shareLink = str_replace('http://pds.impulsecreative.com', Request::root(), $resource->resource_share_link);
                        } ?>
                        <input name="link" type="text" class="form-control r-share" value="{{$shareLink}}" data-toggle="tooltip" data-placement="top" data-original-title="Highlight and click ctrl+c (windows) or cmd+c (osx)"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>
<div id="loading">
    <div>Loading resources...</div>
</div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    {{ HTML::script('js/moment.js') }}
    {{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
    {{ HTML::script('js/library-scripts.js') }}
    <script>
        $('#category-filter-select').change(function () {
                //console.log('hit change');
                $('.filter-form').submit();
        });
        $(window).load(function() {
          $('#loading').hide();
        });
    </script>
@stop
