@extends('layouts.pds-advantage')

@section('head')
@parent
{{ HTML::style('css/forum-styles.css') }}
{{ HTMl::style('css/bootstrap-datetimepicker.min.css') }}
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section('content')
<?php

function shorten_string($string, $length) {
    if (strlen($string) > $length) {
        $string = trim(substr($string, 0, $length) . "...");
    }
    return $string;
}

function cleanForId($string) {
    $string = str_replace(' ', '_', $string); // Replaces all spaces with underscores.

    return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars and makes lower case
}
?>
{{-- variables --}}
<div class="container forum-container">
    <div class="col-lg-3 col-md-3">
        @include('Forums.Library.partials.resource-type-sidebar')
    </div>
    <div class="col-lg-9 col-md-9 resource-wrap">
        <div class="topic-list-information small-padding">
            <div class="col-lg-6 filtered-by">
                <p>
                <h1>Trashbin</h1>
                <hr/>
                </p>
            </div>
            @if(Auth::check())
                <?php $group_data = explode(':', Auth::user()->groups); ?>
                @if(in_array('6', $group_data) || in_array('1', $group_data) || in_array('11', $group_data) || in_array('5', $group_data))
                    <div class="col-lg-3 check-trash pull-right">
                        <a href="{{ URL::route('app.trashed.resource') }}" class="btn btn-default new-thread-btn"><i class="fa fa-trash-o"></i>&nbsp;Open Trashbin</a>
                    </div>
                @endif
            @endif
            <div class="clearfix"></div>
            <div class="trashcan-info">
                <p>Here you can restore or permanently destroy any file that you have deleted from the main resource list.</p>
            </div>
        </div>
        @if(Session::has('Success'))
            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
        @endif
        @if(Session::has('Failure'))
            <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
        @endif
        <div class="resource-wrap">
            @if(count($resources) > 0)
                @foreach($resources as $key => $resource)
                    <?php
                    $filename = $resource->filename;
                    $icon_list = ['avi', 'css', 'doc', 'eps', 'excel', 'mov', 'mp3', 'pdf', 'ppt', 'psd', 'txt', 'wav', 'zip'];
                    $icon_path = in_array($resource->file_type, $icon_list) ? URL::to('/') . '/images/application/resources/icons/' . $resource->file_type . '.png' : URL::to('/') . '/images/application/resources/icons/unknown.png';
                    ?>
                    <div class="featured-resource green-background">
                        <div class="resource-download vcenter">
                            <input type="checkbox" value={{$resource->id}} title="add to download list"/>
                        </div><!--
                        --><div class="resource-thumbnail vcenter">
                            <div class="thumbnail-img">
                                <img src="{{ $icon_path }}"/>
                            </div>
                        </div><!--
                        --><div class="resource-title vcenter">
                            <h2>{{ shorten_string($resource->filename, 25) }}&nbsp;<span class="muted-text">{{$resource->resource_name}}</span></h2>
                            <p>by {{ $resource->user->fname }}&nbsp;{{ $resource->user->lname }} on {{ date("m/d/Y", strtotime($resource->created_at)) }}</p>
                        </div><!--
                        --><div class="resource-downloads vcenter pull-right">
                            <p><a href="{{ URL::route('app.restore.resource', $resource->id) }}" class="purple-text"><span class="glyphicon glyphicon-floppy-open"></span>&nbsp;Restore</a>&nbsp;<a href="{{ URL::route('app.obliterate.resource', $resource->id) }}" class="red-text"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Destroy</a></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                @endforeach
                {{-- End of Resources --}}	
            @else
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>No resources found</div>
            @endif


        </div>
        <div class="hs_forum_cta_mobile medium-padding">
            <!--HubSpot Call-to-Action Code -->
            <span class="hs-cta-wrapper" id="hs-cta-wrapper-fc1d9070-1875-4789-b40c-8089ec496835">
                <span class="hs-cta-node hs-cta-fc1d9070-1875-4789-b40c-8089ec496835" id="hs-cta-fc1d9070-1875-4789-b40c-8089ec496835">
                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                    <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/fc1d9070-1875-4789-b40c-8089ec496835"><img class="hs-cta-img" id="hs-cta-img-fc1d9070-1875-4789-b40c-8089ec496835" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/fc1d9070-1875-4789-b40c-8089ec496835.png" /></a>
                </span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
hbspt.cta.load(37772, 'fc1d9070-1875-4789-b40c-8089ec496835');
                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
        <div class="forum-category-list-mobile mobile-categories">
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="category-list-mobile" data-toggle="dropdown" aria-expanded="true">
                    Select a Resource to View
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu resource-dropdown" role="menu" aria-labelledby="category-list-mobile">
                    @foreach($file_types_array as $key => $type)
                    @if($key == 0) <li class="cat-item item-{{ $key }}"><a href="{{ URL::route('library.index') }}">All Resources</a></li> <?php continue; ?> @endif
                    <li class="cat-item item-{{ $key }}"><a href="{{ URL::to('library/category') }}?type={{$key}}">{{ $type }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@stop

@section('extra-js')
@parent
{{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
{{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
{{ HTML::script('js/moment.js') }}
{{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
{{ HTML::script('js/library-scripts.js') }}
<script>
    $('#category-filter-select').change(function () {
        //console.log('hit change');
        $('.filter-form').submit();
    });
</script>
@stop