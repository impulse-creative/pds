@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/forum-styles.css') }}
	{{ HTMl::style('css/bootstrap-datetimepicker.min.css') }}
	
@stop

@section('content')
<?php 
$wistia = trim(strstr($div, '_'), '_');
?>
<div class="container forum-container">
    <div class="col-lg-3 col-md-3">
            @include('Forums.Library.partials.resource-type-sidebar')
    </div>
    <div class="col-lg-9 col-md-9 resource-wrap">
        <h1>Resource Library</h1>
        <hr/>
        <div id="{{$div}}" class="wistia_embed" style="width:640px;height:360px;"> </div>
        <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>
        <script>
        wistiaEmbed = Wistia.embed("{{$wistia}}");
        </script>
    </div>
            
      
</div>
  

@stop
