@extends('layouts.pds-advantage')

@section("content")
<div id="group-list-container">
	<div class="col-lg-12 col-md-12 col-xs-12 forum-data-container">
    	<h2>Editing Topic: <span class="purple-text">{{ $topic->title }}</span></h2>
		<hr/>
		@if(Session::has('Success'))
			<div class="col-lg-10 col-md-6">
				<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
			</div>
		@endif
		@if(Session::has('Failure'))
			<div class="col-lg-10 col-md-6">
				<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
			</div>
		@endif
		<div class="col-lg-6 col-md-6 col-xs-6 edit-container">
			<div class="return-to-manager small-padding">
				<a href="{{ URL::route('forum.manager.index') }}" class="small-padding orange-text"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Return to Manager</a>
			</div>
			{{ Form::model($topic, ['url' => URL::route('edit.forum.topic', $topic->id).'?edit=1', 'class' => 'updateForumTopic', 'role' => 'form'])}}
				<div class="form-group">
					<label for="title">Topic Title</label>
					{{ Form::input('text', 'title', Input::old('title'), ['placeholder' => 'Topic Title', 'class' => 'form-control']) }}
					@if($errors->has('title')) <p class="help-block red-text">{{ $errors->first('title') }}</p><div class="clearfix"></div> @endif
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control description" id="editor" name="description" cols="10" rows="5" placeholder="Include a description for your topic that engages the user's thoughts and provokes them to join the conversation.">{{ $topic->description }}</textarea>
					@if($errors->has('description')) <p class="help-block red-text">{{ $errors->first('description') }}</p><div class="clearfix"></div> @endif
				</div>
				<div class="form-group">
					<label for="author_id">Author</label>
					{{ Form::select('author_id', $authors, $topic->author_id, ['class' => 'form-control author-dropdown']) }}
				</div>
				<div class="form-submit">
					{{ Form::submit('Save Changes', ['class' => 'btn btn-default']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop

@section('extra-js')
	@parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
	{{ HTML::script('js/forum-scripts.js') }}
	@if($errors->has('author_id'))
		<script type="text/javascript">
		    $(window).load(function(){
		        $('#createTopicCategory').modal('show');
		    });
		</script>
	@endif
@stop