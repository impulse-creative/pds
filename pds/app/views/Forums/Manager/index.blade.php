@extends('layouts.pds-advantage')

@section("content")
<div id="group-list-container">

    <div class="col-md-12 forum-data-container">
        <div id="group-list-header">
            <div class="forum-manager-title">
                <h1 class="pull-left">Message Board Manager&nbsp;</h1><span><a href="{{ URL::route('forum.index') }}" class="orange-text pull-left" target="_blank"><span class="glyphicon glyphicon-link"></span>&nbsp;Visit the Message Boards</a></span>
                <div class="clearfix"></div>
                <hr>
            </div>
            @if($errors)		
            <div class="col-lg-10 col-md-6">
                @if($errors->has('title'))
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $errors->first('title') }}</div>
                @elseif($errors->has('subtitle'))
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $errors->first('subtitle') }}</div>
                @endif
            </div>
            @endif
            @if(Session::has('Success'))
            <div class="col-lg-10 col-md-6">
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
            </div>
            @endif
            @if(Session::has('Success Remove'))
            <div class="col-lg-10 col-md-6">
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success Remove', 'default') }}</div>
            </div>
            @endif
        </div>
        <div class="clearfix"></div>

        <h2 class="pull-left">Categories</h2>&nbsp;&nbsp;
        <div class="new-category-container col-lg-4 small-padding small-waist">
            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#createForumCategory" >
                <span class="glyphicon glyphicon-plus"></span>&nbsp;Add Category
            </a>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="forum-panel-group panel-group" id="accordion">
            <?php $check_first = 0; ?>
            @foreach ($categories as $key => $value)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title col-lg-12">
                        <a class="white-text" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$value->id}}">
                            {{{ $value->title }}}&nbsp;
                            <?php
                            $choice_change = $value->admin_only == 1 ? 0 : 1;
                            $icon = $value->admin_only == 1 ? 'glyphicon-eye-open' : 'glyphicon-eye-close';
                            $alert = $value->admin_only == 1 ? 'public' : 'private';
                            ?>
                            @if($value->admin_only == 1)<span class="orange-text">( Members Only )</span>@endif
                            &nbsp;
                            <a class="pull-right confirm orange-text" href="{{ URL::to('dashboard/manage/pds-forum/remove/restriction') }}?choice={{$choice_change}}&cat_id={{ $value->id }}" data-confirm="Are you sure you want to make this category {{$alert}}?" title="Make Category {{ $alert }}">
                                <span class="glyphicon {{$icon}}"></span>&nbsp;Click to make {{$alert}}
                            </a>
                        </a>
                    </h4>
                    <div class="clearfix"></div>
                </div>
                <div id="collapse{{$value->id}}" class="panel-collapse collapse">
                    <div class="panel-body ">
                        <?php $topics = \ForumTopics::where('parent_category', $value->id)->paginate(10); ?>
                        <div class="table-responsive col-lg-12 col-md-12 col-xs-12">
                            <p>{{ $value->subtitle }}</p>
                            <div class="topic-manager-pagination">
                                {{ $topics->fragment('collapse'.$value->id)->links() }}
                            </div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Edit</th>
                                        <th>Topic</th>
                                        <th>Author</th>
                                        <th>Views</th>
                                        <th>Replies</th>
                                        <th>Featured</th>
                                        <th>Close Topic</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($topics as $key => $val)
                                    <tr>
                                        <td><a class="purple-text edit-topic" href="{{ URL::route('edit.forum.topic', $val->id) }}" data-topic-id="{{ $val->id }}"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                        <?php $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $val->title); ?>
                                        <td><a class="purple-text admin-topic-title" href={{ URL::to('/forum/post').'/'.preg_replace('/ /', '-', $scrubbed_title).'/'.$val->id }} target="_blank">{{ $val->title }}</a></td>
                                        <?php $topic_author = User::find($val->author_id); ?>
                                        <td>{{ $topic_author->fname }}&nbsp;{{ $topic_author->lname }}</td>
                                        <td class="center"><span class="glyphicon glyphicon-eye-open purple-text"></span>&nbsp;{{{ $val->post_views }}}</td>
                                        <td class="center"><span class="glyphicon glyphicon-comment purple-text"></span>&nbsp;<a href={{URL::to('/forum/post/').'/'.preg_replace('/ /', '-', $scrubbed_title)."/".$val->id."#disqus_thread" }}>0</a></td>
                                        <td class="center">
                                            <?php
                                            $type = $val->featured == 0 ? 1 : 0;
                                            $icon = $val->featured == 0 ? '-empty' : '';
                                            $color = $val->featured == 0 ? 'purple-text' : 'orange-text';
                                            ?>
                                            <a href="{{ URL::to('dashboard/manage/pds-forum/update/featured/topic') }}?update_type={{$type}}&id={{$val->id}}" class="{{$color}}"><span class="glyphicon glyphicon-star{{$icon}}"></span></a>
                                        </td>
                                        <td class="center"><a class="red-text confirm" href={{ URL::route('destroy.forum.topic', ['id' => $val->id])}} data-confirm="Are you sure you want to delete this topic?"><span class="glyphicon glyphicon-remove"></span></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="new-category-container col-lg-6 col-md-6 col-xs-6 small-padding small-waist">
                            <a class="btn btn-success add-topic-btn small-margin" data-toggle="modal" data-cat-id="{{ $value->id }}" data-target="#createTopicCategory" >
                                <span class="glyphicon glyphicon-plus"></span>&nbsp;Add Topic
                            </a>
                            <a class="btn btn-primary edit-category small-margin" data-toggle="model" data-target="#updateCategory" data-category-id="{{ $value->id }}">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit Category
                            </a>
                            <a href="{{ URL::route('destroy.forum.category', ['cat_id' => $value->id]) }}" class="btn btn-danger small-margin confirm" data-confirm="Are you sure you want to remove this category?">
                                <span class="glyphicon glyphicon-remove"></span>&nbsp;Remove Category
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
</div>


{{-- Add New Category --}}
<div class="modal fade" id="createForumCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Create New Category</h4>
            </div>
            <div class="modal-body">
                <div class="validation-warning"></div>
                {{ Form::open(array('route' => 'create.forum.category', 'class' => 'createForumCategoryForm', 'novalidate' => '')) }}
                <input name="title" id="title" type="text" class="inp-field" placeholder="Category Title" value="{{ Input::old('title') }}"/><br/>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('title') }}</p> @endif
                <textarea name="subtitle" id="subtitle" class="inp-field" placeholder="Sub-title or Description" value="{{ Input::old('subtitle') }}"></textarea><br/>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('subtitle') }}</p> @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            {{ Form::label('category_icon', 'Choose Tag:', ['class' => 'modal-label']) }}
                            {{ Form::select('category_icon', 
						['null' => '-- Please select a choice --',
						 'http://cdn2.hubspot.net/hub/37772/file-1808744602-png/Lead-white-small.png?t=1414692402273' => 'Lead',
						 'http://cdn2.hubspot.net/hub/37772/file-1808744597-png/Innovate-White-small.png?t=1414692402273' => 'Innovate',
						 'http://cdn2.hubspot.net/hub/37772/file-1808744607-png/Connect-white-small.png?t=1414692402273' => 'Connect'], null, ['class' => 'inp-field author-dropdown']) }}
                            <br/>
                            <input name="admin_only" type="radio" class="member-rad-btn" value="1">&nbsp;
                            <label for="admin_only">Members Only?</label><br/>
                            
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeNewCategoryCreator" data-dismiss="modal">Close</button>
                {{ Form::submit('Create', array('class' => 'btn btn-primary new-category-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


{{-- Add New Topic --}}
<div class="modal fade" id="createTopicCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Create New Topic</h4>
            </div>
            <div class="modal-body">
                <div class="validation-warning"></div>
                {{ Form::open(array('route' => 'create.forum.topic', 'class' => 'createTopicCategory', 'novalidate' => '')) }}
                {{ Form::hidden('category_choice', null, ['class' => 'categoryTopic_hidden_id'])}}
                <div class="row">
                    <div class="col-lg-12">
                        <input name="title" id="title" type="text" class="inp-field" placeholder="What topic would spark an amazing conversation?" value="{{ Input::old('title') }}"/><br/>
                        <textarea class="inp-field description" id="editor" name="description" cols="10" rows="5" placeholder="Include a description for your topic that engages the user's thoughts and provokes them to join the conversation."></textarea>
                    </div>
                </div>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('title', 'Please add a topic title to continue.') }}</p> @endif
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            {{ Form::label('author_id', 'Author', ['class' => 'modal-label']) }}
                            {{ Form::select('author_id', $authors, Auth::id(), ['class' => 'inp-field author-dropdown']) }}
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeNewTopicCreator" data-dismiss="modal">Close</button>
                {{ Form::submit('Create', array('class' => 'btn btn-primary new-topic-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

{{-- Update Category --}}
<div class="modal fade" id="updateCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Update Category</h4>
            </div>
            <div class="modal-body">
                <div class="validation-warning"></div>
                {{ Form::open(array('route' => 'update.forum.category', 'class' => 'updateCategory', 'novalidate' => '')) }}
                {{ Form::hidden('cat_id', null, ['class' => 'category_hidden_id']) }}
                <input name="title" id="title" type="text" class="inp-field cat-title" placeholder="Category Title" value="{{ Input::old('title') }}"/><br/>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('title') }}</p> @endif
                <textarea name="subtitle" id="subtitle" class="inp-field cat-subtitle" placeholder="Sub-title or Description" value="{{ Input::old('subtitle') }}"></textarea><br/>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('subtitle') }}</p> @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <input name="admin_only" type="radio" class="member-rad-btn" value="1">&nbsp;
                            <label for="admin_only">Members Only?</label><br/><span class="member-rad-btn"><i>*If you don't select 'Members Only', your previous choice will be used by default.</i></span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeUpdateCategory" data-dismiss="modal">Close</button>
                {{ Form::submit('Update', array('class' => 'btn btn-primary update-topic-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    {{ HTML::script('js/forum-scripts.js') }}
    <script>
        $(document).ready(function() {
            var anchor = window.location.hash.replace("#", "");
            $("#" + anchor).collapse('show');
        });
    </script>
    @if($errors->has('author_id'))
    <script type="text/javascript">
        $(window).load(function () {
            $('#createTopicCategory').modal('show');
        });
    </script>
    @endif
@stop