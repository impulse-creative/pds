@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/forum-styles.css') }}
@stop

@section('content')
	<div class="container small-padding">
		<div class="pagination-wrap center">
			@if(isset($categories) && !isset($recent_method) && !isset($category_id))
				{{ $categories->links() }}
			@endif
		</div>
	</div>
	<div class="container medium-padding">
		{{--<span><a href="{{ URL::route('forum.manager.index') }}" class="orange-text"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;To Dashboard (temp anchor)</a></span>--}}
		<div class="filter-container well well-sm">
			{{ Form::open(['route' => 'forum.filter.cat', 'class' => 'filter-form']) }}
				<div class="input-group col-lg-2 col-md-2 col-sm-2 pull-left">
					@if(!empty($categoriesArray))
						<label class="small-margin">Latest topics...</label>
						<a href="{{ URL::action('ForumController@getRecentTopics') }}" class="btn btn-primary recent-topics-btn"><span class="glyphicon glyphicon-tasks"></span>&nbsp;View Recent Topics</a>
					@endif
				</div>
				<div class="input-group col-lg-3 col-md-3 col-sm-3 pull-left">
					@if(isset($category_id))
						<?php $cat_choice = $category_id; ?>
					@else
						<?php $cat_choice = 1; ?>
					@endif
					@if(!empty($categoriesArray))
						<label class="small-margin" for='category'>or filter by category</label>
						{{ Form::select('category', $categoriesArray, $cat_choice, ['id' => 'category-filter-select', 'class' => 'inp-field']) }}
					@else
						<div class="small-margin orange-text">No categories or topics were found</div>
					@endif
				</div>
				<div class="input-group col-lg-2 col-md-2 col-sm-2 pull-right">
					<a href="{{ URL::route('forum.index') }}" class="btn btn-default reset-btn">Reset List</a>
				</div>
				<div class="clearfix"></div>
				<div class="input-group col-lg-2">
					{{-- Form::submit('Filter', ['class' => 'btn btn-default filter-submit-btn']) --}}
				</div>
			{{ Form::close() }}
		</div>
		@if(isset($recent_posts) && isset($recent_method))
			<div class="panel panel-default">
				<div class="panel-heading">
					<a class="white-text"><h2>Recent Discussions</h2></a>
				</div>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<div>
								<tr>
									<th>Topic</th>
									<th>Author</th>
									<th>Views</th>
									<th>Created At</th>
									<th>Replies</th>
								</tr>
							</div>
						</thead>
						<tbody>
							@if (count($recent_posts) > 0)
								@foreach($recent_posts as $topic)
									<?php $author = User::find($topic->author_id); ?>
									<tr class="topic-data">
										<td>
											<?php
												$scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $topic->title);
											?>
											<a class="purple-text" href={{URL::to('/forum/post/')."/".preg_replace('/ /', '-', $scrubbed_title)."/".$topic->id}}>{{{ $topic->title }}}</a>
										</td>
										<td>{{{ $author->fname }}}&nbsp;{{{ $author->lname }}}</td>
										<td>{{{ $topic->post_views }}}</td>
										<td>{{{  date("F d, Y", strtotime($topic->updated_at)) }}} at {{{ date("g:i a",strtotime($topic->updated_at)) }}}</td>
										<td><a href={{URL::to('/forum/post/')."/".preg_replace('/ /', '-', $scrubbed_title)."/".$topic->id."#disqus_thread" }}>0</a></td>
									</tr>
								@endforeach
							@else
								<tr>
									<th colspan="3">
										No topics found
									</th>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		@endif
		{{-- CATEGORY FILTER SECTION | SHOWS SELECTED CATEGORY WITH RELATED TOPICS --}}
		@if(isset($sub_topics) && isset($category_id))
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php
						$parent = $sub_topics->count() > 0 ? $sub_topics[0]->parent_category : 0;
						if(!$parent) $parent = $category_id;
						$category = ForumCategories::find($parent);
					?>
					<a class="white-text">
						<p class="category-list-title">
						@if(isset($category->icon) && $category->icon != 'null')
							<div class="category-icon pull-left"><img src="{{ $category->icon }}" alt="Category icon"></div>&nbsp;
						@else
							<span class="glyphicon glyphicon-bullhorn"></span>&nbsp;
						@endif
						{{{ $category->title }}}</p>
					</a>
				</div>
				<div class="panel-body">{{{ $category->subtitle }}}</div>
				<div class="table-responsive">
					<table class="table table-condensed table-striped">
						<thead>
							<div>
								<tr>
									<th>Topic</th>
									<th>Author</th>
									<th>Views</th>
									<th>Created At</th>
									<th>Replies</th>
									@if(Auth::check())
										<?php $user_groups = explode(':', Auth::user()->groups); ?>
										@if(in_array('5', $user_groups) || in_array('6', $user_groups))
											<th>Report</th>
										@endif
									@endif
								</tr>
							</div>
						</thead>
						<tbody>
							@if (count($sub_topics) > 0)
								@foreach($sub_topics as $topic)
									<?php $author = User::find($topic->author_id); ?>
									<tr class="topic-data">
										<td>
											<div class="subtopic_title col-md-12">
												<?php
													$scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $topic->title);
												?>
												<a class="purple-text" href={{URL::to('/forum/post/')."/".preg_replace('/ /', '-', $scrubbed_title)."/".$topic->id }}>{{{ $topic->title }}}</a>
											</div>
										</td>
										<td>{{{ $author->fname }}}&nbsp;{{{ $author->lname }}}</td>
										<td>{{{ $topic->post_views }}}</td>
										<td>{{{  date("F d, Y", strtotime($topic->updated_at)) }}} at {{{ date("g:i a",strtotime($topic->updated_at)) }}}</td>
										<td><a href={{URL::to('/forum/post/')."/".preg_replace('/ /', '-', $scrubbed_title)."/".$topic->id."#disqus_thread" }}>0</a></td>
										@if(Auth::check())
											<?php $user_groups = explode(':', Auth::user()->groups); ?>
											@if(in_array('5', $user_groups) || in_array('6', $user_groups))
												<td><a href="#" class="red-text"><span class="glyphicon glyphicon-flag"></span></a></td>
											@endif
										@endif
									</tr>
								@endforeach
							@else
								<tr>
									<th colspan="3">
										No topics found
									</th>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
				<div class="clearfix"></div>
				@if(Auth::check())
					<?php $user_groups = explode(':', Auth::user()->groups); ?>
					@if(in_array('5', $user_groups) || in_array('6', $user_groups))
						<a class="btn btn-success add-topic-btn small-margin" data-toggle="modal" data-cat-id="{{ $category->id }}" data-target="#createTopicCategory" >
			      			<span class="glyphicon glyphicon-plus"></span>&nbsp;Add Topic
			      		</a>
		      		@endif
		      	@endif
			</div>
		@endif
		{{-- MAIN CATEGORY LIST SECTION | SHOWS CATEGORIES WITH TOPICS --}}
		@if(isset($categories) && !isset($category_id) && !isset($recent_method))
			@foreach ($categories as $category)
				<div class="panel panel-default">
					<div class="panel-heading">
						<a class="white-text">
							<p class="category-list-title">
							@if(isset($category->icon) && $category->icon != 'null')
								<div class="category-icon pull-left"><img src="{{ $category->icon }}" alt="Category icon"></div>&nbsp;
							@else
								<span class="glyphicon glyphicon-bullhorn"></span>&nbsp;
							@endif
							{{{ $category->title }}}</p>
						</a>
					</div>
					<div class="panel-body">{{{ $category->subtitle }}}</div>
					<div class="table-responsive">
						<table class="table table-condensed table-striped">
							<thead>
								<tr>
									<th>Topic</th>
									<th>Author</th>
									<th>Views</th>
									<th>Created At</th>
									<th>Replies</th>
									@if(Auth::check())
										<?php $user_groups = explode(':', Auth::user()->groups); ?>
										@if(in_array('5', $user_groups) || in_array('6', $user_groups))
											<th>Report</th>
										@endif
									@endif
								</tr>
							</thead>
							<tbody>
								<?php
									//get topics for the current category section 
									$topics_data = ForumTopics::where('parent_category', $category->id)->get();
								?>
								@if (count($topics_data) > 0)
									@foreach($topics_data AS $subtopic)
										<?php $author = User::find($subtopic->author_id); ?>
										<tr class="topic-data">
											<td>
												<div class="subtopic_title col-md-12">
													<?php
														$scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $subtopic->title);
													?>
													<a class="purple-text" href={{URL::to('/forum/post/')."/".preg_replace('/ /', '-', $scrubbed_title)."/".$subtopic->id }}>{{{ $subtopic->title }}}</a>
												</div>
											</td>
											<td>{{{ $author->fname }}}&nbsp;{{{ $author->lname }}}</td>
											<td>{{{ $subtopic->post_views }}}</td>
											<td>{{{  date("F d, Y", strtotime($subtopic->updated_at)) }}} at {{{ date("g:i a",strtotime($subtopic->updated_at)) }}}</td>
											<td><a href={{URL::to('/forum/post/')."/".preg_replace('/ /', '-', $scrubbed_title)."/".$subtopic->id."#disqus_thread" }}>0</a></td>
											@if(Auth::check())
												<?php $user_groups = explode(':', Auth::user()->groups); ?>
												@if(in_array('5', $user_groups) || in_array('6', $user_groups))
													<td><a href="#" class="red-text"><span class="glyphicon glyphicon-flag"></span></a></td>
												@endif
											@endif
										</tr>
									@endforeach
								@else
									<tr>
										<th colspan="3">
											No topics found
										</th>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
					<div style="clear:both;"></div>
					@if(Auth::check())
						<?php $user_groups = explode(':', Auth::user()->groups); ?>
						@if(in_array('5', $user_groups) || in_array('6', $user_groups))
							<a class="btn btn-success add-topic-btn small-margin" data-toggle="modal" data-cat-id="{{ $category->id }}" data-target="#createTopicCategory" >
				      			<span class="glyphicon glyphicon-plus"></span>&nbsp;Add Topic
				      		</a>
			      		@endif
			      	@endif
				</div>
			@endforeach
		@endif
	</div>
	<div class="containerd small-padding">
		<div class="pagination-wrap center">
			@if(isset($categories) && !isset($recent_method) && !isset($category_id))
				{{ $categories->links() }}
			@endif
		</div>
	</div>


{{-- Add New Topic --}}
<div class="modal fade" id="createTopicCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="modalLabel">Create New Topic</h4>
      </div>
      <div class="modal-body">
        <div class="validation-warning"></div>
        {{ Form::open(array('route' => 'create.forum.topic', 'class' => 'createTopicCategory', 'novalidate' => '')) }}
        	{{ Form::hidden('cat_id', null, ['class' => 'categoryTopic_hidden_id'])}}
            <input name="title" id="title" type="text" class="inp-field" placeholder="What topic would spark an amazing conversation?" value="{{ Input::old('title') }}"/><br/>
            @if($errors) <p class="red-text small-waist">{{ $errors->first('title', 'Please add a topic title to continue.') }}</p> @endif
			<div class="row">
			  <div class="col-lg-6">
			    <div class="input-group">
			    	{{ Form::label('author_id', 'Author', ['class' => 'modal-label']) }}
					{{ Form::select('author_id', $authors, null, ['class' => 'inp-field author-dropdown']) }}
					{{ Form::hidden('main_view', 0) }}
			    </div><!-- /input-group -->
			  </div><!-- /.col-lg-6 -->
			</div><!-- /.row -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default closeNewTopicCreator" data-dismiss="modal">Close</button>
        {{ Form::submit('Create', array('class' => 'btn btn-primary new-topic-submit')) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
@stop


@section('extra-js')
	@parent
		<script type="text/javascript">
			/* * * Add disqus site shortname here  * * */
			var disqus_shortname = 'pdsposts'; // required: replace example with your forum shortname
			/* * * Don't make any change for bellow lines * * */
			(function () {
			var s = document.createElement('script'); s.async = true;
			s.type = 'text/javascript';
			s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
			(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
			}());
		</script>
		<script>
			$('#category-filter-select').change(function(){
				//console.log('hit change');
				$('.filter-form').submit();
			});
		</script>
@stop