<div class="forum-categories">
    <div class="forum-search">
        {{ Form::open(['route' => 'forum.search.private.boards', 'class' => 'forum-topic-search']) }}
        <div class="control-group">
            {{ Form::input('text', 'search_key', Input::old('search_key'), ['class' => 'topic-search inp-search-field form-control', 'placeholder' => 'search the message board']) }}
        </div>
        {{ Form::close() }}
    </div>
    <div class="forum-category-list">
        <ul>
            @if($forum_data['companies'])
            @foreach($forum_data['companies'] as $companyId => $employeeType)
            <li class="divider purple-text"><strong>{{ Company::find($companyId)->name }}</strong></a></li>
                @foreach($forum_data['privateCategories'] as $categoryId => $categoryName)
                @if($categoryId != 0)
                    <li class="cat-item private {{ $active = (Input::get('category') == $categoryId && Input::get('companyId') == $companyId) ? ' active-category' : '' }}">
                        <a href="{{ URL::to('message-boards/private/company') }}?category={{$categoryId}}&companyId={{$companyId}}">{{ $categoryName }}</a>
                    </li>
                @endif
                @endforeach
            @endforeach
            @endif
        </ul>
    </div>
    @if(Auth::check())
        @if(in_array('1', $forum_data['usr_permissions']) || in_array('5', $forum_data['usr_permissions']))
            <div class="new-category-container col-lg-4 small-padding small-margin-waist inline-element">
                <a href="#" class="btn btn-primary mobile-new-category-btn pull-left" data-toggle="modal" data-target="#createForumCategory">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;Create New Category
                </a>
                <a href="#" data-target="#createTopic" data-toggle="modal" class="btn btn-primary mobile-new-thread-btn pull-right">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;Create New Topic
                </a>
            </div>
        @endif
    @endif
    <div class="forum-category-list-mobile mobile-categories col-lg-4 inline-element">
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="category-list-mobile" data-toggle="dropdown" aria-expanded="true">
                Select a Category to View
                <span class="caret"></span>
            </button>
            
            <ul class="dropdown-menu resource-dropdown" role="menu" aria-labelledby="category-list-mobile">
                @if($forum_data['companies'])
                    @foreach($forum_data['companies'] as $companyId => $employeeType)
                        <li class="cat-item purple-text"><strong>{{ Company::find($companyId)->name }}</strong></a></li>
                        @foreach($forum_data['privateCategories'] as $categoryId => $categoryName)
                        @if($categoryId != 0)
                            <li class="cat-item private">
                                <a href="{{ URL::to('message-boards/private/company/') }}?category={{$categoryId}}">{{ $categoryName }}</a>
                            </li>
                        @endif
                        @endforeach
                        <li class="divider"></li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="hs_forum_cta medium-padding">
        <!--HubSpot Call-to-Action Code -->
        <span class="hs-cta-wrapper" id="hs-cta-wrapper-fc1d9070-1875-4789-b40c-8089ec496835">
            <span class="hs-cta-node hs-cta-fc1d9070-1875-4789-b40c-8089ec496835" id="hs-cta-fc1d9070-1875-4789-b40c-8089ec496835">
                <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/fc1d9070-1875-4789-b40c-8089ec496835"><img class="hs-cta-img" id="hs-cta-img-fc1d9070-1875-4789-b40c-8089ec496835" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/fc1d9070-1875-4789-b40c-8089ec496835.png" /></a>
            </span>
            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
            <script type="text/javascript">
hbspt.cta.load(37772, 'fc1d9070-1875-4789-b40c-8089ec496835');
            </script>
        </span>
        <!-- end HubSpot Call-to-Action Code -->
    </div>
</div>

{{-- Add New Category --}}
<div class="modal fade" id="createForumCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Create New Category</h4>
            </div>
            <div class="modal-body">
                <div class="validation-warning"></div>
                {{ Form::open(array('route' => 'create.forum.category', 'class' => 'createForumCategoryForm', 'novalidate' => '')) }}
                <input name="title" id="title" type="text" class="inp-field" placeholder="Category Title" value="{{ Input::old('title') }}"/><br/>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('title') }}</p> @endif
                <textarea name="subtitle" id="subtitle" class="inp-field" placeholder="Sub-title or Description" value="{{ Input::old('subtitle') }}"></textarea><br/>
                @if($errors) <p class="red-text small-waist">{{ $errors->first('subtitle') }}</p> @endif
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <input name="admin_only" type="checkbox" class="member-rad-btn" value="1">&nbsp;
                            <label for="admin_only">Members Only?</label><br/><br/>
                            <input name="mypharmacy" type="checkbox" class="member-rad-btn" value="1">&nbsp;
                            <label for="mypharmacy">Private / MyPharmacy Category?</label><br/><br/>
                            {{ Form::label('category_icon', 'Choose Tag:', ['class' => 'modal-label']) }}
                            {{ Form::select('category_icon',
                                            ['null' => '-- Please select a choice --',
                                             'http://cdn2.hubspot.net/hub/37772/file-1808744602-png/Lead-white-small.png?t=1414692402273' => 'Lead',
                                             'http://cdn2.hubspot.net/hub/37772/file-1808744597-png/Innovate-White-small.png?t=1414692402273' => 'Innovate',
                                             'http://cdn2.hubspot.net/hub/37772/file-1808744607-png/Connect-white-small.png?t=1414692402273' => 'Connect'], null, ['class' => 'inp-field author-dropdown']) }}
                        </div><!-- /input-group -->
                        <div class="access_level_group input-group">
                            {{ Form::label('min_access_level', 'Choose Minimum Access Level:', ['class' => 'category-access-level']) }}
                            {{ Form::select('min_access_level', $forum_data['acl'], Input::old('min_access_level'), ['class' => 'inp-field author-dropdown']) }}
                        </div>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeNewCategoryCreator" data-dismiss="modal">Close</button>
                {{ Form::submit('Create Category', array('class' => 'btn btn-primary new-category-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
