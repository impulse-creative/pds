@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/forum-styles.css') }}
@stop

@section('content')

<div class="container forum-container">
	<div class="col-lg-3 col-md-3">
		
	</div>
	<div class="col-lg-9 col-md-9">
		<div class="update-topic-reply">
			@if(Session::has('Success'))
				<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
			@endif
			@if(Session::has('Failure'))
				<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
			@endif

			{{-- reply box --}}
			@if(Auth::check())
				<div id="replyBox" class="reply-to-post small-padding">
					<h1 class="purple-text small-padding">Update Your Reply</h1>
                                        {{ Form::model($message, ['url' => URL::to('message-boards/update/user/reply/'.$message->id.$private = Input::get('private') != null && Input::get('private') == 'true' ? '?private=true&category='.Input::get('category').'&companyId='.Input::get('companyId') : '' ), 'class' => 'add-reply'])}}
                                            {{ $reply_editor }}
                                            @if($errors->has('data')) <p class="red-text">{{ $errors->first('data', 'Please fill the required field below. <a href="#" class="orange-text">Trying to delete your comment?</a>') }}</p> @endif
                                            {{ Form::submit('Update Reply', ['class' => 'btn btn-default']) }}
                                        {{ Form::close() }}
				</div>
			@endif

		</div>
	</div>
</div>
<div class="clearfix"></div>
@stop

@stop


@section('extra-js')
	@parent
		{{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
		{{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
		<script>
			$('#category-filter-select').change(function(){
				//console.log('hit change');
				$('.filter-form').submit();
			});
		</script>
@stop