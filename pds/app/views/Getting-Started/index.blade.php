@extends('layouts.pds-advantage')

@section("content")


  <div class="container forum-container">
  
    <div class="col-sm-12 dash-getting-started">
        <div id="getting-started-header">
            <h1>Getting Started</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="getting-started-display">
            <p>Here is an introduction on how to use the application!</p>
            
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td class="center">Task</td>
                        <td class="center">Description</td>
                        <td>Completed</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($achievements as $key => $value)
                    <tr>
                        
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->description }}</td>
                        <td class="center">{{ $complete = array_key_exists($value->id, $completedAchievements) ? '<span class="glyphicon glyphicon-check"></span>' : '<span class="glyphicon glyphicon-ban-circle"></span>' }} </td>
                        
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if($sections['getting-started'] != 0)
            <h2>That means you only have {{$sections['getting-started']}} left!!</h2>
            @endif
        </div>
    </div>
  </div>

        
        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop