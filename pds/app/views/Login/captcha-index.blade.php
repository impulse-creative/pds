
@extends("layouts.Login.login-layout")
@section("content")
{{-- main content section | .main-content --}}
<section id="main-content" class="container-fluid">
	<div id="call-us" class="pull-right">
                <span class="glyphicon glyphicon-earphone" style="margin-right:5px;"></span><h1> <a class="white-text" href="tel:8009877386">(800) 987-7386</a></h1>
	</div> 
    <div id="login-container">
        <div class="pds-logo">
            <img src="{{ asset('images/application/app-login-logo.png') }}" alt="PDS Company Logo" id="form-logo" />
        </div>
        <!-- Divider -->
        <hr/>
        <!-- form to join/login -->
        {{ Form::open(array( 'url' => 'login', 'id' => 'login-form'))}}
            {{ Form::token() }}
            @if(Session::has('Unsuccessful Login'))
                    <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Unsuccessful Login', 'default') }} </div>
            @endif
            @if(Session::has('AlreadyRegistered'))
                <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('AlreadyRegistered', 'default') }} </div>
            @endif
            @if(Session::has('Incomplete'))
                    {{ Session::flush() }}
                    <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> Please make sure you have filled in all the fields. Hint: click "Sign up free" if you haven't already. </div>
            @endif
            <div class='ajax-load-gif'></div>
            {{ Form::text('email', Input::old('email'), array('class' => 'inp-field', 'id' => 'usr-email', 'rel' => 'tooltip', 'title' => 'Make sure to use your work email', 'placeholder' => 'Work Email', 'autofocus')) }}<br/>
            <div class="hs-acc-finder">
                <div id="hs-acc-img-holder">
                    <img id="hs-img" class="img-circle" />
                </div>
                <div class="acc-info">
                    {{-- filled by ajax info --}}
                    <p id="hs-acc-name"></p>
                    <p id="hs-acc-mail"></p>
                </div>
                <div class="clearfix"></div>
                <div id="decision-buttons">
                    <br/>
                    Is this you?
                    <a id='thats-me'>Yes</a>&nbsp;&nbsp;<a id='not-me'>No</a>
                </div>
                <div class="clearfix"></div><br/>
            </div> 
            <input name="password" type="password" class="inp-field" id="pass-conf" placeholder="Password" rel="tooltip" title="Enter your password here."/>
            {{ Form::label('What is 6 + 10?*', null, array('class' => 'small-margin')) }}
            {{ Form::input('number', 'bot-blocker', Input::old('bot-blocker'), array('class' => 'inp-field')) }}
            <div id="hidden-form" class="row-fluid">
                    <input name="fname" type="text" class="inp-field" id="fname" placeholder="First Name" rel="tooltip" title="Enter your first name here"/><br/>
                    <input name="lname" type="text" class="inp-field" id="lname" placeholder="Last Name" rel="tooltip" title="Enter your last name here"/><br/>
                    <input name="company-name" type="text" class="inp-field" id="company-name" placeholder="Company Name" rel="tooltip" title="Enter your company name here."/>
            </div>
            <div id="submit">
                <input type="submit" class="login-form-submit" value="Login"/>
            </div>
            <div id="remem-creds">                            
                <input name="remember-my-creds" data-toggle="tooltip" data-placement="top" type="checkbox" data-original-title="Remember My Credentials for Next Time"/>
                <label>Remember Me</label>
            </div>
            <div class="clear"></div>
            <div class="social-sign-up">
                <!-- Facebook, Twitter, linkedIn, or Google+ sign up/in -->
            </div>
        {{ Form::close() }}
        <!-- Divider -->
        <hr/>
        <div class="extra-stuff row">
            <span>Don't have an account?&nbsp;</span><br/>
            <p id="sign-up" class="col-lg-6"><a href="{{ URL::route('login.form') }}">Back to regular login</a></p>
        </div>
        <div class="clear"></div>        
    </div>{{-- End of login container --}}
    <div class="cta-area container-fluid">
        <div id="pds-login-cta">
            <!--HubSpot Call-to-Action Code -->
            <span class="hs-cta-wrapper" id="hs-cta-wrapper-ee9f0437-2e1b-4473-a9a2-409d6efac517">
                <span class="hs-cta-node hs-cta-ee9f0437-2e1b-4473-a9a2-409d6efac517" id="hs-cta-ee9f0437-2e1b-4473-a9a2-409d6efac517">
                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                    <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/ee9f0437-2e1b-4473-a9a2-409d6efac517"><img class="hs-cta-img" id="hs-cta-img-ee9f0437-2e1b-4473-a9a2-409d6efac517" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/ee9f0437-2e1b-4473-a9a2-409d6efac517.png" /></a>
                </span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(37772, 'ee9f0437-2e1b-4473-a9a2-409d6efac517');
                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
    </div> 
    {{ HTML::script('js/login.js') }}
</section>
@stop