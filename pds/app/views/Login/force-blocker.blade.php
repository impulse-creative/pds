@extends("layouts.Login.login-layout")
@section("content")
         {{-- main content section | .main-content --}}
        <section id="main-content" class="container-fluid"> 
            <div class="force-blocker center">
                <div class="force-blocker-message block-element page-center force-transparent-background">
                    <p class="blocker-text inline-element">
                        There have been too many login attempts on this account. For your protection, we have temporarily stopped login capabilities for your account.
                        If you think that you can remember your password, as well as comprehend simple math, <a href="{{ URL::route('captcha.login') }}">click here</a> to log in through a safe portal.
                    </p>
                    @if(intval($time_remaining))
                        <p class="blocker-text inline-element">
                            <h1 class="orange-text">An error occured: {{ $time_remaining }}</h1>
                        </p>
                    @else
                        <p class="blocker-text inline-element">
                            <h1 class="orange-text">{{ $time_remaining }}</h1>
                        </p>
                    @endif
                </div>
            </div>
        </section>
@stop