@extends("layouts.Login.login-layout")
@section("content")
{{-- main content section | .main-content --}}
<section id="main-content" class="container-fluid">
    <div id="call-us" class="pull-right">
        <span class="glyphicon glyphicon-earphone" style="margin-right:5px;"></span> <a class="white-text" href="tel:8009877386">(800) 987-7386</a>    </div> 
    <div id="login-container">
        <div class="pds-logo">
            <img src="{{ asset('images/application/app-login-logo.png') }}" alt="PDS Company Logo" id="form-logo" />
        </div>
        <!-- Divider -->
        <hr/>
        <!-- form to join/login -->
        {{ Form::open(array( 'url' => 'login', 'id' => 'login-form'))}}
        {{ Form::token() }}
        
        
        {{ Form::text('email', Input::old('email'), array('class' => 'inp-field', 'id' => 'usr-email', 'rel' => 'tooltip', 'title' => 'Make sure to use your work email', 'placeholder' => 'Work Email', 'autofocus')) }}<br/>
        @if($errors->has('email'))
            <div class="red-text text-muted">{{ $errors->first('email') }}</div>
        @endif
        @if($errors)
            <script>
                $(document).ready(function() {
                    $( "#sign-up-cta a" ).trigger( "click" ); 
                });
            </script>
        @endif
        <div class="hs-acc-finder">
            <div id="hs-acc-img-holder">
                <img id="hs-img" class="img-circle" />
            </div>
            <div class="acc-info">
                {{-- filled by ajax info --}}
                <p id="hs-acc-name"></p>
                <p id="hs-acc-mail"></p>
            </div>
            <div class="clearfix"></div>
            <div id="decision-buttons">
                <br/>
                Is this you?
                <a id='thats-me'>Yes</a>&nbsp;&nbsp;<a id='not-me'>No</a>
            </div>
            <div class="clearfix"></div><br/>
        </div>
        <input name="password" type="password" class="inp-field" id="password" placeholder="Password" rel="tooltip" title="Enter your password here."/>
        @if($errors->has('password'))
            <div class="red-text text-muted">{{ $errors->first('password', 'Password is required') }}</div>
        @endif
        <div id="hidden-form" class="row-fluid">
            <input name="fname" type="text" class="inp-field" id="fname" placeholder="First Name" rel="tooltip" title="Enter your first name here"/><br/>
            @if($errors->has('fname'))
                <div class="red-text text-muted">{{ $errors->first('fname', 'Provide your first name') }}</div>
            @endif
            <input name="lname" type="text" class="inp-field" id="lname" placeholder="Last Name" rel="tooltip" title="Enter your last name here"/><br/>
            @if($errors->has('lname'))
                <div class="red-text text-muted">{{ $errors->first('lname', 'Provide your last name') }}</div>
            @endif
            <input name="company-name" type="text" class="inp-field" id="company-name" placeholder="Company Name" rel="tooltip" title="Enter your company name here."/>
            @if($errors->has('company-name'))
                <div class="red-text text-muted">{{ $errors->first('company-name', 'Provide your company name') }}</div>
            @endif
            <div class="center">
                <div class="small-padding">Select which describes you:</div><br>
                <label>
                    {{ Form::radio('account-type', '1') }} &nbsp;
                    I work at/own a pharmacy
                </label>
                <label>
                    {{ Form::radio('account-type', '2') }} &nbsp;
                    My business assists pharmacies
                </label>
                @if($errors->has('account-type'))
                    <div class="red-text text-muted">{{ $errors->first('account-type') }}</div>
                @endif
                <input type="hidden" name="action" id="action" value="login" />
            </div>
        </div>

        <div id="submit" class="center">
            <input id="thors-hammer" type="submit" class="login-form-submit" value="Login"/>
        </div>
        <div id="remem-creds" class="small-padding">
            <input id="remem-checkbox" name="remember-my-creds" data-toggle="tooltip" data-placement="top" type="checkbox" value="1" data-original-title="Remember My Credentials for Next Time" checked="checked"/>
            &nbsp;<label>Remember Me</label>
        </div>
        @if(Session::has('passwordReset'))
        <div class="alert alert-success" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('passwordReset', 'default') }} </div>
        @endif
        @if(Session::has('imported'))
        <div class="alert alert-success" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('imported', 'default') }} </div>
        @endif
        @if(Session::has('Unsuccessful Login'))
        <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Unsuccessful Login', 'default') }} </div>
        @endif
        @if(Session::has('Sent'))
        <div class="alert alert-success" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Sent', 'default') }} </div>
        @endif
        @if(Session::has('AlreadyRegistered'))
        <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('AlreadyRegistered', 'default') }} </div>
        @endif
        @if(Session::has('Incomplete'))
        {{ Session::flush() }}
        <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> Please make sure you have filled in all the fields. Hint: click "Create an Account" if you haven't already. </div>
        @endif
        @if(Session::has('Failure'))
        <div class="alert alert-danger" data-dismiss="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Failure', 'default') }} </div>
        @endif
        <div class='ajax-load-gif'></div>
        <div class="clear"></div>
        <div class="social-sign-up">
            <!-- Facebook, Twitter, linkedIn, or Google+ sign up/in -->
        </div>
        </form>
        <!-- Divider -->
        <hr/>
        <div class="extra-stuff row">
            <span>Don't have an account?&nbsp;</span><br/>
            <p id="sign-up" class="sign-up-cta col-lg-6"><a href="#">Sign up today!</a></p>
            <p id="forgot-pass" class="forgot-pass-cta col-lg-6"><a href="{{ URL::route('remind.pass') }}">Forgot Your Password?</a></p>
        </div>
        <div class="clear"></div>        
    </div>{{-- End of login container --}}
    <div class="cta-area container-fluid">
        <div id="pds-login-cta">
            <!--HubSpot Call-to-Action Code -->
            <span class="hs-cta-wrapper" id="hs-cta-wrapper-ee9f0437-2e1b-4473-a9a2-409d6efac517">
                <span class="hs-cta-node hs-cta-ee9f0437-2e1b-4473-a9a2-409d6efac517" id="hs-cta-ee9f0437-2e1b-4473-a9a2-409d6efac517">
                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                    <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/ee9f0437-2e1b-4473-a9a2-409d6efac517"><img class="hs-cta-img" id="hs-cta-img-ee9f0437-2e1b-4473-a9a2-409d6efac517" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/ee9f0437-2e1b-4473-a9a2-409d6efac517.png" /></a>
                </span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(37772, 'ee9f0437-2e1b-4473-a9a2-409d6efac517');
                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
    </div> 
    {{ HTML::script('js/login.js') }}

</section>
@stop
