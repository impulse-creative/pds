@extends('layouts.marketplace.pds-market-layout')
@section('content')
@include('Marketplace.partials.page-title')
<div class="container">
    <div class="col-sm-8">
        <div class="company-description small-padding">
            <div class="hidden-logo medium-padding center">
                @if($results)
                    @if(strpos($results->logo_src, 'feed-icon'))
                        <img class="img-responsive" alt="{{ $results->logo_alt }}" title="Company Logo" src="{{ asset('images/placeholder_300x300.jpg') }}"/>
                    @else
                        <img class="img-responsive" alt="{{ $results->logo_alt }}" title="{{ $results->logo_alt }}" src="{{ $image = strpos($results->logo_src, 'http://localhost:8000') > -1 ? str_replace('http://localhost:8000', 'http://app.pharmacyowners.com', $results->logo_src) : $results->logo_src }}"/>
                    @endif
                @endif
            </div>
            <a href="{{ URL::route('marketplace.index') }}"><span class="glyphicon glyphicon-arrow-left small-padding"></span> Return to the marketplace </a>
            <h2>Overview</h2>
            <hr/>
            <p class="small-padding">
                @if($results)
                    {{ $results->description }}
                @endif
            </p>
        </div>
        <div class="company-contact small-padding">
            <h2>Contact Us</h2>
            <hr/>
            <p>
                @if($results)
                    {{ $results->contact_info }}<br>
                    @if($results->site_url != '' && $results->site_url != '#' && $results->site_url != 'http://')
                        <a href="{{ $results->site_url }}" target="_blank">Website</a>
                    @endif
                    <hr>
                    {{ $results->years }}
                @endif
            </p>
        </div>
    </div>
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar small-padding">
        <div class="sidebar-module sidebar-module-inset company-logo-container center">
            @if($results)
                <div class="hidden-real-vendor">{{ $results->vendor or 'No vendor' }}</div>
                <div class="hidden-real-email">{{ $results->real_email or 'No Email' }}</div>
                @if(strpos($results->logo_src, 'feed-icon'))
                    <img alt="company-logo" title="Company Logo" src="{{ asset('images/placeholder_300x300.jpg') }}"/>
                @else
                    <img alt="{{ $results->logo_alt }}" title="{{ $results->logo_alt }}" src="{{ $image = strpos($results->logo_src, 'http://localhost:8000') > -1 ? str_replace('http://localhost:8000', 'http://app.pharmacyowners.com', $results->logo_src) : $results->logo_src }}"/>
                @endif
            @endif
        </div>
        <hr/>
        <div class="sidebar-module">
            <div class="hs-company-form">
                <script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script> 
                <script>
                    hbspt.forms.create({
                        portalId: '37772',
                        formId: '8c325bc8-04c2-4577-bb28-5685c8f01754',
                        onFormReady: function ($form) {
                            jQuery('body input[name=marketplace_notification_contact]').val($('.hidden-real-email').text());
                            jQuery('body input[name=marketplace_notification_company]').val($('.hidden-real-vendor').text());
                        }
                    });
                </script> 
            </div>
        </div>
    </div>
</div>
<div class="clear:both;"></div>
@stop
