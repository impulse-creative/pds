<header class="market-header purple-background small-padding">
    <div class="header page-center">
        <div class="market-header-logo">
            <a href="{{ URL::route('marketplace.index') }}"><img src="{{ asset('images/marketplace/marketplace-logo.png') }}" alt="PDS Marketplace" title="PDS Marketplace"/></a>
        </div>
        <div class="hidden-nav pull-right">
            <div class="col-lg-2 col-md-2 col-xs-2"><label for="mobile-nav-check" class="toggle-menu"> ☰ </label></div>
        </div>
        <div class="hs-marketplace-header-nav pull-right">
            <div class="col-lg-12 col-md-12 col-xs-12 market-nav">
                <ul>
                    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/what-we-do" class="nav-link">What We Do</a></li>
                    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/who-we-help" class="nav-link">Who We Help</a></li>
                    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/training" class="nav-link">Training</a></li>
                    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/our-story" class="nav-link">About</a></li>
                    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/contact-pharmacy-development-services" class="nav-link">Contact</a></li>
                    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/new-pds-blog" class="nav-link">Blog</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
