@extends('layouts.marketplace.pds-market-layout')

@section('head')
    @parent
    {{ HTML::style('css/WallopSlider.css') }}
@stop

@section('content')
<div class="container">
    @if($all_vendors)
    {{-- category search will be hidden until we figure out how to do so with the temporary profiles table --}}
    <div class="well small-margin col-lg-3 col-md-3 col-xs-3">
        <div class="filter-container">
            <ul class="category-list">
                @foreach($categories as $key => $category)
                    <li><a tab-index="-1" href="{{ URL::to('/marketplace/company/filter/vendor/listing').'?temp_filter='.$category }}">{{ $category }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    @endif

    @if($all_vendors)
    <div class="col-md-8" style="margin-left:20px;">
        <div class="marketplace-description medium-padding">
            <p>
            <h2><span class="purple-text">PDS Marketplace:</span></h2>
            A place for Independent Pharmacy owners to connect with the services and suppliers that help them do business.<br/><br/> 

            Including: marketing services, computer systems, accreditation help, high-quality niche products, billing systems, and more.
            </p>
        </div>

        <?php
        function shorten_string($string, $length) {
            if (strlen($string) > $length) {
                $string = trim(substr($string, 0, $length) . "...");
            }
            return $string;
        }
        ?>
    
        <div class="panel panel-default">
            <div class="panel-heading">Featured Vendors</div>
            <div class="panel-body">
                <?php $count = 1 ?>
                <div class="featured-slider" style="position: relative;">
                    <ul>
                    @foreach($all_vendors as $key => $vendor)
                        @if($vendor->featured == 0) <?php continue; ?> @endif
                        <li id="item-{{$key}}" class="wallop-slider__item">
                            <div class="featured-block ">
                                <div class="featured-img small-margin-waist col-lg-2 col-md-2 col-xs-2">
                                    <img src="{{$vendor->logo_src}}" alt="{{$vendor->vendor}}"/>
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                    <h2 class="purple-text">{{ $vendor->vendor }}</h2>
                                    <p class="featured-description small-padding">{{ shorten_string($vendor->description, 200) }}...&nbsp;</p>
                                    <?php $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $vendor->vendor); ?>
                                    <p class="">
                                        <a href="{{ URL::route('marketplace.company', ['company_id' => $vendor->id, 'company_name' => preg_replace('/ /', '-', $scrubbed_title)]) }}" class="btn btn-primary">Read More</a>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-12 small-padding company-list-header">
            <h2>Company List</h2>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 medium-padding company-name-container">
            <div class="companies-list">
                @foreach($all_vendors as $key => $val)
                {{-- change condition to '== 0' when switched over to real pds listing --}}

                <?php $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $val->vendor); ?>
                <p class="company_name"><a href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">{{ $val->vendor }}</a></p>
                @endforeach
            </div>
        </div>
    </div>
    @endif
</div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/bootstrap-submenu.min.js') }}
    <script>$('.dropdown-submenu > a').submenupicker();</script>
    {{ HTML::script('js/impulse-carousel.js') }}
    <script>
        $(document).ready(function() {
            $('.wallop-slider__item').impulsefadecarousel();
        });
    </script>
@stop