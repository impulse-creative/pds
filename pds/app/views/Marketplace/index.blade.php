@extends('layouts.marketplace.pds-market-layout')

@section('head')
@parent
{{-- HTML::style('css/bootstrap-submenu.css') --}}
@stop

@section('content')
<div class="container">
    @if($all_vendors)
    {{-- category search will be hidden until we figure out how to do so with the temporary profiles table --}}
    <div class="well small-margin col-lg-3 col-md-3 col-xs-3">
        <div class="filter-container">
            <ul class="category-list">
                @foreach($categories as $key => $category)
                <li>
                    <a tab-index="-1" href="{{ URL::to('/marketplace/company/filter/vendor/listing').'?temp_filter='.$category }}">{{ $category }}</a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    @endif

    {{-- Here we list the marketplace list items, or each company more specifically --}}
    @include('Marketplace.partials.list-item')

</div>
@stop

@section('extra-js')
@parent
{{ HTML::script('js/bootstrap-submenu.min.js') }}
<script>$('.dropdown-submenu > a').submenupicker();</script>
@stop