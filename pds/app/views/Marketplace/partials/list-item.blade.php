@if($all_vendors)
	<div class="col-lg-8 col-md-8 col-xs-8 medium-padding vendor-list">
	@if($real_cat != '')
		<div class="col-lg-12">
			<span><a class="btn btn-primary pull-right" href="{{ URL::route('marketplace.index') }}">Return To List</a></span>
			<p class="filter-choice orange-text"><span class="purple-text">Filtered By:&nbsp;</span>{{$real_cat}}</p>
		</div>
		<div class="clearfix"></div>
	@endif
	@if(is_object($filtered_results))
		@foreach($filtered_results as $val)
			<?php $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $val->vendor); ?>
			<div class="company-container">
				<div class="company-img img-responsive placeholder">
					@if(strpos($val->logo_src, 'feed-icon'))
						{{-- Do Nothing --}}
					@else
						<a href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">
							<img alt="{{ $val->logo_alt }}" title="{{ $val->logo_alt }}" class="img-responsive" src="{{ $val->logo_src }}" width="150" height="150"/>
						</a>
					@endif
				</div>
				<div class="company-summary">
					<h2><a title="{{ $val->vendor }}" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">{{ $val->vendor }}</a></h2>
					<hr/>
					<p>
						@if($val->description === "No Description")
							<p>No Description...&nbsp;<a title="Visit {{ $val->vendor }}'s company profile" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">Read More</a></p>
						@else
							{{ substr( $val->description, 0, strrpos( substr($val->description, 0, 250), ' ' ) ) }}...&nbsp;<a title="Visit {{ $val->vendor }}'s company profile" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">Read More</a>
						@endif
					</p>
					<p class="small-padding orange-text item-categories gray-background"><span class="purple-text">Category Tags:</span>&nbsp;{{ $val->category }}&nbsp;-&nbsp;{{ $val->subcategory }}</p>
				</div>
			</div>
		@endforeach
	@else
		@foreach($all_vendors as $val)
			<?php $scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $val->vendor); ?>
			<div class="company-container">
				<div class="company-img img-responsive placeholder">
					@if(strpos($val->logo_src, 'feed-icon'))
						{{-- Do Nothing --}}
					@else
						<a href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">
							<img alt="{{ $val->logo_alt }}" title="{{ $val->logo_alt }}" class="img-responsive" src="{{ $val->logo_src }}" width="150" height="150"/>
						</a>
					@endif
				</div>
				<div class="company-summary">
					<h2><a title="{{ $val->vendor }}" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">{{ $val->vendor }}</a></h2>
					<hr/>
					<p>
                                            {{var_dump($val->logo_src)}}
						@if($val->description === "No Description")
							<p>No Description...&nbsp;<a title="Visit {{ $val->vendor }}'s company profile" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">Read More</a></p>
						@else
							{{ substr( $val->description, 0, strrpos( substr($val->description, 0, 250), ' ' ) ) }}...&nbsp;<a title="Visit {{ $val->vendor }}'s company profile" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id.'/'.preg_replace('/ /', '-', $scrubbed_title) }}">Read More</a>
						@endif
					</p>
				</div>
			</div>
		@endforeach
	@endif
	@if($real_cat != '')
		<div class="col-lg-12">
			<span><a class="btn btn-primary pull-right" href="{{ URL::route('marketplace.index') }}">Return To List</a></span>
			<p class="filter-choice orange-text"><span class="purple-text">Filtered By:&nbsp;</span>{{$real_cat}}</p>
		</div>
		<div class="clearfix"></div>
	@endif
	</div>
@endif