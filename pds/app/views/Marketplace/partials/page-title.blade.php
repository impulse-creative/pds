<div class="company-title-container purple-transparent-background white-text">
	<div class="container medium-padding company-title">
		@if($company)
			<h1>{{ $company }}</h1>
		@endif
	</div>
</div>

