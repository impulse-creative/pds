@if($results)
	@foreach($results as $val)
		<div class="company-container">
			<div class="company-img img-responsive placeholder">
				@if(strpos($val->company_logo, 'feed-icon'))
					{{-- Do Nothing --}}
				@else
					<img alt="{{ $val->company_name }}" title="{{ $val->company_name }}" class="img-responsive" src="{{ URL::to('/') }}/storage/files/company_logos/{{ $val->company_logo }}" width="150" height="150"/>
				@endif
			</div>
			<div class="company-summary">
				<h2><a title="{{ $val->company_logo }}" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id }}?company={{$val->company_name}}">{{ $val->company_name }}</a></h2>
				<hr/>
				<p>
					@if($val->description === "No Description")
						No Description...&nbsp;<a title="Visit {{ $val->company_name }}'s company profile" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id }}?company={{$val->company_name}}">Read More</a>
					@else
						{{ substr( $val->description, 0, strrpos( substr($val->description, 0, 250), ' ' ) ) }}...&nbsp;<a title="Visit {{ $val->company_name }}'s company profile" href="{{ $url = URL::to('/marketplace/company').'/'.$val->id }}?company={{$val->company_name}}">Read More</a>
					@endif						
				</p>
			</div>
		</div>
	@endforeach
@endif
