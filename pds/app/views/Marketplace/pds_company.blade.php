@extends('layouts.marketplace.pds-market-layout')

@section('content')
	@include('Marketplace.partials.page-title')
	<div class="container">
		<div class="col-sm-8">
			<div class="company-description small-padding">
        <div class="hidden-logo medium-padding center">
          @if($results)
            @if(strpos($results->company_logo, 'feed-icon') || is_null($results->company_logo)) 
              <img class="img-responsive" alt="{{ $results->company_name }}" title="Company Logo" src="{{ asset('images/placeholder_300x300.jpg') }}"/>
            @else
              <img class="img-responsive" alt="{{ $results->company_name }}" title="{{ $results->company_name }}" src="{{ URL::to('storage/files/company_logos') }}/{{ $results->company_logo }}"/>
            @endif
          @endif
        </div>
        <a href="{{ URL::route('marketplace.index') }}"><span class="glyphicon glyphicon-arrow-left small-padding"></span> Return to the marketplace </a>
				<h2>Overview</h2>
				<hr/>
				<p class="small-padding">
				  @if($results)
            {{ $results->description }}
          @endif
        </p>
			</div>
			<div class="company-contact small-padding">
				<h2>Contact Us</h2>
				<hr/>
				<p>
          @if($results)
            @if($results->email != '')<p class="contact-item"><span><strong>Email:&nbsp;</strong>{{ $results->email }}</span></p>@endif
            @if($results->phone != '')<p class="contact-item"><span><strong>Phone:&nbsp;</strong>{{ $results->phone }}</span></p>@endif
            @if(!preg_match('/^http/', $results->company_website)) <?php $real_web = $results->company_website; ?> @else <?php $real_web = 'http://'.$results->company_website; ?> @endif
            @if($results->company_website != '')<p class="contact-item"><span><strong>Website:&nbsp;</strong><a href="{{ $real_web }}" target="_blank">{{ $results->company_website }}</a></span></p>@endif
            @if($results->address != '')<p class="contact-item"><span><strong>Address:&nbsp;</strong>{{ $results->address }}</span></p>@endif
            @if($results->company_blog_or_vlog != '')
              <p class="contact-item"><span><strong>Blog:&nbsp;</strong><a href="{{ $results->company_blog_or_vlog }}" target="_blank">{{ $results->company_blog_or_vlog }}</a></span></p>
            @endif
          @endif
				</p>
			</div>
		</div>
		<div class="col-sm-3 col-sm-offset-1 blog-sidebar small-padding">
          <div class="sidebar-module sidebar-module-inset company-logo-container center">
            @if($results)
              @if(strpos($results->vendor, 'feed-icon'))
                <img alt="company-logo" title="Company Logo" src="{{ asset('images/placeholder_300x300.jpg') }}"/>
              @else
                <img alt="{{ $results->logo_alt }}" title="{{ $results->logo_alt }}" src="{{ URL::to('storage/files/company_logos') }}/{{ $results->company_logo }}"/>
              @endif
            @endif
          </div>
          <hr/>
          <div class="sidebar-module">
            <div class="hs-company-form">
              {{ Form::open(array('route' => 'marketplace.company', 'class' => 'hs-form')) }}
                <h2>Contact Company</h2>
                {{ Form::input('text', 'fname', Input::old('fname'), array('class' => 'inp-field', 'placeholder' => 'First Name')) }}
                {{ Form::input('text', 'lname', Input::old('lname'), array('class' => 'inp-field', 'placeholder' => 'Last Name')) }}
                {{ Form::input('text', 'company', Input::old('company'), array('class' => 'inp-field', 'placeholder' => 'Company Name')) }}
                {{ Form::input('email', 'email', Input::old('email'), array('class' => 'inp-field', 'placeholder' => 'Email')) }}
                {{ Form::input('text', 'phone', Input::old('phone'), array('class' => 'inp-field', 'placeholder' => 'Phone Number')) }}<br/>
                {{ Form::submit('Go!', array('class' => 'btn btn-primary company-form-submit')) }}
              {{ Form::close() }}
            </div>
          </div>
    </div>
	</div>
	<div class="clear:both;"></div>
@stop