@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
$permissions = explode(':', $sections['permissions']);
$user = isset($tw_data['user'][0]) ? $tw_data['user'][0] : $tw_data['user'];
if(Auth::check()){
    $is_owner = User::checkIfCompanyOwner($user->email);
}
?>

<div class="forum-bg account-profile">
    <div class="container">
        <div class="col-md-12">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            @if(isset($user->my_coach) || isset($user->my_is) || isset($user->is_id) || isset($user->coach_id))

                <div class="page-header center">
                    @if(Auth::check())
                        @if((preg_match("/ceo/", $is_owner) || preg_match("/owner/", $is_owner) || preg_match("/developer/", $is_owner)) && is_null($user->teamwork_company_id))
                            <a href="{{ URL::route('create.new.teamwork.company.account') }}" class="btn btn-primary small-margin">Register Your Company</a>
                        @endif
                        @if(!preg_match("/ceo/", $is_owner) || !preg_match("/owner/", $is_owner))
                            @if(is_null($user->teamwork_company_id) && ($user->coach_id != '' && $user->is_id != ''))
                                {{-- <a href="{{ URL::route('create.new.company.employee') }}" class="btn btn-primary small-margin">Register an Account With Your Company</a> --}}
                            @endif
                            @if(isset($user->teamwork_company_id) && ($user->coach_id == '' && $user->is_id == ''))
                                <a class="btn btn-primary small-margin project-access">Login to Project Tools</a><br/>
                            @endif
                        @endif
                        @if(in_array('234234', $permissions))
                            <p class="text-muted"><span class="glyphicon glyphicon-chevron-down"></span>&nbsp;Impulse tools (permission: 5)&nbsp;<span class="glyphicon glyphicon-chevron-down"></span></p>
                            <a href="{{ URL::route('get.add.new.project') }}?for_user={{$tw_data['user']->id}}" class="btn btn-primary small-margin">Add New Project</a>
                            <a href="{{ URL::route('get.add.new.milestone') }}?for_user={{$tw_data['user']->id}}" class="btn btn-primary small-margin">Add New Milestone</a>
                            <a href="{{ URL::route('get.add.new.message') }}?for_user={{$tw_data['user']->id}}" class="btn btn-primary small-margin">Add New Message</a>
                            <a href="{{ URL::route('get.add.new.task', ['project_id' => $tw_data['project_id']]) }}&for_user={{$tw_data['user']->id}}" class="btn btn-primary small-margin">Add New Task</a><br/>
                        @endif
                    @endif
                    @if(Auth::user()->employee_type === 1)
                        <div class="change-projects">
                            @if(!isset($tw_data['company_project_list']['none']))
                                {{ Form::open(['route' => ['get.project.management', $user->id], 'class' => 'change-project-summary']) }}
                                    {{ Form::select('projectId', $tw_data['company_project_list'], -1, ['class' => 'form-control change-project']) }}
                                {{ Form::close() }}
                            @endif
                        </div>
                        <div class="hidden">
                            <script>
                                $(document).ready(function(){
                                   $('.change-project').on('change', function(){
                                       $('.change-project-summary').submit();
                                   });
                                   $('.project-access').on('click', function(){
                                       $('#teamworkAccessForm').submit();
                                   });
                                });
                            </script>
                        </div>
                    @endif
                </div>
            @endif
            <div class="pharmacy-profile-container">
                @include('Projects.company-profile')
            </div>
            <div class="clearfix"></div>
            {{-- Start of main content --}}
            @if((isset($user->teamwork_company_id) || $user->my_coach != '' || $user->my_is != ''))
                <div class="row auto-login-form hidden">
                    <form name="loginForm" id="teamworkAccessForm" action="http://mypds.pharmacyowners.com/" method="post">
                        <input type="hidden" name="action" value="login">
                        <div>
                            <label for="userLogin">Username:</label><br /><input type="text" name="userLogin" id="userLogin" value="{{Auth::user()->project_name}}"/><br/>
                            <label for="password">Password:</label><br /><input type="password" name="password" id="password" value="{{Auth::user()->project_password}}"/><br/>
                            <input type="submit" value="Login"/>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="activity-feed col-lg-6 col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title white-text pull-left">Wildly Important Goals</h3>
                                <a class="pull-right orange-text" href="http://mypds.pharmacyowners.com/all_milestones" target="_blank">View all goals&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                @if(isset($tw_data['tw_milestones']->milestones) && count($tw_data['tw_milestones']->milestones) > 0)
                                    <?php $wig_limiter = 0; ?>
                                    @foreach($tw_data['tw_milestones']->milestones as $milestone)
                                        @if(preg_match('/WIG/', $milestone->title)) <div class="purple-text"><a href="http://mypds.pharmacyowners.com/milestones/{{$milestone->id}}" class="purple-text">{{ $milestone->title }}</a></div> @endif
                                        <?php
                                            if($wig_limiter >= 8) break;
                                            $wig_limiter++;
                                        ?>
                                    @endforeach
                                @else
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button> 
                                        No Upcoming WIGS
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="activity-feed right-aligned col-lg-6 col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title white-text pull-left">Other Milestones</h3>
                                <a class="pull-right orange-text" href="http://mypds.pharmacyowners.com/all_milestones" target="_blank">View all milestones&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                                <div class="clearfix"></div>                            
                            </div>
                            <div class="panel-body resources-list">
                                @if(isset($tw_data['tw_milestones']->milestones) && count($tw_data['tw_milestones']->milestones) > 0)
                                    <?php $milestone_limiter = 0; ?>
                                    @foreach($tw_data['tw_milestones']->milestones as $milestone)
                                        @if(!preg_match('/WIG/', $milestone->title)) <div class="purple-text"><a href="http://mypds.pharmacyowners.com/milestones/{{$milestone->id}}" class="purple-text">{{ $milestone->title }}</a></div> @endif
                                        <?php 
                                            if($milestone_limiter >= 8) break;
                                            $milestone_limiter++;
                                        ?>
                                    @endforeach
                                @else
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button> 
                                        No Upcoming Milestones
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                   <div class="activity-feed col-lg-6 col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title white-text pull-left">Project Messages/Notes</h3>
                                <a class="pull-right orange-text" href="http://mypds.pharmacyowners.com/all_messages" target="_blank">View all messages&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                @if(isset($tw_data['tw_messages']->posts) && count($tw_data['tw_messages']->posts) > 0)
                                    <?php $msg_limiter = 0; ?>
                                    @foreach($tw_data['tw_messages']->posts as $message)
                                        <?php 
                                            if($msg_limiter >= 6) break;
                                            $msg_limiter++;
                                        ?>
                                        <div class="message-item row">
                                            <div class="col-lg-4 message-title purple-text"><a href="http://mypds.pharmacyowners.com/messages/{{$message->id}}?scrollTo=pmp{{ $message->{'post-id'} }}" class="purple-text">{{$message->title}}</a></div>
                                            <div class="col-lg-4 message-posted-date text-muted">{{ $message->{'user-display-posted-date'} }}</div>
                                            <div class="message-avatar col-lg-4"><img src="{{ $message->{'author-avatar-url'} }}"/></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endforeach
                                @else
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        No Messages or Notes
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="activity-feed right-aligned col-lg-6 col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title white-text pull-left">Upcoming Project Tasks</h3>
                                <?php
                                    $project_title = 'unknown';
                                    foreach($tw_data['tw_projects']->projects as $project){
                                        if($project->id === $tw_data['project_id']) $project_title = strtolower(trim($project->name));
                                    }
                                    $clean_project_title = preg_replace("/[^a-zA-Z0-9]+/", "-", $project_title = strtolower(trim($project->name)));
                                ?>
                                <a class="pull-right orange-text" href="http://mypds.pharmacyowners.com/projects/{{$tw_data['project_id']}}{{$clean_project_title}}/tasks" target="_blank">View tasks&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php $current_week = 0;$last_week = 0;$next_week = 0; $late_tasks_exist = 0; ?>
                                @if(isset($tw_data['tw_tasklist']->{'todo-items'}) && count($tw_data['tw_tasklist']->{'todo-items'}) > 0)
                                    @foreach($tw_data['tw_tasklist']->{'todo-items'} as $task) 
                                        <?php 
                                            $now = \Carbon\Carbon::today('America/Vancouver'); 
                                            $start_date = new \Carbon\Carbon($task->{'start-date'}, 'America/Vancouver'); 
                                            $due_date = new \Carbon\Carbon($task->{'due-date'},'America/Vancouver'); 
                                            $start_of_next_week = \Carbon\Carbon::today('America/Vancouver')->addWeek()->startOfWeek();
                                            $end_of_next_week = \Carbon\Carbon::today('America/Vancouver')->addWeek()->endOfWeek();
                                        ?>
                                        @if($due_date->format('Ymd') < $start_of_next_week->format('Ymd') || $start_date->format('Ymd') < $start_of_next_week->format('Ymd'))
                                            @if($current_week == 0) <p class="current-week-tasks">Current Week Tasks</p> @endif
                                            <?php $current_week = 1; $last_week = 0; $next_week =0; ?>
                                        @endif
                                        @if($start_date->format('Ymd') > $start_of_next_week->format('Ymd') && $start_date->format('Ymd') < $end_of_next_week->format('Ymd'))
                                            @if($next_week == 0) <p class="next-week-tasks">Next Week Tasks</p> @endif
                                            <?php $current_week = 0; $last_week = 0; $next_week = 1; ?>
                                        @endif
                                        @if($start_date->format('Ymd') >= $end_of_next_week->format('Ymd')) <?php continue; ?> @endif
                                        <div class="task-item">
                                            <div class="task-name col-lg-6 purple-text"><a href="http://mypds.pharmacyowners.com/tasks/{{$task->id}}" class="purple-text">{{$task->content}}</a></div>
                                            <div class="task-start col-lg-6 text-muted">Starts on {{ $start_date->format('M j, Y') }}</div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endforeach
                                @else
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        No Upcoming Tasks
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <a href="http://mypds.pharmacyowners.com/projects/{{$tw_data['project_id']}}/overview?display=latetasks" class="red-text" target="_blank"><i class="fa fa-exclamation-triangle"></i>&nbsp;View late tasks</a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="activity-feed right-aligned col-lg-12 col-md-12 col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title white-text pull-left">Project Activity Feed</h3>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php $limiter = 0; ?>
                                @if(isset($tw_data['tw_activity_feed']->activity) && count($tw_data['tw_activity_feed']->activity) > 0)
                                    @foreach($tw_data['tw_activity_feed']->activity as $activity)
                                        <?php 
                                            if($limiter >= 8) break;
                                            $limiter++;
                                        ?>
                                        <div class="activity">                                   
                                            <div class="activity-item col-lg-6">
                                                <a class="purple-text" href="http://mypds.pharmacyowners.com/{{$activity->link}}" target="_blank" target="_blank">[{{$activity->activitytype}} {{$activity->type}}]: {{$activity->description}}</a>
                                            </div>
                                            <div class="activity-date col-lg-6">
                                                <?php 
                                                    $date = new \Carbon\Carbon($activity->datetime); 
                                                    $wording = 'by';
                                                    if($activity->activitytype === 'new') $wording = 'created by';
                                                    if($activity->activitytype === 'edited') $wording = 'edited by';
                                                ?>
                                                <div class="text-muted">Occurred on {{$date->format('M d, Y')}} @if(isset($activity->fromusername) && $activity->fromusername != '') | {{$wording}} {{$activity->fromusername}} @endif</div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endforeach
                                @else
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button> 
                                        No Recent Activity
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @else
                @if(is_null($user->teamwork_company_id) && $user->coach_id == '' && $user->is_id == '' && (!preg_match("/ceo/", strtolower($is_owner)) || !preg_match("/owner/", strtolower($is_owner))))
                    <div class="row request-teamwork-information-container center">
                        <div class="request-access-information">
                            <h2>Hello, {{{$user->fname.' '.$user->lname }}}</h2>
                            <p>It seems that your company has not set up a project in myPDS, or you do not have an account set up in myPDS with your company!</p>
                            <p>Whatever it is, we can help.</p>
                            <p>Click "Request Access" below so we can assist you in getting connected with your company or setting up your account.</p>
                            <p class="text-muted">
                                Cheers,<br/>- PDS Support Team
                            </p>
                        </div>
                        <script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
                        <script>
                            hbspt.forms.create({
                              portalId: '37772',
                              formId: '12741335-4fc1-44d6-ab8a-eba779661ebe'
                            });
                        </script>
                    </div>
                @else
                    @if(isset($tw_data['tw_managed_users']))
                        <div class="coach-and-IS-header">
                            <h2>Client List</h2>
                            <hr/>
                        </div>
                        <div class="coach-and-IS-company-list">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="pharmacy-table-header">
                                        <tr>
                                            <th>Client Company</th>
                                            <th>Owner</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <div class="overflow-div">
                                            @if(count($tw_data['tw_managed_users']) > 0)
                                                @foreach($tw_data['tw_managed_users'] as $managed_user)
                                                    <tr>
                                                        <td><a href="{{ URL::route('get.project.management', ['id' => $managed_user->user->id]) }}">{{ $managed_user->pharmacy_name }}</a></td>
                                                        <td>{{ $managed_user->pharmacy_owner }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td>No clients found.</td>
                                                </tr>
                                            @endif
                                        </div>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                @endif
            @endif
        </div>
    </div>
</div>

@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/mypds.js') }}
@stop
