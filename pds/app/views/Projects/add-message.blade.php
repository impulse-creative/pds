@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
    $permissions = explode(':', $sections['permissions']);
?>

<div class="forum-bg account-profile">
    <div class="container">
        <div class="col-md-12">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            <div class="row new-milestone-form col-lg-6 large-margin">
                <h2>Create a New Message</h2>
                <div class="create-new-message-form-container">
                    {{-- REMEMBER YOU NEED TO PULL IN ALL MESSAGE CATEGORIES AND REPLACE THE PROJECT CATEGORIES IN THE HIDDEN DETAILS WITH IT --}}
                    {{ Form::open(['route' => 'create.new.message', 'class' => 'create-new-message-form'] ) }}
                        <div class="control-group">
                            {{ Form::label('project_id', 'Choose a Project') }}
                                @if($errors->has('project_id'))
                                    <p class="red-text">{{ $errors->first('project_id') }}</p>
                                @endif
                            {{ Form::select('project_id', $company_project_list, '0', ['class' => 'form-control']) }}
                        </div>
                        <div class="control-group">
                            {{ Form::label('message_recipient', 'Choose a Recipient') }}
                                @if($errors->has('message_recipient'))
                                    <p class="red-text">{{ $errors->first('message_recipient') }}</p>
                                @endif
                            {{ Form::select('message_recipient', $potential_company_users, -1, ['class' => 'form-control']) }}
                        </div>
                        <div class="control-group">
                            {{ Form::label('message_title', 'Message Title') }}
                                @if($errors->has('message_title'))
                                    <p class="red-text">{{ $errors->first('message_title') }}</p>
                                @endif
                            {{ Form::input('text', 'message_title' , Input::old('message_title'), ['class' => 'form-control']) }}
                        </div>
                        <div class="control-group">
                                {{ Form::label('message_body', 'Message') }}
                                <textarea name="message_body" class="form-control" placeholder="Provide a message for your recipient">{{Input::old('message_body')}}</textarea>
                        </div>
                        <div class="control-group">
                            {{ Form::label('is_private', 'Is this a private message?') }}
                            {{ Form::checkbox('is_private', '1') }}
                        </div>
                        <div class="new-message-submit-container control-group small-margin">
                            {{ Form::submit('Send Message', ['class' => 'btn btn-primary']) }}
                        </div>
                    {{ Form::close() }}
                </div>
                   
            </div> 
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

@stop

@section('extra-js')
    @parent

@stop 