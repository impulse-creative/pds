@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/bootstrap-datetimepicker.min.css') }}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
    $permissions = explode(':', $sections['permissions']);
?>

<div class="forum-bg account-profile">
    <div class="container">
        <div class="col-md-12">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            <div class="row new-milestone-form col-lg-6 large-margin">
                <h2>Create a New Milestone</h2>
                {{ Form::open(['route' => 'create.new.milestone', 'class' => 'create-new-milestone']) }}
                    <div class="control-group">
                        {{ Form::label('project_id', 'Project to Add Milestone to') }}
                        @if($errors->has('project_id'))
                            <p class="red-text">{{ $errors->first('project_id', 'Please select a project') }}</p>
                        @endif
                        {{ Form::select('project_id', $company_project_list, 0, ['class' => 'form-control']) }}
                    </div>
                    <div class="control-group">
                        {{ Form::label('milestone_title', 'Project Milestone Title') }}
                        @if($errors->has('milestone_title'))
                            <p class="red-text">{{ $errors->first('milestone_title', 'Please include a title.') }}</p>
                        @endif
                        {{ Form::input('text', 'milestone_title', Input::old('milestone_title'), ['class' => 'form-control', 'placeholder' => 'Milestone Title']) }}
              
                    </div>
                    <div class="control-group">
                        {{ Form::label('milestone_description', 'Project Milestone Description') }}
                        <textarea name="milestone_description" class="form-control" placeholder="Make the description for your milestone clear, short, and relevant to the milestone title."></textarea>
                        @if($errors->has('milestone_description'))
                            <p class="red-text">{{ $errors->first('milestone_description', 'Please include a description.') }}</p>
                        @endif
                    </div>
                    <label for="deadline_date">Project Milestone Deadline:</label>
                        @if($errors->has('deadline_date'))
                            <p class="red-text">{{ $errors->first('deadline_date', 'Please include a deadline date.') }}</p>
                        @endif
                    <div class="input-group date" id="deadline_date">
                        <input name="deadline_date" type='text' class="form-control" data-date-format="MM/DD/YYYY" value="{{ Input::old('deadline_date') }}" placeholder="Choose a deadline for your milestone"/>
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <div class="control-group">
                        {{ Form::label('notify_party', 'Notify User of Milestone') }}
                        {{ Form::checkbox('notify_party', true) }}
                    </div>
                    <div class="control-group">
                        {{ Form::label('remind_party', 'Remind User of Milestone When Close') }}
                        {{ Form::checkbox('remind_party', true) }}
                    </div>
                    <div class="control-group">
                        {{ Form::label('responsible_party', 'Responsible Party') }}
                        {{ Form::select('responsible_party', $potential_company_users, 0, ['class' => 'form-control']) }}
                    </div>
                    <div class="hidden hidden-details">
                    </div>
                    <div class="control-group">
                        {{ Form::submit('Create Milestone', ['class' => 'btn btn-primary small-margin']) }}
                    </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/moment.js') }}
    {{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
    <script>
        /*-- initialize datetime picker for new resources. Used for resource expiration --*/
        if($('#deadline_date').length > 0) {
            $('#datetimepicker5, #datetimepicker5 input').datetimepicker({
                pickTime: false
            });
            $('#deadline_date, #deadline_date input').datetimepicker({
                pickTime: false
            });
        }
    </script>
@stop