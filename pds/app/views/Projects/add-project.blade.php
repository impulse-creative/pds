@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
$permissions = explode(':', $sections['permissions']);

?>

<div class="forum-bg account-profile">
    <div class="container">
        <div class="col-md-12">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            <div class="row new-project-form col-lg-6 large-margin">
                <h2>Create a New Project</h2>
                {{ Form::open(['route' => 'create.new.project', 'class' => 'create-new-project']) }}
                    <div class="control-group">
                        {{ Form::label('project_name', 'Project Title') }}
                        {{ Form::input('text', 'project_name', Input::old('project_name'), ['class' => 'form-control', 'placeholder' => 'Project Title']) }}
                    </div>
                    <div class="control-group">
                        {{ Form::label('project_description', 'Project Description') }}
                        <textarea name="project_description" class="form-control" placeholder="Make the description for your project clear, short, and relevant to the project title."></textarea>
                    </div>
                    <div class="hidden hidden-details">
                        {{ Form::hidden('company_id', $tw_company->teamwork_company_id) }}
                    </div>
                    <div class="control-group">
                        {{ Form::submit('Create Project', ['class' => 'btn btn-primary small-margin']) }}
                    </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

@stop

@section('extra-js')
    @parent

@stop