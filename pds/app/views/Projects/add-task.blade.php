@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/bootstrap-datetimepicker.min.css') }}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
    $permissions = explode(':', $sections['permissions']);
?>

<div class="forum-bg account-profile">
    <div class="container">
        <div class="col-md-12">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            <div class="row new-task-form col-lg-6 large-margin">
                <h2>Create a New Task</h2>
                <div class="create-new-task-form-container">
                    {{-- REMEMBER YOU NEED TO PULL IN ALL MESSAGE CATEGORIES AND REPLACE THE PROJECT CATEGORIES IN THE HIDDEN DETAILS WITH IT --}}
                    {{ Form::open(['route' => 'create.new.task', 'class' => 'create-new-task-form'] ) }}
                        <div class="control-group">
                            {{ Form::label('tasklist_id', 'Select a Task List') }}
                                @if($errors->has('tasklist_id'))
                                    <p class="red-text">{{ $errors->first('tasklist_id') }}</p>
                                @endif
                            {{ Form::select('tasklist_id', $company_task_lists, '0', ['class' => 'form-control']) }}
                        </div>
                        <div class="control-group">
                            {{ Form::label('task_recipient', 'Select the Responsible Party') }}
                                @if($errors->has('task_recipient'))
                                    <p class="red-text">{{ $errors->first('task_recipient') }}</p>
                                @endif
                            {{ Form::select('task_recipient', $potential_company_users, -1, ['class' => 'form-control']) }}
                        </div>
                    <div class="control-group">
                        {{ Form::label('task_priority', 'What is the priority level of this task?') }}
                            @if($errors->has('task_priority'))
                                <p class="red-text">{{ $errors->first('task_priority') }}</p>
                            @endif
                        {{ Form::select('task_priority', ['0' => '--Select priority level--', 'low' => 'Low', 'medium' => 'Medium', 'high' => 'High'], '0', ['class' => 'form-control']) }}
                    </div>
                        <div class="control-group">
                            {{ Form::label('task_title', 'Task Title') }}
                                @if($errors->has('message_title'))
                                    <p class="red-text">{{ $errors->first('task_title', 'Please provide a title for your task.') }}</p>
                                @endif
                            {{ Form::input('text', 'task_title' , Input::old('task_title'), ['class' => 'form-control']) }}
                        </div>
                        <div class="control-group">
                                {{ Form::label('task_description', 'Task Description') }}
                                    @if($errors->has('task_description'))
                                        <p class="red-text">{{ $errors->first('task_description', 'Please provide a description for your task') }}</p>
                                    @endif
                                <textarea name="task_description" class="form-control" placeholder="Provide a description for your task">{{Input::old('task_description')}}</textarea>
                        </div>
                        <label for="start_date">Task Start Date:</label>
                        @if($errors->has('start_date'))
                            <p class="red-text">{{ $errors->first('start_date', 'Please include a start date.') }}</p>
                        @endif
                        <div class="input-group date" id="start_date">
                            <input name="start_date" type='text' class="form-control" data-date-format="MM/DD/YYYY" value="{{ Input::old('start_date') }}" placeholder="Choose a start date for your task"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <label for="end_date">Task End Date:</label>
                        @if($errors->has('end_date'))
                            <p class="red-text">{{ $errors->first('end_date', 'Please include an end date.') }}</p>
                        @endif
                        <div class="input-group date" id="end_date">
                            <input name="end_date" type='text' class="form-control" data-date-format="MM/DD/YYYY" value="{{ Input::old('end_date') }}" placeholder="Choose an end date for your task"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <div class="control-group">
                            {{ Form::label('is_private', 'Is this a private task?') }}
                            {{ Form::checkbox('is_private', '1') }}
                        </div>
                        <div class="control-group">
                            {{ Form::label('notify_party', 'Notify user of new task?') }}
                            {{ Form::checkbox('notify_party', true) }}
                        </div>
                        <div class="new-task-submit-container control-group small-margin">
                            {{ Form::submit('Assign Task', ['class' => 'btn btn-primary']) }}
                        </div>
                    {{ Form::close() }}
                </div>
                   
            </div> 
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/moment.js') }}
    {{ HTMl::script('js/bootstrap-datetimepicker.min.js') }}
    <script>
        /*-- initialize datetime picker for new resources. Used for resource expiration --*/
        if($('#start_date, #end_date').length > 0) {
            $('#datetimepicker5, #datetimepicker5 input').datetimepicker({
                pickTime: false
            });
            $('#start_date, #start_date input, #end_date, #end_date input').datetimepicker({
                pickTime: false
            });
        }
    </script>
@stop 