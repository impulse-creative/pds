@if($tw_data['pharmacy_name'] != 'none')
    <a class="btn btn-primary pharmacy-profile-btn" data-toggle="collapse" href="#pharmacyProfile" aria-expanded="false" aria-controls="pharmacyProfile">
        {{{ $tw_data['pharmacy_name'] }}} <span>(Click to see details)</span>
    </a>
    <div class="collapse" id="pharmacyProfile">
        <div class="pharmacy">
            <div class="pharmacy-profile">
                <div class="pharmacy-profile-header">
                    <?php
                        $copmany_start_date = $tw_data['company']->created_at;
                        $start_date = new \Carbon\Carbon($copmany_start_date);
                    ?>
                    <p class="pharmacy-name inline-element">{{{ $tw_data['pharmacy_name'] }}}</p>&nbsp;<p class="pharmacy-since inline-element text-muted">Active Member Since {{ $start_date->toFormattedDateString() }}</p>
                    <hr/>
                </div>
                <div class="pharmacy-specifics">
                    <div class="pharmacy-website">
                        <p><strong>Company Website:</strong>&nbsp;<a class="purple-text" href="{{ $tw_data['company']->domain_name or "#" }}" target="_blank">{{ $tw_data['company']->domain_name or "#" }}</a></p>
                    </div>
                </div>
                <div class="pharmacy-details">
                    <div class="general-details-header">
                        <h2 class="purple-text">Company Employees:</h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead class="pharmacy-table-header">
                                <tr>
                                    <th>Employee</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Title</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div class="overflow-div">
                                    @foreach($tw_data['tw_pharmacy_employees'] as $employee)
                                        <tr>
                                            <?php $clean_name = trim(preg_replace('/ /', '-', $employee->fname.' '.$employee->lname)); ?>
                                            <td>
                                                <a class="purple-text" href="{{ URL::route('user.profile', ['id' => $employee->id, 'name' => $clean_name]) }}" target="_blank">{{{ $employee->fname.' '.$employee->lname }}}</a>
                                            </td>
                                            <td><a class="purple-text" href="mailto:{{$employee->email}}" target="_blank">{{{$employee->email}}}</a></td>
                                            <?php $clean_phone = strtr($employee['phone'], array('(' => '', ')' => '', '-' => '')); ?>
                                            <td><a class="purple-text" href="tel:{{{ trim(preg_replace('/ /', '', $clean_phone)) }}}" target="_blank">{{{ $employee->phone }}}</a></td>
                                            <td>{{{ $employee->job_title or 'N/A' }}}</td>
                                        </tr>
                                    @endforeach
                                </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif