<h2>My Pharmacies</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead class="pharmacy-table-header">
            <tr>
                <th>Pharmacy Name</th>
                <th>Owner Name</th>
                <th>Owner Email</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <div class="overflow-div">
                @if(count($pharmaciesInTW) > 0)
                    @foreach($pharmaciesInTW as $company)
                        <tr>
                            <td>{{ $company->pharmacy_name }}</td>
                            <td>{{ $company->pharmacy_owner or 'N/A' }}</td>
                            <td>{{ $company->user->email or 'N/A' }}</td>
                            <td><a href="{{ URL::route('get.pharmacy.project', ['companyId' => $company->company_id]) }}" class="btn btn-primary">View myPDS for {{ $company->pharmacy_name }}</a></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">
                            <span class="orange-text">No pharmacies registered yet</span>. 
                            Look below under 'My Unregistered Pharmacies' to register a pharmacy of your choice.
                            Please note that you need to register the pharmacy in the system first before you can register the pharmacy again for myPDS.
                        </td>
                    </tr>
                @endif
            </div>
        </tbody>
    </table>
</div>