@if(count($companiesAvailable) > 0)
    <h2>My Unregistered Pharmacies</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead class="pharmacy-table-header">
                <tr>
                    <th>Pharmacy Name</th>
                    <th>Owner Name</th>
                    <th>Owner Email</th>
                    <th>Register Pharmacy</th>
                </tr>
            </thead>
            <tbody>
                <div class="overflow-div">
                    @foreach($companiesAvailable as $company)
                        <tr>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->owner_name or 'N/A' }}</td>
                            <td>{{ $company->owner_email or 'N/A' }}</td>
                            <td>
                                <?php $companies = json_decode(Auth::user()->companies);?>
                                @if(Auth::user()->employee_type === 1 || property_exists($companies, $company->id))
                                    <a href="{{ URL::route('create.new.teamwork.company.account', ['pharmacy_id' => $company->id]) }}" class="btn btn-primary">Register</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </div>
            </tbody>
        </table>
    </div>
@endif
