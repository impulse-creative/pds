{{-- PHARMACY DETAIL VIEW --}}
@foreach($pharmacy_data['companies'] as $company)
    @if(Company::belongsToCompany(Auth::id(), $company->id))
    <div class="pharmacy-details">
        <div class="pharmacy-title col-lg-6 col-md-6">
            <h1>{{$company->name}}</h1>
            <p class="text-muted">
                <a href="{{ $address = strpos($company->domain_name, 'http://') > -1 ? $company->domain_name : 'http://'.$company->domain_name }}" class="orange-text" target="_blank" rel="nofollow">
                    <i class="fa fa-link"></i>&nbsp;Visit pharmacy site
                </a>
            </p>
        </div>

        <div class="clearfix"></div>
        <div class="pharmacy-specifics">
            <div class="col-lg-6 col-md-6">
                <div class="list-group">
                    <a href="#" class="list-group-item disabled">
                        Pharmacy Details
                    </a>
                    <a href="{{ URL::route('user.profile', ['id' => $company->admin_id, 'name' => toAsciiUrl($company->owner_name)]) }}" class="list-group-item">
                        Owner: <span class="green-text">{{ $company->owner_name or 'Not Available' }}</span>
                    </a>
                    <a href="mailto:{{ $company->owner_email or '#' }}" class="list-group-item {{ isset($company->owner_email) ? '' : 'disabled' }}">
                        Owner Email: <span class="green-text">{{ $company->owner_email or 'Not Available' }}</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Owner Address: <span class="green-text">{{ $company->address or 'Not Available' }}</span>
                    </a>
                    <a href="tel:{{ $company->phone or '#' }}" class="list-group-item {{ isset($company->phone) ? '' : 'disabled' }}">
                        Pharmacy Phone: <span class="green-text">{{ $company->phone or 'Not Available' }}</span>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="list-group">
                    <span class="list-group-item disabled">
                        Pharmacy Activity&nbsp;<span class="text-muted">( overall )</span>
                    </span>
                    <span class="list-group-item">
                        <span class="badge">{{ $pharmacy_data['topic_count'][$company->id] or 0 }}</span> Message Board Topics
                    </span>
                    <span class="list-group-item">
                        <span class="badge">{{ $pharmacy_data['reply_count'][$company->id] or 0 }}</span> Message Board Replies
                    </span>
                    <a href="http://pdsadvantage.uservoice.com/forums/278198-beta-users-forum" target="_blank" class="list-group-item">
                        <span class="orange-text">
                            What else would you like to see on this page<i class="fa fa-question"></i>
                            &nbsp;Click to request.
                        </span>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="employee-list">
        <h2>Team Members
            @if($sections['user']->employee_type == 1) 
            <a class="btn btn-primary pull-right" href="#"><span class="glyphicon glyphicon-user"></span> Add New User</a><div class="clear-fix"></div>
            @endif
        </h2>
        <div class="clearfix"></div>
        <div class="medium-padding add-new-user hidden">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title white-text">Add New User To My Company</h3>
                </div>
                <div class="panel-body">
                    {{ Form::open(['route' => 'add.new.user'] ) }}
                    @if($errors->has('email'))
                        @if($errors->first('email') == 'The email has already been taken.')
                            <div class="alert alert-danger" role="alert">This email address <strong>{{ Input::old('email') }}</strong> already has an account. They will need to login and associate themselves with your company.  Have them input <strong>{{ $company->name }}</strong> for Company Name and <strong>{{ $company->owner_email }}</strong> for Owner Email Address.</div>
                        @else
                            <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
                        @endif
                    @endif
                    @if($errors->first('fname'))
                        <div class="alert alert-danger" role="alert">{{ $errors->first('fname') }}</div>                            
                    @endif
                    @if($errors->first('lname'))
                        <div class="alert alert-danger" role="alert">{{ $errors->first('lname') }}</div>                            
                    @endif
                    @if($errors->first('access'))
                        <div class="alert alert-danger" role="alert">{{ $errors->first('access') }}</div>                            
                    @endif
                    <div class="col-md-12">
                        <div class="col-md-3">
                            {{ Form::input('email', 'email', Input::old('email'), ['class' => 'inp-field', 'placeholder' => 'Email Address']) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::input('fname', 'fname', Input::old('fname'), ['class' => 'inp-field', 'placeholder' => 'First Name']) }}
                        </div>
                        <div class="col-md-3">
                        {{ Form::input('lname', 'lname', Input::old('lname'), ['class' => 'inp-field', 'placeholder' => 'Last Name']) }}    
                        </div>
                        <div class="col-md-3">
                            {{ Form::select('access', ['0' => 'Employee Type', '1' => 'Manager', '2' => 'Staff Member'], 0, ['class' => 'changeAccess inp-field' ])}}
                        </div>
                    </div>
                    <div class="col-md-12 center">
                    
                        {{ Form::submit('Create User', ['class' => 'btn btn-primary'])}}
                    </div>
                    
                    {{ Form::hidden('company', $company->id ) }}
                    
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="employee-table table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Employee Type</th>
                        <th>Approval Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($pharmacy_data['employees']))
                        @foreach($pharmacy_data['employees'][$company->id] as $employee)
                            @if(isset($pharmacy_data['employee_data'][$company->id][$employee->id])) 
                                <tr id="{{ $employee->id }}">
                                    <td>
                                        {{ $employee->fname }}
                                    </td>
                                    <td>{{ $employee->lname }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>
                                        @if($employee->id == $company->admin_id)
                                            <span class="orange-text">Owner</span> 
                                        @else
                                            @if($employee->employee_type == 1)
                                                <span class="orange-text">Manager</span>
                                            @else
                                                @if($employee->employee_type == 2)
                                                    <span class="orange-text">Staff</span>
                                                @else
                                                    <span class="orange-text">Not Approved</span>
                                                @endif
                                            @endif
                                            @if($sections['user']->id == $company->admin_id)
                                                <a href="{{ URL::route('swap.admins', ['employeeId' => $employee->id, 'companyId' => $company->id]) }}"
                                                   class="confirm"
                                                   data-confirm="There can only be one owner. This will revert you to a manager. You will still be able to manage the team.">
                                                    <i class="fa fa-key"></i>&nbsp;Make Owner
                                                </a>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if(preg_match('/pending/', $employee->companies))
                                            <p id="{{$company->id}}"><span class="red-text glyphicon glyphicon-ban-circle"></span>&nbsp;
                                        @else
                                            <p id="{{$company->id}}"><span class="green-text glyphicon glyphicon-check"></span>&nbsp;
                                        @endif
                                        @if($sections['user']->employee_type == 1)
                                            {{ Form::select('access', ['0' => 'No Access', '1' => 'Manager', '2' => 'Staff Member'], $employee->employee_type, ['id' => $employee->id, 'class' => 'changeAccess' ])}}
                                            <a class="btn  approve-employee" rel="nofollow" href="{{ URL::route('update.employee.approval.status', ['id' => $employee->id, 'company_id' => $company->id, 'employee_type' => $employee->employee_type]) }}">
                                                <span class="glyphicon glyphicon-check"></span>&nbsp;
                                                Apply Change to {{$employee->fname}}
                                            </a><br/>
                                            @if(is_null($employee->teamwork_company_id) && isset($sections['user']->teamwork_company_id))
                                                <a class="btn pull-right" href="{{ URL::route('add.employee.to.mypds', ['id' => $employee->id]) }}">
                                                    <span class="glyphicon glyphicon-check"></span>&nbsp;Add to myPDS
                                                </a>
                                                <div class="clearfix"></div>
                                            @endif
                                        @else
                                            {{-- do not place anything --}}
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        @if($pharmacy_data['approvals'] != null)
                            @foreach($pharmacy_data['approvals'] as $companyId => $userId)
                                @if($company->id == $companyId)
                                    <?php $approval = User::find($userId) ?>
                                    <tr>
                                        <td>
                                            {{ $approval->fname }}
                                        </td>
                                        <td>{{ $approval->lname }}</td>
                                        <td>{{ $approval->email }}</td>
                                        <td>
                                            <span class="orange-text">Not Approved</span>
                                        </td>
                                        <td>
                                            <p id="{{$companyId}}"><span class="red-text glyphicon glyphicon-ban-circle"></span>&nbsp;
                                            @if($sections['user']->employee_type == 1)
                                                {{ Form::select('access', ['0' => 'No Access', '1' => 'Manager', '2' => 'Staff Member'], $approval->employee_type, ['id' => $approval->id, 'class' => 'changeAccess' ])}}
                                                <a class="btn  approve-employee" rel="nofollow" href="{{ URL::route('update.employee.approval.status', ['id' => $approval->id, 'company_id' => $company->id, 'employee_type' => $approval->employee_type]) }}">
                                                    <span class="glyphicon glyphicon-check"></span>&nbsp;
                                                    Apply Change to {{ $approval->fname }}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @else
         <div class="alert alert-info small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> You are not a member of this company </div>
    @endif
    
@endforeach
<script>
    var baseUrl = window.location.protocol + "//" + window.location.host + "/";
    $(document).ready(function() {
        if($('body').find('.changeAccess').length) {
            addEmployeeApprovalHref($(this));
        }
        $('body').on('change', '.changeAccess', function() {
            addEmployeeApprovalHref($(this));
        });
        $('.employee-list h2').on('click', 'a', function(e) {
            e.preventDefault();
            AddNewUser();
        });
    });
    
    function AddNewUser() {
        $switch = $('.add-new-user').hasClass('hidden') ? 'open' : 'close';
        if($switch == 'open') {
            $('.employee-list h2 a').html(' <span class="glyphicon glyphicon-user"></span> Close');
            $('.add-new-user').removeClass('hidden').slideDown();
        } else {
            $('.add-new-user').slideUp().addClass('hidden');
            $('.employee-list h2 a').html('<span class="glyphicon glyphicon-user"></span> Add New User');
        }
        
    }
    
    function addEmployeeApprovalHref(element) {
            employeeType = $(element).val();
            employee = $(element).attr('id');
            company = $(element).parent().attr('id');
            $(element).parent().find('.approve-employee').attr('href', baseUrl+'dashboard/approve/pharmacy/employee/'+employee+'/'+company+'/'+employeeType);
    };
</script>
<?php
//cleaning strings for URL
function toAsciiUrl($str) {
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_| -]+/", '-', $clean);
        return $clean;
}
