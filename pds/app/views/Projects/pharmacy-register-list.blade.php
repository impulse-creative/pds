@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
    <?php
        $permissions = explode(':', $sections['permissions']);
        $user = $sections['user'];
        if(Auth::check()){
            $is_owner = User::checkIfCompanyOwner($user->email);
        }
    ?>

    <div class="forum-bg account-profile">
        <div class="container">
            <div class="col-md-12">
                @if(Session::has('Success'))
                    <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('Failure'))
                    <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
                @endif
                
                {{-- setting up the new way of doing things --}}
                {{-- things to note: teamwork_companies table now holds a teamwork_companys projects as opposed to information regarding the TW company. --}} 
                {{-- A teamwork company is represented by the company owner name --}}
                {{-- and each project in the company represents a pharmacy under that owner --}}
                {{-- coaches and IS need to be given permissions for those projects. Get these ids from the company record,  --}}
                @if(is_null($user->companies) || $user->companies === '0' || $user->companies === '"{}"')
                    <div class="panel panel-danger send-to-pharmacy-registration">
                        <div class="panel-heading">You are not associated with a company in our system!</div>
                        <div class="panel-body">
                            <p>
                                You cannot access myPDS features until you have associated your PDS account with the pharmacy you are employed at.
                            </p><br/>
                            <p>
                                Please <a href="{{ URL::route('dash.main') }}" class="orange-text">visit your dashboard home page</a> and follow the prompts in the pop up to connect to your pharmacy.
                            </p><br/>
                            <p>
                                If the pop up is not showing for you, and you haven't seen it any point, please provide us with information on your troubles by clicking the small question bubble in the bottom right
                                or click the support button in the main navigation menu.
                            </p>
                        </div>
                    </div>
                @endif
                
                @if(isset($user->companies) && $user->companies !== '0' && $user->companies !== '"{}"')
                    @if((is_null($user->teamwork_company_id) && $user->employee_type == 2) || $user->employee_type == 0) 
                        <div class="panel panel-warning send-to-pharmacy-registration">
                            <div class="panel-heading">Your pharmacy owner has not registered his account for myPDS yet!</div>
                            <div class="panel-body">
                                <p>
                                    We're sorry! 
                                </p>
                                <p>
                                    Please wait for your pharmacy owner to register their company for myPDS. After they have completed registration, revisit this page and you should see all
                                    information pertaining to your company's account.
                                </p>
                            </div>
                        </div>
                    @elseif(is_null($user->teamwork_company_id) && $user->employee_type == 1)
                        <div class="panel panel-warning send-to-pharmacy-registration">
                            <div class="panel-heading">You have a pharmacy to register with myPDS! Click register to begin registration</div>
                            <div class="panel-body">
                                @include('Projects.partials.unregistered-pharmacies-table')
                            </div>
                        </div>
                    @elseif(isset($user->teamwork_company_id) && $user->employee_type == 1)
                        <div class="registered-pharmacies">
                            @include('Projects.partials.registered-pharmacies-table')
                        </div>
                        <hr/>
                        <div class="unregistered-pharmacies">
                            @include('Projects.partials.unregistered-pharmacies-table')
                        </div>
                    @elseif(isset($user->teamwork_company_id) && $user->employee_type == 2)
                        {{ $employeeProjectView }}
                    @endif
                @endif
            </div>
        </div>
    </div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/mypds.js') }}
@stop
