@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop

@section("content")
<?php
    $permissions = explode(':', $sections['permissions']);
    $user = $sections['user'];
    if(Auth::check()){
        $is_owner = User::checkIfCompanyOwner($user->email);
    }
?>

<div class="forum-bg pharmacy-profile">
    <div class="container">
        <div class="col-md-12 large-margin">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            
            
            @if(preg_match('/pending/', $user->companies))
                <div class="approval-pending">
                    <h2>Welcome to your pharmacy profile!</h2>
                    <p>Any owner or manager can approve your account.  They will need to log in and visit myPharmacy Dashboard to approve your account.  
                    </p>
                </div>
            @else
                {{ $pharmacy_profile }}
                {{-- This means the current user is the owner of the company, since the owner id is used for the admin_id field --}}
                
            @endif
        </div>
    </div>
</div>

@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/mypds.js') }}
@stop
