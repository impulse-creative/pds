@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
$permissions = explode(':', $sections['permissions']);

?>

<div class="forum-bg account-profile">
    <div class="container">
        <div class="col-md-12">
            @if(Session::has('Success'))
                <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
            @endif
            <div class="row new-project-form col-lg-6 large-margin">
                <h2>Thank You For Your Request!</h2>
                <div class="thank-you-content">
                    <p>
                        So, you want to enjoy the awe-inspiring experience that is PDS, huh? You're well on your way!
                    </p>
                    <p>
                        PDS has received your request and is working hard to put together a plan customized <em>just for you</em>.
                        The joys of being a member are endless, exciting, and rewarding.
                    </p>
                    <p>
                        Please, take our hand as we guide your company to new pharmaceutical heights!
                    </p>
                    <p class="muted-text">
                        <em>- PDS Master Sales Squad</em>
                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

@stop

@section('extra-js')
    @parent

@stop