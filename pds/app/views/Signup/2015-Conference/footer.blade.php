</div>
<div class="medium-padding dark-gray-background">
	<div class="footer container medium-padding center">
		<div class="white-text">Copyright © 1998-{{date('Y')}} Pharmacy Development Services, Inc . All Rights Reserved.</div>
                <div class="footer-social-links">
                    <ul>
                        <li><a href="https://www.facebook.com/PharmacyDevelopmentServices" class="white-text" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="https://twitter.com/rxmarketing" class="white-text" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/565830" class="white-text" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                    </ul>
                </div>
	</div>
</div>
