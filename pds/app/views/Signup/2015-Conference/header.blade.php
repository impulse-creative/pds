<div class="header page-center small-padding">
	<div class="conference-logo">
		<img src="{{ asset('images/conference/pdsconferencelogo.png') }}" alt="Pharmacy Development Services Annual Conference">
	</div>
	<div class="conference-nav-container pull-right">
		<nav class="conference-nav">
			<ul>
 
				<li><a class="register-nav-item" href="{{ URL::route('dash.main') }}">dashboard</a></li>
			</ul>
		</nav>
	</div>
</div>