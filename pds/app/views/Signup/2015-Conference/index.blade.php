@extends('layouts.Conference.2015.master-layout')

@section('head')
@parent
{{ HTML::style('css/Conference/2015/wysiwig-styles.css') }}
@stop

@section('content')

@include('Signup.2015-Conference.progress-partials.conference-progress')

<div class="hero-section">
    <div class="black-overlay">
        <div class="page-center medium-padding">
            <h1 class="medium-padding form-title">Marketplace Listing 
                @if($vendorDetails['completed'] == 1) 
                <span class="completed-section">&nbsp;<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Completed<span> 
                        @endif</h1>

                        <div class="col-md-7 small-padding small-waist">
                            <p class="force-orange-text">Any changes made here won't be active until after January 15th, 2015.</p>
                            <p>
                                Each Exhibitor is given a dedicated ad on the <a href="http://app.pharmacyowners.com/marketplace/company/list" target="_blank">PDS Marketplace</a> where customers and prospects can learn more about your company.
                            </p>
                            <p>
                                If you are a NEW PDS Exhibitor, please complete the information on the right and check the appropriate box at the top.
                            </p>
                            <p>	
                                If you are a PREVIOUS PDS Exhibitor, <a target="_blank" href="{{ URL::route('marketplace.index') }}">please review your current PDS Marketplace Listing here</a>. After you have reviewed your existing listing and wish to keep it as is, please check the appropriate box at the top and fill out your business category as they have changed. If you would like to update, please fill complete all the fields on the form. 
                            </p>

                            <p class="force-orange-text">Important information on submitting your PDS Marketplace Listing:</p>

                            <ul>
                                <li>If you would like to link your company blog page or video, simply provide the direct url link to that page, blog, or video.</li>
                                <li><p>PDS must approve all media you post on your Marketplace Listing.</p></li>
                                <li>The PDS Marketplace categories are different from last year! We have streamlined them to make it easier to find you. Don't see a category that fits your company? No problem! 
                                    You can contact Carole Bebout at <a href="mailto:carole@pharmacyowners.com">carole@pharmacyowners.com</a> or <a href="tel:561-275-2637">561-275-2637</a> 
                                    or Koreen Leavitt at <a href="tel:561-275-2659">561-275-2659</a> and <a href="mailto:koreen@pharmacy-owners.com">koreen@pharmacy-owners.com</a> with your suggestions.</li>
                            </ul>
                        </div>
                        <div class="col-md-5 off-white-text small-padding small-waist exhibitor-form">
                            <div class="small-padding">
                                <a href="{{ URL::route('conference.show.guide-entry') }}" class="btn btn-warning">Skip for now&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                            {{ Form::model($user, array('route' => 'conference.create.market-listing', 'class' => 'market-listing-form', 'files' => 'true')) }}
                            <input type="checkbox" name="approve_previous" value="1" title="Approve Previous" @if(isset($user) && $user->approved == 1) checked @endif />&nbsp;<span>I have reviewed my previous marketplace listing and would like to keep it the same</span><br/>
                            {{ Form::input('text', 'first_name', Input::old('first_name'), array('class' => 'inp-field', 'placeholder' => 'Company Contact First Name (will appear in listing)')) }}
                            {{ Form::input('text', 'last_name', Input::old('last_name'), array('class' => 'inp-field', 'placeholder' => 'Company Contact Last Name (will appear in listing)'))   }}
                            {{ Form::input('text', 'email', Input::old('email'), array('class' => 'inp-field', 'placeholder' => 'Company or Contact Email')) }}
                            @if ($errors->has('email')) <p class="red-text">{{ $errors->first('email') }}</p> @endif
                            {{ Form::input('text', 'phone', Input::old('phone'), array('class' => 'inp-field', 'placeholder' => 'Company or Contact Phone')) }}
                            {{ Form::input('text', 'company_name', Input::old('company_name'), array('class' => 'inp-field', 'placeholder' => 'Company Name')) }}
                            {{ Form::input('text', 'company_website', Input::old('company_website'), array('class' => 'inp-field', 'placeholder' => 'Website URL')) }}
                            {{ Form::input('text', 'address', Input::old('company_addresses'), array('class' => 'inp-field', 'placeholder' => 'Street Address')) }}
                            {{ Form::input('text', 'city', Input::old('city'), array('class' => 'inp-field', 'placeholder' => 'City')) }}
                            {{ Form::input('text', 'state_or_region', Input::old('state_or_region'), array('class' => 'inp-field', 'placeholder' => 'State/Region')) }}
                            {{ Form::input('text', 'postal_code', Input::old('postal_code'), array('class' => 'inp-field', 'placeholder' => 'Postal Code')) }}
                            <p>Please upload your Company Logo</p>
                            <p>File formats accepted include .pdf, .png, .jpg. Please make sure the files are web ready. Max size 300px in width.</p>
                            <div class="new-logo">
                                {{ Form::file('company_logo') }} @if(isset($user->company_logo)) <a class="btn btn-warning small-margin update-cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</a> @endif
                            </div>
                            @if(isset($user->company_logo))
                            <script> $('.new-logo').addClass('hidden');</script>
                            <div class="logo-preview col-sm-12 center">
                                <p><i>*preview does not reflect how the image will actually look</i></p>
                                <img src="{{ URL::to('/storage/files/company_logos') }}/{{ $user->company_logo }}" alt="{{ $user->company_name }}" class="logo-small-preview"/><br/>
                                <a class="btn btn-primary change-marketplace-logo"><span class="glyphicon glyphicon-picture"></span>&nbsp;Change Logo</a>
                            </div>
                            <div class="clearfix"></div>
                            @endif

                            <div id="wysihtml5-toolbar" style="display:none;">
                                <div class="btn-group">
                                    <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
                                    <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
                                </div>
                                <div class="btn-group">
                                    <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </a>
                                    <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
                                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                    </a>
                                    <a data-wysihtml5-command="createLink" class="btn btn-default">
                                        <span class="glyphicon glyphicon-link"></span>
                                    </a>
                                </div><br/>
                                <div data-wysihtml5-dialog="createLink" style="display: none;" class="create-link-container">
                                    <label>
                                        Link:
                                        <input data-wysihtml5-dialog-field="href" value="http://" class="text">
                                    </label>
                                    <a data-wysihtml5-dialog-action="save">OK</a> <a data-wysihtml5-dialog-action="cancel">Cancel</a>
                                </div>
                            </div>
                            <textarea class="inp-field description-text" id="editor" name="description" cols="10" rows="5" placeholder="Please insert your company's 250 word descriptions which will appear in your PDS Marketplace Listing">
					@if(isset($user->description))
						{{ $user->description }}
					@endif
                            </textarea>
                            <p class="word_count">Word Count: <span>0</span>/250</p>
                            <input class="inp-field description-html" id="hidden-html" name="html_edits" type="hidden" value="">
                            <p>What is Your Business Category? *&nbsp;@if($errors->has('category')) <span class="red-text">{{ $errors->first('category') }}</span> @endif</p>
                            {{ Form::select('category', $vendorDetails['allCategories'] , $vendorDetails['chosenCategories'], ['id' => 'categorySelect'] ) }}<br/>
                            <p>Subcategory *&nbsp;@if($errors->has('subCategory')) <span class="red-text">{{ $errors->first('subCategory') }}</span> @endif</p>
                            {{ Form::select('subCategory', $vendorDetails['allSubCategories'], $vendorDetails['chosenSubCategories'], ['id' => 'subCategorySelect']) }}<br>
                            <label for="company_blog_or_vlog">Insert your company's blog or video page url here (optional)</label>
                            {{ Form::input('text', 'company_blog_or_vlog', Input::old('company_blog_or_vlog'), array('class' => 'inp-field', 'placeholder' => 'Link to Your Company Blog or Video Page')) }}
                            <p><input type="checkbox" name="agree[]" value="agreed" title="I Agree" checked/>&nbsp;* I am aware that all content must be approved by PDS before being published on the Marketplace.<br/></p>
                            {{ Form::submit('Submit Your Marketplace Listing', array('class' => 'btn btn-success market-submit')) }}
                            {{ Form::close() }}
                        </div>
                        <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        </div>
                        </div>

                        @stop

                        @section('extra-js')
                        @parent
                        {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
                        {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
                        {{ HTML::script('js/vendor-signup.js') }}
                        <script>
                            jQuery(document).ready(function ($) {
                                $('.m-marketplace > li').addClass('active');
                                $('.m-profile > li').addClass('next');
                            });
                        </script>
                        @stop
