@extends('layouts.Conference.2015.master-layout')

@section('head')
  @parent
@stop

@section('content')

@include('Signup.2015-Conference.progress-partials.conference-progress')

<div class="hero-section">
<div class="black-overlay">
  <div class="page-center medium-padding">
    <h1 class="medium-padding form-title">Submit Your Conference Guide Advertisement @if($completed == 1) <br/><span class="completed-section">&nbsp;<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Completed<span> @endif</h2>
    <div class="col-md-7 small-padding small-waist">
        <p>Spread your message to all the attendees with an advertisement in the Conference Guide. This a great opportunity to capture their attention year round and drive them to your booth at the event.</p>
        <p class="orange-text" style="color: #F58220!important;"><b>Included in your package is:</b></p>
        <ul>
            <li>Bronze Sponsor... Purchase and ad to the right</li>
            <li>Silver Sponsor… Half Page Ad</li>
            <li>Gold Sponsor…. Full Page Ad</li>
            <li>Event Sponsor… Two Page Spread</li>
        </ul>
        <p>Interested in purchasing more space? Fill out the form on the right and upload your artwork. </p>
        <p>Document Setup Guidelines</p>
        <ul>
          <li>We accept high resolution JPG, PDF, PNG files</li>
          <li>Full page and spread ads require a minimum of 1/8” bleed.</li>
          <li>Half Page sizing specs: 7-1/4th x 5" </li>
          <li>Full Page sizing specs: 7-1/4th x 10" </li>
          <li>Resolution - image files need to be at least 300 dpi at 100% print size.</li>
          <li>All ads are full color. All artwork must be supplied by the exhibitor and approved by PDS. By submitting artwork, you agree that your submission is press-ready.</li>
        </ul>
    </div>
    <div class="col-md-5 off-white-text small-padding exhibitor-form">
      <div class="small-padding"><a href="{{ URL::route('conference.show.raffle') }}" class="btn btn-warning">Skip for now&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></div>
      {{ Form::open(array('route' => 'conference.create.guide-ad', 'class' => 'guide-ad-form', 'files' => 'true')) }}
        @if(Session::has('no_ad_types')) <span class="red-text">{{ $no_ad_types = Session::pull('no_ad_types') }}</span>  @endif
        <p class="small-padding">Select your ad type *@if ($errors->has('ad_type'))&nbsp;<span class="red-text">{{ $errors->first('ad_type', 'Please choose an ad type') }}</span> @endif</p>
        <input type="checkbox" name="ad_type[]" value="1" title="Half Page" @if(isset($guide_choices) && in_array(1, explode(",", $guide_choices->ad_types))) checked @endif />&nbsp;<span>$ 295......Half Page 7-1/4th x 5”</span><br/>
        <input type="checkbox" name="ad_type[]" value="2" title="Full Page" @if(isset($guide_choices) && in_array(2, explode(",", $guide_choices->ad_types))) checked @endif />&nbsp;<span>$ 395......Full Page  7-1/4th x 10”</span><br/>
        <input type="checkbox" name="ad_type[]" value="3" title="Two Page" @if(isset($guide_choices) && in_array(3, explode(",", $guide_choices->ad_types))) checked @endif />&nbsp;<span>$ 800......Two Page Spread</span><br/>
        <input type="checkbox" name="ad_type[]" value="4" title="Inside Front" @if(isset($guide_choices) && in_array(4, explode(",", $guide_choices->ad_types))) checked @endif />&nbsp;<span>$2,000.....Inside Front Cover</span><br/>
        <input type="checkbox" name="ad_type[]" value="5" title="Inside Back" @if(isset($guide_choices) && in_array(5, explode(",", $guide_choices->ad_types))) checked @endif />&nbsp;<span>$2,000.....Inside Back Cover</span><br/>
        <p class="small-padding">Billing Info: *@if ($errors->has('bill_info'))&nbsp;<span class="red-text">{{ $errors->first('bill_info', 'Please choose a billing option') }}</span> @endif</p>
        <?php

        if(isset($guide_choices)) {
         $choice_list = $guide_choices->billing_info;
        }else{
         $choice_list = 0;
        }

          switch($choice_list) {
            case '1':
              $onfile_checked = true;
              $paid_checked = false;
              $invoice_checked = false;
              break;
            case '2':
              $invoice_checked = true;
              $onfile_checked = false;
              $paid_checked = false;
              break;
            case '3':
              $paid_checked = true;
              $invoice_checked = false;
              $onfile_checked = false;
              break;
            default:
              $paid_checked = false;
              $invoice_checked = false;
              $onfile_checked = false;
          }
        ?>

        {{ Form::radio('bill_info', '1', $onfile_checked) }}&nbsp;<span>Please charge the credit card I have on file</span><br/>
        {{ Form::radio('bill_info', '2', $invoice_checked) }}&nbsp;<span>Send me an invoice</span><br/>
        {{ Form::radio('bill_info', '3', $paid_checked) }}&nbsp;<span>Paid for in Exhibitor Package</span><br/>
        <p class="small-padding">Click to Upload & Submit Your Ad *</p>
        <p class="small-padding">Please select a file from your computer by clicking on the Browse Button. File formats accepted include: .pdf, .png, .jpg</p>
        <div class="new-logo">
          {{ Form::file('company_ad') }} @if( isset($guide_choices) && !is_null($guide_choices->company_ad)) <a class="btn btn-warning small-margin update-cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</a> @endif
          @if ($errors->has('company_ad'))&nbsp;<span class="red-text">{{ $errors->first('company_ad', 'Please provide a graphic for you ad') }}</span> @endif
        </div>
        <p>If you would like to upload multiple files please upload the first one above and then your <a href="http://www.pharmacyowners.com/multiple-file-upload" target="_blank">additional files here</a>.
        @if(isset($guide_choices->company_ad))
          <script> $('.new-logo').addClass('hidden'); </script>
          <div class="logo-preview col-sm-12 center">
            <p><i>*preview does not reflect how the image will actually look</i></p>
            <img src="{{ URL::to('/storage/files/guide_ads') }}/{{ $guide_choices->company_ad }}" alt="{{ $user->company_name }}" class="logo-small-preview"/><br/>
            <a class="btn btn-primary change-marketplace-logo"><span class="glyphicon glyphicon-picture"></span>&nbsp;Change Conference Ad</a>
          </div><div class="clearfix"></div>
        @endif
        <p class="small-padding">I agree that this artwork submission was edited and will be published "as is" in the conference guide and that Pharmacy Development Services will not be making edits for grammar or spelling.</p>
        <input type="checkbox" name="agree[]" value="agreed" title="I Agree" checked/>&nbsp;<span>I Agree *@if ($errors->has('agree'))&nbsp;<span class="red-text">{{ $errors->first('agree') }}</span> @endif</span><br/><br/>
        {{ Form::submit('Submit Conference Guide Ad', array('class' => 'btn btn-success market-submit')) }}
      {{ Form::close() }}

    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
    <script>
        jQuery(document).ready(function($) { 
            $('.m-marketplace > li').addClass('visited');
            $('.m-profile > li').addClass('visited');
            $('.m-guide > li').addClass('active'); 
            $('.m-raffle > li').addClass('next'); 
        });
    </script>
@stop