<div class="panel panel-info">
	<div class="panel-heading">
		<h3><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Conference Venue and Hotel Reservations</h3>
	</div>
	<div class="panel-body">
            <b>Conference Venue: </b>
            <address>Marriott Renaissance Orlando <br>
            6677 Sea Harbor Drive <br>
            Orlando, FL 32821 </address>
            <p>(407) 351-5555 

            <p>Uh-oh... we sold out of room nights for one of the nights of the conference. 

            <p>If you still need to book your rooms, reservations can be made by contacting Gayanne at (561) 275-2654 or email her at gayanne@pharmacy-owners.com. She can work with you on your reservations and guide you through additional options. 

            <p>Please only book through Gayanne and not the hotel website or phone number to ensure that you receive all of the options available to PDS conference exhibitors and attendees.
	    
	</div>
</div>