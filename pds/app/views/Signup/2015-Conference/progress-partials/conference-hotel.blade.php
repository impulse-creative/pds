@extends('layouts.pds-advantage')


@section('head')
  @parent
  {{-- HTML::style('css/main-styles.css') --}}
@stop

@if($menu_data)
    @section('menu')
        @include($menu_data)
    @stop
@endif

@section('content')
	@parent
<div class="row-fluid">
    
    <div class="col-md-12  pass-reset-area">        
        @include('Signup.2015-Conference.progress-partials.conference-hotel-data')
    </div>
    
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
@stop