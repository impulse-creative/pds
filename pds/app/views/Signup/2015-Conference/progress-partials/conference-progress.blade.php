<div class="page-center">
	<div class="checkout-wrap">
  		<ul class="checkout-bar">
                    <a class="purple-text m-marketplace" href="{{ URL::route('conference.signup') }}"><li class="first">Marketplace Listing</li></a>
                    <a class="purple-text m-profile" href="{{ URL::route('conference.show.guide-entry') }}"><li class="">100-Word Profile</li></a>
                    <a class="purple-text m-guide" href="{{ URL::route('conference.show.guide-ad') }}"><li class="">Conference Guide Ads</li></a>
                    <a class="purple-text m-raffle" href="{{ URL::route('conference.show.raffle') }}"><li class="">Vendor Raffle</li></a>
                    <a class="purple-text m-tickets" href="{{ URL::route('conference.vip-tickets') }}"><li class="">Free Tickets</li></a>
                    <a class="purple-text m-sponsors" href="{{ URL::route('conference.choose.sponserships') }}"><li class="last">Sponsors</li></a>
  		</ul>
	</div>
</div>