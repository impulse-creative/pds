<div class="panel panel-info">
    <div class="panel-heading">
        <h3><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Conference Hours</h3>
    </div>
    <div class="panel-body">
        <table class="data">
            <tr>
                <td colspan="2">
                    <h3><strong>Wednesday, February 18, 2015</strong></h3>
                </td>
            </tr>
            <tr>
                <td>11:00am – 7:00pm</td>
                <td>Exhibitor Registration</td>
            </tr>
            <tr>
                <td>12:00pm – 6:00pm</td>
                <td>Exhibitor Setup</td>
            </tr>
            <tr>
                <td>6:00pm – 7:30pm</td>
                <td>Exhibitor Grand Opening & Welcome Reception</td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3><strong>Thursday, February 19, 2015</strong></h3>
                </td>
            </tr>
            <tr>
                <td>7:15am – 6:30pm</td>
                <td>Exhibitor Hours</td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3><strong>Friday, February 20, 2015</strong></h3>
                </td>
            </tr>
            <tr>
                <td>7:15am – 6:30pm</td>
                <td>Exhibitor Hours</td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3><strong>Saturday, February 21, 2015</strong></h3>
                </td>
            </tr>
            <tr>
                <td>7:15am – 5:30pm</td>
                <td>Exhibitor Hours</td>
            </tr>
            <tr>
                <td>5:30pm – 8:30pm</td>
                <td>Exhibitor Move Out</td>
            </tr>
        </table>
    </div>        
</div>