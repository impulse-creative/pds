    <hr/>
    <div class="page-center medium-padding">
        @include('Signup.2015-Conference.progress-partials.conference-hotel-data')
        <div class="clearfix"></div>
        @include('Signup.2015-Conference.progress-partials.exhibit-hours')
        <div class="clearfix"></div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Shipping Details</h3>
            </div>
            <div class="panel-body">
                <p>
                    Freeman is our official service contractor for all of your shipping needs. 
                    <a target="_blank" href="http://www.freemanco.com/store/show/landing.jsp?showID=401298&from=op">Please go through Freeman to ship your items</a>. It will be necessary to create an account with them if you have not already done so. 
                </p>

                <p>If it's necessary to ship any last minute items, please use FedEx and you must arrange to have items picked up and delivered to your booth. </p>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Transportation to Renaissance Orlando</h3>
            </div>
            <div class="panel-body">
                <ul>
                    <li><strong>Mears Shuttle Service:</strong> Transportation can be arranged one  level  below Baggage Claim at  the  Mears  desk  located next  to  the  car rental   services.  The  telephone  number  is 800-759-5219.  Fee is $20-$25</li>
                    <li><strong>Taxi Service:</strong> Cabs are readily available at the airport approximately $60-$62 each way.</li>
                    <li><strong>Private Limo Service:</strong> Make arrangements with <a href="http://www.transportationorlandoflorida.com/" target="_blank">RP Tours</a> ahead of time</li>
                </ul>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Electricity</h3>
            </div>
            <div class="panel-body">
                <p>If you ordered standard electric (5 amps) and you will have multiple computers and need more power, please let us know and we can make arrangements to ensure that you have the power you need. Basic electric will only power 2 laptops. <br/><br/>Contact Carole directly at <a href="5612752637">(561) 275-2637</a> or <a href="mailto:carole@pharmacy-owners.com">carole@pharmacy-owners.com</a>.
            </div>
        </div>
    </div>
    <div class="clearfix"></div>