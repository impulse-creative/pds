@extends('layouts.Conference.2015.master-layout')

@section('head')
  @parent
  {{ HTML::style('css/Conference/2015/wysiwig-styles.css') }}
@stop

@section('content')
<?php
  $registered_user = DB::table('guide_entry')->where('user_id', '=', $id)->first();

  if (is_null($registered_user)) {
    $guide_entries = $user;
    $model_bind = $user;
  }else{
    $model_bind = $guide_entries;
  }
?>

@include('Signup.2015-Conference.progress-partials.conference-progress')

<div class="hero-section">
<div class="black-overlay">
  <div class="page-center medium-padding">
    <h1 class="medium-padding form-title">Submit Your Free 100 Word Exhibitor Directory Listing Information @if($completed == 1) <br/><span class="completed-section">&nbsp;<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Completed<span> @endif</h2>
    <div class="col-md-7 small-padding small-waist">
        <p>Each Exhibitor receives a free listing in the Conference Guide Exhibitor Directory Section</p>

        <p>This is where you describe your company and add contact information and your company Logo. The information you provide to the right will appear in the guide.</p>

        <p><span class="orange-text">Want more exposure in the conference guide?</span> Purchase additional space to feature your company throughout the guide. To purchase additional space, go to the <a href="{{ URL::route("conference.show.guide-ad") }}">Conference Guide Ad</a> page.</p>

        <p>Questions? Please contact <a href="mailto:carole@pharmacy-owners.com">Carole Bebout</a> at  <a href="tel:561-275-2637">561-275-2637</a>.</p>
       
    </div>
    <div class="col-md-5 off-white-text small-padding small-waist exhibitor-form">
      <div class="small-padding"><a href="{{ URL::route('conference.show.guide-ad') }}" class="btn btn-warning">Skip for now&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></div>
      {{ Form::model($model_bind, array('route' => 'conference.create.guide-entry', 'class' => 'guide-entry-form', 'files' => 'required')) }}
        {{ Form::input('text', 'sales_first_name', Input::old('sales_first_name'), array('class' => 'inp-field', 'placeholder' => 'Sales Contact First Name(will appear in listing)', 'required' => 'required')) }}
        {{ Form::input('text', 'sales_last_name', Input::old('sales_last_name'), array('class' => 'inp-field', 'placeholder' => 'Sales Contact Last Name(will appear in listing)', 'required' => 'required')) }}
        {{ Form::input('text', 'sales_email', Input::old('sales_email'), array('class' => 'inp-field', 'placeholder' => 'Sales Contact Email(will appear in listing)', 'required' => 'required')) }}
        @if($errors->has('sales_email')) <p class="red-text">{{ $errors->first('sales_email', 'Email is already in use.') }}</p> <script style="display:none">$('input[name=sales_email]').addClass('has-error');</script> @endif
        {{ Form::input('text', 'company_name', Input::old('company_name'), array('class' => 'inp-field', 'placeholder' => 'Company Name', 'required' => 'required')) }}
        {{ Form::input('text', 'company_website', Input::old('company_website'), array('class' => 'inp-field', 'placeholder' => 'Website URL')) }}
        {{ Form::input('text', 'address', Input::old('company_address'), array('class' => 'inp-field', 'placeholder' => 'Street Address')) }}
        {{ Form::input('text', 'city', Input::old('city'), array('class' => 'inp-field', 'placeholder' => 'City')) }}
        {{ Form::input('text', 'state_or_region', Input::old('state_or_region'), array('class' => 'inp-field', 'placeholder' => 'State/Region')) }}
        {{ Form::input('text', 'postal_code', Input::old('postal_code'), array('class' => 'inp-field', 'placeholder' => 'Postal Code')) }}
        <p>Please upload a file for your Company Logo</p>
        <p>Please select a file from your computer. File formats accepted include .pdf, .png, .jpg</p>
        <div class="new-logo">
          {{ Form::file('company_guide_entry_img') }} @if(isset($guide_entries->company_guide_entry_img)) <a class="btn btn-warning small-margin update-cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</a> @endif
        </div>
        @if(isset($guide_entries->company_guide_entry_img))
          <script> $('.new-logo').addClass('hidden'); </script>
          <div class="logo-preview col-sm-12 center">
            <p><i>*preview does not reflect how the image will actually look</i></p>
            <img src="{{ URL::to('/storage/files/guide_logos') }}/{{ $guide_entries->company_guide_entry_img }}" alt="{{ $guide_entries->company_name }}" class="logo-small-preview"/><br/>
            <a class="btn btn-primary change-marketplace-logo"><span class="glyphicon glyphicon-picture"></span>&nbsp;Change Logo</a>
          </div>
          <div class="clearfix"></div>
        @endif
        <div id="wysihtml5-toolbar" style="display:none;">
            <div class="btn-group">
                <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
                <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
            </div>
            <div class="btn-group">
                <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
                    <span class="glyphicon glyphicon-list"></span>
                </a>
                <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
                </a>
                <a data-wysihtml5-command="createLink" class="btn btn-default">
                  <span class="glyphicon glyphicon-link"></span>
                </a>
            </div>
            <br/>
            <div data-wysihtml5-dialog="createLink" style="display:none; ">
              <label>
                Link:
                <input data-wysihtml5-dialog-field="href" value="http://" style="color:black;">
              </label>
              <a data-wysihtml5-dialog-action="save">OK</a>
              <a data-wysihtml5-dialog-action="cancel">Cancel</a>
            </div>
        </div>
        @if ($errors->has('description'))
          {{ Form::textarea('description', $errors->first('description'), array('class' => 'inp-field has-error description-text hundred-words', 'placeholder' => 'Your Company 100-Word Description for the Conference Guide', 'required' => 'required', 'size' => '10x5')) }}
        @else
          <textarea class="inp-field description-text hundred-words" id="editor" name="description" cols="10" rows="5" placeholder="Your 100-Word Description for the Conference Guide(will appear in listing)">
            @if(isset($guide_entries->description))
              {{ $guide_entries->description }}
            @endif
          </textarea>
        @endif
        <p class="word_count">Word Count: <span>0</span>/100</p>
        <input class="inp-field description-html" id="hidden-html" name="html_edits" type="hidden" value="">
        <p>Yes! I have edited all of my information. I understand it will be published "as is" in the Conference Guide and Pharmacy Development Services will not be making edits for grammar, spelling or design.</p>
        <input type="checkbox" name="agree[]" value="agreed" title="I Agree" checked/>&nbsp;<span>I Agree *</span>@if ($errors->has('agree')) <span class="red-text">{{$errors->first('agree')}}</span> @endif<br/>
        <br>
        {{ Form::submit('Submit Your 100 Word Exhibitor Directory Listing', array('class' => 'btn btn-success market-submit')) }}
      {{ Form::close() }}
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
</div>
@stop

@section('extra-js')
  @parent
  {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
  {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
  {{ HTML::script('js/vendor-signup.js') }}
    <script>
        jQuery(document).ready(function($) { 
            $('.m-marketplace > li').addClass('visited');
            $('.m-profile > li').addClass('active'); 
            $('.m-guide > li').addClass('next'); 

           //console.log($('#wysihtml5-toolbar').length);

        });
    </script>
@stop