@extends('layouts.Conference.2015.master-layout')

@section('head')
  @parent
@stop

@section('content')

@include('Signup.2015-Conference.progress-partials.conference-progress')

<div class="hero-section">
    <div class="black-overlay">
          <div class="page-center medium-padding">
            <h1 class="medium-padding form-title">Purchase A Sponsorship Item & Submit @if($completed == 1) <span class="completed-section">&nbsp;<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Completed<span> @endif</h2>
            <div class="col-md-6 small-padding">
              <p class="off-white-text small-padding small-waist">
              <p><strong>We have some great opportunities for you to take advantage of. Don't delay, select your favorite sponsorship opportunity now.</strong></p>
              <p>Have a great idea for a sponsorship and don’t see it listed <a href="tel:561-275-2637">call Carole 561-275-2637</a></p>
                <div id="sponsorship-details">
                    <p>$4,000 Reusable Water Bottles<br>
                    Your logo will be with every attendee wherever they go as they use this water bottle to hydrate them all 3 days.
                    <p>$3,500 Conference Tote Bags <br>
                    Your Logo on the tote will be 1 color. This sponsorship also includes a one-page complimentary bagstuffer that is provided by the exhibitor.
                    <p>$3,000 Introduction of Keynote Speaker<br>
                    Greet the attendees for 1 minute and introduce the keynote speaker. 
                    <p>$3,000 Entrepreneur of the Year <br>
                    Sponsor the Entrepreneur of the Year and be a part of announcing the award.
                    <p>$3,000 Pharmacist of the Year <br>
                    Sponsor the Pharmacist of the Year and be a part of announcing the award.
                    <p>$3,000 Hotel Room Custom Keys <br>
                    Place your company logo on the front of hotel room keys for conference attendees.
                    <p>$3,000 Registration Area Sponsors <br>
                    Have your logo prominently displayed at the Registration Desks for everyone to see. 
                    <p>$2,500 Lunch Sponsor<br>
                    Have your logo prominently displayed at the lunch entrance.
                    <p>$2,000 Floor Stickers <br>
                    This sponsorship includes (4) floor stickers to include your company logo along with the conference theme. 
                    <p>$2,000 Refreshment Break Sponsor<br>
                    Have your logo prominently displayed at the refreshment table.
                    <p>$1,500. Tote Bag Stuffer <br>
                    Item is supplied by exhibitor and approved by PDS.
                    <p>$1,5000. Chair Drop<br>
                    Chair Drop is supplied by Exhibitor and approved by PDS.
                    <p>$1,000 Name Badge Lanyard<br>
                    Draped around the neck of every attendee all three days, display your company’s logo and/or name on a flat, 8’’ L x ¼’’ H lanyard ribbon.
                    <p>$800 Conference Pens <br>
                    Put your name in their hands as the attendees take notes. Exhibitor provides pens. Choice of placement in the Tote bag or a Table Drop. 
                </div>
              </p>
            </div>
            <div class="col-md-6 off-white-text small-padding small-waist exhibitor-form">
            <div class="small-padding"><a href="{{ URL::route('dash.main') }}" class="btn btn-warning">Save And Finish Later&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></a></div>
                {{ Form::open(array('route' => 'conference.add.sponsorships', 'class' => 'guide-advertisement-form', 'files' => 'true')) }}
                    <p class="small-padding">Billing Info: *@if ($errors->has('bill_info'))&nbsp;<span class="red-text">{{ $errors->first('bill_info', 'Please choose a billing option') }}</span> @endif</p>
                    <?php
                        switch ($bill_type) {
                            case '1':
                                $onfile = true;
                                $invoice = false;
                                break;
                            case '2':
                                $onfile = false;
                                $invoice = true;
                                break;
                            default:
                                $onfile = false;
                                $invoice = false;
                                break;
                        }
                    ?>
                    {{ Form::radio('bill_info', '1', $onfile) }}&nbsp;<span>Pay with credit card on file</span><br/>
                    {{ Form::radio('bill_info', '2', $invoice) }}&nbsp;<span>Pay by check and send me an invoice</span><br/>
                    <p class="small-padding">Select Your Desired Sponsorship Opportunities *</p>
                    @if($errors->has('opportunities')) <p class="red-text">{{ $errors->first('opportunities', 'Please select an ad type for your guide sponsorship.') }}</p> @endif
                    <div id="sponsorship-inputs">
                        @foreach($sponsorships as $key => $value)
                        <label>
                            <input type="checkbox" name="opportunities[]" value="{{ $value->id }}"
                                    @if(isset($sponsor_choices) && in_array( $value->id, $sponsor_choices)) checked @endif
                                    @if(isset($sponsor_choices) && $value->sale_limit == 0 && !in_array( $value->id, $sponsor_choices)) disabled @endif
                            />&nbsp;
                            <span class="@if(isset($sponsor_choices) && $value->sale_limit == 0 && !in_array( $value->id, $sponsor_choices)) sold-out @endif">
                                {{ $value->price }}&nbsp;USD.....{{$value->type}}&nbsp;
                                @if($value->sale_limit > 0) ( {{ $value->sale_limit }} available ) @endif
                                @if($value->sale_limit == 0) <span class="orange-text">( SOLD OUT )</span> @endif
                            </span>
                        </label>
                        @endforeach
                    </div>
                    <br>
                    {{ Form::submit('Submit Sponsorship Item!', array('class' => 'btn btn-success market-submit')) }}
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
          </div>
        <div class="clearfix"></div>
    </div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
    <script>
        jQuery(document).ready(function($) { 
            $('.m-marketplace > li').addClass('visited');
            $('.m-profile > li').addClass('visited');
            $('.m-guide> li').addClass('visited');
            $('.m-raffle > li').addClass('visited');
            $('.m-tickets> li').addClass('visited');
            $('.m-sponsors > li').addClass('active'); 
        });
    </script>
@stop