@extends('layouts.Conference.2015.master-layout')

@section('head')
  @parent
@stop

@section('content')

@include('Signup.2015-Conference.progress-partials.conference-progress')

<div class="hero-section">
<div class="black-overlay">
  <div class="page-center medium-padding">
    <h1 class="medium-padding form-title"> Submit your Raffle Prize Information @if($completed == 1) <span class="completed-section">&nbsp;<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Completed<span> @endif</h2>
    <div class="col-md-7 small-padding small-waist">
        <p>Don’t lose your stage time in front of 1,000 Attendees! Tell us what your are bringing to raffle.</p>

        <p>How this works... each attendee receives a raffle ticket upon registration. During the Conference, each Exhibitor who donates a giveaway will be allowed one minute on stage where you can let the entire audience know more about your company. Then you select a raffle ticket and give away your item to a lucky attendee.</p>

        <p>Remember... the only way to go on stage is to inform us what your raffle item is by February 10, 2015.</p>

        <p>Please bring raffle item with you to the conference. </p>
        
    </div>
    <div class="col-md-5 off-white-text small-padding small-waist exhibitor-form">
      <div class="small-padding"><a href="{{ URL::route('conference.vip-tickets') }}" class="btn btn-warning">Skip for now&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></div>
      {{ Form::model($user, array('route' => 'conference.new.giveaway.item', 'class' => 'giveaway-form', 'files' => 'true')) }}
        {{ Form::label('Giveaway Item *') }}
        {{ Form::input('text', 'item_name', Input::old('item_name'), array('class' => 'inp-field', 'placeholder' => 'What is Your Giveaway Item?', 'required' => 'true')) }}
        <br><br>
        {{ Form::submit('Submit Raffle Prize Information', array('class' => 'btn btn-success market-submit')) }}
      {{ Form::close() }}
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
    <script>
        jQuery(document).ready(function($) { 
            $('.m-marketplace > li').addClass('visited');
            $('.m-profile > li').addClass('visited');
            $('.m-guide> li').addClass('visited');
            $('.m-raffle > li').addClass('active'); 
            $('.m-tickets > li').addClass('next'); 
        });
    </script>  
@stop