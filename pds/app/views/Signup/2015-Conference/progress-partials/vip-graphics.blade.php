@extends('layouts.Conference.2015.master-layout')

@section('head')
  @parent
@stop

@section('content')
<div class="page-center">
  <div class="checkout-wrap">
      <ul class="checkout-bar">
        <li class="visited first">Marketplace Listing</li>
        <li class="visited">100-Word Profile</li>
        <li class="visited">Conference Guide AD</li>
        <li class="visited">Vendor Raffle</li>
        <li class="active">VIP Graphics</li>
        <li class="next">Sponsors</li>
      </ul>
  </div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
@stop