@extends('layouts.Conference.2015.master-layout')

@section('head')
  @parent
@stop

@section('content')

@include('Signup.2015-Conference.progress-partials.conference-progress')

<div class="hero-section">
<div class="black-overlay">
  <div class="page-center medium-padding">
    <h1 class="medium-padding form-title">Your chance to give away a free conference ticket @if($completed == 1) <span class="completed-section">&nbsp;<span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Completed<span> @endif</h2>
    <div class="col-md-7 small-padding">
        <p>Every year we invite each of our exhibitors to give One Free PDS Pharmacy Growth Conference ticket away to a pharmacy owner of their choosing (<span class="orange-text">non-PDS member</span>). Who would you like to invite to attend?</p>
        <p>This is your chance to share with a special customer an unparalleled conference experience. Invite them to the premiere pharmacy event of the year, where they can join hundreds of the most progressive Pharmacy Entrepreneurs to sharpen their mind, broaden their leadership skills and discover ready-to-implement ideas that will make their business take off.</p>
        <p>Once you have confirmed with your pharmacy customer that they are able to attend, please fill out this form and we will personally call to register them for the conference.</p>
        <p><b>Before you give us a name, please make sure that your customer:</b></p>
        <ul>
            <li>Has accepted and is available to attend the conference on February 18-21st</li>
            <li>Is NOT already a PDS member</li>
        </ul>

        <p>To make things a bit easier for you, we created a <a href="http://cdn2.hubspot.net/hub/37772/file-2156583483-pdf/PDS-2015_Conference_Flyer-Exhibitor_general.pdf?t=1417454405167">conference flyer</a> and a <a href="http://cdn2.hubspot.net/hub/37772/file-2160806247-pdf/NEW-PDS_Brochure_12.1.2014.pdf?t=1417454068423">PDS Brochure</a> that you can print or forward to your pharmacy customers to provide them with a little more information about PDS and the conference. Remember, use your code for $500 off a conference ticket to invite as many people as you wish. But you only have a limited number of FREE tickets to give away! </p>
        <ul>
            <li>Bronze Sponsor (1) Ticket to give away</li>
            <li>Silver Sponsor (2) Tickets to give away</li>
            <li>Gold Sponsor (3) Tickets to give away</li>
            <li>Event Sponsor (3) Tickets to give away</li>
        </ul>

        <p>So go ahead, fill out the form and make your customer's day! </p>

        <p>Call Carole at <a href="tel:5612752637">561-275-2637</a></p>

    </div>
    <div class="col-md-5 off-white-text small-padding small-waist exhibitor-form">
      <div class="small-padding"><a href="{{ URL::route('conference.choose.sponserships') }}" class="btn btn-warning">Skip for now&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></div>
      @if($errors->has())
        <span class="red-text">Please fill out at least one recipient form to continue.</span>
      @endif
      {{ Form::model($user, array('route' => 'conference.add.vip-tickets', 'class' => 'market-listing-form', 'files' => 'true')) }}
      @if (Session::has('Update Failure'))
            <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $response = Session::pull('Update Failure', 'default') }}</div>
      @endif
        <div class="vip-section">
          {{ Form::input('text', 'vip_1_fname', Input::old('vip_1_fname'), array('class' => 'inp-field', 'placeholder' => '1st Customers First Name')) }}<br/>
          @if ($errors->has('vip_1_fname')) <script style="display:none">$('input[name=vip_1_fname]').addClass('has-error');</script> @endif
          {{ Form::input('text', 'vip_1_lname', Input::old('vip_1_lname'), array('class' => 'inp-field', 'placeholder' => '1st Customers Last Name')) }}<br/>
          @if ($errors->has('vip_1_lname')) <script style="display:none">$('input[name=vip_1_lname]').addClass('has-error');</script> @endif
          {{ Form::input('email', 'vip_1_email', Input::old('vip_1_email'), array('class' => 'inp-field', 'placeholder' => '1st Customers Email')) }}<br/>
          @if ($errors->has('vip_1_email')) <script style="display:none">$('input[name=vip_1_email]').addClass('has-error');</script> @endif

          {{ Form::input('phone', 'vip_1_phone', Input::old('vip_1_phone'), array('class' => 'inp-field', 'placeholder' => '1st Customers Phone Number')) }}<br/>
          @if ($errors->has('vip_1_email')) <script style="display:none">$('input[name=vip_1_phone]').addClass('has-error');</script> @endif

          {{ Form::input('text', 'vip_1_company', Input::old('vip_1_company'), array('class' => 'inp-field', 'placeholder' => '1st Customers Company Name')) }}<br/>
          @if ($errors->has('vip_1_company')) <script style="display:none">$('input[name=vip_1_company]').addClass('has-error');</script> @endif
        </div>
        <div class="vip-section">
          {{ Form::input('text', 'vip_2_fname', Input::old('vip_2_fname'), array('class' => 'inp-field', 'placeholder' => '2nd Customers First Name')) }}<br/>
          @if ($errors->has('vip_2_fname')) <script style="display:none">$('input[name=vip_2_fname]').addClass('has-error');</script> @endif
          {{ Form::input('text', 'vip_2_lname', Input::old('vip_2_lname'), array('class' => 'inp-field', 'placeholder' => '2nd Customers Last Name')) }}<br/>
          @if ($errors->has('vip_2_lname')) <script style="display:none">$('input[name=vip_2_lname]').addClass('has-error');</script> @endif
          {{ Form::input('email', 'vip_2_email', Input::old('vip_2_email'), array('class' => 'inp-field', 'placeholder' => '2nd Customers Email')) }}<br/>
          @if ($errors->has('vip_2_email')) <script style="display:none">$('input[name=vip_2_email]').addClass('has-error');</script> @endif

          {{ Form::input('phone', 'vip_2_phone', Input::old('vip_2_phone'), array('class' => 'inp-field', 'placeholder' => '2nd Customers Phone Number')) }}<br/>
          @if ($errors->has('vip_2_phone')) <script style="display:none">$('input[name=vip_2_phone]').addClass('has-error');</script> @endif

          {{ Form::input('text', 'vip_2_company', Input::old('vip_2_company'), array('class' => 'inp-field', 'placeholder' => '2nd Customers Company Name')) }}<br/>
          @if ($errors->has('vip_2_company')) <script style="display:none">$('input[name=vip_2_company]').addClass('has-error');</script> @endif
        </div>
        <div class="vip-section">
          {{ Form::input('text', 'vip_3_fname', Input::old('vip_3_fname'), array('class' => 'inp-field', 'placeholder' => '3rd Customers First Name')) }}<br/>
          @if ($errors->has('vip_3_fname')) <script style="display:none">$('input[name=vip_3_fname]').addClass('has-error');</script> @endif
          {{ Form::input('text', 'vip_3_lname', Input::old('vip_3_lname'), array('class' => 'inp-field', 'placeholder' => '3rd Customers Last Name')) }}<br/>
          @if ($errors->has('vip_3_lname')) <script style="display:none">$('input[name=vip_3_lname]').addClass('has-error');</script> @endif
          {{ Form::input('email', 'vip_3_email', Input::old('vip_3_email'), array('class' => 'inp-field', 'placeholder' => '3rd Customers Email')) }}<br/>
          @if ($errors->has('vip_3_email')) <script style="display:none">$('input[name=vip_3_email]').addClass('has-error');</script> @endif

          {{ Form::input('phone', 'vip_3_phone', Input::old('vip_3_phone'), array('class' => 'inp-field', 'placeholder' => '3rd Customers Phone Number')) }}<br/>
          @if ($errors->has('vip_3_phone')) <script style="display:none">$('input[name=vip_3_phone]').addClass('has-error');</script> @endif

          {{ Form::input('text', 'vip_3_company', Input::old('vip_3_company'), array('class' => 'inp-field', 'placeholder' => '3rd Customers Company Name')) }}<br/> 
          @if ($errors->has('vip_3_company')) <script style="display:none">$('input[name=vip_3_company]').addClass('has-error');</script> @endif  
        </div>
           <br>
        {{ Form::submit('Submit your free conference ticket(s)', array('class' => 'btn btn-success market-submit')) }}
      {{ Form::close() }}
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
    <script>
        jQuery(document).ready(function($) { 
            $('.m-marketplace > li').addClass('visited');
            $('.m-profile > li').addClass('visited');
            $('.m-guide> li').addClass('visited');
            $('.m-raffle > li').addClass('visited');
            $('.m-tickets > li').addClass('active'); 
            $('.m-sponsors > li').addClass('next'); 
        });
    </script>  
@stop