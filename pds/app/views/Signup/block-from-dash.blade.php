@extends('layouts.request-confirm')

@section('content')

	<div class="row row-content">
		<div id="conf-container">
                    <p>
                        <h2>You're almost done! Next Steps...</h2>
                    </p>
                    <p>A confirmation email was sent. Check your email inbox, and THEN YOUR SPAM BOX. The email was sent to {{ Auth::user()->email }} from postmaster @ mg.pharmacyowners.com.
                        Please click on the link in the message to complete your registration. If you do not see the email in a few moments, please check
                        your spam folder again.
                    </p>
                    <p>
                        Upon confirmation, you will receive instant access to the PDSadvantage dashboard and other features. if you have any questions, please
                        call <a href="tel:8009877386">(800) 987-7386</a>
                    </p>
                    <p>
                        Thank you for participating in the PDS Community!
                    </p>
                    <h2>If you are having issues, try one of the links below</h2>
                    <p>
                        <a href="{{ URL::route('dash.logout') }}">Logout and try again.</a>
                    </p>
                    


                    <br>
		</div>
	</div>
        <script src="{{ URL::to('/js/global.js') }}"></script>      
      
        
@stop