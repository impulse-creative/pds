@extends('layouts.request-confirm')

@section('content')

	<div class="row row-content">
		<div id="conf-container">
                    <h2>You're almost done! Now you have to choose your password.</h2>
                    @if(Session::has('Error'))
                        <div class="alert alert-danger" role="alert">There was an error, please try typing in a password again again</div>
                    @endif
                    {{ Form::open(array('url' => 'activate/save/password')) }}
                    <input type="hidden" name="userid" id="userid" value="{{ $user->id }}" /> 
                    {{ Form::label('password', 'New Password:') }}
                    {{ Form::password('password') }}
                    <div class="center">
                        
                        {{ Form::submit('Set My Password', ['class' => 'btn btn-primary', 'id' => 'set-password']) }}
                    </div>

                    {{ Form::close() }}
                </div>
	</div>
        <script src="{{ URL::to('/js/global.js') }}"></script>      
        
@stop