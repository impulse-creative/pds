@extends('layouts.request-confirm')

@section('content')
	<div class="reset-form-column transparent-background medium-padding medium-waist">
	{{ Form::open(array('route' => 'confirm.email', 'class' => 'reset-form')) }}
		<div>
			<h1 class="white-text">Confirm Your Account</h1><br/><hr/>
		</div>
    	<input type="hidden" name="token" value="{{ $token }}">
    	<input type="hidden" name="user_id" value="{{ $id }}">
    	<input class="login-form-submit" type="submit" value="Click to Confirm">
	{{ Form::close() }}
	<div class="clearfix"></div>
	</div>
@stop