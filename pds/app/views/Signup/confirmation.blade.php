<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
            <table width="600">
                <tr>
                    <td>
                        <img src="{{ asset('images/application/email-header.png') }}" alt="Pharmacy Development Services Logo" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>To confirm your account, please click this link: {{ URL::to('confirm/email', array($token, $id)) }}.<br/>
                        <p>Thank you for registering for our PDS Exhibitor Portal, {{ $name }}!</p>

                        <p>This is where {{ $company }}’s personalized conference portal lives. You will find pertinent information regarding show details<br/> and other great tools. Most importantly, this is the portal where {{ $company }} will input all of your information, such as:</p>

                        <ul>
                            <li>250-Word Marketplace Logo & Description</li>
                            <li>100-Word Conference Guide Logo & Description</li>
                            <li>Conference Guide Advertising Upgrades & Orders</li>
                            <li>Raffle Submission for 1-Minute Stage Time</li>
                            <li>Free Conference Ticket Submission</li>
                            <li>Sponsorship Orders</li>
                        </ul>
                        <p>We’re excited for you to experience this portal. If you have any questions or concerns please contact <a href="mailto:carole@pharmacy-owners.com">Carole Bebout</a> at  <a href="tel:561-275-2637">561-275-2637</a>.
                        <p>
                            - PDS team
                        </p>
                    </td>
                </tr>
            </table>
	</body>
</html>