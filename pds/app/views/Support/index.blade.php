@extends('layouts.pds-advantage')


@section('head')
  @parent
  {{ HTML::style('css/dash-styles.css') }}
  {{ HTML::style('css/main-styles.css') }}
@stop

@if($menu_data)
    @section('menu')
        @include($menu_data)
    @stop
@endif

@section('content')
	@parent
<div class="row-fluid">
   
    <div class="col-md-12  pass-reset-area">
        @if(!$off_menu) 
            <h1>Support</h1> 
            <p class="pull-left"><a href="mailto:carole@pharmacy-owners.com">Carole Bebout</a> @ <a href="tel:5612752637">(561) 275-2637</a></p>
            <p>&nbsp;|&nbsp;<a href="mailto:carole@pharmacy-owners.com">carole@pharmacy-owners.com</a></p>
            <p class="pull-left"><a href="mailto:koreen@pharmacy-owners.com">Koreen Leavitt</a> @ <a href="tel:5612752659">(561) 275-2659</a></p>
            <p>&nbsp;|&nbsp;<a href="mailto:koreen@pharmacy-owners.com">koreen@pharmacy-owners.com</a></p>
            <div class="clearfix"></div>
        @else
            <div class="container"><h1> Exhibitor FAQ </h1></div>
        @endif
        @include('Signup.2015-Conference.progress-partials.faq-data')
    </div>
    <div class="clearfix"></div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
@stop