@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">
    
    <div class="col-sm-12 main dash-access-groups">
        <div id="access-groups-manager-header">
            <h1>Edit Access Group</h1>
            <hr>
            <div class="response_errors">
                
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                
                @if($errors->all())
                    @foreach($errors->all() as $key => $error)
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $error }}</div>
                    @endforeach
                @endif
            </div>
    	</div>
        <div class="access-groups-create">
            {{ Form::open(array('url' => 'access-groups', 'files' => true)) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::text('description', null, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('permissions-needed', 'Permissions Needed') }}
                    <div class="list-permissions">
                        <ul>
                            @foreach($permissionsArray as $id => $name)
                                <li class="label label-default" data-id="{{ $id }}">{{ $name }}</li>
                            @endforeach
                        </ul>
                    </div>
                    {{ Form::text('permissions', Input::old('permissions'), array('class' => 'form-control hidden', 'id' => 'permissions-container')) }}
                </div>
            
                {{ Form::submit('Save the Access Group!', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
        
        
        
        
        
    </div>
  </div>
    
     
@endif
{{ HTML::script('js/access-groups-admin.js') }}

@stop