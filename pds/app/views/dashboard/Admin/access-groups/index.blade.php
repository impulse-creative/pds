@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(1, $permissions))

  <div class="container access-group-admin-container">

    <div class="col-sm-12 main dash-access-group">
        <div id="access-group-manager-header">
            <h1>Manage Access Groups</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="access-group-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('access-groups/create') }}">Add Access Group</a>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Group Name</td>
                            <td>Associated Permissions</td>
                            <td>Edit</td>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($accessGroups as $group)
                    <?php $activePermissions = explode(':', $group->permissions); ?>
                        <tr>
                            <td>{{ $group->id }}</td>
                            <td>{{ $group->name }}</td>
                            <td>
                                @foreach($activePermissions as $key => $value)
                                    <span class="btn btn-primary">
                                        {{ $permissionsArray[$value] }} <span class="badge">{{ $value }}</span>
                                    </span>
                                @endforeach
                            </td>
                            <td>@if(in_array(5, $permissions))
                                <a href="{{ URL::to('access-groups/'.$group->id.'/edit') }}"><span class="glyphicon glyphicon-pencil"></span> edit</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>

@endif
        
        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop