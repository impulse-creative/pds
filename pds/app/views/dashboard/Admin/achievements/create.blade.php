@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">
    
    <div class="col-sm-12 main dash-achievements">
        <div id="achievement-manager-header">
            <h1>Create Achievement</h1>
            <hr>
            <div class="response_errors">
                
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                
                @if($errors->all())
                    @foreach($errors->all() as $key => $error)
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $error }}</div>
                    @endforeach
                @endif
            </div>
    	</div>
        <div class="achievement-create">
            {{ Form::open(array('url' => 'achievements')) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
                </div>

                {{ Form::submit('Create the Achievement!', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
        
        
        
        
        
    </div>
  </div>

        
@endif       
        
        
     

{{-- HTML::script('js/gamify-admin.js') --}}

@stop