@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">

    <div class="col-sm-12 main dash-achievements">
        <div id="achievement-manager-header">
            <h1>Manage Achievements</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="achievement-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('achievements/create') }}">Add Achievement</a>
            </div>
            
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Description / Suggestion</td>
                        <td>Active</td>
                        <td>Edit</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($achievements as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->description }}</td>
                        <td>{{ $value->active }}</td>
                        <td><a href="{{ URL::to('achievements/'.$value->id.'/edit') }}"><span class="glyphicon glyphicon-pencil"></span> edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>

@endif
        
        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop