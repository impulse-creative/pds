@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">
    
    <div class="col-sm-12 main dash-achievements">
        <div id="achievement-manager-header">
            <h1>Achievement: {{ $achievement->name }}</h1>
            
    	</div>
        <div class="achievement-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('achievements/create') }}">Add Achievement</a>
                <a class="btn btn-small btn-info" href="{{ URL::to('achievements/'.$achievement->id.'/create') }}">Edit Achievement</a>
            </div>
            
            <div class="jumbotron text-center">
                <h2>{{ $achievement->name }}</h2>
                <p>
                    <strong>Description:</strong> {{ $achievement->description }}<br>
                </p>
            </div>
        </div>
        
        
        
        
        
    </div>
  </div>

        
@endif        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop