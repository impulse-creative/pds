@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(1, $permissions))

  <div class="container approvals-admin-container">

    <div class="col-sm-12 main dash-approvals-listings">
        <div id="approvals-manager-header">
            <h1>Users to Approve</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="approvals-display center">
            {{ $users->links() }}
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Company</td>
                            <td>Account Type</td>
                            <td>Sign Up Date</td>
                            <td>Email / Confirmed</td>
                            <td>Location</td>
                            <td>Approval</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)   
                        <tr>
                            <td>{{ $user->fname }} {{ $user->lname }}</td>
                            <td>{{ $user->pharmacy }}</td>

                            <td>{{ $user->account_type == 2 ? 'Vendor' : 'Pharmacy' }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                {{ $user->email }} / 
                                {{ $user->confirmed_user == 1 ? '<span class="green-text glyphicon glyphicon-check"></span>' : '<span class="red-text glyphicon glyphicon-ban-circle"></span>' }}
                            </td>
                            <td>@if(isset($user->state) ){{ $user->city }}, {{ $user->state }} @endif</td>
                            <td>
                                Access Level: {{ Form::select('access levels', $accessGroups, -1) }}<br>
                                <div class="btn btn-primary" data-userid="{{ $user->id }}" href="#"><span class="glyphicon glyphicon-hand-right"></span> Grant Access</div>
                            </td>
                        </tr>
                        <tr>
                            <td><a class="center" href="{{ URL::to('dashboard/accounts/refresh/'.$user->id) }}">Refresh Permissions</a><br><a href="{{ URL::route('ghost.user', $user->id) }}">Ghost User</a></td>
                            <td><a href="https://app.hubspot.com/contacts/37772/contact/{{$user->vid}}/overview/" target="_blank">Hubspot</a><br>[vid: {{ $user->vid }}] <a href="https://na2.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=005&sen=00P&sen=006&sen=00Q&sen=a1J&sen=001&sen=015&sen=00T&sen=003&sen=00U&sen=a0s&sen=00O&sen=701&str={{$user->email}}" target="_blank">SalesForce</a> </td>
                            <td><a href="{{URL::to('/dashboard/approve/newUsers/'.$user->email)}}" target="_blank">Assign Group Permissions</a></td>
                            <td colspan = 4>{{ Group::getGroupNamesForUser($user->groups) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>

@endif
        
<script>

$(document).ready(function() {
    $('table').on('click', '.btn-primary', function() {
        id = $(this).data('userid');
        window.location.href = "{{ URL::to('dashboard/approve/newUsers') }}/"+id+"/"+$(this).parent().find('select').val();
    });
    
});

</script>

@stop