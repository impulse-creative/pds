@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">
    
    <div class="col-sm-12 main dash-badges">
        <div id="badge-manager-header">
            <h1>Create Badge</h1>
            <hr>
            <div class="response_errors">
                
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                
                @if($errors->all())
                    @foreach($errors->all() as $key => $error)
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $error }}</div>
                    @endforeach
                @endif
            </div>
    	</div>
        <div class="badge-create">
            {{ Form::open(array('url' => 'badges', 'files' => true)) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('image', 'Badge Image') }}
                    {{ Form::file('badge_image') }} 
                </div>
                <div class="form-group">
                    {{ Form::label('achievements-needed', 'Achievements Needed') }}
                    <div class="list-achievements">
                        <ul>
                            @foreach($achievements as $achievement)
                                <li class="label label-default" data-id="{{ $achievement->id }}">{{ $achievement->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                    {{ Form::text('achievements', Input::old('achievements'), array('class' => 'form-control hidden', 'id' => 'achievement-container')) }}
                </div>
            
                {{ Form::submit('Create the Badge!', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
        
        
        
        
        
    </div>
  </div>

     
<!-- Small modal -->
<button id="startModal" type="button" class="btn btn-primary hidden" data-toggle="modal" data-target=".achievement-amount">Small modal</button>

<div class="modal fade achievement-amount" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Total Achievements</h4>
            </div>
            <div class="modal-body">
                
                    <label class="small-padding">
                        How many times must the user accomplish this <span id="achievement-amount-label">achievement</span>
                    </label>
                    {{ Form::text('achievement-id', Input::old('achievement-id'), array('class' => 'form-control hidden', 'id' => 'achievement-id')) }}
                    {{ Form::text('achievement-total', Input::old('achievement-total'), array('class' => 'form-control', 'id' => 'achievement-total')) }}
                
            </div>
            <div class="modal-footer">
                <button id="achievement-action" type="button" class="btn btn-primary" data-dismiss="modal">Continue</button>
            </div>
            
        </div>
    </div>
</div>   
        
        
     
@endif
{{ HTML::script('js/gamify-admin.js') }}

@stop