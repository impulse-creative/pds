@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">
   
    <div class="col-sm-12 main dash-badges">
        <div id="badge-manager-header">
            <h1>Manage Badges</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="badge-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('badges/create') }}">Add Badge</a>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Description</td>
                            <td>Image</td>
                            <td>Achievements Needed</td>
                            <td>Active</td>
                            <td>Edit</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($badges as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->description }}</td>
                            <td><img src="{{ URL::to('storage/files/badge_images/'.$value->url) }}" /></td>
                            <td>
                                @foreach($value->achievements as $key => $achievement)
                                <span class="btn btn-primary">
                                    {{ $achievement->name }} <span class="badge">{{ $achievement->times }}</span>
                                </span>

                                @endforeach

                            </td>
                            <td>{{ $value->active }}</td>
                            <td><a style="display:none;" href="#{{--URL::to('badges/'.$value->id.'/edit')--}}"><span class="glyphicon glyphicon-pencil"></span> edit</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>

        
@endif        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop