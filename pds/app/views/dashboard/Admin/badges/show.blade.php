@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(5, $permissions))

  <div class="container">
   
    <div class="col-sm-12 main dash-badges">
        <div id="badge-manager-header">
            <h1>Badge: {{ $badge->name }}</h1>
            
    	</div>
        <div class="badge-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('badges/create') }}">Add Badge</a>
                <a class="btn btn-small btn-info" href="{{ URL::to('badges/'.$badge->id.'/create') }}">Edit Badge</a>
            </div>
            
            <div class="jumbotron text-center">
                <h2>{{ $badge->name }}</h2>
                <p>
                    <strong>Description:</strong> {{ $badge->description }}<br>
                    <strong>Achievements Needed:</strong><br>
                    @foreach($achievements as $achievement)
                    <span class="btn btm-primary">{{ $achievement->name }}<span class="badge">{{ $achievement->total }}</span>
                    @endforeach
                    <img src="{{ $badge->url }}" />
                </p>
            </div>
        </div>
    </div>
  </div>

        
@endif        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop