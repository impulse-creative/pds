@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(8, $permissions))

  <div class="container">
    
    <div class="col-sm-12 main dash-marketplace-listings">
        <div id="marketplace-listing-manager-header">
            <h1>Create Marketplace Listing</h1>
            <hr>
            <div class="response_errors">
                
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                
                @if($errors->all())
                    @foreach($errors->all() as $key => $error)
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $error }}</div>
                    @endforeach
                @endif
            </div>
    	</div>
        <div class="marketplace-create">
            {{ Form::open(array('url' => 'pdsmarketplace')) }}

                <div class="form-group">
                    {{ Form::label('logo', 'Logo') }}
                    {{ Form::text('logo_src', Input::old('logo_src'), array('class' => 'form-control', 'placeholder' => 'Provide a link to the company logo...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('name', 'Vendor') }}
                    {{ Form::text('vendor', Input::old('vendor'), array('class' => 'form-control', 'placeholder' => 'Vendor Company Name...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('category', 'Category') }}
                    {{ Form::text('category', Input::old('category'), array('class' => 'form-control', 'placeholder' => 'Vendor Category...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('subcategory', 'SubCategory') }}
                    {{ Form::text('subcategory', Input::old('subcategory'), array('class' => 'form-control', 'placeholder' => 'Vendor SubCategory...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('real_email', 'Email') }}
                    {{ Form::text('real_email', Input::old('real_email'), array('class' => 'form-control', 'placeholder' => 'Vendor Email...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('site_url', 'Site Url') }}
                    {{ Form::text('site_url', Input::old('site_url'), array('class' => 'form-control', 'placeholder' => 'Vendor website URL (must begin with http or https)...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('contact_href', 'Contact Href') }}
                    {{ Form::text('contact_href', Input::old('contact_href'), array('class' => 'form-control', 'placeholder' => 'Vendor Contact Email...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('contact_info', 'Contact Info') }}
                    {{ Form::text('contact_info', Input::old('contact_info'), array('class' => 'form-control', 'placeholder' => 'Vendor Contact Information...')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('years', 'Years') }}
                    {{ Form::text('years', Input::old('years'), array('class' => 'form-control', 'placeholder' => 'Years Vendor Attended Conference...')) }}
                </div>
            
            
                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
                </div>

                {{ Form::submit('Create the Marketplace Listing!', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
        
        
        
        
        
    </div>
  </div>

        
@endif       
@stop