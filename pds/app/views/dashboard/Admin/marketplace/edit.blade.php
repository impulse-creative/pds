@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(8, $permissions))

  <div class="container">
   
    <div class="col-sm-12 main dash-marketplace-listings">
        <div id="marketplace-listing-manager-header">
            <h1>Edit Marketplace Listing: {{ $marketplace->name }}</h1>
            <hr>
            <div class="response_errors">
                
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                
                @if($errors->all())
                    @foreach($errors->all() as $key => $error)
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $error }}</div>
                    @endforeach
                @endif
            </div>
    	</div>
        
        <div class="marketplace-edit">
            {{ Form::model($marketplace, array('route' => array('pdsmarketplace.update', $marketplace->id), 'method' => 'PUT')) }}
            
                <div class="form-group">
                    {{ Form::label('logo', 'Logo') }}
                    {{ Form::text('logo_src', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('name', 'Vendor') }}
                    {{ Form::text('vendor', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('category', 'Category') }}
                    {{ Form::text('category', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('subcategory', 'SubCategory') }}
                    {{ Form::text('subcategory', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('real_email', 'Email') }}
                    {{ Form::text('real_email', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('site_url', 'Site Url') }}
                    {{ Form::text('site_url', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('contact_href', 'Contact Href') }}
                    {{ Form::text('contact_href', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('contact_info', 'Contact Info') }}
                    {{ Form::text('contact_info', null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('years', 'Years') }}
                    {{ Form::text('years', null, array('class' => 'form-control')) }}
                </div>
                
                <div id="wysihtml5-toolbar" style="display:none;">
                    <div class="btn-group">
                        <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
                        <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
                    </div>
                    <div class="btn-group">
                        <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
                            <span class="glyphicon glyphicon-list"></span>
                        </a>
                        <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
                            <span class="glyphicon glyphicon-sort-by-attributes"></span>
                        </a>
                        <a data-wysihtml5-command="createLink" class="btn btn-default">
                            <span class="glyphicon glyphicon-link"></span>
                        </a>
                    </div><br/>
                    <div data-wysihtml5-dialog="createLink"  class="create-link-container">
                        <label>
                            Link:
                            <input data-wysihtml5-dialog-field="href" value="http://" class="text">
                        </label>
                        <a data-wysihtml5-dialog-action="save">OK</a> <a data-wysihtml5-dialog-action="cancel">Cancel</a>
                    </div>
                </div>
                <textarea class="inp-field description-text" id="editor" name="description" cols="10" rows="5" placeholder="Please insert your company's 250 word descriptions which will appear in your PDS Marketplace Listing">
                            @if(isset($marketplace->description))
                                    {{ $marketplace->description }}
                            @endif
                </textarea>
                
                <input class="inp-field description-html" id="hidden-html" name="html_edits" type="hidden" value="">
            
                <a class="btn btn-small btn-cancel" href="{{ URL::to('pdsmarketplace') }}">Cancel</a>
                {{ Form::submit('Save the Marketplace Listing!', array('class' => 'btn btn-primary')) }}
                

            {{ Form::close() }}
        </div>
        
        
        
        
    </div>
  </div>

        
@endif   
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop