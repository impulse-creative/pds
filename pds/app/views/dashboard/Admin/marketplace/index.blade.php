@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(8, $permissions))

  <div class="container marketplace-admin-container">

    <div class="col-sm-12 main dash-marketplace-listings">
        <div id="marketplace-listing-manager-header">
            <h1>Manage Marketplace Listings</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="achievement-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('pdsmarketplace/create') }}">Add Marketplace Listing</a>
            </div>
            <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Logo</td>
                        <td>Company</td>
                        <td>Category</td>
                        <td>SubCategory</td>
                        <td>Email</td>
                    </tr><tr>
                        <td>URL</td>
                        <td>Contact</td>
                        <td colspan="3">Listing</td>
                        <td>Edit</td>
                    </tr>
                </thead>
                <tbody>
                    
                @foreach($marketplace as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td><img style="max-width: 100px" src="{{ $image = strpos($value->logo_src, 'http://localhost:8000') > -1 ? str_replace('http://localhost:8000', 'http://app.pharmacyowners.com', $value->logo_src) : $value->logo_src }}" /></td>
                        <td>{{ $value->vendor }}</td>
                        <td>{{ $value->category }}</td>
                        <td>{{ $value->subcategory }}</td>
                        <td>{{ $value->real_email }}</td>
                    </tr><tr>
                        <td>{{ $value->site_url }}</td>
                        <td>{{ $value->contact_href }}</td>
                        <td colspan="3">{{ $value->description }}</td>
                        <td><a href="{{ URL::to('pdsmarketplace/'.$value->id.'/edit') }}"><span class="glyphicon glyphicon-pencil"></span> edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>

@endif
        
        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop