@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(8, $permissions))

  <div class="container marketplace-admin-container">

    <div class="col-sm-12 main dash-marketplace-listings">
        <div id="marketplace-listing-manager-header">
            <h1>Manage Marketplace Listings</h1>
            <hr>
            <div class="response_errors">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                @endif
            </div>
    	</div>
        <div class="vendor-display">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>Company</td>
                        <td>Logo</td>
                        <td>Website</td>
                        <td>Address 1</td>
                        <td>Address 2</td>
                        
                    </tr><tr>
                        <td>Category</td>
                        <td>Sub Category</td>
                        <td colspan="2">Description</td>
                        <td>Blog/Vlog</td>
                    </tr>
                </thead>
                <tbody>
                    
                @foreach($vendors as $vendor)
                    <tr>
                        <td>{{ $vendor->company_name }}</td>
                        <td><a href="{{ URL::to('storage/files/company_logos/'.$vendor->company_logo) }}" target="_blank"><img src="{{ URL::to('storage/files/company_logos/'.$vendor->company_logo) }}" style="width:150px" /></a></td>
                        
                        <td>{{ $vendor->company_website }}</td>
                        <td>{{ $vendor->address }}</td>
                        <td>{{ $vendor->city }}, {{ $vendor->state_or_region }} {{ $vendor->postal_code }}</td>
                    </tr><tr>
                        <td>{{ $vendor->category}}</td>
                        <td>{{ $vendor->subCategory }}</td>
                        <td colspan="2">{{ $vendor->description }}</td>
                        <td>{{ $vendor->company_blog_or_vlog }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>

@endif
        
        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop