@extends('layouts.pds-advantage')

@section("content")
<?php $permissions = explode(':', $sections['permissions']); ?>
@if(in_array(8, $permissions))

  <div class="container">
    
    <div class="col-sm-12 main dash-marketplace-listings">
        <div id="marketplace-listing-manager-header">
            <h1>Marketplace Listing: {{ $marketplace->vendor }}</h1>
            
    	</div>
        <div class="achievement-display">
            <div class="small-padding">
                <a class="btn btn-small btn-info" href="{{ URL::to('pdsmarketplace/create') }}">Add Marketplace Listing</a>
                <a class="btn btn-small btn-info" href="{{ URL::to('pdsmarketplace/'.$marketplace->id.'/edit') }}">Edit Marketplace Listing</a>
            </div>
            
            <div class="jumbotron text-center">
                <h2>#{{ $marketplace->id }} - {{ $marketplace->vendor }}</h2>
                <p>Email: {{ $marketplace->real_email }}</p>
                <p>{{ $marketplace->site_url }} - {{ $marketplace->contact_href }}</p>
                <p>{{ $marketplace->phone }}</p>
                <p><img src="{{ $image = strpos($marketplace->logo_src, 'http://localhost:8000') > -1 ? str_replace('http://localhost:8000', 'http://app.pharmacyowners.com', $marketplace->logo_src) : $marketplace->logo_src }}" /></p>   
                <p>Category: {{ $marketplace->category }}</p>
                <p>Subcategory: {{ $marketplace->subcategory }}</p>
                <p>
                    <strong>Description:</strong> {{ $marketplace->description }}<br>
                </p>
            </div>
        </div>
        
        
        
        
        
    </div>
  </div>

        
@endif        
        
        
     

{{-- HTML::script('js/notifications.js') --}}

@stop