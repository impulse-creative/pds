@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/accounts-styles.css') }}
@stop

<?php

function checkVendor($groups) {
    if(strpos($groups, '3') > -1) {
        return true;
    }
    return false;
}

function checkBadge($userBadges, $badgeId) {
    if($userBadges != '' | $userBadges != null) {
        $badges = json_decode($userBadges);
        if(property_exists($badges, $badgeId)) {
            return $badges->{1}->url;
        }
        return false;
    }
    return false;
    //$completed = Badge::where()
}



function getForumInfo($id) {
    $topics = ForumTopics::where('author_id', $id)->count();
    $messages = ForumTopics::where('author_id', $id)->count();
    return "$topics / $messages";
}

function checkProgress($user) {
    $progress = 0;
    $listing_completed = Vendor::where('user_id', '=', $user->id)->first();
    $market_profile_completed = GuideEntries::where('user_id', '=', $user->id)->first();
    $guide_ad_completed = GuideAds::where('user_id', '=', $user->id)->first();
    $raffle_item_completed = RaffleItems::where('user_id', '=', $user->id)->first();
    $vip_tickets_complete = VipTicketRecipient::where('user_id', '=', $user->id)->first();
    $sponsorships_complete = Sponsorships::where('user_id', '=', $user->id)->first();
    if (is_object($listing_completed)) $progress += 16.7;
    if (is_object($market_profile_completed)) $progress += 16.7;
    if (is_object($guide_ad_completed)) $progress += 16.7;
    if (is_object($raffle_item_completed)) $progress += 16.7;
    if (is_object($vip_tickets_complete)) $progress += 16.7;
    if (is_object($sponsorships_complete)) $progress += 16.7;
    return $progress;
}

?>


@section("content")

<div id="account-list-container" class="forum-bg">
    <div class="col-md-12 main dash-main">
        <div id="account-list-header">
            <h1 class="inline-element">Accounts</h1>
            <hr>
            <div>
                <span class="btn btn-primary small-margin pull-right"><span class="glyphicon glyphicon-heart-empty"></span>&nbsp;{{ $stats['twentyFourHoursActive'] }} Active Users in Past 24 Hours</span> <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>
                <span class="btn btn-primary small-margin pull-right"><span class="glyphicon glyphicon-hand-up"></span>&nbsp;{{ $stats['thirtyDaysActive'] }} active users In the past 30 days</span> <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>
                <span class="btn btn-primary small-margin pull-right"><span class="glyphicon glyphicon-user"></span>&nbsp;{{ $stats['sixMonthsActive'] }} active users in the past 6 months</span> <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>
                <span class="btn btn-primary small-margin pull-right"><span class="glyphicon glyphicon-heart-empty"></span>&nbsp;{{ $stats['totalActive'] }} total Activated Accounts</span>
                
                
                <a href="{{ URL::route('accounts.index') }}?downloadVendorProgressReport=1" class="btn btn-primary small-margin pull-right hidden"><span class="glyphicon glyphicon-export"></span>&nbsp;Export Vendor Progress Report</a>
                <span class="clear-fix"></span>
            </div>
            @if(Session::has('Success'))
                <div class="alert alert-success">{{ Session::pull('Success') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger">{{ Session::pull('Failure') }}</div>
            @endif
            @if(Session::has('No records'))
                    <div class="alert alert-danger">{{Session::pull('No records')}}</div>
            @endif
        </div>
        <div class="center">
            {{ $accounts->links()."<br/>" }}    
        </div>
        
            <div class="table-search-container purple-transparent-background">
                <div class="table-search fix-block">
                    {{ Form::open(array('route' => 'accounts.search.users', 'id' => 'search-form')) }}
                            {{ Form::input('text', 'search-param', $value = $searchParam != '' ? $searchParam : Input::old('search-param'), array('class' => 'search-field pull-left inline-element', 'placeholder' => 'Search accounts by name or email...')) }}
                            <a onclick="document.getElementById('search-form').submit();" class="btn btn-primary search-submit-btn inline-element small-margin" type="submit"><span class="glyphicon glyphicon-search"></span>&nbsp;Search</a>
                            <a href="{{ URL::route('accounts.index') }}" class="reset-search btn btn-primary"><span class="glyphicon glyphicon-repeat"></span>&nbsp;Reset Search</a>
                            <a href="{{ URL::to('dashboard/accounts/search/users/confirmations-check') }}" class="btn btn-primary"><span class="glyphicon glyphicon-check"></span>&nbsp;Confirmation Checks</a>
                            <a href="{{ URL::to('dashboard/accounts/search/users/most-recent') }}" class="btn btn-primary"><span class="glyphicon glyphicon-headphones"></span>&nbsp;By Most Recent Activity</a>
                            <a href="{{ URL::to('dashboard/accounts/search/users/last-ten-minutes') }}" class="btn btn-primary"><span class="glyphicon glyphicon-qrcode"></span>&nbsp;Active in Last 15 minutes</a>
                            <a href="{{ URL::to('dashboard/accounts/search/users/non-confirmed-active') }}" class="btn btn-primary"><span class="glyphicon glyphicon-qrcode"></span>&nbsp;Non-Confirmed with activity in last (1)three days</a>
                    {{ Form::close() }}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><p class="center">Name</p></th>
                            <th><p class="center">Company</p></th>
                            <th><p class="center">Email</p></th>
                            <th><p class="center">Joined</p></th>
                            <th><p class="center">Last Action</p></th>
                            <th><p class="center">Confirmed</p></th>
                            <th><p class="center">Topics / Replies</p></th>
                            <th><p class="center">Remove Account</p></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($accounts as $account)
                            <tr>
                                <td>{{ $image = User::getBioImage($account->id, '') }}</td>
                                <td><a class="list-name" href="{{ URL::to('user/') }}/{{ $account->id }}/{{ $account->fname }}-{{ $account->lname }}">{{ $account->fname }}&nbsp;{{ $account->lname }}</a></td>
                                <td class="orange-text">{{ $account->pharmacy }}</td>
                                <td>{{ $email = isset($account->email) ? $account->email : 'N/A' }}</td>
                                <td>{{ date("F d, Y", strtotime($account->created_at)) }} at {{ date("g:i a",strtotime($account->created_at)) }}</td>
                                <td>{{ date("F d, Y", strtotime($account->updated_at)) }} at {{ date("g:i a",strtotime($account->updated_at)) }}</td>
                                <td><p class="center">{{ $confirmed = $account->confirmed_user == 0 ?  '<i class="fa red-text fa-circle"></i>' : '<i class="fa green-text fa-check-circle-o"></i>'  }}</p></td>
                                <td><p class="center">{{ getForumInfo($account->id) }}</p></td>
                                <td>
                                    <p class="center">
                                        <a href="{{ URL::to('dashboard/accounts/destroy/account').'/'.$account->id }}" class="confirm" data-confirm="Are you sure you want to remove this user account?">
                                            <p class="red-text"><span class="glyphicon glyphicon-remove"></span></p></a>
                                    </p>
                                        
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <a href="https://app.hubspot.com/contacts/37772/contact/{{$account->vid}}/overview/" target="_blank">Hubspot</a><br>[vid: {{ $account->vid }}]
                                </td>
                                <td>
                                    <a href="https://na2.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=005&sen=00P&sen=006&sen=00Q&sen=a1J&sen=001&sen=015&sen=00T&sen=003&sen=00U&sen=a0s&sen=00O&sen=701&str={{$account->email}}" target="_blank">SalesForce</a>
                                </td>
                                <td>
                                    <a href="{{URL::to('/dashboard/approve/newUsers/'.$account->email)}}" target="_blank">Assign Group Permissions</a>
                                </td>
                                <td>
                                    <a href="{{URL::to('dashboard/accounts/confirm/user/'.$account->id)}}">Confirm User</a>
                                </td>
                                <td>
                                    {{$badge = checkBadge($account->badges, 1) ? '<img class="badge-icon" src="'.URL::to('storage/files/badge_images/'.checkBadge($account->badges, 1)).'" />' : '<a href="'.URL::to('grantlast/achievement/'.$account->id).'"><i class="fa fa-trophy"><br>Give Feedback Achievement</i></p></a>'}}
                                </td>
                                <td>
                                    @if(checkVendor($account->groups))
                                        <p class="center @if(checkProgress($account) == 0) red-text @else green-text @endif">
                                            <span class="glyphicon glyphicon-stats"></span>
                                            &nbsp;{{ checkProgress($account) }}%</p>
                                    @else
                                    <p class="center">- - -</p>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ URL::route('ghost.user', $account->id) }}">Ghost User</a>
                                </td>
                                <td>
                                    hs_acc: {{ $account->hs_acc_number }}<br>
                                    <a href="{{URL::to('dashboard/hscompany/'.$account->id)}}" target="_blank">Get HS Company ID</a>
                                </td>
                                <td>
                                    sf_id: {{ $account->sf_id }}
                                </td>
                                <td>
                                    {{ $imported = $account->imported = 1 ? 'im so imported' : 'im not imported please' }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <p class="center smaller">
                                        {{ Group::getGroupNamesForUser($account->groups) }}
                                    </p>
                                    <a class="center smaller" href="{{ URL::to('dashboard/accounts/refresh/'.$account->id) }}">Refresh Permissions</a>
                                    <a class="center smaller" href="{{ URL::route('manually.update.user.permissions', ['id' => $account->id]) }}">Update Permissions</a>
                                </td>
                                <td colspan="5">
                                    <p class="cener smaller">
                                        <?php $achievements = UserAchievement::getAcheivementsForDash($account->id); ?>
                                        @if($achievements != null)
                                            @foreach($achievements as $id=>$times)
                                            [ {{ Achievement::getAchievementName($id) }} : {{$times}} ] 
                                            @endforeach
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
              </table>
            </div>
        <div class="center">
            {{ $accounts->links() }}
        </div>
        
    </div>
    <div class="clearfix"></div>
	
</div>
@stop

@section('extra-js')
	@parent
	<script style="display:none;">
		$('.search-field').focus();
	</script>
@stop