@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/accounts-styles.css') }}
@stop

@section("content")
<div id="group-list-container">
	<div id="group-list-header">
		@if($user_data)
			<h1>Account Summary: {{ $user_data->fname }}&nbsp;{{ $user_data->lname }}</h1>
			<span>
				<i>Account created on {{ date("F d, Y", strtotime($user_data->created_at)) }} at {{ date("g:ha",strtotime($user_data->created_at)) }}</i>
				
			</span>
			<hr>
		@endif
		@if(Session::has('Success'))
			<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ Session::pull('Success') }}</div>
		@endif
		@if(Session::has('Failure'))
			<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ Session::pull('Failure') }}</div>
		@endif
	</div>
	<div class="back-to-account-list"><a class="purple-text" href="{{ URL::route('accounts.index') }}"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Return to Account List</a></div>

	{{-- Display user account information --}}
	@if(is_object($summary_data['user_data']))
		<div class="col-lg-6 info-section">
			<h2>Account Information</h2>
			<div class="user-item">
				<label for="full-name"><strong class="purple-text">Full Name:</strong></label>
				<span id="full-name">{{ $summary_data['user_data']->fname }}&nbsp;{{ $summary_data['user_data']->lname }}</span>
			</div>
			<div class="user-item">
				<label for="email"><strong class="purple-text">Email:</strong></label>
				<span id="email">{{ $summary_data['user_data']->email }}</span>
			</div>
			<div class="user-item">
				<label for="pharmacy"><strong class="purple-text">Pharmacy:</strong></label>
				<span id="pharmacy">{{ $summary_data['user_data']->pharmacy }}</span>
			</div>
			<div class="user-item">
				<label for="permissions"><strong class="purple-text">Permissions:</strong></label>
				<span id="permissions">
					<ul>
						@foreach($groups as $key => $group)
							<li>{{ $group }}</li>
						@endforeach
					</ul>
				</span>
			</div>
			@if($summary_data['user_data']->confirmed_user == 0)
				<div class="user-item">
					<a href="{{ URL::to('dashboard/accounts/confirm/user') }}/{{$summary_data['user_data']->id}}" class="btn btn-default">Confirm User Account</a>
				</div>
			@endif
			{{-- Not until we get let into the pds companies in HS
			<div class="user-item">
				<label for="hs-acc-id"><strong class="purple-text">Hubspot Company ID:</strong></label>
				<span id="hs-acc-id">{{ $summary_data['user_data']->hs_acc_number }}</span>
			</div>
			--}}
		</div>
	@else
		<?php $missing['no-acc'] = 'No account information found' ?>
	@endif


	{{-- Display user vendor information (if it exists) --}}
	@if(is_object($vendor_data))
		<div class="col-lg-6 info-section">
                    <h2>Marketplace Listing</h2>
                                @if(is_object($vendor_data) || is_object($user_data))
                                        <?php 
                                                $groups_list = explode(':', $user_data->groups);
                                                if(in_array("3", $groups_list) && is_object($vendor_data)) echo '&nbsp;&nbsp;<a data-toggle="modal" data-target="#listingModal" class="btn btn-primary view-listing-btn">View Marketplace Listing</a>';
                                        ?>
                                @endif
                    
				@if($vendor_data->approved == 0)
					<span><a href="{{ URL::to('dashboard/accounts/approve/user') }}/{{$vendor_data->id}}?undo=0" class="btn btn-default"><span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Approve Listing</a></span>
				@else
					<span><a href="{{ URL::to('dashboard/accounts/approve/user') }}/{{$vendor_data->id}}?undo=1" class="btn btn-default"><span class="glyphicon glyphicon-ban-circle"></span>&nbsp;Undo Approve Listing</a></span>
				@endif
                                <br><br>
			<div class="user-item">
				<label for="using-prev-list"><strong class="purple-text">Using Previous Marketplace Listing:</strong></label>
				<span id="using-prev-list" class="orange-text">@if($user_data->prev_marketplace_approved == 1) Yes @else No @endif</span>
			</div>
			<div class="user-item">
				<label for="full-name"><strong class="purple-text">Company Contact:</strong></label>
				<span id="full-name">{{ $vendor_data->first_name }}&nbsp;{{ $vendor_data->last_name }}</span>
			</div>
			<div class="user-item">
				<label for="email"><strong class="purple-text">Contact Email:</strong></label>
				<span id="email">{{ $vendor_data->email }}</span>
			</div>
			<div class="user-item">
				<label for="pharmacy"><strong class="purple-text">Company Name:</strong></label>
				<span id="pharmacy">{{ $vendor_data->company_name }}</span>
			</div>
			<div class="user-item">
				<label for="acc-lvl"><strong class="purple-text">Phone:</strong></label>
				<span id="acc-lvl">{{ ucfirst($vendor_data->phone) }}</span>
			</div>	
			<div class="user-item">
				<label for="address"><strong class="purple-text">Address:</strong></label>
				<span id="address">{{ $address = $vendor_data->address != '' ? $vendor_data->address : '<span class="orange-text">No Address Provided.</span>' }}</span>
			</div>
                        <div class="user-item">
				<label for="city-state"><strong class="purple-text">City/State:</strong></label>
				<span id="city-state">{{ $vendor_data->city }},&nbsp;{{ $vendor_data->state_or_region }}</span>
			</div>
			<div class="user-item">
				<label for="postal"><strong class="purple-text">Postal Code:</strong></label>
				<span id="postal">{{ $postal = $vendor_data->postal_code != '' ? $vendor_data->postal_code : '<span class="orange-text">No Postal Code Provided.</span>' }}</span>
			</div>
			<div class="user-item">
				<label for="website"><strong class="purple-text">Website:</strong></label><br/>
				<span id="vendor-website">{{ $website = $vendor_data->company_website != '' ? $vendor_data->company_website  : '<span class="orange-text">No Website Provided.</span>' }}</span>
			</div>
                        <div class="user-item">
				<label for="category"><strong class="purple-text">Category:</strong></label><br/>
				<span id="vendor-category">{{ $website = $vendor_data->category != '' ? $category : '<span class="orange-text">No Category Provided.</span>' }}</span>
			</div>
                                
                        <div class="user-item">
				<label for="subcategory"><strong class="purple-text">Subcategory:</strong></label><br/>
				<span id="vendor-subcategory">{{ $category = $vendor_data->subCategory != '' ? $subCategory : '<span class="orange-text">No Subcategory Provided.</span>' }}</span>
			</div>
                        <div class="user-item">
				<label for="vlog"><strong class="purple-text">Additional URL:</strong></label><br/>
				<span id="vendor-additional-url">{{ $additionalUrl = $vendor_data->company_blog_or_vlog != '' ? $vendor_data->company_blog_or_vlog : '<span class="orange-text">No Additional URL Provided.</span>' }}</span>
			</div>
                        <div class="user-item">
				<label for="description"><strong class="purple-text">250 Word Listing:</strong></label><br/>
				<span id="vendor-description">{{ $description = $vendor_data->description != '' ? $vendor_data->description  : '<span class="orange-text">No Description Provided.</span>' }}</span>
			</div>
                                
			@if(!is_null($vendor_data->company_logo))
                        <div class="user-item">
                            <a href="{{ URL::to('dashboard/accounts/summary/downloadMarketplaceLogo/'.$vendor_data->company_logo) }}" class="btn btn-default"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Download Marketplace Logo</a>
                            <img class="medium-thumbnail" src="{{ URL::to('/storage/files/company_logos/'.$vendor_data->company_logo) }}" />
                                
                                
                        </div>   
                        @else
                        <div class="user-item">
                            <label for="description"><strong class="purple-text">Company Logo:</strong></label>
                            <span class="orange-text"><i>No Logo submitted</i></span>
                        </div>
                        @endif
                                
		</div>
	@else
		<?php $missing['no-vendor'] = 'No vendor information found' ?>
	@endif

	{{-- Display user guide ad profile information (if it exists) --}}
	@if(is_object($guide_entry_data))
		<div class="col-lg-6 info-section">
			<h2>100 Word Exhibitor Listing</h2>
			<div class="user-item">
				<label for="full-name"><strong class="purple-text">Sales Contact:</strong></label>
				<span id="full-name">{{ $guide_entry_data->sales_first_name }}&nbsp;{{ $guide_entry_data->sales_last_name }}</span>
			</div>
			<div class="user-item">
				<label for="email"><strong class="purple-text">Email:</strong></label>
				<span id="email">{{ $guide_entry_data->sales_email }}</span>
			</div>
			<div class="user-item">
				<label for="pharmacy"><strong class="purple-text">Company Name:</strong></label>
				<span id="pharmacy">{{ $guide_entry_data->company_name }}</span>
			</div>
                        <div class="user-item">
				<label for="address"><strong class="purple-text">Address:</strong></label>
				<span id="address">{{ $address = $guide_entry_data->address != '' ? $guide_entry_data->address : '<span class="orange-text">No Address Provided.</span>' }}</span>
			</div>
			<div class="user-item">
				<label for="city-state"><strong class="purple-text">City/State:</strong></label>
				<span id="city-state">{{ $city = $guide_entry_data->city != '' ? $guide_entry_data->city : '<span class="orange-text">No City Provided.</span>' }}{{ $state = $guide_entry_data->state_or_region != '' ?  ', '.$guide_entry_data->state_or_region : ', <span class="orange-text">No State Provided.</span>' }}</span>
			</div>
			<div class="user-item">
				<label for="postal"><strong class="purple-text">Postal Code:</strong></label>
				<span id="postal">{{ $postal = $guide_entry_data->postal_code != '' ? $guide_entry_data->postal_code : '<span class="orange-text">No Postal Code Provided.</span>' }}</span>
			</div>
                        <div class="user-item">
				<label for="exhibitor-url"><strong class="purple-text">Website:</strong></label>
				<span id="exhibitor-url">{{ $website = $guide_entry_data->company_website != '' ? $guide_entry_data->company_website : '<span class="orange-text">No Postal Code Provided.</span>' }}</span>
			</div>
                        @if(!is_null($guide_entry_data->company_guide_entry_img))
                        <div class="user-item">
                            <a href="{{ URL::to('dashboard/accounts/summary/downloadGuideLogo/'.$guide_entry_data->company_guide_entry_img) }}" class="btn btn-default"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Download Exhibitor Guide Logo</a>
                            <img class="medium-thumbnail" src="{{ URL::to('/storage/files/guide_logos/'.$guide_entry_data->company_guide_entry_img) }}" />
                                
                                
                        </div>   
                        @else
                        <div class="user-item">
                            <label for="logo"><strong class="purple-text">Logo:</strong></label>
                            <span class="orange-text"><i>No Logo submitted</i></span>
                        </div>
                        @endif
                        <div class="user-item">
                            <label for="hundred-word"><strong class="purple-text">100 Word Listing:</strong></label><br>
                            <span id="hundred-word">{{ $hundredWord = $guide_entry_data->description != '' ? $guide_entry_data->description : '<span class="orange-text"><i>No Listing submitted</i></span>' }}</span> 
                        </div>
			
		</div>
	@else
		<?php $missing['no-guide'] = 'No guide profile information found' ?>
	@endif


	{{-- Display user guide ad entry information (if it exists) --}}
	@if(is_object($guide_ad_data))
            	<div class="col-lg-6 info-section">
			<h2>Conference Guide Advertisement</h2>
                        <div class="user-item">
				<label for="full-name"><strong class="purple-text">Guide Ads Chosen:</strong></label>
				<ul>
					@foreach($guide_ad_type as $ad_type)
						<li class="ad-item">{{ $ad_type->type }}</li>
					@endforeach
				</ul>
			</div>
			<div class="user-item">
				<label for="email"><strong class="purple-text">Billing Choice:</strong></label>
				<ul>
					<?php
						switch($guide_ad_billing_choice->type) {
							case 'send-invoice':
								$billing_choice = 'Send vendor an invoice for the ad(s)';
								break;
							case 'on-file':
								$billing_choice = 'Bill the card on the vendor\'s file.';
								break;
							case 'paid-in-package':
								$billing_choice = 'Vendor has paid for the ad in the package they purchased.';
								break;
							default:
								$billing_choice = 'No billing option chosen';
						}
					?>
					<li class="bill-item">{{ $billing_choice }}</li>
				</ul>
			</div>
                        @if(!is_null($guide_ad_data->company_ad))
                        <div class="user-item">
                                <a href="{{ URL::to('dashboard/accounts/summary/downloadGuideAd/'.$guide_ad_data->company_ad) }}" class="btn btn-default"><span class="glyphicon glyphicon-cloud-download"></span>&nbsp;Download Conference Guide Ad</a>
                        </div>
                        @endif
		</div>
		<div class="clearfix"></div>
	@else
		<?php $missing['no-guide-ad'] = 'No guide ad information found' ?>
	@endif

	{{-- Display user vip ticket recipient information (if it exists) --}}
	@if(is_object($vip_recipients) && count($vip_recipients) > 0)
		<div class="clearfix"></div>
		<div class="col-lg-6 info-section">
			<h2>Free Conference Tickets</h2>
			@foreach($vip_recipients as $recipient_data)
				<?php if ($recipient_data->vip_fname == '' && $recipient_data->vip_lname == '' && $recipient_data->vip_email == '' && $recipient_data->vip_company_name == '') { continue; } ?>
				<div class="user-item recipient-container gray-background small-margin small-padding small-waist">
					<label for="recipient-name"><strong class="purple-text">Recipient Name:</strong></label>
					@if($recipient_data->vip_fname == '' && $recipient_data->vip_lname == '') 
						<span id="recipient-name orange-text">No name provided.</span><br/>
					@else
						<span id="recipient-name">{{ $fname = isset($recipient_data->vip_fname) ? $recipient_data->vip_fname : '' }}&nbsp;{{ $lname = isset($recipient_data->vip_lname) ? $recipient_data->vip_lname : '' }}</span><br/>
					@endif
                                        <label for="recipient-company"><strong class="purple-text">Company Name:</strong></label>
                                        @if($recipient_data->vip_company_name == '') 
                                        <span id="recipient-company orange-text">No Company provided. Contact the vendor for information at: <a href="tel:{{ $vendor_data->phone }}">{{ $vendor_data->phone }}</a>. </span><br>
					@else
                                        <span id="recipient-company">{{ $recipient_data->vip_company_name }}</span><br>
					@endif
					<label for="recipient-email"><strong class="purple-text">Recipient Email:</strong></label>
					@if($recipient_data->vip_email == '') 
						<span id="recipient-email orange-text">No email provided. Contact the vendor for information at: <a href="tel:{{ $vendor_data->phone }}">{{ $vendor_data->phone }}</a>. </span>
					@else
						<span id="recipient-email"><a href="mailto:{{ $recipient_data->vip_email }}">{{ $email = $recipient_data->vip_email }}</a></span>
					@endif
					<label for="recipient-phone"><strong class="purple-text">Recipient Phone:</strong></label>
					@if($recipient_data->vip_phone == '')
						<span id="recipient-email orange-text">No phone number provided.</span>
					@else
						<a href="tel:{{ $recipient_data->vip_phone }}"><span id="recipient-phone">{{ $recipient_data->vip_phone }}</span></a>
					@endif
				</div>
			@endforeach
		</div>
	@else
		<?php $missing['no-vip-ticket'] = 'No free ticket information found' ?>
	@endif
        
        {{-- Display user guide ad profile information (if it exists) --}}
        @if(is_object($giveaway_item))
		<div class="col-lg-6 info-section">
			<h2>Raffle Item</h2>
			<div class="user-item">
				<label for="giveaway-item"><strong class="purple-text">Giveaway Item:</strong></label>
				<span id="giveaway-item">{{ $giveaway = $giveaway_item->item_name != '' ? $giveaway_item->item_name : '<span class="orange-text">No Item Named.</span>' }}</span>
			</div>
                </div>
        @else
            <?php $missing['no-giveaway-item'] = 'No Giveaway Item found' ?>
        @endif

	{{-- Display user vip ticket recipient information (if it exists) --}}
	@if(isset($sponsorship_choices) && count($sponsorship_choices) > 0)
		<div class="col-lg-6 info-section">
			<h2>Conference Sponsorship Items</h2>
			<label for="sponsor-choice"><strong class="purple-text">Sponshorships Chosen:</strong></label>
			<ul>
				@foreach($sponsorship_choices as $key => $val)
					<li class="sponsor-item">{{ $val->type }}&nbsp;( {{ $val->price }}&nbsp;USD )</li>
				@endforeach
			</ul>
		</div>
	@else
		<?php $missing['no-sponsorships'] = 'No sponsor choices information found' ?>
	@endif
	<div class="clearfix"></div>

	{{-- Let the user know which sections could not be displayed because of missing values or incorrect information --}}
	<?php $missing['nothing'] = ''; ?>
	<div class="missing-info small-margin small-padding">
		<ul>
			@if($missing)
				@foreach($missing as $key => $value)
					@if($key == 'nothing') 
						{{-- Nothing --}}
					@else
						<li class="missing-item orange-text">{{ $value }}</li>
					@endif
				@endforeach
			@endif
		</ul>
	</div>
</div>

@if(is_object($vendor_data))
	<div id="listingModal" class="modal fade">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h3 class="small-margin modal-title pull-left">{{ $vendor_data->company_name }}</h3>
	        	@if($vendor_data->featured == 0)
		        	<a href="{{ URL::route('update.listing.featured') }}?listing_id={{ $vendor_data->id }}" class="btn btn-success pull-right small-margin small-margin-waist">
		        		<span class="glyphicon glyphicon-star"></span>
		        		&nbsp;Make Featured Listing
		        	</a>
	        	@else
		        	<a href="{{ URL::route('update.listing.featured') }}?listing_id={{ $vendor_data->id }}&un_feature=1" class="btn btn-warning pull-right small-margin small-margin-waist">
		        		<span class="glyphicon glyphicon-star-empty"></span>
		        		&nbsp;Remove From Featured Listing
		        	</a>
	        	@endif
	        	<a type="button" class="btn btn-primary pull-right small-margin small-margin-waist" data-toggle="modal" data-target="#editingModal"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit Vendor Information</a>
	        <div class="clearfix"></div>
	      </div>
	      <div class="modal-body">
			<?php 
				$scrubbed_title = preg_replace('/[^a-z\d\s]+/i', '', $vendor_data->company_name);
				$title = preg_replace('/ /', '-', $scrubbed_title);
			?>
	        <p><iframe id="market-listing-view" name="myiframe" src="{{ URL::to('marketplace/company/new_vendor/listing') }}/{{ $vendor_data->id }}/{{ $title }}" style="width:100%; height:600px;"></iframe></p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#editingModal"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit Vendor Information</a>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div id="editingModal" class="modal fade">
	  <div class="modal-dialog modal-md">
	    <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h3 class="small-margin modal-title">Editing:&nbsp;{{ $vendor_data->company_name }}</h3>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body">
				{{ Form::model($vendor_data, ['route' => 'update.listing.info', 'class' => 'update-market-listing', 'files' => true]) }}
					{{ Form::hidden('vendor_id', $vendor_data->id) }}
					{{ Form::hidden('user_id', $vendor_data->user_id) }}
					<div class="inp-grp">
						{{ Form::label('company_name', 'Company:', ['class' => 'vendor-label']) }}
						{{ Form::input('text', 'company_name', null, ['class' => 'inp-field', 'placeholder' => 'Provide your company name.']) }}
					</div>
					<div class="inp-grp">
						{{ Form::label('first_name', 'Owner First Name:', ['class' => 'vendor-label']) }}
						{{ Form::input('text', 'first_name', null, ['class' => 'inp-field', 'placeholder' => 'First Name'])}}
					</div>
					<div class="inp-grp">
						{{ Form::label('last_name', 'Owner Last Name:', ['class' => 'vendor-label']) }}
						{{ Form::input('text', 'last_name', null, ['class' => 'inp-field', 'placeholder' => 'Last Name'])}}
					</div>
					<div class="inp-grp">
						{{ Form::label('phone', 'Phone Number:', ['class' => 'vendor-label']) }}
						{{ Form::input('text', 'phone', null, ['class' => 'inp-field', 'placeholder' => 'Phone Number']) }}
					</div>
					<div class="inp-grp">
						{{ Form::label('email', 'Company Email:', ['class' => 'vendor-label']) }}
						{{ Form::input('email', 'email', null, ['class' => 'inp-field', 'placeholder' => 'Provide an email.']) }}
					</div>
					<div class="inp-grp">
						{{ Form::label('company_website', 'Company Website:', ['class' => 'vendor-label']) }}
						<div class="input-group">
						  <span class="input-group-addon">http://</span>
						  <?php $str = $vendor_data->company_website;
								$minified_url = preg_replace('#^https?://#', '', $str); ?>
						  {{ Form::input('text', 'company_website', $minified_url, ['class' => 'form-control', 'placeholder' => 'Provide your company website url.']) }}
						</div>	
					</div>
					<div class="inp-grp">
						{{ Form::label('address', 'Company Address:', ['class' => 'vendor-label']) }}
						{{ Form::input('text', 'address', null, ['class' => 'inp-field', 'placeholder' => 'Provide your company address.']) }}
					</div>
					<div class="inp-grp">
						{{ Form::label('description', 'Company Description:', ['class' => 'vendor-label']) }}
				        <div id="wysihtml5-toolbar" style="display:none;">
				            <div class="btn-group">
				                <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
				                <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
				            </div>
				            <div class="btn-group">
				                <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
				                    <span class="glyphicon glyphicon-list"></span>
				                </a>
				                <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
				                    <span class="glyphicon glyphicon-sort-by-attributes"></span>
				                </a>
				            </div>
				        </div>
						<textarea class="inp-field vendor-description-text" id="editor" name="description" cols="10" rows="5" placeholder="Please insert your company's 250 word descriptions which will appear in your PDS Marketplace Listing">
							@if(isset($vendor_data->description))
								{{ $vendor_data->description }}
							@endif
						</textarea>
						<input class="inp-field description-html" id="hidden-html" name="html_edits" type="hidden" value="">
					</div>
					<div class="inp-grp">
						{{ Form::label('company_logo', 'Company Logo Link:', ['class' => 'vendor-label'])}}
						<div class="new-logo">
								{{ Form::file('company_logo') }} @if(isset($vendor_data->company_logo)) <a class="btn btn-warning small-margin update-cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</a> @endif
						</div>
						@if(isset($vendor_data->company_logo))
							<script> $('.new-logo').addClass('hidden'); </script>
							<div class="logo-preview col-sm-12 center">
								<p><i>*preview does not reflect how the image will actually look</i></p>
								<img src="{{ URL::to('/storage/files/company_logos') }}/{{ $vendor_data->company_logo }}" alt="{{ $vendor_data->company_name }}" class="logo-small-preview"/><br/>
								<a class="btn btn-primary change-marketplace-logo"><span class="glyphicon glyphicon-picture"></span>&nbsp;Change Logo</a>
							</div>
							<div class="clearfix"></div>
						@endif
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{{ Form::submit('Save Changes', ['class' => 'btn btn-primary']) }}
				{{ Form::close() }}
			</div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endif

@stop

@section('extra-js')
	@parent
	{{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
	{{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
	<script>
		$("#market-listing-view").prop( "readonly", true );
	</script>

@stop