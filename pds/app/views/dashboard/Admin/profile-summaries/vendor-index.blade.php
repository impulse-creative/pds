@extends('layouts.pds-advantage')

@section('head')
	@parent
	{{ HTML::style('css/accounts-styles.css') }}
@stop

@section("content")
<div id="account-list-container">
	<div id="account-list-header">
		<h1>Vendor Accounts</h1>
		<hr>
		@if(Session::has('No records'))
			<div class="alert alert-danger">{{Session::pull('No records')}}</div>
		@endif
	</div>
    <div class="center">
        {{ $accounts->links()."<br/>" }}
    </div>
	
	<div class="col-lg-12 vendor-accounts-listing">	
				{{ var_dump($accounts) }}	  	
		    	@if(is_object($accounts))
		    		<?php $row = 0; ?>
					@foreach($accounts as $account)
						<div class="vendor-item">
							<div class="vendor-img-container">
								<img src="" alt="{{ $account->vendor }}"/>
							</div>
							<div class="vendor-name">
								<p>{{ $account->vendor }}</p>
							</div>
						</div>
						<?php $row++; ?>
				    @endforeach
				    @if($row == 0)
				    	<tr>
							<td>No results.</td>
						</tr>
				    @endif
				@else
					<p>There are no accounts.</p>
				@endif
	</div>
	<div class="clearfix"></div>
        <div class="center">
            {{ $accounts->links() }}
        </div>
	
</div>
@stop

@section('extra-js')
	@parent
	<script style="display:none;">
		$('.search-field').focus();
	</script>
@stop