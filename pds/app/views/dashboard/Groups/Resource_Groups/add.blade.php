@extends('layouts.pds-advantage')

@section("content")
<div id="group-manager-container">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul id="dash-nav" class="nav nav-sidebar">
                <li><a href="{{ URL::route('dash.main') }}">Dashboard</a></li>
                @include('dashboard.group-dash-views.menu')
                <li><a href="{{ URL::route('password.remind') }}">Settings</a></li>
                <li><a href="{{ URL::route('dash.logout') }}">Logout</a></li>
            </ul>
        </div>  
        <div class="col-sm-9 main">
            <div id="group-manager-header">
                <h1>Add Resource Group</h1>
                <hr>
                <div class="response_errors">
                    @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
                    @endif
                    @if(Session::has('errors'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
                    @endif
                </div>
            </div>
            <div class="add-new-resource-container col-lg-6 col-md-6">
                {{ Form::open(['route' => 'insert.resource.group', 'class' => 'add_new_resource_group']) }}
                <label for="name">Resource Container Name:</label>
                <span class="muted-text">*please be mindful of the length of your description. This will be shown as the category choice in the Resources portion of the application</span>
                <div class="input-group small-padding">
                    {{ Form::input('text', 'name', Input::old('name'), ['class' => 'form-control', 'placeholder' => 'Add a name for the new container']) }}
                </div>
                <label for="description">Description:</label>
                <div class="control-group small-padding">
                    <div id="wysihtml5-toolbar" style="display:none;" class="small-padding">
                        <div class="btn-group">
                            <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
                            <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
                        </div>
                        <div class="btn-group">
                            <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
                                <span class="glyphicon glyphicon-list"></span>
                            </a>
                            <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
                                <span class="glyphicon glyphicon-sort-by-attributes"></span>
                            </a>
                        </div>
                    </div>
                    <textarea class="form-control vendor-description-text" id="editor" name="description" cols="10" rows="5" placeholder="Provide a description for your resource"></textarea>
                    <input class="inp-field data-html" id="hidden-html" name="html_edits" type="hidden" value="">
                </div>
                <div class="input-group">
                    <label for="access">Minimum Group Access Level</label>
                    {{ Form::select('access', $access, 0, ['class' => 'form-control']) }}
                </div>
                <div class="form-submit small-padding">
                    {{ Form::submit('Add Container', ['class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
        </div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/dash-groups.js') }}
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    <script>
        /*-- forum update reply: store html message before submit --*/
        $('.add_new_resource_group').submit(function (e)
        {
            var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
            $('#hidden-html').val(html);
        });
    </script>
@stop