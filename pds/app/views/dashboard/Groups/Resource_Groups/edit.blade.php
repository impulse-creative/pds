@extends('layouts.pds-advantage')

@section('content')
<div class="group-add-form-container">
    <div class="group-add-form-container-title">
        <h1>Edit Resource Group</h1>
        <hr/>
    </div>
    <div class="col-md-4 medium-padding">
        {{ Form::open([
		"route" => 'acl.resource.group.edit',
		"autocomplete" => "off",
		"method" => "post"
	]) }}

        {{ Form::hidden('id', $group->id) }}
        {{ Form::field([
			"name" => "name",
			"label" => "Enter in the edited version of the group",
			"form" => $form,
			"placeholder" => "group name",
			"value" => $group->group_name
		]) }}

        {{ Form::field([
			"name" => "description",
			"label" => "Enter in a description for the resource group",
			"form" => $form,
			"placeholder" => "group name",
			"value" => $group->description
		]) }}

        <div class="input-group">
            <label for="access">Group Access Level</label>
            {{ Form::select('access', $access, $group->permitted_groups, ['class' => 'form-control']) }}
        </div>
        <br/>
        {{ Form::submit('save', ['class' => 'btn btn-primary']) }}

        {{ Form::close() }}
    </div>
    <div class="clearfix"></div>
</div>
@stop