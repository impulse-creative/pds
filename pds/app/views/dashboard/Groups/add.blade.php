@extends('layouts.pds-advantage')

@section('content')
<div class="group-add-form-container">
	<div class="group-add-form-container-title">
		<h1>Add a New Group</h1>
		<hr/>
		<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;Please refrain from using numbers (1, 2, 42, etc.) or special characters (?, @, #, etc.) in the group name. This could cause conflicts with syncing the group to Hubspot.</div>
	</div>
	<div class="col-md-4">
		@if($errors->has('name'))
			<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $errors->first('name') }}</div>
		@endif
		{{ Form::open([
			"route" => "acl.addgroups",
			"autocomplete" => "off",
			"id" => "add-group-form"
		]) }}

		{{ Form::field([
			"name" => "name",
			"label" => "Enter the name of the group",
			"placeholder" => "Examples: new group, new_group, New Group..."
		]) }}

		{{ Form::field([
			"name" => "description",
			"label" => "Enter a short description of the group",
			"placeholder" => "Forum access, Resource Access, etc..."
		]) }}

		{{ Form::submit("Add Group", array('class' => 'btn btn-success')) }}
		{{ Form::close() }}
	</div>
</div>
<div class="clearfix"></div>
@stop

<!-- Form field did contain "form" => $form -->