@extends('layouts.pds-advantage')

@section('content')
<div class="group-add-form-container">
	<div class="group-add-form-container-title">
		<h1>Edit Group</h1>
		<hr/>
	</div>
	<div class="col-md-4">
	{{ Form::open([
		"url" => URL::full(),
		"autocomplete" => "off",
		"method" => "post"
	]) }}

		{{ Form::field([
			"name" => "name",
			"label" => "Enter in the edited version of the group",
			"form" => $form,
			"placeholder" => "group name",
			"value" => $group->name
		]) }}

		{{ Form::submit('save') }}
		
	{{ Form::close() }}
	</div>
</div>
@stop