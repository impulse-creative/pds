
@extends('layouts.pds-advantage')

@section("content")
<div class="container" id="group-manager-container">
    <div class="row">
   
    <div class="col-sm-12 main">

    	<div id="group-manager-header">
    		<h1>Manage Permission Groups</h1>
    		<hr>
    		<div class="response_errors">
    			@if(Session::has('Success'))
    				<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
    			@endif
    			@if(Session::has('errors'))
    				<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('errors', 'default') }}</div>
    			@endif
    		</div>
    	</div>
        <div class="tabpanel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li id="groupTab" role="presentation" class="active"><a href="#access" aria-controls="access" role="tab" data-toggle="tab">Access Levels</a></li>
                <li id="groupTab" role="presentation"><a href="#resource_containers" aria-controls="resource_containers" role="tab" data-toggle="tab">Resource Container Groups</a></li>
                <li id="groupTab" role="presentation"><a href="#employees" aria-controls="employees" role="tab" data-toggle="tab">Employee Mapping</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="access">
                    <a href="{{ url('dashboard/group/add') }}" class="btn btn-primary">add group</a>
                    @if (isset($groups))
                        <div class="table-responsive">
                            <table class="group-list table"> 
                                <tr>
                                    <th>name</th> 
                                    <th>Description</th> 
                                </tr>
                                @foreach ($groups as $group)
                                    <tr>
                                        <td>{{ $group->name }}</td>
                                        <td>{{ $group->description }}</td>
                                        <td><a href="{{ URL::route('acl.editor') }}?id={{ $group->id }}">edit</a></td>
                                        <td><a href="{{ URL::route('acl.delete') }}?id={{ $group->id }}" class="confirm" data-confirm="Are you sure you want to delete this group?">delete</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @else
                        <p>There are no groups.</p>
                    @endif
                    <a href="{{ url('dashboard/group/add') }}" class="btn btn-primary">add group</a>
                </div>
                <div role="tabpanel" class="tab-pane" id="resource_containers">
                    <div class="col-lg-12 col-md-12 small-padding">
                        <a href="{{ URL::route('add.resource.group') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add a New Resource Group</a>
                    </div>
                    <div class="table-responsive">
                        @if (isset($resource_groups))
                            <div class="table-responsive">
                                <table class="group-list table"> 
                                    <tr>
                                        <th>name</th>
                                        <th>Description</th>
                                        <th>Access Level</th>
                                    </tr>
                                    @foreach ($resource_groups as $key => $group)
                                        <tr>
                                            <td>{{ $group->group_name }}</td>
                                            <td>{{ $group->description }}</td>
                                            <td>{{ $group->group->description }}</td>
                                            <td class="center"><a href="{{ URL::route('acl.resource.group.edit') }}?id={{ $group->id }}">edit group permissions</a></td>
                                            <td><a href="{{ URL::route('acl.delete') }}?id={{ $group->id }}&resource_group=true" class="confirm" data-confirm="Are you sure you want to delete this group?">delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <p>There are no groups.</p>
                        @endif
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="employees">
                    <div class="col-lg-12 col-md-12 small-padding">
                        <div class="table-responsive">
                            {{ Form::open(['route' => 'save.pds.employees']) }}
                            @if(isset($employees))
                                <table class="group-list table"> 
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Coach ID</th>
                                        <th>Implementation Specialist ID</th>
                                        <th>myPDS Username</th>
                                        <th>myPDS Password</th>
                                    </tr>
                                    @foreach ($employees as $key => $value)
                                        <tr>
                                            <td>{{ $value->fname.' '.$value->lname }}</td>
                                            <td>{{ $value->email }}</td>
                                            <td>{{ Form::text('coach_id['.$value->id.']', $value->coach_id) }}</td>
                                            <td>{{ Form::text('is_id['.$value->id.']', $value->is_id) }}</td>
                                            <td>{{ Form::text('teamwork_id['.$value->id.']', $value->project_name) }}</td>
                                            <td>{{ Form::text('teamwork_password['.$value->id.']', $value->project_password) }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            @else
                                <p>All the employees are gone. I think something is wrong!.</p>
                            @endif
                            <div class="center">
                                {{ Form::submit('Save Employees', ['class' => 'btn btn-primary']) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
@stop

@section('extra-js')
    @parent
	{{ HTML::script('js/dash-groups.js') }}
    <script>
        $('#groupTab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });
    </script>
@stop