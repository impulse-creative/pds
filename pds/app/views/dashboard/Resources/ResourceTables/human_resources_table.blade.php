          <div class="table-responsive">
            {{ Form::open(array('url' => '/admin/resources/show'))}}
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Filename</th>
                    <th>Version</th>
                    <th>Creation Date</th>
                    <th>Creator</th>
                    <th># of Downloads</th>
                    @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))
                      <th>Remove</th>
                    @endif
                  </tr>
                </thead>
                <tbody id="human_resources">
                  @if($table_data)
                    @foreach($table_data as $key => $val)
                      {{-- image references may disappear, I was just happy I got this working, so I was having some fun ;) --}}
                      @if($val->resource_type_id === 1)
                        <?php 
                          //get real file name from dropbox link string
                          $file_correct = explode("/", $val->drop_box_link);
                          //set pointer to end of array to collect real file name, this will be the case in every instance so we will be ok
                          $filename = end($file_correct);
                        ?>
                        <tr>
                          <td>{{ Form::checkbox('dwldChoice', $val->id) }}&nbsp;<img src="{{ URL::to('/storage/files/') }}/{{ $filename }}" width="30" height="30"/>&nbsp;{{ $val->filename }}&nbsp;<a class="downloadThis confirm" href="{{ url('dashboard/resources/show/'. $filename) }}" data-confirm="Are you sure you want to download this resource?"><span class="glyphicon glyphicon-download-alt"></span></a>@if(Auth::user()->groups === "impulse_admin" || Auth::user()->groups === "admin")&nbsp;<span class="glyphicon glyphicon-pencil fileInfoChange editInfoIcon" data-toggle="modal" data-id="{{ $val->id }}" data-target="#changeFileInfo" title="Edit"></span>@endif</td>
                          <td>{{ $val->file_version }}</td>
                          <td>{{ $val->created_at }}</td>
                          <td>{{ $val->author }}</td>
                          <td id="dwnld-count">{{ $val->download_count }}</td>
                          @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))
                            <td><a class="deleteThis confirm btn btn-danger" href="{{ url('dashboard/resources/'. $val->id) }}" data-confirm="Are you sure you want to delete this resource?"><span class="glyphicon glyphicon-trash"></span>&nbsp;Trash</a></td>
                          @endif
                        </tr>
                      @endif
                    @endforeach                
                  @endif
                </tbody>
              </table>
            {{ Form::close() }}
          </div>