          <div class="table-responsive">
	            <table class="table table-striped">
	              <thead>
	                <tr>
		                <th>Filename</th>
		                <th>Version</th>
		                <th>Creation Date</th>
		                <th>Creator</th>
		                <th># of Downloads</th>
	                    @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))
	                      <th>Remove</th>
	                    @endif
	                </tr>
	              </thead>
	                  {{-- Replaced by Ajax information --}}
                    <tbody id="marketing" ></tbody>
	            </table>
	      </div>