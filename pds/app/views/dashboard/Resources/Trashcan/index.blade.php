@extends('layouts.pds-advantage')

@section('extra-js')
		@parent
        {{ HTML::script('js/resource-file-scripts.js') }}
@stop

@section('content')
<div class="trashed-resources">
	<h1>Recently Removed Resources</h1>
	<div class="soft-deleted-items">
		@if($soft_deletes)
			<p class="muted"> Click the red 'x' to permanently delete the item, or the green '+' to restore! </p><br/>
			<a href="{{ URL::route('dash.resources') }}"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Return to resource tables</a>
			@if(Session::has('Removal Success'))
        		<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Removal Success', 'default') }}</div>
			@elseif (Session::has('Removal Failure'))
        		<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Removal Failure', 'default') }}</div>
			@endif
			@if(Session::has('Restore Success'))
        		<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Restore Success', 'default') }}</div>
			@elseif (Session::has('Restore Failure'))
        		<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Restore Failure', 'default') }}</div>
			@endif
          	<div class="table-responsive">
	            <table class="table table-striped">
	              	<thead>
	                	<tr>
	                  		<th>Filename</th>
					  		<th>Date Removed</th>
	                  		<th>Restore</th>
	                  		<th>Destroy</th>
	                	</tr>
	              	</thead>
	                  {{-- Replaced by Ajax information --}}
                    <tbody id="marketing" >
						@foreach($soft_deletes as $key => $val)
							<tr class="item">
								<td class="orange-text file-control">{{ $val->filename }}</td>
								<td class="date-removed">{{ $val->deleted_at }}</td>
								<td><a href="{{ URL::route('resource.resurrect') }}?file_id={{ $val->id }}"><span class="glyphicon glyphicon-plus green-text"></span></a></td>
								<td><a href="{{ URL::route('resource.decimate') }}?file_id={{ $val->id }}" data-confirm="Are you sure you want to delete this resource?"><span class="glyphicon glyphicon-remove red-text"></span></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@endif
	</div>
</div>
@stop