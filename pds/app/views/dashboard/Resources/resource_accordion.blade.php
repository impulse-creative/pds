@extends('layouts.pds-advantage')

@section('head')
  @parent
  {{ HTML::style('css/loading.css') }}
@stop

@section('extra-js')
        {{ HTML::script('//code.jquery.com/ui/1.11.3/jquery-ui.min.js' )}}
        {{ HTML::script('js/resource-file-scripts.js') }}
        {{ HTML::script('js/loading.js') }}
@stop

@section('content')
<div id="resource-header">
	<h1 class="test-console">Resources</h1>
	<hr/>
  {{-- 'Success' is the result of a successful file edit --}}
  @if(Session::has('Edit Success'))
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Edit Success', 'default') }}</div>
  @endif
  @if (Session::has('No Files'))
    <div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('No Files', 'default') }}</div>
  @endif
  @if (Session::has('Upload Success'))
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Upload Success', 'default') }}</div>
  @endif
  @if (Session::has('Resource Removed'))
    <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Resource Removed', 'default') }}</div>
  @endif
  @if (Session::has('Not Allowed'))
    <div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Not Allowed', 'default') }}</div>
  @endif
</div>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="phar-doc-section">
          Pharmacy Documents
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li id="resourceTab" class="active hr-tab ajax-fill" data-auth-type="{{ Auth::user()->groups }}" data-r-type="1" data-ajax-type="human_resources" data-r-table="hr-resources"><a href="#hr-resources" role="tab" data-toggle="tab">Human Resources</a></li>
            <li id="resourceTab" class="marketing-tab ajax-fill" data-auth-type="{{ Auth::user()->groups }}" data-r-type="2" data-ajax-type="marketing" data-r-table="marketing-resources"><a href="#marketing-resources" role="tab" data-toggle="tab">Marketing</a></li>
            <li id="resourceTab" class="tax-tab ajax-fill" data-auth-type="{{ Auth::user()->groups }}" data-r-type="3" data-ajax-type="tax" data-r-table="tax-resources"><a href="#tax-resources" role="tab" data-toggle="tab">Tax</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active active-ajax" id="hr-resources">@include('dashboard.Resources.ResourceTables.human_resources_table')</div>
            <div class="tab-pane" id="tax-resources">@include('dashboard.Resources.ResourceTables.tax_table')</div>
            <div class="tab-pane" id="marketing-resources">@include('dashboard.Resources.ResourceTables.marketing_table')</div>
        </div>{{-- End of Tab Content --}}
        <div class="downloadFiles">
          <a class="btn btn-primary dwldFilesBtn"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download</a>
        </div><br/>        
        @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))
        {{-- Add file functionality --}}
        <div id="hr-modal" class="uploader">
          <h3>Upload a File</h3>
          <span>Click 'Upload' to add additional files</span>
          <hr/>
          {{ Form::open(array('route' => 'store.resource', 'class' => 'hr-form', 'files' => true))}}
            {{ Form::hidden('section', 'human_resources') }}
            {{ Former::files('file')->accept('gif', 'jpg', 'png', 'docx', 'pdf', 'csv') }}<br/>
            {{ Form::submit('Upload', array('class' => 'btn btn-success submit-form')) }}
            <a href='#'>cancel</a>
          {{ Form::close() }}
        </div><br/>
        <div class="addFile">
          <a class="btn btn-success hr-show-form" href="#"><span class="glyphicon glyphicon-plus"></span> Add File</a>
        </div>
        @endif
	  </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="spreadsheet-section" data-auth-type="{{ Auth::user()->groups }}" data-r-type="4" data-ajax-type="spreadsheet" data-r-table="spreadsheet-resource" class="ajax-fill">
          Spreadsheets
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse spreadsheet-collapse">
      <div class="panel-body" id="spreadsheet-resource">
        @include('dashboard.Resources.ResourceTables.spreadsheets_table')
        <div class="downloadFiles">
          <a class="btn btn-primary dwldFilesBtn"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download</a>
        </div><br/> 
        @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))
        {{-- Add file functionality --}}
        <div id="ss-modal" class="uploader">
          <h3>Upload a File</h3>
          <span>Click 'Upload' to add additional files</span>
          <hr/>
          {{ Form::open(array('route' => 'store.resource', 'class' => 'ss-form', 'files' => true))}}
            {{ Form::hidden('section', 'spreadsheet') }}
            {{ Former::files('file')->accept('gif', 'jpg', 'png', 'docx', 'pdf', 'csv') }}<br/>
            {{ Form::submit('Upload', array('class' => 'btn btn-success')) }}
            <a href='#'>cancel</a>
          {{ Form::close() }}
        </div><br/>
        <div class="addFile">
          <a class="btn btn-success ss-show-form" href="#"><span class="glyphicon glyphicon-plus"></span> Add File</a>
        </div>
        @endif
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" id="webinar-section" data-auth-type="{{ Auth::user()->groups }}" data-r-type="5" data-ajax-type="webinar" data-r-table="webinar-resource" class="ajax-fill">
          Webinars
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body" id="webinar-resource">
        @include('dashboard.Resources.ResourceTables.webinar_table')
        <div class="downloadFiles">
          <a class="btn btn-primary dwldFilesBtn"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download</a>
        </div><br/>         
        @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))       
        {{-- Add file functionality --}}
        <div id="webinar-modal" class="uploader">
          <h3>Upload a File</h3>
          <span>Click 'Upload' to add additional files</span>
          <hr/>
          {{ Form::open(array('route' => 'store.resource', 'class' => 'webinar-form', 'files' => true))}}
            {{ Form::hidden('section', 'webinar') }}
            {{ Former::files('file')->accept('video') }}<br/>
            {{ Form::submit('Upload', array('class' => 'btn btn-success')) }}
            <a href='#'>cancel</a>
          {{ Form::close() }}
        </div><br/>
        <div class="addFile">
          <a class="btn btn-success webinar-show-form" href="#"><span class="glyphicon glyphicon-plus"></span> Add File</a>
        </div>
        @endif
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" id="video-section" data-auth-type="{{ Auth::user()->groups }}" data-r-type="6" data-ajax-type="video" data-r-table="video-resource" class="ajax-fill">
          Videos
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse">
      <div class="panel-body" id="video-resource">
        @include('dashboard.Resources.ResourceTables.videos_table')
        <div class="downloadFiles">
          <a class="btn btn-primary dwldFilesBtn"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Download</a>
        </div><br/>         
        @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))      
        {{-- Add file functionality --}}
        <div id="video-modal" class="uploader">
          <h3>Upload a File</h3>
          <span>Click 'Upload' to add additional files</span>
          <hr/>
          {{ Form::open(array('route' => 'store.resource', 'class' => 'video-form', 'files' => true))}}
            {{ Form::hidden('section', 'video') }}
            {{ Former::files('file')->accept('video') }}<br/>
            {{ Form::submit('Upload', array('class' => 'btn btn-success')) }}
            <a href='#'>cancel</a>
          {{ Form::close() }}
        </div><br/>
        <div class="addFile">
          <a class="btn btn-success video-show-form" href="#"><span class="glyphicon glyphicon-plus"></span> Add File</a>
        </div>
        @endif
      </div>
    </div>
  </div>
  @if(in_array('impulse_admin', $user_groups) || in_array('1', $user_groups) || in_array('5', $user_groups))
    <span class="pull-right"><a href="{{ url('/admin/resources/softdeletes/list/index') }}">Need to dig through the trash?</a></span>
  @endif
</div>

{{-- Change Basic Information Modal --}}
<div class="modal fade" id="changeFileInfo" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="modalLabel">Change File Information</h4>
      </div>
      <div class="modal-body">
        @if(Session::has('Edit Failure'))
              <script>
                $(document).on('ready', function() {
                  $(".fileInfoChange").trigger('click');
                });
              </script>
              <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Edit Failure', 'default') }}</div>
        @endif
        <div class="validation-warning"></div>
        {{ Form::open(array('route' => 'update.file.info', 'class' => 'updateFileInfo', 'novalidate' => '')) }}
          <input name="file_name" id="file_name" type="text" class="inp-field" placeholder="File Name" value="{{ Input::old('file_name') }}"/><br/>
          <input name="owner_name" id="author_name" type="text" class="inp-field" placeholder="Owner First and Last Name" value="{{ Input::old('owner_name') }}"/><br/>
          <div class="file-version">
            <input name="version_number" id="version_number" type="text" class="inp-field" placeholder="1"/>.<input type="text" class="inp-field" name="sub_version_number" placeholder="0"/>
          </div>
          <input name="file_id" type="hidden" class="hidden-file-id"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default closeFileInfoChange" data-dismiss="modal">Close</button>
        {{ Form::submit('Save Changes', array('class' => 'btn btn-primary basic-file-change-submit')) }}
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
@stop