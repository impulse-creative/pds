@extends('layouts.Login.login-layout')

@section('content')
	<div class="forgot-pass-container center">
		<div class="forgot-pass">
			@if(Session::has('Success'))
	            <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Success', 'default') }}</div>
			@endif
		    {{ Form::open(array('route' => 'remind.handle')) }}
		        <input type="email" id="email" name="email" class="inp-field" placeholder="Email to send reset to"><br/>
                        <p style="color:#fff;">Please Click Reset Button Once Only</p>
                        <div class="center">
                            <input type="submit" value="Reset Password" class="btn btn-primary"/>
                        </div>
		    {{ Form::close() }}
		</div>
		<div style="clear:both;"></div>
    </div>


@stop