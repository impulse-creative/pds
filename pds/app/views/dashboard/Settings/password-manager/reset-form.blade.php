@extends('layouts.request-confirm')

@section('content')

<div class="row row-content">
    <div id="conf-container">

        {{ Form::open(array('route' => 'password.update', 'class' => 'reset-form')) }}
        <div>
            <h1>Change Your Password</h1><br/><hr/>
            <span>Enter your new password, confirm it, then click "Reset Password"</span>
        </div>
        <input type="hidden" name="token" value="{{ $token }}">
        <input class="inp-field" type="email" name="email" value="{{ Input::old('email') }}" placeholder="Email"><br/>
        @if($errors->has('email')) <p class="alert alert-danger" style="font-size: 12px;">{{ $errors->first('email') }}</p> @endif
        <input class="inp-field" type="password" name="password" placeholder="Password"><br/>
        @if($errors->has('password')) <p class="alert alert-danger" style="font-size: 12px;">{{ $errors->first('password') }}</p> @endif
        <input class="inp-field" type="password" name="password_confirmation" placeholder="Password Confirmation"><br/>
        @if($errors->has('password_confirmation')) <p class="alert alert-danger" style="font-size: 12px;">{{ $errors->first('password_confirmation') }}</p> @endif
        <input class="pass-change-btn" type="submit" value="Reset Password">
        {{ Form::close() }}
    </div>
</div>
<script src="{{ URL::to('/js/global.js') }}"></script>      

@stop