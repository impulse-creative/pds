@extends('layouts.pds-advantage')


@section('head')
@parent
{{-- HTML::style('css/main-styles.css') --}}
@stop

@if($menu_data)
@section('menu')
@include($menu_data)
@stop
@endif

@section('content')
@parent
{{-- Password Reset Section --}}
<div class="row-fluid">

    <div class="col-md-12  pass-reset-area">
        @if (Session::has('wrongpass'))
        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('wrongpass', 'default') }}</div>
        @endif
        @if (Session::has('error'))
        {{ trans(Session::get('reason')) }}
        @elseif (Session::has('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">
                    &times;
                </span>
                <span class="sr-only">
                    Close
                </span>
            </button>
            {{ $value = Session::pull('success', 'default') }}
        </div>
        @endif

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            {{-- <li id="myTab"><a href="#general" role="tab" data-toggle="tab">General</a></li> --}}
            <li id="myTab" class="active"><a href="#password" role="tab" data-toggle="tab">Password</a></li>
            {{--<li id="myTab"><a href="#privacy" role="tab" data-toggle="tab">Privacy</a></li>--}}
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            {{-- 
            <div class="tab-pane" id="general">
                <a href="{{ URL::to('dashboard/accounts/destroy/account').'/'.Auth::id() }}" class="btn btn-danger small-margin confirm" data-confirm="Are you sure you want to remove this category?">
            <span class="glyphicon glyphicon-remove"></span>&nbsp;Delete Account
            </a>
        </div>
        --}}
        <div class="tab-pane active" id="password">
            <h3>Change Password</h3>
            <hr/>
            <div class="col-md-4">
                {{ Form::open(array('route' => 'password.request')) }}
                <input type="email" name="email" class="inp-field" placeholder="Email to send reset to"><br/>
                <input type="submit" value="Reset Password" class="pass-change-btn">
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
        </div>
        {{--<div class="tab-pane" id="privacy">Privacy</div>--}}
    </div>{{-- End of Tab Content --}}
</div>{{-- End of Password Reset Area --}}
<div class="clearfix"></div>
</div>
@stop