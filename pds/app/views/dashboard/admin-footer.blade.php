
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
    <script type='text/javascript' src="http://imsky.github.io/holder/holder.js"></script>
        <!-- main footer section | .main-footer -->
        <footer id="main-footer" class="container-fluid">
            <div class="footer container medium-padding center">
		<div class="white-text">Copyright © 1998-{{date('Y')}} Pharmacy Development Services, Inc . All Rights Reserved.</div>
                <div class="footer-social-links">
                    <ul>
                        <li><a href="https://www.facebook.com/PharmacyDevelopmentServices" class="white-text" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="https://twitter.com/rxmarketing" class="white-text" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/565830" class="white-text" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                    </ul>
                </div>
	</div>
        </footer>
