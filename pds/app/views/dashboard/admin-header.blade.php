

@section('admin-header')
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ URL::route('dash.main') }}"> </a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right mobile-nav">
            <li><a href="{{ URL::route('dash.main') }}">Dashboard</a></li>
            @include('dashboard.group-dash-views.menu')
            <li><a href="{{ URL::route('password.remind') }}">Settings</a></li>
            <li><a href="{{ URL::route('dash.logout') }}">Logout</a></li>
            <li><div class="dash-supp-btn"><a href="{{ URL::route('dash.get.support') }}">Support</a></div></li>
          </ul>
          <div class="desktop-version dash-supp-btn pull-right"><a href="{{ URL::route('dash.get.support') }}">Support</a></div>
          @if(isset($user))
            <div class="white-text inline-block pull-right top-bar-greeting">Welcome, {{ $user_info->fname }}&nbsp;{{ $user_info->lname }}!</div>
          @endif
        </div>
      </div>
    </div>
@show