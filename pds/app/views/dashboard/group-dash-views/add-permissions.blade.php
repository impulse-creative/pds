@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
    <?php
    $user_permissions = explode(':', $target['user']['groups']);
    $profile = $target['user'];
    $name = $target['user']['fname'] . ' ' . $sections['user']['lname'];
    ?>

    <div class="forum-bg">
        <div class="container">
            <div class="col-md-12 large-padding">
                <?php $target_user = User::find($target['id']); ?>
                <h1>Editing Permissions For: {{{ $target_user->fname.' '.$target_user->lname }}}</h1>
                <span class="text-muted">* If a box is checked, then the user has that permission, and vice versa.</span>
                @if(Session::has('Success'))
                    <div class="alert alert-success small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
                @endif
                @if(Session::has('Failure'))
                    <div class="alert alert-danger small-margin"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
                @endif
                <div class="add-permissions-form-container medium-padding">
                    {{ Form::open(['route' => 'update.permissions', 'class' => 'update-permissions-form']) }}
                    <div class="checkbox added-groups col-lg-12">
                        {{ Form::hidden('target_id', $target['id']) }}
                        @foreach($permissions as $key => $name)
                            @if($name == '' || $key == 0) <?php continue; ?> @endif
                            <?php 
                            $checked = false;
                            if(in_array($key, $user_permissions)) { $checked = true; } ?>
                            <label class="col-lg-5 pull-left">
                                {{ Form::checkbox($name, $key, $checked) }}
                                {{ $name }}
                            </label>
                            <br/>
                        @endforeach
                    </div>
                    <div class="control-group hidden-items">
                        {{ Form::hidden('target_id', $target['id']) }}
                    </div>
                    <div class="control-group">
                        {{ Form::submit('Set Permissions', ['class' => 'btn btn-primary']) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
@stop