@extends('layouts.pds-advantage')

@section("content")
<?php 

    $permissions = explode(':', $sections['permissions']);
    $user = $sections['user'];
    $name = $sections['user']['fname'].' '.$sections['user']['lname'];
    $assData = json_decode($sections['user']['associated_data']);
    $something = 'does not make sense to me';
  
?>
<div class="forum-bg user-profile">
    
        
    
  <div class="container">
    
    <div class="col-md-12 main dash-main">
        
      @if (Session::has('Sent'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Sent', 'default') }}</div>
      @endif
      @if (Session::has('InsufficientPermissions'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('InsufficientPermissions', 'default') }}</div>
      @endif
      @if (Session::has('Success'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Success', 'default') }}</div>
      @endif
      @if (Session::has('Successful Conference Signup'))
        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Successful Conference Signup', 'default') }}</div>
      @endif
      
      {{-- Start of main content --}}

      <h1 class="page-header">Hello, {{ $name }}, where would you like to go today? </h1>
      <div class="row placeholders">
          {{ $notifications['topics'] }}
          {{ $notifications['resources'] }}
      <div class="row placeholders small-padding">

        @if(in_array(10, $permissions) || in_array(11, $permissions))
            <div class ='col-md-6'>
                <h2>Recent Message Board Topics</h2>
                
                @foreach($forum['recent'] as $topic)
                <?php
                $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $topic->title); 
                //get message count  
                $messageCount = \ForumMessages::where('parent_topic', $topic->id)->count(); 
                //get first reply guy
                $lastRepliedAuthor = \User::find($topic->last_replied);
                 
                ?>
                <div class="topic-item">
                    <div class="author-img col-lg-2 col-md-2 col-xs-2 vcenter">
                        {{ $image = User::getBioImage(Auth::id(), 'vcenter forum-mini-profile-img') }}
                        
                    </div><!--
                    --><div class="topic-info col-lg-8 col-md-8 col-xs-8 vcenter">
                            <h2 class="topic-title"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}" class="purple-text">{{ $topic->title }}</a></h2>
                            <p class="author-info">Posted by <span class="purple-text"><a target="_blank" href="{{ URL::to('user/'.$topic->author_id.'/'.$topic->fname.'-'.$topic->lname) }}">{{ $topic->fname }}&nbsp;{{ $topic->lname }}</a> at {{$topic->created_at->toFormattedDateString()}}</span>
                                @if(is_object($lastRepliedAuthor))
                                        <br>Latest reply by <span class="purple-text"><a target="_blank" href="{{ URL::to('user/'.$lastRepliedAuthor->id.'/'.$lastRepliedAuthor->fname.'-'.$lastRepliedAuthor->lname) }}">{{ $lastRepliedAuthor->fname.' '.$lastRepliedAuthor->lname }}</a></span>
                                    @else 
                                    <br><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}" class="force-orange-text">Be the first to reply!</a> 
                                    @endif
                            </p>
                    </div><!--
                        --><div class="topic-comment-count col-lg-2 col-md-2 col-xs-2 vcenter"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle
                                    )}}/{{$topic->id}}#post-replies">{{ $messageCount }}</a></div>
                        <div class="clearfix"></div>
                </div>
                @endforeach
            
            </div>
            <div class ='col-md-6'>
            <h2>Recent Popular Message Board Topics</h2>
                
                @foreach($forum['popular'] as $topic)
                <?php
                $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $topic->title); 
                //get message count  
                $messageCount = \ForumMessages::where('parent_topic', $topic->id)->count(); 
                //get first reply guy
                $lastRepliedAuthor = \User::find($topic->last_replied);
                
                ?>
                <div class="topic-item">
                    
                                <div class="author-img col-lg-2 col-md-2 col-xs-2 vcenter">
                                    {{ $image = User::getBioImage(Auth::id(), 'vcenter forum-mini-profile-img') }}
                                </div><!--
                                --><div class="topic-info col-lg-8 col-md-8 col-xs-8 vcenter">
                                        <h2 class="topic-title"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}" class="purple-text">{{ $topic->title }}</a></h2>
                                        <p class="author-info">Posted by <span class="purple-text"><a target="_blank" href="{{ URL::to('user/'.$topic->author_id.'/'.$topic->fname.'-'.$topic->lname) }}">{{ $topic->fname }}&nbsp;{{ $topic->lname }}</a> at {{$topic->created_at->toFormattedDateString()}}</span>
                                            @if(is_object($lastRepliedAuthor))
                                            <br>Latest reply by <span class="purple-text"><a target="_blank" href="{{ URL::to('user/'.$lastRepliedAuthor->id.'/'.$lastRepliedAuthor->fname.'-'.$lastRepliedAuthor->lname) }}">{{ $lastRepliedAuthor->fname.' '.$lastRepliedAuthor->lname }}</a></span>
                                                @else 
                                                <br><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}" class="force-orange-text">Be the first to reply!</a> 
                                                @endif
                                        </p>
                                </div><!--
                                    --><div class="topic-comment-count col-lg-2 col-md-2 col-xs-2 vcenter"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle
                                                )}}/{{$topic->id}}#post-replies">{{ $messageCount }}</a></div>
                                    <div class="clearfix"></div>
                            </div>
                @endforeach
            
            </div>
        @else
        <div class ='col-md-6'>
            <p>You have not yet been approved for access to the forums.  Contact <a href="#">NOONE</a></p>
        </div>
        @endif
      </div>
      <div class="row placeholders medium-padding hidden">
        <h2>Recent Event Activity</h2>
        @if(in_array(12, $permissions) || in_array(11, $permissions))
            <div class ='col-md-6'>Recent Event Postings</div>
            <div class ='col-md-6'>Recent Popular Events</div>
        @else
        <div class ='col-md-6'>
            <p>You have not yet been approved for access to the events center.  Contact <a href="#">NOONE</a></p>
        </div>
        @endif
      </div>
      <div class="row placeholders medium-padding hidden">
        <h2>Recent Resource Activity</h2>
        @if(in_array(12, $permissions) || in_array(15, $permissions))
            <div class ='col-md-6'>Recent Resource Updates</div>
            <div class ='col-md-6'>Recent Popular Resources</div>
        @else
        <div class ='col-md-6'>
            <p>You have not yet been approved for access to the resource center.  Contact <a href="#">NOONE</a></p>
        </div>
        @endif
      </div>
      <div class="row placeholders-vendor medium-padding">
        @if(in_array('3', $permissions))
            @include('dashboard.partials.main.nav-circles')
        @endif
      </div>

      
    </div>
  </div>
</div>

{{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
{{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
{{ HTML::script('js/notifications.js') }}

@stop