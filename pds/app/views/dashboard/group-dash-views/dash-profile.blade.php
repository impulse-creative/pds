@extends('layouts.pds-advantage')

@section('head')
    @parent
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
@stop 

@section("content")
<?php
$permissions = explode(':', $sections['permissions']);
$profile = $target['user'];
$name = $target['user']['fname'] . ' ' . $sections['user']['lname'];
$assData = json_decode($target['user']['associated_data']);
?>
@if (Session::has('Failure'))
    <script>
        setTimeout(function () {
            $('#getProfile').trigger('click');
        }, 101);
    </script>
@endif
<div class="forum-bg user-profile">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-6">
                <h1 class="page-header">User Profile: {{ ' '.ucwords($profile->fname).' '.ucwords($profile->lname) }} </h1>
            </div>
            <div class="col-md-6">
                @if($user->id == Auth::id())
                <h3 class="page-header"><a class="pull-right pointer" id="getProfile" data-toggle="modal" data-target="#changeBasicInfo"><span class="glyphicon glyphicon-pencil"></span> &nbsp;Edit My Profile</a></h3>
                @endif
            </div>
        </div>
        <div class="col-md-12">    
            {{-- Start of main content --}}
            <div class="row">
                <div class="col-md-6">
                    <div>
                        {{ $image = User::getBioImage($profile['id'], 'display-profile-image make-badge-larger') }}
                    </div>
                    <div class="profile-info-and-replies">
                        {{ ucwords($profile['fname']).' '.ucwords($profile['lname']) }}<br>
                        {{ $company = $profile['pharmacy'] != 'null' ? '<strong>'.ucwords($profile['pharmacy']).'</strong><br>' : '<br>' }}
                        {{ isset($profile['city']) ? ucwords($profile['city']) : '' }}{{ isset($profile['state']) && $profile['state'] != 0 ? isset($profile['city']) ? ', ' . ucwords($states_list[$profile['state']]) : ucwords($states_list[$profile['state']]) : '' }}<br/>
                        {{ strlen($profile['website']) > 6 ? '<a href="'.$profile['website'].'">'. str_replace('http://', '', $profile['website']) .'</a>' : '' }}
                        <div class="small-padding">
                            @if($assData != null ||  $assData != '')
                                <?php
                                $icon['facebook'] = '<i style="font-size:30px;" class="fa fa-facebook-square"></i>';
                                $icon['twitter'] = '<i style="font-size:30px;" class="fa fa-twitter-square"></i>';
                                $icon['linkedin'] = '<i style="font-size:30px;" class="fa fa-linkedin-square"></i>';
                                ?>
                                @if($assData->facebook != '' || $assData->twitter != '' || $assData->linkedin != '')
                                    <div class="center user-social">
                                        @foreach($assData as $key => $value)
                                            @if($value != null && $value != '')
                                                <a href="{{$value = strpos($value, 'http') > -1 ? $value : 'http://'.$value }}" target="_blank">{{$icon[$key]}}</a>
                                            @endif
                                        @endforeach
                                    </div>
                                    <br>
                                @endif
                            @endif
                            @if(strlen($profile['bio']) > 0)
                                <div class="user-bio mobile-bio">
                                    <label>{{ucwords($profile['fname'])}}'s Bio:</label>&nbsp;<p>{{ $bio = $profile['bio'] != '' ? $profile['bio'] : '' }}</p>
                                </div>
                            @endif
                        </div>

                        <br>
                        <h2>Recent Replies by {{ ucwords($profile['fname']) }}</h2>
                        <div class="clearfix"></div>
                        @foreach($forum['replies'] as $topic)
                            <?php
                            //get message count  

                            $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $topic->title);

                            //get first reply guy
                            $messageCount = \ForumMessages::where('parent_topic', $topic->parent_topic)->count();
                            ?>
                            <div class="topic-item"><!--
                                --><div class="topic-info col-lg-12 col-md-12 col-xs-12 vcenter">
                                    <h2 class="topic-title"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}" class="purple-text">{{ $topic->title }}</a> on {{date('l, F jS \\a\\t h:i', strtotime($topic->created_at))}}</h2>
                                    <p class="author-info">{{ $topic->data }}</div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                        {{--- Badges --}}
                    </div>

                    {{-- bring data into from recent activity --}}

            </div>
            <div class="col-md-6">
                @if(strlen($profile['bio']) > 0)
                    <div class="user-bio desktop-bio">
                        <label>{{ucwords($profile['fname'])}}'s Bio:</label>&nbsp;<p>{{ $bio = $profile['bio'] != '' ? $profile['bio'] : '' }}</p>
                    </div>
                @endif
                <div class="small-padding">
                    <h2>Recent Topics by {{ ucwords($profile['fname']) }}</h2>

                    @foreach($forum['posts'] as $topic)
                        <?php
                        $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $topic->title);
                        //get message count  
                        $messageCount = \ForumMessages::where('parent_topic', $topic->id)->count();
                        //get first reply guy
                        ?>
                        <div class="topic-item"><!--
                            --><div class="topic-info col-lg-12 col-md-12 col-xs-12 vcenter">
                                <h2 class="topic-title"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}" class="purple-text">{{ $topic->title }}</a></h2>
                                <p>{{ $topic->description }}</p>
                            </div><!--
                            --><div class="topic-comment-count col-lg-2 col-md-2 col-xs-2 vcenter"><a href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle
                                        )}}/{{$topic->id}}#post-replies">{{ $messageCount }}</a></div>
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

@if(Auth::id() == $profile->id)
<!-- Change Basic Information Modal -->
<div class="modal fade" id="changeBasicInfo" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Change Basic Profile Information</h4>
            </div>
            <div class="modal-body row">
                <div class ='col-md-6'>
                    @if (Session::has('Failure'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{{ $value = Session::pull('Failure', 'default') }}</div>
                    @endif
                    @if($errors)
                        @foreach($errors->all as $error)
                            <p class="red-text">{{$error}}</p>
                        @endforeach
                    @endif
                    <div class="validation-warning"></div>
                    {{ Form::model($user, array('route' => 'basic.info.update', 'files' => true, 'class' => 'updateBasicInfo', 'novalidate' => '')) }}
                    {{ Form::hidden('user_id', Auth::user()->id) }}
                    {{ Form::input('text', 'fname', Input::old('fname'), array('class' => 'inp-field', 'placeholder' => 'New First Name'))}}
                    {{ Form::input('text', 'lname', Input::old('lname'), array('class' => 'inp-field', 'placeholder' => 'New Last Name'))}}
                    {{ Form::input('text', 'pharmacy', Input::old('pharmacy'), array('class' => 'inp-field', 'placeholder' => 'New company name'))}}
                    {{ Form::input('text', 'city', Input::old('city') ? Input::old('city') : $profile['city'], ['class' => 'inp-field', 'placeholder' => 'New City']) }}
                    {{ Form::select('state', $states_list, $profile['state'], ['class' => 'inp-field']) }}
                    {{ Form::input('email', 'email', Input::old('email'), array('class' => 'inp-field', 'placeholder' => 'New Email'))}}
                    {{ Form::input('website', 'website', Input::old('website'), ['class' => 'inp-field', 'placeholder' => 'Your Website URL']) }}
                    <div class="email-preferences">
                        <hr>
                        <h4>Manage Your Email Preferences</h4>
                        <ul>
                            <li>
                                <label>
                                    <input type="radio" name="digest" value ="0" {{ $digest == 0 ? 'checked' : '' }} /> Unsubscribes
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="digest" value ="1" {{ $digest == 1 ? 'checked' : '' }} /> Daily Digest
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="radio" name="digest" value ="2" {{ $digest == 2 ? 'checked' : '' }} /> Weekly Digest
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="resetPassword medium-padding center">
                        <a class="btn btn-primary change-password-button"><span class="glyphicon glyphicon-lock"></span>&nbsp;Change Password</a>
                        <div id="password-form" class="col-md-12 medium-padding" style="display:none">
                            <input type="hidden" name="changepassword" id="changepassword" value="0" />
                            <div class="col-md-12">
                                <h2>PDSadvantage password</h2>
                                {{ Form::label('password', 'New Password') }}<br>
                                {{ Form::password('password', Input::old('password'), ['class' => 'inp-field'])}}
                            </div>
                            <div class="col-md-12">
                                {{ Form::label('password_confirmation', 'Confirm Password') }}<br>
                                {{ Form::password('password_confirmation', Input::old('new_password'), ['class' => 'inp-field'])}}
                            </div>
                        </div>
                    </div>
                </div>     
                <div class ='col-md-6'>
                    <div class="input-group">
                        {{ Form::input('text', 'facebook', $value = is_object($assData) ? $assData->facebook : '' , array('class' => 'inp-field', 'placeholder' => 'Your Facebook Profile URL'))}}
                        {{ Form::input('text', 'twitter', $value = is_object($assData) ? $assData->twitter : '', array('class' => 'inp-field', 'placeholder' => 'Your Twitter Profile URL'))}}
                        {{ Form::input('text', 'linkedin', $value = is_object($assData) ? $assData->linkedin : '', array('class' => 'inp-field', 'placeholder' => 'Your LinkedIn Profile URL'))}}
                    </div>
                    <div class="input-group">
                        <div class="btn-toolbar" data-role="bio-toolbar" data-target="#bio_editor">
                            <div class="btn-group">
                                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                    <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                    <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-default" data-edit="bold" data-original-title="Bold (Ctrl/Cmd+B)" title="Bold"><i class="fa fa-bold"></i></a>
                                <a class="btn btn-default" data-edit="italic" data-original-title="Italic (Ctrl/Cmd+I)" title="Italic"><i class="fa fa-italic"></i></i></a>
                                <a class="btn btn-default" data-edit="underline" data-original-title="Underline (Ctrl/Cmd+U)" title="Underline"><i class="fa fa-underline"></i></i></a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-default" data-edit="insertunorderedlist" data-original-title="Bullet list" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                <a class="btn btn-default" data-edit="insertorderedlist" data-original-title="Number list" title="Number list"><i class="fa fa-list-ol"></i></a>
                                <a class="btn btn-default" data-edit="justifyleft" data-original-title="Align Left (Ctrl/Cmd+L)" title="Align left"><i class="fa fa-align-left"></i></a>
                                <a class="btn btn-default" data-edit="justifycenter" data-original-title="Center (Ctrl/Cmd+E)" title="Center"><i class="fa fa-align-center"></i></a>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-default" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                <a class="btn btn-default" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                            </div>
                        </div>
                        <div id="bio_editor" class="editBio editor">{{ $target['user']->bio or '' }}</div>
                        <textarea class="form-control body-description hidden" id="hidden-editor" name="bio"></textarea>
                        <script class="hidden">
                            //initialize editor
                            $('#bio_editor').wysiwyg({ toolbarSelector: '[data-role=bio-toolbar]'});
                            //need to bring in form class and wisywig editor id
                            $(document).ready(function() {
                                $('.updateBasicInfo').submit(function(event) {
                                    $('#hidden-editor').val($('#bio_editor').cleanHtml());
                                });
                                $('a[title]').tooltip({container:'body'});
                                $('.dropdown-menu input').click(function() {return false;})
                                            .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
                                .keydown('esc', function () {this.value='';$(this).change();});
                            });
                        </script>
                    </div>
                    <div class="profile-image">
                        {{ Form::file('bio_image') }} 
                        @if( isset($sections['user']['bio_image']) && !is_null($sections['user']['bio_image'])) <a class="btn btn-warning small-margin update-cancel"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</a> @endif
                        @if ($errors->has('bio_image'))&nbsp;<span class="red-text">{{ $errors->first('bio_image', 'Please provide an image to be displayed when you are active in the applicaiton') }}</span> @endif
                    </div>
                    @if(isset($sections['user']['bio_image']))
                        <script>$('.profile-image').hide();</script>

                        <div class="logo-preview col-sm-12 center">
                            <img src="{{ $image = $sections['user']['bio_image'] ? URL::to('/storage/files/profile_images').'/'.$sections['user']['bio_image'] : 'https://i0.wp.com/assets1.uvcdn.com/pkg/admin/icons/user_70-c68d06098b40646a91b7656094632c19.png?ssl=1' }}" alt="{{ $user->fname . $user->lname }}" class="logo-small-preview small-padding"/><br/>
                            <a class="btn btn-primary change-profile-image"><span class="glyphicon glyphicon-picture"></span>&nbsp;Change Profile Image</a>
                            <p class="small-padding smaller left-text">Please upload a square image if possible. A great size is 250px x 250px.  The image must be a PNG or a JPG file.  After uploading the image, the corners will be rounded on the image.</p>
                        </div><div class="clearfix"></div>
                    @endif
                    @if(isset($sections['user']['project_name']))
                        <div class="change-mypds-pass control-group">
                            {{ Form::label('project_password', 'myPDS Account Password', ['class' => 'form-label']) }}
                            {{ Form::input('text', 'project_password', $target['user']->project_password, ['class' => 'inp-field', 'placeholder' => 'MyPDS password']) }}
                        </div><div class="clearfix"></div>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default closeBasicInfoChange" data-dismiss="modal">Close</button>
                {{ Form::submit('Save Changes', array('class' => 'btn btn-primary basic-info-change-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endif
<script>
   //console.log('checking out profiles');
    $(document).ready(function () {
        $('.add-new-resource-form').submit(function (e) {
            var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
            $('#hidden-html').val(html);
        });
        $('body').on('click', '.change-profile-image', function (e) {
            e.preventDefault();
            $('.profile-image').fadeIn();
            $('.change-profile-image').fadeOut();
        });
        $('body').on('click', '.update-cancel', function (e) {
            e.preventDefault();
            $('.profile-image').fadeOut();
            $('.change-profile-image').fadeIn();
        });
        $('body').on('click', '.change-password-button', function(e) {
            e.preventDefault();
            $('#changepassword').val(1);
            $('#password-form').fadeIn();
        });
        $('option:nth-child(50)').after('<option value="">--------Provinces---------</option>');

        if(window.location.hash) {
            $('#changeBasicInfo').modal('show');
            $('input[name=project_password]').css('border-color', 'red');
        }
    });

</script>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
@stop