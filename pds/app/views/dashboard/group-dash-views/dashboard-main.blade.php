@extends('layouts.pds-advantage')

@section("content")
<?php
    $permissions = explode(':', $sections['permissions']);
    $user = $sections['user'];
    $name = $sections['user']['fname'].' '.$sections['user']['lname'];
    $assData = json_decode($sections['user']['associated_data']);
    
    function cleanForId($dirty_string) {
        $lather = str_replace(' ', '_', $dirty_string); // Replaces all spaces with underscores.
        return $rinse = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $lather)); // Removes special chars and makes lower case
    }
?>
<div class="forum-bg user-profile">
    @if($user->companies === '0' || is_null($user->companies) || $user->companies === '"{}"' || $user->companies === '{}'))
        {{ $pharmacy_setup_modal }}
    @endif
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 main dash-main">
            @if (Session::has('Sent'))
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Sent', 'default') }}</div>
            @endif
            @if (Session::has('InsufficientPermissions'))
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('InsufficientPermissions', 'default') }}</div>
            @endif
            @if (Session::has('Success'))
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Success', 'default') }}</div>
            @endif
            @if (Session::has('Successful Conference Signup'))
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Successful Conference Signup', 'default') }}</div>
            @endif
            @if (Session::has('Failure'))
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('Failure', 'default') }}</div>
            @endif
            @if(isset($user->project_name) && is_null($user->project_password))
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> 
                    Hey there {{ $user->fname }}! We have located your myPDS account under the username {{ $user->project_name }}, but we don't have your password! 
                    <a href="{{ URL::to('/user/'.$user->id.'/'.trim($user->fname.'-'.$user->lname)) }}#changeBasicInfo" class="orange-text">Please enter your password here.</a>
                </div>
            @endif

            {{-- Start of main content --}}
            <h1 class="page-header">Hello, {{ $name }}, where would you like to go today?</h1>
            <div class="row placeholders">
                @if(count($sections['notifications']) > 0)
                @foreach($sections['notifications'] as $notification)
                    @if($notification->last_replied == $user->id) <?php continue; ?> @endif
                    @if($user->id == 161) {{d($notification)}} @endif
                    <div class="topic-notification notification alert alert-info text-left notification-alert">
                        <button type="button" class="close hide-notification" data-target-id="{{ $notification->id }}" data-type="{{ isset($notification->title) ? 'topics' : 'resources' }}" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="notification-fix">
                            @if(isset($notification->last_replied))
                                {{ $notification->lastReplied->fname .' '. $notification->lastReplied->lname }} 
                                replied to 
                            @endif
                            @if(isset($notification->title))
                                <a class="orange-text" href="{{ URL::to('/message-boards/'.preg_replace('/ /', '-', preg_replace('/[^a-z\d\s]+/i', '', $notification->title))) }}/{{ $notification->id }}" target="_blank">
                                    {{ $notification->title }}
                                </a>
                            @else
                                <a class="orange-text" href="{{ $notification->resource_share_link }}" target="_blank">
                                    Resource: {{ $notification->filename }} was updated.
                                </a>
                            @endif  
                        </span>  
                    </div>
                @endforeach
                @endif
            </div>
            
            
            <div class="announcements">
                <div class="announcement-container">
                    <div class="announcement-title purple-text">
                        <h2>
                            Directly From Dan
                            @if(in_array('1', $permissions) || in_array('5', $permissions) || in_array(14, $permissions)) 
                                <span class="orange-text edit-announcement glyphicon glyphicon-pencil"></span>
                            @endif
                        </h2>
                    </div>
                    <div class="announcement-quote">
                        <p>
                            {{$dan_quote->danQuote or ''}}
                        </p>
                    </div>
                    {{ Form::open(['route' => 'update.dan.quote']) }}
                        <div class="announcement-update-quote control-group">
                            <div id="wysihtml5-toolbar" style="display:none;">
                                <div class="btn-group">
                                    <a data-wysihtml5-command="bold" class="btn btn-default">Bold</a>
                                    <a data-wysihtml5-command="italic" class="btn btn-default">Italic</a>
                                </div>
                                <div class="btn-group">
                                    <a data-wysihtml5-command="insertUnorderedList" class="btn btn-default">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </a>
                                    <a data-wysihtml5-command="insertOrderedList" class="btn btn-default">
                                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                    </a>
                                </div>
                            </div>
                            <textarea class="form-control" id="editor" name="announcement-content" cols="10" rows="5" placeholder="Description.">{{$dan_quote->danQuote or ''}}</textarea>
                            {{ Form::submit('Save', ['class' => 'btn btn-primary small-margin']) }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="recent-activity-container">
                <div class="activity-feed col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title white-text pull-left">Trending Topics</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            @foreach($forum['recent'] as $topic)
                                <?php $scrubbedTitle = preg_replace('/[^a-z\d\s]+/i', '', $topic->title); ?>
                                <p class="activity-item"><a class="purple-text" href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', $scrubbedTitle)}}/{{$topic->id}}">{{$topic->title}}</a>&nbsp;<span class="text-muted">( author: {{$topic->users->fname}}&nbsp;{{$topic->users->lname}} )</span></p>
                            @endforeach
                        </div>
                        <div class="panel-footer">
                            <a class="pull-right orange-text" href="{{ URL::route('forum.index') }}">View more topics&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                </div>
                <div class="activity-feed right-aligned col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title white-text pull-left">Upcoming Events</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            @foreach($events['upcoming_events'] as $event)
                                <?php $start = new \Carbon\Carbon($event->start_event); ?>
                                <p class="activity-item">
                                    <a class="purple-text" href="{{ URL::route('app.view.event', ['id' => $event->id, 'event' => cleanForId($event->title)]) }}">{{$event->title}}</a>&nbsp;
                                    <span class="text-muted">( starts on {{$start->format("F dS, Y")}} )</span>
                                </p>
                            @endforeach
                        </div>
                        <div class="panel-footer">
                            <a class="pull-right orange-text" href="{{ URL::route('events.index') }}">View more events&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="activity-feed col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title white-text pull-left">Recent Blog Posts</h3>
                            <a class="pull-right orange-text" href="http://www.pharmacyowners.com/blog">View more posts&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            {{-- generated from PDS blog rss feed --}}
                            <?php $max = $blog_posts->get_item_quantity(5); ?>
                            @for($i=0; $i < $max; $i++)
                                <?php $post = $blog_posts->get_item($i); ?>
                                <p class="activity-item">
                                    <a class="purple-text" href="{{$post->get_permalink()}}" target="_blank">{{$post->get_title()}}</a>
                                </p>
                            @endfor
                        </div>
                        <div class="panel-footer">
                            <a class="pull-right orange-text" href="http://www.pharmacyowners.com/blog">View more posts&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="activity-feed right-aligned col-lg-6 col-md-6 col-sm-6">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title white-text pull-left">New Resources</h3>
                            <div class="clearfix"></div>                            
                        </div>
                        <div class="panel-body resources-list">
                            @if(in_array('15', $permissions))
                                @foreach($newest_resources as $resource)
                                    <p class="activity-item">
                                        <a class="purple-text" href="{{ URL::route('app.preview.resource', $resource->id) }}">{{$resource->filename}}</a>&nbsp;
                                    </p>
                                @endforeach
                            @else 
                                <p class="activity-item">
                                    You don't have access to resources, click become member in the orange bar to learn more.
                                </p>
                            @endif
                        </div>
                        <div class="panel-footer">
                            <a class="pull-right orange-text" href="{{ URL::route('library.index') }}">View more resources&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>                            
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <h3 class="page-header"><a class="pull-right pointer" id="getProfile" href="{{ URL::to('user/'.$user->id.'/'.$user->fname.'-'.$user->lname.'#profile') }}"><span class="glyphicon glyphicon-pencil"></span> &nbsp;Edit My Profile</a></h3>
<!--    //reserved for teamwork api data
                <div class="activity-feed col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title white-text pull-left">Recent Messages</h3>
                            <a class="pull-right orange-text" href="{{ URL::route('library.index') }}">View more messages&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>                            
                        </div>
                        <div class="panel-body">
                            @foreach($newest_resources as $resource)
                                <p class="activity-item">
                                    <a class="purple-text">{{$resource->filename}}</a>&nbsp;
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="activity-feed right-aligned col-lg-6 col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title white-text pull-left">Upcoming Tasks</h3>
                            <a class="pull-right orange-text" href="{{ URL::route('library.index') }}">View more tasks&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></a>
                            <div class="clearfix"></div>                            
                        </div>
                        <div class="panel-body">
                            @foreach($newest_resources as $resource)
                                <p class="activity-item">
                                    <a class="purple-text">{{$resource->filename}}</a>&nbsp;
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
-->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@stop

@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    {{ HTML::script('js/notifications.js') }}
    @if(Session::has('Open Setup'))
        <script>
            $('#pharmacySetup').modal('show');
            $('.new-pharmacy-submit').show();
            $('.pharmacy-list').hide();
            $('.choose-employee-type').hide();
            $('.checking-for-existing-pharmacy').hide();
            $('.create-new-pharmacy').show();
            $('.setup-progress > div').removeClass('active');
            $('.setup-progress > div.step-three').addClass('active');
        </script>
    @endif

    @if(($user->companies === '0' || is_null($user->companies) || $user->companies === '"{}"' || $user->companies === '{}') && $user->account_type !== 2)
        {{ HTML::script('js/pharmacy-check.js') }}
    @endif
    
@stop
