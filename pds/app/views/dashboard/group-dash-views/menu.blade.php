{{-- new dynamic menu --}}

<?php $user_groups = explode(':', Auth::user()->groups); ?>
@if(in_array('5', $user_groups))
	<li><a href="{{ URL::route('acl.control') }}">Permissions, Containers & Owners</a></li>
	<li><a href="{{ URL::route('forum.manager.index') }}">Message Board Manager</a></li>
@endif
@if(in_array('1', $user_groups) || in_array('5', $user_groups))
	{{--<li><a href="#">Project Management</a></li>--}}
	<li><a href="{{ URL::route('accounts.index') }}">Account Manager</a></li>
@endif
@if(in_array('3', $user_groups))
	<li><a target="_blank" href="{{ URL::route('conference.signup') }}">Marketplace Listing</a></li>
	<li><a target="_blank" href="{{ URL::route('conference.vip-tickets') }}">Free Tickets</a></li>
	<li><a target="_blank" href="{{ URL::route('conference.choose.sponserships') }}">Sponsorship Opportunities</a></li>
	<li><a href="{{ URL::route('conference.get.faqsheet') }}">Exhibitor FAQ</a></li>
	<li><a href="{{ URL::route('conference.get.hours') }}">Conference Hours</a></li>
	<li><a href="{{ URL::route('conference.get.hotel') }}">Hotel Information</a></li>
@endif