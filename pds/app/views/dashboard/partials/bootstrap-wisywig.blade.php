<div class="btn-toolbar" data-role="{{$editor_data['editor_id']}}-toolbar" data-target="#{{$editor_data['editor_id']}}">
    <div class="btn-group">
        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a data-edit="fontSize 5" data-target="#{{$editor_data['editor_id']}}"><font size="5">Huge</font></a></li>
            <li><a data-edit="fontSize 3" data-target="#{{$editor_data['editor_id']}}"><font size="3">Normal</font></a></li>
            <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
        </ul>
    </div>
    <div class="btn-group">
        <a class="btn btn-default" data-edit="bold" data-original-title="Bold (Ctrl/Cmd+B)" title="Bold"><i class="fa fa-bold"></i></a>
        <a class="btn btn-default" data-edit="italic" data-original-title="Italic (Ctrl/Cmd+I)" title="Italic"><i class="fa fa-italic"></i></i></a>
        <a class="btn btn-default" data-edit="underline" data-original-title="Underline (Ctrl/Cmd+U)" title="Underline"><i class="fa fa-underline"></i></i></a>
    </div>
    <div class="btn-group">
        <a class="btn btn-default" data-edit="insertunorderedlist" data-original-title="Bullet list" title="Bullet list" data-target="#{{$editor_data['editor_id']}}"><i class="fa fa-list-ul"></i></a>
        <a class="btn btn-default" data-edit="insertorderedlist" data-original-title="Number list" title="Number list" data-target="#{{$editor_data['editor_id']}}"><i class="fa fa-list-ol"></i></a>
        <a class="btn btn-default" data-edit="outdent" data-original-title="Reduce indent (Shift+Tab)" title="Reduce indent" data-target="#{{$editor_data['editor_id']}}"><i class="fa fa-outdent"></i></a>
        <a class="btn btn-default" data-edit="indent" data-original-title="Indent (Tab)" title="Indent" data-target="#{{$editor_data['editor_id']}}"><i class="fa fa-indent"></i></a>
    </div>
    <div class="btn-group">
        <a class="btn btn-default" data-edit="justifyleft" data-original-title="Align Left (Ctrl/Cmd+L)" title="Align left"><i class="fa fa-align-left"></i></a>
        <a class="btn btn-default" data-edit="justifycenter" data-original-title="Center (Ctrl/Cmd+E)" title="Center"><i class="fa fa-align-center"></i></a>
        <a class="btn btn-default" data-edit="justifyright" data-original-title="Align Right" title="Center"><i class="fa fa-align-right"></i></a>
    </div>
<!--    <div class="btn-group open">
        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="fa fa-link"></i></a>
        <div class="dropdown-menu input-append">
            <input type="text" data-edit="createLink"/>
            <a class="btn" type="button">Add</a>
        </div>
        <a class="btn btn-default" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
    </div>-->
    <div class="btn-group">
        <a class="btn btn-default" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
        <a class="btn btn-default" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
    </div>
</div>
<div id="{{$editor_data['editor_id']}}" class="{{$editor_data['editor_class']}} editor">{{ $editor_data['old_input'] }}</div>
<textarea class="form-control body-description hidden {{$editor_data['editor_id']}}" id="hidden-editor" name="{{ $editor_data['input_name'] }}"></textarea>


<script class="hidden">
    //initialize editor
    $('<?php echo '.'.$editor_data['editor_class']; ?>').wysiwyg({ toolbarSelector: '[data-role=<?php echo $editor_data['editor_id']; ?>-toolbar]'});
    //need to bring in form class and wisywig editor id
    $(document).ready(function() {
        $('<?php echo '.'.$editor_data['form_class']; ?>').submit(function(event) {
            $('<?php echo '.'.$editor_data['form_class'].' .'.$editor_data['editor_id']; ?>').val($('<?php echo '.'.$editor_data['editor_class']; ?>').cleanHtml());
        });
        $('#editor').on('focus', function(){
            if($(this).text() == 'Include a description for your topic that engages the user\'s thoughts and provokes them to join the conversation.'){
                $(this).html('');
            }
        });
        $('a[title]').tooltip({container:'body'});
    	$('.dropdown-menu input').click(function() {return false;})
		    .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
        .keydown('esc', function () {this.value='';$(this).change();});

        $("#<?php echo $editor_data['editor_id']; ?>").on('paste',function(e){
            e.preventDefault();
            console.log('what');
            var text = (e.originalEvent || e).clipboardData.getData('text/plain') || prompt('Paste something..');
            document.execCommand('insertText', false, text);
        });
    });
</script>