          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Pharmacy</th>
                  <th>Edit</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ Auth::user()->fname }}</td>
                  <td>{{ Auth::user()->lname }}</td>
                  <td>{{ Auth::user()->email }}</td>
                  <td>{{ Auth::user()->pharmacy }}</td>
                  <td><a id="getProfile" data-toggle="modal" data-target="#changeBasicInfo"><span class="glyphicon glyphicon-pencil"></span></a></td>
                </tr>
              </tbody>
            </table>
          </div>