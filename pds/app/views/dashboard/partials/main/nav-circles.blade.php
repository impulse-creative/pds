            {{-- 
            <div class="col-xs-6 col-sm-3">
              <img data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4><a href="{{ URL::to('marketplace/company', array(Auth::id())) }}?company={{ $vendor->company_name }}">Profile</a></h4>
              <span class="text-muted">View your marketplace profile</span>
            </div> --}}
            <?php $percent = $vendor['percentage']; ?>

            {{-- Click to sign up for the pds conference --}}
            <a href="{{ URL::route('conference.signup') }}" target="_blank">
              <div class="col-xs-10 col-sm-3 center signup-box dash-icon">
                <div class="progress progress-move" >
                  <div class="progress-bar progress-move @if($percent >= 100) progress-bar-success @elseif($percent < 30) progress-bar-danger @else progress-bar-warning @endif" role="progressbar" aria-valuenow="{{ $percent }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percent }}%">
                    <span>@if($percent >= 100) 100% @else {{ $percent }}% @endif Complete</span>
                  </div>
                </div>
                <img src="//cdn2.hubspot.net/hub/37772/file-1791650016-png/Version_2/conference-signup.png" class="img-responsive" alt="Signup for PDS15">
                <h4>Submit Your Company Info</h4>
                <span class="text-muted">Marketplace, 100-word Description, Free Giveaway Tickets, etc. </span>
              </div>
            </a>

            {{-- Click to jump to the VIP Ticket Giveaway --}}
            <a href="{{ URL::route('conference.vip-tickets') }}" target="_blank">
              <div class="col-xs-10 col-sm-3 placeholder-no-progress vip-box dash-icon">
                <img src="//cdn2.hubspot.net/hub/37772/file-1791301107-png/Version_2/vip-tickets.png" class="img-responsive" alt="VIP Tickets">
                <h4>Give Away Your Free Conference Ticket</h4>
                <span class="text-muted">Invite your clients to the Conference</span>
              </div>
            </a>

            {{-- Click to jump to the PDS Conference sponsorship opportunities --}}
            <a href="{{ URL::route('conference.choose.sponserships') }}" target="_blank">
              <div class="col-xs-10 col-sm-3 placeholder-no-progress sponsor-box dash-icon">
                <img src="//cdn2.hubspot.net/hub/37772/file-1791644976-png/Version_2/sponsorship-opps.png" class="img-responsive" alt="Sponsorship Opportunities">
                <h4>Sponsorship Opportunities</h4>
                <span class="text-muted">Let Your Brand Be Known</span>
              </div>
            </a>