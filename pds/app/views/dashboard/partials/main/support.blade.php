@extends('layouts.pds-advantage')


@section('head')
  @parent
  {{-- HTML::style('css/main-styles.css') --}}
@stop

@if($menu_data)
    @section('menu')
        @include($menu_data)
    @stop
@endif

@section('content')
	@parent
<div class="row-fluid">
    <div class="col-md-9  pass-reset-area">        
        <h1>Support</h1>
        <h2>Conference Support</h2>
        <p><a href="mailto:carole@pharmacy-owners.com">Carole Bebout</a> @ <a href="tel:5612752637">(561) 275-2637</a></p>
        <p><a href="mailto:koreen@pharmacy-owners.com">Koreen Leavitt</a> @ <a href="tel:5612752659">(561) 275-2659</a></p>
        <h3>Technical Support</h3>
        <p>Coming Soon....</p>
        @include('Signup.2015-Conference.progress-partials.faq-data')
        
    </div>
    <div class="col-md-3">  
        
    </div>
</div>
@stop

@section('extra-js')
  {{ HTML::script('js/vendor-signup.js') }}
@stop