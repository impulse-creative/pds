{{-- Setup Pharmacy --}}
<div class="modal fade" id="pharmacySetup" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Setup Your Pharmacy</h4>
                <div class="setup-progress">
                    <div class="col-md-2 step-one center active">
                        <p>1</p>
                    </div>
                    <div class="col-md-2 step-two center">
                        <p>2</p>
                    </div>
                    <div class="col-md-2 step-three center">
                        <p>3</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-body">
                    @if(Session::has('False Admin'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $value = Session::pull('False Admin', 'default') }}</div>
                    @endif
                    <div class="checking-for-existing-pharmacy">
                        <div class="choice-question small-padding">
                            <h4>Thank you, {{$sections['user']->fname}}. We are going to see if your pharmacy is already in the system. Please fill out the info below and hit next.</h4>
                        </div>
                        <div class="form-group">
                            <label for="pharmacy_name" class="col-sm-2 control-label">Pharmacy Name</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="pharmacy_name" placeholder="Pharmacy Name" name="pharmacy_name">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="pharmacy_address" class="col-sm-2 control-label">Pharmacy Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pharmacy_address" placeholder="Pharmacy Address" name="pharmacy_address">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="pharmacy_phone" class="col-sm-2 control-label">Pharmacy Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pharmacy_phone" placeholder="Pharmacy Phone Number" name="pharmacy_phone">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="pharmacy_phone" class="col-sm-2 control-label">Pharmacy Owner Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="owner_email" placeholder="Pharmacy Owner Email" name="owner_email">
                                <p class="help-block">*Ask the individual who created the company owner account on PDS.</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="center">
                            <a class="btn btn-default back-to-employee-choice"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                            <a class="btn btn-default go-to-company-list">Next&nbsp;<i class="fa fa-arrow-right"></i></a>
                        </div>
                        
                    </div>
                    <div class="clearfix"></div>
                    <div class="pharmacy-list">
                        <div class="choice-question center">
                            <h4>We found these Pharmacies that match your pharmacy<br>Please click on your pharmacy</h4>
                            <p class="text-muted">If not, then please click "Create A New Pharmacy" at the bottom.</p>
                        </div>
                        <div class="list-group">
                            <div class="spinner">
                                <div class="rect1"></div>
                                <div class="rect2"></div>
                                <div class="rect3"></div>
                                <div class="rect4"></div>
                                <div class="rect5"></div>
                            </div>
                        </div>
                        <div class="center">
                            <a class="btn btn-default back-to-pharmacy-check"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                            <a class="btn btn-primary none-are-me"><i class="fa fa-hospital-o fa-lg"></i>&nbsp;Create a New Pharmacy</a>
                        </div>
                    </div>
                    {{ Form::open(['route' => 'store.company', 'class' => 'register-pharmacy']) }}
                    <div class="choose-employee-type">
                        <div class="intro-text">
                            <p class="alert alert-info"><i class="fa fa-info-circle fa-lg"></i>&nbsp;It appears your account isn't associated with a company in our system yet. Please fill out the following forms, it will just take a moment of your time and we won't bother you about it again.</p>
                        </div>
                        <div class="choice-question">
                            <h4>Define your role, please.</h4>
                        </div>
                        <div class="radio">
                            <?php $employee_type = Input::old('employee_type'); ?>
                            <label class="choice">
                                <?php $checked = isset($employee_type) ? true : false; ?>
                                {{ Form::radio('employee_type', 1, $checked, ['class' => 'employee_type']) }}
                                I am a pharmacy owner...
                            </label>
                        </div>
                        <div class="radio">
                            <label class="choice">
                                <?php $checked = isset($employee_type) ? true : false; ?>
                                {{ Form::radio('employee_type', 1, $checked, ['class' => 'employee_type']) }}
                                I am pharmacy manager...
                            </label>
                        </div>
                        <div class="radio">
                            <label class="choice">
                                <?php $checked = isset($employee_type) ? true : false; ?>
                                {{ Form::radio('employee_type', 2, $checked, ['class' => 'employee_type']) }}
                                I am a pharmacy employee...
                            </label>
                        </div>
                        <div class="radio">
                            <label class="choice">
                                <?php $checked = isset($employee_type) ? true : false; ?>
                                {{ Form::radio('employee_type', 0, $checked, ['class' => 'employee_type']) }}
                                I am a vendor...
                            </label>
                        </div>
                    </div>
                    <div class="create-new-pharmacy">
                        <div class="choice-question small-padding center">
                            <h4>Fill out the form below to register your company with PDSadvantage</h4>
                        </div>
                        <div class="form-group @if($errors->has('name')) has-error @endif">
                            <label for="pharmacy_name" class="col-sm-2 control-label">Pharmacy</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pharmacy_name" placeholder="Pharmacy Name" name="pharmacy_name" value="{{ Input::old('pharmacy_name') }}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group @if($errors->has('address')) has-error @endif">
                            <label for="pharmacy_address" class="col-sm-2 control-label">Pharmacy Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pharmacy_address" placeholder="Pharmacy Address" name="pharmacy_address" value="{{ Input::old('pharmacy_address') }}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group @if($errors->has('phone')) has-error @endif">
                            <label for="pharmacy_phone" class="col-sm-2 control-label">Pharmacy Phone</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pharmacy_phone" placeholder="Pharmacy Phone Number" name="pharmacy_phone" value="{{ Input::old('pharmacy_phone') }}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group @if($errors->has('owner_name')) has-error @endif">
                            <label for="owner_name" class="col-sm-2 control-label">Pharmacy Owner's Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="owner_name" placeholder="Pharmacy Owner Name" name="owner_name" value="{{ Input::old('owner_name') }}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group @if($errors->has('owner_email')) has-error @endif">
                            <label for="owner_email" class="col-sm-2 control-label">Pharmacy Owner's Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="owner_email" placeholder="Pharmacy Owner Email" name="owner_email" value="{{ Input::old('owner_email') }}">
                                @if($errors->has('owner_email'))<p class="help-block"> {{ $errors->first('owner_email') }}</p>@endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group @if($errors->has('domain_name')) has-error @endif">
                            <label for="pharmacy_site" class="col-sm-2 control-label">Pharmacy Website</label>
                            <div class="col-sm-10">
                                <input type="url" class="form-control" id="pharmacy_site" placeholder="Pharmacy Website" name="pharmacy_site" value="{{ Input::old('pharmacy_site') }}">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group  @if($errors->has('state') || $errors->has('city') || $errors->has('postal_code')) has-error @endif">
                            <div class="col-sm-3">
                                <label for="pharmacy_state" class="col-sm-2 control-label" style="padding-left: 0px;">State</label>
                                {{ Form::select('pharmacy_state', $event_states, Input::old('pharmacy_state'), ['class' => 'form-control']) }}
                                @if($errors->has('state'))<p class="help-block"> {{ $errors->first('state') }}</p>@endif
                            </div>
                            <div class="col-sm-6">
                                <label for="pharmacy_city" class="col-sm-2 control-label" style="padding-left: 0px;">City</label>
                                <input type="text" class="form-control" id="pharmacy_city" placeholder="Pharmacy City" name="pharmacy_city" value="{{ Input::old('pharmacy_city') }}">
                                @if($errors->has('city'))<p class="help-block"> {{ $errors->first('city') }}</p>@endif
                            </div>
                            <div class="col-sm-6">
                                <label for="pharmacy_zip" class="col-sm-2 control-label" style="padding-left: 0px;">Zipcode</label>
                                <input type="text" class="form-control" id="pharmacy_zip" placeholder="Pharmacy Zipcode" name="pharmacy_zip" value="{{ Input::old('pharmacy_zip') }}">
                                @if($errors->has('postal_code'))<p class="help-block"> {{ $errors->first('postal_code', 'The zipcode field is required.') }}</p>@endif
                            </div>
                        </div>
                        <div class="clearfix"></div><br/>
                        <div class="center">
                            <a class="btn btn-default go-to-company-list"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                            {{ Form::submit('Create Pharmacy', array('class' => 'btn btn-primary fix-button')) }}
                        </div>
                        
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closePharmacySetup" data-dismiss="modal">Close</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>