<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
                <img src="{{ asset('images/application/email-header.png') }}" alt="Pharmacy Development Services Logo" />
		<h2>Password Reset Request</h2>

		<div>
                    <h3>You are receiving this email because you requested a password reset for the email address: {{ $email['email'] }}.</h3>
                    <p style="font-size: 18px;">To reset your password, follow the instructions on the following page: <a href="{{ URL::to('retrieve/password/confirmation', array($token)) }}">Reset My Password</a>. 
                        This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes from when this email was sent. </p>

                    <p style="font-size: 18px;">
		</div>
	</body>
</html>