<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
            <table width="600">
                <tr>
                    <td>
                        <img src="{{ asset('images/application/email-header.png') }}" alt="Pharmacy Development Services Logo" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Welcome to PDSadvantage, {{ $name }}!

                        <p>To confirm your account, please click this link: <a href="{{ URL::to('confirm/email', array($token, $id)) }}">Confirm Email Address</a></p>

                        <p>Once confirmed, you’ll have access into your brand new PDSadvantage!</p>

                        <p>A few things to help you get started: </p>
                        <ul>
                        <li>Your new dashboard is your homepage for all the latest happenings in PDSadvantage</li>
                        <li>Visit our Message Boards and join in the conversation</li>
                        <li>Check out our latest events and trainings in the Events section </li>
                        </ul>
                        <p>We’re excited for you to join us. If you have any questions or concerns please contact JoAnn Curry at <a href="tel:561-275-2651">561-275-2651</a>.</p>
                        <p>- PDS team</p>
                    </td>
                    <td>
                        <strong>Pharmacy Development Services | 2459 S. Congress Ave, #204, Palm Springs, FL</strong><br>
                        You received this email because you signed up on App.Pharmacyowners.com from Pharmacy Development Services.<br> This is a one time email.<br><br>
                    </td>
                </tr>
            </table>
	</body>
</html>