<?php
    $name = $user['fname'].' '.$user['lname'];
    $count = 1;
    ?>

	<head>
		<meta charset="utf-8">
	</head>
	<body style="font-family:Tahoma;background:#EFEFEF;">
            <table width="580" style="font-family:Tahoma;margin: 0 auto; background: #fff;" celpadding="5">
                <tr>
                    <td>
                        <img src="{{ asset('images/application/email-header.png') }}" alt="Pharmacy Development Services Logo" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-family:Tahoma;padding:10px;width: 100%;">
                            <h2 style="font-size:15px;">Hello {{ $name }}, here is your daily digest for {{ (new \DateTime())->format("F dS, Y") }}.</h2>
                            
                        </div>
                        @if($totalNotifications != 0)
                            <div style="text-align:right; padding:10px 20px;">
                                <a style="color:#F58220;font-weight:bold;" href="{{ URL::to('dashboard') }}">Take me to my {{ $totalNotifications }} notification{{ $plural = $totalNotifications > 1 ? 's' : '' }}</a>
                            </div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="560" style="font-family:Tahoma;margin: 0 auto; background: #fff;" celpadding="5">
                            <tr style="font-family:Tahoma;background:#412985; color: #fff;" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:18px;float:left; padding-left: 10px;">Active Topics</h2>
                                </td>
                            </tr>
                            <?php $activeTopicCount = 0; ?>
                            @foreach($activeTopics as $topic)
                            <?php $messageCount = \ForumMessages::where('parent_topic', $topic->id)->count(); 
                                $page =  ceil($messageCount / 10);
                                $lastReply = $topic->messages->last() != null ? $topic->messages->last()->id : '';
                            ?>
                            <tr style="font-family:Tahoma;background:{{ $activeTopicCount % 2 == 1 ? '#fff' : '#e2e2e2' }};" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:16px;padding: 10px 10px 5px 10px; margin:0;">
                                        <a style="font-family:Tahoma;color:#412985;text-decoration:none;" href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', preg_replace('/[^a-z\d\s]+/i', '', $topic->title))}}/{{$topic->id}}?page={{$page}}#{{$lastReply}}">
                                            {{$topic->title}}
                                        </a>
                                    </h2>
                                    <p style="font-family:Tahoma;color:#000;padding: 0 10px 5px 10px;margin:0;">{{ count(explode(' ', strip_tags($topic->description))) > 40 ?  Str::words(strip_tags($topic->description), 32).'...' : strip_tags($topic->description) }} <span style="font-family:Tahoma;color:#777;">- posted by {{ $topic->users->fname.' '.$topic->users->lname }}.</p>
                                    @if($topic->messages->count() > 0)
                                        <div style="font-family:Tahoma;padding: 5px 10px 0 15px; margin-bottom: 5px;">
                                            <hr>
                                            Latest reply: <span style="font-family:Tahoma;color:#777;"> {{ $topic->messages->last()->users->fname.' '.$topic->messages->last()->users->lname }}</span><br>
                                            <p> {{ count(explode(' ', strip_tags($topic->messages->last()->data))) > 40 ?  Str::words(strip_tags($topic->messages->last()->data), 32).'...' : strip_tags($topic->messages->last()->data) }}</p>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                            <?php $activeTopicCount++; ?>
                            @endforeach
                            <tr cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;color:#777;font-size:14px;float:right;padding-right:10px;"><a style="font-family:Tahoma;color:#F58220;text-decoration:none;" href="{{ URL::route('forum.index') }}">View more topics</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        <div style="margin: 15px 0;">
                            <!--HubSpot Call-to-Action Code -->
                            <span class="hs-cta-wrapper" id="hs-cta-wrapper-e50f9331-76e7-4a60-a3f6-3ea7b9cf272e">
                                <span class="hs-cta-node hs-cta-e50f9331-76e7-4a60-a3f6-3ea7b9cf272e" id="hs-cta-e50f9331-76e7-4a60-a3f6-3ea7b9cf272e">
                                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                                    <a href="http://cta-redirect.hubspot.com/cta/redirect/37772/e50f9331-76e7-4a60-a3f6-3ea7b9cf272e" ><img class="hs-cta-img" id="hs-cta-img-e50f9331-76e7-4a60-a3f6-3ea7b9cf272e" style="font-family:Tahoma;border-width:0px;" src="https://no-cache.hubspot.com/cta/default/37772/e50f9331-76e7-4a60-a3f6-3ea7b9cf272e.png"  alt="Daily Digest CTA 2"/></a>
                                </span>
                                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                                <script type="text/javascript">
                                    hbspt.cta.load(37772, 'e50f9331-76e7-4a60-a3f6-3ea7b9cf272e');
                                </script>
                            </span>
                            <!-- end HubSpot Call-to-Action Code -->
                        </div>

                    </td>
                </tr>
                
                <tr>
                    <td>
                        <table width="560" style="font-family:Tahoma;margin: 0 auto; background: #fff;" celpadding="5">
                            <tr style="font-family:Tahoma;background:#412985; color: #fff;" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:18px;float:left; padding-left: 10px;">New Message Board Topics</h2>
                                </td>
                            </tr>
                            <?php $newTopicCount = 0; ?>
                            @foreach($newTopics as $topic)
                            <?php $messageCount = \ForumMessages::where('parent_topic', $topic->id)->count(); 
                                $lastReply = $topic->messages->last() != null ? $topic->messages->last()->id : '';
                                $page = ceil($messageCount / 10);
                            ?>
                            <tr style="font-family:Tahoma;background:{{ $newTopicCount % 2 == 1 ? '#fff' : '#e2e2e2' }};" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:16px;padding: 10px 10px 5px 10px; margin:0;">
                                        <a style="font-family:Tahoma;color:#412985;text-decoration:none;" href="{{ URL::to('message-boards') }}/{{preg_replace('/ /', '-', preg_replace('/[^a-z\d\s]+/i', '', $topic->title))}}/{{$topic->id}}?page={{$page}}#{{$lastReply}}">
                                            {{$topic->title}}
                                        </a>
                                    </h2>
                                    <p style="font-family:Tahoma;color:#000;padding: 0 10px 10px 10px;margin:0;">{{ count(explode(' ', strip_tags($topic->description))) > 40 ?  Str::words(strip_tags($topic->description), 32).'...' : strip_tags($topic->description) }} <span style="font-family:Tahoma;color:#777;">- posted by {{ $topic->users->fname.' '.$topic->users->lname }}.</p>
                                    @if($topic->messages->count() > 0)
                                        <div style="font-family:Tahoma;padding: 5px 10px 0 15px; margin-bottom: 5px;">
                                            <hr>
                                            Latest reply: <span style="font-family:Tahoma;color:#777;"> {{ $topic->messages->last()->users->fname.' '.$topic->messages->last()->users->lname }}</span><br>
                                            <p> {{ count(explode(' ', strip_tags($topic->messages->last()->data))) > 40 ?  Str::words(strip_tags($topic->messages->last()->data), 32).'...' : strip_tags($topic->messages->last()->data) }}</p>
                                        </div>
                                    @endif
                                    
                                    
                                </td>
                            </tr>
                            <?php $newTopicCount++; ?>
                            @endforeach
                            <tr cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;color:#777;font-size:14px;float:right;padding-right:10px;"><a style="font-family:Tahoma;color:#F58220;text-decoration:none;" href="{{ URL::route('forum.index') }}">View more topics</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        
                        

                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="560" style="font-family:Tahoma;margin: 0 auto; background: #fff;" celpadding="5">
                            <tr style="font-family:Tahoma;background:#412985; color: #fff;" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:18px;float:left; padding-left: 10px;">Upcoming Events</h2>
                                </td>
                            </tr>
                           <?php $eventCount = 0; ?>
                            @foreach($events as $event)
                            <?php $start = new \Carbon\Carbon($event->start_event); ?>
                            
                            <tr style="font-family:Tahoma;background:{{ $eventCount % 2 == 1 ? '#fff' : '#e2e2e2' }};" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:16px;padding: 10px 10px 5px 10px; margin:0;">
                                        <a style="font-family:Tahoma;color:#412985;text-decoration:none;" href="{{ URL::route('app.view.event', ['id' => $event->id, 'event' => strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '_', ($event->title))))]) }}">
                                            {{ $event->title }}
                                        </a> <br>
                                        <span style="font-family:Tahoma;color:#777;font-size:13px;">{{$start->format("F dS, Y")}}</span>
                                    </h2>
                                    <p style="font-family:Tahoma;color:#000;padding: 0 10px 10px 10px;margin:0;">{{ strip_tags($event->description, '<li><ul></li></ul><br><br/><br />') }} </p>
                                </td>
                            </tr>
                            <?php $eventCount++; ?>
                            @endforeach
                            <tr cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;color:#777;font-size:14px;float:right;padding-right:10px;"><a style="font-family:Tahoma;color:#F58220;text-decoration:none;" href="{{ URL::route('events.index') }}">View more events</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="560" style="font-family:Tahoma;margin: 0 auto; background: #fff;" celpadding="5">
                            <tr style="font-family:Tahoma;background:#412985; color: #fff;" cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:18px;float:left; padding-left: 10px;">Recently Updated Resources</h2>
                                </td>
                            </tr>
                           <?php $resourceCount = 0; ?>
                            @foreach($resources as $resource)
                            @if($resource->is_forum_attachement != 1)
                                <tr style="font-family:Tahoma;background:{{ $resourceCount % 2 == 1 ? '#fff' : '#e2e2e2' }};" cellpadding ="5">
                                    <td>
                                        <h2 style="font-family:Tahoma;font-size:16px;padding: 10px 10px 5px 10px; margin:0;">
                                            <a style="font-family:Tahoma;color:#412985;text-decoration:none;" href="{{ URL::route('app.preview.resource', $resource->id) }}">
                                                {{ $resource->filename }}
                                            </a>
                                        </h2>
                                    </td>
                                </tr>
                                <?php $eventCount++; ?>
                            @endif
                            @endforeach
                            <tr cellpadding ="5">
                                <td>
                                    <h2 style="font-family:Tahoma;color:#777;font-size:14px;float:right;padding-right:10px;"><a style="font-family:Tahoma;color:#F58220;text-decoration:none;" href="{{ URL::route('library.index') }}">View more resources</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @if($danQuote != '')
                <tr>
                    <td>
                        <table width="560" style="font-family:Tahoma;margin: 0 auto; background: #fff;" celpadding="5">
                            <tr style="font-family:Tahoma;background:#fff; color: #000;" cellpadding ="5">
                                <td cellpadding="5">
                                    <div style="padding:10px">
                                        <img src="{{ URL::to('images/application/dan-benemoz-fullbody.jpg') }}" />
                                    </div>
                                    
                                </td>
                                <td>
                                    <h2 style="font-family:Tahoma;font-size:16px; color:#F58220">
                                        Directly From Dan
                                    </h2>
                                    <div style="padding:10px">
                                        {{ $danQuote->danQuote }}
                                    </div>
                                            
                                                
                                            
                                </td>
                            </tr>
                        </table>
                            
                        </div>
                        
                    </td>
                </tr>
                @endif
                
                
                <tr>
                    <td>
                        <p style="font-family:Tahoma;text-align:center;font-size:11px;padding:10px;"><strong>Pharmacy Development Services | 2459 S. Congress Ave, #204, Palm Springs, FL</strong><br>
                        You received this email because you signed up on App.Pharmacyowners.com from Pharmacy Development Services. <br>If you would like to <a href="{{URL::to('user/'.$user->id.'/'.$user->fname.'-'.$user->lname.'#profile')}}">unsubscribe or manage your preferences click here</a>
                    
                    </td>
                </tr>
            </table>
        </body>
    </html>
