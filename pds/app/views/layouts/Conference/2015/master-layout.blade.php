<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>pdsAdvantage</title>
    @section('head')
    {{-- PDS shortcut icon --}}
    <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
    <!-- Latest compiled JQuery - minified -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    {{-- HTML::script('js/jquery-1.11.1.min.js') --}}
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- Latest compiled and minified CSS/JS -->
    {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}
    {{ HTML::style('js/jquery-ui/jquery-ui.min.css') }}
        
    <!-- Custom CSS -->
    {{ HTML::style('css/Conference/2015/conference-styles.css') }}
    
    @show
    
</head>
<body>
	<header class="conference-header-container purple-background">
		@include('Signup.2015-Conference.header')
	</header>

	<div class="content">
        @yield('content')
	</div>
    <div class="clearfix"></div>
	
    <footer class="conference-footer-container">
        @section('extra-js')
        @show
		@include('Signup.2015-Conference.footer')
	</footer>
    <script src="{{ URL::to('/js/global.js') }}"></script>
</body>
</html>