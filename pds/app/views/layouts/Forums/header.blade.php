{{-- Main Header --}}
<?php 
    $permissions = explode(':', $sections['permissions']);
    $user = $sections['user'];
    $name = $sections['name'];      
    $secret = in_array(5, $permissions) ? '<div id="adminMenu"><a href="#" id="makeMagic"><i class="fa fa-cogs"> </i></a></div>' : '';
?>
<div class="forum-header {{ $background = in_array('2', $permissions) ? 'purple' : 'orange' }}-background">
    	<div class="container">
            {{$secret}}
		<div class="mini-acc pull-right">
                    @if(!in_array('2', $permissions))
                    <div class="signup-cta pull-right vcenter">
			<a href="http://www.pharmacyowners.com/free-consultation" target="_blank" class="btn btn-default pull-right">Become a Member</a>
                    </div>
                    @endif
                    <div class="mini-profile">
                        <p><a href="{{ URL::to('/user/'.$sections['user']['id'].'/'.$sections['profile_url']) }}">Hi, {{$name}}</a>!			
                            &nbsp;<a href="{{ URL::to('dash.main') }}" class="vcenter">Dashboard <span class="badge">{{ $total = $sections['notifications'] != 0 ? $sections['notifications'] : '' }}</span></a>
                                @if($sections['getting-started'] != 0)
                                    &nbsp;<a href="{{ URL::to('getting-started') }}" class="vcenter">Getting Started <span class="badge">{{$sections['getting-started']}}</span></a>
                                @endif
                                <a href="{{ URL::to('/user/'.$sections['user']['id'].'/'.$sections['profile_url']) }}">
                                    {{ $image = User::getBioImage(Auth::id(), 'vcenter forum-mini-profile-img') }}
                                </a>
                                <a href="{{ URL::to('dashboard/logout') }}"><span class="glyphicon glyphicon-log-out"></span></a>
                    </div>
		</div>
		<div class="clearfix"></div>
		<div class="forum-logo small-padding col-lg-3 col-md-3 vcenter">

			<a href="{{ URL::route('dash.main') }}"><img src="{{ asset('images/application/pds-white-logo-small.png') }}" alt="PDS"/>

		</div><!--
		--><div class="forum-nav small-padding col-lg-9 vcenter">
			<ul>
				<li class="nav-item">
					<a href="{{ URL::route('forum.index') }}" class="nav-link">Message Boards</a>
				</li>
				<li class="nav-item">
					<a href="{{ URL::route('events.index') }}" class="nav-link">Events</a>
				</li>
				<li class="nav-item">
					<a href="{{ URL::route('library.index') }}" class="nav-link">Resource Library</a>
				</li>
				<li class="nav-item">
					<a href="https://mypds.pharmacyowners.com/" target="_blank" class="nav-link">Projects</a>
				</li>
				<li class="nav-item">
					<a href="http://pdsadvantage.uservoice.com" target="_blank" class="nav-link">Support</a>
				</li>
			</ul>
		</div>
	</div>
</div>
