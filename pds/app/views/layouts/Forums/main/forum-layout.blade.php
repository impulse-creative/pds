<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>Pharmacy Development Services | Message Board and Friends</title>
        @section('head')

        <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
        <!-- Latest compiled and minified CSS/JS -->
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}
        {{ HTML::style('css/slicknav.css') }}
        
        <!-- Custom CSS -->
        {{ HTML::style('css/main-styles.css') }}
        {{-- js below helps to keep webkit columns supported web browsers < IE 10 --}}
        <!--[if lt IE 10]>
              {{ HTML::script('js/css-multi-col.js') }}
        <![endif]-->
        
        @show
 
    </head>
    <body class="message-boards-pages">
        <div class="page-wrap">
            {{-- layout header section --}}
        	@include('layouts.Forums.header')
            <div class="clearfix"></div>
            <div id="cog-menu" class="container">
                <ul id="dash-nav" class="nav nav-sidebar">
                    <li><a href="{{ URL::route('dash.main') }}">Dashboard</a></li>
                    @include('dashboard.group-dash-views.menu')
                    <li><a href="{{ URL::route('password.remind') }}">Settings</a></li>
                    <li><a href="{{ URL::route('dash.logout') }}">Logout</a></li>
                </ul>
            </div>
            
            {{-- layout content section --}}
        	
        		@yield("content")
        	

            {{-- layout footer section --}}
        	@include("layouts.Forums.footer")

            {{-- Custom JQuery Scripts | Global Scripts | loading in footer for faster load times --}}
            @section('extra-js')
                <!-- Latest compiled JQuery - minified -->
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                {{ HTML::script('js/jquery.slicknav.min.js') }}
                <script style="display: none;">
                    $(function(){
                        $('#menu').slicknav();
                        if (!window.location.origin)
                            window.location.origin = window.location.protocol+"//"+window.location.host+'/';
                        $('.slicknav_menu > a').before('<a class="mobile-logo" href="http://www.pharmacyowners.hs-sites.com/"><img src="'+ window.location.origin +'/images/application/pds-forum-logo.png" /></a>');
                    });
                </script>
                {{ HTML::script('js/script.js') }}
                {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
                <script type="text/javascript">
                $(function () {
                    $("[rel='tooltip']").tooltip({
                        placement: 'top',
                        trigger: 'hover focus',
                    });
                    $(document).on('focus', '[rel=tooltip]', function () { 
                        $(this).tooltip('show'); });
                });
                </script>
            @show
        </div>
        <script src="{{ URL::to('/js/global.js') }}"></script>      
    </body>
</html>