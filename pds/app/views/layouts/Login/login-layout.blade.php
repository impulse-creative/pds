<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>Pharmacy Development Services | World's Best Vendor to Vendors</title>
        @section('head')
        <!-- Latest compiled JQuery - minified -->
        {{ HTML::script('js/jquery-1.11.1.min.js') }}
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
        <!-- Latest compiled and minified CSS -->
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}

        <!-- Latest compiled and minified JavaScript -->
        {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
        <script type='text/javascript' src="http://imsky.github.io/holder/holder.js"></script>
        {{ HTML::script('js/jquery.session.js') }}
        
        <!-- Custom CSS -->
        {{ HTML::style('css/main-styles.css') }}
        @foreach($css as $style)
        {{ HTML::style('css/'.$style.'-styles.css') }}
        @endforeach
        
        
        
        <!-- Custom JQuery Scripts | Global Scripts -->
        {{ HTML::script('js/custom-scripts.js') }}
        <script src="{{ URL::to('/js/global.js') }}"></script>
        @show
 
    </head>
    <!-- BODY CONTAINS RESPONSIVE BACKGROUND IMAGE -->
    <body class="login-body">
    	@include("Login.header")
    	<div class="container-fluid content">
    		@yield("content")
    	</div>
    	@include("Login.footer")
        <script src="{{ URL::to('/js/global.js') }}"></script>      
    </body>
</html>