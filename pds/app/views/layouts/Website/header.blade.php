
<ul id="menu">
    <li class="nav-item">
        <a class="nav-link">What We Do</a>
        <ul>
            <li class="sub-item"><a class="nav-link" href="http://pharmacyowners.hs-sites.com/what-we-do">What We Do</a></li>
            <li class="sub-item"><a class="nav-link" href="http://pharmacyowners.hs-sites.com/lead">Lead</a></li>
            <li class="sub-item"><a class="nav-link" href="http://pharmacyowners.hs-sites.com/innovate">Innovate</a></li>
            <li class="sub-item"><a class="nav-link" href="http://pharmacyowners.hs-sites.com/connect">Connect</a></li>
            <li class="sub-item"><a class="nav-link" href="http://pharmacyowners.hs-sites.com/membership">Membership</a></li>
        </ul>
    </li>
    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/who-we-help" class="nav-link">Who We Help</a></li>
    <li class="nav-item">
        <a class="nav-link" href="http://pharmacyowners.hs-sites.com/training">Training</a> 
        <ul>
            <li class="sub-item"><a class="nav-link" href="http://pharmacyowners.hs-sites.com/training">Training</a></li>
            <li class="sub-item"><a href="http://pharmacyowners.hs-sites.com/training?type=webinar">Webinars</a></li> 
            <li class="sub-item"><a href="http://pharmacyowners.hs-sites.com/training?type=regional">Regional</a></li> 
            <li class="sub-item"><a href="http://pharmacyowners.hs-sites.com/training?type=on-site">On-Site</a></li> 
            <li class="sub-item"><a href="http://pharmacyowners.hs-sites.com/training?type=members-only">Members Only</a></li> 
        </ul>
    </li>
    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/our-story" class="nav-link">About</a></li>
    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/contact-pharmacy-development-services" class="nav-link">Contact</a></li>
    <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/new-pds-blog" class="nav-link">Blog</a></li>
    @if(Auth::check())
    {{-- <li class="nav-item"><a class="orange-text" href="{{ URL::route('dash.main') }}">Dashboard</a></li> --}}
@endif
</ul>


<div class="white-pre-header small-padding">
    <div class="page-center small-padding">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <ul>
                <li class="white-header-item"><a href="http://www.pdsconference.com/" class="pds-conference-cta">#PDS15 Conference</a></li>
                <li id="pds-phone" class="white-header-item"><a href="tel:800-987-7386" class="fix-menu">800-987-7386</a></li>
                <li class="white-header-item"><a href="https://rxsolutioncenter.com/SignIn.aspx?ReturnUrl=%2fLibrary.aspx/" class="fix-menu" target="_blank">Library</a></li>
                <li class="white-header-item"><a href="https://rxsolutioncenter.com/newsignup.aspx" target="_blank" class="fix-menu">Message Board</a></li>
                
                <li class="white-header-item"><a href="https://rxsolutioncenter.com/" target="_blank" class="fix-menu">Solution Center</a></li>
                
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
<header class="market-header small-padding">
    <div class="header-overlay">
        <div class="header page-center">

            <div class="navbar navbar-static-top navbar-inverse site-main-navigation" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="http://pharmacyowners.hs-sites.com/home"><img src="http://cdn2.hubspot.net/hub/37772/file-1513961891-png/Conference_Images/pds-logo.png?t=1413987154963" alt="Pharmacy Development Services Logo"/></a>
                    </div>
                    <div class="navbar-collapse collapse site-nav-collapse">
                        <div class="header-nav pull-right">
                            <div class="col-lg-12 col-md-12 col-xs-12 site-nav">
                                <div class="nav-container">
                                    <ul class="navbar-nav">
                                        <li id="whatwedo" class="nav-item dropdown">
                                            <a id="what-we-do" href="http://pharmacyowners.hs-sites.com/what-we-do" class="nav-link">What We Do</a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="what-we-do">
                                                <li class="sub-item"><a role="menuitem" tabindex="-1" href="http://pharmacyowners.hs-sites.com/lead">Lead</a></li>
                                                <li class="sub-item"><a role="menuitem" tabindex="-1" href="http://pharmacyowners.hs-sites.com/innovate">Innovate</a></li>
                                                <li class="sub-item"><a role="menuitem" tabindex="-1" href="http://pharmacyowners.hs-sites.com/connect">Connect</a></li>
                                                <li class="sub-item"><a role="menuitem" tabindex="-1" href="http://pharmacyowners.hs-sites.com/membership">Membership</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/who-we-help" class="nav-link">Who We Help</a></li>
                                        <li id="training" class="nav-item training dropdown">
                                            <a id="training" class="nav-link" href="http://pharmacyowners.hs-sites.com/training">Training</a> 
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="training">
                                                <li class="sub-item"><a role="menuitem" tabindex="0" href="http://pharmacyowners.hs-sites.com/training?type=webinar">Webinars</a></li> 
                                                <li class="sub-item"><a role="menuitem" tabindex="1" href="http://pharmacyowners.hs-sites.com/training?type=regional">Regional</a></li> 
                                                <li class="sub-item"><a role="menuitem" tabindex="2" href="http://pharmacyowners.hs-sites.com/training?type=on-site">On-Site</a></li> 
                                                <li class="sub-item"><a role="menuitem" tabindex="3" href="http://pharmacyowners.hs-sites.com/training?type=members-only">Members Only</a></li> 
                                            </ul>
                                        </li>
                                        <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/our-story" class="nav-link">About</a></li>
                                        <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/contact-pharmacy-development-services" class="nav-link">Contact</a></li>
                                        <li class="nav-item"><a href="http://pharmacyowners.hs-sites.com/new-pds-blog" class="nav-link">Blog</a></li>
                                        @if(Auth::check())
                                        {{-- <li class="nav-item"><a class="orange-text" href="{{ URL::route('dash.main') }}">Dashboard</a></li> --}}
                                        @endif
                                    </ul>
                                </div> 
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div><!--/.nav-collapse -->
                </div>
            </div>

            <div class="page-header-title">
                @if(isset($company))
                <h1>{{ $company }}</h1>
                @else
                <h1>{{ $page }}</h1>
                @endif
            </div>
        </div>
    </div>
</header>
