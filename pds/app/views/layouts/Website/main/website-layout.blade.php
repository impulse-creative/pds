<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <title>Pharmacy Development Services | Marketplace For Our Community</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        @section('head')

        <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
        <!-- Latest compiled and minified CSS/JS -->
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}
        {{ HTML::style('css/slicknav.css') }}
        
        <!-- Custom CSS -->
        {{ HTML::style('css/main-styles.css') }}
        {{-- js below helps to keep webkit columns supported web browsers < IE 10 --}}
        <!--[if lt IE 10]>
              {{ HTML::script('js/css-multi-col.js') }}
        <![endif]-->
 
        @show
        
 
    </head>
    <body>
        <div class="page-wrap">
        	@include('layouts.pds-advantage-header')
            <div class="clearfix"></div>
        	<div class="container-fluid content forum-bg">
        		@yield("content")
        	</div>
        	@include("layouts.Website.footer")
            <!-- Custom JQuery Scripts | Global Scripts | loading in footer for faster load times -->
            @section('extra-js')
                <!-- Latest compiled JQuery - minified -->
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                {{ HTML::script('js/jquery.slicknav.min.js') }}
                <script style="display: none;">
                    $(function(){
                        $('#menu').slicknav();
                        $('.slicknav_menu > a').before('<a class="mobile-logo" href="http://www.pharmacyowners.hs-sites.com/"><img src="//cdn2.hubspot.net/hub/37772/file-1513961891-png/Conference_Images/pds-logo.png" /></a>');
                    });
                </script>
                {{ HTML::script('js/script.js') }}
                {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
            @show
        </div>
    <script src="{{ URL::to('/js/global.js') }}"></script>
    </body>
</html>