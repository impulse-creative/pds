<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <meta name="_token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Your Dashboard to the Pharmacy World | pdsAdvantage </title>
        @section('head')
            <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
            <!-- Latest compiled JQuery - minified -->
            <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

            {{-- Javascript DropBox datastore API --}}
            {{--<script src="https://www.dropbox.com/static/api/1/dropbox-datastores-0.1.0-b3.js"></script>--}}
            

            {{-- Latest compiled and minified CSS/JS (unless localhost, then not minified)--}}
            @if(($environment = App::environment()) == 'production')
                {{-- Form::assets('dash-production-css') --}}
                {{ Form::assets('dash-css') }}
                
            @elseif(($environment = App::environment()) == 'local')
                {{ Form::assets('dash-css') }}
                
            @endif
            {{ HTML::style('css/forum-styles.css') }}
            {{ HTML::style('css/main-styles.css') }}
        @show
 
    </head>
    <body>
    	@include('layouts.Forums.header')
    	<div class="container-fluid content forum-bg">
            @yield("content")
    	</div>
    	@include("dashboard.admin-footer")
        <!-- Custom JQuery Scripts | Global Scripts | loading in footer for faster load times -->
        {{ Form::assets('dash-js') }}
        @section('extra-js')
        @show   
        <script>
            //data confirm pop up
            //-->Global data confirm class
            $("body").on("click", ".confirm", function() {
                return confirm($(this).data("confirm")); 
            });
        </script>
        <script src="{{ URL::to('/js/global.js') }}"></script>
    </body>
</html>