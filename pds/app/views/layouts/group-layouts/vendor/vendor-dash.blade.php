<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <title>Pharmacy Development Services | World's Best Vendor to Vendors</title>
        @section('head')
        <!-- Latest compiled JQuery - minified -->
        {{ HTML::script('js/jquery-1.11.1.min.js') }}
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
        <!-- Latest compiled and minified CSS -->
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}

        <!-- Latest compiled and minified JavaScript -->
        {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
        
        <!-- Custom CSS -->
        {{ HTML::style('css/dash-styles.css') }}
        {{-- HTML::style('css/main-styles.css') --}}

        <!-- Custom JQuery Scripts | Global Scripts -->
        {{ HTML::script('js/custom-scripts.js') }}
        @show
        

    </head>
    <body>
    	@include('admin-header')
    	<div class="container-fluid content forum-bg">
    		@yield("content")
            
    	</div>
    	@include("admin-footer")
        <script src="{{ URL::to('/js/global.js') }}"></script>
    </body>
</html>