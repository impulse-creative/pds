<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Expires" content="0"/>
        <title>Pharmacy Development Services | Marketplace For Our Community</title>
        @section('head')
            <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
            {{-- HTML::script('js/jquery-1.11.1.min.js') --}}
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

            <!-- Latest compiled and minified CSS/JS -->
            {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}
            {{ HTML::style('css/slicknav.css') }}
            <!-- Custom CSS -->
            {{ HTML::style('css/main-styles.css') }}
            {{ HTML::style('css/market-styles.css') }}
        @show
        <!-- Start of Async HubSpot Analytics Code -->

    </head> 
    <body>
        <div class="page-wrap">
        	@include('layouts.Website.header')
        	<div class="container-fluid content forum-bg">
        		@yield("content")
        	</div>
        	@include("layouts.Website.footer")
            <!-- Custom JQuery Scripts | Global Scripts | loading in footer for faster load times -->
            {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
            {{ HTML::script('js/jquery.session.js') }}
            {{ HTML::script('js/script.js') }}
            {{ HTML::script('js/holder.js')}}
            {{ HTML::script('js/jquery.slicknav.min.js') }}
            @section('extra-js')
            @show
        </div>
        <script src="{{ URL::to('/js/global.js') }}"></script>
    </body>
</html>