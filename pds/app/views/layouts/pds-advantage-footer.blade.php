
<footer class="site-footer dark-gray-background off-white-text">
	<div class="footer container medium-padding center">
		<div class="white-text">Copyright © 1998-{{date('Y')}} Pharmacy Development Services, Inc . All Rights Reserved.</div>
                <div class="footer-social-links">
                    <ul>
                        <li><a href="https://www.facebook.com/PharmacyDevelopmentServices" class="white-text" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                        <li><a href="https://twitter.com/rxmarketing" class="white-text" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/565830" class="white-text" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                    </ul>
                </div>
	</div>
</footer>
<div id="uv-email-address" class="hidden">{{$sections['user']['email']}}</div>
<div id="uv-name" class="hidden">{{$sections['user']['fname'].' '.$sections['user']['lname']}}</div>
<div id="uv-id" class="hidden">{{$sections['user']['id']}}</div>
<div id="uv-vid" class="hidden">{{$sections['user']['vid']}}</div>
<script>
      /* Hubspot Tracking Code */
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/37772.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
</script>
<?php
if(Cache::has('ghoster'))
{
    $ghoster_id = Cache::get('ghoster');
    $ghoster_user = User::find($ghoster_id);
    $permissions = explode(':', $ghoster_user->groups);
    if(in_array('5', $permissions)) {
        echo "<div class='reverse-ghosting'>";
        echo "<a href='".URL::route('unghost.user', ['ghoster' => $ghoster_id])."'>Reverse Ghost</a>";
        echo "</div>";
    }
}