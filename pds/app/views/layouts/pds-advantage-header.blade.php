{{-- Forum Header --}}

{{ HTML::script('js/bootstrap-wisyhtml5.js') }}
<?php 
    $permissions = explode(':', $sections['permissions']);
    $user = $sections['user'];
    $name = $sections['name'];      
    $secret = in_array(5, $permissions) ? '<div id="adminMenu"><a href="#" id="makeMagic"><i class="fa fa-cogs"> </i></a></div>' : '';
    setlocale(LC_ALL, 'en_US.UTF8');
    function toAscii($str) {
            $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
            $clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
            $clean = strtolower(trim($clean, '-'));
            $clean = preg_replace("/[\/_| -]+/", '-', $clean);

            return $clean;
    }
?>
<div class="slicknav_menu hidden">
    <ul id="menu">
        <li class="nav-item">
            <a href="{{ URL::route('forum.index') }}" class="nav-link">Message Boards</a>
        </li>
        <li class="nav-item">
            <a href="{{ URL::route('events.index') }}" class="nav-link">Events</a>
        </li>
        <li class="nav-item">
            <a href="{{ URL::route('library.index') }}" class="nav-link">Resource Library</a>
        </li>
        <li class="nav-item">
            <!--<a href="{{ URL::route('get.project.management', ['id' => Auth::id()]) }}" class="nav-link">myPDS</a>-->
            myPharmacy
        </li>
        <ul>
            @if($user->companies != '0' && $user->companies != null)
                <li><a href="{{ URL::to('dashboard/pharmacy/profile/view/'.$user->id) }}" class="nav-link">Dashboard</a></li>
                @if(in_array('2', $permissions))
                    <li><a href="{{ URL::to('message-boards/private') }}" class="nav-link">Message-Boards</a></li>
                @endif
            @else
            <li>Coming Soon...</li>
            @endif
            @if(in_array('33', $permissions))
                <li><a href="{{ URL::route('get.project.management', ['id' => Auth::id()]) }}" class="nav-link">myPDS</a></li>
            @endif
        </ul>
        </li>
        <li class="nav-item">
            <a href="http://pdsadvantage.uservoice.com" target="_blank" class="nav-link">Support</a>
        </li>
        
        @if(in_array('8', $permissions))
            <li class="nav-item">
                <a href="{{ URL::to('pdsmarketplace') }}">Marketplace Admin</a>
            </li>
        @endif
        @if(Auth::check())
            <li>
                <a href="{{ URL::to('/user/'.$sections['user']['id'].'/'.$sections['profile_url']) }}"><span class="{{ $profile_link = in_array('2', $permissions) ? 'orange-text' : 'purple-text' }}">My Profile</span></a>
                
            </li>
            <li>
                <a href="{{ URL::to('/user/'.$sections['user']['id'].'/'.$sections['profile_url'].'#profile') }}"><span class="{{ $profile_link = in_array('2', $permissions) ? 'orange-text' : 'purple-text' }}">Edit Profile</span></a>
            </li>
            <li>
                <a href="{{ URL::to('dashboard/main') }}" class="vcenter">My Dashboard <span class="badge">{{ $total = count($sections['notifications']) != 0 ? count($sections['notifications']) : '' }}</span></a>
            </li>
            <li>
                @if($sections['getting-started'] != 0)
                    <a href="{{ URL::to('getting-started') }}" class="vcenter" title="Getting started with PDS advantage tools">Getting Started <span class="badge">{{$sections['getting-started']}}</span></a>
                @endif
            </li>
            <li>
                <a class="logout" href="{{ URL::to('dashboard/logout') }}" title="Logout" data-confirm="Logging Out will prevent you from automatically signing in next time you visit PDSadvantage Are you sure you want to logout">Logout</a>
            </li>
        @endif
        
    </ul>
</div>

<div class="forum-header {{ $background = in_array('2', $permissions) ? 'purple' : 'orange' }}-background">
    	<div class="container">
            {{$secret}}
		<div class="mini-acc pull-right">
                    @if(!in_array('2', $permissions))
                        <div class="signup-cta pull-right vcenter">
                            <a href="http://hubs.ly/y0xs880" target="_blank" class="btn btn-default pull-right">Become a Member</a>
                        </div>
                    @endif
                    <div class="mini-profile">
                        <p>
                        <ul>
                            <li><a href="{{ URL::to('/user/'.$sections['user']['id'].'/'.$sections['profile_url']) }}">Hi, <span class="{{ $profile_link = in_array('2', $permissions) ? 'orange-text' : 'purple-text' }}">{{$name}}</span></a></li>
                            <li class="seperator">|</li>
                            <li><a href="{{ URL::to('dashboard/main') }}" class="vcenter">Dashboard <span class="badge">{{ $total = count($sections['notifications']) != 0 ? count($sections['notifications']) : '' }}</span></a></li>
                            <li class="seperator">|</li>
                            <li>
                                @if($sections['getting-started'] != 0)
                                    &nbsp;<a href="{{ URL::to('getting-started') }}" class="vcenter" title="Getting started with PDS advantage tools">Getting Started <span class="badge">{{$sections['getting-started']}}</span></a>
                                @endif
                            </li>
                            <li>
                                <a href="{{ URL::to('/user/'.$sections['user']['id'].'/'.$sections['profile_url']) }}">
                                    {{ $image = User::getBioImage(Auth::id(), 'vcenter forum-mini-profile-img') }}
                                </a>
                                <a class="logout" href="{{ URL::to('dashboard/logout') }}" title="Logout"><span class="glyphicon glyphicon-log-out confirm" data-confirm="Logging out will prevent you from automatically signing in next time you visit PDSadvantage. Are you sure you want to logout?"></span></a>
                            </li>
                        </ul>
                    </div>
		</div>
		<div class="clearfix"></div>
		<div class="forum-logo small-padding col-lg-3 col-md-3 vcenter">
			<a href="{{ URL::route('dash.main') }}"><img src="{{ asset('images/application/pds-white-logo-small.png') }}" alt="PDS"/>
		</div><!--
		--><div class="forum-nav small-padding col-lg-9 vcenter">
                        <ul class="menu">
                            <li class="nav-item">
                                <a href="{{ URL::route('forum.index') }}" class="nav-link">Message Boards</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ URL::route('events.index') }}" class="nav-link">Events</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ URL::route('library.index') }}" class="nav-link">Resource Library</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">myPharmacy <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    @if($user->companies != '0' && $user->companies != null)
                                        @if(in_array('33', $permissions) && in_array('5', $permissions))
                                            <li><a href="{{ URL::route('get.project.management', ['id' => Auth::id()]) }}" class="nav-link">myPDS</a></li>
                                        @endif
                                        <li><a href="{{ URL::to('dashboard/pharmacy/profile/view/'.$user->id) }}" class="nav-link">Dashboard</a></li>
                                        @if(in_array('2', $permissions))
                                            <li><a href="{{ URL::to('message-boards/private') }}" class="nav-link">Message-Boards</a></li>
                                        @endif
                                    @else
                                        <li>Setup Your company first by visiting the <a href="{{ URL::route('dash.main') }}" class="nav-link">Main Dashboard</a></li>
                                    @endif
                                </ul>
                            </li>
                            @if(in_array('8', $permissions))
                                <li class="nav-item">
                                    <a href="{{ URL::to('pdsmarketplace') }}">Marketplace Admin</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a href="http://pdsadvantage.uservoice.com" target="_blank" class="nav-link">Support</a>
                            </li>
			</ul>
		</div>
	</div>
</div>
<!--<div class="small-padding">
    <div class="alert alert-info center" role="alert"> If you are a paid member and your Header Bar is orange, use the support button to request access. If you are an unpaid member, mention it also and we will activate your account.  </div>
</div>-->
{{ $message = $sections['notifications-record'] == false ? '<div class="alert alert-info center" role="alert">Not receiving Daily Digest Emails?  <a href="'.URL::to('/user/'.$user->id.'/'.trim($user->fname).'-'.trim($user->lname).'#profile').'">Edit My Settings</a></div>' : '' }}
{{ $citystate = !isset($user->state) ? '<div class="alert alert-warning center" role="alert">Please complete your city / state and other items in your profile when you have a moment by visiting:  <a href="'.URL::to('/user/'.$user->id.'/'.trim($user->fname).'-'.trim($user->lname).'#profile').'">Edit My Settings</a></div>' : '' }}