<?php
    $permissions = explode(':', $sections['permissions']);
    $background = in_array('2', $permissions) ? 'purple-background' : 'orange-background';
?>
<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>{{ $title = isset($pageTitle) ? $pageTitle : 'PDSadvantage' }}</title>
        @section('head')
            <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <!-- Latest compiled and minified CSS/JS -->
            {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}
            {{ HTML::style('css/slicknav.css') }}
<!--            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.2.2/css/material-wfont.css"/>-->

            <!-- Custom CSS -->
            @foreach($css as $style)
                {{ HTML::style("css/$style-styles.css") }}

            @endforeach
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
            {{-- js below helps to keep webkit columns supported web browsers < IE 10 --}}
            <!--[if lt IE 10]>
                  {{ HTML::script('js/css-multi-col.js') }}
            <![endif]-->
        
        @show
 
    </head>
    <body class="message-boards-pages">
        <div class="page-wrap">
            {{-- layout header section --}}
                @include('layouts.pds-advantage-header')
            <div class="clearfix"></div>
            <div class="forum-bg">
                <div id="cog-menu" class="container-fluid">
                    <ul id="dash-nav" class="nav nav-sidebar">
                        <li><a href="{{ URL::to('achievements') }}">Achievements</a></li>
                        <li><a href="{{ URL::to('badges') }}">Badges</a></li>
                        <li><a href="{{ URL::to('access-groups') }}">Access Groups</a></li>
                        <li><a href="{{ URL::to('dashboard/approve/newUsers') }}">Approve New Users</a></li>
                        @include('dashboard.group-dash-views.menu')
                        <li><a href="{{ URL::route('password.remind') }}">Settings</a></li>
                        <li><a href="{{ URL::route('dash.logout') }}">Logout</a></li>
                    </ul>
                </div>

                {{-- layout content section --}}

                            @yield("content")


                {{-- layout footer section --}}
            </div>
        	@include("layouts.pds-advantage-footer")

            {{-- Custom JQuery Scripts | Global Scripts | loading in footer for faster load times --}}
            @section('extra-js')
                <!-- Latest compiled JQuery - minified -->
                
                
                <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
                {{ HTML::script('js/jquery.slicknav.min.js') }}
                {{ HTML::script('js/script.js') }}
                {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
                <?php $dash_url = URL::route('dash.main'); ?>
                <script style="display: none;">
                    $(function(){
                        $('#menu').slicknav();
                        if (!window.location.origin) 
                            window.location.origin = window.location.protocol+"//"+window.location.host+'/';
                        $('.slicknav_menu > a').before('<a class="mobile-logo" href="<?php echo $dash_url; ?>"><img src="'+ window.location.origin +'/images/application/pds-white-logo-small.png" /></a>');
                        $('.message-boards-pages .slicknav_menu').addClass("<?php echo $background; ?>");
                        $('.message-boards-pages .slicknav_btn').addClass("<?php echo $background; ?>");
                    });
                </script>
                <script type="text/javascript">
                $(function () {
                    $("[rel='tooltip']").tooltip({
                        placement: 'top',
                        trigger: 'hover focus'
                    });
                    $(document).on('focus', '[rel=tooltip]', function () { 
                        $(this).tooltip('show'); });
                });
                </script>
            @show
        </div>
        <script src="{{ URL::to('/js/global.js') }}"></script>      
    </body>
</html>
