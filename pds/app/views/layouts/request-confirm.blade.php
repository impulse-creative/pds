<!DOCTYPE>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title>Pharmacy Development Services | World's Best Vendor to Vendors</title>
        <link rel="shortcut icon" href="//cdn2.hubspot.net/hub/37772/file-1997967039-ico/pdsfavicon.ico?t=1414780233000">
        @section('head')
        <!-- Latest compiled JQuery - minified -->
        {{ HTML::script('js/jquery-1.11.1.min.js') }}
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
        <!-- Latest compiled and minified CSS -->
        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') }}

        <!-- Latest compiled and minified JavaScript -->
        {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js') }}
        
        <!-- Custom CSS -->
        {{ HTML::style('css/main-styles.css') }}
        {{ HTML::style('css/email-conf-styles.css') }}

        <!-- Custom JQuery Scripts | Global Scripts -->
        {{ HTML::script('js/custom-scripts.js') }}
        
        @show

    </head>
    <body>
        <div class="content-wrap">
        	<div class="content">
        		@yield("content")
        	</div>
            @include("dashboard.admin-footer")
        </div>
        <script src="{{ URL::to('/js/global.js') }}"></script>
    </body>
</html>