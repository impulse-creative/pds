@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/forum-styles.css') }}
    <?php $permissions = explode(':', Auth::user()->groups); ?>
@stop
@section('content')
<div class="container forum-container">
    <div class="col-lg-3 col-md-3">
        @if(!preg_match('/pending/', $sections['user']->companies))
            @include('Forums.partials.private-category-sidebar')
        @endif
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="alert alert-info"><button type="button" class="close hidden" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> Management is for Owners and Management team only.  Staff Member's can see Operations ONLY.</div>
        @if(!preg_match('/pending/', $sections['user']->companies))
            @if(in_array('2', $sections['permissionsArray']))
                <div class="topic-list-information small-padding">
                    <div class="col-lg-9 filtered-by">
                        @if($forum_data['categoryId'] != 0) <p>Message Board: @endif
                            <span>
                                @if($forum_data['categoryId'] != 0) 
                                <span class="purple-text">{{ Company::find($forum_data['activeCompany'])->name }}</span> <span style="color:#666;">-</span> {{ $forum_data['privateCategories'][$forum_data['categoryId']] }}
                                @else

                                    @if(Input::get('search_key') == null)
                                    <span class="orange-text">Select a category from the left to view your private message boards.</span>
                                    @else
                                        @if(count($forum_data['allTopics']) == 0)
                                            <span class="orange-text">No Results Found</span>
                                        @else
                                            <span class="orange-text">Showing {{ count($forum_data['allTopics']) }} Result{{ $plural = count($forum_data['allTopics']) == 1 ? '' : 's'}}</span>
                                        @endif
                                    @endif
                                @endif
                            </span>
                        </p>
                        <div class="clearfix"></div>
                    </div>
                    @if(Auth::check() && $forum_data['categoryId'] != 2)
                        <div class="col-lg-3 create-thread">
                            @if($forum_data['categoryId'] != 0) 
                                <a href="#" data-target="#createTopic" data-toggle="modal" class="btn btn-primary new-thread-btn pull-right">
                                    <span class="glyphicon glyphicon-plus"></span>&nbsp;Create New Topic
                                </a>
                            @endif
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>
                <div class="forum-topics">
                    @if(Session::has('Success'))
                        <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Success', 'default') }}</div>
                    @endif
                    @if(Session::has('Failure'))
                        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $topic = Session::pull('Failure', 'default') }}</div>
                    @endif
                    @if(isset($forum_data['allTopics']))
                        @if(count($forum_data['allTopics']) > 0)
                            @if(is_object($forum_data['allTopics']))
                                <div class="center"> {{$forum_data['allTopics']->appends(Input::except('page'))->links()}} </div>
                            @endif
                            @foreach($forum_data['allTopics'] as $topic)
                                <?php $messageCount = \ForumMessages::where('parent_topic', $topic->id)->count(); ?>
                                    <div class="topic-item purple-topic"> 
                                        <div class="author-img col-lg-4 col-md-4 col-xs-4 vcenter">
                                            {{ $image = User::getBioImage($topic->author_id, 'vcenter forum-mini-profile-img') }}
                                        </div><!--
                                        --><div class="topic-info col-lg-9 col-md-9 col-xs-9 vcenter">
                                            <h2 class="topic-title"><a href="{{ URL::to('message-boards/private/company') }}/{{ preg_replace('/ /', '-', preg_replace('/[^a-z\d\s]+/i', '', $topic->title)) }}/{{$topic->id}}?private=true&category={{$topic->parent_category}}&companyId={{$topic->company}}" class="purple-text">{{ $topic->title }}</a></h2>
                                            <p class="author-info">Posted by <span class="purple-text"><a target="_blank" href="{{ URL::to('user/'.$topic->author_id.'/'.$topic->users->fname.'-'.$topic->users->lname) }}">{{ $topic->users->fname }}&nbsp;{{ $topic->users->lname }}</a> on {{$topic->created_at->format('l, F j \\a\\t h:ia')}}</span>
                                                @if(is_object($topic->lastReplied))
                                                <?php $lastRepliedUser = User::find($topic->lastReplied->author_id); ?>
                                                | Latest reply by <span class="purple-text"><a target="_blank" href="{{ URL::to('user/'.$lastRepliedUser->id.'/'.$lastRepliedUser->fname.'-'.$lastRepliedUser->lname) }}">{{ $lastRepliedUser->fname.' '.$lastRepliedUser->lname }}</a></span>
                                                @else 
                                                | <a href="{{ URL::to('message-boards/private/company') }}/{{ preg_replace('/ /', '-', preg_replace('/[^a-z\d\s]+/i', '', $topic->title)) }}/{{$topic->id}}?private=true&category={{$topic->parent_category}}&companyId={{$topic->company}}#post-replies" class="force-orange-text">Be the first to reply!</a> 
                                                @endif
                                            </p>
                                        </div><!--
                                        --><div class="topic-comment-count col-lg-2 col-md-2 col-xs-2 vcenter"><a href="{{ URL::to('message-boards') }}/{{ preg_replace('/ /', '-', preg_replace('/[^a-z\d\s]+/i', '', $topic->title)) }}/{{ $topic->id }}#post-replies?private=true">{{ $messageCount }}</a></div>
                                        <div class="clearfix"></div>
                                    </div>

                            @endforeach        
                        @endif
                    @endif
                </div>

                <div class="forum-category-list-mobile mobile-categories">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="category-list-mobile" data-toggle="dropdown" aria-expanded="true">
                            Select a Category to View
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu resource-dropdown" role="menu" aria-labelledby="category-list-mobile">
                            @foreach($forum_data['privateCategories'] as $key => $category)
                                @if($key == 0) 
                                    <li class="cat-item item-{{ $key }}"><a href="{{ URL::to('message-boards') }}">All Categories</a></li> <?php continue; ?> 
                                @endif
                                <li class="cat-item item-{{ $key }}"><a href="{{ URL::to('message-boards/category') }}?category={{$key}}">{{ $category }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @else
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> You don't have permission to use this.</div>
            @endif
        @else
            <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> Your account is pending and needs to be approved by your company owner.</div>
        @endif
    </div>
</div>
<div class="clearfix"></div>
{{-- Add New Topic --}}
<div class="modal fade" id="createTopic" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Create New Topic</h4>
            </div>
            <div class="modal-body">
                <div class="validation-warning"></div>
                {{ Form::open(array('route' => 'create.forum.topic', 'class' => 'createTopic', 'novalidate' => '', 'files' => true)) }}
                {{ Form::hidden('cat_id', null, ['class' => 'categoryTopic_hidden_id']) }}
                {{ Form::hidden('last_replied', 0) }}
                @if(Input::get('companyId') != null)
                    {{ Form::hidden('is_pharmacy_private_post', 1) }}
                    {{ Form::hidden('company', Input::get('companyId')) }}
                @endif
                <input name="title" id="title" type="text" class="inp-field" placeholder="What topic would spark an amazing conversation?" value="{{ Input::old('title') }}"/><br/>
                @if($errors->has('title')) <p class="red-text small-waist">{{ $errors->first('title', 'Please add a topic title to continue.') }}</p> @endif
                    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                        <div class="btn-group">
                          <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <a class="btn btn-default" data-edit="bold" data-original-title="Bold (Ctrl/Cmd+B)" title="Bold"><i class="fa fa-bold"></i></a>
                            <a class="btn btn-default" data-edit="italic" data-original-title="Italic (Ctrl/Cmd+I)" title="Italic"><i class="fa fa-italic"></i></i></a>
                            <a class="btn btn-default" data-edit="underline" data-original-title="Underline (Ctrl/Cmd+U)" title="Underline"><i class="fa fa-underline"></i></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn btn-default" data-edit="insertunorderedlist" data-original-title="Bullet list" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                            <a class="btn btn-default" data-edit="insertorderedlist" data-original-title="Number list" title="Number list"><i class="fa fa-list-ol"></i></a>
                            <a class="btn btn-default" data-edit="outdent" data-original-title="Reduce indent (Shift+Tab)" title="Reduce indent"><i class="fa fa-outdent"></i></a>
                            <a class="btn btn-default" data-edit="indent" data-original-title="Indent (Tab)" title="Indent"><i class="fa fa-indent"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn btn-default" data-edit="justifyleft" data-original-title="Align Left (Ctrl/Cmd+L)" title="Align left"><i class="fa fa-align-left"></i></a>
                            <a class="btn btn-default" data-edit="justifycenter" data-original-title="Center (Ctrl/Cmd+E)" title="Center"><i class="fa fa-align-center"></i></a>
                            <a class="btn btn-default" data-edit="justifyright" data-original-title="Align Right (Ctrl/Cmd+R)" title="Align right"><i class="fa fa-align-right"></i></a>
                            <a class="btn btn-default" data-edit="justifyfull" data-original-title="Justify (Ctrl/Cmd+J)" title="Justify"><i class="fa fa-align-justify"></i></a>
                        </div>
                    </div>
                    <div id="editor" class="editor">{{ $topic_data['topic']->description or 'Include a description for your topic that engages the user\'s thoughts and provokes them to join the conversation.' }}</div>
                    <textarea class="form-control description hidden" id="hidden-editor" name="description" cols="10" rows="5" placeholder="Include a description for your topic that engages the user's thoughts and provokes them to join the conversation."></textarea>
                @if($errors->has('description')) <p class="red-text small-waist">{{ $errors->first('description', 'Please add a description to continue.') }}</p> @endif
                <div class="row">
                    <div class="col-lg-6">
                        <div class="control-group">
                            @if(in_array('6', $permissions))
                                {{ Form::label('author_id', 'Author', ['class' => 'modal-label']) }}
                                {{ Form::select('author_id', $forum_data['authors'], Auth::id(), ['class' => 'form-control author-dropdown']) }}
                            @else
                                <input name="author_id" id="author_id" type="text" class="inp-field hidden" value="{{ Auth::id() }}"/><br/>
                            @endif
                            @if(Input::get('companyId') != null)
                            {{ Form::label('company', 'Company', ['class' => 'modal-label']) }}
                            {{ Form::select('company', $forum_data['companies'], $forum_data['activeCompany'], ['class' => 'form-control author-dropdown']) }}
                            @endif
                            <br/>
                            {{ Form::label('parent_category', 'Category', ['class' => 'model-label']) }}
                            <?php unset($forum_data['privateCategories'][2]); ?>
                            {{ Form::select('parent_category', $forum_data['privateCategories'], $forum_data['categoryId'], ['class' => 'form-control author-dropdown']) }}
                            @if($errors->has('parent_category')) 
                                <p class="red-text small-waist">{{ $errors->first('parent_category', 'Please choose a category to continue.') }}</p> 
                            @endif
                            <br/>
                            {{ Form::hidden('main_view', 0) }}
                        </div><!-- /.control-group -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        {{ Form::label('attachment', 'Add Attachment', ['class' => 'modal-label']) }}
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">
                                    Browse&hellip; {{ Form::file('attachment') }}
                                </span>
                            </span>
                            <input type="text" name="attachment" class="form-control" readonly>
                        </div><!-- /.control-group -->
                        <span class="help-block">
                            We only allow single file attachments. Multiple files need to be zipped.
                        </span>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeNewTopicCreator" data-dismiss="modal">Close</button>
                {{ Form::submit('Create', array('class' => 'btn btn-primary new-topic-submit')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop


@section('extra-js')
    @parent
    {{ HTML::script('js/wysihtml5/parser_rules/advanced.js') }}
    {{ HTML::script('js/wysihtml5/dist/wysihtml5-0.3.0.min.js') }}
    {{ HTML::script('js/forum-scripts.js') }}
    {{ HTML::script('js/bootstrap-wisyhtml5.js') }}
    <script>
        $('#editor').wysiwyg();
        $('.createTopic').submit(function(event) {
            $('#hidden-editor').val($('#editor').cleanHtml());
        });
        $('#editor').on('focus', function(){
           if($(this).text() == 'Include a description for your topic that engages the user\'s thoughts and provokes them to join the conversation.')
           $(this).html('');
        });
    </script>
    <script>
        /*-- for the forum featured tags -- this makes sure the featured tag height stays the same as the parent div --*/
        $.fn.adjustTagHeightForParent = function () {
           //console.log('start');
            return $(this).each(function () {
                var parentHeight = $(this).parent().outerHeight();
                $(this).css('width', parentHeight - 2);
            });
        };
        $(document).ready(function () {
            $('#category-filter-select').change(function () {
                $('.filter-form').submit();
            });

            $('.featured-bar').adjustTagHeightForParent();
        });
        $(window).resize(function () {
            $('.featured-bar').adjustTagHeightForParent();
        });
        
        $(document).on('change', '.btn-file :file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [numFiles, label]);
        });

        $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            });
            
            /*-- initialize each wysiwig editor for adding events page --*/
            if($('body').hasClass('wysihtml5-supported')) {
                var editor = new wysihtml5.Editor("editor", { // id of textarea element
                  toolbar:      "wysihtml5-toolbar", // id of toolbar element
                  parserRules:  wysihtml5ParserRules // defined in parser rules set 
                });
            };
           
            $('.createTopic').submit(function(e){
                /*-- get rid of bullshit iframe --*/
                if($('iframe').length > 1) {
                    console.log('here');
                    $('iframe').eq(0).remove();
                }
                var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
                console.log(html);
                $('#hidden-html').val(html);
            });
        });
    </script>
@stop
