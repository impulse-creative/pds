@extends('layouts.pds-advantage')

@section('head')
    @parent
    {{ HTML::style('css/forum-styles.css') }}
@stop

@section('content')
<div class="container forum-container">
    <div class="col-lg-3 col-md-3">
        @include('Forums.partials.private-category-sidebar')
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="topic-list-information small-padding">
            <div class="col-lg-7 filtered-by">
                <p>Message Board:
                    <span>
                        <span class="purple-text">{{ Company::find(Input::get('companyId'))->name }}</span> <span style="color:#666;">-</span> 
                        {{ $forum_data['privateCategories'][Input::get('category')] }}
            </div>
            <div class="col-lg-5 create-thread">
                <a href="#replyBox" class="btn btn-primary new-thread-btn">Post a reply</a>
                <span data-type="topics" data-id="{{$topic_data['topic']->id}}" class="btn btn-primary subscribe-button">Subscribe to this Topic</span>
            </div>
            <div class="clearfix"></div>
        </div>
        {{-- post details --}}
        <div class="forum-post">
            @if(Session::has('Success'))
                <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Success', 'default') }}</div>
            @endif
            @if(Session::has('Failure'))
                <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> {{ $val = Session::pull('Failure', 'default') }}</div>
            @endif
            {{-- post --}}
            <div class="forum-post-view">
                <div class="topic-item-view">
                    <div class="featured-topic-badge green-background @if($topic_data['topic']->first()->users->featured == 0) hidden @endif">
                        Featured
                    </div><!--
                    --><div class="author-img col-lg-4 col-md-4 col-xs-4 vcenter">
                        {{ $image = User::getBioImage($topic_data['topic']->author_id, 'vcenter forum-mini-profile-img') }}
                    </div><!--
                    --><div class="topic-info col-lg-9 col-md-9 col-xs-9 vcenter">

                        <h2 class="topic-title">{{ $topic_data['topic']->title }}</h2>
                        <p class="author-info">
                            Posted by <span class="purple-text"><a href="{{ URL::to('/user/'.$topic_data['topic']->author_id.'/'.$topic_data['topic']->users->fname.'-'.$topic_data['topic']->users->lname) }}" target="_blank">{{ $topic_data['topic']->users->fname }}&nbsp;{{ $topic_data['topic']->users->lname }}</a> on {{$topic_data['topic']->created_at->format('l, F jS \\a\\t h:i')}}</span>
                            @if(isset($topic_data['messages'][0]))
                            | Latest reply by 
                            <span class="purple-text">
                                <a href="{{ URL::to('/user/'.$topic_data['messages']->last()->users->id.'/'.$topic_data['messages']->last()->users->fname.'-'.$topic_data['messages']->last()->users->lname) }}" target="_blank">
                                    {{ $topic_data['messages']->last()->users->fname }}&nbsp;{{ $topic_data['messages']->last()->users->lname }}
                                </a>
                            </span>
                            @endif
                        </p>
                    </div><!--
                    --><div class="topic-comment-count col-lg-2 col-md-2 col-xs-2 vcenter" style="background:#F58220!important;"><a href="#post-replies">{{ $topic_data['count'] }}</a></div>
                    <hr/>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="topic-description">
                    <p>{{$topic_data['topic']->description}}</p>
                </div>
                @if(Auth::check())
                    @if(isset($topic_data['topic']->attachment))
                        <div class="file-attachments">
                            <p class="text-muted">This post has an attachment</p>
                            <p>
                                <a class="orange-text" href="{{ $topic_data['topic']->attachment->resource_share_link }}" target="_blank">
                                    {{ $topic_data['topic']->attachment->filename }}
                                    &nbsp;&nbsp;<span class="text-muted">{{ $topic_data['topic']->attachment->file_size }}</span>
                                </a>
                            </p>
                        </div>
                    @endif
                    @if(in_array('6', $forum_data['usr_permissions']) || in_array('1', $forum_data['usr_permissions']) || in_array('5', $forum_data['usr_permissions']) || $topic_data['topic']->author_id == Auth::id())
                        <div class="update-btn pull-left">
                            <a data-toggle="modal" data-target="#updateTopicCategory" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</a>
                        </div>
                        @if(in_array('6', $forum_data['usr_permissions']) || in_array('1', $forum_data['usr_permissions']) || in_array('5', $forum_data['usr_permissions']))
                            <div class="update-btn pull-left">
                                @if($topic_data['topic']->featured == 0)
                                    <a class="btn btn-primary" href="{{ URL::to('message-boards/make/topic/featured/'.$topic_data['topic']->id) }}"><span class="glyphicon glyphicon-star"></span>&nbsp;Make This Post Featured</a>
                                @else
                                    <a class="btn btn-primary" href="{{ URL::to('message-boards/make/topic/unfeatured/'.$topic_data['topic']->id) }}"><span class="glyphicon glyphicon-star"></span>&nbsp;Un-Feature This Post</a>
                                @endif
                            </div>
                        @endif
                        <div class="destroy-btn pull-left">
                            <a href="{{ URL::route('app.destroy.forum.topic', $topic_data['topic']->id) }}" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i>&nbsp;Remove Topic</a>
                        </div>
                    @endif
                @endif
                <div class="topic-post-time pull-right">
                    <p class="post-time">Topic posted on {{ date("F d, Y", strtotime($topic_data['topic']->created_at)) }} at {{ date("g:i a",strtotime($topic_data['topic']->created_at)) }}</p>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
        {{-- post replies --}}
        <div class="forum-post-replies small-padding">
            <h2 id="post-replies">Replies</h2>
            <div class="message-pagination small-margin center">
                {{ $topic_data['messages']->appends(Input::except('page'))->links() }}
            </div>
            {{-- replies --}}
            @foreach($topic_data['messages'] as $key => $message)
                <?php
                    //choose correct styling of message label for special users
                    $group_list = explode(':', $message->groups);
                    if (in_array('1', $group_list) || in_array('14', $group_list)) {
                        $bg = 'purple-background';
                        $type = 'PDS Staff';
                        $class = 'admin-author-acc-badge';
                    } elseif (in_array('2', $group_list)) {
                        $bg = 'orange-background';
                        $type = 'PDS Member';
                        $class = 'author-acc-badge';
                    } else {
                        $bg = '';
                        $type = '';
                        $class = '';
                    }
                ?>
                <div id="{{$message->id}}" class="forum-reply-view">
                    <div class="reply-item-view">
                        <div class="{{$class}} {{$bg}} @if(!in_array('1', $group_list) && !in_array('14', $group_list) && !in_array('2', $group_list)) hidden @endif">
                            {{$type}}
                        </div><!--
                        --><div class="author-img col-lg-4 col-md-4 col-xs-4 vcenter">
                            {{ $image = User::getBioImage($message->users->id, 'vcenter forum-mini-profile-img') }}
                        </div><!--
                        --><div class="reply-info col-lg-9 col-md-9 col-xs-9 vcenter">
                            <h2 class="topic-title"><a href="{{ URL::to('/user/'.$message->users->id.'/'.$message->users->fname.'-'.$message->users->lname) }}" target="_blank">{{$message->users->fname}}&nbsp;{{$message->users->lname}}</a></h2>
                            <?php
                            $replies = \ForumMessages::where('author_id', $message->author_id)->count();
                            $topic_count = \ForumTopics::where('author_id', $message->author_id)->count();
                            $vendor_data = \Vendor::select('state_or_region')->where('user_id', $message->author_id)->first();
                            ?>
                            <p class="reply-author-details">&nbsp;&nbsp;<span>Topics:&nbsp;{{$topic_count}}</span>,&nbsp;<span>Replies:&nbsp;{{$replies}}<p>
                            <div class="clearfix"></div>
                            <p class="author-info">
                                <span class="purple-text">{{$message->users->pharmacy}}</span>&nbsp;@if(is_object($vendor_data) && isset($vendor_data->state_or_region)) |&nbsp;State:&nbsp;<span class="purple-text">{{$vendor_data->state_or_region}}</span> @endif
                            </p>
                        </div>
                        <hr/>
                        <div class="clearfix"></div>
                    </div>
                    <div class="topic-description" data-msg-id="{{ $message->id }}">
                        <p>{{$message->data}}</p>
                    </div>
                    @if(isset($message->attachment_id) && $message->attachment_id != 0)
                        <?php $attachment = Resources::messageAttachment($message->id)->first(); ?>
                        @if(count($attachment) > 0)
                            <div class="file-attachments">
                                <p class="text-muted">This reply has an attachment</p>
                                <p>
                                    <a class="orange-text" href="{{ $attachment->resource_share_link }}" target="_blank">
                                        {{ $attachment->resource_name }}
                                        &nbsp;&nbsp;<span class="text-muted">{{ $attachment->file_size }}</span>
                                    </a>
                                </p>
                            </div>
                        @else
                        <div class="file-attachments">
                            <p class="text-muted red-text">Error. Could not load resource.</p>
                        </div>
                        @endif
                    @endif
                    <div class="update-btn small-margin">
                        @if(Auth::check())
                            @if(in_array('6', $forum_data['usr_permissions']) || in_array('1', $forum_data['usr_permissions']) || in_array('5', $forum_data['usr_permissions']) || $message->author_id == Auth::id())
                                <a id="update-topic" class="btn btn-primary" href="{{ URL::to('message-boards/update/reply/'.$message->id.'?private=true&category='.Input::get('category').'&companyId='.Input::get('companyId')) }}"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit Reply</a>
                            @endif
                            <span class="topic-date">Replied:&nbsp;{{ date("F d, Y", strtotime($message->updated_at)) }} at {{ date("g:i a",strtotime($message->updated_at)) }}</span>
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="message-pagination small-margin center">
                {{ $topic_data['messages']->appends(Input::except('page'))->links() }}
            </div>
            @if(Auth::check())
                <div id="replyBox" class="reply-to-post small-padding">
                    <h1 class="purple-text small-padding">Reply to this thread</h1>
                    {{ Form::open(['route' => 'create.new.message', 'class' => 'add-reply', 'files' => true])}}
                    {{ $reply_editor }}
                    <!--<input class="inp-field data-html" id="hidden-html" name="html_edits" type="hidden" value="">-->
                    <div class="col-lg-6 col-md-6 col-xs-12" style="padding: 0;">
                        {{ Form::label('attachment', 'Add an Attachment', ['class' => 'modal-label']) }}
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    Browse&hellip; {{ Form::file('attachment') }}
                                </span>
                            </span>
                            <input type="text" name="attachment" class="form-control" readonly>
                        </div><!-- /.control-group -->
                        <span class="help-block">
                            Single file attachments. Multiple files should be zipped.
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="hidden-inputs">
                        {{ Form::hidden('parent_topic', $topic_data['topic']->id, null) }}
                        {{ Form::hidden('company', $topic_data['topic']->company) }}
                        @if(Auth::check())
                        {{ Form::hidden('author_id', Auth::id()) }}
                        @endif
                        {{ Form::hidden('subscribe', $topic_data['topic']->id) }}
                        {{ Form::hidden('page_id', $topic_data['messages']->getLastPage()) }}
                    </div>
                    {{ Form::submit('Post Your Reply', ['class' => 'btn btn-primary post-reply']) }}
                    {{ Form::close() }}
                </div>
            @endif
            {{-- end of reply box --}}
        </div>
    </div>
</div>
<div class="clearfix"></div>

{{-- Update Topic --}}
<div class="modal fade" id="updateTopicCategory" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modalLabel">Update Topic: <span class="purple-text">{{ $topic_data['topic']->title }}</span></h4>
            </div>
            <div class="modal-body">
                <div class="validation-warning"></div>
                {{ Form::model($topic_data['realTopic'], ['url' => URL::route('edit.forum.topic', $topic_data['topic']->id).'?edit=1', 'class' => 'updateForumTopic', 'role' => 'form'])}}
                <div class="form-group">
                    <label for="title">Topic Title</label>
                    {{ Form::input('text', 'title', Input::old('title'), ['placeholder' => 'Topic Title', 'class' => 'form-control']) }}
                    @if($errors->has('title')) <p class="help-block red-text">{{ $errors->first('title') }}</p><div class="clearfix"></div> @endif
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    {{ $update_editor }} 
                    @if($errors->has('description')) <p class="help-block red-text">{{ $errors->first('description') }}</p><div class="clearfix"></div> @endif
                </div>
                {{--<div class="form-group">
                    <label for="author_id">Author</label>
                    {{ Form::select('author_id', $topic_data['authors'], $topic_data['topic']->author_id, ['class' => 'form-control author-dropdown']) }}
                </div>--}}
                <div class="form-submit">
                    {{ Form::submit('Save Changes', ['class' => 'btn btn-default']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop


@section('extra-js')
    @parent
    <script>
        function linkify(inputText) {
            var replacedText, replacePattern1, replacePattern2, replacePattern3;

            //URLs starting with http://, https://, or ftp://
            replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
            replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

            //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
            replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

            //Change email addresses to mailto:: links.
            replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
            replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

            return replacedText;
        }
                
                
                
//        $('#editor').wysiwyg();
//        $('.updateTopicEditor').wysiwyg();
    </script>
    <script>
        $('#category-filter-select').change(function () {
            $('.filter-form').submit();
        });
        $(document).on('change', '.btn-file :file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [numFiles, label]);
        });

        $(document).ready( function() {
            $('.topic-description').each(function() {
                $(this).html(linkify($(this).html()));
                console.log('checked one');
            });
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;
                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            });
        });
    </script>
@stop