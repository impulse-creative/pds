
function clickPermission(listItem) {
    var id = listItem.data('id');
    if(listItem.hasClass('label-success')) {
        listItem.removeClass('label-success');
        return removePermission(id);
    }
    listItem.addClass('label-success');
    return addPermissionsToHiddenInput(id);
}
function removePermissionsFromArray(id, array) {
    var permissions = '';
    for(var i = 0; i < array.length; i++) {
        if(array[i].indexOf(id) != 1) {
            permissions +=array[i]+'-';
        } 
    }
    return permissions;
}

function removePermission(id) {
    currentString = $('#permissions-container').val();
    var permissionArray = currentString.split('-');
    newString = removePermissionsFromArray(id, permissionArray);
    $('#permissions-container').val(newString);
}

function addPermissionsToHiddenInput(id) {
    jsonString = '['+id+']-';
    currentString = $('#permissions-container').val();
    $('#permissions-container').val(currentString + jsonString);
}

$(document).ready(function() {
   //console.log('PDS::Access Groups Admin Loaded and processing. Approach your biffels with friendly arms only.  Friend Zoned Admins Only. Prepare docking. Reset. Ford the river with your oxen.');
    $('.list-permissions').on('click', 'li', function(e) {
        e.preventDefault();
        clickPermission($(this));
    });
    $('body').on('click', '#permission-action', function() {
        addPermissionToHiddenInput();
    });
});