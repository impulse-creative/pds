// call this from the developer console and you can control both instances
var calendars = {};

function cleanForId(string) {
    var lather = string.replace('/\s+/g', '_'); // Replaces all spaces with underscores.
    var rinse = string.replace('/[^A-Za-z0-9\-]/g', '') // Removes special chars
    var result = string.toLowerCase(); // makes lower case
    return result;
}

function createEventContainer(url, date, location, title, eventDescripton, event_type)
{
  var html = '<li class="timeline-inverted">';
  html += '<div class="timeline-badge default"><div class="timeline-badge-inner primary"></div></div>'; 
  html += '<div class="timeline-panel">';
  html += '<div class="timeline-heading">';
  html += '<h4 class="timeline-title"><a class="purple-text" href="'+url+'">'+title+'</a></h4>';
  html += '</div>';
  html += '<div class="timeline-body">';
  html += '<div class="timeline-description">';
  //cut off description if it's too big for the container; it would stretch out the box too much
  if(eventDescripton.length > 150) { var descr = eventDescripton.substring(0, 150)+'...'; } else { var descr = eventDescripton; }
  html += '<p>'+descr+'</p>';
  html += '</div>';
  if (event_type == 4) {
    var event_type_or_location = 'Event Location:';
  }else{
    var event_type_or_location = 'Event Type:';
  };
  html += '<p class="timeline-location">'+event_type_or_location+' <span class="orange-text">'+location+'</span>';
  html += '</p>';
  html += '</div>';
  html += '<a href="'+url+'" class="timeline-see-more"><i class="glyphicon glyphicon-chevron-right"></i></a>';
  html += '</div>';
  html += '</li>';

  return html;
}

$(document).ready( function() {

  // assuming you've got the appropriate language files,
  // clndr will respect whatever moment's language is set to.
  // moment.locale('ru');


  // PARDON ME while I do a little magic to keep these events relevant for the rest of time...
  var currentMonth = moment().format('YYYY-MM');
  var nextMonth    = moment().add('month', 1).format('YYYY-MM');

  //ajax calls to each Pharmacy Document section tables and list its relevant data in the appropriate table
  var urlbase = window.location.protocol + "//" + window.location.host + '/events/get/all/events/';
  // run ajax request
  $.ajax({
      type: "POST",
      cache: false,
      url: urlbase,
      success: function (d) {
          // replace calendar content with returned data
          //console.log(d);
          var eventArray = [];
          $.each(d, function(key,value){
            var obj = {};
            var location;
            obj['date'] = value['date'];
            obj['title'] = value['title'];
            obj['event_location'] = value['event_location'].length > 0 ? value['event_location'] : value['event_location'].type;
            obj['url'] = value['url'];
            obj['event_type'] = value['event_type'];
            obj['event_description'] = value['event_description'];
            eventArray.push(obj);
            
          });
          
          calendars.clndr2 = $('.cal2').clndr({
            template: $('#template-calendar').html(),
            events: eventArray,
            startWithMonth: moment(),
            clickEvents: {
              click: function(target) {
                if(target.events.length) {
                  var daysContainer = $('.cal2 .clndr').find('.days');
                  var eventContainer = [];
                  var event_date = target.events[0].date;
                  for (var i = 0; i <= target.events.length - 1; i++) {
                      eventContainer[i] = createEventContainer(target.events[i].url, target.events[i].date, target.events[i].event_location, target.events[i].title, target.events[i].event_description, target.events[i].event_type);
                  };
                  daysContainer.find('.modal-body .timeline-list .timeline').html(eventContainer.join(' '));
                };
              }
            },
            forceSixRows: true,
            doneRendering: function(){ 
              $('.cal2 .clndr .days-of-the-week .header-day').wrapAll('<div class="week-days"></div>');

              var allElements = $('.cal2 .clndr .days-of-the-week .days .day'),
                  WRAP_BY = 7;
              for (var i = 0; i < allElements.length; i += WRAP_BY) {
                  //first loop, elements 0 : 7, next loop elements 7 : 14 and so on
                  allElements.slice(i, i + WRAP_BY).wrapAll('<div class="week" />');
              }

              $('.cal2 .clndr .clndr-today-button').addClass('btn btn-primary');

            }
          });

          // bind both clndrs to the left and right arrow keys
          $(document).keydown( function(e) {
            if(e.keyCode == 37) {
              // left arrow
              calendars.clndr2.back();
            }
            if(e.keyCode == 39) {
              // right arrow
              calendars.clndr2.forward();
            }
          });
      },
      error: function(xhr, textStatus, thrownError) {
         //console.log(xhr);
         //console.log(textStatus);
         //console.log(thrownError);
      } 
  }, "json");
  //console.log(events);

});