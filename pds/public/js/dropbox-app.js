var client      = new Dropbox.Client({ key: qel6b4dulmfo3v6 }),
    storeFileApp    = storeFileApp || {
 
        storedFileList: null,
 
        init: function() {
			client.authenticate({
        		interactive: false
			}, function( error, response ) {
    			if ( error ) {
        			//console.log( 'OAuth error: ' + error );
    			}
			});
			storeFileApp.checkClient();
        },
 
        checkClient: function() {
        	if ( client.isAuthenticated() ) {
        		$( '#db-auth-check' ).fadeOut();
        		$( '#main' ).fadeIn();
        		client.getDatastoreManager().openDefaultDatastore( function( error, Datastore ) {
    				if ( error ) {
        				//console.log( 'Datastore error: ' + error );
    				}
    				storedFileList   = Datastore.getTable( 'pds_resources' );
 
    				storeFileApp.updateResources();
    				Datastore.recordsChanged.addListener( storeFileApp.updateResources );
				});
				$('#add-file').submit( storeFileApp.createResources );
    		} else {
        		$( '#main' ).fadeOut();
        		$( '#db-auth-check' ).click( function() {
   	 				client.authenticate();
				});
    		}
        },
 
        createResources: function( e ) {
        	e.preventDefault();

        	storedFileList.insert({
        		pds_resources: $('placeholder').val(),//link string here
        		created: new Date(),
        		completed: false
        	});
        	$('placeholder')val();
        },
 
        updateResources: function() {
				var list    = $( '#resource-list' ),
    			records = storedFileList.query();
 
				list.empty();
 
				for ( var i = 0; i &lt; records.length; i++ ) {
    				var record  = records[i],
        			item    = list.append(
            			$( '&lt;li&gt;' ).attr( 'data-record-id', record.getId() ).append(
                			$( '&lt;button&gt;' ).html( '&amp;times;' )
            			).append(
                			$( '&lt;input type="checkbox" name="completed" class="task_completed"&gt;' )
            			).append(
                			$( '&lt;span&gt;' ).html( record.get( 'todo' ) )
            			).addClass( record.get( 'completed' ) ? 'completed' : '' )
        			)		
 
        			if ( record.get( 'completed' ) ) {
            			$( 'input', item ).attr( 'checked', 'checked' );
        			}
				}
				//deleting a record
				$( 'li button' ).click( function( e ) {
    				e.preventDefault();
 
    				var id  = $( this ).parents( 'li' ).attr( 'data-record-id' );
    				storedFileList.get( id ).deleteRecord();
				});
				//updating a record
				$( 'li input' ).click( function( e ) {
    				var el  = $( e.target ),
        			id  = el.parents( 'li' ).attr( 'data-record-id' );
 
    				storedFileList.get( id ).set( 'completed', el.is( ':checked' ) );
				});
        }
    };
$( 'document' ).ready( storeFileApp.init );