$(document).ready(function(){
    if($('.e-types').val() == 4) {
        $('.regional-details').slideDown();
    }
    
    if(!$('.is-free-check input').is(':checked')) {
        $('.paid-event').show();
        $('.free-event').hide();
    } else {
        $('.paid-event').hide();
        $('.free-event').show();
    }
   
    /*-- initialize datetime picker for new resources. Used for resource expiration --*/
    if($('#start_date').length > 0) {
        $('#datetimepicker5, #datetimepicker5 input').datetimepicker();
        $('#start_date, #start_date input').datetimepicker();
        $('#end_date, #end_date input').datetimepicker();
    }
    

    /*-- initialize each wysiwig editor for adding events page --*/
    if($('body').hasClass('wysihtml5-supported')) {
        var editor = new wysihtml5.Editor("editor", { // id of textarea element
	  toolbar:      "wysihtml5-toolbar", // id of toolbar element
	  parserRules:  wysihtml5ParserRules // defined in parser rules set 
	});
	var editor = new wysihtml5.Editor("editor1", { // id of textarea element
	  toolbar:      "wysihtml5-toolbar-1", // id of toolbar element
	  parserRules:  wysihtml5ParserRules // defined in parser rules set 
	});
	var editor = new wysihtml5.Editor("editor2", { // id of textarea element
	  toolbar:      "wysihtml5-toolbar-2", // id of toolbar element
	  parserRules:  wysihtml5ParserRules // defined in parser rules set 
	});
	var editor = new wysihtml5.Editor("editor3", { // id of textarea element
	  toolbar:      "wysihtml5-toolbar-3", // id of toolbar element
	  parserRules:  wysihtml5ParserRules // defined in parser rules set 
	});
    }
	

    /*-- New Events: store html description before submit --*/
    $('.add-new-event').submit(function(e) {
	    /*-- get rid of bullshit iframe --*/
	    if($('iframe').length > 4) {
	    	console.log('here');
	    	$('iframe').eq(0).remove();
	    }
    });

    /*-- if user says the event is free, then hide the option to enter prices --*/
    $('.is-free-check input').on('click', function() {
       //console.log('click');
        if($('.paid-event').is(':hidden')) {
            $('.paid-event').show();
            $('.free-event').hide();
        } else {
            $('.paid-event').hide();
            $('.free-event').show();
            
        }
        
    });

    $('.is-member-only-event input').on('click', function() {
       //console.log('click');
        if ($('.non_member').is(':hidden')) {
            $('.non_member').show();
        } else {
            $('.non_member').hide();
        };
    });
        
    $('.e-types').change(function(){
    	if ($(this).val() == 4) {
    		$('.regional-details').slideDown();
    	} else {
    		$('.regional-details').slideUp();
    	};
    });

});