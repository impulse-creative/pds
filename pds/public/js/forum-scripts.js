//jquery scripts for the Forums in Forum manager in the dashboard
//Author: Derek J. Foster

$( document ).ready(function() {


  $('.edit-category').on('click', function(e)
  {
      $('#updateCategory').modal('show');
     //console.log($(this).data('category-id'));
      $.session.clear(); //destroys any garbage session data from any previous ajax calls
      var baseUrl = window.location.protocol+"//"+window.location.host + '/admin';
      $.ajaxSetup({
          headers: {
              'X-CSRF-Token': $('meta[name="_token"]').attr('content')
          }
      });
      $.ajax({
          type: 'post',
          cache: false,
          url: baseUrl+"/manage/pds-forum/update/category/"+$(this).data('category-id'), 
          success: function(data) {
              var jsonConvert = $.parseJSON(data);//converts response object to json
                $.each(jsonConvert, function(key, value)//parses through json and constructs output
                {
                    switch(key)
                    {
                        case 'id':
                            //console.log(value);
                            $('body .category_hidden_id').val(value);
                            break;
                        case 'title': 
                            $('body .cat-title').val(value);
                            //console.log(value);
                            break;
                        case 'subtitle':
                            $('body .cat-subtitle').val(value);
                            //console.log(value);
                            break;
                        case 'admin_only':
                            //console.log(value);
                            break;
                        default:
                            //console.log('failure');
                            break;
                    }
                });
          },
          error: function(xhr, textStatus, thrownError) {
              //console.log(xhr);
              //console.log(textStatus);
              //console.log(thrownError);
          }
      }, "json");
  });


  $('.add-topic-btn').on('click', function()
  {
      $('body .categoryTopic_hidden_id').val($(this).data('cat-id'));
  });

  /*-- Script to show access level list when creating a new category for the message board --*/
  $('.member-rad-btn').on('click', function(){
      if ( $(this).is(':checked') ) {
          $('.access_level_group').show();
      } 
      else {
          $('.access_level_group').hide();
      }
  });

});