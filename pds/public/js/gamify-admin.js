
function clickAchievement(listItem) {
    var id= listItem.data('id');
    if(listItem.hasClass('label-success')) {
        listItem.removeClass('label-success');
        return removeAchievement(id);
    }
    listItem.addClass('label-success');
    return addAchievement(id);

}

function addAchievement(id) {
    $('#startModal').trigger('click');
    $('#achivement-total').focus();
    $('#achievement-id').val(id);
}

function removeAchievementFromArray(id, array) {
    var achievements = '';
    for(var i = 0; i < array.length; i++) {
        if(array[i].indexOf(id) != 1) {
            achievements +=array[i]+'-';
        } 
    }
    return achievements;
}


function removeAchievement(id) {
    currentString = $('#achievement-container').val();
    var achievementArray = currentString.split('-');
    newString = removeAchievementFromArray(id, achievementArray);
    $('#achievement-container').val(newString);
}

function addAchievementToHiddenInput() {
    id = $('#achievement-id').val();
    total = $('#achievement-total').val();
    jsonString = '['+id+' : '+total+']-';
    $('#achievement-id').val('');
    $('#achievement-total').val('');
    currentString = $('#achievement-container').val();
    $('#achievement-container').val(currentString + jsonString);
}

$(document).ready(function() {
   //console.log('PDS::Gamify Admin Loaded and processing. Approach your helmets with caution.  Fun Zone Admins Only.');
    $('.list-achievements').on('click', 'li', function(e) {
        e.preventDefault();
        clickAchievement($(this));
    });
    $('body').on('click', '#achievement-action', function() {
        addAchievementToHiddenInput();
    });
});