/* Load This stuff up */

function checkActiveMenu() {
    if(window.location.href.indexOf('message-boards') > 1) {
        return addActive(0);
    }
    if(window.location.href.indexOf('events') > 1) {
        return addActive(1);
    }
    if(window.location.href.indexOf('library') > 1) {
        return addActive(2);
    }
    if(window.location.href.indexOf('my-pharmacy') > 1) {
        return addActive(3);
    }
    if(window.location.href.indexOf('pdsmarketplace') > 1) {
        return addActive(4);
    }
    
}

function addActive(menuOption) {
    $('.menu li').eq(menuOption).addClass('active-nav');
}

function addNotification(id, type) {
   //console.log(id+' '+type);
    var dataString = 'id='+id+'&type='+type;
    var baseUrl = window.location.protocol + "//" + window.location.host + "/";
    ajaxAddNotification(dataString, baseUrl);
}

function blink(div, times) {
    $(div).on('click', function() {
        return;
    });
    for(var n = 0; n < times; n++) {
        for(var i = 0; i < 1; i++) {
            $(div).fadeTo(700, 0.1).fadeTo(700, 1.0);    
        }
    }
}

function ajaxAddNotification(dataString, baseUrl) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        cache: false,
        url: baseUrl+"ajax/notification/add", //url to post ajax info to
        data: dataString,
        success: function(response) {
           //console.log('success');
            $('body').after('<div class="pop-notifications top-right"><div>'+response+'</div></div>');
            $('.pop-notifications').delay(1200).fadeOut();
        },
        error: function(xhr, textStatus, thrownError, url) {
           //console.log(url);
           //console.log(xhr);
           //console.log(textStatus);
           //console.log(thrownError);
        }
    }, "json");
}

function editProfile() {
    $('#getProfile').trigger('click');
}

jQuery(document).ready(function() {
    checkActiveMenu();
    //-->Global data confirm class
    $("body").on("click", ".confirm", function() {
        return confirm($(this).data("confirm")); 
    });

    
    if(window.location.href.indexOf('localhost') == -1) {
       //console.log('Welcome internet user');
    } else {
       //console.log('pds::advantage initiated.  Prepare your console');
    }
    
    if(window.location.href.indexOf('#profile') == -1) {
       //console.log('You want to edit your profile? Well you did not enter the secret codes');
    } else {
       //console.log('pds::profile loaded.  Prepare your editing');
        editProfile();   
    }
    
    $('#makeMagic').on('click', function(e) {
        e.preventDefault();
       //console.log('magic is making');
        if($('#cog-menu').is(':hidden')) {
            $('#cog-menu').slideDown();
        } else {
            $('#cog-menu').slideUp();
        }
    });
    
    
    $('body').on('click', '.subscribe-button', function() {
        var id = $(this).data('id');
        var type = $(this).data('type');
        addNotification(id, type);
       //console.log('You like subscriptions?  Good. We are going to notify you!!!');
    });
    
  


    /* Google Analytics */
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-57420251-1', 'auto');
    ga('send', 'pageview');

    /* Tool Tips */
    $(function () {
        $("[rel='tooltip']").tooltip({
            placement: 'left',
            trigger: 'hover focus'
        });
        $(document).on('focus', '[rel=tooltip]', function () { 
            $(this).tooltip('show'); });

    });

    /* Slick Menu */
    
    /*!
        SlickNav Responsive Mobile Menu v1.0.1
        (c) 2014 Josh Cope
        licensed under MIT
    */
    ;(function(e,t,n){function o(t,n){this.element=t;this.settings=e.extend({},r,n);this._defaults=r;this._name=i;this.init()}var r={label:"MENU",duplicate:true,duration:200,easingOpen:"swing",easingClose:"swing",closedSymbol:"&#9658;",openedSymbol:"&#9660;",prependTo:"body",parentTag:"a",closeOnClick:false,allowParentLinks:false,nestedParentLinks:true,showChildren:false,init:function(){},open:function(){},close:function(){}},i="slicknav",s="slicknav";o.prototype.init=function(){var n=this,r=e(this.element),i=this.settings,o,u;if(i.duplicate){n.mobileNav=r.clone();n.mobileNav.removeAttr("id");n.mobileNav.find("*").each(function(t,n){e(n).removeAttr("id")})}else{n.mobileNav=r}o=s+"_icon";if(i.label===""){o+=" "+s+"_no-text"}if(i.parentTag=="a"){i.parentTag='a href="#"'}n.mobileNav.attr("class",s+"_nav");u=e('<div class="'+s+'_menu"></div>');n.btn=e(["<"+i.parentTag+' aria-haspopup="true" tabindex="0" class="'+s+"_btn "+s+'_collapsed">','<span class="'+s+'_menutxt">'+i.label+"</span>",'<span class="'+o+'">','<span class="'+s+'_icon-bar"></span>','<span class="'+s+'_icon-bar"></span>','<span class="'+s+'_icon-bar"></span>',"</span>","</"+i.parentTag+">"].join(""));e(u).append(n.btn);e(i.prependTo).prepend(u);u.append(n.mobileNav);var a=n.mobileNav.find("li");e(a).each(function(){var t=e(this),r={};r.children=t.children("ul").attr("role","menu");t.data("menu",r);if(r.children.length>0){var o=t.contents(),u=false;nodes=[];e(o).each(function(){if(!e(this).is("ul")){nodes.push(this)}else{return false}if(e(this).is("a")){u=true}});var a=e("<"+i.parentTag+' role="menuitem" aria-haspopup="true" tabindex="-1" class="'+s+'_item"/>');if(!i.allowParentLinks||i.nestedParentLinks||!u){var f=e(nodes).wrapAll(a).parent();f.addClass(s+"_row")}else e(nodes).wrapAll('<span class="'+s+"_parent-link "+s+'_row"/>').parent();t.addClass(s+"_collapsed");t.addClass(s+"_parent");var l=e('<span class="'+s+'_arrow">'+i.closedSymbol+"</span>");if(i.allowParentLinks&&!i.nestedParentLinks&&u)l=l.wrap(a).parent();e(nodes).last().after(l)}else if(t.children().length===0){t.addClass(s+"_txtnode")}t.children("a").attr("role","menuitem").click(function(t){if(i.closeOnClick&&!e(t.target).parent().closest("li").hasClass(s+"_parent")){e(n.btn).click()}});if(i.closeOnClick&&i.allowParentLinks){t.children("a").children("a").click(function(t){e(n.btn).click()});t.find("."+s+"_parent-link a:not(."+s+"_item)").click(function(t){e(n.btn).click()})}});e(a).each(function(){var t=e(this).data("menu");if(!i.showChildren){n._visibilityToggle(t.children,null,false,null,true)}});n._visibilityToggle(n.mobileNav,null,false,"init",true);n.mobileNav.attr("role","menu");e(t).mousedown(function(){n._outlines(false)});e(t).keyup(function(){n._outlines(true)});e(n.btn).click(function(e){e.preventDefault();n._menuToggle()});n.mobileNav.on("click","."+s+"_item",function(t){t.preventDefault();n._itemClick(e(this))});e(n.btn).keydown(function(e){var t=e||event;if(t.keyCode==13){e.preventDefault();n._menuToggle()}});n.mobileNav.on("keydown","."+s+"_item",function(t){var r=t||event;if(r.keyCode==13){t.preventDefault();n._itemClick(e(t.target))}});if(i.allowParentLinks&&i.nestedParentLinks){e("."+s+"_item a").click(function(e){e.stopImmediatePropagation()})}};o.prototype._menuToggle=function(e){var t=this;var n=t.btn;var r=t.mobileNav;if(n.hasClass(s+"_collapsed")){n.removeClass(s+"_collapsed");n.addClass(s+"_open")}else{n.removeClass(s+"_open");n.addClass(s+"_collapsed")}n.addClass(s+"_animating");t._visibilityToggle(r,n.parent(),true,n)};o.prototype._itemClick=function(e){var t=this;var n=t.settings;var r=e.data("menu");if(!r){r={};r.arrow=e.children("."+s+"_arrow");r.ul=e.next("ul");r.parent=e.parent();if(r.parent.hasClass(s+"_parent-link")){r.parent=e.parent().parent();r.ul=e.parent().next("ul")}e.data("menu",r)}if(r.parent.hasClass(s+"_collapsed")){r.arrow.html(n.openedSymbol);r.parent.removeClass(s+"_collapsed");r.parent.addClass(s+"_open");r.parent.addClass(s+"_animating");t._visibilityToggle(r.ul,r.parent,true,e)}else{r.arrow.html(n.closedSymbol);r.parent.addClass(s+"_collapsed");r.parent.removeClass(s+"_open");r.parent.addClass(s+"_animating");t._visibilityToggle(r.ul,r.parent,true,e)}};o.prototype._visibilityToggle=function(t,n,r,i,o){var u=this;var a=u.settings;var f=u._getActionItems(t);var l=0;if(r){l=a.duration}if(t.hasClass(s+"_hidden")){t.removeClass(s+"_hidden");t.slideDown(l,a.easingOpen,function(){e(i).removeClass(s+"_animating");e(n).removeClass(s+"_animating");if(!o){a.open(i)}});t.attr("aria-hidden","false");f.attr("tabindex","0");u._setVisAttr(t,false)}else{t.addClass(s+"_hidden");t.slideUp(l,this.settings.easingClose,function(){t.attr("aria-hidden","true");f.attr("tabindex","-1");u._setVisAttr(t,true);t.hide();e(i).removeClass(s+"_animating");e(n).removeClass(s+"_animating");if(!o){a.close(i)}else if(i=="init"){a.init()}})}};o.prototype._setVisAttr=function(t,n){var r=this;var i=t.children("li").children("ul").not("."+s+"_hidden");if(!n){i.each(function(){var t=e(this);t.attr("aria-hidden","false");var i=r._getActionItems(t);i.attr("tabindex","0");r._setVisAttr(t,n)})}else{i.each(function(){var t=e(this);t.attr("aria-hidden","true");var i=r._getActionItems(t);i.attr("tabindex","-1");r._setVisAttr(t,n)})}};o.prototype._getActionItems=function(e){var t=e.data("menu");if(!t){t={};var n=e.children("li");var r=n.find("a");t.links=r.add(n.find("."+s+"_item"));e.data("menu",t)}return t.links};o.prototype._outlines=function(t){if(!t){e("."+s+"_item, ."+s+"_btn").css("outline","none")}else{e("."+s+"_item, ."+s+"_btn").css("outline","")}};o.prototype.toggle=function(){var e=this;e._menuToggle()};o.prototype.open=function(){var e=this;if(e.btn.hasClass(s+"_collapsed")){e._menuToggle()}};o.prototype.close=function(){var e=this;if(e.btn.hasClass(s+"_open")){e._menuToggle()}};e.fn[i]=function(t){var n=arguments;if(t===undefined||typeof t==="object"){return this.each(function(){if(!e.data(this,"plugin_"+i)){e.data(this,"plugin_"+i,new o(this,t))}})}else if(typeof t==="string"&&t[0]!=="_"&&t!=="init"){var r;this.each(function(){var s=e.data(this,"plugin_"+i);if(s instanceof o&&typeof s[t]==="function"){r=s[t].apply(s,Array.prototype.slice.call(n,1))}});return r!==undefined?r:this}}})(jQuery,document,window)
    
    $(function(){
        $('#menu').slicknav();
        //$('.slicknav_menu > a').before('<a class="mobile-logo" href="http://www.pharmacyowners.hs-sites.com/"><img src="//cdn2.hubspot.net/hub/37772/file-1513961891-png/Conference_Images/pds-logo.png?t=1414165617864&t=1415809653075" /></a>');
    });
    
    /* Craaaazy Egg */
    setTimeout(function(){var a=document.createElement("script");
    var b=document.getElementsByTagName("script")[0];
    a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0020/4787.js?"+Math.floor(new Date().getTime()/3600000);
    a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);

    /* Uservoice */
   //console.log('marketplace: ' + window.location.href.indexOf('marketplace'));
    if(window.location.href.indexOf('marketplace') == -1) {

        // Include the UserVoice JavaScript SDK (only needed once on a page)
        UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/CpvGmwUW16zfavNwxohttQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

        //
        // UserVoice Javascript SDK developer documentation:
        // https://www.uservoice.com/o/javascript-sdk
        //

        // Set colors
        UserVoice.push(['set', {
          accent_color: '#e2753a  ',
          trigger_color: 'white',
          trigger_background_color: '#e2753a  '
        }]);
    
        UserVoice.push(['set', 'ticket_custom_fields', {'toaccount': 'http://app.pharmacyowners.com/dashboard/accounts/'+$('#uv-email-address').text()}]);
        UserVoice.push(['set', 'ticket_custom_fields', {'approve': 'http://app.pharmacyowners.com/dashboard/approve/newUsers/'+$('#uv-email-address').text()}]);
        UserVoice.push(['set', 'ticket_custom_fields', {'salesforce': 'http://na2.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=005&sen=00P&sen=006&sen=00Q&sen=a1J&sen=001&sen=015&sen=00T&sen=003&sen=00U&sen=a0s&sen=00O&sen=701&str=' + $('#uv-email-address').text()}]);
        UserVoice.push(['set', 'ticket_custom_fields', {'hubspot': 'http://app.hubspot.com/contacts/37772/contact/'+$('#uv-vid').text()+'/overview/'}]);
        UserVoice.push(['set', 'ticket_custom_fields', {'ghost': 'http://app.pharmacyowners.com/dashboard/accounts/ghost/user/'+$('#uv-id').text()}]);
        // Identify the user and pass traits
        // To enable, replace sample data with actual user traits and uncomment the line
        UserVoice.push(['identify', {
            email: $('#uv-email-address').text(),
            name: $('#uv-name').text(),
            id: $('#uv-id').text()
            
          //email:      'john.doe@example.com', // User’s email address
          //name:      'John Doe', // User’s real name
          //created_at: 1364406966, // Unix timestamp for the date the user signed up
          //id:        123, // Optional: Unique id of the user (if set, this should not change)
          //type:      'Owner', // Optional: segment your users by type
          //account: {
          //  id:          123, // Optional: associate multiple users with a single account
          //  name:        'Acme, Co.', // Account name
          //  created_at:  1364406966, // Unix timestamp for the date the account was created
          //  monthly_rate: 9.99, // Decimal; monthly rate of the account
          //  ltv:          1495.00, // Decimal; lifetime value of the account
          //  plan:        'Enhanced' // Plan name for the account
          //}
        }]);

        // Add default trigger to the bottom-right corner of the window:
        UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-right' }]);

        // Or, use your own custom trigger:
        //UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

        // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
        UserVoice.push(['autoprompt', {}]);
    }
    
});