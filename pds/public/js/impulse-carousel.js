(function ($) {
    $.fn.impulsefadecarousel = function(options) {
        //default options
        var settings = $.extend({
            pause: 3000,
            motion: 500
        }, options);

        $(this).hide().eq(0).show(); //hide all slides except first

        var slides = $(this);
        var count = slides.length;
        var i = 0;

        setTimeout(transition, settings.pause);

        //transition between marketplace slides
        function transition() {
            slides.eq(i).animate({opacity: 'toggle', top: '40px'}, settings.motion);
            //reset so we don't keep fucking stopping
            if(++i>=count) {
                i=0;
            };
            slides.eq(i).animate({opacity: 'toggle', top: '20px'}, settings.motion);
            setTimeout(transition, settings.pause);
        };
    };
}( jQuery ));