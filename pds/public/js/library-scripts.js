$(document).ready(function () {
    $('.file-url').hide();
    $('.wistia').hide();
    if ($('#is_link').is(':checked')) {
       //console.log('islink');
        $('.file-url').show();
        $('.file-upload').hide();
    }
    if ($('#is_wistia').is(':checked')) {
       //console.log('wistia');
        $('.file-url').show();
        $('.file-upload').hide();
    }
    /*-- initialize datetime picker for new resources. Used for resource expiration --*/
    $('#datetimepicker5').datetimepicker();

    /*-- library new resource: store html description before submit --*/
    $('.add-new-resource-form').submit(function (e)
    {
        var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
        $('#hidden-html').val(html);
    });

    /*-- for resource share links --*/
    if (window.location.hash) {
        var hash = window.location.hash; //Puts hash in variable
        //alert (hash);
        // hash found
        $(hash).modal('show');
    } else {
        //no hash found
        //alert('No Hash variable found in the url');
    }

    $('.new-file-btn').on('click', function () {
        $('.edit-section .thumbnail').hide();
        $('.new-file').removeClass('hidden');
    });

    $('.cancel-new-file a').on('click', function () {
        resetFormElement($('.btn-file  input'));
    });

    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {
        $('.btn-file').after('&nbsp;' + label);
    });
    
    $('body').on('click', '#is_link, #is_wistia', function () {
        if ($('.file-url').is(':hidden')) {
            $('.file-url').show();
            $('.file-upload').hide();
        } else {
            $('.file-url').hide();
            $('.file-upload').show();
        }
    });


    /* MULTIPLE FILE DOWNLOAD */
    $('.dwldFilesBtn').on('click', function (e) {
        e.preventDefault();
        var checkedValues = $(".featured-resource input:checkbox:checked").map(function(){
            return $(this).val();
        }).get();
        
        if(checkedValues.length == 0) {
            alert('Please select resources to download.');
        }
        //uncheck boxes
        $('input[name=dwldChoice]').attr('checked', false);
        $.session.clear(); //destroys any garbage session data from any previous ajax calls
        var baseUrl = window.location.protocol + "//" + window.location.host + '/library/';
       //console.log(baseUrl + "resources/multi-download?file_name=" + checkedValues);
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            cache: false,
            url: baseUrl + "resources/multi-download?file_name=" + checkedValues,
            success: function (data) {
                if(data === 'link'){
                    alert('Cannot download videos.');
                }
                if ($('.hidden-iframe').is(':hidden')) {
                    $('.hidden-iframe').replaceWith('<div class="hidden-iframe" style="display:none;"><iframe src="' + data + '"></iframe></div>');
                } else {
                    $('body').append('<div class="hidden-iframe" style="display:none;"><iframe src="' + data + '"></iframe></div>');
                }
                checkedValues = [];
            },
            error: function (xhr, textStatus, thrownError) {
                checkedValues = [];
               //console.log(xhr);
               //console.log(textStatus);
               //console.log(thrownError);
            }
        }, "json");
    });

});

$(document).on('change', '.btn-file :file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

function resetFormElement(e) {
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
    $('.new-file').addClass('hidden');
    $('.edit-section .thumbnail').show();
}

/* HTML EDITOR */
function add_html_editor() {
    var editor = new wysihtml5.Editor("editor", {// id of textarea element
        toolbar: "wysihtml5-toolbar", // id of toolbar element
        parserRules: wysihtml5ParserRules, // defined in parser rules set 
        stylesheets: ['{{ URL::to(' / ') }}/css/Conference/2015/wysiwig-styles.css'],
        supportTouchDevices: true
    });
    editor.on("load", function () {
        check_first_words($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor'));
        //console.log('loaded');
        //console.log($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').text());
    });

    editor.on("newword:composer", function () {
        //console.log('new word added');
        //console.log('--------');
        check_words($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor'));
    });
    jQuery('textarea').change(function () {
        var words = $(this).val().split(/\b[\s,\.-:;]*/);
        if (words.length > maxWords) {
            words.splice(maxWords);
            $(this).val(words.join(""));
            alert("You've reached the maximum allowed words. Extra words removed.");
        }
    });
}