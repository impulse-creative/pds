function displayManualConfirmationForm() {
    $('#manual-confirmation').slideUp().removeClass('hidden').slideDown('slow');
}
function checkConfirmationForm() {
    var email = $('input#usr-email').val();
    if(email == '') {
        return;
    };
    var dataString = $('form').serializeArray();
    var secret = '6Lc7e_8SAAAAABiwwVvvyg7pWjntWyZ4fI291G8P';
    /*$.ajax({
       type: 'get',
       url: 'https://www.google.com/recaptcha/api/siteverify?secret='.secret.'&response='.response;
    });*/
    
    
    var baseUrl = window.location.protocol + "//" + window.location.host + "/";
        
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        cache: false,
        url: baseUrl+"confirm/email/manual", //url to post ajax info to
        data: dataString,
        success: function(response) {
           //console.log(response);
            $.ajax({
                type: 'get',
                cache: false,
                url: response,
                success: function(secondResponse) {
                   //console.log(secondResponse);
                },
                error: function(xhr, textStatus, thrownError) {
                   //console.log(xhr);
                   //console.log(textStatus);
                   //console.log(thrownError);
                }
            }, "json");
            /*
            if(response.indexOf('signuptime') != -1) {
                removeAllAlerts();
                var message = response.replace('signuptime', '');
                $('#usr-email').before(message);
                displayRegistration();
                checkIfInHubspot(email, dataString, baseUrl);
            } else {
                removeAllAlerts();
                $('#usr-email').before(response);
                displayLoginForm();
            } */
            
        },
        error: function(xhr, textStatus, thrownError) {
           //console.log(xhr);
           //console.log(textStatus);
           //console.log(thrownError);
        }
    }, "json");
}


$(function() {
    $('body').on('click', '#start-confirmation', function(e) {
        e.preventDefault();
        displayManualConfirmationForm();
    });
    $('body').on('click,', '#confirm-email-address', function(e) {
        e.preventDefault();
        checkConfirmationForm();
    });
    
});