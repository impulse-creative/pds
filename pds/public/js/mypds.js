
$(document).ready(function(){
    var company_title = $('.pharmacy-profile-btn');
    var original_title = $('.pharmacy-profile-btn').html();
    company_title.on('click', function() {
        if($('.modified').length){
            $(this).removeClass('modified');
            $(this).html(original_title);
        }else{
            $(this).addClass('modified');
            $('.pharmacy-profile-btn').html('Company Details');   
        }
    });
}); 