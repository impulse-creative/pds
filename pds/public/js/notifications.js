





function hideNotification(id, type) {
   //console.log(id+' '+type);
    var dataString = 'id='+id+'&type='+type;
    var baseUrl = window.location.protocol + "//" + window.location.host + "/";
    ajaxNotification(dataString, baseUrl);
}

function ajaxNotification(dataString, baseUrl) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        cache: false,
        url: baseUrl+"ajax/notification/hide", //url to post ajax info to
        data: dataString,
        success: function(response) {
           //console.log('success');
            $('body').after('<div class="pop-notifications top-right"><div>'+response+'</div></div>');
            $('.pop-notifications').delay(500).fadeOut();
        },
        error: function(xhr, textStatus, thrownError, url) {
           //console.log(url);
           //console.log(xhr);
           //console.log(textStatus);
           //console.log(thrownError);
        }
    }, "json");
}

$(document).ready(function() {
   //console.log('Your notification panel has been loaded');
    $('body').on('click', '.hide-notification', function(e) {
        var id = $(this).data('target-id');
        var type = $(this).data('type');
        hideNotification(id, type); 
    });
    
});

