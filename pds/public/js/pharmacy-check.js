function createLoadingAnimation() {
    var html = '<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>';
    return html;
}

function createListItem(id, name, address, owner_name) {
    address = address !== 'NULL' ? address : '--unavailable--';
    owner_name = owner_name !== 'NULL' ? owner_name : '--unavailable--';
    var html = '<a href="#" class="list-group-item confirm" id="company-'+id+'" data-confirm="Are you sure this is the correct company?">';
    html += '<h4 class="list-group-item-heading purple-text">'+name+'</h4>';
    html += '<p class="list-group-item-text">';
    html += 'Owner: <span class="orange-text">'+owner_name+'</span><br/>Address: <span class="orange-text">'+address+'</span></p></a>';
    return html;
}
function createNewPharmacy() {
    $('.pharmacy-list').hide();
    $('.new-pharmacy-submit').show();
    $('.create-new-pharmacy').show();
    $('.create-new-pharmacy #pharmacy_name').val($('.checking-for-existing-pharmacy #pharmacy_name').val());
    $('.create-new-pharmacy #pharmacy_address').val($('.checking-for-existing-pharmacy #pharmacy_address').val());
    $('.create-new-pharmacy #pharmacy_phone').val($('.checking-for-existing-pharmacy #pharmacy_phone').val());
    $('.create-new-pharmacy #owner_email').val($('.checking-for-existing-pharmacy #owner_email').val());   
    return blinkEmptyInputs('input');
}

function blinkEmptyInputs() {
    $('.create-new-pharmacy input').each(function() {
        if($(this).val() == '') {
            if(!$(this).hasClass('btn-primary')) {
                $(this).css('border', '1px solid #F58220');
                blink($(this), 2);
            } 
        }
    });
}

function noMatches() {
    alert('We couldn\'t find your Pharmacy.  You can create a new Pharmacy in the system or you can go back and try your search again.');
    $('.spinner').remove();
}

function addToPharmacy(id) {
    var html = '<div class="center orange-text medium-padding">';
    html += '<h2>Thank you.  The owner will verify you before you can join their Pharmacy. This window will close in just a moment.';
    $('.choose-employee-type').append(html);
    window.setTimeout(function() {
        window.location.href = window.location.protocol+"//"+window.location.host + '/dashboard/add/user/to/'+id+'?type='+$('input[type="radio"][name="employee_type"]:checked').val();
    }, 2000);
}

function vendorSelected() {
    var html = '<div class="center orange-text medium-padding">';
    html += '<h2>Thank you.  Your Vendor setup will happen at a later date. This window will close in just a moment.';
    $('.choose-employee-type').append(html);
    window.setTimeout(function() {
        window.location.href = window.location.protocol+"//"+window.location.host + '/dashboard/vendor/setup/'+$('#uv-id').text();
    }, 2000);
}

$(document).ready(function(){
    $('#pharmacySetup').modal('show');
    $('.new-pharmacy-submit').hide();
    $('.employee_type').on('click', function(){
        var type = $(this).val();
        if(type == '0') {
            return vendorSelected();
        } 
        $('.choose-employee-type').hide();
        $('.checking-for-existing-pharmacy').show();
        $('.setup-progress > div').removeClass('active');
        $('.setup-progress > div.step-two').addClass('active');
    });
    $('.back-to-employee-choice').on('click', function(){
        $('.choose-employee-type').show();
        $('.checking-for-existing-pharmacy').hide();
        $('.setup-progress > div').removeClass('active');
        $('.setup-progress > div.step-one').addClass('active');
    });
    $('.go-to-company-list').on('click', function(){
        $('.pharmacy-list').show();
        $('.checking-for-existing-pharmacy').hide();
        $('.new-pharmacy-submit').hide();
        $('.create-new-pharmacy').hide();
        $('.setup-progress > div').removeClass('active');
        $('.setup-progress > div.step-three').addClass('active');
    });
    $('.back-to-pharmacy-check').on('click', function(){
        $('.pharmacy-list').hide();
        $('.checking-for-existing-pharmacy').show();
        $('.setup-progress > div').removeClass('active');
        $('.setup-progress > div.step-two').addClass('active');
    });

    $('body').on('click', '.list-group-item', function(e) {
        e.preventDefault();
        var pharmacyId = $(this).attr('id').split('-')[1];
        addToPharmacy(pharmacyId);
    });

    $('body').on('click', '.none-are-me', function() {
        createNewPharmacy();
    });

    $('.go-to-company-list').on('click', function()
    {
        var baseUrl = window.location.protocol+"//"+window.location.host + '/dashboard/pharmacies/get/related/companies';
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
        var name = $('.checking-for-existing-pharmacy #pharmacy_name').val();
        var address = $('.checking-for-existing-pharmacy #pharmacy_address').val();
        var phone = $('.checking-for-existing-pharmacy #pharmacy_phone').val();
        var owner_email = $('.checking-for-existing-pharmacy #owner_email').val();
        $.ajax({
            type: 'post',
            cache: false,
            url: baseUrl,
            data: { name:name, address:address, phone:phone, owner_email:owner_email },
            success: function(data) {
                var jsonConvert = $.parseJSON(data); //converts response object to json
                var list_item = '';
                if(jsonConvert == '[]' || jsonConvert == '') {
                    return noMatches();
                }
                $.each(jsonConvert, function(key, value) {
                    list_item += createListItem(value['id'], value['name'], value['address'], value['owner_name']);
                });
                if(list_item.indexOf('undefined') > -1) {
                    list_item.replace('undefined', '');
                }
                $('.pharmacy-list .list-group').html(list_item);
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(xhr);
                console.log(textStatus);
                console.log(thrownError);
            }
        }, "json");
    });
});
