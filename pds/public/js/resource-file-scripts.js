//jquery scripts for the resources in resource manager in the administration dashboard
//Author: Derek J. Foster

$(document).ready(function () {

    //Add functionality for the javascript bootstrap tabs in resources
    $('#resourceTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    /* VARIABLES 
     ==================================================*/
    //Pharmacy Document variables
    var hr_form = $('#hr-modal');
    var showHrFormBtn = $('.hr-show-form');
    var hrFormCancel = $('.hr-form a');

    //Spreadsheet section variables
    var ss_form = $('#ss-modal');
    var showSSFormBtn = $('.ss-show-form');
    var ssFormCancel = $('.ss-form a');
    var ss_section = $('#spreadsheet-section');

    //Webinar section variables
    var w_form = $('#webinar-modal');
    var showWebFormBtn = $('.webinar-show-form');
    var wFormCancel = $('.webinar-form a');
    var w_section = $('#webinar-section');

    //Video section variables
    var v_form = $('#video-modal');
    var showVidFormBtn = $('.video-show-form');
    var vFormCancel = $('.video-form a');
    var v_section = $('#video-section');

    //Tab variables
    var hr_tab = $('.hr-tab');
    var m_tab = $('.marketing-tab');
    var t_tab = $('.tax-tab');

    //data attribute root element'
    var ajax_type = $('.ajax-fill').attr('data-r-type');


    /* APPLICATION RESOURCE LOGIC
     ==================================================*/

    //Control animations for showing each upload form for each section in the resources view//

    //-->Pharmacy Documents table form handlers
    showHrFormBtn.on('click', function (e) {
        e.preventDefault();
        hr_form.slideToggle('fast');
        $(this).hide();
    });

    hrFormCancel.on('click', function (e) {
        e.preventDefault();
        $(this).parent().parent().slideToggle('fast');
        showHrFormBtn.show();
    });

    //-->Spreadsheet table form handlers
    showSSFormBtn.on('click', function (e) {
        e.preventDefault();
        ss_form.slideToggle('fast');
        $(this).hide();
    });

    ssFormCancel.on('click', function (e) {
        e.preventDefault();
        $(this).parent().parent().slideToggle('fast');
        showSSFormBtn.show();
    });

    //-->Webinar table form handlers 
    showWebFormBtn.on('click', function (e)
    {
        e.preventDefault();
        w_form.slideToggle();
        $(this).hide();
    });

    wFormCancel.on('click', function (e)
    {
        e.preventDefault();
        $(this).parent().parent().slideToggle('fast');
        showWebFormBtn.show();
    });

    //-->Video table form handlers
    showVidFormBtn.on('click', function (e)
    {
        e.preventDefault();
        v_form.slideToggle();
        $(this).hide();
    });

    vFormCancel.on('click', function (e)
    {
        e.preventDefault();
        $(this).parent().parent().slideToggle('fast');
        showVidFormBtn.show();
    });

    //switch hidden field value for upload to work for specific resource type
    $('.ajax-fill').on('click', function ()
    {
        $('input[name=section]').val($(this).data('ajax-type'));
    });

    $('#phar-doc-section').on('click', function ()
    {
        $('input[name=section]').val($('.nav-tabs li.active').data('ajax-type'));
    });

    //data confirm pop up
    //-->Global data confirm class
    $("body").on("click", ".confirm", function () {
        return confirm($(this).data("confirm"));
    });

    $('.active-ajax').on("ajax.start", function ()
    {
        $(this).loading();
    });
    $('.active-ajax').on('ajax.stop', function ()
    {
        $(this).loading('stop');
    });

    //ajax calls to each Pharmacy Document section tables and list its relevant data in the appropriate table
    $('.ajax-fill').on('click', function (e)
    {
        var table = $(this).data('r-table');
        $('body').loading({
            message: "Retreiving Resources..."
        });

        var url_base = window.location.protocol + "//" + window.location.host;
        var download_url = window.location.protocol + "//" + window.location.host + "/admin/resources/show/";
        var tableType = $(this).data('r-type');
        var tableResource = $(this).data('ajax-type');
        var authType = $(this).data('auth-type');
        
        // run ajax request
        $.ajax({
            type: "POST",
            cache: false,
            url: url_base + "/admin/resources/tabledata/" + tableType,
            success: function (d) {

                $('body').loading('stop');
                var jsonConvert = $.parseJSON(d);//converts response object to json
                // replace div's content with returned data
                
                var tableData;
                var decodeThis;
                if (!$.isEmptyObject(jsonConvert)) {
                    $.each(jsonConvert, function (key, value)//parses through json and constructs output
                    {
                        var to_split = value["drop_box_link"];
                        var split_filepath = to_split.split('/');
                        tableData += '<tr>';
                        tableData += '<td><input name="dwldChoice" type="checkbox" value="' + value["id"] + '">&nbsp;<img src="' + window.location.protocol + "//" + window.location.host + '/storage/files/' + split_filepath[split_filepath.length - 1] + '" width="30" height="30"/>&nbsp;' + value["filename"] + '&nbsp;<a class="downloadThis confirm" data-confirm="Are you sure you want to download this resource?" href="' + download_url + value["filename"] + '" title="Download"><span class="glyphicon glyphicon-download-alt"></span></a>';
                        if (authType === "impulse_admin" || authType === "admin") {
                            tableData += '&nbsp;<span class="glyphicon glyphicon-pencil fileInfoChange editInfoIcon" data-toggle="modal" data-id="' + value["id"] + '" data-target="#changeFileInfo" title="Edit"></span>';
                        }
                        tableData += '</td>';
                        tableData += '<td>' + value["file_version"] + '</td>';
                        tableData += '<td>' + value["created_at"] + '</td>';
                        tableData += '<td>' + value["author"] + '</td>';
                        tableData += '<td id="dwnld-count">' + value["download_count"] + '</td>';
                        if (authType === 5 || authType === 1) {
                            tableData += '<td><a class="deleteThis confirm btn btn-danger" href="' + window.location.protocol + "//" + window.location.host + '/admin/resources/' + value["id"] + '" data-confirm="Are you sure you want to delete this resource?"><span class="glyphicon glyphicon-trash"></span>&nbsp;Trash</a></td>';
                        }
                        tableData += '</tr>';
                    });
                    $('#' + tableResource).html(tableData);
                } else {
                    if ($('.' + tableResource + '-no-files-alert').length === 0) {
                        $('#' + tableResource).parent().after('<tr><td><div class="alert alert-warning ' + tableResource + '-no-files-alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Nothing has been uploaded yet. To upload, click the "Add File" button.</div>');
                    }
                }
            },
            error: function (xhr, textStatus, thrownError) {
                //$('.active-ajax').trigger("ajax.stop");
                $('body').loading('stop');
                //console.log(xhr);
                //console.log(textStatus);
                //console.log(thrownError);
            }
        }, "json");
    });

    /* MULTIPLE FILE DOWNLOAD */
    var checkedValues = [];
    //storing multiple downloads that are clicked into an array 
    $('body').on('click', 'input[name=dwldChoice]', function ()
    {
        //console.log('clicked');
        checkedValues.push($(this).val());
    });

    $('.dwldFilesBtn').on('click', function (e)
    {
        e.preventDefault();
        //console.log(checkedValues);
        //uncheck boxes
        $('input[name=dwldChoice]').attr('checked', false);
        $.session.clear(); //destroys any garbage session data from any previous ajax calls
        var baseUrl = window.location.protocol + "//" + window.location.host + '/admin';
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            cache: false,
            url: baseUrl + "/resources/download/download-multi/" + checkedValues,
            success: function (data) {
                //console.log(data);
                if ($('.hidden-iframe').is(':hidden')) {
                    $('.hidden-iframe').replaceWith('<div class="hidden-iframe" style="display:none;"><iframe src="' + data + '"></iframe></div>');
                } else {
                    $('body').append('<div class="hidden-iframe" style="display:none;"><iframe src="' + data + '"></iframe></div>');
                }
                checkedValues = [];
            },
            error: function (xhr, textStatus, thrownError) {
                checkedValues = [];
                //console.log(xhr);
                //console.log(textStatus);
                //console.log(thrownError);
            }
        }, "json");
    });

    /* SCRIPTS FOR FILE INFORMATION CHANGE FORM
     ===============================================================*/
    $('body').on('click', '.basic-file-change-submit', function (e)
    {
        if (!$.trim($('.inp-field').val()).length) {
            e.preventDefault();
            $('body .validation-warning').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Please fill out all form fields.</div>');
        } else {
            $('.updateFileInfo').submit();
        }
    });
    $('body').on('click', '.fileInfoChange', function ()
    {
        $('.hidden-file-id').val($(this).data('id'));
    });
    /* END OF FILE INFO UPDATE SCRIPTS */

    /* TRASHBIN 
     ===============================================================*/
    if ($('.item').length == 0) {
        $('.soft-deleted-items').after('<div class="alert alert-info">Nothing in the trash. How tidy of you!</div>');
    }
    ;

    $('.test-console').on('click', function ()
    {
        //console.log('clicked');
    });
});