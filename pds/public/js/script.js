maxWords = 1000;
$(document).ready(function(){
    /* AJAX LOADING GIF ON START AND STOP */
    $(document).on({
        ajaxStart: function() { 
            $('body').addClass('loading'); 
        },
        ajaxStop: function() { 
            $('body').removeClass('loading'); 
        }
    });
    /* SCRIPTS TO VALIDATE BASIC USER INFORMATION CHANGE FORM
    ===============================================================*/
    $('body').on('click', '.basic-info-change-submit', function(e)
    {
        if(!$.trim($('.inp-field').val()).length) {
            e.preventDefault();
            $('body .validation-warning').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Please fill out all form fields.</div>');
        }else{
            $('.updateBasicInfo').submit();
        }
    });
    /* END OF BASIC INFO UPDATE FORM VALIDATION */
    $('#whatwedo.nav-item')
        .mouseover(function()
        {
            $('body #whatwedo').addClass('open');
        })
        .mouseout(function()
        {
            $('body #whatwedo').removeClass('open'); 
        });

    $('.training')
       .mouseover(function()
        {
            $('body .training').addClass('open');
        })
        .mouseout(function()
        {
            $('body .training').removeClass('open'); 
        });

    /* MARKETPLACE PAGINATION SCRIPTS */
    var previous_tab = $('.pagination li.active').prev().text();
    var current_tab = $('.pagination li.active a').attr('href');
    var next_tab = $('.pagination li.active').next().text();
    var base_url = window.location.protocol+"//"+window.location.host;
    $('.prev-page-arrow a').attr('href', base_url+"/marketplace/company/list?letter="+previous_tab[0]);
    $('.next-page-arrow a').attr('href', base_url+"/marketplace/company/list?letter="+next_tab[0]);

    if (previous_tab[0] == '«') { $('.prev-page-arrow a').attr('href', '#'); };
    if (next_tab[0] == '»') { $('.next-page-arrow a').attr('href', '#'); };

    /* MESSAGE BOARD FILTER SCRIPTS */
    if($('#categorySelect').length > 0) {
        $('.real_cat').val('');
        $('#categorySelect').change(function(){
            $('.real_cat').val($(this).find(':selected').text());

           //console.log($('.real_cat').val());

            $('.filter-listings').submit();
        });
    }

    if($('#subCategorySelect').length > 0) {
        $('.real_subcat').val('none');
        $('#subCategorySelect').change(function(){
            $('.hidden_cat').val($('#categorySelect').find(':selected').text());
            $('.real_subcat').val($(this).find(':selected').text());
            $('.subcat-filter-listings').submit();
        });
    }

    if($('.dropdown-sub-item').length > 0)
    {
        $('.dropdown-sub-item').on('click', function()
        {

           //console.log($(this).text());

            $('.temporary_filter').val($(this).text());
            $('.filter-temp-listings').submit();   
        });
    }
    $('.add-topic-btn').on('click', function()
    {
        $('body .categoryTopic_hidden_id').val($(this).data('cat-id'));
    });

    /* MARKETPLACE FEATURED VENDORS CAROUSEL */
    $('.featured-item').eq(0).addClass("active").nextAll().css({'opacity':'0','display':'none'});
    var interval = setInterval("$.fn.slideshow()", 3000);
    
    /*-- stop slide show on mouseover --*/
    $('.featured-item').mouseover(function() {
          clearInterval(interval);
    });
    
    /*-- restart slide show on mouseout --*/
    $('.featured-item').mouseout(function() {
          interval = setInterval("$.fn.slideshow()", 3000);
    });

    /*-- html editor and logo previews (account editing) --*/
    if($('.logo-preview').length > 0) {
        load_image_preview();
    }
    if($('#wysihtml5-toolbar').length > 0) {
        add_html_editor();
    }

    /*-- put new description in hidden form field so we can see it come through --*/
    $('.update-market-listing').submit(function(e)
    {
        //e.preventDefault();
        var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
        $('#hidden-html').val(html);
    });

    /*-- forum create new reply: store html message before submit --*/
//    $('.add-reply').submit(function(e)
//    {
//        var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
//        $('#hidden-html').val(html);
//    });
//    $('.add-reply').submit(function(event) {
//        $('#hidden-editor').val($('#editor').cleanHtml());
//    });
    
    $('.updateForumTopic').submit(function(e) {
        $('#hidden-update-editor').val($('.updateTopicEditor').cleanHtml());
    });

    /*-- forum update reply: store html message before submit --*/
    $('.updateForumTopic').submit(function(e)
    {
        var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
        $('#hidden-html').val(html);
    });

    //console.log(document.URL.split('?')[1]);
    
    if($('.active-category').length == 0) {
        selectActiveCategory();
    }
    
    /*-- showing and hiding the form to update the announcement text on the main dashboard -- DIRECTLY FROM DAN --*/
    $('.edit-announcement').on('click', function() {
       //console.log($('.announcement-update-quote').length);
        if($('.announcement-update-quote').is(':hidden')){
            $('.announcement-quote').hide();
            $('.announcement-update-quote').show();
        }else{
            $('.announcement-quote').show();
            $('.announcement-update-quote').hide();
        }
    });
    
    /*-- handling the user's projects tab form submit to access their projects in teamwork --*/
    $('.project-access').on('click', function(){
        $('#teamworkAccessForm').submit();
    });

});

$.fn.slideshow = function() { 
    var cp = $(".featured-item.active").index();
    var np = (cp<$(".featured-item").length-1?cp+1:0);
    $('.featured-item').eq(cp).animate({opacity: 0}, 250, function() {
        $('.featured-item').eq(cp).removeClass("active").css('display','none');
        $('.featured-item').eq(np).addClass("active").css('display','block');
        $('.featured-item').eq(np).animate({opacity: 100}, 1000);
     });
}

/* MARKETPLACE FILTER FUNCTION METHODS */
//ajax for subcategories after category select
function getSubCategories(categoryId) {
    var base_url = window.location.protocol+"//"+window.location.host;
    $.ajax({
        type: "POST",
        cache: false,
        url: base_url + '/conference/vendors/subcategories/' + categoryId,
        success: function(response) {
            $('#subCategorySelect').html(response);
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
            //console.log(textStatus);

           //console.log(thrownError);

        }
    });
}

/* HTML EDITOR */
function add_html_editor() {
    var editor = new wysihtml5.Editor("editor", { // id of textarea element
        toolbar:      "wysihtml5-toolbar", // id of toolbar element
        parserRules:  wysihtml5ParserRules, // defined in parser rules set 
        stylesheets:  ['{{ URL::to('/') }}/css/Conference/2015/wysiwig-styles.css'],
        supportTouchDevices: true
    });
    editor.on("load", function() {
        if($('.event-textarea').length > 0) { maxWords = 1000; }
        check_first_words($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor'));;
    });

    editor.on("newword:composer", function() {
        if($('.event-textarea').length > 0) { maxWords = 1000; }
        check_words($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor'));
    });
    jQuery('textarea').change(function() {
        var words = $(this).val().split(/\b[\s,\.-:;]*/);
        if (words.length > maxWords) {
            words.splice(maxWords);
            $(this).val(words.join(""));
            alert("You've reached the maximum allowed words. Extra words removed.");
        }
    });
}
    

function load_image_preview() {
    //marketplace logo update logic (show and hide update form)
    var updateLogo = $('.change-marketplace-logo');
    var currlogo = $('.logo-preview');
    var newLogo = $('.new-logo');
    var newLogoBtn = $('.new-logo input');
    var cancelLogoUpdate = $('.update-cancel');

    updateLogo.on('click', function()
    {
        currlogo.hide("fast", function()
        {
            newLogo.removeClass('hidden');
            newLogo.addClass('fix-block');
            newLogo.show();
            //return false;
        });
    });

    cancelLogoUpdate.on('click', function()
    {
        newLogo.removeClass('fix-block');
        newLogo.addClass('hidden');
        newLogo.hide("fast", function()
        {  
            currlogo.show();
        });

    });
}

function check_first_words(elem) {
    var wordcount;

   //console.log('+++++++++++++++++++++++++++++++');
   //console.log(elem.text());
   //console.log('+++++++++++++++++++++++++++++++');


    words = elem.text();
    //console.log(words.length);
    wordcount = words.split(/\b[\s,\.-:;]*/).length;
    if (wordcount > maxWords) {
        jQuery("body .word_count span").text("" + maxWords);
       //console.log('failed check first words');
        alert("You've reached the maximum allowed words.");
        return false;
    } else {

       //console.log('success');

        return jQuery(".word_count span").text(wordcount);
    }
}

function selectActiveCategory() {
    if(document.URL.indexOf('events') > -1) {
        var current = document.URL.split('/')[6];
        if(current == null || current === 'undefined') {
            current = '0';
        }
        $('.event_type_list li').eq(current).addClass('active-category');
    } else {
        var current = document.URL.split('?')[1];
       //console.log('here is your current url split');
       //console.log(current);
        if(current == null || current === 'undefined') {
            current = '0';
        } else {
            current = current.split('=')[1];
        }

        $('.forum-category-list ul li.item-'+current).addClass('active-category');
    }
    

    
}

function check_words(elem) {
    var wordcount;

   //console.log('+++++++++++++++++++++++++++++++');
   //console.log(elem.text());
   //console.log('+++++++++++++++++++++++++++++++');


    words = elem.text().trim();
    //console.log(words.length);
    wordcount = words.split(/\b[\s,\.-:;]*/).length;
    if (wordcount > maxWords) {
        jQuery("body .word_count span").text("" + maxWords);
       //console.log('failed check words');
        alert("You've reached the maximum allowed words in the forum.");
        return false;
    } else {
        return jQuery(".word_count span").text(wordcount);
    }
}