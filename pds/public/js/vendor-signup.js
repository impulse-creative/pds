//Vendor Sign up JS

function highlightSponsorshipDetails(input, position) {
    
}


var base_url = window.location.protocol+"//"+window.location.host;
var maxWords = 1000;

//ajax for subcategories after category select
function getSubCategories(categoryId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: base_url + '/conference/vendors/subcategories/' + categoryId,
        success: function(response) {
            $('#subCategorySelect').html(response);
        },
        error: function(xhr, textStatus, thrownError) {
            //console.log(xhr);
            //console.log(textStatus);

           //console.log(thrownError);

        }
    });
}

function add_html_editor() {
    var editor = new wysihtml5.Editor("editor", { // id of textarea element
        toolbar:      "wysihtml5-toolbar", // id of toolbar element
        parserRules:  wysihtml5ParserRules, // defined in parser rules set 
        stylesheets:  ['{{ URL::to('/') }}/css/Conference/2015/wysiwig-styles.css'],
        supportTouchDevices: true
    });
    editor.on("load", function() {
        check_first_words($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor'));
        //console.log('loaded');
        //console.log($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').text());
    });

    editor.on("newword:composer", function() {
        //console.log('new word added');
        //console.log('--------');
        check_words($('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor'));
    });
    jQuery('textarea').change(function() {
        var words = $(this).val().split(/\b[\s,\.-:;]*/);
        if (words.length > maxWords) {
            words.splice(maxWords);
            $(this).val(words.join(""));
            alert("You've reached the maximum allowed words. Extra words removed.");
        }
    });
}
    

function load_image_preview() {
    //marketplace logo update logic (show and hide update form)
    var updateLogo = $('.change-marketplace-logo');
    var currlogo = $('.logo-preview');
    var newLogo = $('.new-logo');
    var newLogoBtn = $('.new-logo input');
    var cancelLogoUpdate = $('.update-cancel');

    updateLogo.on('click', function()
    {
        currlogo.hide("fast", function()
        {
            newLogo.removeClass('hidden');
            newLogo.addClass('fix-block');
            newLogo.show();
            //return false;
        });
    });

    cancelLogoUpdate.on('click', function()
    {
        newLogo.removeClass('fix-block');
        newLogo.addClass('hidden');
        newLogo.hide("fast", function()
        {  
            currlogo.show();
        });

    });
}

function check_first_words(elem) {
    var wordcount;

   //console.log('+++++++++++++++++++++++++++++++');
   //console.log(elem.text());
   //console.log('+++++++++++++++++++++++++++++++');


    words = elem.text();
    //console.log(words.length);
    wordcount = words.split(/\b[\s,\.-:;]*/).length;
    if (wordcount > maxWords) {
        jQuery(".word_count span").text("" + maxWords);
        alert("You've reached the maximum allowed words.");
        return false;
    } else {

       //console.log('success');

        return jQuery(".word_count span").text(wordcount);
    }
}

function check_words(elem) {
    var wordcount;

   //console.log('+++++++++++++++++++++++++++++++');
   //console.log(elem.text());
   //console.log('+++++++++++++++++++++++++++++++');


    words = elem.text().trim();
    //console.log(words.length);
    wordcount = words.split(/\b[\s,\.-:;]*/).length;
    if (wordcount > maxWords) {
        jQuery(".word_count span").text("" + maxWords);
        alert("You've reached the maximum allowed words.");
        return false;
    } else {
        return jQuery(".word_count span").text(wordcount);
    }
}


$(document).ready(function(){
    if($('#categorySelect').length > 0) {
        $('#categorySelect').on('change', function() {
            var options = getSubCategories($(this).val());
        
        }); 
    }
    if($('.logo-preview').length > 0) {
        load_image_preview();
    }
    if($('#wysihtml5-toolbar').length > 0) {
        add_html_editor();
    }
    
    
    $('.market-listing-form').submit(function(e)
    {
        //e.preventDefault();
        var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
        $('#hidden-html').val(html);
    });

    $('.guide-entry-form').submit(function(e)
    {
        //e.preventDefault();
        var html = $('.wysihtml5-sandbox').contents().find('body.wysihtml5-editor').html();
        $('#hidden-html').val(html);
    });

    //Sponsorship description highlights
    if($('#sponsorship-inputs').length === 1) {
         $('#sponsorship-details p').hide();
        $('#sponsorship-inputs').on('mouseenter', 'label', function() {
            var eq = parseInt($(this).find('input').val()) - 1;
            $('#sponsorship-details p').eq(eq).show();
            $('#sponsorship-details p').eq(eq).addClass('sponsorship-details-highlight');
        });
        $('#sponsorship-inputs').on('mouseleave', 'label', function() {
            var eq = parseInt($(this).find('input').val()) - 1;
            $('#sponsorship-details p').eq(eq).hide();
            $('#sponsorship-details p').eq(eq).removeClass('sponsorship-details-highlight');
        });
    }
});