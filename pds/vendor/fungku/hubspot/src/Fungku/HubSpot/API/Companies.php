<?php

namespace Fungku\HubSpot\API;

/**
* Copyright 2014 Impulse Creative
*
*   Licensed under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied.  See the License for the specific
* language governing permissions and limitations under the
* License.
*/

class Companies extends BaseClient {
    //Client for HubSpot Contacts API

        //Define required client variables
    protected $API_PATH = 'companies';
    protected $API_VERSION = 'v2';

    /* Get All Companies */

    public function get_all_companies($params) {
        $endpoint = 'companies';
        try{
            return json_decode($this->execute_get_request($this->get_request_url($endpoint,$params)));
        }
        catch(HubSpotException $e){
            throw new HubSpotException('Unable to get all companies: '.$e);
        }
    }

    /* Get Company by ID */

    public function get_company($id, $params = array()) {
        $endpoint = 'companies/'.$id;
        try{
            return json_decode($this->execute_get_request($this->get_request_url($endpoint,$params)));
        }
        catch(HubSpotException $e){
            throw new HubSpotException('Unable to get all companies: '.$e);
        }    
    }

    public function get_company_groups($params = array()) {
        $endpoint = 'groups';
        try{
            return json_decode($this->execute_get_request($this->get_request_url($endpoint,$params)));
        }
        catch(HubSpotException $e){
            throw new HubSpotException('Unable to get all companies: '.$e);
        }    
    }

    public function add_company_properties($params) {
        $endpoint = 'properties';
        try{
            return json_decode($this->execute_JSON_post_request($this->get_request_url($endpoint, null),json_encode($params)));
        }
        catch(HubSpotException $e){
            throw new HubSpotException('Unable to get all companies: '.$e);
        }    
    }
    public function update_company($id, $params) {
        $endpoint = 'companies/'.$id;
        try{
            return json_decode($this->execute_put_request($this->get_request_url($endpoint, null),json_encode($params)));
        }
        catch(HubSpotException $e){
            throw new HubSpotException('Unable to get all companies: '.$e);
        }    
    }
    
   
    public function get_all_employees($companyId, $params = array()) {
        $endpoint = 'companies/'.$companyId.'/vids';
        try{
            return json_decode($this->execute_get_request($this->get_request_url($endpoint,$params)));
        }
        catch(HubSpotException $e){
            throw new HubSpotException('Unable to get all companies: '.$e);
        }    
    }


}

?>